'''
Created on 23-12-2012

@author: lucst
'''
import os
import re
import sys



#version = "1.3.0"

##Check if user has input version number 
try: 
    version = sys.argv[1];
except: 
    raise Exception("Given version is not in standard format. Use e.g. '1.0.0'");
    sys.exit(1);
    
##Check if given version is in correct version format
if not re.compile("\b\d+.\d+.\d+\b").match('\b'+version+'\b'):
    raise Exception("Given version is not in standard format. Use e.g. '1.0.0'");
    sys.exit(1);

try:
    main_program = sys.argv[2];
except:
    raise Exception("Missing information what setup.py should be user. Standard or CMD main file");
    sys.exit(1);

##Check what type of setup.py must be executed
if main_program=="setupCMD":
    setup_name = r"setup_basicCMD.py";
elif main_program =="setupBasic":
    setup_name = r"setup_basic.py";
else: 
    raise Exception("Given setup name is not in standard format. Use e.g. 'setupCMD' or 'setupBasic' ");
    sys.exit(1);
##This will be version number
print "Building for version number:", version;


##change version in setup.py file for py2exe
setupbasic_file = open(setup_name, "rb");
setup_file = open(r"setup.py", 'wb');
for line in setupbasic_file:
    if "version" in line:
        line = re.sub("\d+.\d+.\d+",version,line)  #change match string
    setup_file.write(line);
    setup_file.flush();
setup_file.close();
setupbasic_file.close();


##change version in ui_QCinter_About.py file
ui_QCinter_AboutFile_curr = r"..\src\lib\QCabout\guiLib\ui_QCinter_About.py";
ui_QCinter_AboutFile_tmp = r"..\src\lib\QCabout\guiLib\ui_QCinter_About_new.py";
ui_QCinter_AboutBasic_file = open(ui_QCinter_AboutFile_curr, "rb");
ui_QCinter_About_file = open(ui_QCinter_AboutFile_tmp, 'wb');
for line in ui_QCinter_AboutBasic_file:
    if "version" in line:
        line = re.sub("\d+.\d+.\d+",version,line)  #change match string
    ui_QCinter_About_file.write(line);
    ui_QCinter_About_file.flush();
ui_QCinter_About_file.close();
ui_QCinter_AboutBasic_file.close();

os.remove(ui_QCinter_AboutFile_curr); ##Delete file old ui file 
os.rename(ui_QCinter_AboutFile_tmp, ui_QCinter_AboutFile_curr) ##change name of new file with changed version 
