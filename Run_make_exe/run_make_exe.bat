

rmdir /S /Q QCinterExec


IF [%1]==[] echo Missing progam version
IF [%1]==[] EXIT /B 1


REM Run Exe generation for qcintegartion Gui
c:\python26\python setprogramVersion.py %1 "setupBasic"

c:\python26\python setLogginLevel.py "Production"
copy /Y setup.py ..\src\
c:\python26\python ..\src\setup.py py2exe
c:\python26\python setLogginLevel.py "Internal"
del setup.py
del ..\src\setup.py

mkdir QCinterExec
mkdir .\QCinterExec\main
xcopy /D /E /Y .\dist .\QCinterExec\main
rmdir /S /Q .\dist
rmdir /S /Q .\build

Run Exe generation for qcintegartion CMD
c:\python26\python setprogramVersion.py %1 "setupCMD"

c:\python26\python setLogginLevel.py "Production"
copy /Y setup.py ..\src\
c:\python26\python ..\src\setup.py py2exe
c:\python26\python setLogginLevel.py "Internal"
del setup.py
del ..\src\setup.py

copy /Y .\dist\QCinterCMD.exe .\QCinterExec\main\QCinterCMD.exe
rmdir /S /Q .\dist
rmdir /S /Q .\build 




mkdir .\QCinterExec\Result\QCgraph
mkdir .\QCinterExec\conf
copy /Y %CD%\..\src\conf\test.cfg .\QCinterExec\conf\test.cfg

copy /Y Graphic_Icon\icon\qci_ikona.ico QCinterExec\main\qci_ikona.ico
copy /Y QCintegration.bat QCinterExec\QCintegration.bat

start Bat_To_Exe_Converter.exe -bat QCinterExec\QCintegration.bat -save QCinterExec\QCintegration.exe -invisible -icon QCinterExec\main\qci_ikona.ico


rmdir /S /Q QCintegration_install
mkdir QCintegration_install

"%PROGRAMFILES(X86)%\NSIS\makensis.exe" .\NullSoftInstall\QCinter_installer.nsi

del QCinterExec\QCintegration.bat