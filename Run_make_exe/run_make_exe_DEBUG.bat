rmdir /S /Q QCinterExec

REM set program version
c:\python26\python setprogramVersion.py "0.0.0"

REM In logfile.py change level of loggin to higher = ERROR
c:\python26\python setLogginLevel.py "Production"
copy /Y setup.py ..\src\
REM Create exe file from py
c:\python26\python ..\src\setup.py py2exe
REM Switch level of loggin to defult value.
c:\python26\python setLogginLevel.py "Internal"
del setup.py
del ..\src\setup.py

mkdir QCinterExec
mkdir .\QCinterExec\main
xcopy /D /E /Y .\dist .\QCinterExec\main
rmdir /S /Q .\dist
rmdir /S /Q .\build


mkdir .\QCinterExec\Result\QCgraph
mkdir .\QCinterExec\conf
copy /Y %CD%\..\src\conf\test.cfg .\QCinterExec\conf\test.cfg

copy /Y Graphic_Icon\icon\qci_ikona.ico QCinterExec\main\qci_ikona.ico
copy /Y QCintegration.bat QCinterExec\QCintegration.bat

start Bat_To_Exe_Converter.exe -bat QCinterExec\QCintegration.bat -save QCinterExec\QCintegration.exe -invisible -icon QCinterExec\main\qci_ikona.ico
