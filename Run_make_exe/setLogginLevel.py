import os
import sys

arg = sys.argv[1];

if arg=="Production": 
    os.rename(r"..\src\lib\logfile.py", r"..\src\lib\logfile_old.py");
    
    logfile_old = r"..\src\lib\logfile_old.py";
    logfile_new = r"..\src\lib\logfile.py";
    logfileBasic_file = open(logfile_old, "rb");
    logfile_file = open(logfile_new, 'wb');
    for line in logfileBasic_file:
        if "self.log.setLevel(logging.DEBUG)" in line:
            line = line.replace("DEBUG","ERROR")  #disable prinitng log into file
        logfile_file.write(line);
        logfile_file.flush();
    logfile_file.close();
    logfileBasic_file.close();
elif arg=="Internal":
    try: 
        os.rename(r"..\src\lib\logfile_old.py", r"..\src\lib\logfile.py");
    except OSError:
        os.remove(r"..\src\lib\logfile.py");
        os.rename(r"..\src\lib\logfile_old.py", r"..\src\lib\logfile.py");
    
     




