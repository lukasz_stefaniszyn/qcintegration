'''
Created on 2010-06-07

@author: Lucas
'''
#!/usr/bin/env python

"""***************************************************************************
**
** Copyright (C) 2005-2005 Trolltech AS. All rights reserved.
**
** This file is part of the example classes of the Qt Toolkit.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 as published by the Free Software Foundation
** and appearing in the file LICENSE.GPL included in the packaging of
** this file.  Please review the following information to ensure GNU
** General Public Licensing requirements will be met:
** http://www.trolltech.com/products/qt/opensource.html
**
** If you are unsure which license is appropriate for your use, please
** review the following information:
** http://www.trolltech.com/products/qt/licensing.html or contact the
** sales department at sales@trolltech.com.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
***************************************************************************"""

import sys
import os
from PyQt4 import QtCore, QtGui

import qc_walk


class QCstart(object):
    
    def __init__(self):
        print "setUp"
        
        import qc_connect_disconnect
        import crypt_pass
        
        server = "http://muvmp034.nsn-intra.net/qcbin/"; 
        UserName="stefaniszyn";
        pass_ = "e8dbf113946b941a4a34b573363674a3c58520";
        Password = crypt_pass.Heslo(pass_)._Heslo__readHeslo();
        
        DomainName="HIT7300";
        ProjectName="hiT73_R43x";
        
        qcConnect = qc_connect_disconnect.QC_connect(server, UserName, Password, DomainName, ProjectName);
        if qcConnect.connect():
            print "Connected OK";
            self.qcConnect = qcConnect;
        else:
            print "ERR: Not connected"
            self.qcConnect = None;
        
        

    def tearDown(self):
        print "tearDown"
        
        if self.qcConnect is None:
            return;
        
        if self.qcConnect.disconnect_QC():
            print "\nDisconnecting from QC server."\
                    "\n"\
                    "\nThank You for using QC_import_export script.\n"\
                    "\t\t\t\n"\
                    "\t\t\t  Lukasz Stefaniszyn\n"\
                    "\n\t\t\t\t\tPowered by Python 2.5";
            return True;
        else:
            print "Unable to to disconnect";
            return False

    def getDirTestCase(self, ItemQcInstance=None):
        
        if ItemQcInstance is None:
            ItemQcInstance = qc_walk.getRoot(self.qcConnect.qc);
        getDirTestCase = qc_walk.getDirTestCase(ItemQcInstance);
        print "getDirTestCase=", getDirTestCase;
        return getDirTestCase


class TreeItem:
    '''Here are all operation on one item which is given to  -> TreeModel  and then as model to -> TreeView'''
    
    def __init__(self, data, parent=None, iconDirectory=False):
        self.parentItem = parent
        self.itemData = data
        self.childItems = []
        self.columnText = 0  ##this information about place in data where is text, which will be displayed in TreeView;
        self.iconDirectoryItem = iconDirectory; ##is item Directory or TestSet/TestCase

    def __str__(self):
        return "TreeItem, item name:%s"%str(self.itemData);

    def appendChild(self, item):
        '''Append new child into current child.
        input = TreeItem(newChild, parent)'''
        self.childItems.append(item)

    def removeChild(self, row):
        '''Remove child form current childs
        input = int(row)
        '''
        del self.childItems[row];
    
    def child(self, row):
        '''Return child instance,by row number, from upper child
        input= int(row)''' 
        return self.childItems[row]

    def childCount(self):
        '''Give number of childrens as int()'''
        return len(self.childItems)

    def columnCount(self):
        '''Return number of columns  int()'''
        return len(self.itemData)

    def data(self, column):
        return self.itemData[column]

    def parent(self):
        '''Return childs parent'''
        return self.parentItem

    def row(self):
        '''Return row number as int()'''
        if self.parentItem:
            return self.parentItem.childItems.index(self)
        return 0


class TreeModel(QtCore.QAbstractItemModel):
    '''This will create model of data'''
    
    def __init__(self, data, parent=None):
        QtCore.QAbstractItemModel.__init__(self, parent)
        
        rootData = []
        rootData.append(QtCore.QVariant("Subject"))
        self.rootItem = TreeItem(rootData);
        self.iconFile();
        ##tutaj beda tworzone i dodawane liniji do tablicy
#        data = QCstart.getDirTestCase();
        updatemodeltree(self.rootItem, data);

    def iconFile(self):
        '''
        This will sets what icon will be used to TreeView for Directory and Test. 
        Information will be saved under self.filenameDirectory and self.filenameTest
        input = None;
        output = None;      
        '''
        currentDir = os.getcwd();
        self.filenameDirectory = currentDir + r"/" + r"gtk-directory.png";
        self.filenameTest = currentDir + r"/" + r"gtk-file.png";
        return;
        
        
    
    def columnCount(self, parent):
        if parent.isValid():
            return parent.internalPointer().columnCount();
        else:
            return self.rootItem.columnCount();

    def data(self, index, role):
        '''
        This is a heart of TreeModel. 
        By information from data() item from TreeItem will be modeled/visible.
        Decision of what to model/show are in Role attribute.  
        '''
        
        if not index.isValid():
            return QtCore.QVariant()

        if role == QtCore.Qt.DisplayRole:
            ##this will show text of item from TreeItem
            item = index.internalPointer();
            return QtCore.QVariant(item.data(item.columnText));
        elif role == QtCore.Qt.DecorationRole:
            ##this will show icon item from TreeItem
            item = index.internalPointer();
            if item.iconDirectoryItem:
                filename = self.filenameDirectory;
            else:
                filename = self.filenameTest;
            pixmap = QtGui.QPixmap(filename)
            if pixmap.isNull():
                return QtCore.QVariant();
            return QtCore.QVariant(pixmap);
        else:
            return QtCore.QVariant()
        
    def flags(self, index):
        '''This function is triggered every time when user points on some item from tree view'''
        if not index.isValid():
            return QtCore.Qt.ItemIsEnabled

        return QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable

    def headerData(self, section, orientation, role):
        if orientation == QtCore.Qt.Horizontal and role == QtCore.Qt.DisplayRole:
            return self.rootItem.data(section)

        return QtCore.QVariant()

    def index(self, row, column, parent):
        '''To prawdopobodbie pozwala umiescic child w odpowiednim parent
        poprzez self.createIndex(row, column, childItem)'''
        
        if row < 0 or column < 0 or row >= self.rowCount(parent) or column >= self.columnCount(parent):
            #print "QModelIndex:", QtCore.QModelIndex();
            return QtCore.QModelIndex()
        if not parent.isValid():
            parentItem = self.rootItem
        else:
            parentItem = parent.internalPointer()
        childItem = parentItem.child(row)
        if childItem:
            #print "createIndex(row, column, childItem):",(row, column, childItem) 
            return self.createIndex(row, column, childItem)
        else:
            return QtCore.QModelIndex()

    def parent(self, index):
        '''To tworzy parent w odpowiednim miejscu 
        self.createIndex(parentItem.row(), 0, parentItem)'''
        
        if not index.isValid():
            return QtCore.QModelIndex()
        childItem = index.internalPointer()
        parentItem = childItem.parent()
        if parentItem == self.rootItem:
            return QtCore.QModelIndex()
        return self.createIndex(parentItem.row(), 0, parentItem)

    def rowCount(self, parent):
        if parent.column() > 0:
            return 0
        if not parent.isValid():
            parentItem = self.rootItem
        else:
            ##To zwraca polozenie parent
            parentItem = parent.internalPointer()
        return parentItem.childCount()

class TreeView(QtGui.QTreeView):
    '''Give all operation made on user tree view. Like action expand/collapse, double clicked on item view
    This inherit also QAbstractItemModel
    '''
    def __init__(self, parent=None, _model=None):
        QtGui.QTreeView.__init__(self, parent)
        
        self.model = _model;
    
        ##Connect Signals Expanding/Collapsing/DoubleClicked tree to slots
        self.connect(self, QtCore.SIGNAL("expanded (const QModelIndex &)"),
             self.expanded);
        self.connect(self, QtCore.SIGNAL("collapsed (const QModelIndex &)"),
             self.collapsed);
        self.connect(self, QtCore.SIGNAL("doubleClicked (const QModelIndex &)"),
             self.doubleClicked);

    def doubleClicked(self, index):
        '''Connect to the QC to get all folders/test instances. 
        Put results on the user tree view
        
        Signal trigged by user, double click on one of the items from tree view
        input =  QModelIndex(index)
        output = None  
        ''' 
        
        print "doubleClicked";     
        item = index.internalPointer();
        print "Index data:", item.itemData; 
        childrenCount = item.childCount();
        
        
        if childrenCount==0:            
            ##Before add childs in the dataTree, there is need to add childs in modelTree
            self.model.beginInsertRows(index, childrenCount, childrenCount+2);

            if item.iconDirectoryItem:
                ##If item is DirectoryItem then make update TreeView
#                data = QCstart.getDirTestCase(item.itemData[1])
                data = ([
                     ('G test', "G-qcInstance"), 
                     ('H test', "H-qcInstance")
                     ],
                     [
                      ('G01 TestCase', 'TestCase-qcInstance')
                      ]
                     );
                updatemodeltree(index, data);
            else:
                ##If item is not DirectoryItem , then do nothing
                pass;
            ##When you add childs in dataTree, there is need to update those changes in modelTree
            self.model.endInsertRows();
            
        parent = self.model.rootItem
        for top, dirs, nondirs in walkTSitem(parent):
#            fw_file.write("Root: %s \t Dirs: %s \t Nondirs: %s\n"%(repr(top), repr(dirs), repr(nondirs)));
#            fw_file.flush();
            print "Top:", top,
            print "Dirs:", dirs;
            print "NonDirs:", nondirs;
            if nondirs == [] and dirs == []:
                print "Katalog dla QC i wszystkie jego TS/TC:", top;
            elif nondirs != []:
                print "POjedyncze TS/TC:", nondirs

        
    def expanded(self, index):
        '''Signal trigged by user, expand on one of the items from tree view
        input =  QModelIndex(index)
        output = None
        ''' 
        print "expanded";
        
        
    def collapsed(self, index):
        '''Signal trigged by user, collapse on one of the items from tree view
        input =  QModelIndex(index)
        output = None
        '''
        
        print "collapsed";
    
def updatemodeltree(index, data):
    '''
    This will update tree view structure of Directories, TestCases/TestSets under the given Index
    input = (index=TreeItem(),  
            data= ( [ 
                    ( str(DirectoryName), qcInstace(Directory) )
                    ], 
                    [
                    (str(TestName), qcInstance(Test) )
                    ] );
    output = None
    '''
    
    
    
    try: 
        ##parent position in model
        parent = index.internalPointer();
    except AttributeError:
        ## rootItem does not have internalPointer() method. By it self is parent 
        parent = index; ##rootItem
    
    
    ## new data list, from Quality Center

    catalogs = data[0];
    testsets = data[1];
#    print "catalogs",catalogs; 
    ##create item, but do not put this item into model
    if catalogs is not None:
        for catalog in catalogs:
#            print "catalog", catalog;
            item = TreeItem(catalog, parent, iconDirectory=True);
            ##put item into model
            parent.appendChild(item);
    if testsets is not None:
        for testset in testsets:
            item = TreeItem(testset, parent, iconDirectory=False);            
            ##put item into model
            parent.appendChild(item);
 
    

class testPlanTreeView(object):
    
    def __init__(self, q=None):
        ##To utworzy model, liniami ktore beda pozniej odczytywane 
#        if q is None:
#            data = QCstart.getDirTestCase();
#        else:
#            data = q.getDirTestCase();
        data = ([
                 ('A system Design', "A-qcInstance"), 
                 ('B Sevice and Transpoders',"B-qcinstance"), 
                 ('G TL1 Wroclaw', "G-qcinstance")
                ], 
                 [
                  ('G01 TestCase', 'TestCase-qcInstance')
                  ]);
    
        self.model = TreeModel(data)
        
        
    def makeTreeViewTestPlan(self):
        view = TreeView(_model=self.model)
        ##Stworzy nam model wraz z wypelnionymi polami
        view.setModel(self.model)
        view.setWindowTitle("TestPlan Tree Model")
#        view.show()
    
        ##This will add one Child in to main root
        model = self.model;
        parent = model.rootItem.child(0).parent();
        data = ('S test', "S-qcInstance")
        item = TreeItem(data, parent)
        model.rootItem.appendChild(item)
        view.show()
    
    #    ##This will add one Child into first child of root
    #    parent = model.rootItem.childItems[0];
    #    print "parent:", parent 
    #    data = ('D test', "D-qcInstance")
    #    item = TreeItem(data, parent)
    #    parent.appendChild(item);
        return view;
    
class testLabTreeView(object):
    
    def __init__(self):
        ##To utworzy model, liniami ktore beda pozniej odczytywane 
        data = QCstart.getDirTestCase();
        
    #    data = ([
    #             ('A system Design', "A-qcInstance"), 
    #             ('B Sevice and Transpoders',"B-qcinstance"), 
    #             ('G TL1 Wroclaw', "G-qcinstance")
    #            ], 
    #             [
    #              ('G01 TestCase', 'TestCase-qcInstance')
    #              ]);
    
        self.model = TreeModel(data)
        
        
    def makeTreeViewTestLab(self):
        view = TreeView(_model=self.model)
        ##Stworzy nam model wraz z wypelnionymi polami
        view.setModel(self.model)
        view.setWindowTitle("TestLab Tree Model")
        view.show()
    
        ##This will add one Child in to main root
    #    parent = model.rootItem.child(0).parent();
    #    data = ('S test', "S-qcInstance")
    #    item = TreeItem(data, parent)
    #    model.rootItem.appendChild(item)
    
    #    ##This will add one Child into first child of root
    #    parent = model.rootItem.childItems[0];
    #    print "parent:", parent 
    #    data = ('D test', "D-qcInstance")
    #    item = TreeItem(data, parent)
    #    parent.appendChild(item);
        return view;



def walk(parent, topdown = True):
    '''This will print all items in the TreeView '''
    top = parent.data(0)
    try:
        names = parent.childItems;
#        print "listdir:", names
    except:
        print "ERR: not possible to get Dir List";
        return

    dirs, nondirs = [], []
    for name in names:
        if name.iconDirectoryItem: 
            dirs.append(name)
        else:
            nondirs.append(name)
    
    if topdown:
#        print "top, dirs, nondirs", top, dirs, nondirs;
        #        yield top, [dir.Name for dir in dirs]
        yield top, [dir.data(0) for dir in dirs], [nondir.data(0) for nondir in nondirs]
    for name in dirs:
        for x in walk(name):
#            print "x", x
            yield x

def walkTSitem(parent, topdown = True):
    '''This will print only TestSets/TestCases list in the TreeView '''
    top = parent.data(0)
    try:
        names = parent.childItems;
        
#        print "listdir:", names
    except:
        print "ERR: not possible to get Dir List";
        return

    dirs, nondirs = [], [];
    if len(names)==0:
        print "len(0) This is dir with TreeView 0 childs:", top; 
        yield top, [], [];
        return;
   
    for name in names:
        if name.iconDirectoryItem:
            print "This is dir with TreeView 0 childs:", name;
            dirs.append(name); 
        else:
            nondirs.append(name)
            print "This is TestSet/TestCase item from TreeView:", name;
            
    if topdown:
#        print "top, dirs, nondirs", top, dirs, nondirs;
        yield top, [dir.data(0) for dir in dirs], [nondir.data(0) for nondir in nondirs]
    for name in dirs:
        for x in walkTSitem(name):
#            print "x", x
            yield x



#    try:
#        # Note that listdir and error are globals in this module due
#        # to earlier import-*.
#        names = parent.childItems;
#        #print "listdir:", [dir.Name for dir in names]
#    except:
#        print "ERR: not possible to get Dir List";
#        return
#
#    dirs = []
#    for name in names:
#        dirs.append(name)
#        
#
#    if topdown:
#        #print "top, dirs:", top, [dir.Name for dir in dirs];
#        yield top, [dir.Name for dir in dirs]
#    for name in dirs:
#        for x in walk(name, topdown, onerror):
#            #print "x", x
#            yield x


        







        
if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)

    qCstart= QCstart();
    
    testPlanTreeView_1 = testPlanTreeView(qCstart);
    treeView_1 = testPlanTreeView_1.makeTreeViewTestPlan();
    ##This will print current position on TreeView
    print "treeView_1_TestPlan.currentIndex:",treeView_1.currentIndex();
    

    
    model = treeView_1.model
    parent = model.rootItem;
     
#    a = walk(parent);

    fw_file = open(r"d:/Work/tmp/tmp/TestSet_dirList.txt", "wb");
    for top, dirs, nondirs in walkTSitem(parent):
        pass;
        fw_file.write("Root: %s \t Dirs: %s \t Nondirs: %s\n"%(repr(top), repr(dirs), repr(nondirs)));
        fw_file.flush();
        print "Top:", top,
        print "Dirs:", dirs;
        print "NonDirs:", nondirs;
        
        if nondirs == [] and dirs == []:
            print "Katalog dla QC i wszystkie jego TS/TC:", top;
        elif nondirs != []:
            print "POjedyncze TS/TC:", nondirs
        
        
    fw_file.close();
#    for i in a:
#        print i;
#    print "i:", i; 
#    testPlanTreeView_2 = testPlanTreeView()
#    treeView_2 = testPlanTreeView_2.makeTreeViewTestPlan();
#    ##This will print current position on TreeView
#    print "treeView_2_TestPlan.currentIndex:",treeView_2.currentIndex();
    
    
    sys.exit(app.exec_())
    qCstart.tearDown();