# -*- coding: utf-8 -*-
'''
Created on 2010-03-14

@author: Lucas
'''

from PyQt4 import QtCore, QtGui
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from lib.QCTestX.QCTestX_gui import SetQCTestX_TestLab, SetQCTestX_TestPlan
from lib.QCgraph.QCgraph_gui import SetQCgraph
from lib.QClicense import qcLicense_license, checkLicence
from lib.paramfile import OpenParameters, WriteParameters, PreviousParamFile, \
    GetCurrentParam
from lib.ui_QCinter_mainWindow import Ui_MainWindow
import lib.QualityCenter_ext.lib.qc_connect_disconnect as qc_connect_disconnect
import pythoncom
import pywintypes
import sys
#sip.setapi('QString', 2)

 



from globalVariables import GlobalVariables

from lib.logfile import Logger

from minehunter import minehunt
log = Logger(loggername="QCinter", resetefilelog=False).log;

#==============================================================================
# GlobalVariables
#==============================================================================


def criticalWindow(text):
    """Give critical MessageBox for QC connection
    input = str()"""
    QtGui.QMessageBox.critical(None, "Quality Center connection", text);


class ProgressDialog(QtGui.QProgressDialog):
    
        def __init__(self, max, parent=None):
            super(ProgressDialog, self).__init__(parent)

            self.setCancelButtonText("&Cancel")
            self.max = max;
            self.setRange(0, max)
            self.setWindowTitle("Quality Center progress")

        def updateProgress(self, value, text):
            
            
            self.setValue(value)
            self.setLabelText("%s\n Progress %d of %d..." %(text,
                                                            value, 
                                                            self.max));
            self.repaint();
#            QtGui.qApp.processEvents()
            
#            QtGui.qApp.processEvents()
            
            if value == self.max:
                self.close();

class DialogWindows(QtGui.QMessageBox):
    
    def __init__(self, parent):
        super(DialogWindows, self).__init__(parent);
        
        
    def criticalWindow(self, text):
        """Give critical MessageBox for QC connection
        input = str()"""
        QtGui.QMessageBox.critical(self, "Quality Center connection", text);
#------------------------------------------------------------------------------ 

class MainWindow(QtGui.QMainWindow):
    
    
    
    
    
    def __init__(self, parent = None):
        self.parent = parent
        ##set variables
        self.curFile = None;
        self.__qcConnect = None;
        
        ##Create Main Window
#        QtGui.QMainWindow.__init__(self)
        super(MainWindow, self).__init__()

        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        
        
        
        
        self.setupConnection();
        
#        Ui_MainWindow.__init__(self)
#        self.setupUi(self); #jest to metoda zdefiniowana w klasie Ui_MainForm

        
        
        ##Create Dialog Window with MessageBoxes
        self.DialogWindows = DialogWindows(self);
        
        
        ##Open and set previous modified ParameterFile 
        previousParamFile = PreviousParamFile(self);
        lastfileName = previousParamFile.get_lastParamFile();
        if lastfileName:
            self.loadFile(lastfileName);
        
        ##Set QCgraph tabWidget    
        self.setQCgraph = SetQCgraph(self);
        
        ##Set QCTestLab tabWidget
        self.setQCTestX_TestLab = SetQCTestX_TestLab(self)
        ##Set QCTestPlan tabWidget
        self.setQCTestX_TestPlan = SetQCTestX_TestPlan(self)
        
        ##Show main window. This is needed, due to LicenseManagement windows, which may pop-up earlier
        self.show();

        ##Check license 
        self.dataStartupLicenceCheck = checkLicence.DataStartupLicenceCheck(parent=self);
        for message in self.dataStartupLicenceCheck.dataStartupCheck(): 
            pass;
        if not self.dataStartupLicenceCheck.status:
            self.userLicense(False)
        else: 
            pass;
        
        ##Button Handlers:
        self.parent.aboutToQuit.connect(self.quitApplication);  ##operate on Quit buttons
        self.ui.pushButton_Quit.clicked.connect(self.parent.quit); ##operate on Quit buttons  
        
        
    def userLicense(self, status):
        '''
        Set ConnectionButton enabled/disabled depending on the LicenseManagement information
        
        *Input*
        @param status: True/False. Status of license, 
            True- user has license and enable ConnetionButton
            False - user has not license and disable ConnectionButto
        *Output*
        None
        '''
        
        buttonStatusNow = self.ui.pushButton_ConnectQC.isEnabled();
        
        if status:
            if buttonStatusNow: pass ##already enabled
            else: self.ui.pushButton_ConnectQC.setEnabled(True);##enable
        elif not status:
            if not buttonStatusNow: pass ##already disabled
            else: self.ui.pushButton_ConnectQC.setEnabled(False); ##disable
        else:
            pass;


    
    
    def setupConnection(self):
        '''
        This is declaration of connected Buttons, Actions. 
        '''

        ##Button
        self.ui.pushButton_SaveParameterFile.clicked.connect(self.saveAs);
        self.ui.pushButton_OpenParameterFile.clicked.connect(self.open);
        self.ui.pushButton_ConnectQC.clicked.connect(self.qc_connect);
        
        ##Action
        self.ui.actionLicense_Status.triggered.connect(self.licenseStatus)
        self.ui.actionMinehunter.triggered.connect(self.game_minehunt);

    def licenseStatus(self):
        '''
        Open widget for License Management
        '''
        
        qcLicense_license.QcLicense(parent=self)
        
        
    
    def game_minehunt(self):
        '''
        Open game minehunt
        '''
        log.debug("Run game");
        minehunt.main(self.parent, self);
    #----------------------------------------------------------------------------- 
    def __get_qcConnect(self):
#        if self.__qcConnect:
#            print "QC connected ?:", self.__qcConnect.qc.ProjectConnected;
        return self.__qcConnect;
    def __set_qcConnect(self, qcConnect):
        self.__qcConnect = qcConnect;
    def __del_qcConnect(self):
        if self.__qcConnect.disconnect_QC():
            text = "Disconnecting from QualityCenter server"
            QtGui.QMessageBox.information(None, "Quality Center connection", text);
        else:
            text = "Unable disconnect from QualityCenter"
            QtGui.QMessageBox.information(None, "Quality Center connection", text);
            log.info(text);
    qcConnect = property(__get_qcConnect, __set_qcConnect, __del_qcConnect, "QC connetion instance");
    
    def __get_QCconnection_status(self):
        return self.__QCconnection_status;
    def __set_QCconnection_status(self, status):
        self.__QCconnection_status = status;
        #print "Zmieniam QCconnection_status";
    QCconnection_status = property(__get_QCconnection_status, __set_QCconnection_status);
    #----------------------------------------------------------------------------- 


    #----------------------------------------------------------------------------- 
    ##Close application
    def quitApplication(self):
        log.info("Closing application");
        try: self.qcConnect.disconnect_QC();
        except: pass;
        
        
    
    
    ##Open file
    def open(self):
        fileName = QtGui.QFileDialog.getOpenFileName(self, self.tr("Open File"),
                                                     GlobalVariables.DIR_CONF,
                                                     self.tr("Text (*.cfg)"));
        
#        if not fileName.isEmpty():
        if fileName != '':
            self.loadFile(fileName)    
   
    def loadFile(self, fileName):
        log.info(("fileName:", fileName));
        file = QtCore.QFile(fileName)
        
        file.open( QtCore.QFile.ReadOnly | QtCore.QFile.Text); 
        if not file:
            QtGui.QMessageBox.warning(self, self.tr("Warning"),
                    self.tr("Cannot read file %1:\n%2.").arg(fileName).arg(file.errorString()))
            return;
        
        openParameters = OpenParameters(self.ui, file);
        openParameters.read_parameters();
        openParameters.input_parameters();
        
        self.setCurrentFile(fileName)
        self.statusBar().showMessage(self.tr("File loaded"), 2000)

    ##Save file  
    def saveAs(self):
        fileName = QtGui.QFileDialog.getSaveFileName(self, self.tr("Save File"),
                                                     GlobalVariables.DIR_CONF,
                                                     self.tr("Text (*.cfg)"));
        if fileName == '':
#        if fileName.isEmpty():
            return False

        self.saveFile(fileName)
    
    def saveFile(self, fileName):
        file_ = QtCore.QFile(fileName);
        
        if not file_.open( QtCore.QFile.WriteOnly | QtCore.QFile.Text):
            QtGui.QMessageBox.warning(self, self.tr("Warning"),
                    self.tr("Cannot write file_ %1:\n%2.").arg(fileName).arg(file_.errorString()))
            return False
        
        writeParameters = WriteParameters(self.ui, file_);
        writeParameters.read_parameters();
        writeParameters.write_parameters();

        self.setCurrentFile(fileName)
        self.statusBar().showMessage(self.tr("File saved"), 2000)
        return True
        
    def setCurrentFile(self, fileName):
        """
        Current File is temporary opened file name 
        input = str(r"c:\tmp\test.txt");
        """
        self.curFile = fileName
    
    def setProgressDialog(self):
        max = 5;
        progressDialog = ProgressDialog(max, self);
        progressDialog.show();
        
        self.DialogWindows = DialogWindows(self);
        
        qcConnect = self.qcConnect;
        i = 1;
        for result in qcConnect.connect():
            connected, text = result;
            if connected:
                progressDialog.updateProgress(i, text);
                i += 1;
            else:
                self.DialogWindows.criticalWindow(text);
                progressDialog.close();
            if progressDialog.wasCanceled():
                ##This needs to be interaction between ProgeressDialog and the main application 
                log.info("QualityCenter connection stopped");
                progressDialog.close();
        
        progressDialog.close();
        return connected
    
    #-----------------------------------------------------------------------------
    def qc_connect(self):
        """Connect/Disconnect to/from QC database"""
        
        if self.ui.pushButton_ConnectQC.isChecked():            
            Server, UserName, Password, DomainName, ProjectName = GetCurrentParam(self.ui).getParam();
#            print "Server, UserName, Password, DomainName, ProjectName:", Server, UserName, Password, DomainName, ProjectName;
            self.qcConnect = qc_connect_disconnect.QC_connect(Server, 
                                                             UserName, 
                                                             Password, 
                                                             DomainName, 
                                                             ProjectName);
    
            self.QCconnection_status = (None, "Establishing connection") ;
            
            
            if self.setProgressDialog():
                self.ui.pushButton_ConnectQC.setChecked(True);
                self.ui.pushButton_ConnectQC.setText("Disconnect from QC");
                self.ui.pushButton_ConnectQC.repaint();
                self.startup_settings();
            else:
                self.ui.pushButton_ConnectQC.setChecked(False);
                self.ui.pushButton_ConnectQC.setText("Connect to QC")
                self.ui.pushButton_ConnectQC.repaint();
            
#            a_lock = thread.allocate_lock()
#            thread.start_new_thread(self.qcConnect_button_blink_thread, (a_lock,));
#            
#            connection_status = self.qcConnect.connect();
#            print "connection_status:", connection_status
#            a_lock.acquire();
#            self.QCconnection_status = connection_status;
#            a_lock.release();
#            thread.exit_thread();
#            
#            if self.QCconnection_status[0]:
#                self.ui.pushButton_ConnectQC.setChecked(True);
#                self.ui.pushButton_ConnectQC.repaint();
#                print("Connected to QC");
#                self.startup_settings();
#            elif self.QCconnection_status[0] is False:
#                print("Not connected to QC server, exiting");
#                text = self.QCconnection_status[1];
#                self.DialogWindows.criticalWindow(text);
#                self.ui.pushButton_ConnectQC.setChecked(False);
#                self.ui.pushButton_ConnectQC.repaint();
        else:
            log.info("Disconnecting");    
            if self.qcConnect.disconnect_QC():
                self.ui.pushButton_ConnectQC.setChecked(False);
                self.ui.pushButton_ConnectQC.setText("Connect to QC")
                self.ui.pushButton_ConnectQC.repaint();
                log.info("Disconnected");
            else:
                self.ui.pushButton_ConnectQC.setChecked(True);
                self.ui.pushButton_ConnectQC.setText("Disconnect from QC");
                self.ui.pushButton_ConnectQC.repaint();
                log.warning("Unable to disconnect");
    
        
        
    def startup_settings(self):

        ##Set QCgraph tabWidget
#        self.setQCgraph.qc = self.qcConnect.qc;
        self.setQCgraph.create_map_comboBox();
        
        self.setQCgraph.update_comboBox_items();
        
        
        dataTestPlan = self.setQCgraph.qCgraphSpecificTestTestPlan.QcgetDirTestCase(self.qcConnect.qc)
        dataTestLab = self.setQCgraph.qCgraphSpecificTestTestLab.QcgetDirTestSet(self.qcConnect.qc)
        ##TestLab   
#        data = QcgetDirTestSet(self.qcConnect.qc)
        ##TestPlan 
#        data = QcgetDirTestCase(self.qcConnect.qc)
        
        
        self.setQCgraph.qCgraphSpecificTestTestPlan.updateStarterModel(dataTestPlan);
        self.setQCgraph.qCgraphSpecificTestTestLab.updateStarterModel(dataTestLab)

        self.setQCTestX_TestLab.qCTestX.updateStarterModel(dataTestLab)
        self.setQCTestX_TestPlan.qCTestX.updateStarterModel(dataTestPlan)
#    -----------------------------------------------------------------------------
   


        
        
if __name__ == '__main__':
    app = QApplication(sys.argv);
    
    mainForm = MainWindow(parent=app);
#    mainForm.show();
    sys.exit(app.exec_());
