'''
Created on 2010-03-15

@author: Lucas
'''

from PyQt4 import QtCore
from logfile import Logger
import PyQt4.QtGui as QtGui
import QC_graph_ext.QCgraph as QCgraph
import QC_graph_ext.lib.makeFile as QCGraphMakeFile
import QC_graph_ext.lib.makeGraph  as QCGraphMakeGraph
import pywintypes
import time



log = Logger(loggername="lib.runQCgraph", resetefilelog=False).log;

class GraphRunTestLab(object):
    
    def __init__(self, qc=None):
        self.qc = qc;
        
    
    def getAllTestSets(self, dir):
        '''Get all TestSets from given QC directory instance.
        input = dir(dirName, qcDirInstance)
        output = list(tuple(TS_name, qcInstanceTS, TS_ID))'''
        
        dirName, qcDirInstance = dir
        log.debug(("dirName, qcDirInstance:", dirName, qcDirInstance));
        try:
            tsListInstance = qcDirInstance.FindTestSets("");
            if tsListInstance is None:
                tsListInstance = [];
        except pywintypes, err:
            ##print "In Test Lab is not possible to get TestSet directly from Root. This is normal situation"
            ##print err
            tsListInstance = [];
        if len(tsListInstance) > 0:
            tsList = [(unicode(ts.Name),ts, ts.ID) for ts in tsListInstance.__iter__()];
        else:
            tsList = [];
        return tsList;
    
    def getTestSetID(self, nondirs):
        ##Czy powonien byc dodawny TS_ID podczas otwierania programu, czy dopiero teraz odpytawnie o ID ???. Jednka lepiej gdy to drugie. Szybciej program dziala  
        tsList = [];
        for qcTsName, qcTsInstance in nondirs:
            tsList.append((qcTsName, qcTsInstance, qcTsInstance.ID)); 
            
        return tsList;


class GraphRunTestPlan(object):
    
    def __init__(self, qc=None):
        self.qc = qc;
        
    
    def getAllTestCases(self, dir):
        '''Get all TestCases from given QC directory instance.
        input = dir(dirName, qcDirInstance)
        output = list(tuple(TC_name, qcInstanceTC, TC_ID))'''
        
        dirName, qcDirInstance = dir
        try:
            tcListInstance = qcDirInstance.FindTests("");
            if tcListInstance is None:
                tcListInstance = [];
        except pywintypes, err:
            tcListInstance = [];
        if len(tcListInstance) > 0:
            tcList = [(unicode(tc.Name),tc, tc.ID) for tc in tcListInstance.__iter__()];
        else:
            tcList = [];
        return tcList;
    
    def getTestCaseID(self, nondirs):
        ##Czy powonien byc dodawny TS_ID podczas otwierania programu, czy dopiero teraz odpytawnie o ID ???  
        tcList = [];
        for qcTcName, qcTcInstance in nondirs:
            tcList.append((qcTcName, qcTcInstance, qcTcInstance.ID)); 
            
        return tcList;


def criticalWindow(text):
    """Give critical MessageBox for QC connection
    input = str()"""
    QtGui.QMessageBox.critical(None, "Quality Center connection", text);


def get_comboBox_values(setQCgraph):
    
    Xaxis_index = int(setQCgraph.guiQcgraphMain.comboBox_Xaxis.currentIndex());
    Yaxis_index = int(setQCgraph.guiQcgraphMain.comboBox_Yaxis.currentIndex());
    GraphType = str(setQCgraph.guiQcgraphMain.comboBox_GraphType.currentText());
    
    if not setQCgraph.dict_GraphType:
        return None, None, GraphType 
    
#    Axis = setQCgraph.dict_GraphType[GraphType];
#    Xaxis = Axis[0].items()[Xaxis_index];
#    Yaxis = Axis[1].items()[Yaxis_index];
    
    Xaxis = [str(setQCgraph.guiQcgraphMain.comboBox_Xaxis.itemData(Xaxis_index).toString()), ##FieldName     TC_TESTER_NAME
            str(setQCgraph.guiQcgraphMain.comboBox_Xaxis.itemText(Xaxis_index))]; ##userFieldName    Responsible Tester
    Yaxis = [str(setQCgraph.guiQcgraphMain.comboBox_Yaxis.itemData(Yaxis_index).toString()),
             str(setQCgraph.guiQcgraphMain.comboBox_Xaxis.itemText(Xaxis_index))];
    
    return Xaxis, Yaxis, GraphType
    
def makeTestPlanTableData(createDataTable, testCase, Xaxis, Yaxis, ):

    try: rowName = testCase[1].Field(Xaxis[0]); ##this checks if in XAxis value is from TestCase. If yes, then is needed to open TestSet
    except: rowNameStatus = False;
    else: rowNameStatus = True;
    
    try: colName = testCase[1].Field(Yaxis[0]); ##this checks if in YAxis value is from TestCase. If yes, then is needed to open TestSet
    except: colNameStatus = False;
    else: colNameStatus = True;
    
    ##If any of rowName or colName is False, then open TestFactory and go thru all TestCases in TestSEt
    if not rowNameStatus: 
        try: rowName = testCase[1].Field(Xaxis[0]);
        except: rowName = "Unknown";
    if not colNameStatus: 
        try: colName = testCase[1].Field(Yaxis[0]);
        except: colName = "Unknown";
    
#    if not isinstance(rowName, unicode) and not isinstance(colName, unicode):
#        return None;
    log.debug(("Value:", (rowName, colName)));
    createDataTable.addRow(rowName, colName)
    return createDataTable;

def makeTestLabTableData(createDataTable, testSet, Xaxis, Yaxis):

    try: rowName = testSet[1].Field(Xaxis[0]); ##this checks if in XAxis value is from TestCase. If yes, then is needed to open TestSet
    except: rowNameStatus = False;
    else: rowNameStatus = True;
    
    try: colName = testSet[1].Field(Yaxis[0]); ##this checks if in YAxis value is from TestCase. If yes, then is needed to open TestSet
    except: colNameStatus = False;
    else: colNameStatus = True;
    
    if not(rowNameStatus and colNameStatus):
        ##If any of rowName or colName is False, then open TestFactory and go thru all TestCases in TestSEt
        tsf = testSet[1].TSTestFactory;
        if tsf != None:
            tcList = tsf.NewList("");
            for testcase in tcList:
#                print "TCname:", (testcase.Name, testcase.Field(Yaxis[0]));
                if not rowNameStatus: 
                    try: rowName = testcase.Field(Xaxis[0]);
                    except: rowName = "Unknown";
                
                if not colNameStatus: 
                    try: colName = testcase.Field(Yaxis[0]);
                    except: colName = "Unknown";

#                if not (isinstance(rowName, unicode) and isinstance(colName, unicode)):
#                    return None;
                log.debug(("Value:", (rowName, colName)));
                createDataTable.addRow(rowName, colName)
    else:
        ##Both values rowName and colNames are values which can be taken directly from TestSet
        if not (isinstance(rowName, unicode) and isinstance(colName, unicode)):
            return None;
        log.debug(("Value:", (rowName, colName)));
        createDataTable.addRow(rowName, colName)
        
    return createDataTable;


def countItems(setQCgraph):
    maxItemsNumber = 0;
    currentTabName = setQCgraph.currentTabName;
    if currentTabName == "Specific test - Test Lab":
        model = setQCgraph.qCgraphSpecificTestChooseTestSet.treeView.model(); ##get items from ChooseTestLab model
        graphRunTestLab = GraphRunTestLab();   ##create object of TestLab
        for dir, nondir in model.walk():
            if (dir == (None,None)) and (nondir == []):
                log.warning(("WRN: User did not selected any TestSet/Directory to be run in the Chosen TestSets"));
                continue;
            if not nondir:
                allTestSets = graphRunTestLab.getAllTestSets(dir);
                maxItemsNumber += len(allTestSets);
            else:
                testSetID= graphRunTestLab.getTestSetID(nondir);
                maxItemsNumber += len(testSetID);
    elif currentTabName == "Specific test - Test Plan":
        model = setQCgraph.qCgraphSpecificTestChooseTestCase.treeView.model(); ##get items from ChooseTestLab model
        graphRunTestPlan = GraphRunTestPlan();   ##create object of TestLab
        for dir, nondir in model.walk():
            if (dir == (None,None)) and (nondir == []):
                log.warning(("WRN: User did not selected any TestCase/Directory to be run in the Chosen TestCase`s"));
                continue;
            if not nondir:
                allTestCases = graphRunTestPlan.getAllTestCases(dir);
                maxItemsNumber += len(allTestCases);
            else:
                testCaseID= graphRunTestPlan.getTestCaseID(nondir);
                maxItemsNumber += len(testCaseID);
    elif currentTabName == "Cross test":
        log.debug("Started Cross test statistic");
        maxItemsNumber = -1;
    
    
    return maxItemsNumber;

def run(setQCgraph):
    
    currentTabName = setQCgraph.currentTabName;
    
    ##Read what type of Tab is currently open CrossTest/ SpecificTestTestLab/ SpecificTestTestPlan

    
    
    if currentTabName == "Specific test - Test Lab":
        model = setQCgraph.qCgraphSpecificTestChooseTestSet.treeView.model(); ##get items from ChooseTestLab model
        graphRunTestLab = GraphRunTestLab();   ##create object of TestLab
    
        Xaxis, Yaxis, GraphType = get_comboBox_values(setQCgraph);
        log.debug((Xaxis, Yaxis, GraphType)); 
        
        nameField = ("TESTCYCL", "CYCLE"); ##Name of the Fields used in the TestPlan
         
        fieldValues = QCGraphMakeGraph.getFieldValues(setQCgraph.qc,\
                                                       fieldSystemName=Yaxis[0],\
                                                       fieldGroupList=nameField); ##Yaxis[0]
        if fieldValues is False:
            text = "Unable to create statistic from Xaxis and/or Yaxis"
            QtGui.QMessageBox.information(setQCgraph.mainWindow, "Quality Center statistic", text);
            log.warning(("Wrong Xaxis or Yaxis, finish"));
            setQCgraph.uiProgressDialog.cancel();
            return False;
        
        ##fieldValues is a list of column names given directly from QC Field. If it is not possible to get them, then value is []                                      
        log.debug(("fieldValues:", fieldValues)); 
        
        createDataTable = QCGraphMakeFile.CreateDataTable(fieldValues);
        ##createDataTable makes object of future data taken from the QC based on Xaxis, Yaxis 
        allTestSets, testSetID = [], [];
        
        for dir, nondir in model.walk():
            if (dir == (None,None)) and (nondir == []):
                log.warning(("WRN: User did not selected any TestSet/Directory to be run in the Chosen TestSets"))
                continue;
            log.debug(("dir, nondir:",(dir, nondir)));
            
            if not nondir:
                allTestSets = graphRunTestLab.getAllTestSets(dir);
                log.debug(("AllTestSets:", allTestSets));
                for testSet in allTestSets.__iter__():
                    createDataTableStatus = makeTestLabTableData(createDataTable, testSet, Xaxis, Yaxis);
                    if createDataTableStatus is None:
                        text = "Unable to create statistic from Xaxis and/or Yaxis"
                        QtGui.QMessageBox.information(setQCgraph.mainWindow, "Quality Center statistic", text);
                        log.warning(("Wrong Xaxis or Yaxis, finish"));
                        break;
                    ##time.sleep(1)
                    log.info(("*"*40 + "NEW TEST" + "*"*40));
                    setQCgraph.uiProgressDialog.updateProgress(value=1);
                    if setQCgraph.uiProgressDialog.wasCanceled():
                        ##This needs to be interaction between ProgeressDialog and the main application 
                        log.debug(("Progress stop"));
                        break;
            else:
                testSetID= graphRunTestLab.getTestSetID(nondir);
                log.debug(("TestSetID:",  testSetID));
                for testSet in testSetID.__iter__():
                    createDataTableStatus = makeTestLabTableData(createDataTable, testSet, Xaxis, Yaxis);
                    if createDataTableStatus is None:
                        text = "Unable to create statistic from Xaxis and/or Yaxis"
                        QtGui.QMessageBox.information(setQCgraph.mainWindow, "Quality Center statistic", text);
                        log.warning(("Wrong Xaxis or Yaxis, finish"));
                        break;
                    
                    ##time.sleep(1)
                    log.info(("*"*40 + "NEW TEST ID" + "*"*40));
                    setQCgraph.uiProgressDialog.updateProgress(value=1);
                    if setQCgraph.uiProgressDialog.wasCanceled():
                        ##This needs to be interaction between ProgeressDialog and the main application 
                        log.debug(("Progress stop"));
                        break;
            log.debug(createDataTable.data);
        if fieldValues == []:
            ##If values from direct QC Field was empty, then create filedValues based on the generated data table   
            fieldValues = createDataTable.colNamesAsList;
        else:
            ##in case when filedValues was not empty, but column names given from QC Filed is different from column names from created data table. Then add those missing columns   
            if fieldValues.__ne__(createDataTable.colNamesAsList):
                dif = list(set(createDataTable.colNamesAsList).difference(set(fieldValues)))
                fieldValues += dif;
                
        makeGraphFile = QCGraphMakeFile.MakeGraphFile(GraphType, Xaxis, Yaxis, fieldValues);##create csv file based on the column names "filedValues"
        makeGraphFile.makeDictRow(createDataTable.data); ##Save data into the file
        del allTestSets, testSetID
        return makeGraphFile.filename;
    elif currentTabName == "Cross test":
        log.debug("Started Cross test statistic");
        return;
    elif currentTabName == "Specific test - Test Plan":
        model = setQCgraph.qCgraphSpecificTestChooseTestCase.treeView.model(); ##get items from ChooseTestLab model
        graphRunTestPlan = GraphRunTestPlan(); ##create object of TestPlan
        
        Xaxis, Yaxis, GraphType = get_comboBox_values(setQCgraph);
        log.debug((Xaxis, Yaxis, GraphType)); 
        
        nameField = ("TEST"); ##Name of the Fields used in the TestPlan
        fieldValues = QCGraphMakeGraph.getFieldValues(setQCgraph.qc,\
                                                      fieldSystemName=Yaxis[0],\
                                                      fieldGroupList=nameField);
                                                      
        if fieldValues is False:
            text = "Unable to create statistic from Xaxis and/or Yaxis"
            QtGui.QMessageBox.information(setQCgraph.mainWindow, "Quality Center statistic", text);
            log.warning(("Wrong Xaxis or Yaxis, finish"));
            setQCgraph.uiProgressDialog.cancel();
            return False;
        createDataTable = QCGraphMakeFile.CreateDataTable(fieldValues);
        allTestCases, testCaseID = [], [];
        for dir, nondir in model.walk(): ##get TestCases from ChooseTestLab window
            if (dir == (None,None)) and (nondir == []):
                log.warning(("WRN: User did not selected any TestCase/Directory to be run in the Chosen TestCases`s"))
                continue;
            log.debug(("dir, nondir:",(dir, nondir)));
            if not nondir:
                allTestCases = graphRunTestPlan.getAllTestCases(dir); ##read TestCase from directory, source QualityCenter
              #=================================================================
              # Tutaj na allTestCases przy duzej liczbie sie przycina. 
              #=================================================================
                
                log.debug(("AllTestCases:", allTestCases));
                for testCase in allTestCases.__iter__():
                    createDataTableStatus = makeTestPlanTableData(createDataTable, testCase, Xaxis, Yaxis, )
                    if createDataTableStatus is None:
                        log.warning(("Wrong Xaxis or Yaxis, finish"));
                        break;
                    log.info(("*"*40 + "NEW TEST" + "*"*40));
                    setQCgraph.uiProgressDialog.updateProgress(value=1);
                    if setQCgraph.uiProgressDialog.wasCanceled():
                        ##This needs to be interaction between ProgeressDialog and the main application 
                        log.debug(("Progress stop"));
                        break;
            else:
                testCaseID= graphRunTestPlan.getTestCaseID(nondir);  ##read TestCases from directory, source ChooseTestLab 
                log.debug(("TestCaseID:",  testCaseID));
                for testCase in testCaseID.__iter__():
                    createDataTableStatus = makeTestPlanTableData(createDataTable, testCase, Xaxis, Yaxis, )
                    if createDataTableStatus is None:
                        log.warning(("Wrong Xaxis or Yaxis, finish"));
                        break;
                    
                    log.info(("*"*40 + "NEW TEST ID" + "*"*40));
                    setQCgraph.uiProgressDialog.updateProgress(value=1);
                    if setQCgraph.uiProgressDialog.wasCanceled():
                        ##This needs to be interaction between ProgeressDialog and the main application 
                        log.debug(("Progress stop"));
                        break;
            log.debug((createDataTable.data));
        if fieldValues == []:
            ##If values from direct QC Field was empty, then create filedValues based on the generated data table   
            fieldValues = createDataTable.colNamesAsList;
        else:
            ##in case when filedValues was not empty, but column names given from QC Filed is different from column names from created data table. Then add those missing columns   
            if fieldValues.__ne__(createDataTable.colNamesAsList):
                dif = list(set(createDataTable.colNamesAsList).difference(set(fieldValues)))
                fieldValues += dif;
                
            ##put DATA into the CSV file
        makeGraphFile = QCGraphMakeFile.MakeGraphFile(GraphType, Xaxis, Yaxis, fieldValues);##create csv file based on the column names "filedValues"
        makeGraphFile.makeDictRow(createDataTable.data); ##Save data into the file
        del allTestCases, testCaseID
        return makeGraphFile.filename;
