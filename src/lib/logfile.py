'''
Created on 20-06-2011

@author: wro50026
'''
import sys
import os
import logging



def whoami():
    '''
    This will print current function name
    
    # also, by calling sys._getframe(1), you can get this information
    # for the *caller* of the current function.  So you can package
    # this functionality up into your own handy functions:
    
    '''
    return sys._getframe(1).f_code.co_name

def callersname():
    '''
    # this uses argument 1, because the call to whoami is now frame 0.
    # and similarly:
    '''
    return sys._getframe(2).f_code.co_name


class Logger(object):
    def __init__(self, loggername=None, logfilename='logfile.log', resetefilelog=False):
        if resetefilelog:
            logfilename = os.path.join(os.getcwd(), logfilename);
            if os.path.exists(logfilename):
                try:
                    os.remove(logfilename);
                except WindowsError, err:
                    print ("unable remove previous log file:", err);

        if loggername is None: loggername=callersname();
        self.log = logging.getLogger(loggername)
        #formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
        
       
        
        if logfilename is not None:
            logfilename = os.path.join(os.getcwd(), logfilename)
            logging.basicConfig(filename=logfilename, level=logging.DEBUG, 
                                format="%(asctime)s - %(name)s - %(levelname)s - %(message)s")
        #logger1 = logging.getLogger('module1')
        
        streamhandler = logging.StreamHandler()
        streamhandler.setLevel(logging.NOTSET)
        formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
        streamhandler.setFormatter(formatter)
        self.log.addHandler(streamhandler)
        
    #------------------------------------------------------------------------------ 
    def __get_logger(self):
        return self.__logger;
    def __set_logger(self, logger):
        self.__logger = logger;
    def __del_logger(self):
        print "zamykanie Loga";
        self.__logger.shutdown() 
    log = property(__get_logger, __set_logger, __del_logger, "This is logger instance");
    #------------------------------------------------------------------------------ 


    
if __name__ == "__main__":
#    from logfile import Logger
    log = Logger(loggername="old_lib.test", resetefilelog=True).log;
#    print logger.__logger
   
    log.info("cos");