# -*- coding: utf-8 -*

import os
import re

from PyQt4 import QtCore

from crypt_pass import Heslo


class OpenParameters(QtCore.QObject):
    
    def __init__(self, mainWindowUi, file):
        QtCore.QObject.__init__(self);
        
        self.fo_filename = file;
        self.ui = mainWindowUi;     
        
        self.__Server = "";
        self.__UserName = "";
        self.__Password = "";
        self.__DomainName = "";
        self.__ProjectName = "";   
                    
    #----------------------------------------------------------------------------- 
    def __get_foFilename(self):
        """Return file instance"""
        return self.fo_filename;
    def __del_foFilename(self):
        self.fo_filename.close();
    fo_file = property(__get_foFilename, None, __del_foFilename, "Property file instance");
    #-----------------------------------------------------------------------------
     
    
    def read_parameters(self):
        
        while not self.fo_file.atEnd():
            line = self.fo_file.readLine();
            line = unicode( line, "utf-8" ).rstrip();
            #print line;
            if re.compile('Server[\s]*:').match(line):
                self.__Server = line.split(':',1)[1].strip();
                continue;
            elif re.compile('UserName[\s]*:').match(line):
                self.__UserName = line.split(':',1)[1].strip();
                continue;
            elif re.compile('Password[\s]*:').match(line):
                Password = line.split(':',1)[1].strip();
                self.__Password = Heslo(Password)._Heslo__readHeslo();
                continue;
            elif re.compile('DomainName[\s]*:').match(line):
                self.__DomainName = line.split(':',1)[1].strip();
                continue;
            elif re.compile('ProjectName[\s]*:').match(line):
                self.__ProjectName = line.split(':',1)[1].strip();
                continue;
        
    def input_parameters(self):
        """input data parameters to QC_gui"""
        self.ui.lineEdit_QCServer.setText(self.tr(self.__Server));
        self.ui.lineEdit_UserName.setText(self.tr(self.__UserName));
        self.ui.lineEdit_Password.setText(self.tr(self.__Password));
        self.ui.lineEdit_DomainName.setText(self.tr(self.__DomainName));
        self.ui.lineEdit_ProjectName.setText(self.tr(self.__ProjectName))



class WriteParameters(object):
    
    def __init__(self, mainWindowUi, file):
        self.fw_filename = file;
        self.ui = mainWindowUi;     
        
        self.__Server = "";
        self.__UserName = "";
        self.__Password = "";
        self.__DomainName = "";
        self.__ProjectName = "";   
                    
    #----------------------------------------------------------------------------- 
    def __get_fwFilename(self):
        """Return file instance"""
        return self.fw_filename;
    def __del_fwFilename(self):
        self.fw_filename.close();
    fw_file = property(__get_fwFilename, None, __del_fwFilename, "Property file instance");
    #-----------------------------------------------------------------------------
    
    def make_row(self, rowName, value):
        """Make correct format for row line in parameterFile
        e.g. UserName: Testr\n"""
        rowName = QtCore.QByteArray(rowName)
        row = rowName.append(value);
        row = row.append("\n");
        return row;
    
    def read_parameters(self):
        """Read parameters from QC_gui """
        self.__Server = self.make_row("Server: ", self.ui.lineEdit_QCServer.text())
        self.__UserName = self.make_row("UserName: ", self.ui.lineEdit_UserName.text());
        
        __Pass = str(self.ui.lineEdit_Password.text());
        if __Pass != "":
            __Pass = QtCore.QByteArray(Heslo(__Pass)._Heslo__makeHeslo());
        self.__Password = self.make_row("Password: ", __Pass);
        self.__DomainName = self.make_row("DomainName: ", self.ui.lineEdit_DomainName.text());
        self.__ProjectName = self.make_row("ProjectName: ", self.ui.lineEdit_ProjectName.text());
        
    def write_parameters(self):
        """Write data parameters from QC_gui to file"""
        self.fw_file.write(self.__Server);
        self.fw_file.write(self.__UserName);
        self.fw_file.write(self.__Password);
        self.fw_file.write(self.__DomainName);
        self.fw_file.write(self.__ProjectName);
        
        
class PreviousParamFile(object):
    
    def __init__(self, mainWindowUi):
        self.ui = mainWindowUi;
        
    def get_fileList(self):
        if not os.path.exists(r".\conf"):
            return False;
        filelist = filter(lambda file: file.endswith(".cfg"), os.listdir(r".\conf"));
        if not filelist:
            return False;
        #print "filelist:", filelist;
        return filelist;
     
    def lastParamFile(self, filelist):
        
        latestFile = (0, None);
        for file in filelist:
            lastModification = os.path.getmtime(r".\conf\%s"%file);
            if lastModification > latestFile[0]:
                latestFile = (lastModification, r".\conf\%s"%file);
        #print "latestFile:", latestFile;
        return latestFile[1];
    
    def get_lastParamFile(self):
        
        filelist = self.get_fileList();
        if filelist:
            latestFile = self.lastParamFile(filelist);
        else:
            return False;
        return latestFile; 
        
        
class GetCurrentParam(object):
    
    def __init__(self, mainWindowUi):
        self.__ui = mainWindowUi;
        
    #-----------------------------------------------------------------------------        
    def __get_MainWindowUI(self):
        return self.__ui;
    ui = property(__get_MainWindowUI, None, None, "Instance of UI MainWindow")
    #-----------------------------------------------------------------------------
    
    def getParam(self):
        
        Server = str(self.ui.lineEdit_QCServer.text());
        UserName = str(self.ui.lineEdit_UserName.text());
        Password = str(self.ui.lineEdit_Password.text());
        DomainName = str(self.ui.lineEdit_DomainName.text());
        ProjectName = str(self.ui.lineEdit_ProjectName.text());
        
        return Server, UserName, Password, DomainName, ProjectName; 