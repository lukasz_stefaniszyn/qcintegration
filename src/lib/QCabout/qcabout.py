'''
Created on 22-12-2012

@author: lucst
'''
from PyQt4 import QtCore, QtGui
from lib.QCabout.guiLib.ui_QCinter_About import Ui_Form as Ui_QcAbout 

from lib.logfile import Logger, whoami, callersname
log = Logger(loggername="lib.QCabout.qcabout", resetefilelog=False).log;


class GuiQcAboutMain(QtGui.QDialog, Ui_QcAbout):
    def __init__(self, parent):
        super(GuiQcAboutMain, self).__init__(parent)
        self.setupUi(self);




class QCAbout(QtGui.QWidget):
    '''
    Class for QCAbout window
    '''

    def get_versionnumber(self):
        return self.guiQcAbout.label_version.text()
    def set_versionnumber(self, value):
        self.guiQcAbout.label_version.setText(value)
    versionnumber = property(get_versionnumber, set_versionnumber, None, None)

    
    
    def __init__(self, parent=None):
        '''
        @param parent: QtWidget
        '''
        self.parent = parent;
        super(QCAbout, self).__init__(parent);
        self.guiQcAbout= GuiQcAboutMain(parent);
        
        self.guiQcAbout.show();
    
    
    