class Heslo(object):

	def __init__(self, password):
	
		self.__password = password;
		self.__base = "au1h0Cpa$$"

		
	def _hex(self, value):
		#convert int to hex
		return hex(value).rstrip("L").lstrip("0x");
	
	def toHex(self, s):
		#convert string to hex
		return s.encode("hex")

	
	def toStr(self, s):
		#convert hex repr to string
		return s.decode("hex")
		
	def __makeHeslo(self):
	
		orginalHex = self.toHex(self.__password);
		baseHex = self.toHex(self.__base);
		makeHex = self._hex( (int(orginalHex, 16) * int(baseHex, 16)<<3) );
		return makeHex;
		
	def __readHeslo(self):
	
		baseHex = self.toHex(self.__base);
		hesloHex = self.__password.strip();
		if hesloHex == "":
			return ""
		
		readHex = self._hex( (int(hesloHex, 16)>>3) / int(baseHex, 16));
		readStr = self.toStr(readHex);
		return readStr;

		
if __name__ == "__main__":
	passw = "Newpass12!"
	heslo = Heslo(passw);
	cryptPass = heslo._Heslo__makeHeslo();
	print cryptPass;
	
	heslo = Heslo(cryptPass);
	cryptPass = heslo._Heslo__readHeslo();
	print cryptPass;