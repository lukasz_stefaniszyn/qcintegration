from subprocess import Popen, PIPE
import os

## Make ui_QCinter_mainWindow
print "Make ui_QCinter_mainWindow"
make_py = Popen(r"c:\Python26\Lib\site-packages\PyQt4\uic\pyuic.py -o edit_ui_QCinter_mainWindow.py QCinter_mainWindow.ui",shell=True , stdout=PIPE).stdout;
print make_py.read()
make_py.close();


## Replace lines in ui_QCinter_mainWindow
print "Replace lines in ui_QCinter_mainWindow"
fo_QCinter_mainWindow = open("edit_ui_QCinter_mainWindow.py", "rb")
fw_QCinter_mainWindow = open("ui_QCinter_mainWindow.py", "wb")

for line in fo_QCinter_mainWindow.readlines():
#    line = line.replace(r"../../../../../Documents and Settings/Lucas/.designer/backup", ".");
#    line = line.replace(r"C:/../Documents and Settings/Lucas/.designer/backup", ".");
#    line = line.replace(r"Documents and Settings/Lucas/.designer/backup", ".");
#    line = line.replace(r"C:/Documents and Settings/wro50026/.designer/backup/.", ".");
    line = line.replace(r"../icon/", "./icon/");
    
    
    fw_QCinter_mainWindow.write(line);

fw_QCinter_mainWindow.flush();
fw_QCinter_mainWindow.close();
fo_QCinter_mainWindow.close();
    

## Remove edit_ui_QCinter_mainWindow.py   
print "Remove edit_ui_QCinter_mainWindow.py"
os.remove(r".\edit_ui_QCinter_mainWindow.py");

