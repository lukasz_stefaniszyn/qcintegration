'''
Created on 2010-03-15

@author: Lucas
'''
from lib.logfile import Logger
from lib.QCgraph.qCgraphCrossTest import MyTreeViewQCgraphCrossTestResult, \
    GuiQcgraphCrossTest
from lib.QCgraph.qCgraphSpecificTest import MyTreeViewQCgraphSpecificTestResult, \
    MyTreeViewQCgraphSpecificTestTestPlan, \
    MyTreeViewQCgraphSpecificTestChooseTestCase, \
    MyTreeViewQCgraphSpecificTestTestLab, \
    MyTreeViewQCgraphSpecificTestChooseTestSet, GuiQcgraphSpecificTestPlan, \
    GuiQcgraphSpecificTestLab
from lib.QCgraph.ui_QCgraph_main import Ui_Form as Ui_QcT_Main
from PyQt4 import QtCore, QtGui, Qt
from globalVariables import GlobalVariables
from lib.file_viewer import MainWindow
from lib.progressDialog import ProgressDialog
import os

from lib.QCgraph.treeQcModelTestLab import getDirTestSet
from lib.QCgraph.treeQcModelTestPlan import getDirTestCase

import lib.runQCgraph as runQCgraph;

log = Logger(loggername="lib.QCgraph.QCgraph_gui", resetefilelog=False).log;



class FinishMessageBox(QtGui.QMessageBox):
    def __init__(self, file, parent=None, ):
        self.file = file
        self.parent = parent
        super(FinishMessageBox, self).__init__(parent);
        
        self.setText();
        
    def setText(self):
        text = "Finished. User can find result file in Result dialog."
        
        if self.parent is not None:
            file_ = os.path.basename(self.file);
            weekcatalog = os.path.split(os.path.dirname(self.file))[1];
            text += "\n\nUnder: %s \nin file: %s"%(weekcatalog, file_);
            self.information(self.parent, 
                             "QC graph execution status",
                             text);
        else:
            
            text += "\nFile saved here:%s"%(self.file);
            print(text);

class QcGraphCrossTest(GuiQcgraphCrossTest, MyTreeViewQCgraphCrossTestResult):
    
    def __init__(self, tab_CrossTest, dirResult):
        self.guiQcgraphCrossTest = GuiQcgraphCrossTest(tab_CrossTest);
        
#        self.guiQcgraphCrossTest.
        
#        guiQcgraphCrossTest = GuiQcgraphCrossTest(tab_CrossTest)
        self.qCgraphCrossTest = MyTreeViewQCgraphCrossTestResult(self.guiQcgraphCrossTest.treeView_QCgraphCrossTest, dir=dirResult);

class QcGraphSpecificTestTestLab(object):
    
    def __init__(self, tab_SpecTestTestLab, dirResult):
        self.guiQcgraphSpecificTestLab = GuiQcgraphSpecificTestLab(tab_SpecTestTestLab)
        self.qCgraphSpecificTestTestLab = MyTreeViewQCgraphSpecificTestTestLab(self.guiQcgraphSpecificTestLab.treeView_QCGraphSpecificTestTestLab)
        self.qCgraphSpecificTestChooseTestSet = MyTreeViewQCgraphSpecificTestChooseTestSet(self.guiQcgraphSpecificTestLab.treeView_QCGraphSpecificTestTestSets)
        self.qCgraphSpecificTestLabTestResult = MyTreeViewQCgraphSpecificTestResult(self.guiQcgraphSpecificTestLab.treeView_QCgraphSpecificTestResultTestLab, dir=dirResult);
    
        ##Specific Test - TestLab tab
        self.connect(self.guiQcgraphSpecificTestLab.pushButton_QCgraphSpecificTestTestLab_Refresh, QtCore.SIGNAL("clicked()"),
             self.pushButton_Refresh_QCGraph_clicked);
        self.connect(self.guiQcgraphSpecificTestLab.pushButton_QCgraphSpecificTestTestLab_Add, QtCore.SIGNAL("clicked()"),
             self.__pushButton_QCTestXTestLab_Add_clicked);
        self.connect(self.guiQcgraphSpecificTestLab.pushButton_QCgraphSpecificTestTestLab_Remove, QtCore.SIGNAL("clicked()"),
             self.__pushButton_QCTestXTestLab_Remove_clicked);
        self.connect(self.guiQcgraphSpecificTestLab.pushButton_QCgraphSpecificTestTestLab_Clear, QtCore.SIGNAL("clicked()"),
             self.__pushButton_QCTestXTestLab_Clear_clicked);

        ##Specific test - Test Lab tab
    def __pushButton_QCTestXTestLab_Add_clicked(self):
        log.debug("Clicked pushButton_QCgraphSpecificTestTestLab_Add");
        index = self.qCgraphSpecificTestTestLab.treeView.currentIndex()
        item = index.internalPointer();
        if item is not None:
            self.qCgraphSpecificTestChooseTestSet.addItem(item)
        else:
            log.warning("WRN: User did not selected TestSet/Directory to be moved into Chosen TestSets");
            pass;
    def __pushButton_QCTestXTestLab_Remove_clicked(self):
        log.debug("Clicked pushButton_QCgraphSpecificTestTestLab_Remove");
        index = self.qCgraphSpecificTestChooseTestSet.treeView.currentIndex()
        item = index.internalPointer();
        if item is not None:
            self.qCgraphSpecificTestChooseTestSet.removeItem(index)
        else:
            log.warning("WRN: User did not selected TestSet/Directory to be remove from Chosen TestSets");
    def __pushButton_QCTestXTestLab_Clear_clicked(self):
        log.debug("Clicked pushButton_QCgraphSpecificTestTestSet_Clear");
        self.qCgraphSpecificTestChooseTestSet.clearItem();
        
    
class QcGraphSpecificTestTestPlan(QtGui.QWidget):
    
    def __init__(self, tab_SpecTestTestPlan, dirResult):
        
        QtGui.QWidget.__init__(self);
        self.guiQcgraphSpecificTestPlan = GuiQcgraphSpecificTestPlan(tab_SpecTestTestPlan)
        self.qCgraphSpecificTestTestPlan = MyTreeViewQCgraphSpecificTestTestPlan(self.guiQcgraphSpecificTestPlan.treeView_QCGraphSpecificTestTestPlan)
        self.qCgraphSpecificTestChooseTestCase = MyTreeViewQCgraphSpecificTestChooseTestCase(self.guiQcgraphSpecificTestPlan.treeView_QCGraphSpecificTestTestCase)
        self.qCgraphSpecificTestPlanTestResult= MyTreeViewQCgraphSpecificTestResult(self.guiQcgraphSpecificTestPlan.treeView_QCgraphSpecificTestResultTestPlan, dir=dirResult);
        
        ##Specific Test - TestPlan tab
        self.connect(self.guiQcgraphSpecificTestPlan.pushButton_QCgraphSpecificTestTestPlan_Refresh, QtCore.SIGNAL("clicked()"),
             self.pushButton_Refresh_QCGraph_clicked);
        self.connect(self.guiQcgraphSpecificTestPlan.pushButton_QCgraphSpecificTestTestPlan_Add, QtCore.SIGNAL("clicked()"),
             self.pushButton_QCgraphSpecificTestTestPlan_Add_clicked);
        self.connect(self.guiQcgraphSpecificTestPlan.pushButton_QCgraphSpecificTestTestPlan_Remove, QtCore.SIGNAL("clicked()"),
             self.pushButton_QCgraphSpecificTestTestPlan_Remove_clicked);
        self.connect(self.guiQcgraphSpecificTestPlan.pushButton_QCgraphSpecificTestTestPlan_Clear, QtCore.SIGNAL("clicked()"),
             self.pushButton_QCgraphSpecificTestTestPlan_Clear_clicked);
             
        ##Specific test - Test Plan tab
    def pushButton_QCgraphSpecificTestTestPlan_Add_clicked(self):
        log.debug("Clicked pushButton_QCgraphSpecificTest_Add");
        index = self.qCgraphSpecificTestTestPlan.treeView.currentIndex()
        item = index.internalPointer();
        if item is not None:
            self.qCgraphSpecificTestChooseTestCase.addItem(item)
        else:
            log.warning("WRN: User did not selected TestCase/Directory to be moved into Chosen TestSets");
            pass;
    def pushButton_QCgraphSpecificTestTestPlan_Remove_clicked(self):
        log.debug("Clicked pushButton_QCgraphSpecificTest_Remove");
        index = self.qCgraphSpecificTestChooseTestCase.treeView.currentIndex()
        item = index.internalPointer();
        if item is not None:
            self.qCgraphSpecificTestChooseTestCase.removeItem(index)
        else:
            log.warning("WRN: User did not selected TestCase/Directory to be remove from Chosen TestSets");
    def pushButton_QCgraphSpecificTestTestPlan_Clear_clicked(self):
        log.debug("Clicked pushButton_QCgraphSpecificTest_Clear");
        self.qCgraphSpecificTestChooseTestCase.clearItem();
        

class GuiQcgraphMain(Ui_QcT_Main):
    def __init__(self, main_QCgraph):
        Ui_QcT_Main.setupUi(self, main_QCgraph);





class SetQCgraph(QcGraphSpecificTestTestLab, QcGraphSpecificTestTestPlan, QcGraphCrossTest, GuiQcgraphMain, ProgressDialog):
    '''
    Set values for QCgraph tab
    '''


    def __init__(self, MainWindow):
        '''
        In QCgraph tab, fill all widgets with basic data
        '''
        
        self.__MainWindow = MainWindow;
        self.__MainWindowUI = MainWindow.ui;
        dirResult = os.path.join(GlobalVariables.DIR_RESULT, 'QCgraph');
        
        self.guiQcgraphMain = GuiQcgraphMain(self.mainWindowUI.main_QCgraph)
        ##Cross Test - cross test
        QcGraphCrossTest.__init__(self, self.mainWindowUI.tab_CrossTest, dirResult);
        
        ##Specific Test - Test Plan tab
        QcGraphSpecificTestTestPlan.__init__(self, self.mainWindowUI.tab_SpecTestTestPlan, dirResult);
        ##Specific Test - Test Lab tab
        QcGraphSpecificTestTestLab.__init__(self, self.mainWindowUI.tab_SpecTestTestLab, dirResult)
        
        
        self.dict_GraphType = {};
        ##Button Handlers:
          ##QCParameterFile QCconnection handler
        self.connect(self.guiQcgraphMain.comboBox_GraphType, 
                     QtCore.SIGNAL("activated(const QString &)"),
                     self.comboBox_GraphType_activated)
        
          ##QCGraph handlers
        self.connect(self.guiQcgraphMain.phB_Run_QCGraph, 
                     QtCore.SIGNAL("clicked()"),
                     self.phB_Run_QCGraph_clicked);
        self.connect(self.guiQcgraphMain.pushButton_Refresh_QCGraphAxis, 
                     QtCore.SIGNAL("clicked()"),
                     self.pushButton_Refresh_QCGraphAxis_clicked);
    #-----------------------------------------------------------------------------        
    def __get_MainWindowUI(self):
        return self.__MainWindowUI;
    mainWindowUI = property(__get_MainWindowUI, None, None, "Instance of MainWindow GUI")
    
    def __get_MainWindow(self):
        return self.__MainWindow;
    mainWindow = property(__get_MainWindow, None, None, "Instance of MainWindow")
    
    def __get_qcConnect(self):
        return self.mainWindow.qcConnect.qc;
    qc = property(__get_qcConnect, None, None, "Instance of QC connection");

    def __get_QCconnection_status(self):
        return self.mainWindow.QCconnection_status;
    QCconnection_status = property(__get_QCconnection_status, None, None, "Information of Quality Center connection status (True, 'Connected')");
    
    def __get__currentTabName(self):
        index = self.mainWindowUI.tabWidget_QCgraph.currentIndex();
        return self.mainWindowUI.tabWidget_QCgraph.tabText(index);
    currentTabName = property(__get__currentTabName, None, None, "This is text of current open tab name in the QCgraph");

    #----------------------------------------------------------------------------- 
    ##QCGraph handlers
    def comboBox_GraphType_activated(self, text):
        log.debug(("Text comboBox_GraphType:", text));
        self.update_comboBox_items();
    def phB_Run_QCGraph_clicked(self):
        log.debug("Clicked QCGraph Run");
        if self.QCconnection_status[0] is False :
            text = "User is not connected or was logged out from QC server.\nPlease connect to QC server"
            ##criticalWindow(text);
            return;
        
        self.mainWindow.setCursor(QtCore.Qt.WaitCursor)
        
        ##Initialize ProgressDialog
        max_ = runQCgraph.countItems(self);
        log.debug(("Count items:", max_)); 
        
        if max_==0:
            text = "Please select any Tests and/or Directory to be run in the given tab"
            QtGui.QMessageBox.information(self.mainWindow, "Quality Center statistic", text);
            self.mainWindow.setCursor(QtCore.Qt.ArrowCursor)
            return;
        elif max_==-1: 
            log.debug("Max == False");
            text = "Cross test is not supported yet"
            QtGui.QMessageBox.information(self.mainWindow, "Quality Center statistic", text);
            self.mainWindow.setCursor(QtCore.Qt.ArrowCursor)
            return
        
        
        self.uiProgressDialog = ProgressDialog(max_, parent=self.mainWindow);
#        self.uiProgressDialog.show();
        self.uiProgressDialog.updateProgress(value=1);
        
        self.mainWindow.setCursor(QtCore.Qt.ArrowCursor)
        
        filename = runQCgraph.run(self); #generate statistics
        if filename: 
            self.qCgraphCrossTest.updateModel();##update CrossTest dir view of new created file/catalog
            self.qCgraphSpecificTestPlanTestResult.updateModel();##update SpecificTest TestPlan dir view of new created file/catalog
            self.qCgraphSpecificTestLabTestResult.updateModel(); ##update SpecificTest TestPlan Result dir view of new created file/catalog
            
            
            ##Show dialog MessageBox when QC will be finished
            FinishMessageBox(file=filename, parent=self.mainWindow);
        
        
        
    def pushButton_Refresh_QCGraphAxis_clicked(self):
        log.debug("Clicked QCGraphAxisXY Refresh");
        self.qCgraphCrossTest.updateModel();
    def pushButton_Refresh_QCGraph_clicked(self):
        log.debug("Clicked QCGraph Refresh");
        self.qCgraphSpecificTestLabTestResult.refresh();

        ##Refresh treeView of TestLab/TestPlan catalogs list
        currentTabName = self.currentTabName;
        if currentTabName == "Specific test - Test Lab":
            dataTestLab = getDirTestSet(self.qc)
            self.qCgraphSpecificTestTestLab.updateStarterModel(dataTestLab)
        elif currentTabName == "Specific test - Test Plan":
            dataTestPlan = getDirTestCase(self.qc)
            self.qCgraphSpecificTestTestPlan.updateStarterModel(dataTestPlan)
        elif currentTabName == "Cross test":
            pass;
        
    
        
        

    
    def set_comboBox_Xaxis(self, AxisDict):
        comboBox_Xaxis = self.guiQcgraphMain.comboBox_Xaxis;
        comboBox_Xaxis.clear();
        for fieldName, fieldUserLabel in AxisDict.items():
            comboBox_Xaxis.addItem(fieldUserLabel, userData=fieldName);
        comboBox_Xaxis.model().sort(0) ##Qt.AscendingOrder
        
    def set_comboBox_Yaxis(self, AxisDict):
        comboBox_Yaxis = self.guiQcgraphMain.comboBox_Yaxis;
        comboBox_Yaxis.clear();
        for fieldName, fieldUserLabel in AxisDict.items():
            comboBox_Yaxis.addItem(fieldUserLabel, userData=fieldName);
        comboBox_Yaxis.model().sort(0) ##Qt.AscendingOrder
        
    
    def update_comboBox_items(self):
        graphtype = str(self.guiQcgraphMain.comboBox_GraphType.currentText());
        log.debug(("GraphType:", graphtype));
        if not self.dict_GraphType:
            self.guiQcgraphMain.comboBox_Xaxis.clear();
            self.guiQcgraphMain.comboBox_Yaxis.clear();
            self.set_comboBox_Xaxis(['None']);
            self.set_comboBox_Yaxis(['None']);
            return False;
        
        if "Summary" in graphtype:
            Xaxis = self.dict_GraphType.get("Summary")[0];
            log.debug(("Xaxis:", Xaxis)); 
            self.set_comboBox_Xaxis(Xaxis);
            Yaxis = self.dict_GraphType.get("Summary")[1];
            self.set_comboBox_Yaxis(Yaxis);
        elif "Progress" in graphtype:
            Xaxis = self.dict_GraphType.get("Progress")[0];
            self.set_comboBox_Xaxis(Xaxis);
            Yaxis = self.dict_GraphType.get("Progress")[1];
            self.set_comboBox_Yaxis(Yaxis);
        else:
            log.info("Unknown GraphType");
            return False
            
    
    def create_map_comboBox(self):
        """Creates all possible items used in Axis tabWidget
        output=  {"Summary": (dict{Xaxis}, dict(Yaxi)), "Progress":(dict{Xaxis}, dict(Yaxi))}
        """
        axisFields = self.get_AxisFields();
        for GraphType in ["Summary", "Progress"]:
            if "Summary" is GraphType:
                Xaxis, Yaxis = axisFields, axisFields;
            elif "Progress" is GraphType:
                Yaxis={};
                Yaxis.update({"CY_STATUS":"Cycle_Status"});
                Yaxis.update({"CY_ASSIGN_RCYC ":"Cycle_Target Cycle"});
                Xaxis=axisFields;
            else:
                log.info("Unknown GraphType");
                return False;
            self.dict_GraphType[GraphType]= (Xaxis, Yaxis);
        log.debug(("self.dict_GraphType:", self.dict_GraphType));
        

    def get_AxisFields(self):
        """Get Fields names used for generating Graph
        Make mapping of DataBaseName to UserName field
        {'TC_CYCLE':'Test Set'} 
        """
        mapping_QCGraph = {};
        log.debug(("self.qc.Fields(name)=", self.qc));
        for name in ["TESTCYCL", "CYCLE"]:
            fieldList = self.qc.Fields(name);
            for aField in fieldList:
                FieldProp = aField.Property;
                fieldName = aField.Name;
                fieldUserLabel=FieldProp.UserLabel;
                if "TS_" in fieldName:
                    fieldUserLabel = "TS_"+fieldUserLabel;
                elif "TC_" in fieldName:
                    fieldUserLabel = "TC_"+fieldUserLabel;
                elif "CY_" in fieldName:
                    fieldUserLabel = "Cycle_"+fieldUserLabel;
                fieldIsActive=FieldProp.IsActive;
                fieldIsCanFilter=FieldProp.IsCanFilter;
                if fieldIsActive and fieldIsCanFilter: 
                    mapping_QCGraph[str(fieldName)]= str(fieldUserLabel);
        return mapping_QCGraph;