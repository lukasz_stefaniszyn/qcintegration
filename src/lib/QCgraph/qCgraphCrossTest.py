from PyQt4 import QtCore
import os


from treeResultModel import TreeView as ResultTreeView
from treeResultModel import MyDirModel as ResultMyDirModel

from ui_QCgraph_tabCrossTest import Ui_Form as Ui_CrossTest
from globalVariables import GlobalVariables
 

class GuiQcgraphCrossTest(Ui_CrossTest):
    '''This class is used to initialize, control
     TreeViews and buttons which are on the whole tab
    '''
    def __init__(self, parent):
        ##Add Widget responsible for tab_CrossTest
        ##This initialize QcGraph tab Cross Test  
        Ui_CrossTest.setupUi(self, parent);


class MyTreeViewQCgraphCrossTestResult(ResultTreeView, ResultMyDirModel):
    '''
    Operation in the QCgraph -> Cross test -> Result directories
    Used only for Directory view, and open result files in those directories. Based on  self.dir   
    '''
    
    treeView = None;
    
    
    def __init__(self, parent, dir=None):
        ##Add Widget responsible for tab_CrossTest
        ##This initialize QcGraph tab Cross Test  
        ResultTreeView.__init__(self, parent);
#        self.QString('')
#        self.dir = QtCore.QString(r"./Result/QCgraph");
        if dir is None:
            self.dir = os.path.join(GlobalVariables.DIR_RESULT, r'QCgraph');
        else:
            self.dir = dir;
            
        if not os.path.exists(self.dir):
            try: os.makedirs(self.dir);
            except: pass;
        self.updateModel();


    def updateModel(self):
        self.dirModel = ResultMyDirModel();
        self.treeView.setModel(self.dirModel) # we have to set the listView to use this DirModel
        
        self.treeView.setRootIndex(self.dirModel.index(self.dir)) # set the default to load
     
    
