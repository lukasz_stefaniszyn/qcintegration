'''
Created on 27-09-2010

@author: wro50026
'''
from PyQt4 import QtCore
import os

from treeQcModelTestPlan import TreeView as QcTreeViewTestPlan
from treeQcModelTestPlan import TreeModel as QcTreeModelTestPlan
from treeQcModelTestPlan import getDirTestCase as QcgetDirTestCase

from treeQcModelTestLab import TreeView as QcTreeViewTestLab
from treeQcModelTestLab import TreeModel as QcTreeModelTestLab
from treeQcModelTestLab import getDirTestSet as QcgetDirTestSet

from treeResultModel import TreeView as ResultTreeView
from treeResultModel import MyDirModel as ResultMyDirModel

from treeChooseModelTestPlan import TreeView as ChooseTreeViewTestPlan
from treeChooseModelTestPlan import TreeModel as ChooseTreeModelTestPlan
from treeChooseModelTestPlan import TreeItem as ChooseTreeItemTestPlan
from treeChooseModelTestPlan import getDirTestCase as ChoosegetDirTestCase

from treeChooseModelTestLab import TreeView as ChooseTreeViewTestLab
from treeChooseModelTestLab import TreeModel as ChooseTreeModelTestLab
from treeChooseModelTestLab import TreeItem as ChooseTreeItemTestLab
from treeChooseModelTestLab import getDirTestSet as ChoosegetDirTestSet


from ui_QCgraph_tabSpecificTest_TestPlan import Ui_Form as Ui_SpecificTestTestPlan
from ui_QCgraph_tabSpecificTest_TestLab import Ui_Form as Ui_SpecificTestTestLab
from globalVariables import GlobalVariables
 

class GuiQcgraphSpecificTestPlan(Ui_SpecificTestTestPlan):
    '''This class is used to initialize, control
     TreeViews and buttons which are on the whole tab
    '''
    def __init__(self, parent):
        ##Add Widget responsible for tab_CrossTest
        ##This initialize QcGraph tab Cross Test  
        Ui_SpecificTestTestPlan.setupUi(self, parent);

class GuiQcgraphSpecificTestLab(Ui_SpecificTestTestLab):
    '''This class is used to initialize, control
     TreeViews and buttons which are on the whole tab
    '''
    def __init__(self, parent):
        ##Add Widget responsible for tab_CrossTest
        ##This initialize QcGraph tab Cross Test  
        Ui_SpecificTestTestLab.setupUi(self, parent);

class MyTreeViewQCgraphSpecificTestTestPlan(QcTreeViewTestPlan, QcTreeModelTestPlan):

    def __init__(self, parent):
        QcTreeViewTestPlan.__init__(self, parent);
        
        self.QcgetDirTestCase =  QcgetDirTestCase;
        ##Set empty model
        data = ([], []);
        model = QcTreeModelTestPlan(data)
        self.treeView.setModel(model);
    
    def updateModel(self, qc):
        
        data = QcgetDirTestCase(qc)
        model = QcTreeModelTestPlan(data);
        self.treeView.setModel(model);
    def updateStarterModel(self, data):
        '''
        This will be used only once, when main program is starting
        @param data: dirList, tcList; data = ([], []);
        '''
        model = QcTreeModelTestPlan(data);
        self.treeView.setModel(model);

class MyTreeViewQCgraphSpecificTestChooseTestCase(ChooseTreeViewTestPlan, ChooseTreeModelTestPlan, ChooseTreeItemTestPlan):

    def __init__(self, parent):
        ChooseTreeViewTestPlan.__init__(self, parent);
        
        ##Set empty model
        data = ([], []);
        model = ChooseTreeModelTestPlan(data)
        self.treeView.setModel(model);
    
    def addItem(self, itemTestPlan):
        '''Add icon. Add one selected item from Specific TestCase into the Chose Test Case
        input = TreeItem(item)
        output = None'''
        model = self.treeView.model();
        
        ##Add new Item under the root tree
        
        ##create item, but do not put this item into model
        data = itemTestPlan.itemData;
        isDirectory = itemTestPlan.iconDirectoryItem; 
        parent = model.rootItem
        ##create TreeItem of the given data
        item = ChooseTreeItemTestPlan(data, parent, isDirectory);
        childCounting = model.rootItem.childCount();
        ##start adding item into Model, under correct Parents Row
        model.beginInsertRows(self.treeView.rootIndex(), childCounting, childCounting);#        
        ##add item to the Data tree
        parent.appendChild(item);
        ##finish adding item into Model
        model.endInsertRows();
    
    def removeItem(self, index):
        '''Remove icon. Remove one selected item from the Chose Test Case
        input = QModelIndex(index)
        output = None'''
        model = self.treeView.model();
        ##get parent as a QModelIndex, of this market index
        parentIndex = index.parent();
        ##check if given parent is root and then get TreeItem of this parent
        if not parentIndex.isValid():
            parentItem = model.rootItem;
        else:
            parentItem = parentIndex.internalPointer();
        ##get row position of the Item/Index which will be removed    
        row = index.row();
        ##start removing item/index from the Parents Row
        model.beginRemoveRows(parentIndex, row, row);
        ##remove item/index from the Data tree
        parentItem.removeChild(row);
        ##finish removing item/index from Model
        model.endRemoveRows();

    def clearItem(self):
        '''Trash icon. Clear all items in the Chose Test Case
        input = None
        output = None;'''
        model = self.treeView.model();
        ##get parent as a QModelIndex, of this market index
        parentIndex = QtCore.QModelIndex();
        parentItem = model.rootItem;
        
        countChilds = parentItem.childCount();
        ##start removing items from the root Parent
        model.beginRemoveRows(parentIndex, 0, countChilds);
        ##remove all items from the Data tree
        parentItem.removeAllChilds();
        ##finish removing items from the Model
        model.endRemoveRows();
        
    def updateModel(self, qc):
        
        data = ChoosegetDirTestCase(qc)
        model = ChooseTreeModelTestPlan(data);
        self.treeView.setModel(model);
        
    def getAllItems(self):
        
        model = self.treeView.model();
        
        parentItem = model.rootItem;
        parentItem.rowCount();
        
#        childItem = parentItem.child(row)
        
        

class MyTreeViewQCgraphSpecificTestTestLab(QcTreeViewTestLab, QcTreeModelTestLab):

    def __init__(self, parent):
        QcTreeViewTestLab.__init__(self, parent);
        self.QcgetDirTestSet = QcgetDirTestSet;
        ##Set empty model
        data = ([], []);
        model = QcTreeModelTestLab(data)
        self.treeView.setModel(model);
#        resizeAndShow(self, parent)
    
    def updateModel(self, qc):
        
        data = QcgetDirTestSet(qc)
        model = QcTreeModelTestLab(data);
        self.treeView.setModel(model);
    def updateStarterModel(self, data):
        '''
        This will be used only once, when main program is starting
        @param data: dirList, tcList.  data = ([], []);;
        '''
        model = QcTreeModelTestLab(data)
        self.treeView.setModel(model);

class MyTreeViewQCgraphSpecificTestChooseTestSet(ChooseTreeViewTestLab, ChooseTreeModelTestLab, ChooseTreeItemTestLab):

    def __init__(self, parent):
        ChooseTreeViewTestLab.__init__(self, parent);
        
        ##Set empty model
        data = ([], []);
        model = ChooseTreeModelTestLab(data)
        self.treeView.setModel(model);
#        self.show();
    
    def addItem(self, itemTestLab):
        '''Add icon. Add one selected item from Specific TestSet into the Choose Test Set
        input = TreeItem(item)
        output = None'''
        model = self.treeView.model();
        
        ##Add new Item under the root tree
        
        ##create item, but do not put this item into model
        data = itemTestLab.itemData;
        isDirectory = itemTestLab.iconDirectoryItem; 
        parent = model.rootItem
        ##create TreeItem of the given data
        item = ChooseTreeItemTestLab(data, parent, isDirectory);
        childCounting = model.rootItem.childCount();
        ##start adding item into Model, under correct Parents Row
        model.beginInsertRows(self.treeView.rootIndex(), childCounting, childCounting);#        
        ##add item to the Data tree
        parent.appendChild(item);
        ##finish adding item into Model
        model.endInsertRows();
    
    def removeItem(self, index):
        '''Remove icon. Remove one selected item from the Chose Test Set
        input = QModelIndex(index)
        output = None'''
        model = self.treeView.model();
        ##get parent as a QModelIndex, of this market index
        parentIndex = index.parent();
        ##check if given parent is root and then get TreeItem of this parent
        if not parentIndex.isValid():
            parentItem = model.rootItem;
        else:
            parentItem = parentIndex.internalPointer();
        ##get row position of the Item/Index which will be removed    
        row = index.row();
        ##start removing item/index from the Parents Row
        model.beginRemoveRows(parentIndex, row, row);
        ##remove item/index from the Data tree
        parentItem.removeChild(row);
        ##finish removing item/index from Model
        model.endRemoveRows();

    def clearItem(self):
        '''Trash icon. Clear all items in the Chose Test Set
        input = None
        output = None;'''
        model = self.treeView.model();
        ##get parent as a QModelIndex, of this market index
        parentIndex = QtCore.QModelIndex();
        parentItem = model.rootItem;
        
        countChilds = parentItem.childCount();
        ##start removing items from the root Parent
        model.beginRemoveRows(parentIndex, 0, countChilds);
        ##remove all items from the Data tree
        parentItem.removeAllChilds();
        ##finish removing items from the Model
        model.endRemoveRows();
        
    def updateModel(self, qc):
        
        data = ChoosegetDirTestSet(qc)
        model = ChooseTreeModelTestLab(data);
        self.treeView.setModel(model);



class MyTreeViewQCgraphSpecificTestResult(ResultTreeView, ResultMyDirModel):
    '''
    Operation in the QCgraph -> Cross test -> Result directories
    Used only for Directory view, and open result files in those directories. Based on  self.dir   
    '''
    
    def __init__(self, parent, dir=None):
        ResultTreeView.__init__(self, parent);

#        self.dir = QtCore.QString(r"./Result/QCgraph");
        if dir is None:
            self.dir = os.path.join(GlobalVariables.DIR_RESULT, 'QCgraph');
        else:
            self.dir = dir;
            
        if not os.path.exists(self.dir):
            try: os.makedirs(self.dir);
            except: pass;    
        
        
        self.updateModel();
        
    def refresh(self):
        self.updateModel();
    
    def updateModel(self):
        self.dirModel = ResultMyDirModel(self);
        self.treeView.setModel(self.dirModel) # we have to set the listView to use this DirModel
        
        self.treeView.setRootIndex(self.dirModel.index(self.dir)) # set the default to load
