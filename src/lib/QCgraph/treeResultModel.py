'''
Created on 28-09-2010

@author: wro50026
'''

from PyQt4 import QtCore, QtGui
import win32com.client
import pywintypes

import file_viewer

from lib.logfile import Logger
log = Logger(loggername="lib.QCgraph.treeResultModel", resetefilelog=False).log;




class TreeView(QtGui.QTreeView):
    '''
    SubClass of QtGui.QTreeView
    '''

    def __init__(self, parent):
        
        ##NAjlepiej by bylo, gdyby parent stal sie teraz self. Mozna by bylo wtedy wykasoawc wszystkie te koslawe self.treeView 
        
        self.treeView = parent;
        QtGui.QTreeView.__init__(self);
        
        self.ExcelApp = ExcelApp();
        self.FileViewer = FileViewer(); 

        self.connect(self.treeView, QtCore.SIGNAL("doubleClicked (const QModelIndex &)"), self._DoubleClicked);

#    def getModelItemCollection(self):
#        '''
#        From this list get the QModelIndex that we set with .setRootIndex
#        Using QModelIndex and DirModel we can get all the elements at that Index
#        '''
#        ModelIndex = self.treeView.rootIndex()
#        ModelIndexCollection = []
#        for i in range(0, self.treeView.dirModel.rowCount(ModelIndex)):
#            ModelIndexCollection.append(ModelIndex.child(i,0))
#        return ModelIndexCollection
     
    def _DoubleClicked(self):
        dirModel = self.treeView.model()
        if not dirModel.isDir(self.treeView.currentIndex()):
            path = dirModel.filePath(self.treeView.currentIndex());
            log.info(("File path:", path)); 
            ext = str(path).rsplit(".", 1)[1];
            self.openExtFile(ext, path)

            

    
    def openExtFile(self, ext, path):
        '''
        Open file based on the extention
        '''
        log.debug(("ext:", ext));
        if (ext == "xls") or (ext == "xlsx"):
            if self.ExcelApp.excel_object:
                try:
                    self.ExcelApp.openFile(path);
                except pywintypes.com_error, err:
                    log.warning(("Unable to open under Excel, due to the:", err));
                    self.FileViewer.openFile(path);
            else:
                self.FileViewer.openFile(path);
        elif ext == "txt":
            self.FileViewer.openFile(path);

        
        
class MyDirModel(QtGui.QDirModel):
    '''
    SubClass of QtGui.QDirModel
    '''

    def __init__(self, parent=None):
        super(MyDirModel, self).__init__(parent)
                
    def getData(self, ModelIndex):
        '''
        Using QModelIndex I can get data via the data() method or
        via any other method that support a QModelIndex
        '''
        paths = []
        if isinstance(ModelIndex, list):
            for items in ModelIndex:
                #print self.data(items).toString()
                paths.append(self.filePath(items))
            return paths
        else:
            raise ValueError("getData() requires a list (QtGui.QModelIndexs)")
  
class ExcelApp(object):
    '''
    Excel operation
    '''    

    #------------------------------------------------------------------------------ 
    def __init__(self):
        try:
            self.excel_object = win32com.client.Dispatch("Excel.Application");
        except pywintypes.com_error:
            log.warning(("ERR: Unable to run Microsoft Excel program. \n Please install first Microsoft Excel"));
            self.excel_object = None;
    
    def __del__(self):
        del self.__excel_object;
        
    #------------------------------------------------------------------------------
    def __get_excel_object(self):
        return self.__excel_object;
    def __set_excel_object(self, excel_object):
        self.__excel_object = excel_object;
    def __del_excel_object(self):
        del self.__excel_object;
    excel_object = property(__get_excel_object, 
                            __set_excel_object, 
                            __del_excel_object, 
                            "This is an Excel Object Application");
    #------------------------------------------------------------------------------
        
    def openFile(self, path):
        path = path.replace("/", "\\");
        log.info(("Open Excel file:", path));
        self.excel_object.Visible= True;
        self.excel_object.Workbooks.Open(path, ReadOnly=False);
        
        
        
class FileViewer(file_viewer.MainWindow):
    '''
    File_viewer operation. By this open/save any file
    '''        
    
    def __init__(self):
        super(FileViewer, self).__init__();
        #self.Scribble_window = scribble.MainWindow()
        
        
    def openFile(self, path):
        log.info(("Open Any file under scribble:", path));
        path = str(path);
        self.open(path)
        self.show();
