#!/usr/bin/env python

"""PyQt4 port of the tools/codecs example from Qt v4.x"""

import sys
from PyQt4 import QtCore, QtGui


class MainWindow(QtGui.QMainWindow):
    def __init__(self):
        QtGui.QMainWindow.__init__(self)

        self.textEdit = QtGui.QTextEdit()
        self.textEdit.setLineWrapMode(QtGui.QTextEdit.NoWrap)
        self.setCentralWidget(self.textEdit)
        self.saveAsActs = []
        self.createActions()
        self.createMenus()
        self.setWindowTitle(self.tr("File viewer"))
        self.resize(500, 400)
    
    def open(self, fileName=""):
        fileName = QtCore.QString(fileName);
        if fileName.isEmpty():
            fileName = QtGui.QFileDialog.getOpenFileName(self);
        if not fileName.isEmpty():
            inFile = QtCore.QFile(fileName)
            if not inFile.open(QtCore.QFile.ReadOnly):
                QtGui.QMessageBox.warning(self, self.tr("File viewer"),
                                          self.tr("Cannot read file %1:\n%2")
                                          .arg(fileName).arg(inFile.errorString()))
                return
            data = inFile.readAll()
            data = QtCore.QString(data)
            self.textEdit.setPlainText(data)
            
            
            
                
    def save(self):
        fileName = QtGui.QFileDialog.getSaveFileName(self)
        if not fileName.isEmpty():
            outFile = QtCore.QFile(fileName)
            if not outFile.open(QtCore.QFile.WriteOnly):
                QtGui.QMessageBox.warning(self, self.tr("File viewer"),
                                          self.tr("Cannot write file %1:\n%2")
                                          .arg(fileName).arg(outFile.errorString()))
                return
            action = self.sender()
            codecName = action.data().toByteArray()
            out = QtCore.QTextStream(outFile)
            out.setCodec(codecName.data())
            out << self.textEdit.toPlainText()
            
    def about(self):
        QtGui.QMessageBox.about(self, self.tr("About File viewer"), self.tr(
                                "The <b>File viewer</b> example demonstrates how to "
                                "read and write files."))
                    
    def createActions(self):
        self.openAct = QtGui.QAction(self.tr("&Open..."), self)
        self.openAct.setShortcut(self.tr("Ctrl+O"))
        self.connect(self.openAct, QtCore.SIGNAL("triggered()"), self.open)
        
        
        self.saveAct = QtGui.QAction(self.tr("&Save As"), self)
        self.saveAct.setShortcut(self.tr("Ctrl+S"))
        self.connect(self.saveAct, QtCore.SIGNAL("triggered()"), self.save)
        
        self.exitAct = QtGui.QAction(self.tr("E&xit"), self)
        self.exitAct.setShortcut(self.tr("Ctrl+Q"))
        self.connect(self.exitAct, QtCore.SIGNAL("triggered()"),
                     self, QtCore.SLOT("close()"))
        self.aboutAct = QtGui.QAction(self.tr("&About"), self)
        self.connect(self.aboutAct, QtCore.SIGNAL("triggered()"),
                     self.about)
        self.aboutQtAct = QtGui.QAction(self.tr("About &Qt"), self)
        self.connect(self.aboutQtAct, QtCore.SIGNAL("triggered()"),
                     QtGui.qApp, QtCore.SLOT("aboutQt()"))
        
    def createMenus(self):

        self.fileMenu = QtGui.QMenu(self.tr("&File"), self)
        self.fileMenu.addAction(self.openAct)
        self.fileMenu.addAction(self.saveAct)
        self.fileMenu.addSeparator()
        self.fileMenu.addAction(self.exitAct)
        self.helpMenu = QtGui.QMenu(self.tr("&Help"), self)
        self.helpMenu.addAction(self.aboutAct)
        self.helpMenu.addAction(self.aboutQtAct)
        self.menuBar().addMenu(self.fileMenu)
        self.menuBar().addSeparator()
        self.menuBar().addMenu(self.helpMenu)


if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv)
    mainWin = MainWindow()
    mainWin.show()
    sys.exit(app.exec_())
