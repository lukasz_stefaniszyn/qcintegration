# -*- coding: utf-8 -*-
#!/usr/bin/env python

'''
Created on 2009-02-14

@author: Lukasz Stefaniszyn
'''
import sys

import lib.qc_connect_disconnect as qc_connect_disconnect 
import lib.makeGraph as makeGraph
import lib.makeFile as makeFile

import lib.filter_wroclaw as filter_wroclaw;

from ..logfile import Logger
log = Logger(loggername="lib.QC_graph_ext.QCgraph", resetefilelog=False).log;

# TSF = qc.TestSetFactory;

# ##Print TestSet to TestSet Execution
# graph= TSF.BuildSummaryGraph(-1, "TC_CYCLE_NAME", "TC_STATUS", "", 0, TSF.Filter, False, False)

# ##Print Execution Tester to TestSet Execution 
# graph = TSF.BuildSummaryGraph(-1, "TC_ACTUAL_TESTER", "TC_STATUS", "", 0, TSF.Filter, False, False)


def make_graph(TestSetFactory, x, y, graphType, ID=-1):
    
    log.debug(("Building summary graph for:", x, y));
    ##Building summary graph for: ('CY_CYCLE', 'Test Set') ('TC_STATUS', 'Status')
    
    graph = makeGraph.make_summary_graph(x[0], y[0], TestSetFactory, ID);
    if graph:
        MakeGraphFile = makeFile.MakeGraphFile(graphType, x, y);
        MakeGraphFile.writeToCSVFile(graph);
        #MakeGraphFile.writeToXLSFile();
        
        #myWrocFile = filter_wroclaw.MyWrocFile(MakeGraphFile.filename_csv, x, y);
        #myWrocFile.main();
        
        
    else:   
        log.warning(("ERR: Unable to create graph: ", graphType));

    
def QCgraph_92():
      
    
    
    
    # usage_text = "usage: %prog <options> Excel_filename\n"\
                    # "\nExcel_filename - this is excel filename from which Steps, TestCases will be updated to Quality Center\n";
    # server, UserName, Password, DomainName, ProjectName, args = read_arguments.arguments(usage_text);
    
    import getpass
    server = "http://muvmp034.nsn-intra.net/qcbin/" 
    UserName="stefaniszyn"
    Password = getpass.getpass('Password for %s: '%UserName);
    
    DomainName="HIT7300"
    ProjectName="hiT73_R43x"
    
    print "\n";
    print "#"*60;
    print "server:", server;
    print "UserName:", UserName;
    print "DomainName:", DomainName;
    print "ProjectName", ProjectName;
    print "#"*60;
    print "\n";
    
    
    qcConnect = qc_connect_disconnect.QC_connect(server, UserName, Password, DomainName, ProjectName);
    if qcConnect.connect():
        print("Connected to QC");
    else:
        print("Not connected to QC server, exiting");
        sys.exit(1);
    
    
    # all_name = ['TESTCYCL', 'TEST', 'STEP', 'RUN', 'REQ', 'DESSTEPS','CYCL_FOLD','CYCLE', 'BUG','ALL_LISTS']
    # for name in all_name:
        # makeGraph.listFields(qcConnect.qc, name);
    
    
    
    
    ##Print TestSet to TestSet Execution
    ##"TC_CYCLE_NAME", "TC_STATUS"
    
    ##Print Execution Tester to TestSet Execution 
    ##"TC_ACTUAL_TESTER", "TC_STATUS"
    GraphsSummary = {'TestSet_vs_Status': ("CY_CYCLE", "TC_STATUS"), 'ResposibleTester_vs_Status': ("TC_TESTER_NAME", "TC_STATUS")};
    GraphsSummary = ({"CY_CYCLE":"TestSet" , "TC_STATUS":"Status"}, {"TC_TESTER_NAME":"ResposibleTester", "TC_STATUS":"Status"});
    ##Generate graph 
    for graphType in GraphsSummary:
        x, y = graphType.keys();
        print "Building summary graph for:",graphType;
        print "x, y:", x, y
        graph = makeGraph.make_summary_graph(qcConnect.qc, x, y);
        if graph:
            x, y = graphType.items();
            MakeGraphFile = makeFile.MakeGraphFile("Summary", graph, x, y);
            MakeGraphFile.writeToCSVFile();
            #MakeGraphFile.writeToXLSFile();
            
            myWrocFile = filter_wroclaw.MyWrocFile(MakeGraphFile.filename_csv, x, y);
            myWrocFile.main();
            
            
        else:   
            print "ERR: Unable to create graph: ", graphType;
    

    if qcConnect.disconnect_QC():
        print "\nDisconnecting from QC server."\
                "\n"\
                "\nThank You for using QC_import_export script.\n"\
                "\t\t\t\n"\
                "\t\t\t  Lukasz Stefaniszyn\n"\
                "\n\t\t\t\t\tPowered by Python 2.5";
    sys.exit(0);
    
    
    
if __name__ == "__main__":
    print "Start";
    QCgraph_92();