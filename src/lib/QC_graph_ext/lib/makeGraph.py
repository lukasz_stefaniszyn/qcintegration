def getFieldValues(qc, fieldSystemName, fieldGroupList):
    '''Get all possible values from given field
    input:
    qc = quality center connection instance
    fieldSystemName = system name of field, from which will be taken possible values, eg. str("TS_STATUS")  
    fieldGroupList = list of field group of given field, eg.tuple("TEST", "CYCLE") 
    
    output:
    fieldValues = list of possible values of given field
    '''
    
    fieldValue = [];
    
    for fieldGroup in fieldGroupList:
        fieldList = qc.Fields(fieldGroup);
        for field in fieldList.__iter__():
            if fieldSystemName == field.Name:
                prop = field.Property;
    #            if "ListCombo" in prop.EditStyle:
    ##Brak tutaj wydruku dla EditStyle == "UserCombo". Jak np. dla TS_RESPONSIBLE
    ##trzeba bedzie z innego miejsca pobierac UserCombo. Nie mozna tego pola pominac
    
                root = prop.Root;
                if root is not None: 
                    try:
                        fieldsValues = root.NewList();
                    except AttributeError, err:
                        return False;
                    fieldValue.append('Name'); ##The first column is TCname, TSname, etc.
                    for value in fieldsValues.__iter__():
                        fieldValue.append(value.Name);
                break;
#    fieldValue.sort();
    return fieldValue;
    


def make_summary_graph(Xaxis, Yaxis, TestSetFactory, ID=-1):
    '''
    This will create Progress Graph from the excution 
    input = qc_instance(qc), str(Xaxis), str(Yaxis)
    output = qc_graph_instance();
    '''
    
    #!!! Removed TS_DESCRIPTION,   possible   LongVarChar??   
    # all = ['TC_USER_03',
    # 'TS_USER_05',
    # 'TS_USER_06',
    # 'TS_DEV_COMMENTS',
    # 'TS_CREATION_DATE',
    # 'TS_RESPONSIBLE',
    # 'TS_ESTIMATE_DEVTIME',
    # 'TC_EXEC_DATE',
    # 'TS_EXEC_STATUS',
    # 'TC_ITERATIONS',
    # 'TS_VTS',
    # 'TC_VTS',
    # 'TS_PATH',
    # 'TC_PLAN_SCHEDULING_DATE',
    # 'TC_PLAN_SCHEDULING_TIME',
    # 'TC_HOST_NAME',
    # 'TS_USER_03',
    # 'TS_USER_01',
    # 'TC_USER_02',
    # 'TC_USER_01',
    # 'TC_TESTER_NAME',
    # 'TC_STATUS',
    # 'TS_STATUS',
    # 'TS_SUBJECT',
    # 'TC_ASSIGN_RCYC',
    # 'TS_TEMPLATE',
    # 'TS_USER_04',
    # 'TS_NAME',
    # 'TC_TEST_VERSION',
    # 'TS_USER_02',
    # 'TC_ACTUAL_TESTER',
    # 'TC_EXEC_TIME',
    # 'TS_TYPE',
    # 'TS_DESCRIPTION']

    # all = ['CY_CLOSE_DATE',
     # 'CY_USER_01',
     # 'CY_COMMENT',
     # 'CY_REQUEST_ID',
     # 'CY_VTS',
     # 'CY_OPEN_DATE',
     # 'CY_STATUS',
     # 'CY_ASSIGN_RCYC',
     # 'CY_CYCLE',
     # 'CY_FOLDER_ID']    
    
    
    # for axis in all:
        # Xaxis = axis;
        # Yaxis = axis;
        # print "Axis:", axis;
        # graph= TSF.BuildSummaryGraph(-1, Xaxis, Yaxis, "", 0, TSF.Filter, False, False);



        
    ##create graph from X,Y axis
    #print "Building summry graph for x=%s, y=%s"%(Xaxis, Yaxis);
    TSF = TestSetFactory;

    try:
        graph= TSF.BuildSummaryGraph(ID, Xaxis, Yaxis, "", 0, TSF.Filter, False, False);
    except:
        return False;
    
    return graph;

    
    
    
def listFields(qc, name="ALL_LISTS"):
    '''
    This will generate all mapping of fields from UserNameField and qcDatabaseField
    # Fields informations
        # Some possible values for DataType are:

        # "ALL_LISTS" for Subject 
        # "BUG" for Defect 
        # "CYCLE" for Test Set 
        # "CYCL_FOLD" for Test Set Folder 
        # "DESSTEPS" for Design Step 
        # "REQ" for Requirement 
        # "RUN" for Run 
        # "STEP" for Run Step 
        # "TEST" for Test 
        # "TESTCYCL" for Test in Test Set (Test Instance)

        # For more possibilities, see the Quality Center Database Reference.
    '''
    
    f_fields_T = file("fields_T_%s.txt"%name, "wb");
    f_fields_F = file("fields_F_%s.txt"%name, "wb");
    fieldList = qc.Fields(name);
    for aField in fieldList:
        FieldProp = aField.Property;
        if FieldProp.IsActive:
            f_fields = f_fields_T;
        else:
            f_fields = f_fields_F;
        f_fields.write("aFiled.Name: %s \n"%aField.Name);
        f_fields.write("aFiled.Type: %s \n"%aField.Type);
        f_fields.write("FieldProperty.UserLabel: %s\n"%FieldProp.UserLabel);
        f_fields.write("FieldProperty.UserColumnType: %s\n"%FieldProp.UserColumnType); 
        f_fields.write("FieldProperty.DBColumnName: %s\n"%FieldProp.DBColumnName);
        f_fields.write("FieldProperty.DBColumnType: %s\n"%FieldProp.DBColumnType);
        f_fields.write("FieldProperty.IsActive: %s\n"%FieldProp.IsActive);
        f_fields.write("FieldProperty.EditStyle: %s\n"%FieldProp.EditStyle);
        f_fields.write("FieldProperty.IsCanFilter: %s\n"%FieldProp.IsCanFilter); 
        f_fields.write("FieldProperty.IsCanGroup: %s\n"%FieldProp.IsCanGroup); 
        f_fields.write("FieldProperty.IsCustomizable: %s\n"%FieldProp.IsCustomizable); 
        f_fields.write("FieldProperty.IsKey: %s\n"%FieldProp.IsKey); 
        f_fields.write("FieldProperty.IsToSum: %s\n"%FieldProp.IsToSum); 
        f_fields.write("FieldProperty.IsSearchable: %s\n"%FieldProp.IsSearchable); 
        
        
        
         
        f_fields.write(str("-"*60 + "\n"));
        f_fields.flush();
    print f_fields.name;
    f_fields_T.close();
    f_fields_F.close();