# -*- coding: utf-8 -*-
#!/usr/bin/env python

'''
Created on 2009-09-30

@author: Lukasz Stefaniszyn
'''




def arguments(usage_text):
    from optparse import OptionParser;
    
    ##Default values
    UserName_def = '';
    server_def = "http://muvmp034.nsn-intra.net/qcbin/";
    DomainName_def = "HIT7300";
    ProjectName_def = "hiT73_R43x";

    parser = OptionParser(usage_text)
    parser.add_option("-s", "--server", dest="server",
                       help = "input server http address",
                       default = server_def);
    parser.add_option("-u", "--user", dest="UserName",
                      help = "input user name used in QualityCenter",
                      default = UserName_def);
    parser.add_option("-p", '--pass', dest = "Password",
                      help = "input password used in QualityCenter",
                      default = "");
    parser.add_option("-d", "--domain", dest = "DomainName",
                       help = "input Domain name used in QualityCenter, default = HIT7300",
                       default = DomainName_def);
    parser.add_option("-r", "--project", dest = "ProjectName",
                       help = "input Project name used in QualityCenter, deafult = hiT73_R43x",
                       default = ProjectName_def );
    parser.add_option("-f", "--file", dest = "parameterFile", 
                      help = "parameter filename with UserName, DomainName, ProjectName",
                      default = '');
    
    
    
    (options, args) = parser.parse_args()
    
    
    if (options.parameterFile or options.UserName) == '':
        parser.error("Please use --file or --user with/without default --server, --domain, --project");
    
    if options.parameterFile !='':
        import os.path
        import re
        
        
        ##There is need to make blank server, UserName, etc. to have possible overwrite by option.UserName, etc.
        server = '';
        UserName = '';
        DomainName = '';
        ProjectName = '';
        
        parameterFile = options.parameterFile;
        if os.path.exists(parameterFile):
            fo_parameterFile = file(parameterFile, 'rb');
            for line in fo_parameterFile.readlines():
                #print line;
                if re.compile('server[\s]*:').match(line):
                    server = line.split(':',1)[1].strip();
                    continue;
                elif re.compile('UserName[\s]*:').match(line):
                    UserName = line.split(':',1)[1].strip();
                    continue;
                elif re.compile('DomainName[\s]*:').match(line):
                    DomainName = line.split(':',1)[1].strip();
                    continue;
                elif re.compile('ProjectName[\s]*:').match(line):
                    ProjectName = line.split(':',1)[1].strip();
                    continue;
        else:
            parser.error("Unable to find parameter file: %s"%str(parameterFile));

        ##if in file there was not some of server, UserName, then make them as default
        if server == '':
            server = server_def;
        if UserName == '':
            UserName = UserName_def;
        if DomainName == '':
            DomainName = DomainName_def;
        if ProjectName == '':
            ProjectName = ProjectName_def;
        
        #print "File:",server, UserName, DomainName, ProjectName;
        
    


    if (options.parameterFile!=''): 
        if options.server!=server_def:
            server = options.server
        if options.UserName!='':
            UserName = options.UserName;
        if options.DomainName!=DomainName_def:
            DomainName = options.DomainName;
        if options.ProjectName!=ProjectName_def:
            ProjectName = options.ProjectName;
            
        if (options.server!=server_def or options.UserName!='' or options.DomainName!=DomainName_def or options.ProjectName!=ProjectName_def):
            print("Values given from option will be used as first")
    
        #print "Dual:",server, UserName, DomainName, ProjectName;
        
        
    if options.parameterFile != '':
        options.server = server;
        options.UserName = UserName;
        options.DomainName = DomainName;
        options.ProjectName = ProjectName;
        #print "End:",server, UserName, DomainName, ProjectName;
    
    ##if ther was no password, then ask ask for password
    if options.Password == '':
        import getpass;
        options.Password = getpass.getpass('Password for %s: '%options.UserName);
    
          
#    print "\t!!Args:", args;
#    print options.server, options.UserName, options.Password, options.DomainName, options.ProjectName;
    
    return options.server, options.UserName, options.Password, options.DomainName, options.ProjectName, args;