# -*- coding: utf-8 -*-
#!/usr/bin/env python

'''
Created on 2010-02-14

@author: Lukasz Stefaniszyn
'''

import csv
import os;
from time import strftime, localtime;
import types

import win32com.client
from win32com.client import constants

from lib.logfile import Logger
from globalVariables import GlobalVariables
log = Logger(loggername="lib.QC_graph_ext.lib.makeFile", resetefilelog=False).log;

def replace_all(text, dic):
    for i, j in dic.iteritems():
        text = text.replace(i, j);
    return text;


class CreateDataTable(object):
    '''Create Data table with items given from QualityCenter  Xaxis and Yaxis
    input:
    colNames = list of column names, eg. list("Open", "Closed", "Review")
    '''
    
    def __init__(self, colNames=[]):
        self.__data = {};
        self.__colNames = {};
        self.makeColumnNames(colNames); ##create column Names
    #------------------------------------------------------------------------------ 
    def _get_data(self):
        return self.__data;
    def _set_data(self, rowValueName):
        self.__data.update(rowValueName);
    data = property(_get_data, _set_data, None, "Data table object");
    
    def _get_colNames(self):
        return self.__colNames;
    def _set_colNames(self, colName):
        if colName is not None: colName = colName.encode('ascii', 'replace')
        else: colName = "Unknown"
        colName = colName.encode('ascii', 'replace')
        self.__colNames.update({colName:len(self.__colNames)});
    colNames = property(_get_colNames, _set_colNames, None, "Column names for table");
    
    
    def _get_colNameAsList(self):
        
        colNamesAsList = [None for i in range(len(self.colNames))]; ##create blank fieldValue with length of ColNames from CreatedDataTable
#        colNames[0]= 'Name'; ##The first column is TCname, TSname, etc.
        for key, value in self.colNames.iteritems():
            colNamesAsList[value] = key; ##based on ColName position "value" put in the correct order into fieldValue
        return colNamesAsList;
    colNamesAsList = property(_get_colNameAsList, None, None, "ColName created as list from dictionary");
        
        
    #------------------------------------------------------------------------------ 

    def makeColumnNames(self, colNames):
        '''
        At the first start create dictionary from given Column Names list
        input:
        colNames = list of Column Names, eg. list(Open", "Closed", "Review")
        output:
        None, update self.colNames
        '''
        if not colNames:
            self.__colNames = {'Name':0};
            return;
        
        i = 0;
        for colName in colNames:
            colName = colName.encode('ascii', 'replace')
            self.colNames.update({colName:i});
            i += 1;
        log.debug(("Columns:", self.colNames));
        
        return
        

    def getColName(self, colName):
        '''
        Return OR create and return column name
        input:
            colName = this is column name to be check in the header columnNames, eg. "Executed"
        output:
            col = return column name, with position as tuple, e.g. tuple("Exception", 0) 
        '''
        colName = colName.encode('ascii', 'replace')
        if not self.colNames.has_key(colName): ##if column name is not present, then add
            self.colNames = colName;
        self.colNames[colName]
        col = (colName, self.colNames[colName]);
        return col;

    def addRow(self, rowName, colName):
        '''
        Add Or modify row in the data table
        input:
            rowName = this is row name, eg. "TC_new"
            colName = this is column name which will be put to the table as integer based on column name, eg. str("Review")
        output:
            None
        '''
        if colName is not None: 
            if isinstance(colName, str):
                colName = colName.encode('ascii', 'replace');
            elif isinstance(colName, types.InstanceType): ##for instances 
                colName = "Unknown";
            else:
                try: 
                    colName = str(colName);
                except AttributeError: 
                    log.debug(("colName unable to change to str()"));
                    colName = "Unknown";
        else: colName = "Unknown"

        
        if isinstance(rowName, types.InstanceType): ##for instances 
            rowName = "Unknown";
        if not self.data.has_key(rowName): ##row is not yet in the table
            self.getColName(colName)
            value = 1;
            self.data = {rowName:{colName:value}};
        else:  ##row is present in the table. Column Values should be sum
            self.getColName(colName)
            row = self.data.get(rowName);
            if row.has_key(colName): ##check if column name is present in the row
                value = row.get(colName); ##get the previous value of this column and increase
                value += 1;
                row.update({colName:value}); 
            else: ##in this row Column was not yet present
                value = 1;
                row.update({colName:value});
            
class MakeGraphFile(csv.DictWriter, object):

    def __init__(self, graphType, Xaxis, Yaxis, fieldnames=None):
        

        self.__Xaxis = Xaxis[0];
        self.__Yaxis = Yaxis[0];
        
        self.createFile(graphType, Xaxis, Yaxis);
        
        if fieldnames is None:
            self.__graph = None;
            self.__RowCount = self.graph.RowCount;
            self.__ColCount = self.graph.ColCount;
            self.writerCsv = self.fileCsv;
        else:
            csv.DictWriter.__init__(self, self.fileCsv, fieldnames, dialect='excel-tab', extrasaction='ignore');
            self.makeHeader(fieldnames);

    def get_filename(self):
        return self.__filename
    filename = property(get_filename, None, None, "filename with path")
        
    
    def __get_graph(self):
        return self.__graph;
    graph = property(__get_graph, None, None, "Graph QC instance");
    
    def __get_RowCount(self):
        return self.__RowCount;
    RowCount = property(__get_RowCount);
    
    def __get_ColCount(self):
        return self.__ColCount;
    ColCount = property(__get_ColCount);
    
    def __get_Axis(self):
        return self.__Xaxis, self.__Yaxis;
    Axis = property(__get_Axis);
   
    def __get_fileCsv(self):
        return self.__fileCsv;
    def __set_fileCsv(self, filename):
        self.__fileCsv = open(filename, r"wb");
    def __del_fileCsv(self):
        self.__fileCsv.flush();
        self.__fileCsv.close();
    fileCsv = property(__get_fileCsv, __set_fileCsv, __del_fileCsv, "This is file object opened to write in");
    
    def __get_writerCsv(self):
        return self.__writerCsv;
    def __set_writerCsv(self, filename):
        self.__writerCsv = csv.writer(filename, dialect = csv.excel, delimiter = '\t');
    writerCsv = property(__get_writerCsv, __set_writerCsv, None, "CSV writer object");
    #-----------------------------------------------------------
    
    def makeTargetDir(self):
        '''
        This will create .\..\Result\QCgraph with the catalog of the current download in format Download_%d-%m-%Y_%H-%M-%S
        input = None
        output = str() .\..\Result\QCgraph\20120212\
        '''
        mainPath = os.path.join(GlobalVariables.DIR_RESULT, 'QCgraph');
        
        date = strftime('%Y-%m-%d_week%U', localtime());
        
        if not os.path.exists(mainPath):
            os.mkdir(mainPath);
        
        result_dir = os.path.join(mainPath, str(date));
        if not os.path.exists(result_dir):
            os.mkdir(result_dir);
        return result_dir
    
    def createFile(self, graphType, Xaxis, Yaxis):
        '''
        Create filename, used for writing data:
        input:
            graphType = text description of graph type,  "Summary"
            Xaxis = tuple of Xaxis data, e.g. (<qcInstance>, "TC_NAME")
            Yaxis = tuple of Yaxis data, e.g. (<qcInstance>, "TC_RESPONSIBLE")
        output:
            None. Write filename into self.fileCsv
        '''
        
        dateTime = strftime('%Y-%m-%d_%H%M%S_week%U', localtime());
        
        ##Check catalog Result
        result_dir = self.makeTargetDir();
        
        ##create file
        graphType = str(graphType);
        dic = {' ':'', ':':'', '/':'', '.':'', "\\":""};
        Xaxis_name = replace_all(Xaxis[1], dic)
        Yaxis_name = replace_all(Yaxis[1], dic)
        
        filename = str("%s_%s_vs_%s_%s"%(graphType, Xaxis_name, Yaxis_name, dateTime));
        self.__filename = os.path.join(result_dir, filename+".xls"); 
        
        self.fileCsv = self.filename 
        log.info(("File:",self.fileCsv));
#        self.__filename_xls = result_dir +"\\"+ filename + ".xls"; 
        
        
    
    def makeHeader(self, fieldnames):
        '''Create header line if the writer is DictWriter
        input:
            fieldnames = list of column names, e.g. ['Name', 'Blocked','Deferred']
        output:
            None
        '''
        self.writer.writerow(fieldnames)
    
    def makeDictRow(self, rows):
        '''Write one row into the DictWriter instance. Used only when creating class, fieldnames was not None
        input:
            rows = rows as dictionary,  e.g. {'TestCase1':{'Blocked':1, 'Deffered':4}, 'TestCase2':{'Blocked':5}}
        output:
            None
        '''
        log.debug(("Row to write:", rows));
        for k, v in rows.iteritems():
            row = {};
            row['Name'] = k;
            row.update(v);
            log.debug(("Row:", row));
            self.writerow(row);
#            try:
#                self.writerow(row);
#            except:
#                pass;
    
    
    def colNames(self):
        '''Get column names, when in Class graph as qcInstanc is used 
        input:
            None
        output:
            None
        '''
        colNames = [''];
        
        for col in range(self.ColCount):
            colNames.append(self.graph.ColName(col).encode("utf-8"));
        
        
        if self.Axis[1] == "TC_STATUS":
            colNames.extend(['Total','Execution_(Total-N/a-Deferred) [%]', 'Passed_(Total-N/a-Deferred) [%]', 'Execution [%]', 'Passed [%]']);
            #print "colNames=", colNames 

#        print "self.Axis[1] =", self.Axis[1];
#        print "self.Axis=", self.Axis;
        return colNames;
    
    
    def createRow(self, rowNumber):
        '''
        Create full value from all Columns per Row
        graph.Data(Col, Row)
        '''
        rowValue = [];
        
        cell = (self.graph.RowName(rowNumber)).encode("utf-8");
        rowValue.append(cell);
        
        for colNumber in range(self.ColCount):
            # print "col, row=", str(self.graph.Data(colNumber, rowNumber));
            rowValue.append(str(self.graph.Data(colNumber, rowNumber)));

            
        if self.Axis[1] == "TC_STATUS":
            rowValue = self.addStatusRow(rowValue);

        
        return rowValue;
        
        
    def addStatusRow(self, rowValue):
        '''Additional columns are added when axis is TC_STATUS
        input:
            rowValue = row with data values, e.g ['TestCase1', 1, 3, 7, 67, 0]
        output:
            rowValue = row with data values added to the basic rowValue, e.g. 'TestCase1', 1, 3, 7, 67, 0, 234, 67]
        '''
        colNames = {};
        ##get column names to know column position 
        i= 0;
        for colName in self.colNames():
            colNames.update({colName:i});
            i += 1;
            
        try:
            ##count total number of Status
            c_total = sum([int(value) for value in rowValue[1:]]);
            
            ##count TC execution [%]
    #            print "rowValue", rowValue
    #            print "Failed index=", colNames.get('Failed');
    #            print "Passed index=", colNames.get('Passed');
            n_Failed = float(rowValue[colNames.get('Failed')]);
            n_Passed = float(rowValue[colNames.get('Passed')]);
            n_NA = float(rowValue[colNames.get(r'N/A')]);
            n_Deferred = float(rowValue[colNames.get('Deferred')]);
            
            if (c_total) > 0:
                c_execution = (( n_Failed + n_Passed ) / float(c_total)) * 100;
                c_passed =  (n_Passed / float(c_total)) * 100;
            else:
                c_execution = 0;
                c_passed = 0;
            
            if (c_total-n_Deferred-n_NA) > 0:
                c_execution_wroc = (( n_Failed + n_Passed ) / (float(c_total)-n_Deferred-n_NA)) * 100;
                c_passed_wroc =  (n_Passed / (float(c_total)-n_Deferred-n_NA)) * 100;
            else:
                c_execution_wroc = 0;
                c_passed_wroc = 0;



            ##add results into rowValues
            result = [str(num).replace(".", ",") for num in [c_total, round(c_execution_wroc, 2), round(c_passed_wroc, 2), round(c_execution, 2), round(c_passed, 2)] ]
            rowValue.extend(result);
            
            return rowValue
        
        except BaseException, err:
            log.warning(("ERR: ", err));
        
        return False;
    
    
    
    def writeToCSVFile(self, graph):
        '''
        Get all ColumnNamesa and Rows and write them into CSV file. This is used only with qcInstance Graph
        input:
            graph = qc instance of generated graph,  <qcInstanceGraph>
        output:
            None
        '''
        self.graph = graph
        self.writeCsv.writerow(self.colNames());
        
        for rowNumber in range(self.RowCount):
            try:
                self.writeCsv.writerow(self.createRow(rowNumber));
            except:
                log.debug(("unable to write line:", self.createRow(rowNumber)));
        del self.fileCsv;
        
    def writeToXLSFile(self):
        ''' 
        Write result into Excel format file XLS
        '''
        
        
        try:
            object = win32com.client.Dispatch("Excel.Application");
        except:
            log.warning(("ERR: Microsoft Office Excel is not installed"));
            return False
        
        
        ##import from csv format, delimiter=tab
        object.Workbooks.OpenText(Filename=os.getcwd() + '\\' + self.filename_csv, 
                                  DataType=constants.xlDelimited, 
                                  Tab=True);
                                  
        ##switch to recently open TextFile
        myWorkBook = object.ActiveWorkbook;
        
        ##save file as XLS
        myWorkBook.SaveAs(Filename=os.getcwd() + '\\' + self.filename_xls, 
                          FileFormat=constants.xlExcel7)
        ##close open file
        myWorkBook.Close();
        
        del myWorkBook;
        del object;
        
        return True;
        
if __name__ == "__main__":
    
    Xaxis = ("qcObject", "TC_NAME")
    Yaxis = ("qcObject", "TC_RESPONSIBLE")
    Graph = "BLE";
    fieldnames = ['Name', 'Blocked','Deferred', 'Failed', 'N/A', 'No Run', 'Not Completed', 'Passed', 'Total',];
    
    makeGraphFile = MakeGraphFile(Graph, Xaxis, Yaxis, fieldnames);
    
    
    row = {'Name': 'TestSet1',
            'Blocked': '3',
            'Failed': '0',
            'N/A': '0',
            'No Run': '3',
            'Not Completed': '0'};
            
    
    makeGraphFile.makeDictRow(row);
    del makeGraphFile        
        
