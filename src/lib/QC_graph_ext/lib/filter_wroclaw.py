# -*- coding: utf-8 -*-
#!/usr/bin/env python


'''
Created on 2010-02-22

@author: wro50026
'''
import csv
import os

import win32com.client
from win32com.client import constants


#===============================================================================
# GLOBAL VARIABLES
#===============================================================================
TESTERS_LIST = ['michalowski', 'stefaniszyn', 'jarczak', 'francman', 'prajzner', 'lukasik'];
TESTSET_LIST = ['Wroclaw_02'];

class MyWrocFile(object):
    '''
    this will make special action adopted only for Wroclaw usage
    '''


    def __init__(self, filename_csv, Xaxis, Yaxis):


        self.__Xaxis = Xaxis;
        self.__Yaxis = Yaxis;


        ##open previous created CSV file 
        self.__fo_csv = file(filename_csv, 'rb');
        self.fileReaderCSV = csv.reader(self.__fo_csv, dialect = csv.excel, delimiter = '\t');
        


        ##create file     "Wroc_"  +  "TestSet_vs_Status_week08_21-02-2010.txt"
        self.__filename_wroc = filename_csv.rsplit('.', 1)[0] + '.txt';
        
        ##write new filtered csv file
        self.__fw_csv = file(self.filename_wrocCSV, 'wb'); 
        self.fileWriterCSV = csv.writer(self.__fw_csv, dialect = csv.excel, delimiter = '\t');


    def __del__(self):
        ##close opened files 
        del self.fileReaderCSV;
      
    #-------------------------------------------------------

    def __get_Axis(self):
        return self.__Xaxis, self.__Yaxis;
    Axis = property(__get_Axis);

    def __get_filename_wroc(self):
        return self.__filename_wroc;
    filename_wrocCSV = property(__get_filename_wroc, None, None, "File name for Wroclaw CSV")

    def __get_filename_wrocXLS(self):
        filenameXLS = self.filename_wrocCSV.rsplit('.', 1)[0] + ".xls"
        return filenameXLS;  
    filename_wrocXLS = property(__get_filename_wrocXLS, None, None, "File name for Wroclaw XLS")

    def __get_fileReaderCSV(self):
        return self.__fileReaderCSV;
    def __set_fileReaderCSV(self, fileReaderCSV):
        self.__fileReaderCSV = fileReaderCSV;
    def __del_fileReaderCSV(self):
        self.__fo_csv.close();
    fileReaderCSV = property(__get_fileReaderCSV, 
                             __set_fileReaderCSV, 
                             __del_fileReaderCSV, 
                             "CSV file reader");

    def __get_fileWriterCSV(self):
        return self.__fileWriterCSV;
    def __set_fileWriterCSV(self, fileWriterCSV):
        self.__fileWriterCSV = fileWriterCSV
    def __del_fileWriterCSV(self):
        self.__fw_csv.flush();
        self.__fw_csv.close();    
    fileWriterCSV = property(__get_fileWriterCSV, 
                             __set_fileWriterCSV, 
                             __del_fileWriterCSV, 
                             "CSV file writer");


        
    #------------------------------------------------------------------------------


    def filterResposiblerTester(self, testers_list=TESTERS_LIST):
        '''
        Get tester_list as list. Find tester and return only those rows 
        '''
        
        for row in self.fileReaderCSV:
            if self.fileReaderCSV.line_num == 1:
                self.fileWriterCSV.writerow(row); ##write row with column names
                continue;

            tester_csv = row[0]                
            #tester_csv = row[0].encode("utf-8");
            if tester_csv in testers_list:
                self.fileWriterCSV.writerow(row);
                testers_list.remove(tester_csv);
        
        if testers_list:
            print "Did not find in csv file, those testers:", testers_list;
        
        ##close opened to write files
        del self.fileWriterCSV;


    def filterTestSetExcution(self, TestSet_list=TESTSET_LIST):
        '''
        Get TS_name. Find TestSet as list in csv file and return only this ones
        '''
        
        
        for row in self.fileReaderCSV:
            if self.fileReaderCSV.line_num == 1:
                self.fileWriterCSV.writerow(row); ##write row with column names
                continue;
                
            testSet_csv = row[0]
            #testSet_csv = row[0].encode("utf-8");
            if testSet_csv in TestSet_list:
                self.fileWriterCSV.writerow(row);
                TestSet_list.remove(testSet_csv);

        if TestSet_list:
            print "Did not find in csv file, those TestSets:", TestSet_list;
        
        ##close opened to write files
        del self.fileWriterCSV;
     
     
     
    def writeToXLSFile(self):
        ''' 
        Write result into Excel format file XLS
        '''

        
        try:
            object = win32com.client.Dispatch("Excel.Application");
        except:
            print "ERR: Microsoft Office Excel is not installed"
            return False
        
        
        ##import from csv format, delimiter=tab
        object.Workbooks.OpenText(Filename=self.filename_wrocCSV, 
                                  DataType=constants.xlDelimited, 
                                  Tab=True);
                                  
        ##switch to recently open TextFile
        myWorkBook = object.ActiveWorkbook;
        
        ##save file as XLS
        myWorkBook.SaveAs(Filename=self.filename_wrocXLS, 
                          FileFormat=constants.xlExcel7)
        ##close open file
        myWorkBook.Close();
        
        del myWorkBook;
        del object;
            
        return True;
        

    def main(self):
        
        if self.Axis[0] == "TC_TESTER_NAME":
            self.filterResposiblerTester();
        elif self.Axis[0] == "CY_CYCLE":
            self.filterTestSetExcution();
            
        self.writeToXLSFile();
            
            
            
            
        
        
if __name__ == "__main__":

    import os
    os.chdir(r'd:\Work\svn_checkout\lukasz_repository_Fiona\QC_graph\src');

    filename_csv = r"ResposibleTester_vs_Status_week08_21-02-2010.txt"
    myWrocFile = MyWrocFile(filename_csv);

    myWrocFile.filterResposiblerTester(testers_list)
    #myWrocFile.writeToXLSFile();

    filename_csv = r"TestSet_vs_Status_week08_21-02-2010.txt"
    myWrocFile = MyWrocFile(filename_csv);
    #TestSet_list = ['Wroclaw_02'];
    myWrocFile.filterTestSetExcution(TestSet_list);
    #myWrocFile.writeToXLSFile();