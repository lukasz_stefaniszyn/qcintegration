'''
Created on 14-07-2012

@author: lucst
'''
from lib.QClicense.generateCRC import DataStartupCondition, \
    DataInstallationCondition
from lib.logfile import Logger, whoami, callersname
import PyQt4.QtCore as QtCore
import PyQt4.QtGui as QtGui



log = Logger(loggername='lib.QClicence.checkLicence', resetefilelog=False).log;


class DialogWindows(QtGui.QMessageBox):
    
    def __init__(self, parent):
        super(DialogWindows, self).__init__(parent);
        self.setWindowTitle('License Manager')
    
    def criticalWindow(self, text):
        
        self.setIcon(self.Critical);
        self.setStandardButtons(QtGui.QMessageBox.Ok);
        self.setText(text);
        
    def informationWindow(self, text):
        self.setIcon(self.Information);
        self.setStandardButtons(QtGui.QMessageBox.Ok);
        self.setText(text);
        

class DataStartupLicenceCheck(DataStartupCondition):

    def __init__(self, parent=None, datetimeNow=None, cmd=False):
        '''
        This is class for licence operation on non-licence file, 30day expiration
        :param parent: this is Qt parent object
        :param datetimeNow: this is used for unittest value. 
        '''
        self.dataInstallationCondition = DataInstallationCondition(datetimeNow);
        DataStartupCondition.__init__(self, datetimeNow);
        
        
        self.__parent = parent
        self.status = False
#        self.mainStartupCheck(); 

    def get_parent(self):
        return self.__parent
    parent = property(get_parent, None, None, "Qt parent item")

    def get_status(self):
        return self.__status
    def set_status(self, value):
        self.__status = value
    status = property(get_status, set_status, None, "status's docstring")
    
    def checkLicenseCode(self):
        ##first check if license code present.
        licenseCode = self.readLicenseCode();
        if licenseCode:#if value is differnt than False, then I can expect LicenseCode here, like int(3)
            self.status = True
            return licenseCode
        else:#license code is not enabled or it is wrong
            return False
    
    def dataStartupCheck(self, cmd=False):
        '''
        Check demo data license 
        '''
        if self.dataInstallationCondition.registerInstallionCheck():
            ##If all register data where None, then create them
            self.dataInstallationCondition.updateRegisterOnInstallation();

        
        if self.checkRegisterValue():
            log.debug("self.checkRegisterValue = True");
            if self.checkIfDataChanged():
                log.debug("self.checkIfDataChanged = True");
                ##Open information message box. Yield is used for unittests
                for dialogmessage in self.messageboxInformationStandard(cmd): yield dialogmessage
                self.status = True;
            else: 
                log.debug("self.checkIfDataChanged = False");
                ##Open information message box. Yield is used for unittests
                for dialogmessage in self.messageboxInformationExpire(cmd): yield dialogmessage
        else: 
            log.debug("self.checkRegisterValue = False");
            ##Open information message box. Yield is used for unittests
            for dialogmessage in self.messageboxInformationExpire(cmd): yield dialogmessage

    def messageboxInformationStandard(self, cmd=False):
        '''
        This will open information message box with expiration date. Yield operation will be used for unittests
        '''
        
        
        expirationDays = self.diffExpirationDays();
        text = "Your license will expire in %d day(s)"%(expirationDays);
        if not cmd:
            dialogWindows = DialogWindows(self.parent);
            dialogWindows.informationWindow(text);
            
            yield dialogWindows
            dialogWindows.exec_();
        else: 
            print(text);
    
    def messageboxInformationExpire(self, cmd=False):
        '''
        This will open information message box, licence finished. Yield operation will be used for unittests
        '''
        
        text = "Your license expired. Please contact with us to extend Your license"
        
        if not cmd:
            dialogWindows = DialogWindows(self.parent);
            dialogWindows.criticalWindow(text);
            
            yield dialogWindows
            dialogWindows.exec_();
        else:
            print(text);
   
    def mainStartupCheck(self):
        '''
        This is main license check part
        '''
        
        if self.checkLicenseCode():##first check if license code present.
            licenseCode = self.checkLicenseCode();
            #==================================================================
            # Put here code for enablig functionality based on given License Code
            #==================================================================
        else:##then check demo data license 
            log.debug(("Starting check of demo license"));
            for message in self.dataStartupCheck(): yield message;

class LicenseToTable():
    '''
    Operation on LicenceCode 
    '''
    licenseCodeToTextDict = {0:"Trial version.",
                             1:'Standard version.',
                             2:'Extended version with commandline.',
                             3:'Not defined.',
                             4:'Not defined.',
                             5:'Not defined.',
                             6:'Not defined.',
                             7:'Not defined.',
                             8:'Not defined.'};
    
    def __init__(self):
        pass;
    
    def licenseCodeToText(self, lc_decoded):
        '''
        This will translate given decoded license code to text description 
        *Input*
        :param lc_decoded: int(3) this is decode variable of license
        
        *Output*
        :param licenseTextList: False or list() this is list of all license code, ['Standard version', 'Extended version with commandline']
        
        
        '''
        try: 
            lc_decoded = int(lc_decoded);
        except: 
            log.exception(("Not able to translate lc_decodeo into int(), lc_decode=",lc_decoded));
            return False;
        
        if not self.licenseCodeToTextDict.has_key(lc_decoded):
            log.exception(("Given license code is not define, lc_decoded:",lc_decoded));
            return False
        
        licenseTextList = [self.licenseCodeToTextDict[lc] for lc in range(1, lc_decoded+1)];
        return licenseTextList;
