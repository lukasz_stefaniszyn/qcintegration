'''
Created on 01-07-2012

@author: lucst
'''
import sys
import win32api, win32con

from lib.logfile import Logger, whoami, callersname
log = Logger(loggername="lib.QClicence.registerReadWrite", resetefilelog=False).log;

import lib.QClicense.wmi as wmi


class RegisterException(Exception):
    def __init__(self, text):
        log.exception((callersname() + "(): "+ repr(text)))
        self.text = repr(text);
        pass
    def __str__(self):
        return self.text;

def getMotherboardSN():
    '''
    This will return motherboard SerialNumber, like J115LR1.CN701662240006
    '''
    wm = wmi.WMI()
    try: 
        winBaseBoard = wm.Win32_BaseBoard();
        sn = winBaseBoard[0].SerialNumber;
    except:
        raise RegisterException("Problem with reading motherboard SerialNumber")
        sn = None
        return sn
    
    sn = sn.strip().strip('.');
    return sn;



class Register(object):
    '''
    This will make operation on WinRegister
    '''
    __HKEY = win32con.HKEY_CURRENT_USER;
#    __Subkey = "Software\\QCinter";
    
    
    

    def __init__(self, subkey="Software\\Win32inter"):
        '''
        Constructor
        '''
        self.Subkey = subkey;
        self.__key = win32api.RegCreateKey(self.HKEY, self.Subkey);
        
        pass;
    
#----------------------------------------------------------------------------- 
    def get_hkey(self):
        return self.__HKEY
    def get_subkey(self):
        return self.__Subkey
    def set_subkey(self, value):
        self.__Subkey = value
        

    def get_key(self):
        return self.__key
    def del_key(self):
        win32api.RegFlushKey(self.__key);
        win32api.RegCloseKey(self.__key);
        self.__key = None;
 
    HKEY = property(get_hkey, None, None, "HKEY's docstring")
    Subkey = property(get_subkey, set_subkey, None, "Subkey's docstring")
    key = property(get_key, None, del_key, "key's docstring")
    
#----------------------------------------------------------------------------- 
    def readRegister(self, regname):
        '''
        This will read register value
        *Input*
        :param regname: str() of register name to read
        
        *Output*
        :param regvalue: str() register value
        :param regtype: int() value type
        '''
#        print "Name:", sys._getframe().f_code.co_name;
#        print "Name2:", whoami();
#        print "Name3:", callersname();
        
        regvalue= None;
        try:
            regvalue, regtype = win32api.RegQueryValueEx(self.key, regname);
        except Exception, error:
            log.exception(("Not able to read variable: %s. %s"%(regname, repr(error))));
#            raise RegisterException("Not able to read variable. "+repr(error));
        return regvalue;
    
    
    def writeRegister(self, regname, regvalue):
        '''
        This will write vgiven regvalue to the regname
        *Input*
        :param regname: str()
        :param regvalue: str() 
        
        *Output*
        :param True/False;
        '''
        if type(regvalue)!=type(str()):
            regvalue = str(regvalue);
        try: 
            win32api.RegSetValueEx(self.key, 
                                   regname, 
                                   None,
                                   win32con.REG_SZ, 
                                   regvalue);
        except Exception, error:
            log.exception(("Not able to write register. "+repr(error)));
#            raise RegisterException("Not able to write register. "+repr(error));
            return False;

        return True;
    
    def deleteRegisterVariable(self, regname):
        '''
        This will delete variable in key
        
        *Input*
        :param regname: str() this is variable name
        
        *Output*
        True/False
        '''
        try:
            win32api.RegDeleteValue(self.key, regname);
        except Exception, error:
            raise RegisterException("Not able to delete variable in key. "+repr(error));
            return False;
        return True;

    def deleteRegisterKey(self):
        '''
        This will delete main key of program
        
        *Output*
        True/False
        '''
#        del self.key; ##first close current key
        try:
            win32api.RegDeleteKey(self.HKEY, self.Subkey);
        except Exception, error:
            raise RegisterException("Not able to delete main key. "+repr(error));
            return False;
        return True;
    