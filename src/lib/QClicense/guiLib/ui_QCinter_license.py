# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'QCinter_license.ui'
#
# Created: Sat Dec 08 18:02:34 2012
#      by: PyQt4 UI code generator 4.9.1
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName(_fromUtf8("Form"))
        Form.resize(313, 303)
        self.gridLayout = QtGui.QGridLayout(Form)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.verticalLayout_2 = QtGui.QVBoxLayout()
        self.verticalLayout_2.setObjectName(_fromUtf8("verticalLayout_2"))
        self.label = QtGui.QLabel(Form)
        self.label.setObjectName(_fromUtf8("label"))
        self.verticalLayout_2.addWidget(self.label)
        self.horizontalLayout_4 = QtGui.QHBoxLayout()
        self.horizontalLayout_4.setObjectName(_fromUtf8("horizontalLayout_4"))
        self.label_LicenseCode = QtGui.QLabel(Form)
        self.label_LicenseCode.setAutoFillBackground(True)
        self.label_LicenseCode.setText(_fromUtf8(""))
        self.label_LicenseCode.setTextFormat(QtCore.Qt.AutoText)
        self.label_LicenseCode.setOpenExternalLinks(False)
        self.label_LicenseCode.setObjectName(_fromUtf8("label_LicenseCode"))
        self.horizontalLayout_4.addWidget(self.label_LicenseCode)
        spacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_4.addItem(spacerItem)
        self.verticalLayout_2.addLayout(self.horizontalLayout_4)
        self.listWidget_licenselist = QtGui.QListWidget(Form)
        self.listWidget_licenselist.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers)
        self.listWidget_licenselist.setProperty("showDropIndicator", False)
        self.listWidget_licenselist.setSelectionMode(QtGui.QAbstractItemView.NoSelection)
        self.listWidget_licenselist.setObjectName(_fromUtf8("listWidget_licenselist"))
        self.verticalLayout_2.addWidget(self.listWidget_licenselist)
        self.gridLayout.addLayout(self.verticalLayout_2, 0, 0, 1, 1)
        self.line_2 = QtGui.QFrame(Form)
        self.line_2.setFrameShape(QtGui.QFrame.HLine)
        self.line_2.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_2.setObjectName(_fromUtf8("line_2"))
        self.gridLayout.addWidget(self.line_2, 1, 0, 1, 1)
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        self.label_4 = QtGui.QLabel(Form)
        self.label_4.setObjectName(_fromUtf8("label_4"))
        self.horizontalLayout_2.addWidget(self.label_4)
        spacerItem1 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem1)
        self.gridLayout.addLayout(self.horizontalLayout_2, 2, 0, 1, 1)
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.lineEdit_LicenseCode = QtGui.QLineEdit(Form)
        self.lineEdit_LicenseCode.setText(_fromUtf8(""))
        self.lineEdit_LicenseCode.setObjectName(_fromUtf8("lineEdit_LicenseCode"))
        self.horizontalLayout.addWidget(self.lineEdit_LicenseCode)
        self.phb_addLicense = QtGui.QPushButton(Form)
        self.phb_addLicense.setObjectName(_fromUtf8("phb_addLicense"))
        self.horizontalLayout.addWidget(self.phb_addLicense)
        self.gridLayout.addLayout(self.horizontalLayout, 3, 0, 1, 1)
        self.line = QtGui.QFrame(Form)
        self.line.setFrameShape(QtGui.QFrame.HLine)
        self.line.setFrameShadow(QtGui.QFrame.Sunken)
        self.line.setObjectName(_fromUtf8("line"))
        self.gridLayout.addWidget(self.line, 4, 0, 1, 1)
        self.horizontalLayout_3 = QtGui.QHBoxLayout()
        self.horizontalLayout_3.setObjectName(_fromUtf8("horizontalLayout_3"))
        self.verticalLayout = QtGui.QVBoxLayout()
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.label_2 = QtGui.QLabel(Form)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.verticalLayout.addWidget(self.label_2)
        self.label_3 = QtGui.QLabel(Form)
        font = QtGui.QFont()
        font.setUnderline(True)
        self.label_3.setFont(font)
        self.label_3.setTextFormat(QtCore.Qt.RichText)
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.verticalLayout.addWidget(self.label_3)
        self.horizontalLayout_3.addLayout(self.verticalLayout)
        self.phb_Close = QtGui.QPushButton(Form)
        self.phb_Close.setObjectName(_fromUtf8("phb_Close"))
        self.horizontalLayout_3.addWidget(self.phb_Close)
        self.gridLayout.addLayout(self.horizontalLayout_3, 5, 0, 1, 1)

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        Form.setWindowTitle(QtGui.QApplication.translate("Form", "License Management", None, QtGui.QApplication.UnicodeUTF8))
        self.label.setText(QtGui.QApplication.translate("Form", "Active License:", None, QtGui.QApplication.UnicodeUTF8))
        self.label_4.setText(QtGui.QApplication.translate("Form", "Enter license code:", None, QtGui.QApplication.UnicodeUTF8))
        self.phb_addLicense.setText(QtGui.QApplication.translate("Form", "&Add", None, QtGui.QApplication.UnicodeUTF8))
        self.label_2.setText(QtGui.QApplication.translate("Form", "How to buy, please contact with:", None, QtGui.QApplication.UnicodeUTF8))
        self.label_3.setText(QtGui.QApplication.translate("Form", "www.QCinter.eu", None, QtGui.QApplication.UnicodeUTF8))
        self.phb_Close.setText(QtGui.QApplication.translate("Form", "&Close", None, QtGui.QApplication.UnicodeUTF8))

