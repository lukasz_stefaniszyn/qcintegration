'''
Created on 22-12-2012

@author: lucst
'''
from PyQt4 import QtGui
from lib.QClicense.guiLib.ui_QCinter_licensePolicy import Ui_Form as Ui_QcLicensePolicy 

from lib.logfile import Logger
log = Logger(loggername="lib.QClicense.qcLicense_licensePolicy", resetefilelog=False).log;


class GuiQcLicensePolicy(QtGui.QDialog, Ui_QcLicensePolicy):
    def __init__(self, parent):
        super(GuiQcLicensePolicy, self).__init__(parent)
        self.setupUi(self);

class QCLicensePolicy(QtGui.QWidget):
    '''
    Class for QCLicensePolicy window
    '''
    def __init__(self, parent=None):
        '''
        @param parent: QtWidget
        '''
        self.parent = parent;
        super(QCLicensePolicy, self).__init__(parent);
        self.guiQcLicensePolicy= GuiQcLicensePolicy(parent);
        
        self.guiQcLicensePolicy.show();