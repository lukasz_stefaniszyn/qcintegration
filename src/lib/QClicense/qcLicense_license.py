'''
Created on 11-08-2012

@author: lucst
'''
from PyQt4 import QtCore, QtGui

from lib.QClicense.generateCRC import LicenseCode, RegisterData,  DataStartupCondition
from lib.QClicense.checkLicence import DialogWindows
from lib.QClicense.guiLib.ui_QCinter_license import Ui_Form as Ui_QcLicense
from lib.logfile import Logger, whoami, callersname
from lib.QClicense import checkLicence
log = Logger(loggername="lib.QCLicense.qcLicense_license", resetefilelog=False).log;

class GuiQcLicense(QtGui.QDialog, Ui_QcLicense):
    '''This class is used to initialize, control
     TreeViews and buttons which are on the whole tab
    '''
    def __init__(self, parent):
        self.parent = parent
        ##Add Widget responsible for tab_CrossTest
        ##This initialize QcGraph tab Cross Test  
        super(GuiQcLicense, self).__init__(self.parent)
        self.setupUi(self);

    def get_line_edit_license_code(self):
        text = str(self.lineEdit_LicenseCode.text());
        self.lineEdit_LicenseCode.clear();
        return text

    def set_line_edit_license_code(self, value):
        try:
            value = str(value);
        except: 
            log.exception("Unable to write to lineEdit_LicenseCode");
            return
        self.lineEdit_LicenseCode.setText(value)
    lineEditLicenseCodeProp = property(get_line_edit_license_code, set_line_edit_license_code, None, "lineEditLicenseCodeP's docstring")

    def get_label_license_code(self):
        return str(self.label_LicenseCode.text())
    def set_label_license_code(self, value):
        '''
        Update line, which will print license code
        *Input*
        :param value: str(), license code "6bea138fc0a7210455fbc5e3"
        '''
        self.label_LicenseCode.setText(value);
    def del_label_license_code(self):
        self.label_LicenseCode.setText('');
    labelLicenseCodeProp = property(get_label_license_code, set_label_license_code, del_label_license_code, "Label for LicenseCode")


    def addItemToList(self, name):
        '''
        Add new item to widget list
        :param name: str()  item name
        '''
        self.listWidget_licenselist.addItem(QtCore.QString(name));
        
    def removeItemFromList(self):
        '''
        
        Remove all items from widget list
        '''
        
        self.listWidget_licenselist.clear();

    def listItems(self):
        '''
        This will list all items saved in ListWidget
        *Input*
        None
        
        *Output*
        :param listItem: list(QListWidgetItem) this will be QListWidgetItem
        '''
        listItems = [];
        
        for row in range(self.listWidget_licenselist.count()):
            listItems.append(self.listWidget_licenselist.item(row));
        return listItems;
    
    def listItemsText(self):
        '''
        This will list all items saved in ListWidget writen in str() format
        *Input*
        None
        
        *Output*
        :param listItem: list(str('Standard License'))
        '''
        listItems = [];
        
        for QListWidgetItem in self.listItems():
            listItems.append(str(QListWidgetItem.text()));
        return listItems;
    
    def messageboxInvalidLicense(self):
        '''
        This will open information message box, licence invalid. Yield operation will be used for unittests
        '''
        dialogWindows = DialogWindows(self.parent);
        text = "Your license code is invalid. Please correct or contact with www.QCinter.eu"
        dialogWindows.criticalWindow(text);
        dialogWindows.show();
        return dialogWindows;

    
        
        

class QcLicense(QtGui.QWidget):
    '''
    This class collects all information for Gui, when user will start selecting fields to download
    '''


    
    def __init__(self, parent=None):
        '''
        @param parent: QtWidget
        '''
        self.parent = parent;
        super(QcLicense, self).__init__(parent);
        self.guiQcLicense = GuiQcLicense(parent);

        self.statusLicense = False
        ##Add new license code  
        self.guiQcLicense.phb_addLicense.clicked.connect(self.pushButton_addLicense);
        self.guiQcLicense.phb_Close.clicked.connect(self.pushButton_close);
        
        ##Read current LicenseCode from register
        self.getCurrentLicenseCode()
        
        self.guiQcLicense.show();
        
    def pushButton_close(self):
        '''
        Close window License Management
        '''
        log.debug("Close License Management");
        self.guiQcLicense.close();
    
    
    def pushButton_addLicense(self):
        log.debug(("pushButton_addLicense()"));
        
        
        lc = self.guiQcLicense.lineEditLicenseCodeProp; ##read given LicenseCode from lineEdit_LicenseCode
        
        licenseCode = LicenseCode();
        decoded = licenseCode.readLicense(lc)
        
        if not decoded: 
            log.debug(("License code is invalid"));
#            ##Open information message box. Yield is used for unittests
            message = self.guiQcLicense.messageboxInvalidLicense();
            self.statusLicense = False;
            return message;
        else: 
            self.statusLicense = True;
            try: self.parent.userLicense(True);
            except: log.debug("Unable to enable ConnectionButton when good license is activated")
            
        
        ##Write to register given license
        registerReadWrite = RegisterData();
        registerReadWrite.reg_licenseCode = lc;
        
        
        licenseTextList = checkLicence.LicenseToTable().licenseCodeToText(decoded);
        if not licenseTextList: ##if given LicenseCode decoded value is not int() or out of range, then finish
            return 
        else:
            ##Remove every license from ListWidget, just to be able write the new variables
            del self.guiQcLicense.labelLicenseCodeProp
            self.guiQcLicense.removeItemFromList();
            
            ##Get license text name 
            self.guiQcLicense.labelLicenseCodeProp= lc; ##on first place will be user license code which he write in gui
            ##Save license text names into the ListWidget
            for name in licenseTextList:
                self.guiQcLicense.addItemToList(name);
            
    def getCurrentLicenseCode(self):
        '''
        This will read current License code from register
        '''
        ##Get license text name 
        lc = RegisterData().reg_licenseCode; ##on first place will be user license code which he write in gui
        if lc is None:
            decoded = 0 ##This is Trial version
            expirationDays = DataStartupCondition().diffExpirationDays();
            text = " Left %d day(s)"%(expirationDays);
            licenseTextList = [checkLicence.LicenseToTable.licenseCodeToTextDict[decoded] +\
                            text];
        else: 
            self.guiQcLicense.labelLicenseCodeProp = lc; ##on first place will be user license code which he write in gui
            decoded = LicenseCode().readLicense(lc)
            licenseTextList = checkLicence.LicenseToTable().licenseCodeToText(decoded);
            if not licenseTextList: ##if given LicenseCode decoded value is not int() or out of range, then finish
                return 
        ##Save license text names into the ListWidget
        for name in licenseTextList:
            self.guiQcLicense.addItemToList(name);
            
        
        
        