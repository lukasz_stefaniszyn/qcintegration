'''
Created on 07-07-2012

@author: lucst
'''
import sys
import os
import time
import datetime


import lib.QClicense.registerReadWrite as registerReadWrite

from lib.logfile import Logger, whoami, callersname
log = Logger(loggername='lib.QClicence.generateCRC', resetefilelog=False).log;



    

def _datetime():
    '''
    Return digital number of current date and time
    '''
    datetime = time.time();
    try:
        datetimeInt = int(datetime);
    except Exception, err: 
        log.exception(("Unable to convert datetime %s into integer with error: %s", (str(datetime), repr(err))));
        datetimeInt = 0;
    return datetimeInt;

class CalcCRC(object):
    '''
    This will calculate CRC sum for licence
    '''
    md5= '6a8f8df8'

    def __init__(self, licenceCode=None, HW_ID=None):
        '''
        Constructor
        '''
        self.__licenceCode = licenceCode;
        self.__HW_ID = HW_ID
        
        self.username = self.getUserName();

    def get_username(self):
        return self.toHex(self.__username);
    def set_username(self, value):
        self.__username = value
    username = property(get_username, set_username, None, "username's docstring")

    def get_hw_id(self):
        hwId=self.toHex(self.__HW_ID);
        return hwId 
    def del_hw_id(self):
        del self.__HW_ID
    HW_ID = property(get_hw_id, None, del_hw_id, "HW_ID's docstring")
        
    def get_licence_code(self):
        return self.__licenceCode
    def del_licence_code(self):
        del self.__licenceCode
    licenceCode = property(get_licence_code, None, del_licence_code, "licenceCode's docstring")

    def toHex(self, s):
        #convert string to hex
        try: value = s.encode("hex");
        except: value = '1'; 
        return value

    
    def toStr(self, s):
        #convert hex repr to string
        return s.decode("hex")
    
    def getUserName(self):
        '''
        This will return current user name
        '''
        return os.getenv('USERNAME');
    
    
    def calculateBasicCRC(self):
        '''
        Very basic calculation for common CRC calculation part.
        With usrname and md5
        '''
        return int(self.username, 16) + int(self.md5, 16);
    
    def calculate(self):
        
        #Check if HW_ID value is needed 
        hwId = self.HW_ID;

        #Check if license key is available
        licenceCode = self.licenceCode;
        if licenceCode is None: 
            date = _datetime();
            crc = date + self.calculateBasicCRC(); 
        else:
            licenceCode=self.toHex(licenceCode);    
            crc = int(hwId, 16) + int(licenceCode, 16) + self.calculateBasicCRC();  
        return crc


class BasicDataLicence(CalcCRC):
    
    
    
    def __init__(self, datetimeNow=None):
        '''
        
        :param datetimeNow: this parameter is used in unittest
        '''
        CalcCRC.__init__(self);
        
        if datetimeNow is None:
            self.nowData = datetime.datetime.now();
        else:
            self.nowData = datetimeNow;
            
        self.listCRCData = self.listData();
        pass;
    
    def translateDateTimetoTime(self, datetimeValue):
        '''
        Translate datetime format into the floating point number, expressed in seconds 
        
        *input*
        :param datetimeValue: object, example   datetime.datetime(2012, 7, 12, 21, 17, 33, 402000)
        
        *output*
        :param int(time.time()) , like  1234886618
        '''
        
        structure = datetimeValue.timetuple();
        time_ = time.mktime(structure)
        
        try:
            datetimeInt = int(time_);
        except Exception, err: 
            log.exception(("Unable to convert time %s into integer with error: %s", (str(time_), repr(err))));
            datetimeInt = 0;
        return datetimeInt;
        
    def genCrcListData(self, diffDate):
        '''
        Generate one of list data crc.   
        
        *Input*
        :param diffDate: inst(datatime), datetime instance after calculation of current +- some days
        
        *Output*
        int()  calculated one crc for this +- day to current date
           
        '''
        time = self.translateDateTimetoTime(diffDate);##translate datetime into the floating point number expressed in seconds
        return time + self.calculateBasicCRC();
    
    def timeToDatetime(self, time_):
        '''
        Translate int() time format into datetime() format
        :param time_: int()  example = 1342120653
        '''
        return datetime.datetime.fromtimestamp(float(time_));
    
    def uncodeCrcBasicDate(self, crcDate):
        '''
        This is for decode given int time() with coded time+username+dm5 format 
        into datetime() format without  md5+username
        
        *Input*
        :param crcDate: int()   example = 1342120653
        
        *Output*
        :param datetime_: datetime()   example = datetime.datetime(2012, 7, 12, 21, 17, 33, 402000)
        '''
        
        time_ = crcDate - self.calculateBasicCRC();
        return self.timeToDatetime(time_)
        
        
    
    def listData(self):
        '''
        Generate all list of generated crc data for current date  -31days and +31days.
        
        *Input*
        None
        
        *Output*
        list()  of crc data, like [12343, 34075123]
        
        
        '''
        listData = [];
        
        for diff_day in [-30, +31]:##take -31, till +31 days calculated to current date
            diff_datetime = datetime.timedelta(days=diff_day);
            diffDate = self.nowData+diff_datetime;##give target date after modification -31, .. , +31 days 
            diffDate.replace(hour=0, minute=0, second=1); ##this will give information for tillWhen variable, to check the day instead of date and time
            listData.append(self.genCrcListData(diffDate));
            
        return listData;
            
class LicenseCode(object):

    def __init__(self):
    
        self.__base = "2bu1!h0CS"

        
    def __hex(self, value):
        #convert int to hex
        return hex(value).rstrip("L").lstrip("0x");
    
    def __intToBin(self, value):
        '''
        convert int to bin but keep 3 digit as the result
        *Input*
        :param value: int(3)
        
        *Output*
        str(011) OR False
        '''
        #
        if type(value)!=int:
            log.exception(("Given value:%s is not str")%value);
            return False
        if value> 8:
            log.exception(("Given value of license code is bigger than possible binary format int(8, 2)"));
            return False
        
        binValue = ['0']*3;
        i=len(binValue)-1;
        for v in reversed(bin(value).lstrip("0b")):
            binValue[i]=v;
            i-=1;
        return ''.join(binValue)
    
    def __binToInt(self, binValue):
        '''
        convert bin to int 
        *Input*
        :param binValue: str(011)
        
        *Output*
        int(3) OR  False
        '''
        if type(binValue)!=str:
            log.exception(("Given binValue:%s is not str")%binValue);
            return False

        try: 
            value = int(binValue, 2);
        except ValueError, err: 
            log.exception(("Given binValue:%s is not in bin format. %s")%(binValue,err));
            return False
        
        return value;
    
    def __toHex(self, s):
        '''
        Convert string to hex
        :param s:
        '''
        
        return s.encode("hex")

    
    def __toStr(self, s):
        '''
        Convert hex repr to string
        :param s:
        '''
        return s.decode("hex")

    def __gettime(self):
        '''
        Get current time, which will be used in license code generation
        
        *Outpur*
        :param str(date_) - this str() of current time, like str(094459)
        '''
        date_ = datetime.datetime.now();
        return date_.time().strftime("%H%M%S");
        
    def __stuffCode(self, code):
        '''
        Create mix of current datetime and the license code
        *Input*
        :param code: int(3) code for license
        
        *Output*
        str('006315414') time stuffed with license bin code
        '''
        
        strdate_ = self.__gettime();
        licenseNumber = self.__intToBin(code);
        
        license = ['']*9;
        j=0;g=0;prev=0;
        for i in range(9):
            if (i==1) or (prev+3==i):
                try: value=licenseNumber[g];
                except: value='0';
                license[i]=value;
                prev = i;
                g+=1;
            else:
                license[i]=strdate_[j];
                j+=1;
        return ''.join(license);

    def __unstuffCode(self, license):
        '''
        Decode current datetime and the license code
        *Input*
        :param license: str('006315414') time stuffed with license bin code
        
        
        *Output*
        int(3) code for license
        '''
        
        
        codeValue=''; strdate='';
        prev=0;
        for i in range(len(license)):
            if (i==1) or (prev+3==i):
                codeValue += license[i];
                prev = i;
            else:
                strdate += license[i];
        
        try: 
            strdate = datetime.datetime.strptime(strdate, '%H%M%S');
        except:
            log.debug(("Given license code is not in datetime format: %s"%repr(license)));
            return False
        
            
        ##this will return int() value of used license type or False value
        licenseNumber = self.__binToInt(codeValue);
        return licenseNumber;
    
    def makeLicense(self, tocode):
        '''
        Generate license code for given enabled functions
        
        *Input*
        :param tocode: int() this value which enabled code for functions, like int(3) 
        Value can not be bigger that 8. Value is translated into bin() code. 
        bin(tocode)[0] - license standard (1) or 30days (0) 
        bin(tocode)[1] - license extended (1) or not (0)
        bin(tocode)[2] - not decided. For future use 
        
        *Output*
        :param licenseCode: int(16),  like 97bf2b574f6b3a8be4c6936149600b64c89
         
        '''
        license = self.__stuffCode(tocode);
        orginalHex = self.__toHex(license);
        baseHex = self.__toHex(self.__base);
        makeHex = self.__hex( (int(orginalHex, 16) * int(baseHex, 16)) );
        return makeHex;
        
    def readLicense(self, licenseCode):
        '''
        Read license for given license code 
        
        *Input*
        :param licenseCode: int(16),  like 97bf2b574f6b3a8be4c6936149600b64c89
        
        *Output*
        :param readIntLicense: int() this value which enabled code for functions, like int(3) 
        Value can not be bigger that 8. Value is translated into bin() code. 
        bin(tocode)[0] - license standard (1) or 30days (0) 
        bin(tocode)[1] - license extended (1) or not (0)
        bin(tocode)[2] - not decided. For future use
        '''
    
        baseHex = self.__toHex(self.__base);
        hesloHex = licenseCode.strip();
        if hesloHex == "":
            return False
#        readHex = self.hex( (int(hesloHex, 16)) - int(baseHex, 16));
        readHex = ( (int(hesloHex, 16)) / int(baseHex, 16));
        licenseCode = self.__hex(readHex) ##this is hex value -> we need to go to int() value
        licenseCode = self.__toStr(licenseCode);
        readIntLicense = self.__unstuffCode(licenseCode);
        return readIntLicense;

class RegisterData(registerReadWrite.Register):
    
    def __init__(self):
        self.register = registerReadWrite.Register();
        pass;

    def get_reg_run_date(self):
        return self.__getRegisterValueInt(regname='runDate')
    def set_reg_run_date(self, regvalue):
        self.register.writeRegister(regname='runDate', regvalue=regvalue);
    reg_runDate = property(get_reg_run_date, set_reg_run_date, None, "reg_runDate's docstring")

    def get_reg_instal_date(self):
        return self.__getRegisterValueInt(regname='instalDate')
    def set_reg_instal_date(self, regvalue):
        self.register.writeRegister(regname='instalDate', regvalue=regvalue);
    reg_instalDate = property(get_reg_instal_date, set_reg_instal_date, None, "reg_instalDate's docstring")

    def get_reg_till_when(self):
        return self.__getRegisterValueInt(regname='tillWhen')
    def set_reg_till_when(self, regvalue):
        self.register.writeRegister(regname='tillWhen', regvalue=regvalue);
    reg_tillWhen = property(get_reg_till_when, set_reg_till_when, None, "reg_tillWhen's docstring")
    
    def get_reg_license_code(self):
        regvalue = self.__getRegisterValueStr(regname='lc')
        if regvalue is not None:
            try: regvalue = regvalue[1:-1];##this is used for not easy string finding in regedit
            except: 
                log.exception(("Unable to edit license code=%s")%repr(regvalue));
                return None
            if regvalue=='':
                return None;
        return regvalue
    def set_reg_license_code(self, regvalue):
        if type(regvalue)==type(str()):
             regvalue = '9'+regvalue+'4';##this is used for not easy string finding in regedit  
        self.register.writeRegister(regname='lc', regvalue=regvalue);
    reg_licenseCode = property(get_reg_license_code, set_reg_license_code, None, "reg_licenseCode's docstring")
        
    def __getRegisterValueInt(self, regname):
        regvalue = self.register.readRegister(regname)
        try: regvalue = int(regvalue);
        except TypeError:
            log.debug(("Variable %s was None")%(regname));
            return None
        except Exception:
            raise registerReadWrite.RegisterException(("Register: %s with value %s was not able to convert to int()")%(regname, repr(regvalue)))
            return None
        else: ##if there will be no exception then return int(regvalue)
            return regvalue
    
    def __getRegisterValueStr(self, regname):
        regvalue = self.register.readRegister(regname);
        if regvalue is None:
            log.debug(("Variable %s was None")%(regname));
            return None
        try: regvalue = str(regvalue);
        except Exception:
            raise registerReadWrite.RegisterException(("Register: %s with value %s was not able to convert to str()")%(regname, repr(regvalue)))
            return None
        else: ##if there will be no exception then return str(regvalue)
            return regvalue
        
    def __setRegisterValueStr(self, regname, regvalue):
        if not self.register.writeRegister(regname, regvalue):
            return False;
        else:
            return True;


class DataStartupCondition(RegisterData, BasicDataLicence, LicenseCode):
    
    
    
    def __init__(self, datetimeNow=None):
        '''
        
        :param datetimeNow: this parameter is used unittest
        '''
        BasicDataLicence.__init__(self, datetimeNow);
        LicenseCode.__init__(self);
        RegisterData.__init__(self);

        
    def readLicenseCode(self):
        '''
        Read license code from register
        
        *Input*
        None
        
        *Output*
        False OR int(licenseCode), like int(3)
        '''
        
        licenseCode=self.reg_licenseCode
        if licenseCode is None:
            return False
        
        license = self.readLicense(licenseCode)
        if not license:
            return False;
        else:
            return license;
    
    
    def diffExpirationDays(self, time_tillWhen=None, datetime_now=None):
        '''
        This will calculate how many days have left for expiration date
        
        *Input*
        :param datetime_tillWhen: time() or if None, then tillWhen register value will be used 
        :param datetime_now: datetime() or if None then current date will be used
        
        *Output*
        int()  number of days which left to licence expirate
        '''
        if time_tillWhen is None:
            if self.reg_tillWhen is None:
                return 0 ##if register value tillWhen is None then return 0 days in expiration   
            else: 
                time_tillWhen = self.reg_tillWhen;
        
        datetime_tillWhen = self.uncodeCrcBasicDate(time_tillWhen);##time() with md5+username of expiration has been translated into datetime() format
        log.debug(("tillWhen expiration datetime:",str(datetime_tillWhen)));
        
        if datetime_now is None:
            datetime_now = self.nowData;
        
        diff = datetime_tillWhen - datetime_now;
        diffdays = diff.days;
        log.debug(("Expiration days=%d"%(diffdays)));
        
        
        if diffdays<0: diffdays=0; 
        return diffdays;
    
    def checkRegisterValue(self):
        '''
        Check first step. Register variable 
        "date till when"(tillWhen) and 
        "instalation date" (instalDate) and 
        "last running data" (runData) 
        must be in the rage of calculated list CRC date  
        '''
        
        status = False;

        dictRegValue = {'tillWhen':self.reg_tillWhen, 
                        'instalDate':self.reg_instalDate,
                        'runDate':self.reg_runDate
                        };
        
        for regname, regvalue in dictRegValue.items():
            if regvalue is None:
                return False
            if self.listCRCData[0] < regvalue < self.listCRCData[-1]:
                status = True;
            else: 
                status = False;
                log.debug(("Variable %s was out of given range or it was None")%(regname));
                return status;

        return status;
                
    def calculateExpirationDate(self):
        '''
        This will calculate how many days left till licence will expire 
        '''
        currentDate = self.nowData
        val2=self.reg_tillWhen + currentDate; ##sum expiration date (tillWhen) with current date
        
    
    def checkIfDataChanged(self):
        '''
        Check if local computer data was not changed to workaround the licence, 
        like changing local clock. Base value in this calculation will be 
        last time run data (runDate) 
        '''
        
        if (self.reg_runDate and self.reg_tillWhen and self.reg_instalDate) is None:
            return False;

        currentDate = self.genCrcListData(self.nowData)
        log.debug(("Current date value:", currentDate));
        val1=self.reg_runDate ##sum last time run data (runDate) with expiration date (tillWhen) 
        val2=currentDate; ##sum expiration date (tillWhen) with current date
        log.debug(("Val1: ",val1));
        log.debug(("Val2: ",val2));
        
        if val1 <= val2:
            self.register.writeRegister(regname='runDate', regvalue=str(currentDate)); ##if data check is positive, then update new value for last time run data (runDate) 
            return True;
        else:
            return False;

class DataInstallationCondition(RegisterData, BasicDataLicence, LicenseCode):
    
    trialDays = 30;
    
    def __init__(self, datetimeNow=None):
        '''
        
        :param datetimeNow: this parameter is used unittest
        '''
        BasicDataLicence.__init__(self, datetimeNow);
        LicenseCode.__init__(self);
        RegisterData.__init__(self);
    
    
    def registerInstallionCheck(self):
        '''
        Check if all register reg_runDate, reg_tillWhen and runDate  are None. 
        If yes, then this is first startup after instalation
        If not, this is normal startup
        
        *Input*
        None
        
        *Output*
        bool() = True - this is after installation startup
                False - this is normal startup
        '''

        if (self.reg_runDate is None) and\
            (self.reg_tillWhen is None) and\
            (self.reg_instalDate is None):
            log.debug("ALl reg_runDate, reg_tillWhen and runDate values are  None");
            return True;
        else:
            return False;

    
    def updateRegisterOnInstallation(self):
        '''
        This will create registers reg_runDate, reg_tillWhen and reg_runDate
        
        *Input*
        None
        
        *Output*
        None
        '''
        
        ##Calculate tillWhen date
        tillWhenDate = self.nowData + datetime.timedelta(days=self.trialDays);
        
        
        tillWhen = ('tillWhen', tillWhenDate);
        instalDate = ('instalDate', self.nowData);
        runDate = ('runDate', self.nowData);
        for regname, datetime_ in [tillWhen, instalDate, runDate]:
            regvalue = self.genCrcListData(datetime_);
            self.register.writeRegister(regname, regvalue);
        
    
        