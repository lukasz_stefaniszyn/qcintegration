'''
Created on 11-08-2012

@author: lucst
'''
from __future__ import absolute_import
from PyQt4 import QtCore, QtGui, Qt

from lib.QClicense.checkLicence import DataStartupLicenceCheck
from lib.QClicense.generateCRC import LicenseCode, RegisterData, DataStartupCondition
from lib.QClicense.qcLicense_license import QcLicense
import datetime
import sys
import unittest
import os
from lib.QClicense import checkLicence, generateCRC




app = QtGui.QApplication(sys.argv)

class Test(unittest.TestCase):

    registerData = generateCRC.RegisterData();

    def setUp(self):
        self.gui = QcLicense();
        
        ##Remove every license from ListWidget, just to be able write the new variables
        self.gui.guiQcLicense.removeItemFromList();


    def tearDown(self):
        self.gui.close();
        ##clear register
        try: self.registerData.register.deleteRegisterKey();
        except: pass;
        
        


    def testListWidget_1(self):
        '''
        Check if created ListWidget is empty
        '''
        self.assertEqual(self.gui.guiQcLicense.listWidget_licenselist.count(),0);
    
    def testListWidget_2(self):
        '''
        Test adding item to the ListWidget
        '''
        name = 'text';
        self.gui.guiQcLicense.addItemToList(name);
        
        itemFound = self.gui.guiQcLicense.listWidget_licenselist.findItems(name, Qt.Qt.MatchExactly);
        self.assertTrue(len(itemFound)==1, "Item was not found or there is more items than are matching")
        self.assertEqual(name, itemFound[0].text());
        
        self.assertEqual(self.gui.guiQcLicense.listWidget_licenselist.count(),1);

    def testListWidget_3(self):
        '''
        Test if clearin list is working
        '''
        name = 'text';
        self.gui.guiQcLicense.addItemToList(name);
        
        itemFound = self.gui.guiQcLicense.listWidget_licenselist.findItems(name, Qt.Qt.MatchExactly);
        self.assertTrue(len(itemFound)==1, "Item was not found or there is more items than are matching")
        self.assertEqual(name, itemFound[0].text());
        
        self.gui.guiQcLicense.removeItemFromList();
        self.assertEqual(self.gui.guiQcLicense.listWidget_licenselist.count(),0);
    
    def testListWidget_4(self):
        '''
        Test if listItem is working
        '''
        listName = ['text', 'test1', 'text3'];
        for name in listName:
            self.gui.guiQcLicense.addItemToList(name);
        
        listitemFound = self.gui.guiQcLicense.listItemsText();
        self.assertEqual(listitemFound, listName);
    
    def test_updateLicenseWidget(self):
        '''
        Update license widget list
        '''
        datetimeNow = datetime.datetime(2012, 8, 2, 21, 17, 33, 0);
        dataStartupLicenceCheck = DataStartupLicenceCheck(datetimeNow=datetimeNow);
        
        
        licenseCode = dataStartupLicenceCheck.checkLicenseCode();
        if licenseCode:
            print "License code", licenseCode;
        
        
        if dataStartupLicenceCheck.checkRegisterValue():
            if dataStartupLicenceCheck.checkIfDataChanged():
                print "License will expire in";
            else:
                print "License expired";
        else:
            print "License expired";
    
    def test_pshButtonAdd_1_OK(self):
        '''
        test how the pushButton_Add will work
        '''
        
        lc = "97bf2e7a50a6bea138fc0a7210455fbc5e3"
        self.gui.guiQcLicense.lineEditLicenseCodeProp = lc
        
        self.gui.guiQcLicense.phb_addLicense.click(); ##Emit click signal on button
        self.assertTrue(self.gui.statusLicense);
        
    def test_pshButtonAdd_2_OK(self):
        '''
        test how the pushButton_Add will work. Check if, read from lineEdit, check LicenseCode, write to register and confirm if saving user license code to register is ok in next reading
        '''
        
        lc = "97bf2e7a50a6bea138fc0a7210455fbc5e3"
        self.gui.guiQcLicense.lineEditLicenseCodeProp = lc
        
        
        self.gui.guiQcLicense.phb_addLicense.click(); ##Emit click signal on button
        self.assertTrue(self.gui.statusLicense);##check if license code is correct
        
        
        ##correct if saved to register LicenseCode is ok 
        dataStartupCondition = DataStartupCondition();
        decode = dataStartupCondition.readLicenseCode();
        self.assertEqual(decode, 3)

    def test_pshButtonAdd_3_OK(self):
        '''
        test how the pushButton_Add will work. Check if, read from lineEdit, check LicenseCode, write to register and confirm if saving user license code to register is ok in next reading
        '''
        
        lc = "97bf2e7a50a6bea138fc0a7210455fbc5e3"
        self.gui.guiQcLicense.lineEditLicenseCodeProp = lc
        
        
        self.gui.pushButton_addLicense(); ##Emit click signal on button
        self.assertTrue(self.gui.statusLicense);##check if license code is correct
        
        
        ##correct if saved to register LicenseCode is ok 
        dataStartupCondition = DataStartupCondition();
        decode = dataStartupCondition.readLicenseCode();
        self.assertEqual(decode, 3)

        ##check if Lable for LicenseCode is read correctly
        lc_cmp = self.gui.guiQcLicense.labelLicenseCodeProp
        self.assertEqual(lc, lc_cmp);

        ##check if translated license code to text is correctly saved in ListWidget
        listItems_cmp = self.gui.guiQcLicense.listItemsText();
        listItems=[checkLicence.LicenseToTable.licenseCodeToTextDict[i] for i in range(1, decode+1)];
        self.assertEqual(listItems, listItems_cmp);
        
    def test_pshButtonAdd_2_NOK(self):
        '''
        test how the pushButton_Add will work
        '''
        
        lc = "97bf2e7b50a6bea138fc0a7210455fbc5e3"
        self.gui.guiQcLicense.lineEditLicenseCodeProp = lc
        
        message = self.gui.pushButton_addLicense();
        text = "Your license code is invalid. Please correct or contact with www.QCinter.eu"
        self.assertEqual(message.text(), text)
        self.assertEqual(message.icon(), QtGui.QMessageBox.Critical);
        QtCore.QTimer.singleShot(0.5*1000, message, QtCore.SLOT('accept()')); ##this will push OK button after 5 sec
        self.assertFalse(self.gui.statusLicense);
    def test_getCurrentLicenseCode_1_OK(self):
        '''
        check if getCurrentLicenseCode is reading in correct way register LicenseCode.
        LicenseCode is saved in register
        '''
        
        ##Write register value for LicenseCode
        lc = "97bf2e7a50a6bea138fc0a7210455fbc5e3"
        generateCRC.RegisterData().reg_licenseCode=lc;
        
        
        self.gui.getCurrentLicenseCode();
        
        ##correct if saved to register LicenseCode is ok 
        decode = DataStartupCondition().readLicenseCode();
        

        ##check if Lable for LicenseCode is read correctly
        lc_cmp = self.gui.guiQcLicense.labelLicenseCodeProp
        self.assertEqual(lc, lc_cmp);
        
        ##check if translated license code to text is correctly saved in ListWidget
        listItems_cmp = self.gui.guiQcLicense.listItemsText();
        listItems=[checkLicence.LicenseToTable.licenseCodeToTextDict[i] for i in range(1, decode+1)];
        self.assertEqual(listItems, listItems_cmp);
        
    def test_getCurrentLicenseCode_2_OK(self):
        '''
        check if getCurrentLicenseCode is reading in correct way register LicenseCode
        LicenseCode is NOT saved in register 
        '''
        
        ##Delete register values
        registerData = generateCRC.RegisterData();
        registerData.register.deleteRegisterKey();
        
        self.gui.getCurrentLicenseCode();
        
        ##correct if saved to register LicenseCode is ok 
        decoded = DataStartupCondition().readLicenseCode();
        
        ##check if translated license code to text is correctly saved in ListWidget
        listItems_cmp = self.gui.guiQcLicense.listItemsText()[0];
        listItems = checkLicence.LicenseToTable().licenseCodeToTextDict[decoded];
        print listItems, listItems_cmp; 
        self.assertTrue(listItems in listItems_cmp);

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()