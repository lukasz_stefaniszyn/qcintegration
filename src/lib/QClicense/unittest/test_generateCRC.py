'''
Created on 01-07-2012

@author: lucst
'''
import unittest
import win32api, win32con
from functools import partial
import re
import time
import datetime
import os

import lib.QClicense.generateCRC as generateCRC 
from lib.QClicense import registerReadWrite



class TestCaclCrc(unittest.TestCase):


    def setUp(self):
        self.calcCRC = generateCRC.CalcCRC();

    def tearDown(self):
        pass;
    
    def test_getMotherboardSN(self):
        '''
        Check if SerialNumber of motherboard is read correctly
        '''
        sn = registerReadWrite.getMotherboardSN();
        comp = re.compile(r'([0-9A-Za-z\.]+)');
        self.assertEqual(comp.match(sn).group(1) , sn);

    def test_datetime(self):
        '''
        Check if time in int() is calculated correctly
        '''
        datetimeNow = int(time.time());
        
        datetimeCalc = generateCRC._datetime();
        
        diff = datetimeNow - datetimeCalc;
        self.assertFalse(diff>1);##different of more than 1sec then raise Fail
        self.assertTrue(diff<=1);##different of less than 1sec then raise True 
        
    def test_getUserName(self):
        '''
        Check if getting user name is ok
        '''
        
        username = self.calcCRC.getUserName();
        self.assertEqual(self.calcCRC.username, self.calcCRC.toHex(username));
        
        self.assertEqual(os.getenv('USERNAME'), username);

    def test_calculationOnlyDate(self):
        '''
        check calculation for no licence, only date calculation
        '''
        
        date = generateCRC._datetime();
        crc_cmp = date + self.calcCRC.calculateBasicCRC();
#        print crc_cmp 
        
        self.assertEqual(crc_cmp, self.calcCRC.calculate(), "Given calculation of CRC for non licence type, only data, failed");
        
    def test_calculationWithStandardLicence_1(self):
        '''
        Check calculation for standard licence
        HwID is not None
        Licence is not None
        '''
        hwId = registerReadWrite.getMotherboardSN();
        licenceCode = '4733bda';
        
        calcCrc = generateCRC.CalcCRC(licenceCode, HW_ID=hwId);
        
        hwId = calcCrc.toHex(hwId);
        licenceCode = calcCrc.toHex(licenceCode);
        crc_cmp = int(hwId, 16) + int(licenceCode, 16) + calcCrc.calculateBasicCRC();
        crc = calcCrc.calculate();
        self.assertTrue(crc>0); ##check if calculation does not give 0 value
        self.assertEqual(crc_cmp, crc);
        
        
    def test_calculationWithStandardLicence_2(self):
        '''
        Check calculation for standard licence
        HwID is None
        Licence is not None
        '''
        hwId = None;
        licenceCode = '4733bda';
        
        calcCrc = generateCRC.CalcCRC(licenceCode, HW_ID=hwId);
        
        hwId = calcCrc.toHex(hwId);
        licenceCode = calcCrc.toHex(licenceCode);
        crc_cmp = int(hwId, 16) + int(licenceCode, 16) + calcCrc.calculateBasicCRC();
        crc = calcCrc.calculate();
        self.assertTrue(crc>0);##check if calculation does not give 0 value

        self.assertEqual(crc_cmp, crc);

class TestBasicDataLicence(unittest.TestCase):
    
    def setUp(self):
        self.basicDataLicence = generateCRC.BasicDataLicence();

    def tearDown(self):
        pass;
    
    def test_translateDateTimetoTime(self):
        '''
        Test translate datetime format into the floating point number, expressed in seconds 
        '''
        datetimeInt_cmp = 1342120653;
        
        datetimeValue = datetime.datetime(2012, 7, 12, 21, 17, 33, 402000);
        datetimeInt = self.basicDataLicence.translateDateTimetoTime(datetimeValue)
        
        self.assertEqual(datetimeInt_cmp, datetimeInt, "Translation from datetime() format to time() did not finished with success")

    def test_genCrcListData(self):
        '''
        Test genaration for one CRC, base on datetime and md5  
        '''
        crc_cmp = 468955833401;
        
        diffDate = datetime.datetime(2012, 7, 12, 21, 17, 33, 402000);
        crc = self.basicDataLicence.genCrcListData(diffDate)
        
        self.assertEqual(crc_cmp, crc);
        
    
    def test_timeToDatetime(self):
        '''
        Test translate int() time format into datetime() format
        '''
        datetime_ = datetime.datetime(2012, 9, 10, 21, 17, 33, 402000);
        time_ = int(time.mktime(datetime_.timetuple()));  ##change datetime() format into time() format
        
        translateTimeToDatetime = self.basicDataLicence.timeToDatetime(time_);
        self.assertEqual(str(translateTimeToDatetime), str(datetime_).split('.')[0]);
        
    
    
    def test_uncodeCrcBasicDate(self):
        '''
        Test functionality of uncodig time()+username+md5 format into datetime()-(username+md5)   
        '''
        
        diffDate = datetime.datetime(2012, 9, 10, 21, 17, 33, 402000);
        crcDate = self.basicDataLicence.genCrcListData(diffDate); ##generate coded crcData value 
        
        uncodedData = self.basicDataLicence.uncodeCrcBasicDate(crcDate);
        
        
        
        d = abs(diffDate-uncodedData);
        self.assertEqual('0:00:00', str(d).split(".")[0]);
        

class TestDataStartupCondition(unittest.TestCase):
    dictRegValue = {'tillWhen':  '468958511800', 
                    'instalDate':'468955000000',
                    'runDate':   '468955800000',
                    'lc':'6492a566529c65748b'
                    };
    
    
    def setUp(self):
        self.register = registerReadWrite.Register()
        
        self.datetimeNow = datetime.datetime(2012, 7, 12, 21, 17, 33, 402000)
        self.dataStartupCondition = generateCRC.DataStartupCondition(self.datetimeNow)
        
        tillWhen = ('tillWhen', datetime.datetime(2012, 8, 12, 21, 17, 33, 402000));
        instalDate = ('instalDate', datetime.datetime(2012, 7, 13, 21, 17, 33, 402000));
        runDate = ('runDate', datetime.datetime(2012, 7, 13, 21, 17, 33, 402000));
        for name, datetime_ in [tillWhen, instalDate, runDate]:
            print "Calculation of %s = %d"%(name, self.dataStartupCondition.genCrcListData(datetime_));
        

    def tearDown(self):
        for regname, regvalue in self.dictRegValue.items():
            try: self.register.deleteRegisterVariable(regname);
            except: pass;
        
    def test_diffExpirationDays_OK1(self):
        '''
        Test if different time to expiration date is working. 
        In this situation register value tillWhen is None, so the result should be 0  
        '''
        tillWhen = self.dataStartupCondition.reg_tillWhen;##this in time() format

#        time_tillWhen = self.dataStartupLicenceCheck.uncodeCrcBasicDate(tillWhen);         ##time() with md5+username of expiration has been translated into datetime() format
#        print "time_tillWhen:", time_tillWhen; 
        
        datetimeNow = datetime.datetime(2012, 7, 20, 21, 17, 33, 402000);##this is just 1 day before expiration  
         
        diffDays = self.dataStartupCondition.diffExpirationDays(time_tillWhen=tillWhen,
                                                                   datetime_now=datetimeNow);
        
        self.assertEqual(diffDays, 0);

    def test_diffExpirationDays_OK2(self):
        '''
        Test if different time to expiration date is working. 
        In this situation register value tillWhen is not None, so the result should be ==1, 
        based on tillWhen=2012-08-12 21:17:32  
        '''
        ##Save register value, which will be used in the checkRegisterValue()
        for regname, regvalue in self.dictRegValue.items():
            self.register.writeRegister(regname, regvalue);
        
        tillWhen = self.dataStartupCondition.reg_tillWhen;##this in time() format

#        time_tillWhen = self.dataStartupLicenceCheck.uncodeCrcBasicDate(tillWhen);         ##time() with md5+username of expiration has been translated into datetime() format
#        print "time_tillWhen:", time_tillWhen; 
        
        datetimeNow = datetime.datetime(2012, 8, 10, 21, 17, 33, 402000);##this is just 1 day before expiration  
         
        diffDays = self.dataStartupCondition.diffExpirationDays(time_tillWhen=tillWhen,
                                                                   datetime_now=datetimeNow);
        
        self.assertEqual(diffDays, 1);

    def test_diffExpirationDays_OK3(self):
        '''
        Test if different time to expiration date is working. 
        In this situation register value tillWhen is not None, so the result should be ==1, 
        based on tillWhen=2012-08-12 21:17:32
        and 
        datetimeNow = 2012-09-12 21:17:33.402000'    +1month  
        '''
        ##Save register value, which will be used in the checkRegisterValue()
        for regname, regvalue in self.dictRegValue.items():
            self.register.writeRegister(regname, regvalue);
        
        tillWhen = self.dataStartupCondition.reg_tillWhen;##this in time() format

#        time_tillWhen = self.dataStartupLicenceCheck.uncodeCrcBasicDate(tillWhen);         ##time() with md5+username of expiration has been translated into datetime() format
#        print "time_tillWhen:", time_tillWhen; 
        
        datetimeNow = datetime.datetime(2012, 9, 12, 21, 17, 33, 402000);##this is just 1 day before expiration  
         
        diffDays = self.dataStartupCondition.diffExpirationDays(time_tillWhen=tillWhen,
                                                                   datetime_now=datetimeNow);
        
        self.assertEqual(diffDays, 0);

    def test_registerValue_OK_1(self):
        '''
        Check first startup condition for licence
        Register tillWhen, instalDate and runDate are in the range of listCRC data
        Test should be executed correctly 
        '''
        ##Save register value, which will be used in the checkRegisterValue()
        for regname, regvalue in self.dictRegValue.items():
            self.register.writeRegister(regname, regvalue);
        
        
        self.assertTrue(self.dataStartupCondition.checkRegisterValue());
        
    def test_registerValue_NOK_1(self):
        '''
        Check first startup condition for licence
        Register instalDate and runDate are in the range of listCRC data
        tillWhen is out of range
        Test should Fail 
        '''
        ##Save register value, which will be used in the checkRegisterValue()
        
        for regname, regvalue in self.dictRegValue.items():
            if regname == 'tillWhen' :
                regvalue = '1';
            self.register.writeRegister(regname, regvalue);
        
        
        self.assertFalse(self.dataStartupCondition.checkRegisterValue());
        
    def test_registerValue_NOK_2(self):
        '''
        Check first startup condition for licence
        Register tillWhen and runDate are in the range of listCRC data
        instalDate is out of range
        Test should Fail 
        '''
        ##Save register value, which will be used in the checkRegisterValue()
        
        for regname, regvalue in self.dictRegValue.items():
            if regname == 'instalDate' :
                regvalue = '1';
            self.register.writeRegister(regname, regvalue);
        
        
        self.assertFalse(self.dataStartupCondition.checkRegisterValue());

    def test_registerValue_NOK_3(self):
        '''
        Check first startup condition for licence
        Register tillWhen and instalDate are in the range of listCRC data
        runDate is out of range
        Test should Fail 
        '''
        ##Save register value, which will be used in the checkRegisterValue()
        
        for regname, regvalue in self.dictRegValue.items():
            if regname == 'runDate' :
                regvalue = '1';
            self.register.writeRegister(regname, regvalue);
        
        
        self.assertFalse(self.dataStartupCondition.checkRegisterValue());

    def test_registerValue_NOK_4(self):
        '''
        Check first startup condition for licence
        Register tillWhen and instalDate are in the range of listCRC data
        runDate does not exists and will be None
        Test should Fail 
        '''
        ##Save register value, which will be used in the checkRegisterValue()
        
        for regname, regvalue in self.dictRegValue.items():
            if regname == 'runDate' :
                continue;
            self.register.writeRegister(regname, regvalue);
        
        
        self.assertFalse(self.dataStartupCondition.checkRegisterValue());

    def test_checkIfDataChanged_OK(self):
        '''
        Check if date was not changed by changing local clock
        Register tillWhen, instalDate and runDate are in the condition 
        Test should Pass 
        '''
        ##Save register value, which will be used in the checkRegisterValue()
        for regname, regvalue in self.dictRegValue.items():
            self.register.writeRegister(regname, regvalue);
        
        self.assertTrue(self.dataStartupCondition.checkIfDataChanged());
    def test_checkIfDataChanged_NOK_1(self):
        '''
        Check if date was not changed by changing local clock
        Register last run date was changed 
        currentDate < runDate   
        Test should Fail 
        '''
        ##Save register value, which will be used in the checkRegisterValue()
        for regname, regvalue in self.dictRegValue.items():
            if regname == 'runDate' :
                regvalue = self.dataStartupCondition.genCrcListData(self.datetimeNow) + 1;
            self.register.writeRegister(regname, str(regvalue));
        
        self.assertFalse(self.dataStartupCondition.checkIfDataChanged());

    def test_checkIfDataChanged_NOK_2(self):
        '''
        Check if date was not changed by changing local clock
        Register tillWhen and instalDate are in the range
        runDate does not exists and will be None  
        Test should Fail 
        '''
        ##Save register value, which will be used in the checkRegisterValue()
        for regname, regvalue in self.dictRegValue.items():
            if regname == 'runDate' :
                continue;
            self.register.writeRegister(regname, str(regvalue));
        
        self.assertFalse(self.dataStartupCondition.checkIfDataChanged());
        
    def test_readLicenseCode_NOK_1(self):
        '''
        register licenseCode does nor exists. Should be False
        '''
        self.assertFalse(self.dataStartupCondition.readLicenseCode());

    def test_readLicenseCode_NOK_2(self):
        '''
        register licenseCode does nor exists. Should be False
        '''
        self.assertFalse(self.dataStartupCondition.readLicenseCode());


class TestRegisterData(unittest.TestCase):
    
    def setUp(self):
        self.regData = generateCRC.RegisterData();

    def tearDown(self):
        self.regData.register.deleteRegisterKey();
    
    def testWriteLicenseReg_OK_1(self):
        regvalue='6492a566529c65748b';
        regname='lc'
        self.regData.reg_licenseCode = regvalue;
        
        value = self.regData.register.readRegister(regname);
        regvalue = '9'+regvalue+'4';
        self.assertEqual(value, regvalue);
        return True
        
    
    def testReadLicenseReg_OK_1(self):
        if not self.testWriteLicenseReg_OK_1(): 
            return self.fail('ERROR');
        
        regvalue='6492a566529c65748b';
        regname='lc'

        value = self.regData.reg_licenseCode
        self.assertEqual(value, regvalue, "Read register value is different from write one");

    def testReadLicenseReg_NOK_1(self):
        regvalue='6492a566529c65748b';
        regname='lc'

        value = self.regData.reg_licenseCode
        self.assertEqual(value, None);

    def testReadLicenseReg_NOK_2(self):
        regname='lc'
        
        self.regData.register.writeRegister(regname, regvalue='')
        value = self.regData.reg_licenseCode
        self.assertEqual(value, None);
        
    def testReadLicenseReg_NOK_3(self):
        regname='lc'
        
        self.regData.register.writeRegister(regname, regvalue=1)
        value = self.regData.reg_licenseCode
        self.assertEqual(value, None);

class TestDataInstallationCondition(unittest.TestCase):

    dictRegValue = {'tillWhen':  '468958425401', 
                    'instalDate':'468955833401',
                    'runDate':   '468955833401',
                    'lc':'6492a566529c65748b'
                    };


    def setUp(self):
        self.register = registerReadWrite.Register()
        
        for regname, regvalue in self.dictRegValue.items():
            try: self.register.deleteRegisterVariable(regname);
            except: pass;
        
        
        self.datetimeNow = datetime.datetime(2012, 7, 12, 21, 17, 33, 402000)
        self.dataInstallationCondition = generateCRC.DataInstallationCondition(self.datetimeNow)
        
        tillWhen = ('tillWhen', datetime.datetime(2012, 8, 11, 21, 17, 33, 402000));
        instalDate = ('instalDate', datetime.datetime(2012, 7, 12, 21, 17, 33, 402000));
        runDate = ('runDate', datetime.datetime(2012, 7, 12, 21, 17, 33, 402000));
        for name, datetime_ in [tillWhen, instalDate, runDate]:
            print "Calculation of %s = %d"%(name, self.dataInstallationCondition.genCrcListData(datetime_));
        

    def tearDown(self):
        for regname, regvalue in self.dictRegValue.items():
            try: self.register.deleteRegisterVariable(regname);
            except: pass;
        

    def testregisterInstallionCheck_OK1(self):
        '''
        test registerInstallionCheck if is working correct
        All register where removed, method should return True 
        '''
        self.assertTrue(self.dataInstallationCondition.registerInstallionCheck());
        
    def testregisterInstallionCheck_NOK_1(self):
        '''
        test registerInstallionCheck if is working correct
        Register value is updated to some value, method should return False 
        '''
        
        for regname in ['tillWhen', 'runDate', 'instalDate']:
            self.register.writeRegister(regname, regvalue='1');
            self.assertFalse(self.dataInstallationCondition.registerInstallionCheck());
            self.register.deleteRegisterVariable(regname);

    def testregisterInstallionCheck_NOK_2(self):
        '''
        test registerInstallionCheck if is working correct
        All Register value are updated to some value, method should return False 
        '''
        
        for regname in ['tillWhen', 'runDate', 'instalDate']:
            self.register.writeRegister(regname, regvalue='1');
        self.assertFalse(self.dataInstallationCondition.registerInstallionCheck());

    def testupdateRegisterOnInstallatio(self):
        '''
        Test how the register will be updated in first startup phase, after installation
        '''
        self.dataInstallationCondition.updateRegisterOnInstallation();
        
        
        self.assertEqual(str(self.dataInstallationCondition.reg_runDate), 
                         self.dictRegValue.get('runDate'));
        
        self.assertEqual(str(self.dataInstallationCondition.reg_instalDate), 
                         self.dictRegValue.get('instalDate'));
                         
        self.assertEqual(str(self.dataInstallationCondition.reg_tillWhen), 
                         self.dictRegValue.get('tillWhen'));
        
                         
        
        

class TestLicenseCode(unittest.TestCase):
    
    def setUp(self):
        self.licenseCode = generateCRC.LicenseCode();

    def tearDown(self):
        pass
    
    def test__makeLicense(self):
        
        licenseCode = 3;
        coded = self.licenseCode.makeLicense(licenseCode);
        
#        print "for 3 makeLicense:", coded;
    
    def test__readLicense(self):
        
        licenseCode = 3;
        licenseCode = self.licenseCode.makeLicense(licenseCode);
        
        decoded = self.licenseCode.readLicense(licenseCode);
        print "decoded:", decoded;
        self.assertEqual(decoded, 3, "decoded");
        
        
#        licenseCode = '6492a566529c65748b';
        licenseCode = "97bf2e7a50a6bea138fc0a7210455fbc5e3";
        decoded = self.licenseCode.readLicense(licenseCode);
        print "decoded1:", decoded;
        self.assertEqual(decoded, 3, "decoded1");
        
        licenseCode = "97bf319a2ba1ab850e82be9a16329019d28";
        decoded = self.licenseCode.readLicense(licenseCode);
        print "decoded2:", decoded;
        self.assertEqual(decoded, 3, "decoded2");
        
#        
#        licenseCode = "7bf319a2ba8046c3be820b4ac102fbc5e";    
#        decoded = self.licenseCode.readLicense(licenseCode);
#        print "decoded3:", decoded;
#        self.assertEqual(decoded, 3, "decoded3");
#
#        
#        licenseCode = "7bf2e7d76c16c7d62c444ee114947f829";    
#        decoded = self.licenseCode.readLicense(licenseCode);
#        print "decoded4:", decoded;
#        self.assertEqual(decoded, 3, "decoded4");
        
        
        
#    def test_stuffCode(self):
#        code = self.licenseCode.__stuffCode(code=3)
#        licenseCode = [code[1],code[4], code[7]] 
#        licenseCode = ''.join(licenseCode);
##        print "licenceCode:", licenceCode; 
#        self.assertEqual('011', licenseCode);
#    
#    def test_stuffCode_NOK(self):
#        code = self.licenseCode.__stuffCode(code=9)
##        print "licenceCode:", licenceCode; 
#        licenseCode = [code[1],code[4], code[7]]
#        licenseCode = ''.join(licenseCode);
#        self.assertEqual('000', licenseCode);
#        
#    
#    def test_unstuffCode_OK(self):
#        
#        license = '006315414';
#        code = self.licenseCode.__unstuffCode(license)
#        self.assertEqual(code, 3);
#        
#    def test_unstuffCode_NOK(self):
#        
#        license = '906315414';
#        code = self.licenseCode.__unstuffCode(license)
#        self.assertFalse(code);
#        
#        
#    def test__intTobin(self):
#        
#        licenseCode = 4;
#        coded = self.licenseCode.__intToBin(licenseCode);
#        self.assertEqual(coded, '100');
#        
#        licenseCode = 3;
#        coded = self.licenseCode.__intToBin(licenseCode);
#        self.assertEqual(coded, '011');
#        
#        licenseCode = 2;
#        coded = self.licenseCode.__intToBin(licenseCode);
#        self.assertEqual(coded, '010');
#        
#        licenseCode = 1;
#        coded = self.licenseCode.__intToBin(licenseCode);
#        self.assertEqual(coded, '001');
#        
#        licenseCode = 0;
#        coded = self.licenseCode.__intToBin(licenseCode);
#        self.assertEqual(coded, '000');
#        
#    def test__binToInt_1(self):
#        
#        binValue = '011'
#        bin_ = self.licenseCode.__binToInt(binValue)
#        self.assertEqual(3, bin_);
#        
#        binValue = '001'
#        bin_ = self.licenseCode.__binToInt(binValue)
#        self.assertEqual(1, bin_);
#        
#        
#        binValue = '0'
#        bin_ = self.licenseCode.__binToInt(binValue)
#        self.assertEqual(0, bin_);
#        
#        binValue = '111'
#        bin_ = self.licenseCode.__binToInt(binValue)
#        self.assertEqual(7, bin_);
#    
#    def test__binToInt__NOK_1(self):
#        
#        ##input int() value which should return False
#        binValue = 11
#        bin_ = self.licenseCode.__binToInt(binValue)
#        self.assertFalse(bin_);
#        
#        ##input value is not bin() value
#        binValue = '2'
#        bin_ = self.licenseCode.__binToInt(binValue)
#        self.assertFalse(bin_);

        
        
        
def load_tests(loader, tests, pattern):
    suite = unittest.TestSuite()
#    suite.addTest(unittest.makeSuite(Test));  ##Run all tests
    return suite


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()