'''
Created on 01-07-2012

@author: lucst
'''
import unittest
import sys
import datetime
from PyQt4.QtGui import *
from PyQt4.QtCore import *

import lib.QClicense.checkLicence as checkLicence;
from lib.QClicense import registerReadWrite, generateCRC

app = QApplication(sys.argv)



class TestDataStartupLicenceCheck(unittest.TestCase):
    dictRegValue = {'tillWhen':  '468958511800', 
                    'instalDate':'468955919800',
                    'runDate':   '468955919800',
                    };
                    
    register = registerReadWrite.Register()

    def setUp(self):
#        self.register = registerReadWrite.Register()
        ##Save register value, which will be used in the checkRegisterValue()
        for regname, regvalue in self.dictRegValue.items():
            self.register.writeRegister(regname, regvalue);
        
        datetimeNow = datetime.datetime(2012, 7, 13, 21, 17, 34, 0);
        self.dataStartupLicenceCheck = checkLicence.DataStartupLicenceCheck(datetimeNow=datetimeNow);
        
        for regname, regvalue in self.dictRegValue.items():
            print "%s=%s"%(regname, self.dataStartupLicenceCheck.uncodeCrcBasicDate(int(regvalue)));
            

    def tearDown(self):
        del self.dataStartupLicenceCheck
        
#        self.register.deleteRegisterKey();
        for regname, regvalue in self.dictRegValue.items():
            self.register.deleteRegisterVariable(regname)
        


    
    def test_dataStartupCheck_OK_1(self):
        '''
        Check if in standard operation data licence will work correctly. 
        Those 'standard' operation is: 
        1. registerValue has not been modified, 
        2. data is not expired, 
        '''
#        datetimeNow = datetime.datetime(2012, 7, 13, 21, 17, 34, 0);
#        dataStartupLicenceCheck = checkLicence.DataStartupLicenceCheck(datetimeNow=datetimeNow);
        
        for messagebox in self.dataStartupLicenceCheck.dataStartupCheck():
            text = "Your license will expire in 29 day(s)";
            self.assertEqual(text, messagebox.text());
            QTimer.singleShot(0.5*1000, messagebox, SLOT('accept()')); ##this will push OK button after 5 sec
        self.assertTrue(self.dataStartupLicenceCheck.status);
        
    def test_dataStartupCheck_OK_2_1(self):
        '''
        Check if in standard operation data licence will work correctly. 
        Those 'standard' operation is: 
        1. registerValue has not been modified, 
        2. data is not expired,
        '''
        datetimeNow = datetime.datetime(2012, 8, 2, 21, 17, 33, 0);
        dataStartupLicenceCheck = checkLicence.DataStartupLicenceCheck(datetimeNow=datetimeNow);
        
        
        date_min, date_max = dataStartupLicenceCheck.listData();
        print "date_min=%s  date_max=%s"%(dataStartupLicenceCheck.uncodeCrcBasicDate(int(date_min)), 
                                          dataStartupLicenceCheck.uncodeCrcBasicDate(int(date_max)));
        
        for messagebox in dataStartupLicenceCheck.dataStartupCheck():
            text = "Your license will expire in 9";
            self.assertTrue(text in messagebox.text());
            QTimer.singleShot(0.5*1000, messagebox, SLOT('accept()')); ##this will push OK button after 5 sec
        self.assertTrue(dataStartupLicenceCheck.status);

    def test_dataStartupCheck_OK_2_2(self):
        '''
        Check if in standard operation data licence will work correctly. 
        Those 'standard' operation is: 
        1. registerValue has not been modified, 
        2. data is not expired,
        '''
        datetimeNow = datetime.datetime(2012, 8, 3, 21, 17, 33, 402000);
        dataStartupLicenceCheck = checkLicence.DataStartupLicenceCheck(datetimeNow=datetimeNow);
        
        
        date_min, date_max = dataStartupLicenceCheck.listData();
        print "date_min=%s  date_max=%s"%(dataStartupLicenceCheck.uncodeCrcBasicDate(int(date_min)), 
                                          dataStartupLicenceCheck.uncodeCrcBasicDate(int(date_max)));
        
        for messagebox in dataStartupLicenceCheck.dataStartupCheck():
            text = "Your license will expire in 8";
            self.assertTrue(text in messagebox.text());
            QTimer.singleShot(0.5*1000, messagebox, SLOT('accept()')); ##this will push OK button after 5 sec
        self.assertTrue(dataStartupLicenceCheck.status);    
    
    def test_dataStartupCheck_OK_2_3(self):
        '''
        Check if in standard operation data licence will work correctly. 
        Those 'standard' operation is: 
        1. registerValue has not been modified, 
        2. data is not expired,
        '''
        datetimeNow = datetime.datetime(2012, 8, 11, 21, 17, 31, 402000);
        dataStartupLicenceCheck = checkLicence.DataStartupLicenceCheck(datetimeNow=datetimeNow);
        
        
        date_min, date_max = dataStartupLicenceCheck.listData();
        print "date_min=%s  date_max=%s"%(dataStartupLicenceCheck.uncodeCrcBasicDate(int(date_min)), 
                                          dataStartupLicenceCheck.uncodeCrcBasicDate(int(date_max)));
        
        for messagebox in dataStartupLicenceCheck.dataStartupCheck():
            text = "Your license will expire in 1";
            self.assertTrue(text in messagebox.text());
            QTimer.singleShot(0.5*1000, messagebox, SLOT('accept()')); ##this will push OK button after 5 sec
        self.assertTrue(dataStartupLicenceCheck.status); 
    
    def test_dataStartupCheck_OK_2_4(self):
        '''
        Check if in standard operation data licence will work correctly. 
        Those 'standard' operation is: 
        1. registerValue has not been modified, 
        2. data is not expired,
        '''
        datetimeNow = datetime.datetime(2012, 8, 12, 21, 17, 31, 402000);
        dataStartupLicenceCheck = checkLicence.DataStartupLicenceCheck(datetimeNow=datetimeNow);
        
        
        date_min, date_max = dataStartupLicenceCheck.listData();
        print "date_min=%s  date_max=%s"%(dataStartupLicenceCheck.uncodeCrcBasicDate(int(date_min)), 
                                          dataStartupLicenceCheck.uncodeCrcBasicDate(int(date_max)));
        
        for messagebox in dataStartupLicenceCheck.dataStartupCheck():
            text = "Your license will expire in 0";
            self.assertTrue(text in messagebox.text());
            QTimer.singleShot(0.5*1000, messagebox, SLOT('accept()')); ##this will push OK button after 5 sec
        self.assertTrue(dataStartupLicenceCheck.status); 
    
    
    def test_dataStartupCheck_OK_3(self):
        '''
        Check if in standard operation data licence will work correctly. 
        Those 'standard' operation is: 
        1. registerValue has not been modified, 
        2. data was expired, 
        '''
        datetimeNow = datetime.datetime(2012, 8, 30, 21, 17, 33, 402000);
        dataStartupLicenceCheck = checkLicence.DataStartupLicenceCheck(datetimeNow=datetimeNow);
        
        for messagebox in dataStartupLicenceCheck.dataStartupCheck():
            text = "Your license expired. Please contact with us to extend Your license"
            self.assertEqual(messagebox.text(), text)
            QTimer.singleShot(0.5*1000, messagebox, SLOT('accept()')); ##this will push OK button after 5 sec
        self.assertFalse(dataStartupLicenceCheck.status);
        
    def test_dataStartupCheck_OK_3_1(self):
        '''
        Check if in standard operation data licence will work correctly. 
        Those 'standard' operation is: 
        1. registerValue has not been modified, 
        2. data was expired, 
        '''
        datetimeNow = datetime.datetime(2012, 8, 13, 0, 0, 1, 0);
        dataStartupLicenceCheck = checkLicence.DataStartupLicenceCheck(datetimeNow=datetimeNow);
        
        for messagebox in dataStartupLicenceCheck.dataStartupCheck():
            text = "Your license expired. Please contact with us to extend Your license"
            self.assertEqual(messagebox.text(), text)
            QTimer.singleShot(0.5*1000, messagebox, SLOT('accept()')); ##this will push OK button after 5 sec
        self.assertFalse(dataStartupLicenceCheck.status);
            
    def test_dataStartupCheck_OK_4(self):
        '''
        Check if in standard operation data licence will work correctly. 
        Those 'standard' operation is: 
        1. registerValue has been modified, 
        2. data is not expired, 
        '''
        regname = 'instalDate';
        regvalue = '1';
        self.register.writeRegister(regname, regvalue);
        
        
        
        
        for messagebox in self.dataStartupLicenceCheck.dataStartupCheck():
            text = "Your license expired. Please contact with us to extend Your license"
            self.assertEqual(messagebox.text(), text)
            QTimer.singleShot(0.5*1000, messagebox, SLOT('accept()')); ##this will push OK button after 5 sec
        self.assertFalse(self.dataStartupLicenceCheck.status);

    def test_dataStartupCheck_OK_5(self):
        '''
        Check if in standard operation data licence will work correctly. 
        Those 'standard' operation is: 
        1. registerValue has been modified, 
        2. data is not expired, 
        '''
#        regname = 'instalDate';
#        regvalue = '1';
#        self.register.writeRegister(regname, regvalue);
        datetimeNow = datetime.datetime(2012, 7, 15, 21, 17, 34, 0);
        dataStartupLicenceCheck = checkLicence.DataStartupLicenceCheck(datetimeNow=datetimeNow);
        
        regname = 'runDate';
        regvalue = dataStartupLicenceCheck.genCrcListData(datetimeNow) + 1;
        self.register.writeRegister(regname, regvalue);
        
        
        
        
        
        for messagebox in dataStartupLicenceCheck.dataStartupCheck():
            text = "Your license expired. Please contact with us to extend Your license"
            self.assertEqual(messagebox.text(), text)
            self.assertEqual(messagebox.icon(), QMessageBox.Critical);
            QTimer.singleShot(0.5*1000, messagebox, SLOT('accept()')); ##this will push OK button after 5 sec
        self.assertFalse(dataStartupLicenceCheck.status);

    def test_checkLicenseCode_OK1(self):
        '''
        Check license code
        !!remember that reg_lc is edited before write and read. Special char are added to this value 
        '''
        
        
        
        regname = 'lc';
        licenseCodeValue = 3;
        regvalue = self.dataStartupLicenceCheck.makeLicense(licenseCodeValue);
        self.dataStartupLicenceCheck.reg_licenseCode=regvalue;
        
        
        self.assertNotEqual(self.dataStartupLicenceCheck.checkLicenseCode(), False);
        self.assertTrue(self.dataStartupLicenceCheck.status);
        
    def test_checkLicenseCode_NOK1(self):
        '''
        Check license code. 
        Now license register is not created, is None. 
        '''
        try: self.register.deleteRegisterVariable(regname='lc');
        except: pass;
        self.dataStartupLicenceCheck.status = False;
        self.assertFalse(self.dataStartupLicenceCheck.checkLicenseCode());
        
        self.assertFalse(self.dataStartupLicenceCheck.status);    

    def test_mainStartupCheck_OK1(self):
        '''
        Check if checkLicenseCode will be executed
        '''
        
        regname = 'lc';
        licenseCodeValue = 3;
        regvalue = self.dataStartupLicenceCheck.makeLicense(licenseCodeValue);
        self.dataStartupLicenceCheck.reg_licenseCode=regvalue;
        
        datetimeNow = datetime.datetime(2012, 7, 15, 21, 17, 34, 0);
        dataStartupLicenceCheck = checkLicence.DataStartupLicenceCheck(datetimeNow=datetimeNow);
        
        
        
        for i in dataStartupLicenceCheck.mainStartupCheck(): pass;
        self.assertTrue(dataStartupLicenceCheck.status);

    def test_mainStartupCheck_OK2(self):
        '''
        Check if dataStarupCheck will be executed
        '''
        try: self.register.deleteRegisterVariable(regname='lc')
        except: pass;

        datetimeNow = datetime.datetime(2012, 7, 15, 21, 17, 34, 0);
        dataStartupLicenceCheck = checkLicence.DataStartupLicenceCheck(datetimeNow=datetimeNow);

        
        for messagebox in dataStartupLicenceCheck.mainStartupCheck():
            text = "Your license will expire in 27 day(s)";
            self.assertEqual(messagebox.text(), text)
            self.assertEqual(messagebox.icon(), QMessageBox.Information);
            QTimer.singleShot(0.5*1000, messagebox, SLOT('accept()')); ##this will push OK button after 5 sec
        self.assertTrue(dataStartupLicenceCheck.status);

class TestLicenseToTable(unittest.TestCase):
    licenseCodeToTextDict = {0:"Trial version",
                             1:'Standard version',
                             2:'Extended version with commandline',
                             3:'Not defined',
                             4:'Not defined',
                             5:'Not defined',
                             6:'Not defined',
                             7:'Not defined',
                             8:'Not defined'};
                    

    def setUp(self):
        self.licenseToTable = checkLicence.LicenseToTable();

    def tearDown(self):
        pass;
    
    def testlicenseCodeToText_1_NOK(self):
        '''
        Check situation when lc_decoded is as str(). Should return False
        '''
        
        lc_decoded = 'test'
        self.assertFalse(self.licenseToTable.licenseCodeToText(lc_decoded));

    def testlicenseCodeToText_2_NOK(self):
        '''
        Check situation when lc_decoded is as int(), but out of licenseCodeToTextDict. 
        Should return False
        '''
        
        lc_decoded = 9
        self.assertFalse(self.licenseToTable.licenseCodeToText(lc_decoded));

    def testlicenseCodeToText_3_OK(self):
        '''
        Check situation when lc_decoded is as int() and is in range of Dict.
        Should return list of License in text format
        '''
        
        lc_decoded = 1
        licenseTextList = self.licenseToTable.licenseCodeToText(lc_decoded)
        licenseTextList_cmp = [self.licenseCodeToTextDict[1]];
        self.assertEqual(licenseTextList, licenseTextList_cmp);
        
    def testlicenseCodeToText_4_OK(self):
        '''
        Check situation when lc_decoded is as int() and is in range of Dict.
        Should return list of License in text format
        '''
        
        lc_decoded = 2
        licenseTextList = self.licenseToTable.licenseCodeToText(lc_decoded)
        licenseTextList_cmp = [self.licenseCodeToTextDict[1], 
                               self.licenseCodeToTextDict[2]];
        self.assertEqual(licenseTextList, licenseTextList_cmp);


def load_tests(loader, tests, pattern):
    suite = unittest.TestSuite()
#    suite.addTest(unittest.makeSuite(Test));  ##Run all tests
    return suite


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()