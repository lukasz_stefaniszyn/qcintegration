'''
Created on 01-07-2012

@author: lucst
'''
import unittest
import win32api, win32con
from functools import partial

import lib.QClicense.registerReadWrite as registerReadWrite;


class TestReadReg(unittest.TestCase):

    regname = 'testVariableName';
    regvalue = 'testValue'

    def setUp(self):
        self.register = registerReadWrite.Register()
        self.register.writeRegister(self.regname, self.regvalue)


    def tearDown(self):
        self.register.deleteRegisterVariable(self.regname); 
    
    def testReadReg_OK_1(self):
        value = self.register.readRegister(self.regname);
        print value; 
        self.assertEqual(value, self.regvalue, "Read register value is different from write one");
    
    def testReadReg_NOK_1(self):
        '''
        Here will be wrong register value name
        '''
        self.assertEqual(self.register.readRegister(regname="WrongName"), None);
        

class TestWriteReg(unittest.TestCase):

    regname = 'testVariableName';
    regvalue = 'testValue'

    def setUp(self):
        self.register = registerReadWrite.Register()

    def tearDown(self):
        try: self.register.deleteRegisterVariable(self.regname);
        except: pass;
    

    def testWriteReg_OK_1(self):
        self.register.writeRegister(self.regname, self.regvalue);
        
        value = self.register.readRegister(self.regname)
        self.assertEqual(value, self.regvalue, "Read register value is different from write one");
    
    def testWriteReg_OK_2(self):
        '''
        We will write new variable to already existing variable. 
        '''
        self.register.writeRegister(self.regname, self.regvalue);
        self.register.deleteRegisterVariable(self.regname);
        
        newregvalue = "SecondValueTest"
        self.register.writeRegister(self.regname, newregvalue);
        
        value = self.register.readRegister(self.regname)
        self.assertEqual(value, newregvalue, "Read register value is different from write one");
        

    def testWriteReg_NOK_2(self):
        '''
        We will try to write new variable to not existing key 
        '''
        self.register.deleteRegisterKey();
        
        self.assertEqual(self.register.writeRegister(regname=self.regname,
                                                     regvalue=self.regvalue), 
                         False);


class TestDelRegValue(unittest.TestCase):

    regname = 'testVariableName';
    regvalue = 'testValue'

    def setUp(self):
        self.register = registerReadWrite.Register()

    def tearDown(self):
        try: self.register.deleteRegisterVariable(self.regname);
        except: pass;


    def testDelRegisterVariable_OK_1(self):
        '''
        Create register variable and then remove it. At the end You try to read this variable, which should not be possible 
        '''
        self.register.writeRegister(self.regname, self.regvalue);
        self.register.deleteRegisterVariable(self.regname);
        
        self.assertEqual(self.register.readRegister(regname=self.register), None);

    def testDelRegisterVariable_NOK_1(self):
        '''
        Try to remove not existing variable name 
        '''
        callableFunction = partial(self.register.deleteRegisterVariable, 
                                   regname="WrongName", 
                                   );
        self.assertRaises(registerReadWrite.RegisterException, callableFunction);


class TestDelRegKey(unittest.TestCase):
    regname = 'testVariableName';
    regvalue = 'testValue'

    def setUp(self):
        self.register = registerReadWrite.Register()
        self.register.writeRegister(self.regname, self.regvalue);

    def tearDown(self):
        try: self.register.deleteRegisterKey()
        except: pass;

    def testDelRegisterKey_OK_1(self):
        '''
         
        '''
        self.register.deleteRegisterKey();
#        callableFunction = partial(self.register.readRegister, 
#                                   regname=self.regname, 
#                                   );
        self.assertEqual(self.register.readRegister(regname=self.register), None);

    
    def testDelRegisterKey_NOK_1(self):
        '''
         
        '''
        self.register.deleteRegisterKey();
        callableFunction = partial(self.register.deleteRegisterKey, 
                                   );
        self.assertRaises(registerReadWrite.RegisterException, callableFunction);



def load_tests(loader, tests, pattern):
    suite = unittest.TestSuite()
#    suite.addTest(unittest.makeSuite(Test));  ##Run all tests
    return suite


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()