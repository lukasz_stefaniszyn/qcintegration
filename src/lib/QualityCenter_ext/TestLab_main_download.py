# -*- coding: utf-8 -*-
#!/usr/bin/env python

'''
Created on 2009-09-30

@author: Lukasz Stefaniszyn
'''
import sys


def QC_92():
    import lib.qc_connect_disconnect as qc_connect_disconnect 
    import lib.read_arguments as read_arguments
    import lib.TestLab_download as TestLab_download;  
    #    import getpass
#    server = "http://muvmp034.nsn-intra.net/qcbin/" 
#    UserName="stefaniszyn"
#    #Password = getpass.getpass('Password for %s: '%UserName);
#    
#    DomainName="HIT7300"
#    ProjectName="hiT73_R42x"
   
#    TSdir = r'Root\4.25.10_hit7300_TestLab\D_Control_Management\D07 Control - OpticalLinkControl\D07.01 Basic SNMP Commands';
#    TSname = r'D07.01.01 ONNT';
    
    usage_text = "usage: %prog <options> [TestSets directory] [TestSet name]\n"\
                    "\nTestSets directory - this is full path to the TestSets, empty means all directories under Root\\.\n"\
                    "\tExample: Root\\4.25.10_hit7300_TestLab\\D_Control_Management\\D07 Control - OpticalLinkControl\\D07.01 Basic SNMP Commands\n"\
                    "TestSet name - this is TestSet name from TestSet directory\n"\
                    "\tExample: D07.01.01 ONNT\n\n";
    server, UserName, Password, DomainName, ProjectName, args = read_arguments.arguments(usage_text);
    
    
    
    
    print "\n";
    print "#"*60;
    print "server:", server;
    print "UserName:", UserName;
    print "DomainName:", DomainName;
    print "ProjectName", ProjectName;
    
    
    TSdir = None;
    TSname = None;
    if len(args)>0:
        TSdir = args[0];
        try:
            TSname = args[1];
        except IndexError:
            print "No TestSet name in input arguments";
    
        
    print "TestSets directory:",TSdir
    print "TestSet name:", TSname;
    print "#"*60;
    print "\n";
    
    
    qcConnect = qc_connect_disconnect.QC_connect(server, UserName, Password, DomainName, ProjectName);
    if qcConnect.connect():
        print("Connected to QC");
    else:
        print("Not connected to QC server, exiting");
        sys.exit(1);
    
    
    TestLabDownload_CInst = TestLab_download.TestLabDownload(qcConnect.qc) 
#    TestLabDownload_CInst.test_main();
#    


    
    
    dict_instance_TestSets, TestSets_list = TestLabDownload_CInst.give_TestSets(TSdir, TSname)
    #print "dict_instance_TestSets:",dict_instance_TestSets
    
    #return dict_instance_TestSets, qcConnect
    
    TestLab_download.updateExcel(dict_instance_TestSets, TestSets_list, qcConnect);
    
 
    if qcConnect.disconnect_QC():
        print "\nDisconnecting from QC server."\
                "\n"\
                "\nThank You for using QC_import_export script.\n"\
                "\t\t\t\n"\
                "\t\t\t  Lukasz Stefaniszyn\n"\
                "\n\t\t\t\t\tPowered by Python 2.5";
                
#------------------------------------------------------------------------------ 
                
if __name__ == "__main__":
    QC_92();
