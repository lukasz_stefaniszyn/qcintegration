# -*- coding: utf-8 -*-
#!/usr/bin/env python

'''
Created on 2009-09-17

@author: Lukasz Stefaniszyn
'''





excel_fields_TestLab = {'Modify':(1,1, 'Modify'),\
                'TestSet':(1,2, 'TestSet'),\
                'TestCase':(1,4, 'TestCase'),\
                'TestStep':(1,18, 'TestStep'),\
                'Modify blank':(2,1, 'X'),\
                'TestSet Directory':(2,2, 'Directory'),\
                'TestSet Name':(2,3, 'Name'),\
                'TestCase Name':(2,4, 'Name'),\
                'TestCase Type':(2,5, 'Type'),\
                'TestCase Status':(2,6, 'Status'),\
                'TestCase Attachments':(2, 7, 'Attachments'),\
                'TestCase Iteration':(2,8, 'Iteration'),\
                'TestCase Planned Host Name':(2,9, 'Planned Host Name'),\
                'TestCase Responsible Tester':(2,10, 'Resp Tester'),\
                'TestCase MR ID':(2,11, 'MR ID'),\
                'TestCase APS':(2,12, 'APS\LCT\NEC'),\
                'TestCase Execution Date':(2,13, 'Exc Date'),\
                'TestCase Execution Time':(2,14, 'Exc Time'),\
                'TestCase Planned Execution Date':(2,15, 'Planned Exc Date'),\
                'TestCase Planned Execution Time':(2,16, 'Planned Exc Time'),\
                'TestStep Name':(2,17, 'Name'),\
                'TestStep Status':(2,18, 'Status'),\
                'TestStep Attachments':(2, 19, 'Attachments'),\
                'TestStep Description':(2,20, 'Description'),\
                'TestStep Excpected':(2,21, 'Excpected'),\
                'TestStep Actual':(2,22, 'Actual'),\
                'TestStep Execution Date':(2,23, 'Exc Date'),\
                'TestStep Execution Time':(2,24, 'Exc Time'),\
                'TestStep SW Build':(2,25, 'SW Build'), \
                'TestStep MR ID':(2,26, 'MR ID'),\
                }
    
excel_fields_TestPlan = {
                'TestPlan':(1,1, 'Test Plan'),\
                'TestLab':(1,9, 'Test Lab'),\
                'TestCasePlan':(2,1, 'Test Case'),\
                'TestStepPlan':(2,5, 'Step'),\
                'TestSetLab':(2,9, 'Test Set'),\
                'TestCaseLab':(2,11, 'Test Case'),\
                'TestCasePlan Directory':(3,1, 'Directory'),\
                'TestCasePlan Name':(3,2, 'Name'),\
                'TestCasePlan Attachments':(3,3, 'Attachments'),\
                'TestCasePlan Description':(3,4, 'Description'),\
                'TestStepPlan Name':(3,5, 'Name'),\
                'TestStepPlan Attachments':(3,6, 'Attachments'),\
                'TestStepPlan Description':(3,7, 'Description'),\
                'TestStepPlan Expected':(3,8, 'Expected Result'),\
                'TestSetLab Directory':(3,9, 'Directory'),\
                'TestSetLab Name':(3,10, 'Name'),\
                'TestCaseLab Directory':(3,11, 'Directory'),\
                'TestCaseLab Name':(3,12, 'Name'),\
                }