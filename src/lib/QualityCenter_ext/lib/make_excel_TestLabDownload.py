# -*- coding: utf-8 -*-
#!/usr/bin/env python

'''
Created on 2009-09-16

@author: Lukasz Stefaniszyn
'''

import win32com.client
import os;
from time import strftime, localtime;
import pywintypes
import sys;


import excel_global_variable

from lib.logfile import Logger
from globalVariables import GlobalVariables
log = Logger(loggername="lib.QualityCenter_ext.lib.make_excel_TestLabDownload", resetefilelog=False).log;



class ExcelWorksheets(object):
    
    #------------------------------------------------------------------------------ 
    def __init__(self, qcConnect):
        try:
            self.__excel_object = win32com.client.Dispatch("Excel.Application");
            self.__excel_object.Visible = False
        except pywintypes.com_error:
            log.warning(("ERR: Unable to run Microsoft Excel program. \n Please install first Microsoft Excel"));
            qcConnect.disconnect_QC();
            #qcConnect.disconnect_QC();
            log.info(("Disconnecting from QC"));
            sys.exit(1);
            
        
        
        
        self.__workbook = self.__excel_object.Workbooks.Add();
        self.__catalogResult, self.__fileExcelResult = self.filenameExcelMake();
    #------------------------------------------------------------------------------ 
    def __get_excelObject(self):
        return self.__excel_object;
    def __set_excelObject(self, objective):
        self.__excel_object = objective;
    excelObject = property(__get_excelObject, __set_excelObject);
    
    def __get_workbook(self):
        return self.__workbook;
    workbook = property(__get_workbook);
    
    def __get_resultName(self):
        return self.__catalogResult, self.__fileExcelResult;
    resultName = property(__get_resultName);
    
    #------------------------------------------------------------------------------ 
    
    def filenameExcelMake(self):

        os.chdir('..\\.\\');
        resultName = strftime('Download_%d-%m-%Y_%H-%M-%S', localtime());
        catalogResult = os.path.join(GlobalVariables.DIR_RESULT,resultName);
        fileExcelResult = resultName + '.xls';
        try:
            #print "CWD:",os.getcwd();
            os.mkdir(catalogResult);
        except:
            try:
                os.mkdir(os.getcwd()+'\\'+'Result');
                os.mkdir(os.getcwd()+'\\'+catalogResult);
            except:
                log.warning(("ERR: Catalog already exist: ",catalogResult));
                import sys;
                sys.exit(1);
        
        
        return catalogResult, fileExcelResult;
        
    
    def worksheet_make(self, worksheet_number):
        '''This will make default One worksheet with correct cell naming and colors
        input = int(number of worksheet)
        output = None;'''
        
        worksheet_CIns = MakeWorksheet(self.excelObject)
        
        ## This will make worksheet naming
        worksheet_CIns.excelObject.Worksheets.Add(Before = self.excelObject.Worksheets[self.excelObject.Worksheets.Count -3]);
        worksheet_CIns.excelObject.Worksheets[self.excelObject.Worksheets.Count -4].Name = "qc_import_export_xls_"+str(worksheet_number +1);
    
        #QC_to_Excel_setup.exc = self.excelObject;
        
        ##Name columns with Font.Bold
        worksheet_CIns.naming_cells();
    
        ##Color columns
        worksheet_CIns.color_cells();
    
        ##Border set
        worksheet_CIns.borders_cells();
    
        ##Make correct Cell format
        worksheet_CIns.cellFormat();
        
        ##Autofit weight of Columns
        worksheet_CIns.autofit_columns();
        
        ##FreezePanes on row = 2; 
        #worksheet_CIns.freezePanes();
        
        ##Disable Wrap text in column Step Description, Excpected, Actual
        worksheet_CIns.wrapText();
        
        
        return
    
#------------------------------------------------------------------------------ 
#------------------------------------------------------------------------------ 

class MakeWorksheet():
    '''This will make One full worksheet with color and naming for worksheet
    input = inst(excelObject)
    output = None'''


    #------------------------------------------------------------------------------ 
    def __init__(self, excelObject):
        
        self.__excel_object = excelObject;
        self.__main_cells_naming = ['Modify', 'TestSet', 'TestCase', 'TestStep'];
    #------------------------------------------------------------------------------ 
    def __get_excelObject(self):
        return self.__excel_object;
    def __set_excelObject(self, objective):
        self.__excel_object = objective;
    excelObject = property(__get_excelObject, __set_excelObject);

    def __get_mainCellsNaming(self):
        return self.__main_cells_naming;
    mainCellsNaming = property(__get_mainCellsNaming);
    #------------------------------------------------------------------------------ 
    
    def color_cells(self):
        '''This will color columns'''

        ##Color excel cells
        ##1
        
        ##Color Modify column
        column1 = self.excelObject.Columns(excel_global_variable.excel_fields_TestLab.get("Modify")[1]);
        column1.Interior.ColorIndex = 40;
        
        ##Color TestSet
        for column_number in range(excel_global_variable.excel_fields_TestLab.get('TestSet Directory')[1], excel_global_variable.excel_fields_TestLab.get("TestSet Name")[1]+1):
            self.excelObject.Columns(column_number).Interior.ColorIndex = 34;

        ##Color TestCase        
        for column_number in range(excel_global_variable.excel_fields_TestLab.get('TestCase Name')[1], excel_global_variable.excel_fields_TestLab.get("TestCase Planned Execution Time")[1] +1):
            self.excelObject.Columns(column_number).Interior.ColorIndex = 35;

        ##Color Step
        for column_number in range(excel_global_variable.excel_fields_TestLab.get('TestStep Name')[1], excel_global_variable.excel_fields_TestLab.get("TestStep MR ID")[1] + 1):
            self.excelObject.Columns(column_number).Interior.ColorIndex = 36;

    def naming_cells(self):
        '''Create cells with naming of columns, like TestCase, TestSet, Exceution Date, etc.
        Additional here the Font of TestSet, TestCase, Step will be Bold'''


        all_cell_naming = excel_global_variable.excel_fields_TestLab.keys();
        for cell_naming in all_cell_naming:
            self.excelObject.Cells(excel_global_variable.excel_fields_TestLab.get(cell_naming)[0], excel_global_variable.excel_fields_TestLab.get(cell_naming)[1]).Value = excel_global_variable.excel_fields_TestLab.get(cell_naming)[2];

        ##TestSet, TestCase, TestStep cell Make the Bold on text
        for main_cell_name in self.mainCellsNaming:
            self.excelObject.Cells(excel_global_variable.excel_fields_TestLab.get(main_cell_name)[0], excel_global_variable.excel_fields_TestLab.get(main_cell_name)[1]).Characters.Font.Bold = True;

    def borders_cells(self):
        '''Make Borders line on cells
        LineStyle number = http://msdn.microsoft.com/en-us/library/bb241348.aspx
        Weight = http://msdn.microsoft.com/en-us/library/bb240972.aspx
        Item Index = http://msdn.microsoft.com/en-us/library/bb240971.aspx'''

        ##TestSet, TestCase, TestStep cell Make the Bold on text 
        for main_cell_name in self.mainCellsNaming:
            self.excelObject.Cells(excel_global_variable.excel_fields_TestLab.get(main_cell_name)[0], excel_global_variable.excel_fields_TestLab.get(main_cell_name)[1]).Borders.Weight = 4;
        
        for column_number in range(1, excel_global_variable.excel_fields_TestLab.get("TestStep MR ID")[1] + 1):
            self.excelObject.Columns(column_number).Borders.Weight = 1;

        ##Border Modify
        ##Border on Column with Column name
        cell1 = self.excelObject.Cells(excel_global_variable.excel_fields_TestLab.get("TestSet Directory")[0], excel_global_variable.excel_fields_TestLab.get("Modify blank")[1]);
        cell1.Borders.Weight = 2;
        ##Between columns
        column1 = self.excelObject.Columns(excel_global_variable.excel_fields_TestLab.get("Modify")[1]);
        column1.BorderAround(Weight = 3); 


        ##Border TestSet
        ##Border between begin file and TestSet, between TestSet and TestCase
        column1 = self.excelObject.Columns(excel_global_variable.excel_fields_TestLab.get("TestSet Directory")[1]);
        column2 = self.excelObject.Columns(excel_global_variable.excel_fields_TestLab.get("TestSet Name")[1]);
        self.excelObject.Range(column1, column2).BorderAround(Weight = 3);
#        self.excelObject.Columns(1).BorderAround(Weight = 3);
        ##Border on Column with Column name
        cell1 = self.excelObject.Cells(excel_global_variable.excel_fields_TestLab.get("TestSet Directory")[0], excel_global_variable.excel_fields_TestLab.get("TestSet Directory")[1]);
        cell2 = self.excelObject.Cells(excel_global_variable.excel_fields_TestLab.get("TestSet Name")[0], excel_global_variable.excel_fields_TestLab.get("TestSet Name")[1]);
        self.excelObject.Range(cell1, cell2).Borders.Weight = 2;
        ##Remove Vertical Borders between TestSet and TestCase row 1
        cell1 = self.excelObject.Cells(excel_global_variable.excel_fields_TestLab.get("TestSet")[0], excel_global_variable.excel_fields_TestLab.get("TestSet")[1]);
        cell2 = self.excelObject.Cells(excel_global_variable.excel_fields_TestLab.get("TestCase")[0], excel_global_variable.excel_fields_TestLab.get("TestCase")[1] -1);
        self.excelObject.Range(cell1, cell2).Borders.Item(Index=11).LineStyle = -4142;
        
        
        

        ##Border TestCase
        ##Border on Column with Column name
        cell1 = self.excelObject.Cells(excel_global_variable.excel_fields_TestLab.get("TestCase Name")[0], excel_global_variable.excel_fields_TestLab.get("TestCase Name")[1]);
        cell2 = self.excelObject.Cells(excel_global_variable.excel_fields_TestLab.get("TestCase Planned Execution Time")[0], excel_global_variable.excel_fields_TestLab.get("TestCase Planned Execution Time")[1]);
        self.excelObject.Range(cell1, cell2).Borders.Weight = 2;
        ##Border between TestSet and TestCase, between TestCase and TestStep
        column1 = self.excelObject.Columns(excel_global_variable.excel_fields_TestLab.get("TestCase Name")[1]);
        column2 = self.excelObject.Columns(excel_global_variable.excel_fields_TestLab.get("TestCase Planned Execution Time")[1]);
        range_columns = self.excelObject.Range(column1, column2);
        range_columns.BorderAround(Weight=3);        
        ##Remove Vertical Borders between B1:K1
        cell1 = self.excelObject.Cells(excel_global_variable.excel_fields_TestLab.get("TestCase")[0], excel_global_variable.excel_fields_TestLab.get("TestCase")[1]);
        cell2 = self.excelObject.Cells(excel_global_variable.excel_fields_TestLab.get("TestStep")[0], excel_global_variable.excel_fields_TestLab.get("TestStep")[1] -1);
        self.excelObject.Range(cell1, cell2).Borders.Item(Index=11).LineStyle = -4142;



        ##Border Step
        ##Border on Column with Column name
        cell1 = self.excelObject.Cells(excel_global_variable.excel_fields_TestLab.get("TestStep Name")[0], excel_global_variable.excel_fields_TestLab.get("TestStep Name")[1]);
        cell2 = self.excelObject.Cells(excel_global_variable.excel_fields_TestLab.get("TestStep MR ID")[0], excel_global_variable.excel_fields_TestLab.get("TestStep MR ID")[1]);
        self.excelObject.Range(cell1, cell2).Borders.Weight = 2
        ##Border between TestCase and TestStep, between TestStep and end file
        #range_columns = self.excelObject.Range(self.excelObject.Columns(12), self.excelObject.Columns(20))
        column1 = self.excelObject.Columns(excel_global_variable.excel_fields_TestLab.get("TestStep Name")[1]);
        column2 = self.excelObject.Columns(excel_global_variable.excel_fields_TestLab.get("TestStep MR ID")[1]);
        range_columns = self.excelObject.Range(column1, column2);
        range_columns.BorderAround(Weight=3);
        ##Remove Vertical Borders between B1:K1
        cell1 = self.excelObject.Cells(excel_global_variable.excel_fields_TestLab.get("TestStep")[0], excel_global_variable.excel_fields_TestLab.get("TestStep")[1]);
        cell2 = self.excelObject.Cells(excel_global_variable.excel_fields_TestLab.get("TestStep")[0], excel_global_variable.excel_fields_TestLab.get("TestStep MR ID")[1]);

        self.excelObject.Range(cell1, cell2).Borders.Item(Index=11).LineStyle = -4142;
        
    def autofit_columns(self):
        '''Autofit the weight of columns'''
        ##Set autofit for all columns
        column1 = self.excelObject.Columns(excel_global_variable.excel_fields_TestLab.get("TestSet Directory")[1]);
        column2 = self.excelObject.Columns(excel_global_variable.excel_fields_TestLab.get("TestStep MR ID")[1]);
        self.excelObject.Range(column1, column2).AutoFit();
        
        
        ##Set correct width from TestStep Description to TestStep Actual
        cell1 = self.excelObject.Cells(excel_global_variable.excel_fields_TestLab.get("Modify blank")[0], excel_global_variable.excel_fields_TestLab.get("Modify blank")[1]); 
        cell1.ColumnWidth = 6;
        
        
        column1 = self.excelObject.Columns(excel_global_variable.excel_fields_TestLab.get("TestStep Description")[1])
        column2 = self.excelObject.Columns(excel_global_variable.excel_fields_TestLab.get("TestStep Actual")[1])
        column = self.excelObject.Range(column1, column2);
        column.ColumnWidth = 70.0;        
                ##Set width for TestCase Status
        column = self.excelObject.Columns(excel_global_variable.excel_fields_TestLab.get("TestSet Name")[1])
        column.ColumnWidth = 15.0;
            ##Set width for TestCase Name
        column = self.excelObject.Columns(excel_global_variable.excel_fields_TestLab.get("TestCase Name")[1])
        column.ColumnWidth = 15.0;
            ##Set width for TestCase Status
        column = self.excelObject.Columns(excel_global_variable.excel_fields_TestLab.get("TestCase Status")[1])
        column.ColumnWidth = 8.0;
            ##set width for TestCase MR ID
        column = self.excelObject.Columns(excel_global_variable.excel_fields_TestLab.get("TestCase MR ID")[1])
        column.ColumnWidth = 12.0;
            ##Set width for TestStep Status
        column = self.excelObject.Columns(excel_global_variable.excel_fields_TestLab.get("TestStep Status")[1])
        column.ColumnWidth = 8.0;
            ##Set width for TestStep Name
        column = self.excelObject.Columns(excel_global_variable.excel_fields_TestLab.get("TestStep Name")[1])
        column.ColumnWidth = 12.0
            ##set width for Step MR ID
        column = self.excelObject.Columns(excel_global_variable.excel_fields_TestLab.get("TestStep MR ID")[1])
        column.ColumnWidth = 12.0;
            ##set width for Step SW Build
        column = self.excelObject.Columns(excel_global_variable.excel_fields_TestLab.get("TestStep SW Build")[1])
        column.ColumnWidth = 12.0;
        
    def freezePanes(self):
        '''FreezePanes on row'''
        
        row = self.excelObject.Rows(excel_global_variable.excel_fields_TestLab.get("TestSet Directory")[0])
        row.Select();
        
        #return row; 
        
        
        row.FreezePanes = True;
        
        
        
            
    def cellFormat(self):
        '''set correct cell format'''
        
        ##Set column format for Modify column. 
        column = self.excelObject.Columns(excel_global_variable.excel_fields_TestLab.get("Modify blank")[1]);
            ##set horizontal as xlCenter
        column.HorizontalAlignment = -4108;
            ##set Fonts in this column as Bold
        column.Characters.Font.Bold = True;
        
        
        ##Set column format for Date and Time 
        ##TestCase columns
        column_TCexcDate = self.excelObject.Columns(excel_global_variable.excel_fields_TestLab.get("TestCase Execution Date")[1]);
        column_TCexcDate.NumberFormat = '[$-407]dd-mm-yy';
        
        column_TCexcTime = self.excelObject.Columns(excel_global_variable.excel_fields_TestLab.get("TestCase Execution Time")[1]); 
        column_TCexcTime.NumberFormat = '[$-F400]hh:mm:ss';
        
        column_TCplanExcDate = self.excelObject.Columns(excel_global_variable.excel_fields_TestLab.get("TestCase Planned Execution Date")[1]);
        column_TCplanExcDate.NumberFormat = '[$-407]dd-mm-yy';
        
        column_TCplanExcTime = self.excelObject.Columns(excel_global_variable.excel_fields_TestLab.get("TestCase Planned Execution Time")[1]); 
        column_TCplanExcTime.NumberFormat = '[$-F400]hh:mm:ss';
        
        ##Step columns
        column_TCExcDate = self.excelObject.Columns(excel_global_variable.excel_fields_TestLab.get("TestStep Execution Date")[1]);
        column_TCExcDate.NumberFormat = '[$-407]dd-mm-yy';
        
        column_TCExcTime = self.excelObject.Columns(excel_global_variable.excel_fields_TestLab.get("TestStep Execution Time")[1]); 
        column_TCExcTime.NumberFormat = '[$-F400]hh:mm:ss';

    def wrapText(self):
        '''Disable wrap cells'''
        column1 = self.excelObject.Columns(excel_global_variable.excel_fields_TestLab.get("TestStep Description")[1])
        column2 = self.excelObject.Columns(excel_global_variable.excel_fields_TestLab.get("TestStep Actual")[1])
        column = self.excelObject.Range(column1, column2);
        column.WrapText = False; 
        
        column1 = self.excelObject.Columns(excel_global_variable.excel_fields_TestLab.get("TestStep SW Build")[1])
        column2 = self.excelObject.Columns(excel_global_variable.excel_fields_TestLab.get("TestStep MR ID")[1])
        column = self.excelObject.Range(column1, column2);
        column.WrapText = False; 
        
        
    
    
def main():
    
    excelWorksheets_CInst = ExcelWorksheets(None);
    
    worksheet_number= 0;
#    for worksheet_number in [0, 1, 2]:
#        excelWorksheets_CInst.worksheet_make(worksheet_number);
        
    excelWorksheets_CInst.worksheet_make(worksheet_number);

    #self.excelObject.Workbooks.Close();
    

if __name__ == "__main__":
    
    
    main();
        