'''
Created on 22-08-2010

@author: Lucas
'''
import os
from sys import exit

import csv
from csv import DictReader, DictWriter
from copy import deepcopy

from lib.logfile import Logger
from hashlib import __get_builtin_constructor
log = Logger(loggername="lib.QualityCenter_ext.lib.csvOperation", resetefilelog=False).log;


class FileOpenException(Exception):
    def __init__(self, text):
        log.exception(("FileOpenException: ", repr(text)))
        self.text = repr(text);
        pass
    def __str__(self):
        return self.text;


class ReadCSV(object):
    
    def __init__(self, filename):
        self.openFile = filename;
        self.reader = csv.reader(self.openFile, dialect=csv.excel_tab)
        self.setup();

    def __set_openFile(self, file):
        self.__openFile = open(file, "rb");
    def __get_openFile(self):
        return self.__openFile;
    def __del_openFile(self):
        self.__openFile.close();
    openFile = property(__get_openFile, 
                        __set_openFile, 
                        __del_openFile, 
                        "This is used for open/close given file");
                        
    def setup(self):
        try:
            self.mainQcPart = self.reader.next();##This is list of main qc part naming headers in the csv file, like 'TestPlan', 'TestLab'
            self.mainGroupName = self.reader.next();##This is list of main group naming headers in the csv file, like 'TestSet', 'TestCase'
            self.fieldUserNames =  self.reader.next(); ##This is list of headers in the csv file
        except:
            text = "Given CSV file has no header. Probably wrong file"
            raise FileOpenException(text);
    
    def makeMappingFields(self):
        '''
        Create mapping fields from the csv file
        
        *Input*
        None
        
        *Output*
        @param mappingFields: dict(), {FieldUserName: {FieldOrder, FieldQcName, FieldName, FieldType, FieldIsRequired}}
                        mapp= {
                                'TestCase': {'Planned Language': {'FieldOrder': 28}, 
                                            'Plan: Template': {'FieldOrder': 13}, 
                                            'Plan: Subject': {'FieldOrder': 14}, 
                                            'Iterations': {'FieldOrder': 40},
                                            } 
                                'TestSet': {'Status': {'FieldOrder': 1}, 
                                            'Close Date': {'FieldOrder': 2}, 
                                            'Test Set': {'FieldOrder': 7}, 
                                            'Description': {'FieldOrder': 4}, 
                                            }
                                'Step': {'Status': {'FieldOrder': 44}, 
                                        'Source Test': {'FieldOrder': 45}, 
                                        'Actual': {'FieldOrder': 46},
                                        }
                                }
        '''

        mappingFields = {};
        index=1;
        for item in self.fieldUserNames:
            fieldUserName = item;
            fieldValues = {'FieldOrder':index-1,}
#                           'FieldQcName':'',
#                           'FieldType':'',
#                           'FieldIsRequired':'',
#                           'FieldName':'',
#                           };
            mainGroupNames = self.mainGroupName[:index];
            mainGroupNames.reverse();
            index+=1
            groupName=None;
            for groupName in mainGroupNames:
                if groupName =='':continue;
                else: 
                    if not mappingFields.has_key(groupName):
                        mappingFields[groupName]= {}
                    break;
            if (groupName is None) or (groupName==''):
                continue;
            mappingFields[groupName].update({fieldUserName: fieldValues});
        log.debug(("Mapping field from CSV file:", mappingFields));
        return mappingFields
    
    def getRow(self):
        '''
        This return line from CSV like the Generator, yield
        
        *Input*
        None
        
        *Output*
        @param line: generator list(), list of values from one line in csv file  
        '''
        for line in self.reader:
            yield line;
            
    
    def getRequiredIndex(self, mappingFields):
        '''
        This function will give dict() of all Requested fileds which need to be read. Like TestCaseName, TestCasePath, etc.
        *input*
        @param mapping_fields: dict(), this is dictionary of all possible field group, field name and it's values
                                {"TestSet": {'Test Set Name':{'FieldOrder':'1',
                                                              'FieldQcName':'TS_NAME',
                                                              'FieldUserName':'Test Set Name',
                                                              'FieldType':'char',
                                                              'FieldIsRequired':'True'}
                                            },
                                             'Test Set Directory': {'FieldOrder':'1',
                                                              'FieldQcName':'TS_DIR',
                                                              'FieldUserName':'Test Set Directory',
                                                              'FieldType':'char',
                                                              'FieldIsRequired':'True'},
                                            },
                                 "TestCase": {'Test Case Name': {'FieldOrder':'1',
                                                              'FieldQcName':'TC_NAME',
                                                              'FieldUserName':'Test Case Name',
                                                              'FieldType':'char',
                                                              'FieldIsRequired':'True'},
                                              'Test Case Directory': {'FieldOrder':'1',
                                                              'FieldQcName':'TC_DIR',
                                                              'FieldUserName':'Test Case Directory',
                                                              'FieldType':'char',
                                                              'FieldIsRequired':'True'}
                                             },
                                 "Step": {'Step Name': {'FieldOrder':'1',
                                                        'FieldQcName':'ST_NAME',
                                                        'FieldUserName':'Step Name',
                                                        'FieldType':'char',
                                                        'FieldIsRequired':'True'}
                                          }
                                } 
        
        *output*
        @param dictRequest: dict() result of finding
                            {'TestCaseName': {'FieldQcName':'TS_NAME',
                                              'FieldOrder':'1',
                                              'FieldName':'Test Name'},
                             'TestCaseDirectory':{'FieldQcName':'TS_PATH',
                                                  'FieldOrder':'0',
                                                  'FieldName':'Test Directory'},
                             'TestSetName': {'FieldQcName':'CY_CYCLE',
                                             'FieldOrder':'8',
                                             'FieldName':'Test Set Name'},
                             'TestSetDir': {'FieldQcName':'CY_FOLDER_ID',
                                            'FieldOrder':'9',
                                            'FieldName':'Test Set Directory'},
                             'StepNameTP': {'FieldQcName':'DS_STEP_NAME',
                                            'FieldOrder':'5',
                                            'FieldName':'Step Name'},
                             'StepNameTL': {'FieldQcName':'ST_STEP_NAME',
                                            'FieldOrder':'5',
                                            'FieldName':'Step Name'},
                             };
                            
                            
        '''
        ##This translation is need to operate in reading CSV file only on FieldQcName. 
        ##FieldUserName and FieldName can be different. Only FieldQcName will be stable 
        ##dictRequiredBase is the base translation key
        dictRequiredBase  = {'TestCaseName': {'FieldQcName':'TS_NAME',
                                              'FieldOrder':'',
                                              'FieldName':''},
                             'TestCaseDirectory':{'FieldQcName':'TS_SUBJECT',
                                                  'FieldOrder':'',
                                                  'FieldName':''},
                             'TestSetName': {'FieldQcName':'CY_CYCLE',
                                             'FieldOrder':'',
                                             'FieldName':''},
                             'TestSetDirectory': {'FieldQcName':'CY_FOLDER_ID',
                                                  'FieldOrder':'',
                                                  'FieldName':''},
                             'StepNameTP': {'FieldQcName':'DS_STEP_NAME',
                                            'FieldOrder':'',
                                            'FieldName':''},
                             'StepNameTL': {'FieldQcName':'ST_STEP_NAME',
                                            'FieldOrder':'',
                                            'FieldName':''},
                             };
        
        
        dictRequired = {};
        for groupFieldValue in mappingFields.itervalues():
            for fieldName, fieldValues in groupFieldValue.iteritems(): ##go thru the user map choose fields 
                fieldQcName = fieldValues['FieldQcName']; ##get FieldQcName
                if fieldQcName == "": continue;
                for fieldUserNameReq, fieldValuesReq in dictRequiredBase.iteritems(): ##Go thru translate dictionary  
                    if fieldQcName in fieldValuesReq['FieldQcName']:
                        fieldOrder = fieldValues['FieldOrder']; ##based on information from map choose fields I will now also column number from CSV file
                        fieldValueNew = {'FieldQcName':fieldQcName ,'FieldOrder':fieldOrder, 'FieldName':fieldName};
                        dictRequired.update({fieldUserNameReq:fieldValueNew}); ##create new dictionary with bullet proof of basic column naming and its connection to QC database
                        break;
        return dictRequired;
        
        
    def countTestsInFile(self, mappingFields):
        '''
        This will count how many Tests is in given file
        *input*
        @param mapping_fields: dict(), this is dictionary of all possible field group, field name and it's values
                                {"TestSet": {'Test Set Name':{'FieldOrder':'1',
                                                              'FieldQcName':'TS_NAME',
                                                              'FieldUserName':'Test Set Name',
                                                              'FieldType':'char',
                                                              'FieldIsRequired':'True'}
                                            },
                                             'Test Set Directory': {'FieldOrder':'1',
                                                              'FieldQcName':'TS_DIR',
                                                              'FieldUserName':'Test Set Directory',
                                                              'FieldType':'char',
                                                              'FieldIsRequired':'True'},
                                            },
                                 "TestCase": {'Test Case Name': {'FieldOrder':'1',
                                                              'FieldQcName':'TC_NAME',
                                                              'FieldUserName':'Test Case Name',
                                                              'FieldType':'char',
                                                              'FieldIsRequired':'True'},
                                              'Test Case Directory': {'FieldOrder':'1',
                                                              'FieldQcName':'TC_DIR',
                                                              'FieldUserName':'Test Case Directory',
                                                              'FieldType':'char',
                                                              'FieldIsRequired':'True'}
                                             },
                                 "Step": {'Step Name': {'FieldOrder':'1',
                                                        'FieldQcName':'ST_NAME',
                                                        'FieldUserName':'Step Name',
                                                        'FieldType':'char',
                                                        'FieldIsRequired':'True'}
                                          }
                                } 
        
        *output*
        @param maxItemsNumber: int() number of Tests
        '''
        maxItemsNumber = 0;
        
        
        dictRequired = self.getRequiredIndex(mappingFields);
        ##Check if this dictionary works. If not, then exit
#        try: testCaseNameIndex = mappingFields['TestCase'].get('Test Name').get('FieldOrder');
        try: testCaseNameIndex = dictRequired['TestCaseName'].get('FieldOrder');
        except: return maxItemsNumber;
        
        
        for line in self.getRow(): ##go thru all lines in csv file and count Tests
            testCaseNameIndex = dictRequired['TestCaseName'].get('FieldOrder');
            try: testCaseNameIndex  = int(testCaseNameIndex);
            except: continue;
            testCaseName = line[testCaseNameIndex];
            
            if testCaseName != "":
                maxItemsNumber+=1;
        
        ##Reset file position
        self.openFile.seek(0)##because we have read all file, we have to set the pointer at the beginning of file. Without, file pointer will be at the end
        self.setup()##one more time read the headers. And now file pointer is on place where we can read Test data
        
        return maxItemsNumber;
            
    
    
class WriteCSV(object):
    def __init__(self, filename, mappingFields):
        '''
        Create already created file. 
        input = str(mainDirectory), dict(fields);
        '''
        self.mappingFields = mappingFields
#        filename = strftime('Download_%d-%m-%Y_%H-%M-%S.xls', localtime());
        self.csvFilename = filename;
        self.f_download = self.setup(self.csvFilename);##instance of the open to write file
        mainheaderfieldnames, mainfieldnames, fieldnames, self.mappingFields = self.fieldnames(self.mappingFields);##return headers and edited mappingFields, by the Order nummber
        self.__numberOfCells = len(fieldnames);
        self.blankRow();##create blank row;
        self.row = ['']*len(fieldnames); ##this is row, which will go to write
        self.writer = csv.writer(self.f_download, dialect=csv.excel_tab);##create instance of csv writer
        self.add_heading(mainheaderfieldnames, mainfieldnames, fieldnames);##create headers in the csv file
    #------------------------------------------------------------------------------ 
    def __get_fDownload(self):
        return self._f_download;
    def __set_fDownload(self, fileInstance):
        self._f_download = fileInstance;
    def __del_fDownload(self):
        self._f_download.flush();
        self._f_download.close();
    f_download = property(__get_fDownload, __set_fDownload, __del_fDownload, "Instance of the opened file");
   
    def __get_numberOfCells(self):
        return self.__numberOfCells;
    numberOfCells = property(__get_numberOfCells, None, None, "This will give information, how big is row");
    
    #------------------------------------------------------------------------------ 
    def fieldnames(self, mappingFields):
        '''
        Create list of fields which has been chosen by user. This list fields will be a header for file.
        Additionally mappingFields will be updated with new order number
        *Input* 
        @param filedDict: dict(filedDict),  
             filedDict =     {
                               'Step': {
                                        'Attachment': {
                                                       'FieldIsRequired': 'False',
                                                       'FieldOrder': '2',
                                                       'FieldQcName': 'ATTACHMENT',
                                                       'FieldType': 'char'},
                                        'Status': {
                                                   'FieldIsRequired': 'False',
                                                   'FieldOrder': '0',
                                                   'FieldQcName': u'ST_STATUS',
                                                   'FieldType': u'char'},
                                        'Step Name': {
                                                      'FieldIsRequired': 'False',
                                                      'FieldOrder': '1',
                                                      'FieldQcName': u'ST_STEP_NAME',
                                                      'FieldType': u'char'}},
                               'TestCase': {
                                            'Attachment': {
                                                           'FieldIsRequired': 'False',
                                                           'FieldOrder': '5',
                                                           'FieldQcName': 'ATTACHMENT',
                                                           'FieldType': 'char'},

        *Output*
        @param mainfieldnames: list(). List of main column group naming, like 'TestSet', 'TestCase' 
        @param fieldnames: list() List of all column names
                            ['TL_Case Responsible', 'TL_Case Name', ....]
        @param mappingFields_copy: dict() mappingFields with updated new order numbering 
            filedDict_copy =     {
                                 'Step': {
                                        'Attachment': {
                                                       'FieldIsRequired': 'False',
                                                       'FieldOrder': '3',
                                                       'FieldQcName': 'ATTACHMENT',
                                                       'FieldType': 'char'},
                                        'Status': {
                                                   'FieldIsRequired': 'False',
                                                   'FieldOrder': '2',
                                                   'FieldQcName': u'ST_STATUS',
                                                   'FieldType': u'char'},
                                        'Step Name': {
                                                      'FieldIsRequired': 'False',
                                                      'FieldOrder': '1',
                                                      'FieldQcName': u'ST_STEP_NAME',
                                                      'FieldType': u'char'}},
                               'TestCase': {
                                            'Attachment': {
                                                           'FieldIsRequired': 'False',
                                                           'FieldOrder': '0',
                                                           'FieldQcName': 'ATTACHMENT',
                                                           'FieldType': 'char'},
                                 }
        '''
        
        ##Before running thru the given Column names given in mappingFields, I have to sort. 
        ##In standard way it should be 'TestSet', 'TestCase', 'Step', then if exists others
        groupList = mappingFields.keys();
        groupList_ = []
        if 'TestSet' in groupList:
            groupList_.append('TestSet');
            del groupList[groupList.index('TestSet')]
        if 'TestCase' in groupList:
            groupList_.append('TestCase');
            del groupList[groupList.index('TestCase')]
        if 'Step' in groupList:
            groupList_.append('Step');
            del groupList[groupList.index('Step')]
        if len(groupList)>0:
            groupList_.extend(groupList);
        
        fieldnames=[]; ##this is list of all columns names
        mainfieldnames=[]; ##this is list of main fields naming, like 'TestSet', 'TestCase'
        mainheaderfieldnames=[]; ##this is list of main header fields naming, like 'TestLab', 'TestPlan'
        mappingFields_copy = deepcopy(mappingFields);
        for group in groupList_:
            num = len(mappingFields_copy[group])
            if num==0: continue;
            mainfieldnames_sub=['']*num; ##create blank sub list of main columns. At the end I will merge all those sub's
            mainfieldnames_sub[0] = group;
            fieldnames_sub = ['']*num; ##create blank sub list of columns. At the end I will merge all those sub's 
            for colName, fieldValues in mappingFields_copy[group].iteritems():
                try: colNumber = int(fieldValues.get('FieldOrder'));
                except ValueError: continue;
                
                fieldnames_sub[colNumber] = colName; ##based on the column/order number fill the fieldnames list

                
                colNumber = colNumber + len(fieldnames);##add to the known colNumber the last length of fields group
                
                fieldValues.update({'FieldOrder':str(colNumber)});
#                fieldValues = mappingFields_copy[group].get(colName)
#                fieldValues = (fieldValues[0],fieldValues[1],fieldValues[2], str(colNumber)) ##reedit the column Number
#                
#                mappingFields_copy[group].update({colName:fieldValues});##update column number
            mainfieldnames.extend(mainfieldnames_sub);
            fieldnames.extend(fieldnames_sub);
            
        
        ##Create first line in CSV file, like TestLab, TestPlan 
        mainheaderfieldnames=deepcopy(mainfieldnames)
        for col in mainheaderfieldnames: 
            if col == 'TestCase':##update this index by TestPlan
                mainheaderfieldnames[mainheaderfieldnames.index('TestCase')] = 'TestPlan'
            if col == 'Step':##update this index by TestPlan
                mainheaderfieldnames[mainheaderfieldnames.index('Step')] = 'TestPlan'
            elif col == 'TestSet':##update this index by TestLab
                mainheaderfieldnames[mainheaderfieldnames.index('TestSet')] = 'TestLab';
        
        
        return mainheaderfieldnames, mainfieldnames, fieldnames, mappingFields_copy 
        
    def setup(self, csvFilename):
        ##open file as write
        
        if not os.path.exists(csvFilename):
            try:
                f_download = open(csvFilename, 'wb');
                log.debug(("Created file:", csvFilename));
            except:
                text = ("\nFile was edited and saved under Microsoft Office Excel format\n"\
                        "Please reopen this file and Save As *.txt tab delimited\n"\
                        "File: ", csvFilename)
                log.info(text);
                raise FileOpenException(text)
                return False
                
        else:
            text = ("\nFile already exists\n"\
                    "File: ",csvFilename)
            log.info(text);
            raise FileOpenException(text)
            return False;
        return f_download

    def add_heading(self, mainheaderfieldnames, mainfieldnames, fieldnames):
        '''
        Add column names
        
        *Input*
        @param mainheaderfieldnames: list(), list of main header fields group namings, like ['TestLab', '', '', 'TestPlan']
        @param mainfieldnames: list(), list of main fields group namings, like ['TestSet', '', '', 'TestCase'] 
        @param fieldnames: list['TP_Case Name', 'TP_Step Name'];
        
        *Output*
        None
        '''
        self.writer.writerow(mainheaderfieldnames);
        self.writer.writerow(mainfieldnames);
        self.writer.writerow(fieldnames);
        
    def updateCell(self, groupName, fieldUserName, fieldValue):
        '''
        This will update one index in the self.row, by given fieldUserName and its value
        
        *Input*
        @param groupName: str(), this is group Name from mappingFields, like "TestSet", "TestCase", "Step" 
        @param fieldUserName: str(), this is field user name from the mappingFields  'Test Set Name'
        @param fieldValue: str(), this is value of the given fieldUserName
        
        *Output*
        None
        '''
        if self.mappingFields.has_key(groupName):
            if self.mappingFields[groupName].has_key(fieldUserName):
                orderNumber = self.mappingFields[groupName].get(fieldUserName).get('FieldOrder');
                try: orderNumber = int(orderNumber);
                except: log.error(("Given fieldUserName, its OrderNumber can not be translated to int(): ", fieldUserName)) 
                self.row[orderNumber] = fieldValue;
            else:
                log.error(("Given fieldUserName does not exists in the mappingFields: ", fieldUserName))
        else:
            log.error(("Given groupName does not exists in the mappingFields:", groupName));
        
    def blankRow(self):
        '''
        Create blank row, with the size of numberOfCells
        
        *Input*
        None
        
        *Output*
        @param self.row:  list(), this is row with blank data ''  
        '''
        self.row = ['']*self.numberOfCells;
        
    def addRow(self):
        '''
        Save one row

        *Input*
        @param row:= list(), list of fieldValues example ["Passed", "12", "Open your Web browser and type the"]
        
        *Output*
        None
        '''
        self.writer.writerow(self.row);
        self.f_download.flush();
        self.blankRow();


def checkFile(file):
    if not os.path.exists(file):
        log.info(("ERR: Given file does not exists. File:",file)); 
        return False
    else:
        return True


    
    