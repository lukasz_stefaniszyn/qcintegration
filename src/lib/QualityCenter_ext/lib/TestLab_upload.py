#!/usr/bin/env python

'''
Created on 2009-09-16

@author: Lukasz Stefaniszyn
'''

import win32com.client

import os
import sys
import re
from time import localtime, strftime
import pywintypes
import types
import csvOperation

import qc_connect_disconnect


from lib.logfile import Logger
from globalVariables import GlobalVariables
log = Logger(loggername="lib.QualityCenter_ext.lib.TestLab_upload", resetefilelog=False).log;



#==============================================================================
# Global parameters
#==============================================================================
DST_FOLDER = GlobalVariables.DIR_RESULT

class TestSetException(Exception):
    def __init__(self, text):
        log.exception(("TestSetException: ", repr(text)))
        self.text = repr(text);
        pass
    def __str__(self):
        return self.text;



class TestLabUpload(object):
    '''This will make all action that will take Values from QC'''

    #------------------------------------------------------------------------------ 
    def __init__(self, qc):
        self.__tm = qc.TestSetTreeManager;
        self.__root = qc.TestSetTreeManager.Root;
        
    #------------------------------------------------------------------------------ 
    def __get_tm(self):
        return self.__tm;
    tm = property(__get_tm);
    
    def __get_root(self):
        return self.__root;
    root = property(__get_root);
    #------------------------------------------------------------------------------

class TestSet(object):
    '''
    Operation on ONE test Set
    '''

    def __init__(self, testLabUpload, testSetDir, testSetName,  mappingFields):
        '''
        @param testLabUpload: instance(), this is class instance of TestLabUpload 
        @param testSetDir: str() , directory in the TestLab, where to search TestSet 
        @param testSetName: str(), this is test set name
        @param mappingFields: this is user choosen fields only for TestSet
                          {"TestSet": {'Test Set Name':('TS_NAME', 'char', 'True', '0'),
                                     'Test Set Directory': ('TS_DIR', 'char', 'True', '1'),
                                     'Test Set Description': ('TS_DESC', 'char', 'True', '2')
                                     },
                          };
        '''
        self.tm = testLabUpload.tm
        self.root = testLabUpload.root;
        self.mappingFields = mappingFields;
        
        self.__testSetName = testSetName;
        
        self.getTestSet(testSetDir, testSetName); ##this will create us TestSetInstance
            
        
    #------------------------------------------------------------------------------ 
    def __get_testSetName(self):
        return self.__testSetName
    testSetName = property(__get_testSetName, None, None, "Test Set name as string");
    
    def __get_testSetInstance(self):
        return self.__testSetInstance;
    def __set_testSetInstance(self, testSetInstance):
        self.__testSetInstance = testSetInstance
    testSetInstance = property(__get_testSetInstance, __set_testSetInstance, None, "TestSet QualityCenter instance");
    #------------------------------------------------------------------------------ 
    def getTestSet(self, testSetDir, testSetName):
        '''
        Return one TestSet instance from given TestSet directory and given TestSet name
        
        *Input* 
        @param testSetDir: str() , directory in the TestLab, where to search TestSet 
        @param testSetName: str(), this is test set name 
        
        *Output* 
        This could raise TestSetException
        '''
        try:
            dirInstance = self.tm.NodeByPath(Path=testSetDir)
        except:
            dirInstance = None;
            raise TestSetException(("INFO: Unable find given TestSet directory: %s"%repr(str(testSetDir))));
            return;
        testSetInstanceList = dirInstance.FindTestSets(Pattern=testSetName, MatchCase=True);
        if testSetInstanceList is None:
            raise TestSetException(("INFO: Unable find given TestSet : %s"%repr(str(testSetName))));
            return
        else:
            if testSetInstanceList.Count >1:
                log.debug(("Found more than 1 TestSet with pattern name:",testSetName));
            self.testSetInstance = testSetInstanceList[0];
    
    def setFieldsValue(self, fieldUserName, fieldValue):
        
        if self.mappingFields.has_key(fieldUserName):
            fieldName = self.mappingFields[fieldUserName].get('FieldQcName')
            if fieldUserName =="Attachment":
                if fieldValue==None or fieldValue=='': return;
                uploadAttachments(self.testSetInstance, fieldValue);
                return;
            else:
                setFieldValue(self.testSetInstance, fieldName, fieldValue)

class AllTestCases(object):
    '''
    Operation on ALL test cases from given TestSet instance
    '''
    
    def __init__(self, testSetInstance, testSetName):
        self.__testSetName = testSetName
        self.__testSetInstance = testSetInstance;
        self.__testCaseAllDict = {};
        
    #------------------------------------------------------------------------------ 
    def __get_testSetName(self):
        return self.__testSetName;
    testSetName = property(__get_testSetName, None, None, "Test Set name as string");
    
    def __get_allDict(self):
        return self.__testCaseAllDict;
    def __set_allDict(self, instanceList):
        self.__testCaseAllDict = makeDictOfInstance(instanceList);
    testCaseAllDict = property(__get_allDict, __set_allDict, None, "This is dict() of all test cases instance {'TestCaseName': instance(testCase)}");    
    def __get_testSetInstance(self):
        return self.__testSetInstance;
    testSetInstance = property(__get_testSetInstance, None, None, "TestSet QualityCenter instance");
    #------------------------------------------------------------------------------ 
    
    def getAllTestCases(self):
        '''Create list of TestCases which are under given TestSet_instance
        *Input*
        None
        
        *Output*
        @param allTestCases:  list() of TestCases from given TestSet instance 
        '''
        
        testSetFactory = self.testSetInstance.TSTestFactory;
        if testSetFactory is not None:
            allTestCases = testSetFactory.NewList("");
            self.testCaseAllDict = allTestCases;
        else:
            log.info("No TestCase instance in given TestSet");
        return allTestCases;    
    
    
class TestCase(object):
    '''
    Operation on ONE test case    
    
    @testcaseInstance = qcInstance(TestCase)
    @param testCaseName: str(), this is TestCase name
    @mapping_fields =this is user choosen fields only for TestCase
                          {"TestCase": {'Test Case Name':('TC_NAME', 'char', 'True', '0'),
                                     'Test Case Directory': ('TC_DIR', 'char', 'True', '1'),
                                     'Test Case Description': ('TC_DESC', 'char', 'True', '2')
                                     },
                          };
    '''
    def __init__(self, testCaseInst, testCaseName, mappingFields):
        self.__testCaseInstance = testCaseInst;

        self.__testCaseName = testCaseName 
        

        self.mappingFields = mappingFields;
    #------------------------------------------------------------------------------ 
    def __del__(self):
        try: 
            self.testCaseInstance.Post();
            self.testCaseInstance.UnLockObject();
        except: pass;
        
    def __get_testCaseName(self):
        return self.__testCaseName;
    testCaseName = property(__get_testCaseName, None, None, "Test Case name as string");
    
    
    def __get_testCaseInstance(self):
        return self.__testCaseInstance;
    testCaseInstance = property(__get_testCaseInstance, None, None, "TestCase QualityCenter instance");
    #------------------------------------------------------------------------------ 


    def setFieldsValue(self, fieldUserName, fieldValue):
        
        if self.mappingFields.has_key(fieldUserName):
            fieldName = self.mappingFields[fieldUserName].get('FieldQcName')
            if fieldUserName =="Attachment":
                if fieldValue==None or fieldValue=='': return;
                uploadAttachments(self.testCaseInstance, fieldValue);
                return;
#            elif fieldUserName =="Status":
#                if fieldValue==None or fieldValue=='': return;
#                    self.runNameTestCase
            else:
                setFieldValue(self.testCaseInstance, fieldName, fieldValue)

class AllSteps(object):
    '''
    Operation on ALL steps in the given test case
    '''
    
    def __init__(self, testCase, testSetName):
        self.__testSetName = testSetName
        self.__testCase = testCase;
        self.__runInstance = None;
        self.__stepAllDict = {};
        
        self.startRun();
        
    def __del__(self):
        self.endRun();
    #------------------------------------------------------------------------------ 
    def __get_testSetName(self):
        return self.__testSetName;
    testSetName = property(__get_testSetName, None, None, "Test Set name as string");
    def __get_testCaseName(self):
        return self.__testCase.testCaseName;
    testCaseName = property(__get_testCaseName, None, None, "Test Case name as string");
    
    def __get_allDict(self):
        return self.__stepAllDict;
    def __set_allDict(self, instanceList):
        self.__stepAllDict = makeDictOfInstance(instanceList);
    stepsAllDict = property(__get_allDict, __set_allDict, None, "This is dict() of all test cases instance {'StepName': instance(step)}");
    
    def __get_testCaseInstance(self):
        return self.__testCase.testCaseInstance;
    testCaseInstance = property(__get_testCaseInstance, None, None, "TestCase QualityCenter instance");
    
    def __get_runInstance(self):
        return self.__runInstance
    runInstance = property(__get_runInstance, None, None, "Run Instance QualityCenter");
    
    #------------------------------------------------------------------------------ 

    def startRun(self):
        ##Run execution on TestCase instance
        runTestCase = self.testCaseInstance.RunFactory;
        ##Make naming for new run execution
        runName = strftime('AutoRun_%Y-%m-%d_%H-%M-%S', localtime())
        log.info(("\t\tRun name:\"%s\""%(runName)));
        
        self.__runInstance = runTestCase.AddItem(runName);
        self.__runInstance.CopyDesignSteps();
        self.__runInstance.Post();
        
    def endRun(self):
        '''
        Close Run excecution on TestCase instance, by updating the Run Status
        '''
         
        log.info(("\t\t\t\tUPDATE run status"));
        
        
        if self.stepsAllDict == {}:
            log.info(("No update on run status"));
            return
        
        step_listStatus = [];
        for stepInstance in self.stepsAllDict.values():
            step_listStatus.append(unicode(stepInstance.Status));
        self.runInstance.AutoPost = 'True';
        if 'Failed' in step_listStatus:
            self.runInstance.Status = 'Failed';
        ## To set Not Completed if only one Steps status is No Run or N/A or Not Completed
        elif (("No Run") or ("N/A") or ("Not Completed")) in step_listStatus:
            self.runInstance.Status = "Not Completed";
        ## To set Passed when there will be only Passed /Not Analyzed / Posponed / Blocked 
        elif 'Passed' in step_listStatus:
            self.runInstance.Status = "Passed";
        ## To set one of neutral if there are only NotAnalyzes and/or Postponed and/or Blocked
        elif ("Not Analyzed" in step_listStatus) or \
               ("Postponed" in step_listStatus) or \
               ("Blocked" in step_listStatus) or \
               ("Deferred" in step_listStatus) :
            curr_status = filter(lambda StepStatus: not ( ("Not Analyzed" in StepStatus) or \
                                                          ("Postponed" in StepStatus) or \
                                                          ("Blocked" in StepStatus) or \
                                                          ('Deferred' in StepStatus)), \
                                                          step_listStatus);
            if curr_status == []:
                self.runInstance.Status = step_listStatus[-1];
        else:
            log.info(("\t\t\t!!! Unknown STATUS "));





    def getAllSteps(self):
        '''Create list of Steps from last Run in TestCase'''
        
        
        StepFactor = self.runInstance.StepFactory;
        allSteps = StepFactor.NewList("")
        self.stepsAllDict = allSteps;
        
        return allSteps;

class Step(object):
    '''
    Operation on ONE step in the given test case
    input = qcInstance(step), str(testCaseName)
    '''
    
    def __init__(self, stepInstance, testSetName, testCaseName, stepName, mappingFields):
        self.__stepInstance = stepInstance;
        self.__testCaseName = testCaseName;
        self.__testSetName = testSetName;
        self.__stepName = stepName;
        
        self.mappingFields = mappingFields;
        
    def __del__(self):
        del self.__stepInstance
    #------------------------------------------------------------------------------ 
    def __get_stepInstance(self):
        return self.__stepInstance;
    stepInstance = property(__get_stepInstance, None, None, "Step QualityCenter instance");
    
    def __get_testSetName(self):
        return self.__testSetName;
    testSetName = property(__get_testSetName, None, None, "Test Set name as string");
    def __get_testCaseName(self):
        return self.__testCaseName;
    testCaseName = property(__get_testCaseName, None, None, "Test Case name as string");
    def __get_stepName(self):
        return self.__stepName;
    stepName = property(__get_stepName, None, None, "Step name as string");
    
    #------------------------------------------------------------------------------ 
    def setFieldsValue(self, fieldUserName, fieldValue):
        
        if self.mappingFields.has_key(fieldUserName):
            fieldName = self.mappingFields[fieldUserName].get('FieldQcName')
            if fieldUserName =="Attachment":
                if fieldValue==None or fieldValue=='': return;
                uploadAttachments(self.stepInstance, fieldValue);
                return;
            else:
                setFieldValue(self.stepInstance, fieldName, fieldValue)








def makeDictOfInstance(instanceList):
    '''
    This will create dictionary of all QC instances from the given parent QC instance
    This will be used in the AllTestCases, AllSteps  
    
    *Input*
    @param instanceList: list(qc_instance)
    
    *Output*
    @param allDict: disct(),  dictionary of given instanceList
                            {str('TestCaseName'): instance(TestCase)} 
    '''
    allDict = {}
    for instance in instanceList:
        name = decodeText(instance.Name);
#        name = repr(name);
        allDict[name] = instance;
    return allDict;

def setFieldValue(instance, fieldName, fieldValue):
    '''
    This will update field value
    
    *Input*
    @param fieldName: this is QC field name, like  TS_NAME
    @param fieldValue: str(), like 'Passed', '09\07\203'
    
    *Output*
    None
    '''
#    log.debug(("getFieldValue for fieldName, fieldType:",(fieldName, fieldType)));
    try:
        instance.SetField(fieldName, fieldValue);
        instance.Post();
    except:
        log.info(("Unable to update Field: %s with value: %s")%(fieldName, fieldValue));
    

def list_attachments(instance):
        '''Create dictionary of Attachments in instance
        input = instance()
        output = dict{attachment_name:attachment_instance}'''
        
        
        list_attachements = {};
        for attachment_inst in instance.Attachments.NewList(""):
            attachment_name = attachment_inst.Name.split('_', 2)[-1];
            list_attachements[attachment_name] = attachment_inst;
        return list_attachements;

def uploadAttachments(instance, attachments):   
    '''This will upload attachments into QC database 
    input = inst(Instance), str(Attachements)
    output = None;
    example, uploadAttachments(TestCase_instance, str(c:\tmp\test1.txt; c:\test2.txt);
    '''
    
    listAttachments = list_attachments(instance);
    
    log.debug(("attachments:\"%s\""%attachments));
    for attachment in attachments.split(';'): 
        if attachment == "":
            continue;
        attachment = attachment.strip();
        attachment_fileName = os.path.basename(attachment);
        

        ##Check if given attachments are present
        if os.path.exists(attachment):            
            if listAttachments.has_key(attachment_fileName):
                ##IF the attachment to upload will be the same (in name) as the one already on the QC server, then remove this from server
                log.debug(("\tRemoving duplicated attachment:",attachment_fileName)); 
                attachment_inst_remove = listAttachments.get(attachment_fileName);
                instance.Attachments.RemoveItem(attachment_inst_remove.ID);
            
            att = instance.Attachments.AddItem(None);
            att.FileName = r'%s'%attachment;
            ##Upload file to QC;
            att.Type = 1;
            log.debug(("Adding attachment \"%s\" "%attachment));
            att.Post();
        else:
            log.info(("INFO: Given attachment \"%s\" does not exists. Skipping add"%attachment));
    return;

def getAttachment(instance, targetDir):
        '''This will take all attachments from QualityCenter database and save into user directory (TagedDir)
        input = str(os.path(targetDir)) 
        output = str(;attachmentDir;attachmentDir;attachmentDir) OR str('')
        '''
        
        f_attachments = open(os.path.join(DST_FOLDER,"missingAttachments.txt"), "a");
        
        attDstAll = '';
        ##Check if in instance are any attachments
        if (instance.HasAttachment):
            dstFolder = targetDir; 
            ##Make catalog of this Destination Folder
            try:
                os.makedirs(dstFolder);
            except:
                log.info("Unable to create Destination Folder for keep attachment. Skipping attachment")
                return attDstAll;
            
            for att in instance.Attachments.NewList(""):
                ##for every attachment make download it to local machine
                attName = att.GetName();
                attName = decodeText(attName);
                if att.FileSize == 0:
                    information = "Attachment is empty: %s     in : %s"%(attName.split("_", 2)[-1], repr(dstFolder));
                    log.debug(information);
                    f_attachments.write(information+'\n');
                    f_attachments.flush();
                    continue;
                ##This Load will download attachment from QC database to local machine, but to the default user catalog 
                ##synchronize If True, program run waits for download to complete. Otherwise, download is asynchronous. 
                attLocalCatalog = att.Load(True, ""); 
                ##This will move this downloaded attachment file to Result file 
                attSrc = os.path.join(attLocalCatalog,attName);
                ##Create name of the Destination Folder, where attachment from this instance will be keept
                attDst = os.path.join(dstFolder,attName.split("_", 2)[-1]); 
                attDstAll += ';' + attDst;
                try:
                    os.renames(attSrc, attDst);
                except:
                    log.info(("WRN: Unable to get attachment file from QualityCenter, due to the insufficient permission in moving to catalog or catalog/file already exists: ",repr(dstFolder)));
        attDstAll = attDstAll.strip(';');
        
        f_attachments.close();
        return attDstAll;

def decodeText(value):
#        value = repr(value)
#        value = value.lstrip("u'")
#        value = value.rstrip("'")
#        value = unicode(value, errors="ignore")
        try:
            value = value.encode('ascii', 'ignore');
        except: 
            try: 
                value = repr(value);
            except:
                value = "Wrong data";
                log.debug("Not able to decode Text");
        return value;       


def checkFile(filename):
    '''This will check if given file exist'''
    if not os.path.exists(filename):
        log.info((("Not able to find file:"), filename));
        return False;
    return True;

class ProgressDialog(object):
    
    def __init__(self, progressExecutionDialog=None):
        self.progressExecutionDialog = progressExecutionDialog;
        
    def checkcancelStatus(self):
        if self.progressExecutionDialog is not None:
            return self.progressExecutionDialog.cancelStatus;
        else: 
            return False;
        
        
    def updateTextDialog(self, text):
        '''
        Write new line to Dialog Gui 
        *input*
        @param text: str()
        *output*:
        None
        '''
        if self.progressExecutionDialog is not None:
            self.progressExecutionDialog.updateTextDialog(text);
        else: 
            print(text);
        log.info(text);
            
    def updateProgressBar(self):
        '''
        Update number+1 on Dialog Gui of ProgressBar
        *input*
        None
        *output*
        None
        '''
        if self.progressExecutionDialog is not None:
            self.progressExecutionDialog.updateProgressBar();

class MainExecution(object):
    
    def __init__(self, 
                 gui=False,
                 cmd=False,
                 qc=None, 
                 mappingFields=None,
                 filename=None,
                 progressExecutionDialog=None):
        
        self.__mappingFields = mappingFields;
        self.__qc = qc;
        self.__filename=filename
        self.__progressDialog = None;
        self.path = None;
        
        ##This will be used in the Gui and CMD run. This will show progress of execution
        self.__progressDialog = ProgressDialog(progressExecutionDialog);
        
        
        if gui and not cmd:##start Gui part
            self.path = self.mainGui();
        elif cmd and not gui:##start CMD part
            self.path = self.mainCMD()
        else:
            log.error(("Decide which main script have to be executed: Gui or CMD"));

    def __del__(self):
        self.cleanBeforeClose();

    def get_filename(self):
        return self.__filename
    filename = property(get_filename, None, None, "filename's docstring")

    def get_progress_dialog(self):
        return self.__progressDialog
    progressDialog = property(get_progress_dialog, None, None, "progressDialog's docstring")

    def get_qc(self):
        return self.__qc
    def del_qc(self):
        del self.__qc
    qc = property(get_qc, None, del_qc, "qc's docstring")
            

    def get_mapping_fields(self):
        return self.__mappingFields
    mappingFields = property(get_mapping_fields, None, None, "mappingFields's docstring")

    def cleanBeforeClose(self):
        del self.step;
        del self.allSteps;
        del self.testCase;
        del self.testSet;

    def mainGui(self):
        
        self.main()
        try: del self.readCsv;
        except: pass;
        
    def mainCMD(self):
        
        self.main()
        try: del self.readCsv;
        except: pass;


    def main(self):
        '''
        
        @param qc:
        @param filename:
        @param columnGroupMappingFields: this dict() is given by Gui interface
    #    {"TestSet": {"TestSetName": 
    #                                {ColumnName:"TS_NAME", colNumber: 1}
    #                }
        }
        {'Step': {
                'Step Description': {'FieldIsRequired': 'True',
                                   'FieldName': 'QC Step Description',
                                   'FieldOrder': '1',
                                   'FieldQcName': 'ST_DESC',
                                   'FieldType': 'char'},
                 'Step Name': {
                                   'FieldIsRequired': 'True',
                                   'FieldName': 'QC Step Name',
                                   'FieldOrder': '0',
                                   'FieldQcName': 'ST_NAME',
                                   'FieldType': 'char'}},
         'TestCase': {
                     'Test Case Directory': {
                                               'FieldIsRequired': 'True',
                                               'FieldName': 'QC Test Case Directory',
                                               'FieldOrder': '1',
                                               'FieldQcName': 'TC_DIR',
                                               'FieldType': 'char'},
        }}
        '''
        
        if not checkFile(self.filename):
            log.debug(("Given file does not exists: ",repr(self.filename)))
            return False
    
        self.readCsv = csvOperation.ReadCSV(self.filename);
        self.dictRequired = self.readCsv.getRequiredIndex(self.mappingFields); ##this will switch key in dict() to have FieldQcName
        testLabUpload= TestLabUpload(self.qc);
    
        self.testSet = None;
        self.testCase = None;
        self.allSteps = None;
        self.step = None;
    
        for line in self.readCsv.getRow():
            if self.progressDialog.checkcancelStatus(): 
                self.cleanBeforeClose();
                break;##check if user did not pressed Cancel button. If yes, then stop execution
            
            try:
                testSetDirIndex = self.dictRequired['TestSetDirectory'].get('FieldOrder');
                testSetDirIndex  = int(testSetDirIndex);
            except: continue;  
            testSetDir = line[testSetDirIndex];
            
            try:
                testSetNameIndex = self.dictRequired['TestSetName'].get('FieldOrder');
                testSetNameIndex = int(testSetNameIndex);
            except: continue;
            testSetName = line[testSetNameIndex];
            #==========================================================================
            # Part responsible for TestSets
            #==========================================================================
            if (testSetName and testSetDir)!= "": 
                
                
                try: 
                    self.testSet = TestSet(testLabUpload, testSetDir, testSetName, self.mappingFields['TestSet'])
                except TestSetException:
                    self.testSet = None;
                    continue;
                
                self.progressDialog.updateTextDialog("TestSet: %s"%repr(self.testSet.testSetName));
                
                allTestCases = AllTestCases(self.testSet.testSetInstance, testSetName);
                allTestCases.getAllTestCases();
    
                for fieldUserName, fieldValues in self.mappingFields['TestSet'].items():
                    ##do not repeat operation on column name which was already read in dictRequired
                    status = [fieldUserName==fieldValuesReq['FieldName'] for fieldValuesReq in self.dictRequired.values()]
                    if True in status:
                        continue;
                    
                    try: 
                        fieldLineIndex = fieldValues.get('FieldOrder')
                        fieldLineIndex  = int(fieldLineIndex);
                    except: continue; 
                    fieldLineValue = line[fieldLineIndex];
                    self.testSet.setFieldsValue(fieldUserName, fieldLineValue);
                continue;
                
                
            #======================================================================
            # Part responsible for TestCases 
            #======================================================================
            if self.testSet is not None:
                try:
#                    testCaseNameIndex = mappingFields['TestCase'].get('Plan: Test Name').get('FieldOrder');
                    testCaseNameIndex = self.dictRequired['TestCaseName'].get('FieldOrder');
                    testCaseNameIndex = int(testCaseNameIndex);
                except: continue;
                testCaseName = line[testCaseNameIndex];
                if testCaseName != "": 
                    if allTestCases.testCaseAllDict.has_key(testCaseName):
                        del self.allSteps
                        
                        ##increase in the ProgressBar run TestCase's by 1. If this is Gui execution. If not than it will do nothing
                        self.progressDialog.updateProgressBar() 
                        
                        self.testCase = TestCase(allTestCases.testCaseAllDict[testCaseName], 
                                            testCaseName, 
                                            self.mappingFields['TestCase']);
                        ##On gui print Test Case
                        self.progressDialog.updateTextDialog("\tTestCase: %s"%repr(self.testCase.testCaseName));
                        
                        self.allSteps = AllSteps(self.testCase, testSetName)
                        self.allSteps.getAllSteps();
                    else:
                        self.testCase = None;
                        continue;
                    for fieldUserName, fieldValues in self.mappingFields['TestCase'].items():
                        ##do not repeat operation on column name which was already read in dictRequired
                        status = [fieldUserName==fieldValuesReq['FieldName'] for fieldValuesReq in self.dictRequired.values()]
                        if True in status:
                            continue;

                        try:
                            fieldLineIndex = int(fieldValues.get('FieldOrder')); 
                            fieldLineValue = line[fieldLineIndex];
                        except: continue;
                        self.testCase.setFieldsValue(fieldUserName, fieldLineValue);
                    continue;
    
            #==================================================================
            # Part responsible for Steps
            #==================================================================
            if (self.testSet and self.testCase) is not None:
                try: 
                    stepNameIndex = self.dictRequired['StepNameTL'].get('FieldOrder');
                    stepNameIndex  = int(stepNameIndex);
                except: 
                    continue;
                stepName = line[stepNameIndex];
                
                if stepName != "":
                    if self.allSteps.stepsAllDict.has_key(stepName):
                        self.step = Step(self.allSteps.stepsAllDict[stepName], 
                                   testSetName, 
                                   testCaseName, 
                                   stepName,
                                   self.mappingFields["Step"]);
                        ##On gui print Step Name
                        self.progressDialog.updateTextDialog("\t\tStep: %s"%repr(self.step.stepName));
                    else:
                        self.step = None;
                        continue;
                    
                    for fieldUserName, fieldValues in self.mappingFields['Step'].items():
                        if 'Step Name' == fieldUserName:
                            continue;
                        try:
                            fieldLineIndex = int(fieldValues.get('FieldOrder'));
                            fieldLineValue = line[fieldLineIndex];
                        except: continue;
                        self.step.setFieldsValue(fieldUserName, fieldLineValue);
                    del self.step;
