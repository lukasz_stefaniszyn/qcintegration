'''
Created on 11-05-2011

@author: wro50026
'''

from httplib import HTTPConnection 
import os


from lib.logfile import Logger
log = Logger(loggername="lib.QualityCenter_ext.lib.http_con", resetefilelog=False).log;


class QcHttpPage(HTTPConnection):
    '''
    Class used to download/update  files from QC server for run qc connection - OTA Api files
        
    Input:
    - str(server) = server name, e.g. http://qc.inside.com/qcbin/
    - str(dstDir) = destination directory where files from QC server will be saved. Default is current directory '.'
    
    Output:
    - list(filenameResult)- list of files which where downloaded from QC server. e.g. ['./OTAClient.dll', './WebClient.dll']
    '''

    filenameResult = []; 
    dstDir = None;
    

    def __init__(self, server, dstDir='.'):
        self.dstDir = dstDir;
        
        host = self.editServerName(server);
        HTTPConnection.__init__(self, host);
#        self.set_debuglevel(3)
        
        
        file = "OTAClient.lld";
        shouldUpadte = self.compareFiles(file)
        if shouldUpadte:
            filename = self.getFile(file);
            self.filenameResult.append(filename);
        
        file = "WebClient.lld";
        shouldUpadte = self.compareFiles(file)
        if shouldUpadte:
            filename = self.getFile(file);
            self.filenameResult.append(filename);
    
    def __del__(self):
        '''
        Close HTTPConnection
        '''
        
        self.close();    
    
    def editServerName(self, server):
        '''
        There is need to rename given server name to the name which can be used by HTTPConnection class. 
        
        Input:
        str(server) = http://qc.inside.com/qcbin/
        
        Output:
        str(host) = qc.inside.com    OR   192.168.77.45:8080
        '''
        
        server = server.split("/");
        try:
            while server.index(''): del server[server.index('')];
        except: pass;
        
        try: del server[server.index('http:')];
        except: pass;
        
        try: del server[server.index('https:')];
        except: pass;
        
        host = server[0];
        log.info(("Host:", host));    
        return host
        
    def compareFiles(self, filename):
        '''
        Compare local *.dll files to the one from the QC server
        Input:
        - str(filename) - name of the file to compare, e.g. OTAClient.lld. Inside function filename is renamed to OTAClient.dll
        
        Output: 
        bool - Gives information if compared file needs to be update. True/False 
        '''
        
        try:
            self.request("HEAD", r'/qcbin/Install/'+filename)
        except:
            log.info(("ERR: Unable to GET given file"));
            return None
        r1 = self.getresponse();
        header = r1.getheaders()[0];
        r1.read();
#        print len(data);
#        print "HEADERS:", header;
        ## HEADERS: ('content-length', '1721912')
        try:
            lenghtServerFile = int(header[1]);
        except:
            log.debug(("ERR: unable to convert lenght of file"));
        
        
        filename = filename.replace("lld", "dll");
        try:
            lenghtLocalFile =  os.path.getsize(filename)
        except os.error, err:
            log.info(("INFO: Local file does not exists. Updating local file"));
            return True
        
        if lenghtServerFile != lenghtLocalFile:
            log.info(("INFO: Size of file from server and local does not match. Updating local file"));
            return True
        
        log.info(("INFO: Size of file from server and local match."));
        return False

    
    def getFile(self, filename):
        '''
        This will download given file from the QC server to local place. At the end filename is renamed *.lld -> *.dll
        
        Input:
        - str(filename) = this is filename, which will be downloaded, e.g. OTAClient.lld
        
        Output:
        - str(filename)= return filename of file which was saved on local disk. E.g. OTAClient.dll OR None when was some error in download
        '''
        
        
        log.debug(("GET ", filename));
        try:
            self.request("GET", r'/qcbin/Install/'+filename)
        except:
            log.debug(("ERR: Unable to GET given file"));
            return None
        r1 = self.getresponse();
#        print r1.status, r1.reason
        data = r1.read();
        log.debug(("Received file, length:", len(data)));
        filename = self.makeFile(data, filename);
        return filename;
        
    def makeFile(self, data, filename):
        '''
        Function is used inside getFile().
        This will save bytes recieved from QC server into file
        
        Input:
        - HTTPConnection(data) - data recieved during getting file from QCserver
        
        Output:
        - str(filename) - filename of saved file, e.g. OTAClient.dll 
        '''
        dir, filename = os.path.split(filename);
        filename = filename.replace("lld", "dll");
        
        file = open(self.dstDir+'/'+filename, r'wb');
        file.write(data);
        file.close();
        return file.name; 
        

        

if __name__ == "__main__":
    host = "muvmp034.nsn-intra.net"
    host = "qc.inside.com"
#    host = "qualitycenter.insidecom";
#    host = "http://192.168.1.3:8080/qcbin/";
#    host = "http://qc.inside.com/qcbin/";

    qchttppage = QcHttpPage(host);
    del qchttppage 
    
    print "FINISHED"