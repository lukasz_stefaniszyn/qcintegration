# -*- coding: utf-8 -*-
#!/usr/bin/env python

'''
Created on 2009-09-16

@author: Lukasz Stefaniszyn
'''

import win32com.client

import os
import sys
import re
from time import localtime, strftime
import pywintypes


import excel_global_variable
import qc_connect_disconnect

from lib.QualityCenter_ext.lib import csvOperation
from lib.logfile import Logger
from globalVariables import GlobalVariables
log = Logger(loggername="lib.QualityCenter_ext.lib.TestPlan_upload", resetefilelog=False).log;


#------------------------------------------------------------------------------ 
 

#===============================================================================
# Global variable 
#===============================================================================
DBG = False; ##Debug printing
ENABLE_TESTLAB_UPDATE = False # This will create/find TestSet
ENABLED_OVERWRITE_DESCRIPTION = True; # This will overwrite any description
DST_FOLDER = GlobalVariables.DIR_RESULT
#------------------------------------------------------------------------------ 

class SetValueException(Exception):
    
    def __init__(self, text):
        log.exception(("SetValueException: ", repr(text)))
        self.text = repr(text);
        pass
    def __str__(self):
        return self.text;


class TestSetException(Exception):
    def __init__(self, text):
        log.exception(("TestSetException: ", repr(text)))
        self.text = repr(text);
        pass
    def __str__(self):
        return self.text;

class TestSetDirException(Exception):
    
    def __init__(self, text):
        log.exception(("TestCaseDirException: ", repr(text)))
        self.text = repr(text);
        pass
    def __str__(self):
        return self.text;

class TestCaseException(Exception):
    def __init__(self, text):
        log.exception(("TestCaseException: ", repr(text)))
        self.text = repr(text);
        pass
    def __str__(self):
        return self.text;

class TestCaseDirException(Exception):
    def __init__(self, text):
        log.exception(("TestCaseDirException: ", repr(text)))
        self.text = repr(text);
        pass
    def __str__(self):
        return self.text;
    

class NoneException(Exception):
    
    def __init__(self):
        pass
    def __str__(self):
        return "Object was None"; 
    
    

#===============================================================================
# Operation in TestLab in QC
#===============================================================================

class TestPlanUpload(object):
    '''This will make all action that will take Values from QC'''

    #------------------------------------------------------------------------------ 
    def __init__(self, qc):
        self.__tm = qc.TreeManager;
        self.__root = qc.TreeManager.NodeByPath(Path="Subject");

    #------------------------------------------------------------------------------ 
    def __get_tm(self):
        return self.__tm;
    tm = property(__get_tm);
    
    def __get_root(self):
        return self.__root;
    root = property(__get_root);
    
class TestLabUpload(object):
    '''This will make all action that will take Values from QC'''

    #------------------------------------------------------------------------------ 
    def __init__(self, qc):
        self.__tm = qc.TestSetTreeManager;
        self.__root = qc.TestSetTreeManager.Root;
    #------------------------------------------------------------------------------ 
    def __get_tm(self):
        return self.__tm;
    tm = property(__get_tm);
    
    def __get_root(self):
        return self.__root;
    root = property(__get_root);







    #------------------------------------------------------------------------------
class TestCase(object):
    '''
    Operation on ONE test Case
    '''

    def __init__(self, testPlanUpload, testCaseDir, testCaseName,  mappingFields):
        '''
        @param testPlanUpload: instance(), this is class instance of TestLabUpload 
        @param testCaseDir: str() , directory in the TestLab, where to search TestCase 
        @param testCaseName: str(), this is test set name
        @param mappingFields: this is user chosen fields only for TestCase
                          {"TestSet": {'Test Set Name':('TS_NAME', 'char', 'True', '0'),
                                     'Test Set Directory': ('TS_DIR', 'char', 'True', '1'),
                                     'Test Set Description': ('TS_DESC', 'char', 'True', '2')
                                     },
                          };
        '''
        self.tm = testPlanUpload.tm
        self.root = testPlanUpload.root;
        self.mappingFields = mappingFields;
        
        self.__testCaseName = testCaseName;
        self.__testCaseDir = testCaseDir
        self.__testCaseInstance = None;
        
        self.testCaseInstance = self.find_TestCase(testCaseDir, testCaseName); ##this will create or get TestCaseInstance
        
    #------------------------------------------------------------------------------ 
    
    def __get_testCaseName(self):
        return self.__testCaseName
    testCaseName = property(__get_testCaseName, None, None, "Test Case name as string");
    
    def get_test_case_dir(self):
        return self.__testCaseDir
    testCaseDir = property(get_test_case_dir, None, None, "testCaseDir's docstring")
    
    def __get_testCaseInstance(self):
        return self.__testCaseInstance;
    def __set_testCaseInstance(self, testCaseInstance):
        self.__testCaseInstance = testCaseInstance
    def __del_testCaseInstance(self):
        try: 
            self.__testCaseInstance.Post();
            self.__testCaseInstance.UnLockObject();
        except: pass;
    testCaseInstance = property(__get_testCaseInstance, __set_testCaseInstance, __del_testCaseInstance, "TestCase QualityCenter instance");
    #------------------------------------------------------------------------------ 
    
    def make_testCaseDirectory(self, path):
        ''' Create/find folder in TestPlan: 
        input = str(path)
        output = Folder_instance / False (if Subject is not in given path)
        example: make_testCaseDirectory(path=r"Subject\test\something") will create whole directory Subject\test\something or only this missing folder
        '''
        
        found_folder = None;
        
        try:
            ##Check if the whole folder exists
            found_folder = self.tm.NodeByPath(Path=path);
            log.debug(("Folder is already present: ",(repr(path))));
            return found_folder;
        except:
            ##if folder does not exists, then try every level of the given folder. If level does not exists, then create this level
            folder_target = path.split(os.path.sep);
            if "Subject" != folder_target[0].title():
                raise TestCaseDirException(("Given folder does not exists:", repr(folder_target)));
                return False;
            ##start checking which folder does not exists from given path
            for i in range(1, len(folder_target)):
                folder = str('\\'.join([ o for o in folder_target[0:i+1] ])); 
                try:
                    found_folder = self.tm.NodeByPath(Path=folder);
                    ##Folder exists, go to the next level of given path
                    continue;
                except:
                    ##Go one level above from given path 
                    parent_folder = str('\\'.join([o for o in folder_target[0:i] ]));
                    log.debug(("Parent folder:", parent_folder));
                    parent_folder = self.tm.NodeByPath(Path=parent_folder);
                    ##Create folder under parent folder
                    log.debug(("Adding folder:", folder_target[i]));
                    parent_folder.AddNode(NodeName=folder_target[i]);
            found_folder = self.tm.NodeByPath(Path=folder);
            return found_folder;
        
  
    def find_TestCase(self, testCaseDir, testCaseName):
        '''Search for TestCase name
        input = str(testCaseDir), str(testCaseName)
        output = instance(TestCase_instance)
        example: find_TestCase("Subject\a\b\c", "TestCase_test_name") '''
        
        
        ##Go to correct TestCase directory
        log.debug(("Search/create folder \"%s\" of TestCase \"%s\""%(testCaseDir, testCaseName)));
        log.info(("\t\t\tTestCase name:\"%s\""%(testCaseName)));
        folder_instance = self.make_testCaseDirectory(testCaseDir);
        ##Find/create TestCase        
        if folder_instance != False:
            testCaseInstance = folder_instance.FindTests(Pattern=testCaseName, MatchCase=True);
            if testCaseInstance is None:
                ##TestCase was not found, in that case it will be created
                log.debug(("Did not found TestCase:",testCaseName));
                log.debug(("Creation TestCase \"%s\" "%(testCaseName)));
                try:
                    testCaseInstance = folder_instance.TestFactory.AddItem(str(testCaseName));
                except pywintypes.com_error, err:
                    log.info(("Unable to create TestCase."));
                    raise TestCaseException(("ERR:", repr(err[2][2])))
                    return False
                return testCaseInstance;
            else:
                ##TestCase is present 
                if testCaseInstance.Count >1:
                    log.debug(("Found more than 1 TestCase with pattern name:",testCaseName));
                    ##FindTest will give all TestCases with the same part of name. You have to filter and return only the one You need 
                    for testCaseIns in testCaseInstance:
                        if decodeText(testCaseIns.Name) == testCaseName:
                            log.debug(("Found TestCase name:",testCaseName));
                            return testCaseInstance; 
                else:
                    return testCaseInstance[0];
        
#       
                
                
        else:
            raise TestCaseException(("WRN: Unable to create folder \"%s\" and in this situation TestCase \"%s\" will not be created"%(testCaseDir, testCaseName)))
            
    
    def setFieldsValue(self, fieldUserName, fieldValue):
        if not self.testCaseInstance or self.testCaseInstance is None:
            return False;
        
        if self.mappingFields.has_key(fieldUserName):
            fieldName = self.mappingFields[fieldUserName].get('FieldQcName')
            if fieldUserName =="Attachment":
                if fieldValue==None or fieldValue=='': return;
                uploadAttachments(self.testCaseInstance, fieldValue);
                return;
            else:
                setFieldValue(self.testCaseInstance, fieldName, fieldValue)
        else: 
            text = "Given TestCase fieldUserName: \"%s\" does not exits in database"%repr(fieldUserName);
            log.info(text);
            raise SetValueException(text);







class TestSet(TestLabUpload):
    '''
    Operation on ONE test Set
    '''

    def __init__(self, qc, testSetDir, testSetName,  mappingFields):
        '''
        @param testLabUpload: instance(), this is class instance of TestLabUpload 
        @param testSetDir: str() , directory in the TestLab, where to search TestSet 
        @param testSetName: str(), this is test set name
        @param mappingFields: this is user choosen fields only for TestSet
                          {"TestSet": {'Test Set Name':('TS_NAME', 'char', 'True', '0'),
                                     'Test Set Directory': ('TS_DIR', 'char', 'True', '1'),
                                     'Test Set Description': ('TS_DESC', 'char', 'True', '2')
                                     },
                          };
        '''
        TestLabUpload.__init__(self, qc);
        self.mappingFields = mappingFields;
        
        self.__testSetName = testSetName;
        self.__testSetDir = testSetDir;
        self.__testSetInstance = None;
        
        self.makeTestLabTestSet(self.testSetDir, self.testSetName);
        
    #------------------------------------------------------------------------------ 
    def __get_testSetName(self):
        return self.__testSetName
    testSetName = property(__get_testSetName, None, None, "Test Set name as string");

    def __get_testSetDir(self):
        return self.__testSetDir
    testSetDir = property(__get_testSetDir, None, None, "Test Set directory as string");
    
    def __get_testSetInstance(self):
        return self.__testSetInstance;
    def __set_testSetInstance(self, testSetInstance):
        self.__testSetInstance = testSetInstance
    testSetInstance = property(__get_testSetInstance, __set_testSetInstance, None, "TestSet QualityCenter instance");
    #------------------------------------------------------------------------------ 

    def make_TS_directory(self, path):
        ''' Create/find folder in TestPlan: 
        input = str(path)
        output = Folder_instance / False (if Subject is not in given path)
        example: make_testCaseDirectory(path=r"Subject\test\something") will create whole directory Subject\test\something or only this missing folder
        '''
        
        found_folder = None;
        try:
            ##Check if the whole folder exists
            found_folder = self.tm.NodeByPath(Path=path);
            log.debug(("Folder \"%s\" is already present"%(unicode(path))));
            return found_folder;
        except:
            ##if folder does not exists, then try every level of the given folder. If level does not exissts, then create this level
#            folder_target = path.split("\\");
            folder_target = path.split(os.path.sep)
            if "Root" != folder_target[0].title():
                raise TestSetDirException(("Given folder does not exists:", repr(path)));
                return False;
            
            ##start checking which folder does not exists from given path
            for i in range(1, len(folder_target)):
                folder = str('\\'.join([ o for o in folder_target[:i+1] ])); 
                log.debug(("current folder search:", folder));
                try:
                    if ( (i== 0) and (folder_target[i] == 'Root') ):
                        found_folder = self.root;
                    else:
                        found_folder = self.tm.NodeByPath(Path=folder);
                    ##Folder exists, go to the next level of given path
                    continue;
                except:
                    ##Go one level above from given path 
                    parent_folder = str('\\'.join([o for o in folder_target[0:i] ]));
                    log.debug(("Parent folder:", parent_folder));
                    if ( (i== 1) and (folder_target[0] == 'Root') ):
                        parent_folder= self.root;
                    else:    
                        parent_folder = self.tm.NodeByPath(Path=parent_folder);
                    ##Create folder under parent folder
                    log.debug(("Adding folder:", folder_target[i]));
                    parent_folder.AddNode(NodeName=folder_target[i]);
            found_folder = self.tm.NodeByPath(Path=folder);
            return found_folder;
      
    def find_TestSet(self, TS_dir, TS_name):
        '''Search for TestSet name. Create new one if not present
        input = str(TS_dir), str(TS_name)
        output = instance(TestSet_instance) OR False
        example: find_TestSet() '''
        
        
        
        
        log.debug(("Search/create folder \"%s\" of TestSet \"%s\""%(TS_dir, TS_name)));
        log.info(("\t\t\tTestSet name: \"%s\""%(TS_name)));
        folder_instance = self.make_TS_directory(TS_dir);
        ##Find/create TestSet        
        if folder_instance != False:
            TS_instance = folder_instance.FindTestSets(Pattern=TS_name, MatchCase=True);
            if TS_instance is None:
                ##TestSet was not found, in that case it will be created
                log.debug(("Did not found TestSet:",TS_name));
                log.debug(("Creation TestSet \"%s\" "%(TS_name)));
                try:
                    TS_instance = folder_instance.TestSetFactory.AddItem(str(TS_name));
                    TS_instance.Post();
                except pywintypes.com_error, err:
                    log.warn(("Unable to create TestSet."));
                    raise TestSetException(("ERR: Cannot create TestSet. ", unicode(err[2][2])));
                    return False
                return TS_instance;
            else:
                ##TestSet is present 
                if TS_instance.Count >1:
                    log.debug(("Found more than 1 TestSet with pattern name:",TS_name));
                TS_instance = TS_instance[0];
                log.debug(("Found TestSet name:",TS_name));
                return TS_instance;
        else:
            raise TestSetException(("WRN: Unable to create folder \"%s\" and in this situation TestSet \"%s\" will not be created"%(TS_dir, TS_name)));
            return False
    
    
    def linkTestCasetoTestSet(self, TestCase_instance):
        '''This will search in already found/created TestSet, 
        TestCases which are the same as input Excel TestCase
        input= inst(TestCase from TestPlan)
        output= ins(TestCase from TestSet)'''
        ##Check basic information if we can link given TestCase to TestSet
        if self.testSetInstance != False:
            # Add TestCase into create/found TestSet
            if (self.testSetName is None) or (self.testSetDir is None):
                log.debug(("INFO: No TestSet directory/name in TestLab columns"));
                return False
        else:
            log.info(("INFO: TestSet instance was not created. Due to this fact TestCase will not be linked"));
            return False 
        
        #======================================================================
        # If the code is in this place, it means that testSetInstance was created correctly. Now we will find, or create linked TestCase into TestSet
        #======================================================================
        
        ##List all already linked testCase in the TestSet
        list_TestCaseFromTS = {};
        for testCaseInstance in self.TSTestFactory.NewList(""):
            list_TestCaseFromTS[int(testCaseInstance.TestId)] = testCaseInstance; 

        if list_TestCaseFromTS.has_key(int(TestCase_instance.ID)):
            ##Given TestCase already exists in the TestSet, return only this testCaseInstance
            log.debug(("INFO: TestCase \"%s\" already present in the TestSet \"%s\" "%(repr(TestCase_instance.Name), self.testSetName)));
            testCaseInstance = list_TestCaseFromTS.get(int(TestCase_instance.ID));
            return testCaseInstance
        else:
            ##Given TestCase is not linked to the TestSet, return testCaseInstance
            testCaseInstance= self.TSTestFactory.AddItem(TestCase_instance);
            testCaseInstance.Post();
            return testCaseInstance;

    def makeTestLabTestSet(self, testSetDir=None, testSetName=None):
        
        ##This find_TestSet, will find TestSet, or create needed TestSet catalogs, and TestSet
        if (testSetName is not None) and (testSetDir is not None):
            self.testSetInstance = self.find_TestSet(testSetDir, testSetName); ##Go to correct TestSet directory
            self.TSTestFactory = self.testSetInstance.TSTestFactory;
            self.testSetInstance.Post();
        else:
            raise TestSetException(("TestSetName is None and/or TestSetDir is None"));
            return False;
        
        
    def setFieldsValue(self, fieldUserName, fieldValue):
        
        if self.mappingFields.has_key(fieldUserName):
            fieldName = self.mappingFields[fieldUserName].get('FieldQcName')
            if fieldUserName =="Attachment":
                if fieldValue==None or fieldValue=='': return;
                uploadAttachments(self.stepInstance, fieldValue);
                return;
            else:
                setFieldValue(self.testSetInstance, fieldName, fieldValue)
        else: 
            text = "Given TestSet fieldUserName: \"%s\" does not exits in database"%repr(fieldUserName);
            log.info(text);
            raise SetValueException(text);
        

    
class AllSteps(object):
    '''
    Operation on ALL steps in the given test case
    '''
    
    def __init__(self, testCase):
        '''
        This will keep information about AllSteps in the given testCase class
        @param testCase: class(TestCase)
        '''
        self.__testCase = testCase;
        self.__DesignStepFactory = self.testCaseInstance.DesignStepFactory
        self.__stepAllDict = {};
        
        self.removeSteps();
        

    #------------------------------------------------------- 
    def __get_testCaseName(self):
        return self.__testCase.testCaseName;
    testCaseName = property(__get_testCaseName, None, None, "Test Case name as string");
    
    def __get_allDict(self):
        return self.__stepAllDict;
    def __set_allDict(self, instanceList):
        self.__stepAllDict = makeDictOfInstance(instanceList);
    stepsAllDict = property(__get_allDict, __set_allDict, None, "This is dict() of all test cases instance {'StepName': instance(step)}");
    
    def __get_testCaseInstance(self):
        return self.__testCase.testCaseInstance;
    testCaseInstance = property(__get_testCaseInstance, None, None, "TestCase QualityCenter instance");

    def get_design_step_factory(self):
        return self.__DesignStepFactory
    def del_design_step_factory(self):
        del self.__DesignStepFactory
    DesignStepFactory = property(get_design_step_factory, None, del_design_step_factory, "DesignStepFactory's docstring")
    #-------------------------------------------------------

    def removeSteps(self):
        '''Remove current steps 
        input = None
        output = None
        '''
        stepsListInstance = self.DesignStepFactory.NewList("");
        for Step_instance in stepsListInstance:
            self.DesignStepFactory.RemoveItem(Step_instance);
        
    
class Step(object):
    '''
    Operation on ONE step in the given test case
    '''
    
    def __init__(self, testCaseName, allSteps, stepName, mappingFields):
        '''
        
        @param testCaseName: str(), this is test case name 
        @param allSteps: object(), this is object of the AllSteps class
        @param stepName: str(), this is step name which will be created
        @param mappingFields: dict(), this is dedicated part of 'mappingFields' only for Step fields 
        '''
        self.__allSteps = allSteps;
        self.__testCaseName = testCaseName;
        self.__stepName = stepName;
        
        self.mappingFields = mappingFields;

        self.__stepInstance = self.findStep(self.stepName);

        
    def __del__(self):
        del self.stepInstance
    #------------------------------------------------------------------------------ 
    def get_all_steps(self):
        return self.__allSteps
    def del_all_steps(self):
        del self.__allSteps
    allSteps = property(get_all_steps, None, del_all_steps, "allSteps's docstring")
    
    def get_step_instance(self):
        return self.__stepInstance
    def del_step_instance(self):
        try: self.__stepInstance.Post();
        except: pass;
        del self.__stepInstance
    stepInstance = property(get_step_instance, None, del_step_instance, "stepInstance's docstring")

    def __get_testCaseName(self):
        return self.__testCaseName;
    testCaseName = property(__get_testCaseName, None, None, "Test Case name as string");
    def __get_stepName(self):
        return self.__stepName;
    stepName = property(__get_stepName, None, None, "Step name as string");
    
    #------------------------------------------------------------------------------ 
    def findStep(self, stepName):
        '''Search OR create Step name in TestCase
        input = str(Step_name)
        output = instance(Step_instance) OR bool(False)'''

        log.info(("\t\t\t\tStep name:",unicode(stepName)));
        
        ##self.list_OfSteps = dict{Step_name:Step_instance};
#        list_OfSteps = self.list_OfSteps;
        
        
        ##Get Index number of Step_name OR create new Step
        #=======================================================================
        # Use this if You don't want to delete all Steps and create one more time
        # if list_OfSteps.has_key(Step_name):
        #    Step_instance = list_OfSteps.pop(Step_name);
        #     
        # else:
        #    if DBG: print "Step_name could not be found in TestCase:";
        #    Step_instance = DesignStepFactory.AddItem(Step_name);
        #=======================================================================
         
        log.debug(("Step_name could not be found in TestCase"));
        stepInstance = self.allSteps.DesignStepFactory.AddItem(stepName);
        
        
        #=======================================================================
        # #!!!WARNING , now every Step will be added to TestCase, even if it is already present 
        # #Step_instance = DesignStepFactory.AddItem(Step_name);
        #=======================================================================
        return stepInstance;
    
    def setFieldsValue(self, fieldUserName, fieldValue):
        
        if self.mappingFields.has_key(fieldUserName):
            fieldName = self.mappingFields[fieldUserName].get('FieldQcName')
            if fieldUserName =="Attachment":
                if fieldValue==None or fieldValue=='': return;
                uploadAttachments(self.stepInstance, fieldValue);
                return;
            else:
                setFieldValue(self.stepInstance, fieldName, fieldValue)
        else: 
            text = "Given Step fieldUserName: \"%s\" does not exits in database"%repr(fieldUserName);
            log.info(text);
            raise SetValueException(text);

class ProgressDialog(object):
    
    def __init__(self, progressExecutionDialog=None):
        self.progressExecutionDialog = progressExecutionDialog;
        
    def checkcancelStatus(self):
        if self.progressExecutionDialog is not None:
            return self.progressExecutionDialog.cancelStatus;
        else: 
            return False;
        
        
    def updateTextDialog(self, text):
        '''
        Write new line to Dialog Gui 
        *input*
        @param text: str()
        *output*:
        None
        '''
        if self.progressExecutionDialog is not None:
            self.progressExecutionDialog.updateTextDialog(text);
        else: 
            print(text);
        log.info(text);
            
    def updateProgressBar(self):
        '''
        Update number+1 on Dialog Gui of ProgressBar
        *input*
        None
        *output*
        None
        '''
        if self.progressExecutionDialog is not None:
            self.progressExecutionDialog.updateProgressBar();




class MainExecution(object):
    
    def __init__(self, 
                 gui=False,
                 cmd=False,
                 qc=None, 
                 mappingFields=None,
                 filename=None,
                 progressExecutionDialog=None):
        
        self.__mappingFields = mappingFields;
        self.__qc = qc;
        self.__filename=filename
        self.__progressDialog = None;
        self.path = None;
        

            
        self.__progressDialog = None;
        
        
        ##This will be used in the Gui and CMD run. This will show progress of execution
        self.__progressDialog = ProgressDialog(progressExecutionDialog);
        
        
        
        
        if gui and not cmd:##start Gui part
            self.path = self.mainGui();
        elif cmd and not gui:##start CMD part
            self.path = self.mainCMD()
        else:
            log.error(("Decide which main script have to be executed: Gui or CMD"));

    def __del__(self):
        self.cleanBeforeClose();

    def get_filename(self):
        return self.__filename
    filename = property(get_filename, None, None, "filename's docstring")

    def get_progress_dialog(self):
        return self.__progressDialog
    progressDialog = property(get_progress_dialog, None, None, "progressDialog's docstring")

    def get_qc(self):
        return self.__qc
    def del_qc(self):
        del self.__qc
    qc = property(get_qc, None, del_qc, "qc's docstring")
            

    def get_mapping_fields(self):
        return self.__mappingFields
    mappingFields = property(get_mapping_fields, None, None, "mappingFields's docstring")

    def cleanBeforeClose(self):
        del self.step;
        del self.allSteps;
        del self.testCase;
        del self.testSet;

    def mainGui(self):
        
        self.main()
        try: del self.readCsv;
        except: pass;
        
    def mainCMD(self):
        
        self.main()
        try: del self.readCsv;
        except: pass;

    def main(self):
        '''
        
        @param qc:
        @param filename:
        @param columnGroupMappingFields: this dict() is given by Gui interface
        {"TestSet": {"TestSetName": 
                                    {ColumnName:"TS_NAME", colNumber: 1}},
        {'Step': {
                'Step Description': {'FieldIsRequired': 'True',
                                   'FieldName': 'QC Step Description',
                                   'FieldOrder': '1',
                                   'FieldQcName': 'ST_DESC',
                                   'FieldType': 'char'},
                 'Step Name': {
                                   'FieldIsRequired': 'True',
                                   'FieldName': 'QC Step Name',
                                   'FieldOrder': '0',
                                   'FieldQcName': 'ST_NAME',
                                   'FieldType': 'char'}},
         {'TestCase': {
                     'Test Case Directory': {
                                               'FieldIsRequired': 'True',
                                               'FieldName': 'QC Test Case Directory',
                                               'FieldOrder': '1',
                                               'FieldQcName': 'TC_DIR',
                                               'FieldType': 'char'},
        }}
        '''
        
        if not checkFile(self.filename):
            return False
    
        self.readCsv = csvOperation.ReadCSV(self.filename);
        self.dictRequired = self.readCsv.getRequiredIndex(self.mappingFields); ##this will switch key in dict() to have FieldQcName
        testPlanUpload= TestPlanUpload(self.qc);
    
        self.testSet = None;
        self.testCase = None;
        self.allSteps = None;
        self.step = None;
    
        for line in self.readCsv.getRow():
            if self.progressDialog.checkcancelStatus(): 
                self.cleanBeforeClose();
                break;##check if user did not pressed Cancel button. If yes, then stop execution
            
            #======================================================================
            # Part responsible for TestCases 
            #======================================================================
            try:
                testCaseDirIndex = self.dictRequired['TestCaseDirectory'].get('FieldOrder');
                testCaseDirIndex  = int(testCaseDirIndex);
            except: 
                continue;  
            testCaseDir = line[testCaseDirIndex];
        
            try: 
                testCaseNameIndex = self.dictRequired['TestCaseName'].get('FieldOrder');
                testCaseNameIndex  = int(testCaseNameIndex);
            except: 
                continue;
            testCaseName = line[testCaseNameIndex];
            
            if testCaseName != "":
                ##increase in the ProgressBar run TEstCase's by 1. If this is Gui execution. If not than it will do nothing
                self.progressDialog.updateProgressBar()
                
                self.testCase = TestCase(testPlanUpload, 
                                    testCaseDir, 
                                    testCaseName, 
                                    self.mappingFields['TestCase']);
                
                ##On gui and cmd print TestCase Name
                self.progressDialog.updateTextDialog("TestCase: %s"%repr(self.testCase.testCaseName));
                
                self.allSteps = AllSteps(self.testCase)
                for fieldUserName, fieldValues in self.mappingFields['TestCase'].items():
                    ##do not repeat operation on column name which was already read in dictRequired
                    status = [fieldUserName==fieldValuesReq['FieldName'] for fieldValuesReq in self.dictRequired.values()]
                    if True in status:
                        continue;
                    
                    
                    fieldLineIndex = fieldValues.get('FieldOrder') 
                    try: fieldLineIndex  = int(fieldLineIndex);
                    except: continue;
                    fieldLineValue = line[fieldLineIndex];
                    self.testCase.setFieldsValue(fieldUserName, fieldLineValue);
            else: 
                log.info("TestCaseName is empty")
                
            
            if (self.testCase.testCaseInstance is not None) and (self.testCase.testCaseInstance!=False):
                #==========================================================================
                # Part responsible for TestSets
                #==========================================================================
                try: 
                    testSetDirIndex = self.dictRequired['TestSetDirectory'].get('FieldOrder');
                    testSetDirIndex  = int(testSetDirIndex);
                except: 
                    continue;  
                testSetDir = line[testSetDirIndex];
                
                try:
                    testSetNameIndex = self.dictRequired['TestSetName'].get('FieldOrder');
                    testSetNameIndex  = int(testSetNameIndex);
                except: continue;
                testSetName = line[testSetNameIndex];
            
                if (testSetName and testSetDir)!= "": 
                    try: 
                        self.testSet = TestSet(self.qc, testSetDir, testSetName, self.mappingFields['TestSet'])
                        ##On gui or cmd print TestSet Name
                        self.progressDialog.updateTextDialog("\tTestSet: %s"%repr(self.testSet.testSetName));
                    except TestSetException:
                        self.testSet = None;
                        continue
                    
                    ##link TestCase to this TestSet
                    self.testSet.linkTestCasetoTestSet(self.testCase.testCaseInstance);
                    
                    for fieldUserName, fieldValues in self.mappingFields['TestSet'].items():
                        ##do not repeat operation on column name which was already read in dictRequired
                        status = [fieldUserName==fieldValuesReq['FieldName'] for fieldValuesReq in self.dictRequired.values()]
                        if True in status:
                            continue;
                        
                        
                        
                        fieldLineIndex = fieldValues.get('FieldOrder') 
                        try: fieldLineIndex  = int(fieldLineIndex);
                        except: continue;
                        fieldLineValue = line[fieldLineIndex];
                        self.testSet.setFieldsValue(fieldUserName, fieldLineValue);
            
            #==================================================================
            # Part responsible for Steps
            #==================================================================
            if (self.testCase.testCaseInstance is not None) and (self.testCase.testCaseInstance!=False):
                try: 
                    stepNameIndex = self.dictRequired['StepNameTP'].get('FieldOrder');
                    stepNameIndex  = int(stepNameIndex);
                except: 
                    continue;
                stepName = line[stepNameIndex];
                
                if stepName != "":
                    self.step = Step(testCaseName, 
                                self.allSteps, 
                                stepName, 
                                self.mappingFields["Step"]);
                    ##On gui or cmd print Step Name
                    self.progressDialog.updateTextDialog("\t  Step: %s"%repr(self.step.stepName));

                    for fieldUserName, fieldValues in self.mappingFields['Step'].items():
                        ##do not repeat operation on column name which was already read in dictRequired
                        status = [fieldUserName==fieldValuesReq['FieldName'] for fieldValuesReq in self.dictRequired.values()]
                        if True in status:
                            continue;

                        fieldLineIndex = fieldValues.get('FieldOrder');
                        try: fieldLineIndex  = int(fieldLineIndex);
                        except: continue;
                        fieldLineValue = line[fieldLineIndex];
                        self.step.setFieldsValue(fieldUserName, fieldLineValue);
                    del self.step;
        #------------------------------------------------------------------------------ 
        #------------------------------------------------------------------------------ 

def makeDictOfInstance(instanceList):
    '''
    This will create dictionary of all QC instances from the given parent QC instance
    This will be used in the AllTestCases, AllSteps  
    
    *Input*
    @param instanceList: list(qc_instance)
    
    *Output*
    @param allDict: disct(),  dictionary of given instanceList
                            {str('TestCaseName'): instance(TestCase)} 
    '''
    allDict = {}
    for instance in instanceList:
        name = decodeText(instance.Name);
#        name = repr(name);
        allDict[name] = instance;
    return allDict;

def setFieldValue(instance, fieldName, fieldValue):
    '''
    This will update field value
    
    *Input*
    @param fieldName: this is QC field name, like  TS_NAME
    @param fieldValue: str(), like 'Passed', '09\07\203'
    
    *Output*
    None
    '''
#    log.debug(("getFieldValue for fieldName, fieldType:",(fieldName, fieldType)));
    try:
        instance.SetField(fieldName, fieldValue);
        instance.Post();
    except:
        log.info(("Unable to update Field: %s with value: %s")%(fieldName, fieldValue));
    

def list_attachments(instance):
        '''Create dictionary of Attachments in instance
        input = instance()
        output = dict{attachment_name:attachment_instance}'''
        
        
        list_attachements = {};
        for attachment_inst in instance.Attachments.NewList(""):
            attachment_name = attachment_inst.Name.split('_', 2)[-1];
            list_attachements[attachment_name] = attachment_inst;
        return list_attachements;

def uploadAttachments(instance, attachments):   
    '''This will upload attachments into QC database 
    input = inst(Instance), str(Attachements)
    output = None;
    example, uploadAttachments(TestCase_instance, str(c:\tmp\test1.txt; c:\test2.txt);
    '''
    
    listAttachments = list_attachments(instance);
    
    log.debug(("attachments:\"%s\""%attachments));
    for attachment in attachments.split(';'): 
        if attachment == "":
            continue;
        attachment = attachment.strip();
        attachment_fileName = os.path.basename(attachment);
        

        ##Check if given attachments are present
        if os.path.exists(attachment):            
            if listAttachments.has_key(attachment_fileName):
                ##IF the attachment to upload will be the same (in name) as the one already on the QC server, then remove this from server
                log.debug(("\tRemoving duplicated attachment:",attachment_fileName)); 
                attachment_inst_remove = listAttachments.get(attachment_fileName);
                instance.Attachments.RemoveItem(attachment_inst_remove.ID);
            
            att = instance.Attachments.AddItem(None);
            att.FileName = r'%s'%attachment;
            ##Upload file to QC;
            att.Type = 1;
            log.debug(("Adding attachment \"%s\" "%attachment));
            att.Post();
        else:
            log.info(("INFO: Given attachment \"%s\" does not exists. Skipping add"%attachment));
    return;

def getAttachment(instance, targetDir):
        '''This will take all attachments from QualityCenter database and save into user directory (TagedDir)
        input = str(os.path(targetDir)) 
        output = str(;attachmentDir;attachmentDir;attachmentDir) OR str('')
        '''
        
        f_attachments = open(os.path.join(DST_FOLDER,"missingAttachments.txt"), "a");
        
        attDstAll = '';
        ##Check if in instance are any attachments
        if (instance.HasAttachment):
            dstFolder = targetDir; 
            ##Make catalog of this Destination Folder
            try:
                os.makedirs(dstFolder);
            except:
                log.info("Unable to create Destination Folder for keep attachment. Skipping attachment")
                return attDstAll;
            
            for att in instance.Attachments.NewList(""):
                ##for every attachment make download it to local machine
                attName = att.GetName();
                attName = decodeText(attName);
                if att.FileSize == 0:
                    information = "Attachment is empty: %s     in : %s"%(attName.split("_", 2)[-1], repr(dstFolder));
                    log.debug(information);
                    f_attachments.write(information+'\n');
                    f_attachments.flush();
                    continue;
                ##This Load will download attachment from QC database to local machine, but to the default user catalog 
                ##synchronize If True, program run waits for download to complete. Otherwise, download is asynchronous. 
                attLocalCatalog = att.Load(True, ""); 
                ##This will move this downloaded attachment file to Result file 
                attSrc = os.path.join(attLocalCatalog,attName);
                ##Create name of the Destination Folder, where attachment from this instance will be keept
                attDst = os.path.join(dstFolder,attName.split("_", 2)[-1]); 
                attDstAll += ';' + attDst;
                try:
                    os.renames(attSrc, attDst);
                except:
                    log.info(("WRN: Unable to get attachment file from QualityCenter, due to the insufficient permission in moving to catalog or catalog/file already exists: ",repr(dstFolder)));
        attDstAll = attDstAll.strip(';');
        
        f_attachments.close();
        return attDstAll;

def decodeText(value):
#        value = repr(value)
#        value = value.lstrip("u'")
#        value = value.rstrip("'")
#        value = unicode(value, errors="ignore")
        try:
            value = value.encode('ascii', 'ignore');
        except: 
            try: 
                value = repr(value);
            except:
                value = "Wrong data";
                log.debug("Not able to decode Text");
        return value;       


def checkFile(filename):
    '''This will check if given file exist'''
    if not os.path.exists(filename):
        log.info((("Not able to find file:"), filename));
        return False;
    
    return True;


    #------------------------------------------------------------------------------ 
if __name__ == "__main__":
    pass

   
   
   
   