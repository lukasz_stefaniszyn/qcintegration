# -*- coding: utf-8 -*-
#!/usr/bin/env python
'''
Created on 2010-06-23

@author: Lukasz Stefaniszyn
'''
import os
import sys
import copy
from time import strftime, localtime;
import csv;

from lib.logfile import Logger
log = Logger(loggername="lib.QualityCenter_ext.lib.saveCSVTestPlan", resetefilelog=False).log;



#fieldsTestPlan = {
#            'TestPlan':(1,1, 'Test Plan'),\
#            'TestLab':(1,10, 'Test Lab'),\
#            'TestCasePlan':(2,1, 'Test Case'),\
#            'TestStepPlan':(2,6, 'Step'),\
#            'TestSetLab':(2,10, 'Test Set'),\
#            'TestCaseLab':(2,12, 'Test Case'),\
#            'TestCasePlan Directory':(3,1, 'Directory'),\
#            'TestCasePlan Name':(3,2, 'Name'),\
#            'TestCasePlan Attachments':(3,3, 'Attachments'),\
#            'TestCasePlan Description':(3,4, 'Description'),\
#            'TestCasePlan Responsible':(3,5, 'Responsible'),\
#            'TestStepPlan Name':(3,6, 'Name'),\
#            'TestStepPlan Attachments':(3,7, 'Attachments'),\
#            'TestStepPlan Description':(3,8, 'Description'),\
#            'TestStepPlan Expected':(3,9, 'Expected Result'),\
#            'TestSetLab Directory':(3,10, 'Directory'),\
#            'TestSetLab Name':(3,11, 'Name'),\
#            'TestCaseLab Directory':(3,12, 'Directory'),\
#            'TestCaseLab Name':(3,13, 'Name'),\
#            }


fieldsTestPlan = {
            'TestPlan':(1,1, 'Test Plan'),\
            'TestLab':(1,9, 'Test Lab'),\
            'TestCasePlan':(2,1, 'Test Case'),\
            'TestStepPlan':(2,5, 'Step'),\
            'TestSetLab':(2,9, 'Test Set'),\
            'TestCaseLab':(2,11, 'Test Case'),\
            'TestCasePlan Directory':(3,1, 'Directory'),\
            'TestCasePlan Name':(3,2, 'Name'),\
            'TestCasePlan Attachments':(3,3, 'Attachments'),\
            'TestCasePlan Description':(3,4, 'Description'),\
            'TestStepPlan Name':(3,5, 'Name'),\
            'TestStepPlan Attachments':(3,6, 'Attachments'),\
            'TestStepPlan Description':(3,7, 'Description'),\
            'TestStepPlan Expected':(3,8, 'Expected Result'),\
            'TestSetLab Directory':(3,9, 'Directory'),\
            'TestSetLab Name':(3,10, 'Name'),\
            'TestCaseLab Directory':(3,11, 'Directory'),\
            'TestCaseLab Name':(3,12, 'Name'),\
            }

def appendFieldList(typeGroup, fieldToAdd, fieldList):
    '''
    Append new Field which will be used in upload/download
    input = str(typeGroup),  e.g.  TestCase, or Step
            tuple(fieldToAdd), e.g.  ('TS_USER_01', 'Regression')
            dict(fieldList), e.g. {'TestCase': [('TS_USER_01', 0, 3, 'Regression')]}
    output = None
    '''
    groupList = fieldList.get(typeGroup);
    lastRow, lastColumn = groupList[-1][1:3];
    qcFieldName = fieldToAdd[0];
    userFieldName = fieldToAdd[1];
    groupList.append((qcFieldName, lastRow, lastColumn+1, userFieldName));
    



fieldList_TestPlan = {'TestCase': [('TestCasePlan Directory',3,1, 'Directory'),\
                                    ('TestCasePlan Name',3,2, 'Name'),\
                                    ('TestCasePlan Attachments',3,3, 'Attachments'),\
                                   ],\
                      'Step': [('TestStepPlan Name',3,5, 'Name'),\
                               ('TestStepPlan Attachments',3,6, 'Attachments'),\
                               
                               ]
                      }





class SaveAsCSV(object):
    '''
    This can help in saving results from TestPlan download into *.csv format
    '''

    columnCount = None
    row_num = None #Integer of the current last row from can be append new row
#    rowBlank= None; #Blank row with empty strings e.g.['', '', '']

    



    def __init__(self, mainDir, fieldList):
        '''
        
        Create or update already created file. 
        input = str(mainDirectory);
        '''
        filename = strftime('TPdownload_%d-%m-%Y_%H-%M-%S.xls', localtime());
        self._csvFilename = mainDir + "\\" + filename;
        columnCount = self.columnCount();
        self.rowBlank =  ['']*columnCount;
        self.rowPrev = copy.deepcopy(self.rowBlank);
        if os.path.exists(self.csvFilename):
            self.setup(newFile=False); ##open file as False - append   True - write
        else:
            self.setup(newFile=True); ##open file as False - append   True - create
            self.add_heading(fieldList);
            
        
        
    #------------------------------------------------------------------------------ 
    def __get_csvFilename(self):
        return self._csvFilename;
    csvFilename = property(__get_csvFilename, None, None, "String of csv filename");
    
    def __get_fTPdownload(self):
        return self._f_TPdownload;
    def __set_fTPdownload(self, fileInstance):
        self._f_TPdownload = fileInstance;
    def __del_fTPdownload(self):
        self._f_TPdownload.close();
    f_TPdownload = property(__get_fTPdownload, __set_fTPdownload, __del_fTPdownload, "Instance of the opened file");
    
    def __get_f_csv_TPdownload(self):
        return self._f_csv_TPdownload;
    def __set_f_csv_TPdownload(self, csvWriter):
        self._f_csv_TPdownload = csvWriter;
    def __del_f_csv_TPdownload(self):
        del self._f_csv_TPdownload
    f_csv_TPdownload = property(__get_f_csv_TPdownload, __set_f_csv_TPdownload, __del_f_csv_TPdownload, "Instance of the csv Writer");
    
    
    def __get_rowPrev(self):
        return self._rowPrev;
    def __set_rowPrev(self, row):
        self._rowPrev = row;
    def __del_rowPrev(self):
        if self._rowPrev is not None:
            self.add_row(self._rowPrev);
        self.rowPrev = copy.deepcopy(self.rowBlank);
    rowPrev = property(__get_rowPrev, __set_rowPrev, __del_rowPrev, "This is row with previous changes");
    
    #------------------------------------------------------------------------------ 
    
    def columnCount(self):
        '''
        Count how many columns can be in one row. This is needed in creation row
        input = None;
        output = int(columnMax)
        '''
        columnMax = 0;
        for groupList in fieldList_TestPlan:
            for field in fieldList_TestPlan.get(groupList):
                columnNum = field[2];
                if columnNum > columnMax:
                    columnMax = columnNum;
        return columnMax;

    def setup(self, newFile):
        ##open file as False - append   True - write
        try:
            if newFile:
                self.f_TPdownload = open(self.csvFilename, 'w+b');
                row_num = 1;
            else:
                self.f_TPdownload = open(self.csvFilename, 'a+b');
                row_num = sum(1 for row in csv.reader(self.csvFilename, dialect = csv.excel, delimiter = '\t'));  ##count all rows
                row_num = 1 + row_num ##set position on next free row
            
        except:
            log.warning(("\nPlease, close file before running script. Or file .xls was edited and saved under Excel format\n"));
            sys.exit(1);
        self.f_csv_TPdownload = csv.writer(self.f_TPdownload, dialect = csv.excel, delimiter = '\t');
#        self.f_csv_TPdownload = csv.writer(self.f_TPdownload, dialect = csv.excel_tab);
        self.row_num = row_num;

    
    def add_row(self, row):
#        print"row =", row;
        self.f_csv_TPdownload.writerow(row);
        self.f_TPdownload.flush();
        self.row_num += 1;
        return;

    def add_heading(self, fieldList):
#        cells = [fieldsTestPlan.get("TestPlan"), 
#                 fieldsTestPlan.get("TestLab")];
#        for cell in cells:
#            rowNum = cell[0];
#            columnNum = cell[1];
#            cellText = cell[2];
#            self.rowPrev[columnNum-1] = cellText;
#        del self.rowPrev
#                 
#                 
#        cells = [fieldsTestPlan.get("TestCasePlan"), 
#                 fieldsTestPlan.get("TestStepPlan"),
#                 fieldsTestPlan.get("TestSetLab"),
#                 fieldsTestPlan.get("TestCaseLab")];
#        for cell in cells:
#            rowNum = cell[0];
#            columnNum = cell[1];
#            cellText = cell[2];
#            self.rowPrev[columnNum-1] = cellText;
#        del self.rowPrev
        
        
        del self.rowPrev;
        del self.rowPrev;
        
        for cell in fieldList.get('TestCase'):
            rowNum = cell[1];
            columnNum = cell[2];
            cellText = cell[3];
            self.rowPrev[columnNum-1] = cellText;
        for cell in fieldList.get('Step'):
            rowNum = cell[1];
            columnNum = cell[2];
            cellText = cell[3];
            self.rowPrev[columnNum-1] = cellText;
            
        del self.rowPrev
                
        