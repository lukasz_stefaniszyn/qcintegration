# -*- coding: utf-8 -*-
#!/usr/bin/env python
'''
Created on 2010-06-18

@author: Lukasz Stefaniszyn
'''


from globalVariables import GlobalVariables
from lib.QualityCenter_ext.lib import csvOperation
from lib.logfile import Logger
import html2text
import os
import types
from time import strftime, localtime;

log = Logger(loggername="lib.QualityCenter_ext.lib.TestPlan_download", resetefilelog=False).log;


#==============================================================================
# Global parameters
#==============================================================================
DST_FOLDER = GlobalVariables.DIR_RESULT



class AllTestCases(object):
    '''
    Operation on ALL test cases in the given Directory
    '''
    
    def __init__(self, testPlanDownload):
        self.__tm = testPlanDownload.tm;
        self.__root = testPlanDownload.root;
        

    #------------------------------------------------------------------------------ 
    def __get_tm(self):
        return self.__tm;
    tm = property(__get_tm, None, None, "QC TestManager instance")

    def __get_root(self):
        return self.__root;
    root = property(__get_root, None, None, "QC Subject folder instance")
    #------------------------------------------------------------------------------ 
    
   
    def getAllTestCases(self, testCaseDir=None):
        '''
        Return all TestCases from given TestCase directory
        input = None
        output = list(testCasesInstance) OR None;
        '''
        
        if testCaseDir is not None:
            try:
                dirInstance = self.tm.NodeByPath(testCaseDir)
            except:
                dirInstance = None;
                log.info(("INFO: Unable find given TestCase directory: %s"%repr(str(testCaseDir))));
                return None;
            allTestCases = dirInstance.FindTests("");
            return allTestCases
        else:
            allTestCases = self.root.FindTests("");
            return allTestCases
         
    
    def getTestCase(self, testCaseDir, testCaseName):
        '''
        Return one TestCase instance from given TestCase directory and given TestCase name
        input = None
        output = instance(testCase) OR None;
        '''
        try:
            dirInstance = self.tm.NodeByPath(testCaseDir)
        except:
            dirInstance = None;
            log.info(("INFO: Unable find given TestCase directory: %s"%repr(str(testCaseDir))));
            return None;
        testCaseInstanceList = dirInstance.FindTests(Pattern=testCaseName, MatchCase=True);
        
        ##FindTest will give all TestCases with the same part of name. You have to filter and return only the one You need 
        testCaseInstanceListNew =[];
        for testCaseIns in testCaseInstanceList:
            if decodeText(testCaseIns.Name) == testCaseName:
                testCaseInstanceListNew.append(testCaseIns); 
        
#        print "!!!!testCaseInstanceListNew:", testCaseInstanceListNew;
        return testCaseInstanceListNew;
    

    def mainGetTestCase(self, testCaseDir=None, testCaseName=None):
        '''
        Return allTestCases or oneTestCase in list() format
        input = None;
        output = listInstance(TestCase OR TestCases) OR None
        '''
        if testCaseName is None:
            testCasesInstList = self.getAllTestCases(testCaseDir);
        else:
            testCasesInstList = self.getTestCase(testCaseDir, testCaseName);
        return testCasesInstList;

class TestCase(object):
    '''
    Operation on ONE test case
    '''
    def __init__(self, testCaseInstance, mappingFields):
        self._testCaseInstance = testCaseInstance;
        self.mappingFields = mappingFields;
        value = self.testCaseInstance.Name;
        self.__testCaseName = decodeText(value);
    #------------------------------------------------------------------------------ 
    def __get_testCaseInstance(self):
        return self._testCaseInstance;
    testCaseInstance = property(__get_testCaseInstance, None, None, "TestCase QualityCenter instance");
    def __get_testCaseName(self):
        return self.__testCaseName;
    testCaseName = property(__get_testCaseName, None, None, "Test Case name as string");
    #------------------------------------------------------------------------------ 
    def getFieldsValue(self):
        
        for fieldUserName, fieldValues in self.mappingFields.items():
            fieldName = fieldValues.get('FieldQcName');
            fieldType = fieldValues.get('FieldType');
            
            if fieldName=="TS_SUBJECT":
                value = self.directoryField();
                yield fieldUserName, value;
                continue;
            
            if fieldUserName =="Attachment":
                targetFolder =  os.path.join(DST_FOLDER,\
                                self.testCaseName);
                value = getAttachment(self.testCaseInstance, targetFolder);
                yield fieldUserName, value;
                continue;
            
            
            value = getFieldValue(self.testCaseInstance, fieldName, fieldType);
            value = html2text.html2text(value);
            value = value.rstrip();
            yield fieldUserName, value;
    
 
            
    def directoryField(self):
        subjectNode = self.testCaseInstance.Field("TS_SUBJECT")
        try:
            value = subjectNode.Path;
            value = decodeText(value);
        except:
            value = None; 
#        value = repr(value);
        return value;


class AllSteps(object):
    '''
    Operation on ALL steps in the given test case
    '''
    
    def __init__(self, testCaseInstance, testCaseName):
        self._testCaseInstance = testCaseInstance;
        self._designStepFactory = testCaseInstance.DesignStepFactory;
        self.__testCaseName = testCaseName
    #------------------------------------------------------------------------------ 
    def __get_testCaseName(self):
        return self.__testCaseName;
    testCaseName = property(__get_testCaseName, None, None, "Test Case name as string");
    def __get_testCaseInstance(self):
        return self._testCaseInstance;
    testCaseInstance = property(__get_testCaseInstance, None, None, "TestCase QualityCenter instance");
    
    def __get_designStepFactory(self):
        return self._designStepFactory;
    designStepFactory = property(__get_designStepFactory, None, None, "TestCase DesignStepFactory instance");  
    #------------------------------------------------------------------------------ 


    def getAllSteps(self):
        
        stepList = self.designStepFactory.NewList("");
        return stepList;


class Step(object):
    '''
    Operation on ONE step in the given test case
    input = qcInstance(step), str(testCaseName)
    '''
    
    def __init__(self, stepInstance, testCaseName, mappingFields):
        self._stepInstance = stepInstance;
        self._testCaseName = testCaseName;
        self.mappingFields = mappingFields;
        
        value = self.stepInstance.StepName;
        self.__stepName = decodeText(value);
    #------------------------------------------------------------------------------ 
    def __get_stepInstance(self):
        return self._stepInstance;
    stepInstance = property(__get_stepInstance, None, None, "Step QualityCenter instance");
    def __get_stepName(self):
        return self.__stepName;
    stepName = property(__get_stepName, None, None, "Step name as string");
    def __get_testCaseName(self):
        return self._testCaseName;
    testCaseName = property(__get_testCaseName, None, None, "Test Case name as string");
    #------------------------------------------------------------------------------ 
      
    def getFieldsValue(self):
        
        for fieldUserName, fieldValues in self.mappingFields.items():
            fieldName = fieldValues.get('FieldQcName');
            fieldType = fieldValues.get('FieldType');
            
            if fieldUserName =="Attachment":
                targetFolder =  os.path.join(DST_FOLDER,\
                                self.testCaseName,\
                                self.stepName);
                value = getAttachment(self.stepInstance, targetFolder);
                yield fieldUserName, value;
                continue;
            
            
            value = getFieldValue(self.stepInstance, fieldName, fieldType);
            value = html2text.html2text(value);
            value = value.rstrip();
            yield fieldUserName, value;
    #------------------------------------------------------------------------------    



class TestPlanDownload(object):

    def __init__(self, qc):
        self.__tm = qc.TreeManager;
        self.__root = self.tm.TreeRoot("Subject");

    #------------------------------------------------------------------------------ 
    def __get_tm(self):
        return self.__tm;
    tm = property(__get_tm);
    
    def __get_root(self):
        return self.__root;
    root = property(__get_root);
    
    def __get_path(self):
        return self.__path;
    def __set_path(self, pathMain):
        value = decodeText(pathMain);
        self.__path = value;
    path = property(__get_path, __set_path, None, "Main directory of the current download process");
    #------------------------------------------------------------------------------

    def makeTargetDir(self):
        '''
        This will create .\..\Result with the catalog of the current download in format Download_%d-%m-%Y_%H-%M-%S
        input = None
        output = None
        '''
        mainPath = os.path.join(GlobalVariables.DIR_RESULT, 'TestPlan');
        if not os.path.exists(mainPath):
            os.mkdir(mainPath);
        resultName = strftime('Download_%d-%m-%Y_%H-%M-%S', localtime());
        self.path = os.path.join(mainPath,resultName);
        os.mkdir(self.path);
 
 
def getFieldValue(instance, fieldName, fieldType):
    '''
    This will return field value
    @param fieldName: this is QC field name
    @param fieldType: this is QC field type, like 'char', 'time'
    '''
#    log.debug(("getFieldValue for fieldName, fieldType:",(fieldName, fieldType)));
    try:
        value = instance.Field(FieldName=fieldName);
    except:
        log.debug(("Unable to read FieldName:",fieldName));
        value=''
        return value;
    
    if value is None: 
        value='';
    
    
    if type(value)==types.StringTypes or type(value)==types.UnicodeType:
        value = decodeText(value);
    elif type(value)==types.IntType or type(value)==types.FloatType:
        value = str(value);
    elif type(value)==types.InstanceType:
        value = '';
    else:
        try:value = str(value);
        except: value=''; 
    
    return value;

def getAttachment(instance, targetDir):
        '''This will take all attachments from QualityCenter database and save into user directory (TagedDir)
        input = str(os.path(targetDir)) 
        output = str(;attachmentDir;attachmentDir;attachmentDir) OR str('')
        '''
        
        f_attachments = open(os.path.join(DST_FOLDER,"missingAttachments.txt"), "a");
        
        attDstAll = '';
        ##Check if in instance are any attachments
        if (instance.HasAttachment):
            dstFolder = targetDir; 
            ##Make catalog of this Destination Folder
            try:
                os.makedirs(dstFolder);
            except:
                log.info("Unable to create Destination Folder for keep attachment. Skipping attachment")
                return attDstAll;
            
            for att in instance.Attachments.NewList(""):
                ##for every attachment make download it to local machine
                attName = att.GetName();
                attName = decodeText(attName);
                if att.FileSize == 0:
                    information = "Attachment is empty: %s     in : %s"%(attName.split("_", 2)[-1], repr(dstFolder));
                    log.debug(information);
                    f_attachments.write(information+'\n');
                    f_attachments.flush();
                    continue;
                ##This Load will download attachment from QC database to local machine, but to the default user catalog 
                ##synchronize If True, program run waits for download to complete. Otherwise, download is asynchronous. 
                attLocalCatalog = att.Load(True, ""); 
                ##This will move this downloaded attachment file to Result file 
                attSrc = attLocalCatalog+'\\'+attName;
                ##Create name of the Destination Folder, where attachment from this instance will be keept
                attDst = dstFolder+'\\'+ attName.split("_", 2)[-1]; 
                attDstAll += ';' + attDst;
                try:
                    os.renames(attSrc, attDst);
                except:
                    log.info(("WRN: Unable to get attachment file from QualityCenter, due to the insufficient permission in moving to catalog or catalog/file already exists: ",repr(dstFolder)));
        attDstAll = attDstAll.strip(';');
        
        f_attachments.close();
        return attDstAll;

def decodeText(value):
#        value = repr(value)
#        value = value.lstrip("u'")
#        value = value.rstrip("'")
#        value = unicode(value, errors="ignore")
        try:
            value = value.encode('ascii', 'ignore');
        except: 
            try: 
                value = repr(value);
            except:
                value = "Wrong data";
                log.debug("Not able to decode Text");
        return value;     
    
class ProgressDialog(object):
    
    def __init__(self, progressExecutionDialog=None):
        self.progressExecutionDialog = progressExecutionDialog;
        
    def checkcancelStatus(self):
        if self.progressExecutionDialog is not None:
            return self.progressExecutionDialog.cancelStatus;
        else: 
            return False;
        
        
    def updateTextDialog(self, text):
        '''
        Write new line to Dialog Gui 
        *input*
        @param text: str()
        *output*:
        None
        '''
        if self.progressExecutionDialog is not None:
            self.progressExecutionDialog.updateTextDialog(text);
        else: 
            print(text);
        log.info(text);
            
    def updateProgressBar(self):
        '''
        Update number+1 on Dialog Gui of ProgressBar
        *input*
        None
        *output*
        None
        '''
        if self.progressExecutionDialog is not None:
            self.progressExecutionDialog.updateProgressBar();

class MainExecution(object):
    
    def __init__(self, 
                 gui=False,
                 cmd=False,
                 qc=None, 
                 testCaseDir=None, 
                 testCaseName=None, 
                 mappingFields=None, 
                 progressExecutionDialog=None):
        
        self.__mappingFields = mappingFields;
        self.__qc = qc;
        self.__progressDialog = None;
        self.path = None;
        
        ##This will be used in the Gui or CMD run. This will show progress of execution
        self.__progressDialog = ProgressDialog(progressExecutionDialog);
        
        
        if gui and not cmd:##start Gui part
            self.path = self.mainGuiStart();
        elif cmd and not gui:##start CMD part
            self.path = self.mainCMD(testCaseDir, testCaseName)
        else:
            log.error(("Decide which main script have to be executed: Gui or CMD"));

    def __del__(self):
        self.progressDialog.updateTextDialog("File is saved under: %s"%self.writeCsv.csvFilename);

    def get_progress_dialog(self):
        return self.__progressDialog
    progressDialog = property(get_progress_dialog, None, None, "progressDialog's docstring")

    def get_qc(self):
        return self.__qc
    def del_qc(self):
        del self.__qc
    qc = property(get_qc, None, del_qc, "qc's docstring")
            

    def get_mapping_fields(self):
        return self.__mappingFields
    mappingFields = property(get_mapping_fields, None, None, "mappingFields's docstring")


    def mainGuiStart(self):
        '''
        This function will be used as first in Gui. 
        I had to divide main to Gui and CMD. 
        Reason is to have main() where only TestCase read will be executed not also:
        - creating new CSV file, 
        - new instances, etc.
        '''
        testPlanDownload = TestPlanDownload(self.qc);
        testPlanDownload.makeTargetDir();
        try: 
            path = testPlanDownload.path;
            filename = os.path.basename(path) + ".xls";
            fileDir = os.path.join(path, filename)
            
            self.writeCsv = csvOperation.WriteCSV(fileDir, self.mappingFields);
            self.progressDialog.updateTextDialog("File will be saved under: %s"%fileDir);
        except csvOperation.FileOpenException, err:
            log.exception((err));
            return False;
    
        return testPlanDownload.path
    
    
    
    def mainCMD(self, testCaseDir, testCaseName):
        '''
        This function will be used as one for CommandLine execution. 
        I had to divide main to Gui and CMD. 
        Reason is to have main() where only TestCase read will be executed not also:
        - creating new CSV file, 
        - new instances, etc.
        :param testCaseDir: str() TestCase directory
        :param testCaseName: str() TestCase Name
        
        '''
        testPlanDownload = TestPlanDownload(self.qc);
        testPlanDownload.makeTargetDir();
        try: 
            path = testPlanDownload.path;
            filename = os.path.basename(path) + ".xls";
            fileDir = os.path.join(path, filename);
            
            self.writeCsv = csvOperation.WriteCSV(fileDir, self.mappingFields);
            self.progressDialog.updateTextDialog("File will be saved under: %s"%fileDir);
        except csvOperation.FileOpenException, err:
            log.exception((err));
            return False;
    
        #======================================================================
        # Part responsible for TestCases 
        #======================================================================
        allTestCases = AllTestCases(testPlanDownload);
        testCasesInstList = allTestCases.mainGetTestCase(testCaseDir, testCaseName);
        if testCasesInstList == []:
            log.info("No Test Cases were found");
            return testPlanDownload.path; 
        
        for testCaseInstance in testCasesInstList:
            self.main(testCaseInstance);
        
        return testPlanDownload.path
        
    
    def main(self, testCaseInstance):
        '''
        This function will only read TestCase values
        @param testCaseInstance: inst() of QualityCenter TestCase 
        '''
        
        ##increase in the ProgressBar run TEstCase's by 1. If this is Gui execution. If not than it will do nothing
        self.progressDialog.updateProgressBar() 
        
        
        #======================================================================
        # Part responsible for TestCases 
        #======================================================================
        ##TestCase operations
        groupName = 'TestCase'
        testCase = TestCase(testCaseInstance, self.mappingFields[groupName]);
        testCaseName = testCase.testCaseName;
        self.progressDialog.updateTextDialog("TestCase: %s"%repr(testCaseName));
        for fieldUserName, fieldValue in testCase.getFieldsValue():
            self.writeCsv.updateCell(groupName, fieldUserName, fieldValue);
        self.writeCsv.addRow();
       
        #==================================================================
        # Part responsible for Steps
        #==================================================================
        allSteps = AllSteps(testCaseInstance, testCaseName);
        allStepsInstance = allSteps.getAllSteps();
        if allStepsInstance is None:
            return;##Save current row and go to next TestCase
            
        
        for stepIntance in allStepsInstance:
            ##Step operations
            step = Step(stepIntance, testCaseName, self.mappingFields.get('Step'));
        
        allSteps = AllSteps(testCaseInstance, testCaseName);
        allStepsInstance = allSteps.getAllSteps();
        if allStepsInstance is None:
            log.info("No Steps were found");
            return;
        for stepInstance in allStepsInstance:
            ##Step operations
            groupName = 'Step'
            step = Step(stepInstance, testCaseName, self.mappingFields[groupName]);
            stepName = step.stepName;
            self.progressDialog.updateTextDialog("\t Step: %s"%repr(stepName));
            for fieldUserName, fieldValue in step.getFieldsValue():
                self.writeCsv.updateCell(groupName, fieldUserName, fieldValue);
            self.writeCsv.addRow();
        return True

