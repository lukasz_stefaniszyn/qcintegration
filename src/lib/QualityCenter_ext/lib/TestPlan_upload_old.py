# -*- coding: utf-8 -*-
#!/usr/bin/env python

'''
Created on 2009-09-18

@author: Lukasz Stefaniszyn
'''




class TestPlan_upload(object):
    
    
    
    def create_folder(self, name, position=None):
        ''' Create folder in TestLab: root level  or under NodeName=position level
        input = str(name), position=str(NodeName_search)
        output = None
        example: create_folder(name="test1", position="Lukst1\\test") will create "test1" under "Root\Lukst1\test"
        
        '''
        if position is None:
            ##Add new folder in Root level
            try:
                self.TL_root.AddNode(NodeName=name);
            except:
                print "Folder \"%s\" already created under Root"%unicode(name);
                return False;
            return True;
        else:
            try:
                ##Check before add if Folder is present
                path = position+'\\'+name;
                found_child = self.TL_tm.NodeByPath(Path=path);
                print "found_child:",unicode(found_child);
                present_folder = True;
                print "Folder \"%s\" is already present under \"%s\""%(unicode(name), unicode(found_child.Path));
                #return "present_folder = True";
            except:
                present_folder = False;
                #return "present_folder = False";
                

            try:
                ##Go to correct ChildName, under which will be added new folder
                path = position;
                parent = self.TL_tm.NodeByPath(Path=path);
            except:
                print "Did not find NodeName \"%s\" under node \"%s\""%(unicode(name), unicode(position));
                return False;
            if not present_folder:
                parent.AddNode(NodeName=name);
                
            return True;