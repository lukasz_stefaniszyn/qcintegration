# -*- coding: utf-8 -*-
#!/usr/bin/env python

import win32com.client as w32c
import pywintypes


from win32com.client import gencache, DispatchWithEvents, constants
#gencache.EnsureModule('{F645BD06-E1B4-4E6A-82FB-E97D027FD456}', 0, 1, 0);
#w32c.Dispatch("TDApiole80.TDConnection");

import sys;
import subprocess

import http_con

from lib.logfile import Logger
log = Logger(loggername="lib.QualityCenter_ext.lib.qc_connect_disconnect", resetefilelog=False).log;


class QC_connect(object):
    '''Connect to the Quality server '''
    
    ##QC connection instance
    qc = None;
    
    
    def __init__(self, server, UserName, Password, DomainName, ProjectName):
        self.__server = server;
        self.__UserName = UserName;
        self.__Password = Password;
        self.__DomainName = DomainName;
        self.__ProjectName = ProjectName;

    def __del__(self):
        del self.qc
        
    #-----------------------------------------------------------------------------        
    def __get_server(self):
        return self.__server;
    server = property(__get_server);
    
    def __get_login(self):
        login = self.__UserName, self.__Password;
        return login;
    login = property(__get_login);
    
    def __get_project(self):
        project = self.__DomainName, self.__ProjectName;
        return project;
    project = property(__get_project);    
#------------------------------------------------------------------------------ 
        

        
        
    
    def QC_instance(self):
        '''Create QualityServer instance under self.qc
        input = None
        output = bool(True/False)'''
        
        qcHttpPage = http_con.QcHttpPage(self.server, dstDir='.'); ##Download/update file for OTA Api from QC server
        filenameResults = qcHttpPage.filenameResult; ##Filenames which where downloaded
        
        for filename in filenameResults: 
            log.debug(("Filename to create Key Win:", filename));
            subprocess.call(["regsvr32","/s", filename]); ##Register downloaded files in the Windows Register Key
        
        try:
            self.qc = w32c.Dispatch("TDApiole80.TDConnection");
            text = "DLL QualityCenter file correctly Dispatched"
            return True
        except:
            text = "ERR: Unable to find QualityCenter installation files.\nPlease connect first to QualityCenter by web page to install needed files" 
            log.info(text);
            return False;
       
    def connect_server(self, server):
        '''Connect to QC server
        input = str(http adress)
        output = bool(connected) TRUE/FALSE  '''
        try:
            self.qc.InitConnectionEx(server); 
        except:
            text = "Unable connect to Quality Center database: '%s'"%(server); 
            log.warning(("ERR: ",text)); 
        return self.qc.Connected;
    
    def connect_login(self, login):
        '''Login to QC server
        input = str(UserName), str(Password)
        output = bool(Logged) TRUE/FALSE  '''
        UserName, Password = login;
        try:
            self.qc.Login(UserName, Password);
        except pywintypes.com_error, err:
            text = unicode(err[2][2]);
            log.info(("ERR:", text));
        return self.qc.LoggedIn;
    
    def connect_project(self, project):
        '''Connect to Project in QC server
        input = str(DomainName), str(ProjectName)
        output = bool(ProjectConnected) TRUE/FALSE  '''
        DomainName, ProjectName = project
        try:
            self.qc.Connect(DomainName, ProjectName)
        except pywintypes.com_error, err:
            text = "Repository of project '%s' in domain '%s' doesn't exist or is not accessible. Please contact your Site Administrator"%(ProjectName,DomainName); 
            log.info(("ERR: ", unicode(err[2][2])));
            log.info(("ERR: ",text));
        return self.qc.ProjectConnected;
    
    def connect(self):
        yield (True, "Getting QC running files");
        if self.QC_instance():
            yield (True, "Connecting to QC server");
            if self.connect_server(self.server):
                ##connected to server
                yield (True, "Checking username and password");
                if self.connect_login(self.login):
                    yield (True, "Connecting to QC domain and project");
                    if self.connect_project(self.project):
                        text = "Connected"
                        log.info(text)
                        connected = True;
                        yield connected, text;
                    else:
                        text = "Not connected to Project in QC server.\nPlease, correct DomainName and/or ProjectName";
                        log.info((text)); 
                        connected = False;
                        yield connected, text;
                else:
                    text = "Not logged to QC server.\nPlease, correct UserName and/or Password";
                    log.info((text))
                    connected = False;
                    yield connected, text;
            else:
                text = "Not connected to QC server.\nPlease, correct server http address"; 
                log.info((text));
                connected = False;
                yield connected, text;
        else:
            connected = False;
            text = "Unable to find QualityCenter installation files.\nPlease connect first to QualityCenter by web page to install needed files" 
            yield connected, text;
    
    def disconnect_QC(self):
        '''Disconnect from QC server
        input = None
        output = bool(^disconnected);'''
        
        try:
            ##Disconnect from Project
            self.qc.Disconnect();
            ##Logout user
            self.qc.Logout();
            ##Disconnect from QC server;
            self.qc.ReleaseConnection();
        except:
            log.warning("Not able to QC disconnected");
            return False
            
        log.info("QC disconnected");
        return not self.qc.Connected;


if __name__ == "__main__":
    Server= r"http://qc.inside.com/qcbin/"
    UserName= "wro50026"
    Password= "Lukste12&"
    DomainName= "NWS_ON"
    ProjectName= "hiT7300" 
#    
#    Server= "http://muvmp034.nsn-intra.net/qcbin/"
#    UserName= "stefaniszyn"
#    Password= "Lukste12!"
#    DomainName= "HIT7300"
#    ProjectName= "hiT73_R43x"
    
    
    qcConnect = QC_connect(Server, 
                         UserName, 
                         Password, 
                         DomainName, 
                         ProjectName);
    connection_status = qcConnect.connect();
    print "connection_status:", connection_status
    qcConnect.disconnect_QC();
    print "Disconnection";