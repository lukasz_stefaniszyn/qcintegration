# -*- coding: utf-8 -*-
#!/usr/bin/env python
'''
Created on 2009-09-15

@author: Lukasz Stefaniszyn
'''


#from time import localtime, strftime
import re;


import csvOperation
import html2text
import os
import pywintypes
import types
from time import strftime, localtime;


from globalVariables import GlobalVariables

from lib.logfile import Logger
log = Logger(loggername="lib.QualityCenter_ext.lib.TestLab_download", resetefilelog=False).log;




class TestLabDownload(object):
    '''This will make all action that will take Values from QC'''

    #------------------------------------------------------------------------------ 
    def __init__(self, qc):
        self.__tm = qc.TestSetTreeManager;
        self.__root = qc.TestSetTreeManager.Root;
        
    #------------------------------------------------------------------------------ 
    def __get_tm(self):
        return self.__tm;
    tm = property(__get_tm);
    
    def __get_root(self):
        return self.__root;
    root = property(__get_root);
    
    def __get_path(self):
        return self.__path;
    def __set_path(self, pathMain):
        value = decodeText(pathMain);
        self.__path = value;
    path = property(__get_path, __set_path, None, "Main directory of the current download process");
    #------------------------------------------------------------------------------

    def makeTargetDir(self):
        '''
        This will create .\..\Result\TestLab with the catalog of the current download in format Download_%d-%m-%Y_%H-%M-%S
        input = None
        output = None
        '''
        mainPath = os.path.join(GlobalVariables.DIR_RESULT, 'TestLab');
        if not os.path.exists(mainPath):
            os.mkdir(mainPath);
        resultName = strftime('Download_%d-%m-%Y_%H-%M-%S', localtime());
        self.path = os.path.join(mainPath, resultName);
        os.mkdir(self.path);
    

class AllTestSets(object):
    '''
    Operation on ALL test Sets in the given Directory
    '''
    
    def __init__(self, tm, root):
        self.__tm = tm
        self.__root = root
        

    #------------------------------------------------------------------------------ 
    def __get_tm(self):
        return self.__tm;
    tm = property(__get_tm, None, None, "QC TestManager instance")

    def __get_root(self):
        return self.__root;
    root = property(__get_root, None, None, "QC Subject folder instance")
    #------------------------------------------------------------------------------ 
    
    def getAllTestSets(self, TSdir=None):
        '''This will give all catalogs and TestSets under correct parent_catalog
        input = None
        output = list(testSetsInstance) OR None;
        '''
        
        
        
        if TSdir is not None:
            ##this will serch only the specific TestSet
            
            ##this will set the parent catalog from which we will search TestSet
            try:
                dirInstance = self.tm.NodeByPath(Path=TSdir);
            except:
                dirInstance = None;
                log.info(("INFO: Unable find given TestSet directory: %s"%repr(str(TSdir))));
                return None;
            if dirInstance is None:
                log.info(("INFO: Given TestSet directory does not exists: %s"%repr(str(TSdir))));
                return None
            allTestSets = dirInstance.FindTestSets("");
            return allTestSets;
        else: 
            allTestSets = self.root.FindTestSets("");
            return allTestSets;
       
    def getTestSet(self, testSetDir, testSetName):
        '''
        Return one TestSet instance from given TestSet directory and given TestSet name
        input = None
        output = instance(testSet) OR None;
        '''
        try:
            dirInstance = self.tm.NodeByPath(Path=testSetDir)
        except:
            dirInstance = None;
            log.info(("INFO: Unable find given TestSet directory: %s"%repr(str(testSetDir))));
            return None;
        testSetInstanceList = dirInstance.FindTestSets(Pattern=testSetName, MatchCase=True);
        return testSetInstanceList;
    

    def mainGetTestSet(self, testSetDir=None, testSetName=None):
        '''
        Return allTestSets or oneTestSet in list() format
        input = None;
        output = listInstance(TestSet OR TestSets) OR None
        '''
        if testSetName is None:
            testSetsInstList = self.getAllTestSets(testSetDir);
        else:
            testSetsInstList = self.getTestSet(testSetDir, testSetName);
        return testSetsInstList;


class TestSet(object):
    '''
    Operation on ONE test Set
    
    @testSetInstance = qcInstance(TestSet)
    @mapping_fields =this is user choosen fields only for TestSet
                   'Step': {
                            'Attachment': {
                                           'FieldIsRequired': 'False',
                                           'FieldOrder': '2',
                                           'FieldQcName': 'ATTACHMENT',
                                           'FieldType': 'char'},
                            'Status': {
                                       'FieldIsRequired': 'False',
                                       'FieldOrder': '0',
                                       'FieldQcName': u'ST_STATUS',
                                       'FieldType': u'char'},
                            'Step Name': {
                                          'FieldIsRequired': 'False',
                                          'FieldOrder': '1',
                                          'FieldQcName': u'ST_STEP_NAME',
                                          'FieldType': u'char'}},
                   'TestCase': {
                                'Attachment': {
                                               'FieldIsRequired': 'False',
                                               'FieldOrder': '5',
                                               'FieldQcName': 'ATTACHMENT',
                                               'FieldType': 'char'},
    '''
    def __init__(self, testSetInstance,  mappingFields):
        self.__testSetInstance = testSetInstance;
        
        value = self.testSetInstance.Field("CY_CYCLE");
        self.__testSetName = decodeText(value); 
        
        self.mappingFields = mappingFields;
    #------------------------------------------------------------------------------ 
    def __get_testSetName(self):
        return self.__testSetName
    testSetName = property(__get_testSetName, None, None, "Test Set name as string");
    
    def __get_testSetInstance(self):
        return self.__testSetInstance;
    testSetInstance = property(__get_testSetInstance, None, None, "TestSet QualityCenter instance");
    #------------------------------------------------------------------------------ 
    def getFieldsValue(self):
        
        for fieldUserName, fieldValues in self.mappingFields.items():
            fieldName = fieldValues.get('FieldQcName');
            fieldType = fieldValues.get('FieldType');
            
            if fieldUserName =="Test Set Folder":
                value = self.__directoryField();
                yield fieldUserName, value;
                continue;
            elif fieldUserName =="Attachment":
                targetFolder =  os.path.join(GlobalVariables.DIR_RESULT,self.testSetName);
                value = getAttachment(self.testSetInstance, targetFolder);
                yield fieldUserName, value;
                continue;
            
            
            value = getFieldValue(self.testSetInstance, fieldName, fieldType);
            value = html2text.html2text(value);
            value = value.rstrip();
            yield fieldUserName, value;
            
            
    def __directoryField(self):
        value = self.testSetInstance.TestSetFolder.Path
        value = decodeText(value);
        return value;
    
    
class AllTestCases(object):
    '''
    Operation on ALL test cases from given TestSet instance
    '''
    
    def __init__(self, testSetInstance, testSetName):
        self.__testSetName = testSetName
        self.__testSetInstance = testSetInstance;
    #------------------------------------------------------------------------------ 
    def __get_testSetName(self):
        return self.__testSetName;
    testSetName = property(__get_testSetName, None, None, "Test Set name as string");
    
    
    def __get_testSetInstance(self):
        return self.__testSetInstance;
    testSetInstance = property(__get_testSetInstance, None, None, "TestSet QualityCenter instance");
    #------------------------------------------------------------------------------ 
    
    def getAllTestCases(self):
        '''Create list of TestCases which are under given TestSet_instance
        *Input*
        None
        
        *Output*
        @param allTestCases:  list() of TestCases from given TestSet instance 
        '''
        
        testSetFactory = self.testSetInstance.TSTestFactory;
        if testSetFactory is not None:
            allTestCases = testSetFactory.NewList("");
        else:
            log.info("No TestCase instance in given TestSet");
        return allTestCases;    
       
class TestCase(object):
    '''
    Operation on ONE test case    
    
    @testcaseInstance = qcInstance(TestCase)
    @mapping_fields =this is user choosen fields only for TestCase
                          {"TestCase": {'Test Case Name':('TC_NAME', 'char', 'True', '0'),
                                     'Test Case Directory': ('TC_DIR', 'char', 'True', '1'),
                                     'Test Case Description': ('TC_DESC', 'char', 'True', '2')
                                     },
                          };
    '''
    def __init__(self, testCaseInstance, testSetName, mappingFields):
        self.__testSetName = testSetName
        self.__testCaseInstance = testCaseInstance;

        value = self.testCaseInstance.Field("TS_NAME");
        self.__testCaseName = decodeText(value); 
        

        self.mappingFields = mappingFields;
    #------------------------------------------------------------------------------ 
    def __get_testSetName(self):
        return self.__testSetName;
    testSetName = property(__get_testSetName, None, None, "Test Set name as string");
    def __get_testCaseName(self):
        return self.__testCaseName;
    testCaseName = property(__get_testCaseName, None, None, "Test Case name as string");
    
    
    def __get_testCaseInstance(self):
        return self.__testCaseInstance;
    testCaseInstance = property(__get_testCaseInstance, None, None, "TestCase QualityCenter instance");
    #------------------------------------------------------------------------------ 


    def getFieldsValue(self):
        
        for fieldUserName, fieldValues in self.mappingFields.items():
            fieldName = fieldValues.get('FieldQcName');
            fieldType = fieldValues.get('FieldType');
            
            if fieldUserName =="Attachment":
                targetFolder =  os.path.join(GlobalVariables.DIR_RESULT,\
                                self.testSetName,\
                                self.testCaseName);
                value = getAttachment(self.testCaseInstance, targetFolder);
                yield fieldUserName, value;
                continue;
            
            
            value = getFieldValue(self.testCaseInstance, fieldName, fieldType);
            value = html2text.html2text(value);
            value = value.rstrip();
            yield fieldUserName, value;
    
    
class AllSteps(object):
    '''
    Operation on ALL steps in the given test case
    '''
    
    def __init__(self, testCaseInstance, testSetName, testCaseName):
        self.__testSetName = testSetName
        self.__testCaseName = testCaseName
        self.__testCaseInstance = testCaseInstance;
    #------------------------------------------------------------------------------ 
    def __get_testSetName(self):
        return self.__testSetName;
    testSetName = property(__get_testSetName, None, None, "Test Set name as string");
    def __get_testCaseName(self):
        return self.__testCaseName;
    testCaseName = property(__get_testCaseName, None, None, "Test Case name as string");
    
    
    def __get_testCaseInstance(self):
        return self.__testCaseInstance;
    testCaseInstance = property(__get_testCaseInstance, None, None, "TestCase QualityCenter instance");
    #------------------------------------------------------------------------------ 


    def getAllSteps(self):
        '''Create list of Steps from last Run in TestCase'''
        
        LastRun = self.testCaseInstance.LastRun;
        if LastRun is None:
            ##IF there was no Run on this TestCase, then we have make new run, get Steps and cancel this run
            if self.testCaseInstance.HasSteps:
                run_factory = self.testCaseInstance.RunFactory;
                ##Create temporary Run
                run_name = "Temporary_Run to get Steps"
                run = run_factory.AddItem(run_name);
                ##Get Steps from TestPlan 
                run.CopyDesignSteps(); 
                StepFactor = run.StepFactory;
                
                allSteps = StepFactor.NewList("")
                
                ##Delete this temporary Run
                run.CancelRun();
                return allSteps;
        else:
            StepFactor = LastRun.StepFactory;
            if StepFactor is not None:
                allSteps = StepFactor.NewList("")
            else:
                log.info("No Run instance");
            return allSteps;

class Step(object):
    '''
    Operation on ONE step in the given test case
    input = qcInstance(step), str(testCaseName)
    '''
    
    def __init__(self, stepInstance, testSetName, testCaseName, mappingField):
        self.__stepInstance = stepInstance;
        self.__testCaseName = testCaseName;
        self.__testSetName = testSetName;
        
        value = self.stepInstance.Field("ST_STEP_NAME");
        self.__stepName = decodeText(value); 
        
        self.mappingField = mappingField;
    #------------------------------------------------------------------------------ 
    def __get_stepInstance(self):
        return self.__stepInstance;
    stepInstance = property(__get_stepInstance, None, None, "Step QualityCenter instance");
    
    def __get_testSetName(self):
        return self.__testSetName;
    testSetName = property(__get_testSetName, None, None, "Test Set name as string");
    def __get_testCaseName(self):
        return self.__testCaseName;
    testCaseName = property(__get_testCaseName, None, None, "Test Case name as string");
    def __get_stepName(self):
        return self.__stepName;
    stepName = property(__get_stepName, None, None, "Step name as string");
    
    #------------------------------------------------------------------------------ 
    
    def getFieldsValue(self):
        
        for fieldUserName, fieldValues in self.mappingField.items():
            fieldName = fieldValues.get('FieldQcName');
            fieldType = fieldValues.get('FieldType');
            
            if fieldUserName =="Attachment":
                targetFolder =  os.path.join(GlobalVariables.DIR_RESULT,\
                                self.testSetName,\
                                self.testCaseName,\
                                self.stepName);
                value = getAttachment(self.stepInstance, targetFolder);
                yield fieldUserName, value;
                continue;
            
            
            value = getFieldValue(self.stepInstance, fieldName, fieldType);
            value = html2text.html2text(value);
            value = value.rstrip();
            yield fieldUserName, value;
    #------------------------------------------------------------------------------        
def getFieldValue(instance, fieldName, fieldType):
    '''
    This will return field value
    @param fieldName: this is QC field name
    @param fieldType: this is QC field type, like 'char', 'time'
    '''
#    log.debug(("getFieldValue for fieldName, fieldType:",(fieldName, fieldType)));
    value = instance.Field(FieldName=fieldName);
    
    if value is None: 
        value='';
    
    
    if type(value)==types.StringTypes or type(value)==types.UnicodeType:
        value = decodeText(value);
    elif type(value)==types.IntType or type(value)==types.FloatType:
        value = str(value);
    elif type(value)==types.InstanceType:
        value = '';
    else:
        try:value = str(value);
        except: value=''; 
    
    return value;

def getAttachment(instance, targetDir):
        '''This will take all attachments from QualityCenter database and save into user directory (TagedDir)
        input = str(os.path(targetDir)) 
        output = str(;attachmentDir;attachmentDir;attachmentDir) OR str('')
        '''
        
        f_attachments = open(os.path.join(GlobalVariables.DIR_RESULT,r"missingAttachments.txt"), "a");
        
        attDstAll = '';
        ##Check if in instance are any attachments
        if (instance.HasAttachment):
            dstFolder = targetDir; 
            ##Make catalog of this Destination Folder
            try:
                os.makedirs(dstFolder);
            except:
                log.info("Unable to create Destination Folder for keep attachment. Skipping attachment")
                return attDstAll;
            
            for att in instance.Attachments.NewList(""):
                ##for every attachment make download it to local machine
                attName = att.GetName();
                attName = decodeText(attName);
                if att.FileSize == 0:
                    information = "Attachment is empty: %s     in : %s"%(attName.split("_", 2)[-1], repr(dstFolder));
                    log.debug(information);
                    f_attachments.write(information+'\n');
                    f_attachments.flush();
                    continue;
                ##This Load will download attachment from QC database to local machine, but to the default user catalog 
                ##synchronize If True, program run waits for download to complete. Otherwise, download is asynchronous. 
                attLocalCatalog = att.Load(True, ""); 
                ##This will move this downloaded attachment file to Result file 
                attSrc = attLocalCatalog+'\\'+attName;
                ##Create name of the Destination Folder, where attachment from this instance will be keept
                attDst = dstFolder+'\\'+ attName.split("_", 2)[-1]; 
                attDstAll += ';' + attDst;
                try:
                    os.renames(attSrc, attDst);
                except:
                    log.info(("WRN: Unable to get attachment file from QualityCenter, due to the insufficient permission in moving to catalog or catalog/file already exists: ",repr(dstFolder)));
        attDstAll = attDstAll.strip(';');
        
        f_attachments.close();
        return attDstAll;

def decodeText(value):
#        value = repr(value)
#        value = value.lstrip("u'")
#        value = value.rstrip("'")
#        value = unicode(value, errors="ignore")
        try:
            value = value.encode('ascii', 'ignore');
        except: 
            try: 
                value = repr(value);
            except:
                value = "Wrong data";
                log.debug("Not able to decode Text");
        return value;       


class ProgressDialog(object):
    
    def __init__(self, progressExecutionDialog=None):
        self.progressExecutionDialog = progressExecutionDialog;
        
    def checkcancelStatus(self):
        if self.progressExecutionDialog is not None:
            return self.progressExecutionDialog.cancelStatus;
        else: 
            return False;
        
        
    def updateTextDialog(self, text):
        '''
        Write new line to Dialog Gui 
        *input*
        @param text: str()
        *output*:
        None
        '''
        if self.progressExecutionDialog is not None:
            self.progressExecutionDialog.updateTextDialog(text);
        else: 
            print(text);
        log.info(text);
            
    def updateProgressBar(self):
        '''
        Update number+1 on Dialog Gui of ProgressBar
        *input*
        None
        *output*
        None
        '''
        if self.progressExecutionDialog is not None:
            self.progressExecutionDialog.updateProgressBar();


class MainExecution(object):
    
    def __init__(self, 
                 gui=False,
                 cmd=False,
                 qc=None, 
                 testSetDir=None, 
                 testSetName=None, 
                 mappingFields=None, 
                 progressExecutionDialog=None):
        
        self.__mappingFields = mappingFields;
        self.__qc = qc;
        self.__progressDialog = None;
        self.path = None;
        
        ##This will be used only in the Gui run. This will show progress of execution
        self.__progressDialog = ProgressDialog(progressExecutionDialog);
        
        
        if gui and not cmd:##start Gui part
            self.path = self.mainGuiStart();
        elif cmd and not gui:##start CMD part
            self.path = self.mainCMD(testSetDir, testSetName)
        else:
            log.error(("Decide which main script have to be executed: Gui or CMD"));

    def __del__(self):
        self.progressDialog.updateTextDialog("File is saved under: %s"%self.writeCsv.csvFilename);

    def get_progress_dialog(self):
        return self.__progressDialog
    progressDialog = property(get_progress_dialog, None, None, "progressDialog's docstring")

    def get_qc(self):
        return self.__qc
    def del_qc(self):
        del self.__qc
    qc = property(get_qc, None, del_qc, "qc's docstring")
            

    def get_mapping_fields(self):
        return self.__mappingFields
    mappingFields = property(get_mapping_fields, None, None, "mappingFields's docstring")


    def mainGuiStart(self):
        '''
        This function will be used as first in Gui. 
        I had to divide main to Gui and CMD. 
        Reason is to have main() where only TestCase read will be executed not also:
        - creating new CSV file, 
        - new instances, etc.
        '''
        testLabDownload = TestLabDownload(self.qc);
        testLabDownload.makeTargetDir();
        try: 
            path = testLabDownload.path;
            filename = os.path.basename(path) + ".xls";
            fileDir = os.path.join(path,filename); 
            self.writeCsv = csvOperation.WriteCSV(fileDir, self.mappingFields);
            self.progressDialog.updateTextDialog("File will be saved under: %s"%fileDir);
        except csvOperation.FileOpenException, err:
            log.exception((err));
            return False;
    
        return testLabDownload.path
    
    
    
    def mainCMD(self, testSetDir, testSetName):
        '''
        This function will be used as one for CommandLine execution. 
        I had to divide main to Gui and CMD. 
        Reason is to have main() where only TestCase read will be executed not also:
        - creating new CSV file, 
        - new instances, etc.
        :param testCaseDir: str() TestCase directory
        :param testCaseName: str() TestCase Name
        
        '''
        testLabDownload = TestLabDownload(self.qc);
        testLabDownload.makeTargetDir();
        try: 
            path = testLabDownload.path;
            filename = os.path.basename(path) + ".xls";
            fileDir = os.path.join(path,filename); 
            self.writeCsv = csvOperation.WriteCSV(fileDir, self.mappingFields);
            self.progressDialog.updateTextDialog("File will be saved under: %s"%fileDir);
        except csvOperation.FileOpenException, err:
            log.exception((err));
            return False;
        
        #==========================================================================
        # Part responsible for TestSets
        #==========================================================================
        allTestSets = AllTestSets(testLabDownload.tm, testLabDownload.root)
        testSetsInstList = allTestSets.mainGetTestSet(testSetDir, testSetName);
        if testSetsInstList is None:
            log.info("No Test Sets were found");
            return testLabDownload.path; 
        for testSetInstance in testSetsInstList:
            self.main(testSetInstance);
        
        return testLabDownload.path
        
    
    def main(self, testSetInstance):
        '''
        This function will only read TestCase values
        @param testSetInstance: inst() of QualityCenter TestSet 
        '''
        
        ##increase in the ProgressBar run TEstSet's by 1. If this is Gui execution. If not than it will do nothing
        self.progressDialog.updateProgressBar() 
        
        #======================================================================
        # Part responsible for TestSet 
        #======================================================================
        groupName = 'TestSet'
        testSet = TestSet(testSetInstance, self.mappingFields[groupName]);
        testSetName = testSet.testSetName
        self.progressDialog.updateTextDialog("TestSet: %s"%repr(testSetName));
        for fieldUserName, fieldValue in testSet.getFieldsValue():
            self.writeCsv.updateCell(groupName, fieldUserName, fieldValue);
        self.writeCsv.addRow();

        #======================================================================
        # Part responsible for TestCases 
        #======================================================================
        allTestCases = AllTestCases(testSetInstance, testSetName);
        testCasesInstList = allTestCases.getAllTestCases();
        if testCasesInstList is None:
            log.info("No Test Cases were found");
            return 
        for testCaseInstance in testCasesInstList:
            ##TestCase operations
            groupName = 'TestCase'
            testCase = TestCase(testCaseInstance, testSetName, self.mappingFields[groupName] );
            testCaseName = testCase.testCaseName;
            self.progressDialog.updateTextDialog("\tTestCase: %s"%repr(testCaseName));
            for fieldUserName, fieldValue in testCase.getFieldsValue():
                self.writeCsv.updateCell(groupName, fieldUserName, fieldValue);
            self.writeCsv.addRow();
           
            #==================================================================
            # Part responsible for Steps
            #==================================================================
            allSteps = AllSteps(testCaseInstance, testSetName, testCaseName);
            allStepsInstance = allSteps.getAllSteps();
            if allStepsInstance is None:
                log.info("No Steps were found");
                continue;
            for stepInstance in allStepsInstance:
                ##Step operations
                groupName = 'Step'
                try: step = Step(stepInstance, testSetName, testCaseName, self.mappingFields[groupName]);
                except: 
                    log.error(("There is problem with reading StepInstance. Skipping this"))
                    continue;
                stepName = step.stepName;
                self.progressDialog.updateTextDialog("\t\tStep: %s"%repr(stepName));
                for fieldUserName, fieldValue in step.getFieldsValue():
                    self.writeCsv.updateCell(groupName, fieldUserName, fieldValue);
                self.writeCsv.addRow();
        return True;


def QC_90():
    import qc_connect_disconnect
        
    import getpass
    
    server = "http://qualitycenter.inside.com/qcbin"
    UserName="wro50026"
#    Password="test"
    Password = getpass.getpass('Password for %s: '%UserName);

    DomainName="SANDBOX"
    ProjectName="Playground1"

    qcConnect = qc_connect_disconnect.QC_connect(server, UserName, Password, DomainName, ProjectName);
    if qcConnect.connect():
        print("Connected to QC");
    else:
        print("Not connected to QC server, exiting");
        #sys.exit(1);
    
    
    
    
    TestLabDownload_CInst = TestLabDownload(qcConnect.qc) 
#    TestLabDownload_CInst.test_main();
#===============================================================================
#    
#    dict_instance_TestSets = TestLabDownload_CInst.give_TestSets()
#    #print dict_instance_TestSets;
# 
#    ##Get One TestSet
#    TS = dict_instance_TestSets.get('testSet1')
#    print "TS: ", TS
#    
#    ##Get TestCases from TestSet
#    TCs = TestLabDownload_CInst.give_TestCases(TS[1]);
#    print "TCs:", TCs;
# 
#    
#    ##Get One TestCase from TestCases
#    TC = TCs.get('[1]Test1');
#    print "TC:", TC;
#    
#    ##Get Steps from last run from TestCase
#    Steps = TestLabDownload_CInst.give_Steps(TC);
#    print "Steps:", Steps;
#===============================================================================
    
    TSdir = r'Root\Lukas';
    TSname = 'testSet1';
    dict_instance_TestSets, TestSets_list = TestLabDownload_CInst.give_TestSets(TSdir, TSname)
    print((dict_instance_TestSets, TestSets_list));
    updateExcel(dict_instance_TestSets, TestSets_list, qcConnect);
    
    
    if qcConnect.disconnect_QC():
        print "\nDisconnecting from QC server."\
                "\n"\
                "\nThank You for using QC_import_export script.\n"\
                "\t\t\t\n"\
                "\t\t\t  Lukasz Stefaniszyn\n"\
                "\n\t\t\t\t\tPowered by Python 2.5";


def QC_92():
    import qc_connect_disconnect
    
    
    
    import getpass
    import crypt_pass 
    
    
    server = "http://muvmp034.nsn-intra.net/qcbin/" 
    UserName="stefaniszyn"
    pass_ = "e8dbf113946b941a4a34b573363674a3c58520"
    Password = crypt_pass.Heslo(pass_)._Heslo__readHeslo();
    #Password = getpass.getpass('Password for %s: '%UserName);
    DomainName="HIT7300"
    ProjectName="hiT73_R43x"
   
   
#    import read_arguments
#    usage_text = "usage: %prog <options> [TestSets directory] [TestSet name]\n"\
#                    "\nTestSets directory - this is full path to the TestSets, empty means all directories under Root\\.\n"\
#                    "\tExample: Root\\4.25.10_hit7300_TestLab\\D_Control_Management\\D07 Control - OpticalLinkControl\\D07.01 Basic SNMP Commands\n"\
#                    "TestSet name - this is TestSet name from TestSet directory\n"\
#                    "\tExample: D07.01.01 ONNT\n\n";
#    server, UserName, Password, DomainName, ProjectName, args = read_arguments.arguments(usage_text);
#    
    print "\n";
    print "#"*60;
    print "server:", server;
    print "UserName:", UserName;
    print "DomainName:", DomainName;
    print "ProjectName", ProjectName;
    
#    TSdir = None;
#    TSname = None;
#    if len(args)>0:
#        TSdir = args[0];
#        try:
#            TSname = args[1];
#        except IndexError:
#            print "No TestSet name in input arguments";
    
    TSdir = r'Root\4.25.10_hit7300_TestLab\D_Control_Management\D07 Control - OpticalLinkControl\D07.01 Basic SNMP Commands';
    TSname = r'D07.01.01 ONNT';
    
    TSdir = r'Root\4.20.10_hiT7300 Maintenance TestLab\D_ControlManagement\D05 Protection Group Management\D05.01 O03CP\I08T10G'
    TSname = r'D05.01.02.01 - GBE'
    
    TSdir = r'Root\4.30.00_hit7300_TestLab\G TL1-Wroclaw\Test\TestDir1'
    TSname = r'TestSet1'
    
        
    print "TestSets directory:",TSdir
    print "TestSet name:", TSname;
    print "#"*60;
    print "\n";
    
    
    qcConnect = qc_connect_disconnect.QC_connect(server, UserName, Password, DomainName, ProjectName);
    if qcConnect.connect():
        print("Connected to QC");
    else:
        print("Not connected to QC server, exiting");
        #sys.exit(1);
    
    
    
    TestLabDownload_CInst = TestLabDownload(qcConnect.qc) 
#    TestLabDownload_CInst.test_main();
#    




    
    dict_instance_TestSets, TestSets_list = TestLabDownload_CInst.give_TestSets(TSdir, TSname)
    #print "dict_instance_TestSets:",dict_instance_TestSets
    
    #return dict_instance_TestSets, qcConnect
    
    updateExcel(dict_instance_TestSets, TestSets_list, qcConnect);
    
 
    if qcConnect.disconnect_QC():
        print "\nDisconnecting from QC server."\
                "\n"\
                "\nThank You for using QC_import_export script.\n"\
                "\t\t\t\n"\
                "\t\t\t  Lukasz Stefaniszyn\n"\
                "\n\t\t\t\t\tPowered by Python 2.5";
    
    

 


def printFields(TS, TC, Steps, qc):
    
    
    ts = TS[1]
    print("\tts.TestSetFolder.TestSetFactory.Fields");
    for field in ts.TestSetFolder.TestSetFactory.Fields:
        print field;
    
#===============================================================================
# CY_ATTACHMENT
# CY_CLOSE_DATE
# CY_DESCRIPTION
# CY_OS_CONFIG
# CY_USER_01
# CY_CYCLE_ID
# CY_COMMENT
# CY_REQUEST_ID
# CY_MAIL_SETTINGS
# CY_VTS
# CY_OPEN_DATE
# CY_STATUS
# CY_ASSIGN_RCYC
# CY_TASK_STATUS
# CY_CYCLE
# CY_EXEC_EVENT_HANDLE
# CY_FOLDER_ID
# CY_CYCLE_VER_STAMP
# CY_USER_02
# CY_USER_03
# CY_USER_04
# CY_USER_05
# CY_USER_06
# CY_CYCLE_CONFIG    
#===============================================================================
    
    
    
    tc = TC;
    print("\ttc.TestSet.TSTestFactory.Fields");
    for field in tc.TestSet.TSTestFactory.Fields:
        print field;
    
#===============================================================================
# TC_USER_03
# TC_ATTACHMENT
# TS_DEV_COMMENTS
# TC_OS_CONFIG
# TS_CREATION_DATE
# TS_DESCRIPTION
# TS_RESPONSIBLE
# TC_EPARAMS
# TS_ESTIMATE_DEVTIME
# TC_EXEC_DATE
# TS_EXEC_STATUS
# TC_ITERATIONS
# TS_VTS
# TC_VTS
# TS_PATH
# TC_PLAN_SCHEDULING_DATE
# TC_PLAN_SCHEDULING_TIME
# TC_HOST_NAME
# TS_USER_03
# TS_USER_01
# TC_USER_02
# TC_USER_01
# TC_TESTER_NAME
# TC_STATUS
# TS_STATUS
# TS_STEP_PARAM
# TS_STEPS
# TS_SUBJECT
# TC_ASSIGN_RCYC
# TS_TASK_STATUS
# TC_TASK_STATUS
# TS_TEMPLATE
# TC_TEST_ID
# TS_VC_CUR_VER
# TC_EXEC_EVENT_HANDLE
# TC_TEST_INSTANCE
# TC_TESTCYCL_ID
# TS_NAME
# TC_TEST_ORDER
# TS_RUNTIME_DATA
# TC_CYCLE
# TC_TEST_VERSION
# TS_USER_02
# TC_ACTUAL_TESTER
# TC_CYCLE_ID
# TC_EXEC_TIME
# TS_TYPE
# TS_TIMEOUT
# TC_TESTCYCLE_VER_STAMP
# TC_DATA_OBJ
# TS_TEXT_SYNC
# TS_USER_04
# TS_USER_10
# TS_USER_05
# TS_USER_11
# TS_USER_06
# TS_USER_12
# TS_USER_07
# TS_USER_13
# TS_USER_08
# TS_USER_09
# TS_USER_14
# TS_USER_20
# TS_USER_15
# TS_USER_16
# TS_USER_21
# TS_USER_17
# TS_USER_22
# TS_USER_18
# TS_USER_23
# TS_USER_24
# TS_USER_19
# TC_USER_04
# TC_USER_10
# TC_USER_05
# TC_USER_11
# TC_USER_06
# TC_USER_12
# TC_USER_07
# TC_USER_13
# TC_USER_08
# TC_USER_14
# TC_USER_09
# TC_USER_20
# TC_USER_15
# TC_USER_21
# TC_USER_16
# TC_USER_17
# TC_USER_22
# TC_USER_18
# TC_USER_23
# TC_USER_24
# TC_USER_19
# TS_USER_HR_01
# TS_USER_HR_02
# TS_USER_HR_03
# TS_USER_HR_04
# TS_USER_HR_05
# TS_USER_HR_06
#===============================================================================
    
    
    StepFactory = tc.LastRun.StepFactory;
    for field in StepFactory.Fields:
        print field;
        
#===============================================================================
# ST_ACTUAL
# ST_ATTACHMENT
# ST_COMPONENT_DATA
# ST_DESCRIPTION
# ST_DESSTEP_ID
# ST_EXECUTION_DATE
# ST_EXECUTION_TIME
# ST_EXPECTED
# ST_EXTENDED_REFERENCE
# ST_DESIGN_ID
# ST_LEVEL
# ST_OBJ_ID
# ST_LINE_NO
# ST_PATH
# ST_RUN_ID
# ST_TEST_ID
# ST_STATUS
# ST_ID
# ST_STEP_NAME
# ST_STEP_ID
# ST_STEP_ORDER
# ST_PARENT_ID
# ST_USER_01
# ST_USER_02
# ST_USER_03
# ST_USER_04
# ST_USER_05
# ST_USER_06      
#===============================================================================

#===============================================================================
#        NAME column: ST_ACTUAL
# None
#        NAME column: ST_ATTACHMENT
# None
#        NAME column: ST_COMPONENT_DATA
# None
#        NAME column: ST_DESCRIPTION
# <html><body>Establish TL1 connection to NE</body></html>
#        NAME column: ST_DESSTEP_ID
# 35053
#        NAME column: ST_EXECUTION_DATE
# 05/11/09 00:00:00
#        NAME column: ST_EXECUTION_TIME
# 12:19:34
#        NAME column: ST_EXPECTED
# <html><body>ok</body></html>
#        NAME column: ST_EXTENDED_REFERENCE
# None
#        NAME column: ST_DESIGN_ID
# None
#        NAME column: ST_LEVEL
# None
#        NAME column: ST_OBJ_ID
# None
#        NAME column: ST_LINE_NO
# None
#        NAME column: ST_PATH
# None
#        NAME column: ST_RUN_ID
# 8681
#        NAME column: ST_TEST_ID
# 7523
#        NAME column: ST_STATUS
# Passed
#        NAME column: ST_ID
# 40400
#        NAME column: ST_STEP_NAME
# Step 1
#        NAME column: ST_STEP_ID
# None
#        NAME column: ST_STEP_ORDER
# 1
#        NAME column: ST_PARENT_ID
# None
#        NAME column: ST_USER_01
# None
#        NAME column: ST_USER_02
# None
#        NAME column: ST_USER_03
# None
#        NAME column: ST_USER_04
# None
#        NAME column: ST_USER_05
# None
#        NAME column: ST_USER_06
# None
#===============================================================================



    
    
    
    

    
    print "\tqc.TestFactory.Fields";
    for field in qc.TestFactory.Fields:
        print field;

#===============================================================================
# TS_ATTACHMENT
# TS_DEV_COMMENTS
# TS_CREATION_DATE
# TS_DESCRIPTION
# TS_RESPONSIBLE
# TS_ESTIMATE_DEVTIME
# TS_EXEC_STATUS
# TS_VTS
# TS_PATH
# TS_USER_03
# TS_USER_01
# TS_STATUS
# TS_STEP_PARAM
# TS_STEPS
# TS_SUBJECT
# TS_TASK_STATUS
# TS_TEMPLATE
# TS_VC_CUR_VER
# TS_TEST_ID
# TS_NAME
# TS_RUNTIME_DATA
# TS_USER_02
# TS_TYPE
# TS_TIMEOUT
# TS_TEST_VER_STAMP
# TS_TEXT_SYNC
# TS_USER_04
# TS_USER_10
# TS_USER_05
# TS_USER_11
# TS_USER_06
# TS_USER_12
# TS_USER_07
# TS_USER_13
# TS_USER_08
# TS_USER_09
# TS_USER_14
# TS_USER_20
# TS_USER_15
# TS_USER_16
# TS_USER_21
# TS_USER_17
# TS_USER_22
# TS_USER_18
# TS_USER_23
# TS_USER_24
# TS_USER_19
# TS_USER_HR_01
# TS_USER_HR_02
# TS_USER_HR_03
# TS_USER_HR_04
# TS_USER_HR_05
# TS_USER_HR_06
#===============================================================================
        
#===============================================================================
#        NAME column: TC_ACTUAL_TESTER
# jarczak
#        NAME column: TC_ASSIGN_RCYC
# <COMObject Field>
#        NAME column: TC_ATTACHMENT
# None
#        NAME column: TC_CYCLE
# None
#        NAME column: TC_CYCLE_ID
# 1062
#        NAME column: TC_DATA_OBJ
# None
#        NAME column: TC_EPARAMS
# None
#        NAME column: TC_EXEC_DATE
# 05/11/09 00:00:00
#        NAME column: TC_EXEC_EVENT_HANDLE
# None
#        NAME column: TC_EXEC_TIME
# 12:19:47
#        NAME column: TC_HOST_NAME
# None
#        NAME column: TC_ITERATIONS
# None
#        NAME column: TC_OS_CONFIG
# None
#        NAME column: TC_PLAN_SCHEDULING_DATE
# None
#        NAME column: TC_PLAN_SCHEDULING_TIME
# None
#        NAME column: TC_STATUS
# Passed
#        NAME column: TC_TASK_STATUS
# None
#        NAME column: TC_TESTCYCLE_VER_STAMP
# 4
#        NAME column: TC_TESTCYCL_ID
# 10134
#        NAME column: TC_TESTER_NAME
# jarczak
#        NAME column: TC_TEST_ID
# 7523
#        NAME column: TC_TEST_INSTANCE
# 1
#        NAME column: TC_TEST_ORDER
# 1
#        NAME column: TC_TEST_VERSION
# None
#        NAME column: TC_USER_01
# None
#        NAME column: TC_USER_02
# None
#        NAME column: TC_USER_03
# None
#        NAME column: TC_USER_04
# None
#        NAME column: TC_USER_05
# None
#        NAME column: TC_USER_06
# None
#        NAME column: TC_USER_07
# None
#        NAME column: TC_USER_08
# None
#        NAME column: TC_USER_09
# None
#        NAME column: TC_USER_10
# None
#        NAME column: TC_USER_11
# None
#        NAME column: TC_USER_12
# None
#        NAME column: TC_USER_13
# None
#        NAME column: TC_USER_14
# None
#        NAME column: TC_USER_15
# None
#        NAME column: TC_USER_16
# None
#        NAME column: TC_USER_17
# None
#        NAME column: TC_USER_18
# None
#        NAME column: TC_USER_19
# None
#        NAME column: TC_USER_20
# None
#        NAME column: TC_USER_21
# None
#        NAME column: TC_USER_22
# None
#        NAME column: TC_USER_23
# None
#        NAME column: TC_USER_24
# None
#        NAME column: TC_VTS
# 05/11/09 12:19:47
#        NAME column: TS_CREATION_DATE
# 07/03/08 00:00:00
#        NAME column: TS_DESCRIPTION
# <html><body>The EDIT-EQUIPMENT (ED-EQPT) command allows the attribute values associated with a card or shelf in an NE to be modified. Slots definitions cannot be modified in the hiT 7300.<br><br>=&gt; attributes associated with LALIC-1 card</body></html>
#        NAME column: TS_DEV_COMMENTS
# None
#        NAME column: TS_ESTIMATE_DEVTIME
# None
#        NAME column: TS_EXEC_STATUS
# Passed
#        NAME column: TS_NAME
# [1]G0201.02.004 ED-EQPT_LALIC-1
#        NAME column: TS_PATH
# None
#        NAME column: TS_RESPONSIBLE
# michalowski
#        NAME column: TS_RUNTIME_DATA
# None
#        NAME column: TS_STATUS
# Reviewed
#        NAME column: TS_STEPS
# 4
#        NAME column: TS_STEP_PARAM
# 0
#        NAME column: TS_SUBJECT
# Wroclaw
#        NAME column: TS_TASK_STATUS
# None
#        NAME column: TS_TEMPLATE
# None
#        NAME column: TS_TEXT_SYNC
# Y
#        NAME column: TS_TIMEOUT
# None
#        NAME column: TS_TYPE
# MANUAL
#        NAME column: TS_USER_01
# N
#        NAME column: TS_USER_02
# R4.25
#        NAME column: TS_USER_03
# Prio2
#        NAME column: TS_USER_04
# None
#        NAME column: TS_USER_05
# None
#        NAME column: TS_USER_06
# None
#        NAME column: TS_USER_07
# None
#        NAME column: TS_USER_08
# None
#        NAME column: TS_USER_09
# None
#        NAME column: TS_USER_10
# None
#        NAME column: TS_USER_11
# None
#        NAME column: TS_USER_12
# None
#        NAME column: TS_USER_13
# None
#        NAME column: TS_USER_14
# None
#        NAME column: TS_USER_15
# None
#        NAME column: TS_USER_16
# None
#        NAME column: TS_USER_17
# None
#        NAME column: TS_USER_18
# None
#        NAME column: TS_USER_19
# None
#        NAME column: TS_USER_20
# None
#        NAME column: TS_USER_21
# None
#        NAME column: TS_USER_22
# None
#        NAME column: TS_USER_23
# None
#        NAME column: TS_USER_24
# None
#        NAME column: TS_USER_HR_01
# None
#        NAME column: TS_USER_HR_02
# None
#        NAME column: TS_USER_HR_03
# None
#        NAME column: TS_USER_HR_04
# None
#        NAME column: TS_USER_HR_05
# None
#        NAME column: TS_USER_HR_06
# None
#        NAME column: TS_VC_CUR_VER
# None
#        NAME column: TS_VTS
# 05/11/09 12:19:47        
#===============================================================================
        
        
        
    
    print ("\tqc.RunFactory.Fields");
    for field in qc.RunFactory.Fields:
        print field;
        
#===============================================================================
#    
# RN_ATTACHMENT
# RN_OS_CONFIG
# RN_CYCLE_ID
# RN_DURATION
# RN_EXECUTION_DATE
# RN_EXECUTION_TIME
# RN_HOST
# RN_OS_NAME
# RN_OS_BUILD
# RN_OS_SP
# RN_PATH
# RN_RUN_NAME
# RN_VC_STATUS
# RN_VC_LOKEDBY
# RN_VC_VERSION
# RN_RUN_ID
# RN_STATUS
# RN_ASSIGN_RCYC
# RN_TEST_ID
# RN_TESTCYCL_ID
# RN_TEST_INSTANCE
# RN_CYCLE
# RN_TEST_VERSION
# RN_TESTER_NAME
# RN_VTS
# RN_RUN_VER_STAMP
# RN_USER_01
# RN_USER_02
# RN_USER_03
# RN_USER_04
# RN_USER_05
# RN_USER_10
# RN_USER_11
# RN_USER_06
# RN_USER_07
# RN_USER_12
# RN_USER_08
# RN_USER_09    
#===============================================================================
    
    
    print("\tqc.HostFactory.Fields");
    for field in qc.HostFactory.Fields:
        print field;
 
#===============================================================================
# HO_ATTACHMENT
# HO_DESCRIPTION
# HO_NAME
# HO_REX_SERVER 
#===============================================================================
        
        
    print("\tqc.HostGroupFactory.Fields");
    for field in qc.HostGroupFactory.Fields:
        print field;
        
#===============================================================================
# GH_ATTACHMENT
# GH_DESCRIPTION
# GH_NAME
#===============================================================================
 
#    print("\tqc.Customization.Fields");
#    for field in qc.Customization.Fields:
#        print field;
        
        
        
if __name__ == "__main__":
    
    
    print "START"
    QC_92();
    #dict_instance_TestSets, TS, TCs, TC, Steps, qc = QC_92();
    
#    QC_90();
    
    
    
    
    
    
    
    

    