echo Delete old catalog QualityCenter_TestLab_download_upload
rmdir /S /Q QualityCenter_download_upload
rmdir /S /Q RTT_deploy

echo Create catalog
mkdir QualityCenter_download_upload\TestLab_download_upload
mkdir QualityCenter_download_upload\TestPlan_download_upload

echo Copy file: parameter.txt, Download.xls
copy .\Files_to_zip_packet\* .\QualityCenter_download_upload


echo "MAKE QC TestLab download"
python setup_TL_download.py py2exe
xcopy /D /E /Y .\dist .\QualityCenter_download_upload\TestLab_download_upload\TestLab_download\
rmdir /S /Q .\dist
rmdir /S /Q .\build

echo MAKE QC TestLab upload
python setup_TL_upload.py py2exe
xcopy /D /E /Y .\dist .\QualityCenter_download_upload\TestLab_download_upload\TestLab_upload\
rmdir /S /Q .\dist
rmdir /S /Q .\build

echo "MAKE QC TestPlan download"
python setup_TP_upload.py py2exe
xcopy /D /E /Y .\dist .\QualityCenter_download_upload\TestPlan_download_upload\TestPlan_upload\
rmdir /S /Q .\dist
rmdir /S /Q .\build
    

echo "Compress files"
zip.exe -r QualityCenter_download_upload QualityCenter_download_upload

echo "Create scripts ready to deploy to RTT"
mkdir RTT_deploy

cd QualityCenter_download_upload\TestPlan_download_upload\TestPlan_upload
..\..\..\zip.exe ..\..\..\RTT_deploy\TestPlan_upload.jar *
cd ..\..\..

cd QualityCenter_download_upload\TestLab_download_upload\TestLab_upload
..\..\..\zip.exe ..\..\..\RTT_deploy\TestLab_upload.jar *
cd ..\..\..
    
