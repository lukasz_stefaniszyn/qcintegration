from distutils.core import setup
import py2exe




#setup(console  = [{"script": "run_export.py"}])

setup(
    name = 'Quality Center automation',
    description = 'Quality Center automation for download/upload and/or upload test results',
    version = '1.0',

    console = [
                  {
                      'script': 'TestLab_main_upload.py',
                      'icon_resources': [(1, "cygwin.ico")],
                  }
              ]
)



