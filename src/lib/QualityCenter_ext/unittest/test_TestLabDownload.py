'''
Created on 10-01-2012

@author: qc
'''
import unittest
from PyQt4 import QtCore, QtGui
from time import sleep
import sys
import os;
from functools import partial

from win32com.client import gencache, DispatchWithEvents, constants
gencache.EnsureModule('{D66ADC20-B070-11D3-8604-0050046B8F4A}', 0, 1, 0);

import lib.QualityCenter_ext.lib.TestLab_download as TestLab_download
import lib.QualityCenter_ext.lib.qc_connect_disconnect as qc_connect_disconnect
import lib.QualityCenter_ext.lib.csvOperation as csvOperation 


server = "http://qc:8080/qcbin/";
userName="alex_qc";
domainName="DEFAULT";
projectName="QualityCenter_Demo";
password = ""; ##Put the right password



mappingFields_TL ={
                   'Step': {
                            'Attachment': {
                                           'FieldIsRequired': 'False',
                                           'FieldOrder': '2',
                                           'FieldQcName': 'ATTACHMENT',
                                           'FieldType': 'char'},
                            'Status': {
                                       'FieldIsRequired': 'False',
                                       'FieldOrder': '0',
                                       'FieldQcName': u'ST_STATUS',
                                       'FieldType': u'char'},
                            'Step Name': {
                                          'FieldIsRequired': 'False',
                                          'FieldOrder': '1',
                                          'FieldQcName': u'ST_STEP_NAME',
                                          'FieldType': u'char'}},
                   'TestCase': {
                                'Attachment': {
                                               'FieldIsRequired': 'False',
                                               'FieldOrder': '5',
                                               'FieldQcName': 'ATTACHMENT',
                                               'FieldType': 'char'},
                                'Plan: Execution Status': {
                                                           'FieldIsRequired': 'False',
                                                           'FieldOrder': '2',
                                                           'FieldQcName': u'TS_EXEC_STATUS',
                                                           'FieldType': u'char'},
                                'Plan: Path': {
                                               'FieldIsRequired': 'False',
                                               'FieldOrder': '4',
                                               'FieldQcName': u'TS_PATH',
                                               'FieldType': u'char'},
                                'Plan: Subject': {
                                                  'FieldIsRequired': 'False',
                                                  'FieldOrder': '3',
                                                  'FieldQcName': u'TS_SUBJECT',
                                                  'FieldType': u'number'},
                                'Responsible Tester': {
                                                       'FieldIsRequired': 'False',
                                                       'FieldOrder': '1',
                                                       'FieldQcName': u'TC_TESTER_NAME',
                                                       'FieldType': u'char'},
                                'Status': {
                                           'FieldIsRequired': 'False',
                                           'FieldOrder': '0',
                                           'FieldQcName': u'TC_STATUS',
                                           'FieldType': u'char'}},
                   'TestSet': {
                               'Description': {
                                               'FieldIsRequired': 'False',
                                               'FieldOrder': '3',
                                               'FieldQcName': u'CY_COMMENT',
                                               'FieldType': u'char'},
                               'Status': {
                                          'FieldIsRequired': 'False',
                                          'FieldOrder': '0',
                                          'FieldQcName': u'CY_STATUS',
                                          'FieldType': u'char'},
                               'Test Set': {
                                            'FieldIsRequired': 'False',
                                            'FieldOrder': '2',
                                            'FieldQcName': u'CY_CYCLE',
                                            'FieldType': u'char'},
                               'Test Set Folder': {
                                                   'FieldIsRequired': 'False',
                                                   'FieldOrder': '1',
                                                   'FieldQcName': u'CY_FOLDER_ID',
                                                   'FieldType': u'number'}}}; 




mappingFields_TP = {
                    'Step': {
                             'Attachment': {
                                            'FieldIsRequired': 'False',
                                            'FieldOrder': '3',
                                            'FieldQcName': 'ATTACHMENT',
                                            'FieldType': 'char'
                                            },
                             'Description': {
                                             'FieldIsRequired': 'False',
                                             'FieldOrder': '1',
                                             'FieldQcName': u'DS_DESCRIPTION',
                                             'FieldType': u'char'
                                             },
                             'Expected Result': {
                                                 'FieldIsRequired': 'False',
                                                 'FieldOrder': '2',
                                                 'FieldQcName': u'DS_EXPECTED',
                                                 'FieldType': u'char'
                                                 },
                            'Step Name': {
                                          'FieldIsRequired': 'False',
                                          'FieldOrder': '0',
                                          'FieldQcName': u'DS_STEP_NAME',
                                          'FieldType': u'char'
                                          }
                             },
                    'TestCase': {
                                 'Path': {
                                          'FieldIsRequired': 'False',
                                          'FieldOrder': '0',
                                          'FieldQcName': u'TS_PATH',
                                          'FieldType': u'char'},
                                'Test Name': {
                                              'FieldIsRequired': 'True',
                                              'FieldOrder': '1',
                                              'FieldQcName': u'TS_NAME',
                                              'FieldType': u'char'
                                              },
                                 'Status': {
                                              'FieldIsRequired': 'True',
                                              'FieldOrder': '4',
                                              'FieldQcName': u'TS_STATUS',
                                              'FieldType': u'char'
                                              },
                                 'Description': {
                                              'FieldIsRequired': 'False',
                                              'FieldOrder': '3',
                                              'FieldQcName': u'TS_DESCRIPTION',
                                              'FieldType': u'char'
                                              },
                                'Subject': {
                                             'FieldIsRequired': 'False',
                                             'FieldOrder': '2',
                                             'FieldQcName': u'TS_SUBJECT',
                                             'FieldType': u'number'
                                             },
                                 'Attachment': {
                                               'FieldIsRequired': 'False',
                                               'FieldOrder': '2',
                                               'FieldQcName': 'ATTACHMENT',
                                               'FieldType': 'char'
                                               },
                                 },
                    'TestSet': {
                                'Attachment': {
                                               'FieldIsRequired': 'False',
                                               'FieldOrder': '3',
                                               'FieldQcName': 'ATTACHMENT',
                                               'FieldType': 'char'
                                               },
                                'Status': {
                                           'FieldIsRequired': 'False',
                                           'FieldOrder': '2',
                                           'FieldQcName': u'CY_STATUS',
                                           'FieldType': u'char'
                                           },
                                'Test Set': {
                                             'FieldIsRequired': 'False',
                                             'FieldOrder': '0',
                                             'FieldQcName': u'CY_CYCLE',
                                             'FieldType': u'char'
                                             },
                                'Test Set Folder': {
                                                    'FieldIsRequired': 'False',
                                                    'FieldOrder': '1',
                                                    'FieldQcName': u'CY_FOLDER_ID',
                                                    'FieldType': u'number'
                                                    }
                                }
                    }; 




class TestTestLabDownload_AllTestSet(unittest.TestCase):


    def setUp(self):
        self.qcConnect = qc_connect_disconnect.QC_connect(server, userName, password, domainName,projectName);
        for result in self.qcConnect.connect():
            print result;
        self.qc = self.qcConnect.qc;
        
        self.testLabDownload = TestLab_download.TestLabDownload(self.qc);


    def tearDown(self):
        self.qcConnect.disconnect_QC();


    def testGetTestSet_1(self):
        '''
        Test getTestSet(), correct TestSet directory and TestSet name
        '''
        TSdir = r'Root\Mercury Tours (HTML Edition)\Version 1.0\Functionality And UI'
        TSname = r'Mercury Tours UI'
        
        allTestSets = TestLab_download.AllTestSets(self.testLabDownload.tm, self.testLabDownload.root );
        testSetsInstList = allTestSets.getTestSet(TSdir, TSname);
        self.assertEqual(TSname, testSetsInstList[0].Field("CY_CYCLE"));
    
    def testGetTestSet_2(self):
        '''
        Test getTestSet(), wrong TestSet name. Return will be None 
        '''
        
        TSdir = r'Root\Mercury Tours (HTML Edition)\Version 1.0\Functionality And UI'
        TSname = r'WrongName'
        
        allTestSets = TestLab_download.AllTestSets(self.testLabDownload.tm, self.testLabDownload.root );
        testSetsInstList = allTestSets.getTestSet(TSdir, TSname);
        self.assertTrue(testSetsInstList is None);
    
    def testGetTestSet_3(self):
        '''
        Test getTestSet(), wrong TestSet directory. Return will be None 
        '''
        
        TSdir = r'Root\WrongDir'
        TSname = r'Mercury Tours UI'
        
        allTestSets = TestLab_download.AllTestSets(self.testLabDownload.tm, self.testLabDownload.root );
        testSetsInstList = allTestSets.getTestSet(TSdir, TSname);
        self.assertTrue(testSetsInstList is None);
    
    def testGetAllTestSet_1(self):
        '''
        Test getAllTestSet(), correct TestSet directory. Return list of TestSets
        '''
        TSdir = r'Root\Mercury Tours (HTML Edition)\Version 1.0\Functionality And UI'
        
        allTestSets = TestLab_download.AllTestSets(self.testLabDownload.tm, self.testLabDownload.root );
        listAllTestSets = allTestSets.getAllTestSets(TSdir);
        listAllTestSets= [testSet.Field("CY_CYCLE") for testSet in listAllTestSets];
        goldenListAllTestSets = [u'Mercury Tours Functionality', u'Mercury Tours Sanity', u'Mercury Tours UI'];
        self.assertEqual(listAllTestSets, goldenListAllTestSets)
        
    def testGetAllTestSet_2(self):
        '''
        Test getAllTestSet(), wrong TestSet directory. Return None
        '''
        TSdir = r'WrongDir'
        
        allTestSets = TestLab_download.AllTestSets(self.testLabDownload.tm, self.testLabDownload.root );
        listAllTestSets = allTestSets.getAllTestSets(TSdir);
        self.assertTrue(listAllTestSets is None)
    
    def testGetAllTestSet_3(self):
        '''
        Test getAllTestSet(), TestSet directory is not empty. Return list of all TestSets from the QC
        '''
        
        
        allTestSets = TestLab_download.AllTestSets(self.testLabDownload.tm, self.testLabDownload.root );
        listAllTestSets = allTestSets.getAllTestSets();
        listAllTestSets= [testSet.Field("CY_CYCLE") for testSet in listAllTestSets];
        goldenListAllTestSets = [u'Mercury Tours Functionality', u'Mercury Tours Sanity', u'Mercury Tours UI'];
        
        fs = frozenset(goldenListAllTestSets);
        self.assertTrue( fs.issubset(listAllTestSets),
                     "Not found TestSets in all set of TestCases"); #Check if compateTestCaseNames is testCasesInstNames
        
    def testmainGetTestSet_1(self):
        TSdir = r'Root\Mercury Tours (HTML Edition)\Version 1.0\Functionality And UI'
        TSname = r'Mercury Tours UI'
        
        allTestSets = TestLab_download.AllTestSets(self.testLabDownload.tm, self.testLabDownload.root );
        listTestSets = allTestSets.mainGetTestSet(TSdir, TSname);
        self.assertEqual(TSname, listTestSets[0].Field("CY_CYCLE"));
    def testmainGetTestSet_2(self):
        TSdir = r'Root\Mercury Tours (HTML Edition)\Version 1.0\Functionality And UI'
        
        allTestSets = TestLab_download.AllTestSets(self.testLabDownload.tm, self.testLabDownload.root );
        listTestSets = allTestSets.mainGetTestSet(TSdir);
        listAllTestSets= [testSet.Field("CY_CYCLE") for testSet in listTestSets];
        goldenListAllTestSets = [u'Mercury Tours Functionality', u'Mercury Tours Sanity', u'Mercury Tours UI'];
        self.assertEqual(listAllTestSets, goldenListAllTestSets);
        
    def testmainGetTestSet_3(self):
#        TSdir = r'Root\Mercury Tours (HTML Edition)\Version 1.0\Functionality And UI'
        
        allTestSets = TestLab_download.AllTestSets(self.testLabDownload.tm, self.testLabDownload.root );
        listTestSets = allTestSets.mainGetTestSet();
        listAllTestSets= [testSet.Field("CY_CYCLE") for testSet in listTestSets];
        goldenListAllTestSets = [u'Mercury Tours Functionality', u'Mercury Tours Sanity', u'Mercury Tours UI'];
        
        fs = frozenset(goldenListAllTestSets);
        self.assertTrue( fs.issubset(listAllTestSets),
                     "Not found TestSets in all set of TestCases"); #Check if compateTestCaseNames is testCasesInstNames
        
class TestTestLabDownload_TestSet(unittest.TestCase):


    def setUp(self):
        self.qcConnect = qc_connect_disconnect.QC_connect(server, userName, password, domainName,projectName);
        for result in self.qcConnect.connect():
            print result;
        self.qc = self.qcConnect.qc;
        
        self.testLabDownload = TestLab_download.TestLabDownload(self.qc);
        
        TSdir = r'Root\Mercury Tours (HTML Edition)\Version 1.0\Functionality And UI'
        self.TSname = r'Mercury Tours UI'
        
        
        allTestSets = TestLab_download.AllTestSets(self.testLabDownload.tm, self.testLabDownload.root );
        testSetsInstList = allTestSets.getTestSet(TSdir, self.TSname);
        self.testSetsInst = testSetsInstList[0] 


    def tearDown(self):
        self.qcConnect.disconnect_QC();


    def testGetTestSet_1(self):
        '''
        Test getTestSet(), correct TestSet directory and TestSet name
        '''
        
        testSet = TestLab_download.TestSet(self.testSetsInst, mappingFields_TL["TestSet"]);
        for fieldUserName, fieldValue in testSet.getFieldsValue():
            if fieldUserName == "Test Set":
                self.assertEqual(fieldValue, testSet.testSetName )
#            print "fieldUserName:",fieldUserName;
#            print "fieldValue:",fieldValue;
    
    def testGetTestSetName(self):
        testSet = TestLab_download.TestSet(self.testSetsInst, mappingFields_TL["TestSet"]);
        self.assertEqual(testSet.testSetName, self.TSname);

class TestTestLabDownload_AllTestCases(unittest.TestCase):


    def setUp(self):
        self.qcConnect = qc_connect_disconnect.QC_connect(server, userName, password, domainName,projectName);
        for result in self.qcConnect.connect():
            print result;
        self.qc = self.qcConnect.qc;
        
        self.testLabDownload = TestLab_download.TestLabDownload(self.qc);
        
        TSdir = r'Root\Mercury Tours (HTML Edition)\Version 1.0\Functionality And UI'
        TSname = r'Mercury Tours UI'
        allTestSets = TestLab_download.AllTestSets(self.testLabDownload.tm, self.testLabDownload.root );
        testSetsInstList = allTestSets.getTestSet(TSdir, TSname);
        self.testSetsInst = testSetsInstList[0]
         
        testSet = TestLab_download.TestSet(self.testSetsInst, mappingFields_TL["TestSet"]);
        self.testSetName = testSet.testSetName


    def tearDown(self):
        self.qcConnect.disconnect_QC();


    def testGetAllTestCase_1(self):
        '''
        Test getTestCase(), correct TestCase directory and TestCase name
        '''
        
        allTestCases = TestLab_download.AllTestCases(self.testSetsInst, self.testSetName);
        
        
        testCases = allTestCases.getAllTestCases();
        testCasesName = [tc.Field("TS_NAME") for tc in testCases]
        testCasesNameGolden = [u'[1]Welcome Page', u'[1]Registration Page', u'[1]Sign-On Page', u'[1]Edit Profile Page', u'[1]Find Flight Page', u'[1]Select Flight Page', u'[1]Book Flight Page', u'[1]Flight Confirmation Page', u'[1]Itinerary Page']
        
        fs = frozenset(testCasesNameGolden);
        self.assertEqual(testCasesName, testCasesNameGolden, ("Difference:",fs.difference(testCasesName)));
         
        
class TestTestLabDownload_TestCase(unittest.TestCase):


    def setUp(self):
        self.qcConnect = qc_connect_disconnect.QC_connect(server, userName, password, domainName,projectName);
        for result in self.qcConnect.connect():
            print result;
        self.qc = self.qcConnect.qc;
        
        self.testLabDownload = TestLab_download.TestLabDownload(self.qc);
        
        TSdir = r'Root\Mercury Tours (HTML Edition)\Version 1.0\Functionality And UI'
        TSname = r'Mercury Tours UI'
        
        
        allTestSets = TestLab_download.AllTestSets(self.testLabDownload.tm, self.testLabDownload.root );
        testSetsInstList = allTestSets.getTestSet(TSdir, TSname);
        testSetsInst = testSetsInstList[0] 
        
        testSet = TestLab_download.TestSet(testSetsInst, mappingFields_TL["TestSet"]);
        self.testSetName = testSet.testSetName
        
        allTestCases = TestLab_download.AllTestCases(testSetsInst, self.testSetName);
        testCases = allTestCases.getAllTestCases();
        self.testCasesInstance = testCases[0]; 


    def tearDown(self):
        self.qcConnect.disconnect_QC();


    def testGetTestCase_1(self):
        '''
        Test getTestCase(), correct TestCase name
        '''
        
        testCase = TestLab_download.TestCase(self.testCasesInstance, self.testSetName, mappingFields_TL["TestCase"]);
        for fieldUserName, fieldValue in testCase.getFieldsValue():
            if fieldUserName == "Plan: Test Name":
                self.assertEqual(fieldValue, testCase.testCaseName);
#            print "fieldUserName:",fieldUserName;
#            print "fieldValue:",fieldValue;
    
    def testGetTestCaseName(self):
        testCase = TestLab_download.TestCase(self.testCasesInstance, self.testSetName, mappingFields_TL["TestCase"]);
        self.assertEqual(testCase.testCaseName, u'[1]Welcome Page');

class TestTestLabDownload_AllSteps(unittest.TestCase):


    def setUp(self):
        self.qcConnect = qc_connect_disconnect.QC_connect(server, userName, password, domainName,projectName);
        for result in self.qcConnect.connect():
            print result;
        self.qc = self.qcConnect.qc;
        
        self.testLabDownload = TestLab_download.TestLabDownload(self.qc);
        
        
        #======================================================================
        # Find testCase with Run in history
        #======================================================================
        TSdir = r'Root\Mercury Tours (HTML Edition)\Version 1.0\Functionality And UI'
        TSname = r'Mercury Tours UI'
        
        allTestSets = TestLab_download.AllTestSets(self.testLabDownload.tm, self.testLabDownload.root );
        testSetsInstList = allTestSets.getTestSet(TSdir, TSname);
        testSetsInst = testSetsInstList[0] 
        
        testSet = TestLab_download.TestSet(testSetsInst, mappingFields_TL["TestSet"]);
        self.testSetName = testSet.testSetName
        
        allTestCases = TestLab_download.AllTestCases(testSetsInst, mappingFields_TL);
        testCases = allTestCases.getAllTestCases();
        self.testCasesInstance_Run = testCases[0];
        
        testCase = TestLab_download.TestCase(self.testCasesInstance_Run, 
                                             self.testSetName,
                                             mappingFields_TL["TestSet"]);
        self.testCaseName_Run = testCase.testCaseName
        
        
        #======================================================================
        # Find testCase without Run in history
        #======================================================================
        TSdir = r'Root\Mercury Tours (HTML Edition)\Version 1.0\Functionality And UI'
        TSname = r'Mercury Tours Sanity' 

        allTestSets = TestLab_download.AllTestSets(self.testLabDownload.tm, self.testLabDownload.root );
        testSetsInstList = allTestSets.getTestSet(TSdir, TSname);
        testSetsInst = testSetsInstList[0] 
        
        allTestCases = TestLab_download.AllTestCases(testSetsInst, mappingFields_TL);
        testCases = allTestCases.getAllTestCases();
        self.testCasesInstance_NoRun = testCases[0];

        testCase = TestLab_download.TestCase(self.testCasesInstance_NoRun, 
                                             self.testSetName,
                                             mappingFields_TL["TestSet"]);
        self.testCaseName_NoRun = testCase.testCaseName
                                            

    def tearDown(self):
        self.qcConnect.disconnect_QC();


    def testGetAllSteps_1(self):
        '''
        Test getAllSteps(), with Run 
        '''
        AllSteps = TestLab_download.AllSteps(self.testCasesInstance_Run, 
                                             self.testSetName,
                                             self.testCaseName_Run)
        allSteps = AllSteps.getAllSteps();
        steps = [step.Field("ST_STEP_NAME") for step in allSteps];
        allstepsGolden = [u'Connect to Mercury Tours Site', u'Page Title', u'Page Text', u'Forms'];
        
        fs = frozenset(allstepsGolden);
        self.assertTrue(fs.issubset(steps),
                     "Not found Steps in all set of TestCase")
    
    def testGetAllSteps_2(self):
        '''
        Test getAllSteps(), with Run 
        '''
        AllSteps = TestLab_download.AllSteps(self.testCasesInstance_NoRun,
                                             self.testSetName,
                                             self.testCaseName_NoRun);
        allSteps = AllSteps.getAllSteps();
        steps = [step.Field("ST_STEP_NAME") for step in allSteps];
        allstepsGolden = [u'Connect to Mercury Tours Site', u'Registration', u'Sign-On to Mercury Tours Application', u'Click the Profile button.'];
        
        fs = frozenset(allstepsGolden);
        self.assertTrue(fs.issubset(steps),
                     "Not found Steps in all set of TestCase");
                     

class TestTestLabDownload_Step(unittest.TestCase):


    def setUp(self):
        self.qcConnect = qc_connect_disconnect.QC_connect(server, userName, password, domainName,projectName);
        for result in self.qcConnect.connect():
            print result;
        self.qc = self.qcConnect.qc;
        
        self.testLabDownload = TestLab_download.TestLabDownload(self.qc);
        
        TSdir = r'Root\Mercury Tours (HTML Edition)\Version 1.0\Functionality And UI'
        TSname = r'Mercury Tours UI'
        
        
        allTestSets = TestLab_download.AllTestSets(self.testLabDownload.tm, self.testLabDownload.root );
        testSetsInstList = allTestSets.getTestSet(TSdir, TSname);
        testSetsInst = testSetsInstList[0] 
        
        testSet = TestLab_download.TestSet(testSetsInst, mappingFields_TL["TestSet"]);
        self.testSetName = testSet.testSetName
        
        allTestCases = TestLab_download.AllTestCases(testSetsInst, self.testSetName);
        testCases = allTestCases.getAllTestCases();
        testCasesInstance = testCases[0]; 

        testCase = TestLab_download.TestCase(testCasesInstance, 
                                             self.testSetName,
                                             mappingFields_TL["TestSet"]);
        self.testCaseName = testCase.testCaseName;
        
        AllSteps = TestLab_download.AllSteps(testCasesInstance, self.testSetName, self.testCaseName)
        self.stepInstance = AllSteps.getAllSteps()[0];
        
        
        


    def tearDown(self):
        self.qcConnect.disconnect_QC();


    def testGetStep_1(self):
        '''
        Test getStep(), from correct TestCase
        '''
        
        step = TestLab_download.Step(self.stepInstance, 
                                     self.testSetName, 
                                     self.testCaseName, 
                                     mappingFields_TL["Step"]);
        for fieldUserName, fieldValue in step.getFieldsValue():
            if fieldUserName == "Step Name":
                self.assertEqual(fieldValue, step.stepName);
#            print "fieldUserName:",fieldUserName;
#            print "fieldValue:",fieldValue;

    def testGetStepName(self):
        step = TestLab_download.Step(self.stepInstance, 
                                     self.testSetName, 
                                     self.testCaseName, 
                                     mappingFields_TL["Step"]);
                    
        self.assertEqual(step.stepName, "Connect to Mercury Tours Site");


class TestTestLabDownload_main(unittest.TestCase):


    def setUp(self):
        self.qcConnect = qc_connect_disconnect.QC_connect(server, userName, password, domainName,projectName);
        for result in self.qcConnect.connect():
            print result;
        self.qc = self.qcConnect.qc;
        
    def tearDown(self):
        self.qcConnect.disconnect_QC();


    def testMain_1(self):
        '''
        Test main() method. During this main() execution folder and the CSV result file should be created with download result 
        '''
        testSetDir = r'Root\Mercury Tours (HTML Edition)\Version 1.0\Functionality And UI'
        testSetName = r'Mercury Tours UI'
        
        mainExecution= TestLab_download.MainExecution(cmd=True, 
                                                       qc=self.qc, 
                                                       testSetDir=testSetDir, 
                                                       testSetName=testSetName, 
                                                       mappingFields=mappingFields_TL, 
                                                       );
        path = mainExecution.path
        
        filename = os.path.basename(path) + ".xls";
        fileDir = os.path.join(path,filename);
        print""
        
        lineGolden = (
                      ['Open', 'Root\\Mercury Tours (HTML Edition)\\Version 1.0\\Functionality And UI', 'Mercury Tours UI', 'Summary =============== The test set includes tests that verify the Mercury\nTours Site user interface. Purpose =============== The purpose of the test set\nexecution is to check the usability and standard correspondence of the Mercury\nTours site.', '', '', '', '', '', '', '', '', ''],
                      ['', '', '', '', 'Not Completed', 'michael_qc', 'Not Completed', '', '', '', '', '', ''],
                      ['', '', '', '', '', '', '', '', '', '', 'No Run', 'Connect to Mercury Tours Site', ''],
                      ['', '', '', '', '', '', '', '', '', '', 'No Run', 'Page Title', ''],
                      ['', '', '', '', '', '', '', '', '', '', 'No Run', 'Page Text', ''],
                      ['', '', '', '', '', '', '', '', '', '', 'No Run', 'Forms', ''],
                      );

        readCsv = csvOperation.ReadCSV(fileDir);
        i=0;
        for line in readCsv.getRow():
            if i>4:
                break;
#            print line; 
            fs = frozenset(lineGolden[i]);
            print "org line:", line;
            self.assertTrue(fs.issubset(line), ("Golden line: %s \n Difference:%s"%(str(lineGolden[i]),str(fs.difference(line)))));
            i+=1;
        del readCsv
        


if __name__ == "__main__":
    suite = unittest.TestSuite()

#    suite.addTest(unittest.makeSuite(TestTestLabDownload_main));
    
    suite.addTest(unittest.makeSuite(TestTestLabDownload_AllTestSet));  ##Run all tests
    suite.addTest(unittest.makeSuite(TestTestLabDownload_TestSet));  ##Run all tests
    suite.addTest(unittest.makeSuite(TestTestLabDownload_AllTestCases));  ##Run all tests
    suite.addTest(unittest.makeSuite(TestTestLabDownload_TestCase));  ##Run all tests
    suite.addTest(unittest.makeSuite(TestTestLabDownload_AllSteps));  ##Run all tests
    suite.addTest(unittest.makeSuite(TestTestLabDownload_Step));  ##Run all tests
    suite.addTest(unittest.makeSuite(TestTestLabDownload_main));  ##Run all tests

    unittest.TextTestRunner(verbosity=2).run(suite)
