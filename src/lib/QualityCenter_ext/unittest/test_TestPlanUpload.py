'''
Created on 10-01-2012

@author: qc
'''
import unittest
from PyQt4 import QtCore, QtGui
from time import sleep
import sys
import os;
from time import localtime, strftime
from functools import partial

from win32com.client import gencache, DispatchWithEvents, constants
gencache.EnsureModule('{D66ADC20-B070-11D3-8604-0050046B8F4A}', 0, 1, 0);

import lib.QualityCenter_ext.lib.TestPlan_upload as TestPlan_upload
import lib.QualityCenter_ext.lib.qc_connect_disconnect as qc_connect_disconnect
import lib.QualityCenter_ext.lib.csvOperation as csvOperation 


server = "http://qc:8080/qcbin/";
userName="alex_qc";
domainName="DEFAULT";
projectName="QualityCenter_Demo";
password = ""; ##Put the right password



#mappingFields_TP = {
#                    'TestCase': {
#                                'Status': {'colNumber': 11, 'fieldName': u'TS_STATUS'},


mappingFields_TL ={
                   'Step': {
                            'Attachment': {
                                           'FieldIsRequired': 'False',
                                           'FieldOrder': '2',
                                           'FieldQcName': 'ATTACHMENT',
                                           'FieldType': 'char'},
                            'Status': {
                                       'FieldIsRequired': 'False',
                                       'FieldOrder': '0',
                                       'FieldQcName': u'ST_STATUS',
                                       'FieldType': u'char'},
                            'Step Name': {
                                          'FieldIsRequired': 'False',
                                          'FieldOrder': '1',
                                          'FieldQcName': u'ST_STEP_NAME',
                                          'FieldType': u'char'}},
                   'TestCase': {
                                'Attachment': {
                                               'FieldIsRequired': 'False',
                                               'FieldOrder': '5',
                                               'FieldQcName': 'ATTACHMENT',
                                               'FieldType': 'char'},
                                'Plan: Execution Status': {
                                                           'FieldIsRequired': 'False',
                                                           'FieldOrder': '2',
                                                           'FieldQcName': u'TS_EXEC_STATUS',
                                                           'FieldType': u'char'},
                                'Plan: Path': {
                                               'FieldIsRequired': 'False',
                                               'FieldOrder': '4',
                                               'FieldQcName': u'TS_PATH',
                                               'FieldType': u'char'},
                                'Plan: Subject': {
                                                  'FieldIsRequired': 'False',
                                                  'FieldOrder': '3',
                                                  'FieldQcName': u'TS_SUBJECT',
                                                  'FieldType': u'number'},
                                'Responsible Tester': {
                                                       'FieldIsRequired': 'False',
                                                       'FieldOrder': '1',
                                                       'FieldQcName': u'TC_TESTER_NAME',
                                                       'FieldType': u'char'},
                                'Test Case Description': {
                                                          'FieldIsRequired': 'False',
                                                          'FieldOrder': '6',
                                                          'FieldQcName': u'TS_DESCRIPTION',
                                                          'FieldType': u'char',},
                                'Test': {
                                         'FieldIsRequired': 'False',
                                           'FieldName': 'Test',
                                           'FieldOrder': '0',
                                           'FieldQcName': u'TS_USER_05',
                                           'FieldType': u'char',
                                           'FieldUserName': ''},
                                'Status': {
                                           'FieldIsRequired': 'False',
                                           'FieldOrder': '0',
                                           'FieldQcName': u'TC_STATUS',
                                           'FieldType': u'char'}},
                   'TestSet': {
                               'Test': {'FieldIsRequired': 'False',
                                       'FieldName': 'Test',
                                       'FieldOrder': '2',
                                       'FieldQcName': u'CY_USER_01',
                                       'FieldType': u'char'},
                               'Status': {
                                          'FieldIsRequired': 'False',
                                          'FieldOrder': '0',
                                          'FieldQcName': u'CY_STATUS',
                                          'FieldType': u'char'},
                               'Test Set': {
                                            'FieldIsRequired': 'False',
                                            'FieldOrder': '2',
                                            'FieldQcName': u'CY_CYCLE',
                                            'FieldType': u'char'},
                               
                               'Test Set Folder': {
                                                   'FieldIsRequired': 'False',
                                                   'FieldOrder': '1',
                                                   'FieldQcName': u'CY_FOLDER_ID',
                                                   'FieldType': u'number'}}}; 




mappingFields_TP = {
                    'Step': {
                             'Attachment': {
                                            'FieldIsRequired': 'False',
                                            'FieldOrder': '5',
                                            'FieldQcName': 'ATTACHMENT',
                                            'FieldType': 'char'
                                            },
                             'Description': {
                                             'FieldIsRequired': 'False',
                                             'FieldOrder': '6',
                                             'FieldQcName': u'DS_DESCRIPTION',
                                             'FieldType': u'char'
                                             },
                             'Expected Result': {
                                                 'FieldIsRequired': 'False',
                                                 'FieldOrder': '7',
                                                 'FieldQcName': u'DS_EXPECTED',
                                                 'FieldType': u'char'
                                                 },
                            'Step Name': {
                                          'FieldIsRequired': 'False',
                                          'FieldOrder': '4',
                                          'FieldQcName': u'DS_STEP_NAME',
                                          'FieldType': u'char'
                                          }
                             },
                    'TestCase': {
                                 'Path': {
                                          'FieldIsRequired': 'False',
                                          'FieldOrder': '0',
                                          'FieldQcName': u'TS_PATH',
                                          'FieldType': u'char'},
                                'Test Name': {
                                              'FieldIsRequired': 'True',
                                              'FieldOrder': '1',
                                              'FieldQcName': u'TS_NAME',
                                              'FieldType': u'char'
                                              },
                                 'Status': {
                                              'FieldIsRequired': 'True',
                                              'FieldOrder': '4',
                                              'FieldQcName': u'TS_STATUS',
                                              'FieldType': u'char'
                                              },
                                 'Description': {
                                              'FieldIsRequired': 'False',
                                              'FieldOrder': '3',
                                              'FieldQcName': u'TS_DESCRIPTION',
                                              'FieldType': u'char'
                                              },
                                'Subject': {
                                             'FieldIsRequired': 'False',
                                             'FieldOrder': '2',
                                             'FieldQcName': u'TS_SUBJECT',
                                             'FieldType': u'number'
                                             },
                                 'Attachment': {
                                               'FieldIsRequired': 'False',
                                               'FieldOrder': '2',
                                               'FieldQcName': 'ATTACHMENT',
                                               'FieldType': 'char'
                                               },
                                 },
                    'TestSet': {
                                'Test Set': {
                                             'FieldIsRequired': 'False',
                                             'FieldOrder': '9',
                                             'FieldQcName': u'CY_CYCLE',
                                             'FieldType': u'char'
                                             },
                                'Test Set Folder': {
                                                    'FieldIsRequired': 'False',
                                                    'FieldOrder': '8',
                                                    'FieldQcName': u'CY_FOLDER_ID',
                                                    'FieldType': u'number'
                                                    },
                                'Test Set Description':{
                                                        'FieldIsRequired': 'False',
                                                        'FieldOrder': '10',
                                                        'FieldQcName': u'CY_COMMENT',
                                                        'FieldType': u'char'
                                                        },
                                'Test Set Status':{
                                                        'FieldIsRequired': 'False',
                                                        'FieldOrder': '11',
                                                        'FieldQcName': u'CY_STATUS',
                                                        'FieldType': u'char'
                                                        },                        
                                }
                    };         

        
class TestTestPlanUpload_TestCase(unittest.TestCase):


    def setUp(self):
        self.qcConnect = qc_connect_disconnect.QC_connect(server, userName, password, domainName,projectName);
        for result in self.qcConnect.connect():
            print result;
        self.qc = self.qcConnect.qc;
        
        
        self.testCaseDir = r'Subject\TestDir'
        self.testCaseName = r'test'
        
        self.testPlanUpload = TestPlan_upload.TestPlanUpload(self.qc);
        
                
        self.testCase = TestPlan_upload.TestCase(self.testPlanUpload, 
                                                self.testCaseDir, 
                                                self.testCaseName, 
                                                mappingFields_TP['TestCase'])
                
       

    def tearDown(self):
        
        try:
            ##from testCaseDir, like testCaseDir = r'Subject\TestDir\Dir\dir', remove the last Directory
            parentPath, removePath = self.testCaseDir.rsplit(os.path.sep, 1) ##by this You will have ['Subject\\TestDir\\Dir', 'dir']
            parent_folder = self.testPlanUpload.tm.NodeByPath(Path=parentPath);
            parent_folder.RemoveNode(Node=removePath) ##remove testCaseDir 
        except:
            pass;
        
        self.qcConnect.disconnect_QC();


    def testTestCaseMain(self):
        '''
        Create new folder with new TestCase
        '''

        self.assertEqual(self.testCase.testCaseName, self.testCaseName); 

        
    def testMake_testCaseDirectory_1(self):
        '''
        Create folder and check how it will react when we try to create one more time the same folder
        '''
        ##Create folder. Then we will try to create one more time the same folder
        testCaseDir = r'Subject\TestDir\Test'
        parentPath, pathTestCaseDir = testCaseDir.split(os.path.sep, 1) ##by this You will have ['Subject', 'TestDir\\Test]
        folderInstanceTestCase = self.testCase.make_testCaseDirectory(testCaseDir);

        ##Create one more time the same folder. Result will be, that we will receive the same folderInstance
        folderInstance = self.testCase.make_testCaseDirectory(testCaseDir);
        
        ##Check if we have the same folderInstance
        self.assertEqual(folderInstance, folderInstanceTestCase, "FolderInstance of created folder is not the same");
         
    def testMake_testCaseDirectory_2(self):
        '''
        Create folder which path is wrong, without Subject
        '''
        ##Create folder which path is wrong.
        testCaseDir = r'Wrong\TestDir\Test'
        callableFunction = partial(self.testCase.make_testCaseDirectory, 
                                   testCaseDir
                                   );
        ##Check if we have received False response
        self.assertRaises(TestPlan_upload.TestCaseDirException, callableFunction);

    def testFindTestCase_1(self):
        '''
        Create testCase. This is OK case
        '''
        
        testCaseName = 'NewTestCase';
        testCaseInstance = self.testCase.find_TestCase(self.testCaseDir, testCaseName);
        self.assertEqual(testCaseName, testCaseInstance.Name, "Created testCase is not the same, as requested");

    def testFindTestCase_2(self):
        '''
        Get already created testCase. This is OK case
        '''
        
        testCaseName = self.testCaseName;
        testCaseInstance = self.testCase.find_TestCase(self.testCaseDir, testCaseName);
        self.assertEqual(testCaseName, testCaseInstance.Name, "Created testCase is not the same, as requested");
        
    def testFindTestCase_3(self):
        '''
        Create testCase. This is NOK case. Wrong characters,  \\/:"?\'<>|*% are not allowed
        '''
        
        testCaseName = r"!@#$%^&*()_=<>?`~~/\\";
        callableFunction = partial(self.testCase.find_TestCase, 
                                   self.testCaseDir,
                                   testCaseName
                                   );
        ##Check if we have received False response
        self.assertRaises(TestPlan_upload.TestCaseException, callableFunction);

    def testSetFieldValue_1(self):
        '''
        Test getTestCase(), correct TestCase name
        '''
        
        
        fieldUserName = 'Description';
        fieldValue = 'ble';
        self.testCase.setFieldsValue(fieldUserName, fieldValue);
        fieldName = mappingFields_TP['TestCase'].get(fieldUserName).get('FieldQcName')
        text = self.testCase.testCaseInstance.Field(fieldName);
#        self.assertEqual(text, fieldValue);
        self.assertEqual(text, fieldValue);##Field was not saved, there is some restriction
        
        
        fieldUserName = 'Status';
        fieldValue = 'Imported';
        self.testCase.setFieldsValue(fieldUserName, fieldValue);
        fieldName = mappingFields_TP['TestCase'].get(fieldUserName).get('FieldQcName')
        text = self.testCase.testCaseInstance.Field(fieldName);
        self.assertEqual(text, fieldValue);
        

        

#    def testGetTestCaseName(self):
#        testCase = TestLab_upload.TestCase(self.testCasesInstance, self.testSetName, mappingFields_TL["TestCase"]);
#        self.assertEqual(testCase.testCaseName, u'[1]Welcome Page');

class TestTestPlanUpload_AllSteps(unittest.TestCase):


    def setUp(self):
        self.qcConnect = qc_connect_disconnect.QC_connect(server, userName, password, domainName,projectName);
        for result in self.qcConnect.connect():
            print result;
        self.qc = self.qcConnect.qc;
        
        
        self.testCaseDir = r'Subject\TestDir'
        self.testCaseName = r'test'
        
        self.testPlanUpload = TestPlan_upload.TestPlanUpload(self.qc);
        
                
        self.testCase = TestPlan_upload.TestCase(self.testPlanUpload, 
                                                self.testCaseDir, 
                                                self.testCaseName, 
                                                mappingFields_TP['TestCase'])
        

    def tearDown(self):
        try:
            ##from testCaseDir, like testCaseDir = r'Subject\TestDir\Dir\dir', remove the last Directory
            parentPath, removePath = self.testCaseDir.rsplit(os.path.sep, 1) ##by this You will have ['Subject\\TestDir\\Dir', 'dir']
            parent_folder = self.testPlanUpload.tm.NodeByPath(Path=parentPath);
            parent_folder.RemoveNode(Node=removePath) ##remove testCaseDir 
        except:
            pass;
        
        self.qcConnect.disconnect_QC();


    def testremoveAllSteps_1(self):
        '''
        Test removeAllSteps(). First create some steps and then remove them 
        '''
        AllSteps = TestPlan_upload.AllSteps(self.testCase)
        
        ##Create Steps
        stepInstance = AllSteps.DesignStepFactory.AddItem("Step_1")
        stepInstance.Post();
        stepInstance = AllSteps.DesignStepFactory.AddItem("Step_2")
        stepInstance.Post();
        stepInstance = AllSteps.DesignStepFactory.AddItem("Step_3")
        stepInstance.Post();
        ##Read if the steps where created
        stepList = [];
        stepsListInstance = AllSteps.DesignStepFactory.NewList("")
        for Step_instance in stepsListInstance:
            stepList.append(Step_instance.StepName);
        allstepsGolden = [u'Step_1', 'Step_2', 'Step_3'];
        ##Check if steps where created
        self.assertEqual(stepList, allstepsGolden, 'Steps where not created correct')
                
        ##Remove steps
        AllSteps.removeSteps();
        
        ##Check if steps where removed
        stepList = [];
        for Step_instance in stepsListInstance:
            try: stepList.append(Step_instance.StepName);
            except: pass;
        self.assertEqual(stepList, [], 'Steps where not removed')
    
    def testremoveAllSteps_2(self):
        '''
        Test removeAllSteps(). Remove steps on the not existing steps 
        '''
        AllSteps = TestPlan_upload.AllSteps(self.testCase)
        stepsListInstance = AllSteps.DesignStepFactory.NewList("")
        
        
        ##Read if the steps list is empty
        stepList = [];
        for Step_instance in stepsListInstance:
            stepList.append(Step_instance.StepName);
        allstepsGolden = [];
        ##Check if steps where created
        self.assertEqual(stepList, allstepsGolden, 'Steps list is not empty, but it should')
                
        ##Remove steps
        AllSteps.removeSteps();
        
        ##Check if steps where removed
        stepList = [];
        for Step_instance in stepsListInstance:
            stepList.append(Step_instance.StepName);
        self.assertEqual(stepList, [], 'Steps where not removed')
        

class TestTestPlanUpload_Step(unittest.TestCase):


    def setUp(self):
        self.qcConnect = qc_connect_disconnect.QC_connect(server, userName, password, domainName,projectName);
        for result in self.qcConnect.connect():
            print result;
        self.qc = self.qcConnect.qc;
        
        
        self.testCaseDir = r'Subject\TestDir'
        self.testCaseName = r'test'
        self.testStepName = r'StepTest';
        self.stepName = "Step1";
        
        self.testPlanUpload = TestPlan_upload.TestPlanUpload(self.qc);
        
                
        self.testCase = TestPlan_upload.TestCase(self.testPlanUpload, 
                                                self.testCaseDir, 
                                                self.testCaseName, 
                                                mappingFields_TP['TestCase'])
        self.allSteps = TestPlan_upload.AllSteps(self.testCase);
        
        

    def tearDown(self):
        try:
            ##from testCaseDir, like testCaseDir = r'Subject\TestDir\Dir\dir', remove the last Directory
            parentPath, removePath = self.testCaseDir.rsplit(os.path.sep, 1) ##by this You will have ['Subject\\TestDir\\Dir', 'dir']
            parent_folder = self.testPlanUpload.tm.NodeByPath(Path=parentPath);
            parent_folder.RemoveNode(Node=removePath) ##remove testCaseDir 
        except:
            pass;
        
        self.qcConnect.disconnect_QC();

    def testCreateStep_1(self):
        '''
        Test findStep(), from correct TestCase. This will check if the step is created.
        '''
        
        stepsListInstance = self.allSteps.DesignStepFactory.NewList("")
        ##Read if the steps list is empty
        stepList = [];
        for Step_instance in stepsListInstance:
            stepList.append(Step_instance.StepName);
        allstepsGolden = [];
        ##Check if steps where not created before
        self.assertEqual(stepList, allstepsGolden, 'Steps list is not empty, but it should')
        
        ##create one step
        step = TestPlan_upload.Step(self.testCaseName,
                                    self.allSteps, 
                                    self.stepName,
                                    mappingFields_TP["Step"]);
        
        ##check if step was created
        stepsListInstance = self.allSteps.DesignStepFactory.NewList("")
        ##Read if the steps list is empty
        stepList = [];
        for Step_instance in stepsListInstance:
            stepList.append(Step_instance.StepName);
        allstepsGolden = [self.stepName];
        ##Check if step was created
        self.assertEqual(stepList, allstepsGolden, 'Step was not created')


    def testStepSetFieldsValue_1(self):
        '''
        Test setFieldsValue(), from correct Step. This will check if values in the step will be updated
        '''
        ##create one step
        step = TestPlan_upload.Step(self.testCaseName,
                                    self.allSteps, 
                                    self.stepName,
                                    mappingFields_TP["Step"]);
        
        
        
        fieldUserName, fieldValue = 'Description', 'test description';
        step.setFieldsValue(fieldUserName, fieldValue);
        fieldName = mappingFields_TP['Step'].get(fieldUserName).get('FieldQcName');
        text = step.stepInstance.Field(fieldName);
        ##check if value in qc is the same as it was requested to change
        self.assertEqual(text, fieldValue);




class TestTestPlanUpload_TestSet(unittest.TestCase):


    def setUp(self):
        self.qcConnect = qc_connect_disconnect.QC_connect(server, userName, password, domainName,projectName);
        for result in self.qcConnect.connect():
            print result;
        self.qc = self.qcConnect.qc;
        
        
        self.testCaseDir = r'Subject\TestDir\Test'
        self.testCaseName = r'test'
        self.testSetDir = r'Root\TestSetDir\Test'
        self.testSetName = r'TestSetName'

        
        self.testPlanUpload = TestPlan_upload.TestPlanUpload(self.qc);
        self.testLabUpload = TestPlan_upload.TestLabUpload(self.qc);
        
        
                
        self.testCase = TestPlan_upload.TestCase(self.testPlanUpload, 
                                                self.testCaseDir, 
                                                self.testCaseName, 
                                                mappingFields_TP['TestCase'])
        self.testSet =  TestPlan_upload.TestSet(self.qc, 
                                                self.testSetDir,
                                                self.testSetName,
                                                mappingFields_TP["TestSet"]
                                                );        
       

    def tearDown(self):
        
        #Delete TestCase Directory
        try:
            parentPath, removePath = self.testCaseDir.rsplit(os.path.sep, 1) ##by this You will have ['Subject\\TestDir\\Dir', 'dir']
            parent_folder = self.testPlanUpload.tm.NodeByPath(Path=parentPath);
            parent_folder.RemoveNode(Node=removePath) ##remove testCaseDir 
        except:
            pass;
        
        ##Delete TestSet Directory
        try:
            parentPath, removePath = self.testSetDir.rsplit(os.path.sep, 1) ##by this You will have ['Root\\TestDir\\Dir', 'dir']
            parent_folder = self.testLabUpload.tm.NodeByPath(Path=parentPath);
            parent_folder.RemoveNode(Node=removePath) ##remove testSetDir 
        except:
            pass;
        
        
        self.qcConnect.disconnect_QC();


    def testTestSetMain(self):
        '''
        Check created folder with new TestSet
        '''
        
        self.assertEqual(self.testSet.testSetName, self.testSetName);
        self.assertEqual(self.testSet.testSetDir, self.testSetDir);

    def testMake_testSetDirectory_1(self):
        '''
        Create folder and check how it will react when we try to create one more time the same folder
        '''
        
        
        ##Create folder. Then we will try to create one more time the same folder
        testSetDir = self.testSetDir;
        parentPath, pathTestSetDir = testSetDir.split(os.path.sep, 1) ##by this You will have ['Root', 'TestDir\\Test]
        folderInstanceTestSet = self.testSet.tm.NodeByPath(Path=testSetDir);

        ##Create one more time the same folder. Result will be, that we will receive the same folderInstance
        folderInstance = self.testSet.make_TS_directory(testSetDir);
        
        ##Check if we have the same folderInstance
        self.assertEqual(folderInstance, folderInstanceTestSet, "FolderInstance of created folder is not the same");
         
    def testMake_testSetDirectory_2(self):
        '''
        Create folder which path is wrong, without Root
        '''
        ##Create folder which path is wrong.
        testSetDir = r'Wrong\TestDir\Test'
        callableFunction = partial(self.testSet.make_TS_directory, 
                                   testSetDir
                                   );
        ##Check if we have received False response
        self.assertRaises(TestPlan_upload.TestSetDirException, callableFunction);

    def testFindTestSet_1(self):
        '''
        Create testSet. This is OK case
        '''
        
        testSetName = 'NewTestCase';
        testSetInstance = self.testSet.find_TestSet(self.testSetDir, testSetName);
        
        self.assertEqual(testSetName, testSetInstance.Name, "Created testSet is not the same, as requested");

    def testFindTestSet_2(self):
        '''
        Get already created testCase. This is OK case
        '''
        
        testSetName = self.testSetName;
        testSetInstance = self.testSet.find_TestSet(self.testSetDir, testSetName);
        
        self.assertEqual(testSetName, testSetInstance.Name, "Created testSet is not the same, as requested");
        
    def testFindTestSet_3(self):
        '''
        Create testCase. This is NOK case. Wrong characters,  \\/:"?\'<>|*% are not allowed
        '''
        
        testSetName = r"!@#$%^&*()_=<>?`~~/\\";
        callableFunction = partial(self.testSet.find_TestSet, 
                                   self.testSetDir,
                                   testSetName
                                   );
        ##Check if we have received False response
        self.assertRaises(TestPlan_upload.TestSetException, callableFunction);

    def testFindTestSet_4(self):
        '''
        Create testCase. This is NOK case. Wrong testSetDirectory will cause raise Exception
        '''
        
        testSetName = r"NewTestSet";
        testSetDir = r"Wrong\Test\Dir";
        callableFunction = partial(self.testSet.find_TestSet, 
                                   testSetDir,
                                   testSetName
                                   );
        ##Check if we have received False response
        self.assertRaises(TestPlan_upload.TestSetDirException, callableFunction);


    def testMakeTestLabTestSet_1(self):
        '''
        Check how the main part is working. NOK case, empty testCaseName and TestSetDir
        '''
        ##TestSetName is None
        testSetDir = r"Root\Test\Dir";
        callableFunction = partial(self.testSet.makeTestLabTestSet, 
                                   testSetDir=testSetDir,
                                   );
        ##Check if we have received False response
        self.assertRaises(TestPlan_upload.TestSetException, callableFunction);

        ##TestSetDir is None
        testSetName = "NewTestSet";
        callableFunction = partial(self.testSet.makeTestLabTestSet, 
                                   testSetName=testSetName
                                   );
        ##Check if we have received False response
        self.assertRaises(TestPlan_upload.TestSetException, callableFunction);
        
        
        ##TestSetName and TestSetDir are None        
        callableFunction = partial(self.testSet.makeTestLabTestSet, 
                                   );
        ##Check if we have received False response
        self.assertRaises(TestPlan_upload.TestSetException, callableFunction);
        
    def testUpdateValue_1(self):
        '''
        Test setFieldValue. OK case
        '''
        
        fieldUserName = 'Test Set Description';
        fieldValue = 'ble';
        self.testSet.setFieldsValue(fieldUserName, fieldValue);
        fieldName = mappingFields_TP['TestSet'].get(fieldUserName).get('FieldQcName');
        text = self.testSet.testSetInstance.Field(fieldName);
        self.assertEqual(text, fieldValue);
        
        
        fieldUserName = 'Test Set Status';
        fieldValue = 'Closed';
        self.testSet.setFieldsValue(fieldUserName, fieldValue);
        fieldName = mappingFields_TP['TestSet'].get(fieldUserName).get('FieldQcName');
        text = self.testSet.testSetInstance.Field(fieldName);
        self.assertEqual(text, fieldValue);

    def testUpdateValue_2(self):
        '''
        Test setFieldValue. NOK case. Given FieldName does not exists.
        '''
        
        fieldUserName = 'WrongFieldName';
        fieldValue = 'ble';
        
        callableFunction = partial(self.testSet.setFieldsValue, 
                                   fieldUserName, 
                                   fieldValue);
        ##Check if we have received False response
        self.assertRaises(TestPlan_upload.SetValueException, callableFunction);
        
        
    def testLinkTestCasetoTestSet_1(self):
        '''
        Check if TestCase is linked to created TestSet. OK case
        '''
        self.testSet.linkTestCasetoTestSet(self.testCase.testCaseInstance);
        
        ##List all already linked testCase in the TestSet
        list_TestCaseFromTS = [];
        for testCaseInstance in self.testSet.testSetInstance.TSTestFactory.NewList(""):
            list_TestCaseFromTS.append(str(testCaseInstance.TestId)); ##Add this test ID to list 

        ##Check if created TestCase ID is now in the list of TestCases created in TestSet
        self.assertTrue(str(self.testCase.testCaseInstance.ID) in list_TestCaseFromTS, "Created TestCase is not linked to TestSet")

        



class TestTestPlanUpload_main(unittest.TestCase):


    def setUp(self):
        self.qcConnect = qc_connect_disconnect.QC_connect(server, userName, password, domainName,projectName);
        for result in self.qcConnect.connect():
            print result;
        self.qc = self.qcConnect.qc;
        
    def tearDown(self):
        self.qcConnect.disconnect_QC();


    def testMain_1(self):
        '''
        Test main() method. During this main() execution folder and the CSV result file should be created with upload result 
        '''
        filename = r'D:\svn\QC_gui\trunk\src\Result\Download_02-07-2010_08-38-10\TPdownload_02-07-2010_08-38-10.xls'
        mainExecution = TestPlan_upload.MainExecution(cmd=True, qc=self.qc, filename=filename, mappingFields=mappingFields_TP);


class TestTestPlanOpenFile(unittest.TestCase):
    '''
    Check how the CSV file is responding for reading headers and lines
    '''


    
    def setUp(self):
        unittest.TestCase.setUp(self)
        
        self.filename = r'D:\Eclipse_workspace\QC_gui\src\Result\Download_02-07-2010_08-38-10\TPdownload_02-07-2010_08-38-10.xls'
        
        if not  os.path.exists(self.filename):
            self.fail("Files does not exists")

    
        
        
    def tearDown(self):
        unittest.TestCase.tearDown(self)
        
    def testGetColumnsFromFile(self):
        '''
        Check if getting column names from CSV file,  is working good
        '''
        
        goldenMappingField = {
                              'TestCase': {
                                           'Directory': {'FieldOrder': 0}, 
                                           'Name': {'FieldOrder': 1}, 
                                           'Attachments': {'FieldOrder': 2}, 
                                           'Description': {'FieldOrder': 3}}, 
                              'Step': {
                                       'Name': {'FieldOrder': 4}, 
                                       'Attachments': {'FieldOrder': 5}, 
                                       'Description': {'FieldOrder': 6},
                                       'Expected Result': {'FieldOrder': 7}},
                              'TestSet': {
                                          'Directory': {'FieldOrder': 8}, 
                                          'Name': {'FieldOrder': 9}, 
                                          'Description':{'FieldOrder': 10},
                                          'Status':{'FieldOrder': 11},
                                          },
                              }
        
        readCsv = csvOperation.ReadCSV(self.filename);
        mappingFields = readCsv.makeMappingFields();
        fs = frozenset(goldenMappingField);
        self.assertEqual(mappingFields, goldenMappingField, ("Difference:",fs.difference(mappingFields)));
        
    def testCountTestsInFile(self):
        '''
        Check if countTestsInFile() is wortking. This is OK case
        '''
        readCsv = csvOperation.ReadCSV(self.filename);
        countTests = readCsv.countTestsInFile(mappingFields_TP);
        goldenCountTests= 4;
        
        self.assertEqual(countTests, goldenCountTests, "Number of Tests in CSV file is incorrect: %d!=%d"%(countTests, goldenCountTests)); 
        
    def testGetRequiredIndex(self):
        
        readCsv = csvOperation.ReadCSV(self.filename);
        dictReq = readCsv.getRequiredIndex(mappingFields_TP);

        goldenDictReq = {
                        'TestCaseDirectory': {'FieldName': 'Path', 'FieldOrder': '0', 'FieldQcName': u'TS_PATH'}, 
                        'TestSetDirectory': {'FieldName': 'Test Set Folder', 'FieldOrder': '8', 'FieldQcName': u'CY_FOLDER_ID'}, 
                        'TestCaseName': {'FieldName': 'Test Name', 'FieldOrder': '1', 'FieldQcName': u'TS_NAME'}, 
                        'TestSetName': {'FieldName': 'Test Set', 'FieldOrder': '9', 'FieldQcName': u'CY_CYCLE'}, 
                        'StepNameTP': {'FieldName': 'Step Name', 'FieldOrder': '4', 'FieldQcName': u'DS_STEP_NAME'}
                        }
        self.assertEqual(dictReq, goldenDictReq);
    
        


if __name__ == "__main__":
    suite = unittest.TestSuite()
    
#    suite.addTest(TestTestPlanUpload_TestSet('testLinkTestCasetoTestSet_2'));  ##run only one test

    suite.addTest(unittest.makeSuite(TestTestPlanUpload_TestSet));  ##Run all tests
    suite.addTest(unittest.makeSuite(TestTestPlanUpload_TestCase));  ##Run all tests
    suite.addTest(unittest.makeSuite(TestTestPlanUpload_AllSteps));  ##Run all tests
    suite.addTest(unittest.makeSuite(TestTestPlanUpload_Step));  ##Run all tests
    suite.addTest(unittest.makeSuite(TestTestPlanUpload_main));  ##Run all tests
##
    suite.addTest(unittest.makeSuite(TestTestPlanOpenFile));  ##Run all tests


    unittest.TextTestRunner(verbosity=2).run(suite)
