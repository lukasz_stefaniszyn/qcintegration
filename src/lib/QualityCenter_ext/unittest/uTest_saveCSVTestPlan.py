# -*- coding: utf-8 -*-
#!/usr/bin/env python
'''
Created on 2010-06-23

@author: Lukasz Stefaniszyn
'''
import unittest
import sys;
import os;
import csv;

sys.path.append(r"D:\Work\svn_checkout\lukasz_repository_Fiona\QualityCenter\src");
import lib.qc_connect_disconnect as qc_connect_disconnect
import lib.crypt_pass as crypt_pass
import lib.TestPlan_download as TestPlan_download;
import lib.saveCSVTestPlan as saveCSVTestPlan;





def start():
    qcOperation = QCOperation();
    qcOperation._qcConnect();
    return qcOperation; 

def end(qcOperation):
        
    del qcOperation.qcInstance;


class QCOperation(object):

    
    def __init__(self): 
        
        self.server = "http://muvmp034.nsn-intra.net/qcbin/"; 
        self.userName="stefaniszyn";
        pass_ = "e8dbf113946b941a4a34b573363674a3c58520";
        self.password = crypt_pass.Heslo(pass_)._Heslo__readHeslo();
        self.domainName="HIT7300";
        self.projectName="hiT73_R43x";
        
        #self.qcConnect = None;
    
#------------------------------------------------------------------------------ 
    def __get_qcInstance(self):
        return self.qcConnect;
    def __set_qcInstance(self, qcConnection):
        if qcConnection is not None:
            self.qcConnect = qcConnection;
            self.qc = qcConnection.qc;
            self.tm = self.qc.TreeManager;
            self.root = self.tm.TreeRoot("Subject");
        else:
            print "Unable to set qc connection";
    def __del_qcInstance(self):
        print "Disconnecting"
        self.qcDisconnect();
    qcInstance = property(__get_qcInstance,__set_qcInstance,__del_qcInstance,"QC connection instance");
#------------------------------------------------------------------------------ 
    
    def _qcConnect(self):
        qcConnect = qc_connect_disconnect.QC_connect(self.server, 
                                                     self.userName, 
                                                     self.password, 
                                                     self.domainName, 
                                                     self.projectName);
        if qcConnect.connect():
            print "Connected OK";
            #print self.qcInstance; 
            self.qcInstance = qcConnect;
        else:
            print "ERR: Not connected"
            self.qcInstance = None;
        
    def qcDisconnect(self):
        if self.qcInstance is None:
            return False;
        
        if self.qcInstance.disconnect_QC():
            print "\nDisconnecting from QC server."\
                    "\n"\
                    "\nThank You for using QC_import_export script.\n"\
                    "\t\t\t\n"\
                    "\t\t\t  Lukasz Stefaniszyn\n"\
                    "\n\t\t\t\t\tPowered by Python 2.5";
            return True;
        else:
            print "Unable to to disconnect";
            return False
        

    def list_attachments(self, instance):
        '''Create dictionary of Attachments in Step
        input = instance(Step)
        output = dict{attachment_name:attachment_instance}'''
        
        
        list_attachements = {};
        for attachment_inst in instance.Attachments.NewList(""):
            attachment_name = attachment_inst.Name.split('_', 2)[-1];
            list_attachements[attachment_name] = attachment_inst;
        
        return list_attachements;

    def upload_attachments(self, instance, attachments):   
        '''This will upload attachments into QC database 
        input = inst(instance), str(Attachements)
        output = None;
        example, upload_attachments(TestCase_instance, str(c:\tmp\test1.txt; c:\test2.txt);
        '''
        
        list_attachments = self.list_attachments(instance);
        
#        print "attachments:\"%s\""%attachments;
        for attachment in attachments.split(';'): 
            if attachment == "":
                continue;
            attachment = attachment.strip();
            attachment_fileName = os.path.basename(attachment);
            
    
            ##Check if given attachments are present
            if os.path.exists(attachment):            
                if list_attachments.has_key(attachment_fileName):
                    ##IF the attachment to upload will be the same (in name) as the one already on the QC server, then remove this from server
                    attachment_inst_remove = list_attachments.get(attachment_fileName);
                    instance.Attachments.RemoveItem(attachment_inst_remove.ID);
                
                att = instance.Attachments
                att = att.AddItem(None);
                
                att.FileName = r'%s'%(os.getcwd() +"\\" + attachment);
                ##Upload file to QC;
                att.Type = 1;
                #att.AutoPost = 'True';
#                print "Adding attachment \"%s\" "%attachment
                att.Post();
            else:
                print "INFO: Given attachment \"%s\" does not exists. Skipping add"%attachment;
        return;

    def updateTestCase(self, dirInstance, testName):
        
        testCaseInstList = dirInstance.FindTests(Pattern=testName, MatchCase=True);
        testCaseInst = testCaseInstList[0];
        
        testCaseInst.UnLockObject();
        
        testCaseInst.SetField("TS_DESCRIPTION", "TestCase %s description"%(testName));
        testCaseInst.Post();
        
#        testCase.SetField("TS_RESPONSIBLE", "TestCase %s description"%(testName));
        
        attachments = r"LS_main1_1.txt;LS_main1_2.txt";
        self.upload_attachments(testCaseInst, attachments);
        
        dsf = testCaseInst.DesignStepFactory;
        for stepInstance in dsf.NewList(""):
            self.updateStep(stepInstance);
        testCaseInst.Post();
        testCaseInst.UnLockObject();

        
    def updateStep(self, stepInstance):
        
        stepName = stepInstance.StepName
        stepInstance.SetField("DS_DESCRIPTION", 
                                   "Description for %s"%stepName) #Step description field
        stepInstance.SetField("DS_EXPECTED", 
                                   "Expected for %s"%stepName) #Step expected field
        ##Put attachemnts into this step
        attachments = r"LS_main1_%s_1.txt;LS_main1_%s_2.txt"%(stepName, stepName);
        self.upload_attachments(stepInstance, attachments)
        stepInstance.Post();
        
        
    
    def createTPfolders(self, path):
        ''' Create/find folder in TestPlan: 
        input = str(path)
        output = Folder_instance / False (if Subject is not in given path)
        example: make_TC_directory(path=r"Subject\test\something") will create whole directory Subject\test\something or only this missing folder
        '''
        
        found_folder = None;
        
        try:
            ##Check if the whole folder exists
            found_folder = self.tm.NodeByPath(Path=path);
            
            
            print "Folder \"%s\" is already present"%(unicode(path));
            return found_folder;
        except:
            ##if folder does not exists, then try every level of the given folder. If level does not exissts, then create this level
            folder_target = path.split("\\");
            
            ##start checking which folder does not exists from given path
            for i in range(1, len(folder_target)):
                folder = str('\\'.join([ o for o in folder_target[:i+1] ])); 
#                print "current folder search:", folder;
                try:
                    if ( (i== 0) and (folder_target[i] == 'Root') ):
                        found_folder = self.root;
                    else:
                        found_folder = self.tm.NodeByPath(Path=folder);
                    ##Folder exists, go to the next level of given path
                    continue;
                except:
                    ##Go one level above from given path 
                    parent_folder = str('\\'.join([o for o in folder_target[0:i] ]));
#                    print "Parent folder:", parent_folder;
                    if ( (i== 1) and (folder_target[0] == 'Root') ):
                        parent_folder= self.root;
                    else:    
                        parent_folder = self.tm.NodeByPath(Path=parent_folder);
                    ##Create folder under parent folder
                    print "Adding folder:", folder_target[i];
                    parent_folder.AddNode(NodeName=folder_target[i]);
                    parent_folder.Post();
            found_folder = self.tm.NodeByPath(Path=folder);
            return found_folder;
    
    def createTPtestcases(self, dirTestInstance, testNameList):
        
        tf = dirTestInstance.TestFactory;
        for testCase in testNameList:
            try:
                testCaseInst = tf.AddItem(testCase);
                self.createTPsteps(testCaseInst);
                testCaseInst.Post()
            except:
                print "Unittest: TestCase already present:", testCase;
    
    def createTPsteps(self, testCaseInst):
        dsf = testCaseInst.DesignStepFactory;
        stepList = ["Step1", "Step2", "Step3"]
        for step in stepList:
            stepInst = dsf.AddItem(step);
    
    def deleteTPfolders(self, folderName):
        dirWroclaw = self.tm.NodeByPath(r"Subject\G TL1-Wroclaw");
        dirWroclaw.RemoveNode(folderName);
        
    def deleteTPtestcases(self):
        path = r"Subject\G TL1-Wroclaw\LS_Test_Main"
        dirTestInstance= self.tm.NodeByPath(Path=path)
        
        tf = dirTestInstance.TestFactory;
        allTests = dirTestInstance.FindTests("")
        for testCase in allTests:
            #print testCase.Name;
            tf.RemoveItem(testCase);


class TestSaveAsCSV(unittest.TestCase):

    def __init__(self, testname, mainDir):
        super(TestSaveAsCSV, self).__init__(testname);
        self.mainDir = mainDir;

    def setUp(self):
        self.SaveAsCSV = saveCSVTestPlan.SaveAsCSV(self.mainDir); 


    def tearDown(self):
        for root, dirs, files in os.walk(self.mainDir, topdown=False):
            for name in files:
                os.remove(os.path.join(root, name))
            for name in dirs:
                os.rmdir(os.path.join(root, name))

    def test_init__(self):
        row1 = ['Test Plan'] + ['']*8 + ['Test Lab'] + ['']*3
        row2 = ['Test Case'] + ['']*4 + ['Step'] + ['']*3 + ['Test Set']+ [''] + ['Test Case'] + [''];
        row3 = ['Directory']+['Name']+['Attachments']+['Description']+['Responsible']+\
            ['Name']+['Attachments']+['Description']+['Expected Result']+\
            ['Directory']+['Name']+\
            ['Directory']+['Name'];
        f_csv = open(self.SaveAsCSV.csvFilename, "rb");
        reader = csv.reader(f_csv, dialect = csv.excel, delimiter = '\t');
        for row in reader:
            if reader.line_num == 1:
                self.assertEqual(row, row1);
            elif reader.line_num == 2:
                self.assertEqual(row, row2);
            elif reader.line_num == 3:
                self.assertEqual(row, row3);
        f_csv.close();
        del self.SaveAsCSV.f_TPdownload;
        

    def test_setup(self):
        self.SaveAsCSV.setup(newFile=True);
        self.failUnless(os.path.exists(self.SaveAsCSV.csvFilename));
        del self.SaveAsCSV.f_TPdownload;
        

    def test_addRow(self):
        
        row = ['Test Plan'] + ['']*8 + ['Test Lab'] + ['']*3
        self.SaveAsCSV.add_row(row)
        del self.SaveAsCSV.f_TPdownload
        
        
        f_csv = open(self.SaveAsCSV.csvFilename, "rb");
        reader = csv.reader(f_csv, dialect = csv.excel, delimiter = '\t');
        compareRow = row;
        for row in reader:
            if reader.line_num == 1:
                self.assertEqual(row, compareRow);
                break;
        f_csv.close();
        
    def test_addHeading(self):
        row1 = ['Test Plan'] + ['']*8 + ['Test Lab'] + ['']*3
        self.SaveAsCSV.add_row(row1);
        row2 = ['Test Case'] + ['']*4 + ['Step'] + ['']*3 + ['Test Set']+ [''] + ['Test Case'] + [''];
        self.SaveAsCSV.add_row(row2);
        row3 = ['Directory']+['Name']+['Attachments']+['Description']+['Responsible']+\
            ['Name']+['Attachments']+['Description']+['Expected Result']+\
            ['Directory']+['Name']+\
            ['Directory']+['Name'];
        self.SaveAsCSV.add_row(row3);
        del self.SaveAsCSV.f_TPdownload;
        
        f_csv = open(self.SaveAsCSV.csvFilename, "rb");
        reader = csv.reader(f_csv, dialect = csv.excel, delimiter = '\t');
        for row in reader:
            if reader.line_num == 1:
                self.assertEqual(row, row1);
            elif reader.line_num == 2:
                self.assertEqual(row, row2);
            elif reader.line_num == 3:
                self.assertEqual(row, row3);
        f_csv.close();
        
    def test_columnCount(self):
        self.assertEqual(self.SaveAsCSV.columnCount(), 13);
        del self.SaveAsCSV.f_TPdownload;
    
    def test_makeRow(self):
        
        #cell (row, column, celltext)  column = (1...),  row= (1...)
        cells = [(1, 1, "Row=1, Col=1"),\
                 (1, 2, "Row=1, Col=2"),\
                 (1, 3, "Row=1, Col=3")];
        for cell in cells:
            rowNum = cell[0];
            columnNum = cell[1];
            cellText = cell[2];
            self.SaveAsCSV.rowPrev[columnNum-1] = cellText;
        del self.SaveAsCSV.rowPrev

        cell = (2, 1, "Row=2, Col=1");
        rowNum = cell[0];
        columnNum = cell[1];
        cellText = cell[2];
        self.SaveAsCSV.rowPrev[columnNum-1] = cellText;
        del self.SaveAsCSV.rowPrev
        
        cell = (3, 1, "Row=3, Col=1");
        rowNum = cell[0];
        columnNum = cell[1];
        cellText = cell[2];
        self.SaveAsCSV.rowPrev[columnNum-1] = cellText;
        del self.SaveAsCSV.rowPrev

        del self.SaveAsCSV.f_TPdownload;

       
        f_csv = open(self.SaveAsCSV.csvFilename, "rb");
        reader = csv.reader(f_csv, dialect = csv.excel, delimiter = '\t');
        for row in reader:
            if reader.line_num == 4:
                self.assertEqual(row[0], "Row=1, Col=1");
                self.assertEqual(row[1], "Row=1, Col=2");
                self.assertEqual(row[2], "Row=1, Col=3");
            elif reader.line_num == 5:
                self.assertEqual(row[0], "Row=2, Col=1");
            elif reader.line_num == 6:
                self.assertEqual(row[0], "Row=3, Col=1");
        f_csv.close();

if __name__ == "__main__":

    
    qcOperation = start();
    qc, tmp, root = qcOperation.qc, qcOperation.tm, qcOperation.root;
    testPlanDownload = TestPlan_download.TestPlanDownload(qcOperation.qcInstance);
    testPlanDownload.makeTargetDir();
    
    suite = unittest.TestSuite()
    suite.addTest(TestSaveAsCSV("test_init__", testPlanDownload.path));
    suite.addTest(TestSaveAsCSV("test_setup", testPlanDownload.path));
    suite.addTest(TestSaveAsCSV("test_addRow", testPlanDownload.path));
    suite.addTest(TestSaveAsCSV("test_addHeading", testPlanDownload.path));
    suite.addTest(TestSaveAsCSV("test_columnCount", testPlanDownload.path));
    suite.addTest(TestSaveAsCSV("test_makeRow", testPlanDownload.path));
    
    
    
    testResult = unittest.TextTestRunner().run(suite)
    end(qcOperation);
    print "testResult:", testResult.wasSuccessful();