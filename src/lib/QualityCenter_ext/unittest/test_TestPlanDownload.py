'''
Created on 10-01-2012

@author: qc
'''
import unittest
from PyQt4 import QtCore, QtGui
from time import sleep
import sys
import os;
from functools import partial

from win32com.client import gencache, DispatchWithEvents, constants
gencache.EnsureModule('{D66ADC20-B070-11D3-8604-0050046B8F4A}', 0, 1, 0);

import lib.QualityCenter_ext.lib.TestPlan_download as TestPlan_download
import lib.QualityCenter_ext.lib.qc_connect_disconnect as qc_connect_disconnect
import lib.QualityCenter_ext.lib.csvOperation as csvOperation 


server = "http://qc:8080/qcbin/";
userName="alex_qc";
domainName="DEFAULT";
projectName="QualityCenter_Demo";
password = ""; ##Put the right password



mappingFields_TL ={
                   'Step': {
                            'Attachment': {
                                           'FieldIsRequired': 'False',
                                           'FieldOrder': '2',
                                           'FieldQcName': 'ATTACHMENT',
                                           'FieldType': 'char'},
                            'Status': {
                                       'FieldIsRequired': 'False',
                                       'FieldOrder': '0',
                                       'FieldQcName': u'ST_STATUS',
                                       'FieldType': u'char'},
                            'Step Name': {
                                          'FieldIsRequired': 'False',
                                          'FieldOrder': '1',
                                          'FieldQcName': u'ST_STEP_NAME',
                                          'FieldType': u'char'}},
                   'TestCase': {
                                'Attachment': {
                                               'FieldIsRequired': 'False',
                                               'FieldOrder': '5',
                                               'FieldQcName': 'ATTACHMENT',
                                               'FieldType': 'char'},
                                'Plan: Execution Status': {
                                                           'FieldIsRequired': 'False',
                                                           'FieldOrder': '2',
                                                           'FieldQcName': u'TS_EXEC_STATUS',
                                                           'FieldType': u'char'},
                                'Plan: Path': {
                                               'FieldIsRequired': 'False',
                                               'FieldOrder': '4',
                                               'FieldQcName': u'TS_PATH',
                                               'FieldType': u'char'},
                                'Plan: Subject': {
                                                  'FieldIsRequired': 'False',
                                                  'FieldOrder': '3',
                                                  'FieldQcName': u'TS_SUBJECT',
                                                  'FieldType': u'number'},
                                'Responsible Tester': {
                                                       'FieldIsRequired': 'False',
                                                       'FieldOrder': '1',
                                                       'FieldQcName': u'TC_TESTER_NAME',
                                                       'FieldType': u'char'},
                                'Status': {
                                           'FieldIsRequired': 'False',
                                           'FieldOrder': '0',
                                           'FieldQcName': u'TC_STATUS',
                                           'FieldType': u'char'}},
                   'TestSet': {
                               'Description': {
                                               'FieldIsRequired': 'False',
                                               'FieldOrder': '3',
                                               'FieldQcName': u'CY_COMMENT',
                                               'FieldType': u'char'},
                               'Status': {
                                          'FieldIsRequired': 'False',
                                          'FieldOrder': '0',
                                          'FieldQcName': u'CY_STATUS',
                                          'FieldType': u'char'},
                               'Test Set': {
                                            'FieldIsRequired': 'False',
                                            'FieldOrder': '2',
                                            'FieldQcName': u'CY_CYCLE',
                                            'FieldType': u'char'},
                               'Test Set Folder': {
                                                   'FieldIsRequired': 'False',
                                                   'FieldOrder': '1',
                                                   'FieldQcName': u'CY_FOLDER_ID',
                                                   'FieldType': u'number'}}}; 




mappingFields_TP = {
                    'Step': {
                             'Attachment': {
                                            'FieldIsRequired': 'False',
                                            'FieldOrder': '3',
                                            'FieldQcName': 'ATTACHMENT',
                                            'FieldType': 'char'
                                            },
                             'Description': {
                                             'FieldIsRequired': 'False',
                                             'FieldOrder': '1',
                                             'FieldQcName': u'DS_DESCRIPTION',
                                             'FieldType': u'char'
                                             },
                             'Expected Result': {
                                                 'FieldIsRequired': 'False',
                                                 'FieldOrder': '2',
                                                 'FieldQcName': u'DS_EXPECTED',
                                                 'FieldType': u'char'
                                                 },
                            'Step Name': {
                                          'FieldIsRequired': 'False',
                                          'FieldOrder': '0',
                                          'FieldQcName': u'DS_STEP_NAME',
                                          'FieldType': u'char'
                                          }
                             },
                    'TestCase': {
                                 'Path': {
                                          'FieldIsRequired': 'False',
                                          'FieldOrder': '0',
                                          'FieldQcName': u'TS_PATH',
                                          'FieldType': u'char'},
                                'Test Name': {
                                              'FieldIsRequired': 'True',
                                              'FieldOrder': '1',
                                              'FieldQcName': u'TS_NAME',
                                              'FieldType': u'char'
                                              },
                                 'Status': {
                                              'FieldIsRequired': 'True',
                                              'FieldOrder': '4',
                                              'FieldQcName': u'TS_STATUS',
                                              'FieldType': u'char'
                                              },
                                 'Description': {
                                              'FieldIsRequired': 'False',
                                              'FieldOrder': '3',
                                              'FieldQcName': u'TS_DESCRIPTION',
                                              'FieldType': u'char'
                                              },
                                'Subject': {
                                             'FieldIsRequired': 'False',
                                             'FieldOrder': '2',
                                             'FieldQcName': u'TS_SUBJECT',
                                             'FieldType': u'number'
                                             },
                                 'Attachment': {
                                               'FieldIsRequired': 'False',
                                               'FieldOrder': '2',
                                               'FieldQcName': 'ATTACHMENT',
                                               'FieldType': 'char'
                                               },
                                 },
                    'TestSet': {
                                'Attachment': {
                                               'FieldIsRequired': 'False',
                                               'FieldOrder': '3',
                                               'FieldQcName': 'ATTACHMENT',
                                               'FieldType': 'char'
                                               },
                                'Status': {
                                           'FieldIsRequired': 'False',
                                           'FieldOrder': '2',
                                           'FieldQcName': u'CY_STATUS',
                                           'FieldType': u'char'
                                           },
                                'Test Set': {
                                             'FieldIsRequired': 'False',
                                             'FieldOrder': '0',
                                             'FieldQcName': u'CY_CYCLE',
                                             'FieldType': u'char'
                                             },
                                'Test Set Folder': {
                                                    'FieldIsRequired': 'False',
                                                    'FieldOrder': '1',
                                                    'FieldQcName': u'CY_FOLDER_ID',
                                                    'FieldType': u'number'
                                                    }
                                }
                    }; 


class TestTestPlanDownload_AllTestCases(unittest.TestCase):


    def setUp(self):
        self.qcConnect = qc_connect_disconnect.QC_connect(server, userName, password, domainName,projectName);
        for result in self.qcConnect.connect():
            print result;
        self.qc = self.qcConnect.qc;
        
        self.testPlanDownload = TestPlan_download.TestPlanDownload(self.qc);
        
    def tearDown(self):
        self.qcConnect.disconnect_QC();

    def testGetTestCase_1(self):
        '''
        Test getTestCase(), correct TestCase directory and TestCase name
        '''
        testCaseDir = r'Subject\Flight Reservation\Flight Finder'
        testCaseName = r'Departing Date'
        
        allTestCases = TestPlan_download.AllTestCases(self.testPlanDownload);
        testCasesInstList = allTestCases.getTestCase(testCaseDir, testCaseName);
        self.assertEqual(testCaseName, testCasesInstList[0].Name);
    
    def testGetTestCase_2(self):
        '''
        Test getTestCase(), wrong TestCase name. Return will be None 
        '''
        
        testCaseDir = r'Subject\Flight Reservation\Flight Finder'
        testCaseName= r'WrongName'
        
        allTestCases = TestPlan_download.AllTestCases(self.testPlanDownload);
        testCasesInstList = allTestCases.getTestCase(testCaseDir, testCaseName);
        self.assertTrue(testCasesInstList is None);
    
    def testGetTestCase_3(self):
        '''
        Test getTestCase(), wrong TestCase directory. Return will be None 
        '''
        
        testCaseDir = r'Subject\WrongDIR'
        testCaseName = r'Departing Date'
        
        allTestCases = TestPlan_download.AllTestCases(self.testPlanDownload);
        testCasesInstList = allTestCases.getTestCase(testCaseDir, testCaseName);
        self.assertTrue(testCasesInstList is None);
    def testGetAllTestCase_1(self):
        '''
        Test getAllTestCase(), correct TestCase directory. Return list of TestCases
        '''
        testCaseDir = r'Subject\Flight Reservation\Flight Finder'
        
        allTestCases = TestPlan_download.AllTestCases(self.testPlanDownload);
        testCasesInstList = allTestCases.getAllTestCases(testCaseDir);
        listAllTestCases= [testCase.Name for testCase in testCasesInstList];
        goldenListAllTestCases = [u'Find Flight', u'Departing Date', u'Returning Date', u'View Calendar', u'Service Class Preference', u'Airline Preference', u'Flight Time Preference', u'Range of Dates', u'Invalid Date Input', u'Find Flight Page', u'Departing And Arriving Locations', u'Number Of Passengers', u'Trip Type']
        self.assertEqual(listAllTestCases, goldenListAllTestCases)
    
    
    def testGetAllTestCase_2(self):
        '''
        Test getAllTestCase(), wrong TestCase directory. Return None
        '''
        testCaseDir = r'WrongDir'
        
        allTestCases = TestPlan_download.AllTestCases(self.testPlanDownload);
        listAllTestCases = allTestCases.getAllTestCases(testCaseDir);
        self.assertTrue(listAllTestCases is None)
    
    def testGetAllTestCase_3(self):
        '''
        Test getAllTestCase(), TestCase directory is not empty. Return list of all TestCases from the QC
        '''
        
        
        allTestCases = TestPlan_download.AllTestCases(self.testPlanDownload);
        listAllTestCases = allTestCases.getAllTestCases();
        listAllTestCases= [testCase.Name for testCase in listAllTestCases];
        goldenListAllTestCases = [u'Find Flight', u'Departing Date', u'Returning Date', u'View Calendar', u'Service Class Preference', u'Airline Preference', u'Flight Time Preference', u'Range of Dates', u'Invalid Date Input', u'Find Flight Page', u'Departing And Arriving Locations', u'Number Of Passengers', u'Trip Type'];
        
        fs = frozenset(goldenListAllTestCases);
        self.assertTrue( fs.issubset(listAllTestCases),
                     "Not found TestCases in all set of TestCases"); #Check if compateTestCaseNames is testCasesInstNames
        
    def testmainGetTestCase_1(self):
        testCaseDir = r'Subject\Flight Reservation\Flight Finder'
        testCaseName = r'Departing Date'
        
        allTestCases = TestPlan_download.AllTestCases(self.testPlanDownload);
        listTestCases = allTestCases.mainGetTestCase(testCaseDir, testCaseName);
        self.assertEqual(testCaseName, listTestCases[0].Name);
    def testmainGetTestCase_2(self):
        testCaseDir = r'Subject\Flight Reservation\Flight Finder'
        
        allTestCases = TestPlan_download.AllTestCases(self.testPlanDownload);
        listTestCases = allTestCases.mainGetTestCase(testCaseDir);
        listAllTestCases= [testCase.Name for testCase in listTestCases];
        goldenListAllTestCases = [u'Find Flight', u'Departing Date', u'Returning Date', u'View Calendar', u'Service Class Preference', u'Airline Preference', u'Flight Time Preference', u'Range of Dates', u'Invalid Date Input', u'Find Flight Page', u'Departing And Arriving Locations', u'Number Of Passengers', u'Trip Type']
        self.assertEqual(listAllTestCases, goldenListAllTestCases);
        
    def testmainGetTestCase_3(self):
#        TSdir = r'Root\Mercury Tours (HTML Edition)\Version 1.0\Functionality And UI'
        
        allTestCases = TestPlan_download.AllTestCases(self.testPlanDownload);
        listTestCases = allTestCases.mainGetTestCase();
        listAllTestCases= [testCase.Name for testCase in listTestCases];
        goldenListAllTestCases = [u'Find Flight', u'Departing Date', u'Returning Date', u'View Calendar', u'Service Class Preference', u'Airline Preference', u'Flight Time Preference', u'Range of Dates', u'Invalid Date Input', u'Find Flight Page', u'Departing And Arriving Locations', u'Number Of Passengers', u'Trip Type']
        fs = frozenset(goldenListAllTestCases);
        self.assertTrue( fs.issubset(listAllTestCases),
                     "Not found TestCases in all set of TestCases"); #Check if compateTestCaseNames is testCasesInstNames
        
    

        
class TestTestPlanDownload_TestCase(unittest.TestCase):


    def setUp(self):
        self.qcConnect = qc_connect_disconnect.QC_connect(server, userName, password, domainName,projectName);
        for result in self.qcConnect.connect():
            print result;
        self.qc = self.qcConnect.qc;
        
        self.testPlanDownload = TestPlan_download.TestPlanDownload(self.qc);
        
        testCaseDir = r'Subject\Flight Reservation\Flight Finder'
        testCaseName = r'Departing Date'
        
        
        allTestCases = TestPlan_download.AllTestCases(self.testPlanDownload);
        testCasesInstList = allTestCases.mainGetTestCase(testCaseDir, testCaseName)
        self.testCasesInstance = testCasesInstList[0] 
        
        testCase = TestPlan_download.TestCase(self.testCasesInstance, mappingFields_TP["TestCase"]);
        self.testCaseName = testCase.testCaseName
        
    def tearDown(self):
        self.qcConnect.disconnect_QC();


    def testGetTestCase_1(self):
        '''
        Test getTestCase(), correct TestCase name
        '''
        
        testCase = TestPlan_download.TestCase(self.testCasesInstance, mappingFields_TP["TestCase"]);
        for fieldUserName, fieldValue in testCase.getFieldsValue():
            if fieldUserName == "Test Name":
                self.assertEqual(fieldValue, testCase.testCaseName);
#            print "fieldUserName:",fieldUserName;
#            print "fieldValue:",fieldValue;
    
    def testGetTestCaseName(self):
        testCase = TestPlan_download.TestCase(self.testCasesInstance, mappingFields_TP["TestCase"]);
        self.assertEqual(testCase.testCaseName, u'Departing Date');

class TestTestPlanDownload_AllSteps(unittest.TestCase):


    def setUp(self):
        self.qcConnect = qc_connect_disconnect.QC_connect(server, userName, password, domainName,projectName);
        for result in self.qcConnect.connect():
            print result;
        self.qc = self.qcConnect.qc;
        
        self.testPlanDownload = TestPlan_download.TestPlanDownload(self.qc);
        
        testCaseDir = r'Subject\Flight Reservation\Flight Finder'
        testCaseName = r'Departing Date'
        
        
        allTestCases = TestPlan_download.AllTestCases(self.testPlanDownload);
        testCasesInstList = allTestCases.mainGetTestCase(testCaseDir, testCaseName)
        self.testCasesInstance = testCasesInstList[0] 
        
        testCase = TestPlan_download.TestCase(self.testCasesInstance, mappingFields_TP["TestCase"]);
        self.testCaseName = testCase.testCaseName
        
        

    def tearDown(self):
        self.qcConnect.disconnect_QC();


    def testGetAllSteps_1(self):
        '''
        Test getAllSteps(), with Run 
        '''
        AllSteps = TestPlan_download.AllSteps(self.testCasesInstance, 
                                             self.testCaseName)
        allSteps = AllSteps.getAllSteps();
        steps = [step.StepName for step in allSteps];
        allstepsGolden = [u'Call', u'Preparation', u'Step 1: Past Departing Date', u'Step 2: Departing Date Today', u'Step 3: Future Departing Date']
        
        fs = frozenset(allstepsGolden);
        self.assertTrue(fs.issubset(steps),
                     "Not found Steps in all set of TestCase")
    


class TestTestPlanDownload_Step(unittest.TestCase):


    def setUp(self):
        self.qcConnect = qc_connect_disconnect.QC_connect(server, userName, password, domainName,projectName);
        for result in self.qcConnect.connect():
            print result;
        self.qc = self.qcConnect.qc;
        
        self.testPlanDownload = TestPlan_download.TestPlanDownload(self.qc);
        
        testCaseDir = r'Subject\Flight Reservation\Flight Finder'
        testCaseName = r'Departing Date'
        
        
        allTestCases = TestPlan_download.AllTestCases(self.testPlanDownload);
        testCasesInstList = allTestCases.mainGetTestCase(testCaseDir, testCaseName)
        self.testCasesInstance = testCasesInstList[0] 
        
        testCase = TestPlan_download.TestCase(self.testCasesInstance, mappingFields_TP["TestCase"]);
        self.testCaseName = testCase.testCaseName
        
        
        AllSteps = TestPlan_download.AllSteps(self.testCasesInstance, 
                                             self.testCaseName)
        self.stepInstance = AllSteps.getAllSteps()[2];
        
        
        


    def tearDown(self):
        self.qcConnect.disconnect_QC();


    def testGetStep_1(self):
        '''
        Test getStep(), from correct TestCase
        '''
        
        step = TestPlan_download.Step(self.stepInstance, 
                                     self.testCaseName, 
                                     mappingFields_TP["Step"]);
        for fieldUserName, fieldValue in step.getFieldsValue():
            if fieldUserName == "Step Name":
                self.assertEqual(fieldValue, step.stepName);
#            print "fieldUserName:",fieldUserName;
#            print "fieldValue:",fieldValue;

    def testGetStepName(self):
        step = TestPlan_download.Step(self.stepInstance, 
                                     self.testCaseName, 
                                     mappingFields_TP["Step"]);
                    
        self.assertEqual(step.stepName, "Step 1: Past Departing Date");


class TestTestPlanDownload_main(unittest.TestCase):


    def setUp(self):
        self.qcConnect = qc_connect_disconnect.QC_connect(server, userName, password, domainName,projectName);
        for result in self.qcConnect.connect():
            print result;
        self.qc = self.qcConnect.qc;
        
    def tearDown(self):
        self.qcConnect.disconnect_QC();


    def testMain_1(self):
        '''
        Test main() method. During this main() execution folder and the CSV result file should be created with download result 
        '''

        
        testCaseDir = r'Subject\Flight Reservation\Flight Finder'
        testCaseName = r'Departing Date'
        
        
        mainExecution = TestPlan_download.MainExecution(cmd=True, 
                                                        qc=self.qc, 
                                                        testCaseDir=testCaseDir, 
                                                        testCaseName=testCaseName, 
                                                        mappingFields=mappingFields_TP, 
                                                        );
#        path = TestPlan_download.mainCMD(self.qc, testCaseDir, testCaseName, mappingFields_TP);
        
        path = mainExecution.path
        print "path:", path;
        filename = os.path.basename(path) + r".xls";
        fileDir = os.path.join(path, filename);
        
        
        lineGolden = (
                        ['', '', '', '', 'Departing Date', '', 'Ready', '', '', '', '', ''],
                        ['', '', '', '', '', '', '', '', 'Call', 'Connect And Sign-On', '', ''],
                        ['', '', '', '', '', '', '', '', 'Preparation', 'Execute the Preparation step before you execute each step in the test. 1.\nSelect one of the values from the Passengers list. 2. Select Departing From\nand Arriving In locations (selected items must be different). 3. Leave default\nvalues in all other fields.', '', ''],
                        ['', '', '', '', '', '', '', '', 'Step 1: Past Departing Date', '1. Select the One Way option. 2. In Departing Date, select any past date.\nCheck each of the following possibilities: - Previous month, any date\nselected. - Past date, current month selected. 3. Click the Continue button.', 'The list of flights should not be given. You should be requested to insert\nvalid departing date.', ''],
                        ['', '', '', '', '', '', '', '', 'Step 2: Departing Date Today', "1. Select the One Way option. 2. Select today's date as the Departing Date. 3.\nClick the Continue button.", 'The list of flights should not be given. An error message should indicate that\nflight should be booked at least one day in advance.', ''],
                      );

        readCsv = csvOperation.ReadCSV(fileDir);
        i=0;
        for line in readCsv.getRow():
            if i>4:
                break;
#            print line; 
            fs = frozenset(lineGolden[i]);
            self.assertTrue(fs.issubset(line), ("Golden line: %s \nCompare line: %s \nDifference:%s"%(str(lineGolden[i]),str(line), str(fs.difference(line)))));
            i+=1;
        del readCsv
        


if __name__ == "__main__":
    suite = unittest.TestSuite()

#    suite.addTest(TestTestPlanDownload_AllTestCases('testGetTestCase_1'));
#    suite.addTest(TestTestPlanDownload_AllTestCases('testGetTestCase_2'));
#    suite.addTest(TestTestPlanDownload_AllTestCases('testGetTestCase_3'));
#    suite.addTest(TestTestPlanDownload_AllTestCases('testGetAllTestCase_1'));
#    suite.addTest(TestTestPlanDownload_AllTestCases('testGetAllTestCase_2'));
#    suite.addTest(TestTestPlanDownload_AllTestCases('testGetAllTestCase_3'));
#    suite.addTest(TestTestPlanDownload_AllTestCases('testmainGetTestCase_1'));
#    suite.addTest(TestTestPlanDownload_AllTestCases('testmainGetTestCase_2'));
#    suite.addTest(TestTestPlanDownload_AllTestCases('testmainGetTestCase_3'));
    
    suite.addTest(unittest.makeSuite(TestTestPlanDownload_AllTestCases));  ##Run all tests
    suite.addTest(unittest.makeSuite(TestTestPlanDownload_TestCase));  ##Run all tests
    suite.addTest(unittest.makeSuite(TestTestPlanDownload_AllSteps));  ##Run all tests
    suite.addTest(unittest.makeSuite(TestTestPlanDownload_Step));  ##Run all tests
    suite.addTest(unittest.makeSuite(TestTestPlanDownload_main));  ##Run all tests

    unittest.TextTestRunner(verbosity=2).run(suite)
