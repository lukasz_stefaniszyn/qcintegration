'''
Created on 10-01-2012

@author: qc
'''
import unittest
from PyQt4 import QtCore, QtGui
from time import sleep
import sys
import os;
from time import localtime, strftime
from functools import partial

from win32com.client import gencache, DispatchWithEvents, constants
gencache.EnsureModule('{D66ADC20-B070-11D3-8604-0050046B8F4A}', 0, 1, 0);

import lib.QualityCenter_ext.lib.TestLab_upload as TestLab_upload
import lib.QualityCenter_ext.lib.qc_connect_disconnect as qc_connect_disconnect
import lib.QualityCenter_ext.lib.csvOperation as csvOperation 


server = "http://qc:8080/qcbin/";
userName="alex_qc";
domainName="DEFAULT";
projectName="QualityCenter_Demo";
password = ""; ##Put the right password



#mappingFields_TP = {
#                    'TestCase': {
#                                'Status': {'colNumber': 11, 'fieldName': u'TS_STATUS'},


mappingFields_TL ={
                   'Step': {
                            'Attachment': {
                                           'FieldIsRequired': 'False',
                                           'FieldOrder': '51',
                                           'FieldQcName': 'ATTACHMENT',
                                           'FieldType': 'char'},
                            'Status': {
                                       'FieldIsRequired': 'False',
                                       'FieldOrder': '43',
                                       'FieldQcName': u'ST_STATUS',
                                       'FieldType': u'char'},
                            'Step Name': {
                                          'FieldIsRequired': 'False',
                                          'FieldOrder': '49',
                                          'FieldQcName': u'ST_STEP_NAME',
                                          'FieldType': u'char'}},
                   'TestCase': {
                                'Attachment': {
                                               'FieldIsRequired': 'False',
                                               'FieldOrder': '42',
                                               'FieldQcName': 'ATTACHMENT',
                                               'FieldType': 'char'},
                                'Plan: Execution Status': {
                                                           'FieldIsRequired': 'False',
                                                           'FieldOrder': '33',
                                                           'FieldQcName': u'TS_EXEC_STATUS',
                                                           'FieldType': u'char'},
                                'Plan: Path': {
                                               'FieldIsRequired': 'False',
                                               'FieldOrder': '36',
                                               'FieldQcName': u'TS_PATH',
                                               'FieldType': u'char'},
                                'Plan: Subject': {
                                                  'FieldIsRequired': 'False',
                                                  'FieldOrder': '13',
                                                  'FieldQcName': u'TS_SUBJECT',
                                                  'FieldType': u'number'},
                                'Responsible Tester': {
                                                       'FieldIsRequired': 'False',
                                                       'FieldOrder': '20',
                                                       'FieldQcName': u'TC_TESTER_NAME',
                                                       'FieldType': u'char'},
                                'Test Case Description': {
                                                          'FieldIsRequired': 'False',
                                                          'FieldOrder': '30',
                                                          'FieldQcName': u'TS_DESCRIPTION',
                                                          'FieldType': u'char',},
                                'Test': {
                                         'FieldIsRequired': 'False',
                                           'FieldName': 'Test',
                                           'FieldOrder': '24',
                                           'FieldQcName': u'TS_USER_05',
                                           'FieldType': u'char',
                                           'FieldUserName': ''},
                                'Test Case Name': {
                                                   'FieldIsRequired': 'True',
                                                   'FieldName': 'Plan: Test Name',
                                                   'FieldOrder': '26',
                                                   'FieldQcName': u'TS_NAME',
                                                   'FieldType': u'char',
                                                   'FieldUserName': ''},
                                'Status': {
                                           'FieldIsRequired': 'False',
                                           'FieldOrder': '11',
                                           'FieldQcName': u'TC_STATUS',
                                           'FieldType': u'char'}},
                   'TestSet': {
                               'Test': {'FieldIsRequired': 'False',
                                       'FieldName': 'Test',
                                       'FieldOrder': '5',
                                       'FieldQcName': u'CY_USER_01',
                                       'FieldType': u'char'},
                                'Description': {
                                               'FieldIsRequired': 'False',
                                               'FieldOrder': '3',
                                               'FieldQcName': u'CY_COMMENT',
                                               'FieldType': u'char'},
                               'Status': {
                                          'FieldIsRequired': 'False',
                                          'FieldOrder': '0',
                                          'FieldQcName': u'CY_STATUS',
                                          'FieldType': u'char'},
                               'Test Set': {
                                            'FieldIsRequired': 'False',
                                            'FieldOrder': '6',
                                            'FieldQcName': u'CY_CYCLE',
                                            'FieldType': u'char'},
                               
                               'Test Set Folder': {
                                                   'FieldIsRequired': 'False',
                                                   'FieldOrder': '4',
                                                   'FieldQcName': u'CY_FOLDER_ID',
                                                   'FieldType': u'number'}}}; 




mappingFields_TP = {
                    'Step': {
                             'Attachment': {
                                            'FieldIsRequired': 'False',
                                            'FieldOrder': '3',
                                            'FieldQcName': 'ATTACHMENT',
                                            'FieldType': 'char'
                                            },
                             'Description': {
                                             'FieldIsRequired': 'False',
                                             'FieldOrder': '1',
                                             'FieldQcName': u'DS_DESCRIPTION',
                                             'FieldType': u'char'
                                             },
                             'Expected Result': {
                                                 'FieldIsRequired': 'False',
                                                 'FieldOrder': '2',
                                                 'FieldQcName': u'DS_EXPECTED',
                                                 'FieldType': u'char'
                                                 },
                            'Step Name': {
                                          'FieldIsRequired': 'False',
                                          'FieldOrder': '0',
                                          'FieldQcName': u'DS_STEP_NAME',
                                          'FieldType': u'char'
                                          }
                             },
                    'TestCase': {
                                 'Path': {
                                          'FieldIsRequired': 'False',
                                          'FieldOrder': '0',
                                          'FieldQcName': u'TS_PATH',
                                          'FieldType': u'char'},
                                'Test Name': {
                                              'FieldIsRequired': 'True',
                                              'FieldOrder': '1',
                                              'FieldQcName': u'TS_NAME',
                                              'FieldType': u'char'
                                              },
                                 'Status': {
                                              'FieldIsRequired': 'True',
                                              'FieldOrder': '4',
                                              'FieldQcName': u'TS_STATUS',
                                              'FieldType': u'char'
                                              },
                                 'Description': {
                                              'FieldIsRequired': 'False',
                                              'FieldOrder': '3',
                                              'FieldQcName': u'TS_DESCRIPTION',
                                              'FieldType': u'char'
                                              },
                                'Subject': {
                                             'FieldIsRequired': 'False',
                                             'FieldOrder': '2',
                                             'FieldQcName': u'TS_SUBJECT',
                                             'FieldType': u'number'
                                             },
                                 'Attachment': {
                                               'FieldIsRequired': 'False',
                                               'FieldOrder': '2',
                                               'FieldQcName': 'ATTACHMENT',
                                               'FieldType': 'char'
                                               },
                                 },
                    'TestSet': {
                                'Attachment': {
                                               'FieldIsRequired': 'False',
                                               'FieldOrder': '3',
                                               'FieldQcName': 'ATTACHMENT',
                                               'FieldType': 'char'
                                               },
                                'Status': {
                                           'FieldIsRequired': 'False',
                                           'FieldOrder': '2',
                                           'FieldQcName': u'CY_STATUS',
                                           'FieldType': u'char'
                                           },
                                'Test Set': {
                                             'FieldIsRequired': 'False',
                                             'FieldOrder': '0',
                                             'FieldQcName': u'CY_CYCLE',
                                             'FieldType': u'char'
                                             },
                                'Test Set Folder': {
                                                    'FieldIsRequired': 'False',
                                                    'FieldOrder': '1',
                                                    'FieldQcName': u'CY_FOLDER_ID',
                                                    'FieldType': u'number'
                                                    }
                                }
                    };         
        
class TestTestLabUpload_TestSet(unittest.TestCase):


    def setUp(self):
        self.qcConnect = qc_connect_disconnect.QC_connect(server, userName, password, domainName,projectName);
        for result in self.qcConnect.connect():
            print result;
        self.qc = self.qcConnect.qc;
        
        self.testLabUpload = TestLab_upload.TestLabUpload(self.qc);
        
        self.TSdir = r'Root\Mercury Tours (HTML Edition)\Version 1.0\Functionality And UI'
        self.TSname = r'Mercury Tours Sanity'
        


    def tearDown(self):
        self.qcConnect.disconnect_QC();


    def testGetTestSetName(self):
        '''
        Test getTestSet(), correct TestSet directory and TestSet name
        '''
        testSet = TestLab_upload.TestSet(self.testLabUpload, self.TSdir, self.TSname, mappingFields_TL["TestSet"]);
        self.assertEqual(testSet.testSetName, self.TSname);
    
    def testGetTestSet_1(self):
        '''
        Test getTestSet(), exception will be raised, wrong TestSetName
        '''
        TSname = "WrongName";
        callableFunction = partial(TestLab_upload.TestSet, 
                                   self.testLabUpload, 
                                   self.TSdir, 
                                   TSname, 
                                   mappingFields_TL["TestSet"]
                                   );
        self.assertRaises(TestLab_upload.TestSetException, callableFunction);
    
    def testGetTestSet_2(self):
        '''
        Test getTestSet(), exception will be raised, wrong TestSetDirectory
        '''
        TSdir = r'Root\Wrong\\'
        callableFunction = partial(TestLab_upload.TestSet, 
                                   self.testLabUpload, 
                                   TSdir, 
                                   self.TSname, 
                                   mappingFields_TL["TestSet"]
                                   );
        self.assertRaises(TestLab_upload.TestSetException, callableFunction);
        
    def testUpdateValue_1(self):
        '''
        Test setFieldValue, 
        '''
        
        testSet = TestLab_upload.TestSet(self.testLabUpload, self.TSdir, self.TSname, mappingFields_TL["TestSet"]);
        
        fieldUserName = 'Description';
        fieldValue = 'ble';
        testSet.setFieldsValue(fieldUserName, fieldValue);
        
        
        fieldName = mappingFields_TL['TestSet'].get(fieldUserName).get('FieldQcName');
        text = testSet.testSetInstance.Field(fieldName);
        
        self.assertEqual(text, fieldValue);
        
        
        fieldUserName = 'Test';
        fieldValue = '';
        testSet.setFieldsValue(fieldUserName, fieldValue);
#        fieldUserName, fieldValue = 'Description', 'Cos tam';
#        testSet.setFieldsValue(fieldUserName, fieldValue)
        
class TestTestLabUpload_AllTestCases(unittest.TestCase):


    def setUp(self):
        self.qcConnect = qc_connect_disconnect.QC_connect(server, userName, password, domainName,projectName);
        for result in self.qcConnect.connect():
            print result;
        self.qc = self.qcConnect.qc;
        
        self.TSdir = r'Root\Mercury Tours (HTML Edition)\Version 1.0\Functionality And UI'
        self.TSname = r'Mercury Tours Sanity'
        
        self.testLabUpload = TestLab_upload.TestLabUpload(self.qc);
                
        self.testSet = TestLab_upload.TestSet(self.testLabUpload, self.TSdir, self.TSname, mappingFields_TL["TestSet"]); 
        self.testSetName = self.testSet.testSetName


    def tearDown(self):
        self.qcConnect.disconnect_QC();


    def testGetAllTestCase_1(self):
        '''
        Test getTestCase(), correct TestCase directory and TestCase name
        '''
        
        allTestCases = TestLab_upload.AllTestCases(self.testSet.testSetInstance, self.testSetName);
        
        
        testCases = allTestCases.getAllTestCases();
        testCasesName = [tc.Field("TS_NAME") for tc in testCases]
        testCasesNameGolden = [u'[1]Profiling', u'[1]Flight Reservation', u'[1]Itinerary Page', u'[1]Site_Stability']

        fs = frozenset(testCasesNameGolden);
        self.assertEqual(testCasesName, testCasesNameGolden, ("Difference:",fs.difference(testCasesName)));
         
    def testTestCaseAllDict(self):
        
        allTestCases = TestLab_upload.AllTestCases(self.testSet.testSetInstance, self.testSetName);
        allTestCases.getAllTestCases();
        
        testCasesName = allTestCases.testCaseAllDict.keys();
#        print "testCasesName:", testCasesName;
        testCasesNameGolden = [u'[1]Flight Reservation', u'[1]Itinerary Page', u'[1]Profiling', u'[1]Site_Stability']
        
        fs = frozenset(testCasesNameGolden);
        self.assertEqual(testCasesName, testCasesNameGolden, ("Difference:",fs.difference(testCasesName)));
        
        
        
class TestTestLabUpload_TestCase(unittest.TestCase):


    def setUp(self):
        self.qcConnect = qc_connect_disconnect.QC_connect(server, userName, password, domainName,projectName);
        for result in self.qcConnect.connect():
            print result;
        self.qc = self.qcConnect.qc;
        
        
        self.TSdir = r'Root\Mercury Tours (HTML Edition)\Version 1.0\Functionality And UI'
        self.TSname = r'Mercury Tours Sanity'
        
        self.testLabUpload = TestLab_upload.TestLabUpload(self.qc);
                
        self.testSet = TestLab_upload.TestSet(self.testLabUpload, self.TSdir, self.TSname, mappingFields_TL["TestSet"]); 
        self.testSetName = self.testSet.testSetName 

        self.allTestCases = TestLab_upload.AllTestCases(self.testSet.testSetInstance, self.testSetName);
        self.allTestCases.getAllTestCases();
        
        


    def tearDown(self):
        self.qcConnect.disconnect_QC();


    def testSetFieldValue_1(self):
        '''
        Test getTestCase(), correct TestCase name
        '''
        
        testCaseName = '[1]Flight Reservation'
        testCaseInst = self.allTestCases.testCaseAllDict['[1]Flight Reservation']
        testCase = TestLab_upload.TestCase(testCaseInst, testCaseName, mappingFields_TL['TestCase']);
        
        fieldUserName = 'Test';
        fieldValue = 'ble';
        testCase.setFieldsValue(fieldUserName, fieldValue);
        fieldName = mappingFields_TL['TestCase'].get(fieldUserName).get('FieldQcName')
        text = testCase.testCaseInstance.Field(fieldName);
#        self.assertEqual(text, fieldValue);
        self.assertEqual(text, None);##Field was not saved, there is some restriction
        fieldUserName = 'Test';
        fieldValue = '';
        testCase.setFieldsValue(fieldUserName, fieldValue);
        
        
        fieldUserName = 'Status';
        fieldValue = 'Passed';
        testCase.setFieldsValue(fieldUserName, fieldValue);
        fieldName = mappingFields_TL['TestCase'].get(fieldUserName).get('FieldQcName')
        text = testCase.testCaseInstance.Field(fieldName);
        self.assertEqual(text, fieldValue);
        fieldUserName = 'Status';
        fieldValue = 'No Run';
        testCase.setFieldsValue(fieldUserName, fieldValue);

        

#    def testGetTestCaseName(self):
#        testCase = TestLab_upload.TestCase(self.testCasesInstance, self.testSetName, mappingFields_TL["TestCase"]);
#        self.assertEqual(testCase.testCaseName, u'[1]Welcome Page');

class TestTestLabUpload_AllSteps(unittest.TestCase):


    def setUp(self):
        self.qcConnect = qc_connect_disconnect.QC_connect(server, userName, password, domainName,projectName);
        for result in self.qcConnect.connect():
            print result;
        self.qc = self.qcConnect.qc;
        
        self.TSdir = r'Root\Mercury Tours (HTML Edition)\Version 1.0\Functionality And UI'
        self.TSname = r'Mercury Tours Sanity'
        
        self.testLabUpload = TestLab_upload.TestLabUpload(self.qc);
                
        self.testSet = TestLab_upload.TestSet(self.testLabUpload, self.TSdir, self.TSname, mappingFields_TL["TestSet"]); 
        self.testSetName = self.testSet.testSetName 

        self.allTestCases = TestLab_upload.AllTestCases(self.testSet.testSetInstance, self.testSetName);
        self.allTestCases.getAllTestCases();   
        
        testCaseName = '[1]Flight Reservation'
        testCaseInst = self.allTestCases.testCaseAllDict['[1]Flight Reservation']
        self.testCase = TestLab_upload.TestCase(testCaseInst, testCaseName, mappingFields_TL['TestCase']);
                                                 

    def tearDown(self):
        self.qcConnect.disconnect_QC();


    def testGetAllSteps_1(self):
        '''
        Test getAllSteps(), with Run 
        '''
        AllSteps = TestLab_upload.AllSteps(self.testCase, self.testSetName)
        
        allSteps = AllSteps.getAllSteps();
        stepsName = AllSteps.stepsAllDict.keys();
#        steps = [step.Field("ST_STEP_NAME") for step in allSteps];
        allstepsGolden = [u'Connect to Mercury Tours Site', u'Sign-On to Mercury Tours Application', u'Find Flight', u'Select Flight', u'Book Flight', u'Flight Confirmation', u'Print Confirmation']
        
        fs = frozenset(allstepsGolden);
        self.assertTrue(fs.issubset(stepsName),
                     "Not found Steps in all set of TestCase")
    
    def teststartRun(self):
        
        
        allSteps = TestLab_upload.AllSteps(self.testCase, self.testSetName)
        
        runName = strftime('AutoRun_%Y-%m-%d_%H-%M', localtime())
        run = allSteps.runInstance.Name
        run = run.rsplit('-', 1)[0]
        
        self.assertEqual(runName, run);
        

class TestTestLabUpload_Step(unittest.TestCase):


    def setUp(self):
        self.qcConnect = qc_connect_disconnect.QC_connect(server, userName, password, domainName,projectName);
        for result in self.qcConnect.connect():
            print result;
        self.qc = self.qcConnect.qc;
        
        self.TSdir = r'Root\Mercury Tours (HTML Edition)\Version 1.0\Functionality And UI'
        self.TSname = r'Mercury Tours Sanity'
        
        self.testLabUpload = TestLab_upload.TestLabUpload(self.qc);
                
        self.testSet = TestLab_upload.TestSet(self.testLabUpload, self.TSdir, self.TSname, mappingFields_TL["TestSet"]); 
        self.testSetName = self.testSet.testSetName 

        self.allTestCases = TestLab_upload.AllTestCases(self.testSet.testSetInstance, self.testSetName);
        self.allTestCases.getAllTestCases();   
        
        self.testCaseName = '[1]Flight Reservation'
        testCaseInst = self.allTestCases.testCaseAllDict['[1]Flight Reservation']
        self.testCase = TestLab_upload.TestCase(testCaseInst, self.testCaseName, mappingFields_TL['TestCase']);
        
        self.allSteps = TestLab_upload.AllSteps(self.testCase, self.testSetName)
        self.allSteps.getAllSteps();
        self.stepName = 'Connect to Mercury Tours Site';
        self.step = self.allSteps.stepsAllDict[self.stepName];

        


    def tearDown(self):
        del self.step;
        del self.allSteps;
        
        self.qcConnect.disconnect_QC();


    def testGetStep_1(self):
        '''
        Test getStep(), from correct TestCase
        '''
        
        step = TestLab_upload.Step(self.step, 
                                     self.testSetName, 
                                     self.testCaseName, 
                                     self.stepName,
                                     mappingFields_TL["Step"]);
        
        fieldUserName, fieldValue = 'Status', 'Passed';
        step.setFieldsValue(fieldUserName, fieldValue);
        fieldName = mappingFields_TL['Step'].get(fieldUserName).get('FieldQcName');
        text = step.stepInstance.Field(fieldName);
        self.assertEqual(text, fieldValue);
#        fieldUserName = 'Status';
#        fieldValue = 'No Run';
#        step.setFieldsValue(fieldUserName, fieldValue);



class TestTestLabUpload_main(unittest.TestCase):


    def setUp(self):
        self.qcConnect = qc_connect_disconnect.QC_connect(server, userName, password, domainName,projectName);
        for result in self.qcConnect.connect():
            print result;
        self.qc = self.qcConnect.qc;
        
    def tearDown(self):
        self.qcConnect.disconnect_QC();


    def testMain_1(self):
        '''
        Test main() method. During this main() execution folder and the CSV result file should be created with upload result 
        '''
        filename = r"D:\Eclipse_workspace\QC_gui\Result\Download_10-03-2012_19-40-58\Download_10-03-2012_19-40-58.xls"
        main = TestLab_upload.MainExecution(cmd=True,
                                            qc=self.qc, 
                                            filename=filename, 
                                            mappingFields=mappingFields_TL);


class TestTestLabOpenFile(unittest.TestCase):
    '''
    Check how the CSV file is responding for reading headers and lines
    '''


    
    def setUp(self):
        unittest.TestCase.setUp(self)
        
        self.filename = r"D:\Eclipse_workspace\QC_gui\Result\Download_10-03-2012_19-40-58\Download_10-03-2012_19-40-58.xls"
#        self.filename = r'D:\Eclipse_workspace\QC_gui\src\Result\Download_11-03-2012_08-46-18\Download_11-03-2012_08-46-18.xls'
        if not  os.path.exists(self.filename):
            self.fail("Files does not exists")

    
        
        
    def tearDown(self):
        unittest.TestCase.tearDown(self)
        
    def testGetColumnsFromFile(self):
        '''
        Check if getting column names from CSV file,  is working good
        '''
        goldenMappingField ={'TestCase': {
                      'Planned Language': {'FieldOrder': 27}, 
                      'Plan: Template': {'FieldOrder': 12}, 
                      'Plan: Subject': {'FieldOrder': 13}, 
                      'Iterations': {'FieldOrder': 39}, 
                      'Plan: Modified': {'FieldOrder': 40}, 
                      'Test Version': {'FieldOrder': 16}, 
                      'Planned Browser': {'FieldOrder': 19}, 
                      'Modified': {'FieldOrder': 17}, 
                      'Responsible Tester': {'FieldOrder': 20}, 
                      'Planned Host Name': {'FieldOrder': 21}, 
                      'Planned Exec Time': {'FieldOrder': 22}, 
                      'Time': {'FieldOrder': 15}, 
                      'Test': {'FieldOrder': 24}, 
                      'Plan: Type': {'FieldOrder': 29}, 
                      'Plan: Creation Date': {'FieldOrder': 18}, 
                      'Plan: Test': {'FieldOrder': 25}, 
                      'Plan: Test Name': {'FieldOrder': 26}, 
                    'Status': {'FieldOrder': 11}, 
                    'Plan: Reviewed': {'FieldOrder': 28}, 
                    'Plan: Description': {'FieldOrder': 30}, 
                    'Tester': {'FieldOrder': 31}, 
                    'Plan: Level': {'FieldOrder': 32}, 
                    'Plan: Execution Status': {'FieldOrder': 33}, 
                    'Exec Date': {'FieldOrder': 34}, 
                    'Plan: Status': {'FieldOrder': 35}, 
                    'Plan: Path': {'FieldOrder': 36}, 
                    'Plan: Estimated DevTime': {'FieldOrder': 37}, 
                    'Plan: Reviewer': {'FieldOrder': 38}, 
                    'Plan: Designer': {'FieldOrder': 14}, 
                    'Plan: Priority': {'FieldOrder': 23}, 
                    'Planned Exec Date': {'FieldOrder': 41}, 
                    'Attachment': {'FieldOrder': 42}
                    }, 
        'Step': {
                 'Status': {'FieldOrder': 43}, 
                 '': {'FieldOrder': 52}, 
                'Source Test': {'FieldOrder': 44}, 
                'Actual': {'FieldOrder': 45}, 
                'Description': {'FieldOrder': 46}, 
                'Attachment': {'FieldOrder': 51}, 
                'Expected': {'FieldOrder': 47}, 
                'Exec Date': {'FieldOrder': 48}, 
                'Step Name': {'FieldOrder': 49}, 
                'Exec Time': {'FieldOrder': 50}
                        }, 
        'TestSet': {
                    'Status': {'FieldOrder': 0}, 
                    'Close Date': {'FieldOrder': 1}, 
                    'Test Set': {'FieldOrder': 6}, 
                    'Description': {'FieldOrder': 3}, 
                    'Modified': {'FieldOrder': 7}, 
                    'ITG Request Id': {'FieldOrder': 8}, 
                    'Open Date': {'FieldOrder': 9}, 
                    'Dupa': {'FieldOrder': 2}, 
                    'Attachment': {'FieldOrder': 10}, 
                    'Test Set Folder': {'FieldOrder': 4}, 
                    'test': {'FieldOrder': 5}}
         };
        
        
        readCsv = csvOperation.ReadCSV(self.filename);
        mappingFields = readCsv.makeMappingFields();
        fs = frozenset(goldenMappingField);
        self.assertEqual(mappingFields, goldenMappingField, ("Difference:",fs.difference(mappingFields)));
        
         
        
        
    
        


if __name__ == "__main__":
    suite = unittest.TestSuite()
    
    suite.addTest(unittest.makeSuite(TestTestLabUpload_TestSet));  ##Run all tests
    suite.addTest(unittest.makeSuite(TestTestLabUpload_AllTestCases));  ##Run all tests
    suite.addTest(unittest.makeSuite(TestTestLabUpload_TestCase));  ##Run all tests
    suite.addTest(unittest.makeSuite(TestTestLabUpload_AllSteps));  ##Run all tests
    suite.addTest(unittest.makeSuite(TestTestLabUpload_Step));  ##Run all tests
    suite.addTest(unittest.makeSuite(TestTestLabUpload_main));  ##Run all tests

    suite.addTest(unittest.makeSuite(TestTestLabOpenFile));  ##Run all tests


    unittest.TextTestRunner(verbosity=2).run(suite)
