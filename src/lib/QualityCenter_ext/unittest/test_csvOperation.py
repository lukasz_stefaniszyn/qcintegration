'''
Created on 14-01-2012

@author: qc
'''
import unittest
from PyQt4 import QtCore, QtGui
from time import sleep
import sys
import os;
from time import strftime, localtime, sleep;
from functools import partial

import threading, pythoncom;
import win32com.client;
from win32com.client import DispatchWithEvents, constants, Constants



from lib.QualityCenter_ext.lib.csvOperation import *

mappingFields_TL_1 ={
                   'Step': {
                            'Attachment': {
                                           'FieldIsRequired': 'False',
                                           'FieldOrder': '2',
                                           'FieldQcName': 'ATTACHMENT',
                                           'FieldType': 'char'},
                            'Status': {
                                       'FieldIsRequired': 'False',
                                       'FieldOrder': '0',
                                       'FieldQcName': u'ST_STATUS',
                                       'FieldType': u'char'},
                            'Step Name': {
                                          'FieldIsRequired': 'False',
                                          'FieldOrder': '1',
                                          'FieldQcName': u'ST_STEP_NAME',
                                          'FieldType': u'char'}},
                   'TestCase': {
                                'Attachment': {
                                               'FieldIsRequired': 'False',
                                               'FieldOrder': '7',
                                               'FieldQcName': 'ATTACHMENT',
                                               'FieldType': 'char'},
                                'Plan: Execution Status': {
                                                           'FieldIsRequired': 'False',
                                                           'FieldOrder': '6',
                                                           'FieldQcName': u'TS_EXEC_STATUS',
                                                           'FieldType': u'char'},
                                'Plan: Path': {
                                               'FieldIsRequired': 'False',
                                               'FieldOrder': '5',
                                               'FieldQcName': u'TS_PATH',
                                               'FieldType': u'char'},
                                'Plan: Subject': {
                                                  'FieldIsRequired': 'False',
                                                  'FieldOrder': '4',
                                                  'FieldQcName': u'TS_SUBJECT',
                                                  'FieldType': u'number'},
                                'Responsible Tester': {
                                                       'FieldIsRequired': 'False',
                                                       'FieldOrder': '3',
                                                       'FieldQcName': u'TC_TESTER_NAME',
                                                       'FieldType': u'char'},
                                'Test Case Description': {
                                                          'FieldIsRequired': 'False',
                                                          'FieldOrder': '2',
                                                          'FieldQcName': u'TS_DESCRIPTION',
                                                          'FieldType': u'char',},
                                'Test': {
                                         'FieldIsRequired': 'False',
                                           'FieldName': 'Test',
                                           'FieldOrder': '1',
                                           'FieldQcName': u'TS_USER_05',
                                           'FieldType': u'char',
                                           'FieldUserName': ''},
                                'Status': {
                                           'FieldIsRequired': 'False',
                                           'FieldOrder': '0',
                                           'FieldQcName': u'TC_STATUS',
                                           'FieldType': u'char'}},
                   'TestSet': {
                               'Test': {'FieldIsRequired': 'False',
                                       'FieldName': 'Test',
                                       'FieldOrder': '3',
                                       'FieldQcName': u'CY_USER_01',
                                       'FieldType': u'char'},
                               'Status': {
                                          'FieldIsRequired': 'False',
                                          'FieldOrder': '0',
                                          'FieldQcName': u'CY_STATUS',
                                          'FieldType': u'char'},
                               'Test Set': {
                                            'FieldIsRequired': 'False',
                                            'FieldOrder': '2',
                                            'FieldQcName': u'CY_CYCLE',
                                            'FieldType': u'char'},
                               'Test Set Folder': {
                                                   'FieldIsRequired': 'False',
                                                   'FieldOrder': '1',
                                                   'FieldQcName': u'CY_FOLDER_ID',
                                                   'FieldType': u'number'}}}; 




mappingFields_TP = {
                    'Step': {
                             'Attachment': {
                                            'FieldIsRequired': 'False',
                                            'FieldOrder': '3',
                                            'FieldQcName': 'ATTACHMENT',
                                            'FieldType': 'char'
                                            },
                             'Description': {
                                             'FieldIsRequired': 'False',
                                             'FieldOrder': '1',
                                             'FieldQcName': u'DS_DESCRIPTION',
                                             'FieldType': u'char'
                                             },
                             'Expected Result': {
                                                 'FieldIsRequired': 'False',
                                                 'FieldOrder': '2',
                                                 'FieldQcName': u'DS_EXPECTED',
                                                 'FieldType': u'char'
                                                 },
                            'Step Name': {
                                          'FieldIsRequired': 'False',
                                          'FieldOrder': '0',
                                          'FieldQcName': u'DS_STEP_NAME',
                                          'FieldType': u'char'
                                          }
                             },
                    'TestCase': {
                                 'Path': {
                                          'FieldIsRequired': 'False',
                                          'FieldOrder': '1',
                                          'FieldQcName': u'TS_PATH',
                                          'FieldType': u'char'},
                                 'Status': {
                                            'FieldIsRequired': 'False',
                                            'FieldOrder': '2',
                                            'FieldQcName': u'TS_STATUS',
                                            'FieldType': u'char'
                                            },
                                 'Subject': {
                                             'FieldIsRequired': 'False',
                                             'FieldOrder': '3',
                                             'FieldQcName': u'TS_SUBJECT',
                                             'FieldType': u'number'
                                             },
                                'Test Name': {
                                              'FieldIsRequired': 'True',
                                              'FieldOrder': '0',
                                              'FieldQcName': u'TS_NAME',
                                              'FieldType': u'char'
                                              }
                                 },
                    'TestSet': {
                                'Attachment': {
                                               'FieldIsRequired': 'False',
                                               'FieldOrder': '3',
                                               'FieldQcName': 'ATTACHMENT',
                                               'FieldType': 'char'
                                               },
                                'Status': {
                                           'FieldIsRequired': 'False',
                                           'FieldOrder': '2',
                                           'FieldQcName': u'CY_STATUS',
                                           'FieldType': u'char'
                                           },
                                'Test Set': {
                                             'FieldIsRequired': 'False',
                                             'FieldOrder': '0',
                                             'FieldQcName': u'CY_CYCLE',
                                             'FieldType': u'char'
                                             },
                                'Test Set Folder': {
                                                    'FieldIsRequired': 'False',
                                                    'FieldOrder': '1',
                                                    'FieldQcName': u'CY_FOLDER_ID',
                                                    'FieldType': u'number'
                                                    }
                                }
                    }; 

#Define event handler
class EventManager(object):

    def OnWorkbookBeforeSave(self, Wb, SaveAsUI, Cancel):
        print ("Workbook is about to Save");
        Cancel = True;
        return Cancel;
    
    def OnBeforeSave(self, SaveAsUI, Cancel):
        print ("is about to Save");
        Cancel = True;
        return Cancel;


class ExcelApp(object):
    '''
    Excel operation
    '''    
    #------------------------------------------------------------------------------ 
    def __init__(self):
        try:
            self.excel_object = win32com.client.DispatchWithEvents("Excel.Application", EventManager);
        except pywintypes.com_error:
            print "ERR: Unable to run Microsoft Excel program. \n Please install first Microsoft Excel"
            self.excel_object = None;
        self.excel_object.Visible= False;
        self.__workbook = self.excel_object.Workbooks
        
        
    def __del__(self):
        del self.__excel_object;
        
    #------------------------------------------------------------------------------
    def __get_excel_object(self):
        return self.__excel_object;
    def __set_excel_object(self, excel_object):
        self.__excel_object = excel_object;
    def __del_excel_object(self):
        del self.__excel_object;
    excel_object = property(__get_excel_object, 
                            __set_excel_object, 
                            __del_excel_object, 
                            "This is an Excel Object Application");
                            
    def __get_workbook(self):
        return self.__workbook;
    workbook = property(__get_workbook);
    #---------------------------------------
    #------------------------------------------------------------------------------
        
    def openFile(self, path):
        path = path.replace("/", "\\");
        print "Open Excel file:", path;
        self.excelFile = self.workbook.Open(path, ReadOnly=False);
        
    def saveAsFile(self, filename):
        self.excelFile.SaveAs(Filename=filename, FileFormat=39, CreateBackup=False);
        self.excelFile.Close(True)


class TestWriteCSVfile_init(unittest.TestCase):


    def setUp(self):
        self.mainDir = os.getcwd();
        filename = strftime('Download_%d-%m-%Y_%H-%M-%S.xls', localtime());
        self.csvFilename = self.mainDir + "\\" + filename;
        sleep(1);
        
    def tearDown(self):
        try:os.remove(r"%s"%(str(self.csvFilename)));
        except: pass;


    def test_initExistingFile(self):
        
        f_download = open(self.csvFilename, 'wb');
        f_download.flush();
        f_download.close();
        
        callableFunction = partial(WriteCSV, self.csvFilename, mappingFields_TL_1);
        self.assertRaises(FileOpenException, callableFunction);
        
#    def test_initExistingFile_2(self):
#
#        f_download = open(self.csvFilename, 'wb');
#        f_download.flush();
#        f_download.close();
#
#        
#        excel = ExcelApp()
#        excel.openFile(self.csvFilename);
#        excel.saveAsFile(self.csvFilename)
#        del excel 
#        
#        writeCsv = WriteCSV(self.mainDir, mappingFields_TL);
#        callableFunction = partial(writeCsv.setup, self.csvFilename);
#        self.assertRaises(FileOpenException, callableFunction);
#        
#        print "writeCsv.csvFilename:", writeCsv.csvFilename
#        try:os.remove(r"%s"%(str(writeCsv.csvFilename)));
#        except: pass;
        
    def test_fieldname_2(self):
        
        writeCsv = WriteCSV(self.csvFilename, mappingFields_TL_1);
        
        mainheaderfieldnames, mainfieldnames, fieldnames, mappingFields_copy = writeCsv.fieldnames(mappingFields_TL_1);

        fieldnamesGolden = ['Status', 'Test Set Folder', 'Test Set', 'Step Name', 'Attachment']
        
        mainfieldnamesGolden = ['TestSet', '', '', '', '', '', '', '', '', '', '', 'TestCase', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Step', '', '', '', '', '', '', '', '']
        ##Check if created fieldnames is the same as golden fieldnames         
        fs = frozenset(fieldnamesGolden);
        print "fieldnames:", fieldnames
        self.assertTrue(fs.issubset(fieldnames), ("Difference:",fs.difference(fieldnames)));

        ##Check if created mainfieldnames is the same as golden mainfieldnames         
        fs = frozenset(mainfieldnamesGolden);
        self.assertTrue(fs.issubset(mainfieldnames), ("Difference:",fs.difference(mainfieldnames)));

        mappingFieldsGolden = {'TestCase': {
                                            'Plan: Path': {'FieldIsRequired': 'False','FieldOrder': '9','FieldQcName': u'TS_PATH','FieldType': u'char'},
                                            'Plan: Subject': {'FieldIsRequired': 'False','FieldOrder': '8','FieldQcName': u'TS_SUBJECT','FieldType': u'number'},
                                            }, 
                               'Step': {
                                        'Status': {'FieldIsRequired': 'False','FieldOrder': '12','FieldQcName': u'ST_STATUS','FieldType': u'char'},
                                        'Step Name': {'FieldIsRequired': 'False','FieldOrder': '13','FieldQcName': u'ST_STEP_NAME','FieldType': u'char'}
                                        }, 
                               'TestSet': {
                                           'Status': {'FieldIsRequired': 'False', 'FieldOrder': '0', 'FieldQcName': u'CY_STATUS', 'FieldType': u'char'}, 
                                           'Test Set': {'FieldIsRequired': 'False', 'FieldOrder': '2', 'FieldQcName': u'CY_CYCLE', 'FieldType': u'char'}, 
                                           }
                               }
        
        
                            
        
        ##Check if field value of number/order was changed
        self.assertEqual(mappingFieldsGolden['TestSet'].get('Status'),
                         mappingFields_copy['TestSet'].get('Status'));
        self.assertEqual(mappingFieldsGolden['TestSet'].get('Test Set'),
                         mappingFields_copy['TestSet'].get('Test Set'));
        self.assertEqual(mappingFieldsGolden['TestCase'].get('Plan: Path'),
                         mappingFields_copy['TestCase'].get('Plan: Path'));
        self.assertEqual(mappingFieldsGolden['TestCase'].get('Plan: Subject'),
                         mappingFields_copy['TestCase'].get('Plan: Subject'));
        self.assertEqual(mappingFieldsGolden['Step'].get('Status'),
                         mappingFields_copy['Step'].get('Status'));
        self.assertEqual(mappingFieldsGolden['Step'].get('Step Name'),
                         mappingFields_copy['Step'].get('Step Name'));
                         
                         
    def testAddRow(self):
        '''
        Test if AddRow is working correct.
        Additionally I have tested ReadCsv
        '''
        writeCsv = WriteCSV(self.csvFilename, mappingFields_TL_1);
        csvFilename = writeCsv.csvFilename;
        print "csvFilename:", csvFilename;
        mainheaderfieldnames, mainfieldnames, fieldnames, mappingFields_copy = writeCsv.fieldnames(mappingFields_TL_1);
        
        writeCsv.row = ['a', 'b', 'c', 'd'];
        row = writeCsv.row 
        writeCsv.addRow();
        del writeCsv;  

        
        ##Read Csv file 
        readerCsv = ReadCSV(csvFilename);
        self.assertEqual(readerCsv.fieldUserNames, fieldnames);
#        print "Line:", readerCsv.getRow()
        for line in readerCsv.getRow():
            self.assertEqual(line, row); 
        del readerCsv 
        
        try: os.remove(str(csvFilename));
        except: pass;
            
        
        
class TestReadCSVfile_init(unittest.TestCase):


    def setUp(self):
        self.mainDir = os.getcwd();
        filename = r'D:\Eclipse_workspace\QC_gui\Result\Download_19-01-2012_21-28-34\Download_19-01-2012_21-28-34.xls'
        self.csvFilename = filename;
        
    def tearDown(self):
#        try:os.remove(r"%s"%(str(self.csvFilename)));
#        except: pass;
        pass
        
    def test_fieldname_1(self):
        '''
        Check how the ReadCSV will react,when header will be missing
        '''
        
#        writeCsv = WriteCSV(self.csvFilename, mappingFields_TL);
        filename = strftime('Download_%d-%m-%Y_%H-%M-%S.xls', localtime());
        csvFilename = self.mainDir + "\\" + filename;
        try:os.remove(str(csvFilename));
        except: pass
        
        ##create blank file
        f_download = open(csvFilename, 'wb');
        f_download.flush();
        f_download.close();
        
        callableFunction = partial(ReadCSV, csvFilename);
        self.assertRaises(FileOpenException, callableFunction);
        
        try:os.remove(r"%s"%(str(csvFilename)));
        except: pass;
    
    def testMappingField(self):
        '''
        Check if mappingFields is created correctly form the given CSV file
        '''
        ##Read Csv file 
        readerCsv = ReadCSV(self.csvFilename);
        
        
        mappingFields_cmp = {'TestCase': {'Planned Language': {'FieldOrder': 27}, 'Plan: Template': {'FieldOrder': 12}, 'Plan: Subject': {'FieldOrder': 13}, 'Iterations': {'FieldOrder': 39}, 'Plan: Modified': {'FieldOrder': 40}, 'Test Version': {'FieldOrder': 16}, 'Planned Browser': {'FieldOrder': 19}, 'Modified': {'FieldOrder': 17}, 'Responsible Tester': {'FieldOrder': 20}, 'Planned Host Name': {'FieldOrder': 21}, 'Planned Exec Time': {'FieldOrder': 22}, 'Time': {'FieldOrder': 15}, 'Test': {'FieldOrder': 24}, 'Plan: Type': {'FieldOrder': 29}, 'Plan: Creation Date': {'FieldOrder': 18}, 'Plan: Test': {'FieldOrder': 25}, 'Plan: Test Name': {'FieldOrder': 26}, 'Status': {'FieldOrder': 11}, 'Plan: Reviewed': {'FieldOrder': 28}, 'Plan: Description': {'FieldOrder': 30}, 'Tester': {'FieldOrder': 31}, 'Plan: Level': {'FieldOrder': 32}, 'Plan: Execution Status': {'FieldOrder': 33}, 'Exec Date': {'FieldOrder': 34}, 'Plan: Status': {'FieldOrder': 35}, 'Plan: Path': {'FieldOrder': 36}, 'Plan: Estimated DevTime': {'FieldOrder': 37}, 'Plan: Reviewer': {'FieldOrder': 38}, 'Plan: Designer': {'FieldOrder': 14}, 'Plan: Priority': {'FieldOrder': 23}, 'Planned Exec Date': {'FieldOrder': 41}, 'Attachment': {'FieldOrder': 42}}, 'Step': {'Status': {'FieldOrder': 43}, '': {'FieldOrder': 52}, 'Source Test': {'FieldOrder': 44}, 'Actual': {'FieldOrder': 45}, 'Description': {'FieldOrder': 46}, 'Attachment': {'FieldOrder': 51}, 'Expected': {'FieldOrder': 47}, 'Exec Date': {'FieldOrder': 48}, 'Step Name': {'FieldOrder': 49}, 'Exec Time': {'FieldOrder': 50}}, 'TestSet': {'Status': {'FieldOrder': 0}, 'Close Date': {'FieldOrder': 1}, 'Test Set': {'FieldOrder': 6}, 'Description': {'FieldOrder': 3}, 'Modified': {'FieldOrder': 7}, 'ITG Request Id': {'FieldOrder': 8}, 'Open Date': {'FieldOrder': 9}, 'Dupa': {'FieldOrder': 2}, 'Attachment': {'FieldOrder': 10}, 'Test Set Folder': {'FieldOrder': 4}, 'test': {'FieldOrder': 5}}}
        
        
        mappingFields = readerCsv.makeMappingFields();
        
        self.assertEqual(mappingFields, mappingFields_cmp)
        
#        fs = frozenset(mappingFields_cmp);
#        self.assertEqual(mappingFields, mappingFields_cmp, ("Difference:",fs.difference(mappingFields)));
        
        
        
#        print mappingFields 
#        for groupName, fieldValue in mappingFields.iteritems():
#            print groupName;
#            for fieldUserName, fieldValues in fieldValue.iteritems():
#                print "\t", fieldUserName, fieldValues;  

        
        

def load_tests(loader, tests, pattern):
    suite = unittest.TestSuite()
#    suite.addTest(unittest.makeSuite(Test));  ##Run all tests
    return suite


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
