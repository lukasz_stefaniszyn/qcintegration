# -*- coding: utf-8 -*-
#!/usr/bin/env python
'''
Created on 2010-06-18

@author: Lukasz Stefaniszyn
'''

from win32com.client import gencache, DispatchWithEvents, constants
import lib.crypt_pass as crypt_pass
import lib.QualityCenter_ext.lib.qc_connect_disconnect as qc_connect_disconnect
import os
import sys
import unittest
from time import strftime, localtime;


gencache.EnsureModule('{F645BD06-E1B4-4E6A-82FB-E97D027FD456}', 0, 1, 0);


sys.path.append(r"D:\Work\svn_checkout\lukasz_repository_Fiona\QualityCenter\src");
import lib.QualityCenter_ext.lib.TestPlan_download as TestPlan_download;



FULLTEST=True;



def start():
    qcOperation = QCOperation();
    qcOperation._qcConnect();
    
    if FULLTEST:
        folderName = r"Subject\G TL1-Wroclaw\LS_Test_Main";
        dirInstance = qcOperation.createTPfolders(folderName)
        testNameList = ["LS_main1", "LS_main2", "LS_main3","LS_main4" ];
        qcOperation.createTPtestcases(dirInstance, testNameList);
        testName = "LS_main1";
        qcOperation.updateTestCase(dirInstance, testName);
        
        folderName = r"Subject\G TL1-Wroclaw\LS_Test_Main\LS_test1";
        dirInstance = qcOperation.createTPfolders(folderName)
        testNameList = ["LS_test1_1", "LS_test1_2", "LS_test1_3"];
        qcOperation.createTPtestcases(dirInstance, testNameList)
        
        folderName = r"Subject\G TL1-Wroclaw\LS_Test_Main\LS_test2";
        dirInstance = qcOperation.createTPfolders(folderName)
        testNameList = ["LS_test2_1", "LS_test2_2", "LS_test2_3"];
        qcOperation.createTPtestcases(dirInstance, testNameList)
    
    return qcOperation; 

def end(qcOperation):
        
    if FULLTEST:
        qcOperation.deleteTPtestcases()
        folderName = "LS_Test_Main";
        qcOperation.deleteTPfolders(folderName)
    del qcOperation.qcInstance;


class QCOperation(object):

    
    def __init__(self): 
        
        self.server = "http://muvmp034.nsn-intra.net/qcbin/"; 
        self.userName="stefaniszyn";
        pass_ = "e8dbf113946b941a4a34b573363674a3c58520";
        self.password = crypt_pass.Heslo(pass_)._Heslo__readHeslo();
        self.domainName="HIT7300";
        self.projectName="hiT73_R43x";
        
        
        self.server = "http://qualitycenter.inside.com/qcbin/"; 
        self.userName="wro50026";
        pass_ = "e8dbf113946b941a504c0889b93aaba9d7c760";
        self.password = crypt_pass.Heslo(pass_)._Heslo__readHeslo();
        self.domainName="BCS_DWDM";
        self.projectName="hiT7500";
        
        #self.qcConnect = None;
    
#------------------------------------------------------------------------------ 
    def __get_qcInstance(self):
        return self.qcConnect;
    def __set_qcInstance(self, qcConnection):
        if qcConnection is not None:
            self.qcConnect = qcConnection;
            self.qc = qcConnection.qc;
            self.tm = self.qc.TreeManager;
            self.root = self.tm.TreeRoot("Subject");
        else:
            print "Unable to set qc connection";
    def __del_qcInstance(self):
        print "Disconnecting"
        self.qcDisconnect();
    qcInstance = property(__get_qcInstance,__set_qcInstance,__del_qcInstance,"QC connection instance");
#------------------------------------------------------------------------------ 
    
    def _qcConnect(self):
        
        qcConnect = qc_connect_disconnect.QC_connect(self.server, 
                                                     self.userName, 
                                                     self.password, 
                                                     self.domainName, 
                                                     self.projectName);
        for connectionstatus, msg in qcConnect.connect():
            print msg 
#            print("Connected to QC");
        
        
        if not connectionstatus:
            print("Not connected to QC server, exiting");
            try: del qcConnect;
            except: pass;
            finally: sys.exit(1);
        
        self.qcInstance = qcConnect
        
    def qcDisconnect(self):
        if self.qcInstance is None:
            return False;
        
        if self.qcInstance.disconnect_QC():
            print "\nDisconnecting from QC server."\
                    "\n"\
                    "\nThank You for using QC_import_export script.\n"\
                    "\t\t\t\n"\
                    "\t\t\t  Lukasz Stefaniszyn\n"\
                    "\n\t\t\t\t\tPowered by Python 2.5";
            return True;
        else:
            print "Unable to to disconnect";
            return False
        

    def list_attachments(self, instance):
        '''Create dictionary of Attachments in Step
        input = instance(Step)
        output = dict{attachment_name:attachment_instance}'''
        
        
        list_attachements = {};
        for attachment_inst in instance.Attachments.NewList(""):
            attachment_name = attachment_inst.Name.split('_', 2)[-1];
            list_attachements[attachment_name] = attachment_inst;
        
        return list_attachements;

    def upload_attachments(self, instance, attachments):   
        '''This will upload attachments into QC database 
        input = inst(instance), str(Attachements)
        output = None;
        example, upload_attachments(TestCase_instance, str(c:\tmp\test1.txt; c:\test2.txt);
        '''
        
        list_attachments = self.list_attachments(instance);
        
#        print "attachments:\"%s\""%attachments;
        for attachment in attachments.split(';'): 
            if attachment == "":
                continue;
            attachment = attachment.strip();
            attachment_fileName = os.path.basename(attachment);
            
    
            ##Check if given attachments are present
            if os.path.exists(attachment):            
                if list_attachments.has_key(attachment_fileName):
                    ##IF the attachment to upload will be the same (in name) as the one already on the QC server, then remove this from server
                    attachment_inst_remove = list_attachments.get(attachment_fileName);
                    instance.Attachments.RemoveItem(attachment_inst_remove.ID);
                
                att = instance.Attachments
                att = att.AddItem(None);
                
                att.FileName = r'%s'%(os.getcwd() +"\\" + attachment);
                ##Upload file to QC;
                att.Type = 1;
                #att.AutoPost = 'True';
#                print "Adding attachment \"%s\" "%attachment
                att.Post();
            else:
                print "INFO: Given attachment \"%s\" does not exists. Skipping add"%attachment;
        return;

    def updateTestCase(self, dirInstance, testName):
        
        testCaseInstList = dirInstance.FindTests(Pattern=testName, MatchCase=True);
        testCaseInst = testCaseInstList[0];
        
        testCaseInst.UnLockObject();
        
        testCaseInst.SetField("TS_DESCRIPTION", "TestCase %s description"%(testName));
        testCaseInst.Post();
        
#        testCase.SetField("TS_RESPONSIBLE", "TestCase %s description"%(testName));
        
        attachments = r"LS_main1_1.txt;LS_main1_2.txt;security-Login_Attempts.vsd;NE3-ONN-S.xml";
        self.upload_attachments(testCaseInst, attachments);
        
        dsf = testCaseInst.DesignStepFactory;
        for stepInstance in dsf.NewList(""):
            self.updateStep(stepInstance);
        testCaseInst.Post();
        testCaseInst.UnLockObject();

        
    def updateStep(self, stepInstance):
        
        stepName = stepInstance.StepName
        stepInstance.SetField("DS_DESCRIPTION", 
                                   "Description for %s"%stepName) #Step description field
        stepInstance.SetField("DS_EXPECTED", 
                                   "Expected for %s"%stepName) #Step expected field
        ##Put attachemnts into this step
        attachments = r"LS_main1_%s_1.txt;LS_main1_%s_2.txt"%(stepName, stepName);
        self.upload_attachments(stepInstance, attachments)
        stepInstance.Post();
        
        
    
    def createTPfolders(self, path):
        ''' Create/find folder in TestPlan: 
        input = str(path)
        output = Folder_instance / False (if Subject is not in given path)
        example: make_TC_directory(path=r"Subject\test\something") will create whole directory Subject\test\something or only this missing folder
        '''
        
        found_folder = None;
        
        try:
            ##Check if the whole folder exists
            found_folder = self.tm.NodeByPath(Path=path);
            
            
            print "Folder \"%s\" is already present"%(unicode(path));
            return found_folder;
        except:
            ##if folder does not exists, then try every level of the given folder. If level does not exissts, then create this level
            folder_target = path.split("\\");
            
            ##start checking which folder does not exists from given path
            for i in range(1, len(folder_target)):
                folder = str('\\'.join([ o for o in folder_target[:i+1] ])); 
#                print "current folder search:", folder;
                try:
                    if ( (i== 0) and (folder_target[i] == 'Root') ):
                        found_folder = self.root;
                    else:
                        found_folder = self.tm.NodeByPath(Path=folder);
                    ##Folder exists, go to the next level of given path
                    continue;
                except:
                    ##Go one level above from given path 
                    parent_folder = str('\\'.join([o for o in folder_target[0:i] ]));
#                    print "Parent folder:", parent_folder;
                    if ( (i== 1) and (folder_target[0] == 'Root') ):
                        parent_folder= self.root;
                    else:    
                        parent_folder = self.tm.NodeByPath(Path=parent_folder);
                    ##Create folder under parent folder
                    print "Adding folder:", folder_target[i];
                    parent_folder.AddNode(NodeName=folder_target[i]);
                    parent_folder.Post();
            found_folder = self.tm.NodeByPath(Path=folder);
            return found_folder;




        
    
    def createTPtestcases(self, dirTestInstance, testNameList):
        
        tf = dirTestInstance.TestFactory;
        for testCase in testNameList:
            try:
                testCaseInst = tf.AddItem(testCase);
                self.createTPsteps(testCaseInst);
                testCaseInst.Post()
            except:
                print "Unittest: TestCase already present:", testCase;
    
    def createTPsteps(self, testCaseInst):
        dsf = testCaseInst.DesignStepFactory;
        stepList = ["Step1", "Step2", "Step3"]
        for step in stepList:
            stepInst = dsf.AddItem(step);
    
    def deleteTPfolders(self, folderName):
        dirWroclaw = self.tm.NodeByPath(r"Subject\G TL1-Wroclaw");
        dirWroclaw.RemoveNode(folderName);
        
    def deleteTPtestcases(self):
        path = r"Subject\G TL1-Wroclaw\LS_Test_Main"
        dirTestInstance= self.tm.NodeByPath(Path=path)
        
        tf = dirTestInstance.TestFactory;
#        for testCase in testNameList:
#            item = self.dirTestInstance.FindTests(Pattern=testCase, MatchCase=True)
#            print item[0].Name
            #tf.RemoveItem(item[0]);
            
            
        allTests = dirTestInstance.FindTests("")
        for testCase in allTests:
            #print testCase.Name;
            tf.RemoveItem(testCase);
            
       
       
       
            
class TestAllTestCases(unittest.TestCase):

    def __init__(self, testname, testPlanDownload):
#        unittest.TestCase.__init__(self)
        super(TestAllTestCases, self).__init__(testname)
        self.qc = testPlanDownload.qc;
        self.tm = testPlanDownload.tm;
        self.root = testPlanDownload.root;
        self.testPlanDownload = testPlanDownload
        
        

    def setUp(self):
        self.allTestCases = TestPlan_download.AllTestCases(self.testPlanDownload)


    def tearDown(self):
        pass


    def test_mainGetTestCase(self):
        
        
        testCaseDir = r"Subject\G TL1-Wroclaw\LS_Test_Main\LS_test1";
        testCaseName = r"LS_main1";
        testCasesInst = self.allTestCases.mainGetTestCase(testCaseDir, testCaseName);
        if testCasesInst != None:
            self.fail("There was not an exitsting TestCases name, should return None ")
        
        
        
        testCaseDir = r"Subject\G TL1-Wroclaw\LS_Test_Main\LS_test1";
        testCaseName = r"LS_test1_3";
        testCasesInst = self.allTestCases.mainGetTestCase(testCaseDir, testCaseName);
        self.assertEqual(testCasesInst[0].Name , "LS_test1_3")
        
        
        
        testCaseDir = r"Subject\G TL1-Wroclaw\LS_Test_Main\LS_test1";
        testCaseName = None;
        testCasesInst = self.allTestCases.mainGetTestCase(testCaseDir, testCaseName);
        testCasesInstNames = [testCase.Name for testCase in testCasesInst];
        compateTestCaseNames = ["LS_test1_1", "LS_test1_2", "LS_test1_3"]
        self.assertEqual(testCasesInstNames, compateTestCaseNames);
#        
        
        testCaseDir = r"Subject\G TL1-Wroclaw\LS_Test_Main";
        testCaseName = None;
        testCasesInst = self.allTestCases.mainGetTestCase(testCaseDir, testCaseName);
        testCasesInstNames = [testCase.Name for testCase in testCasesInst];
        compateTestCaseNames = ['LS_main1', 'LS_main2', 'LS_main3', 'LS_main4', 'LS_test1_1', 'LS_test1_2', 'LS_test1_3', 'LS_test2_1', 'LS_test2_2', 'LS_test2_3']
        self.assertEqual(testCasesInstNames, compateTestCaseNames);
        
        
        testCaseDir = r"Subject\G TL1-Wroclaw\LS_Test_Main";
        testCaseName = r"LS_test2_3";
        testCasesInst = self.allTestCases.mainGetTestCase(testCaseDir, testCaseName)
        self.assertEqual(testCasesInst[0].Name , "LS_test2_3")

#        print "Checking in all TestCases. This may take a while"
#        testCaseDir = None
#        testCaseName = None
#        testCasesInst = self.allTestCases.mainGetTestCase(testCaseDir, testCaseName);
#        testCasesInstNames = [testCase.Name for testCase in testCasesInst]
#        compateTestCaseNames = ['LS_main1', 'LS_main2', 'LS_main3', 'LS_test1_1', 'LS_test1_2', 'LS_test1_3', 'LS_test2_1', 'LS_test2_2', 'LS_test2_3']
#        fs = frozenset(compateTestCaseNames);
#        self.assert_( fs.issubset(testCasesInstNames),
#                     "Not found TestCases in all set of TestCases"); #Check if compateTestCaseNames is testCasesInstNames 
            
        #print testCasesInstNames;

class TestTestPlanDownload(unittest.TestCase):
    
    def __init__(self, testname, testPlanDownload):
#        unittest.TestCase.__init__(self)
        super(TestTestPlanDownload, self).__init__(testname)
        self.qc = testPlanDownload.qc;
        self.tm = testPlanDownload.tm;
        self.root = testPlanDownload.root;
        self.testPlanDownload = testPlanDownload
    
    def setUp(self):
        pass;
    def tearDown(self):
        pass;
    
    
    def test_makeTargetDir(self):
        '''
        Check creation of directory .\..\Result with the catalog of the current download in format Download_%d-%m-%Y_%H-%M-%S
        '''
        
        self.testPlanDownload.makeTargetDir();
        self.failUnless(os.path.exists(self.testPlanDownload.path), "");

    

class TestMain(unittest.TestCase):
    def __init__(self, testname, qcConnect):
        super(TestMain, self).__init__(testname)
        self.qcConnect = qcConnect;
        pass
    
    def setUp(self):
        pass;
    def tearDown(self):
        pass;
    
    def test_main(self):
        
        testCaseDir = r"Subject\G TL1-Wroclaw\LS_Test_Main";
        testCaseName = r"LS_main1";
        resultPath = TestPlan_download.main(self.qcConnect, testCaseDir, testCaseName);
        self.failUnless(os.path.exists(resultPath), "");
        
        testCaseDir = r"Subject\G TL1-Wroclaw\LS_Test_Main";
        testCaseName = None;
        resultPath = TestPlan_download.main(self.qcConnect, testCaseDir, testCaseName);
        self.failUnless(os.path.exists(resultPath), "");



class TestTestCase(unittest.TestCase):
    
    
    testCase = None;
    
    def __init__(self, testname, testPlanDownload):
        super(TestTestCase, self).__init__(testname);
        self.qc = testPlanDownload.qc;
        self.tm = testPlanDownload.tm;
        self.root = testPlanDownload.root;
        self.testPlanDownload = testPlanDownload
        
        
    def setUp(self):
        allTestCases = TestPlan_download.AllTestCases(self.testPlanDownload)
        testCaseDir = r"Subject\G TL1-Wroclaw\LS_Test_Main";
        testCaseName = r"LS_main1";
        testCaseInstanceList = allTestCases.mainGetTestCase(testCaseDir, testCaseName)
        testCaseInstance = testCaseInstanceList[0]; 
        self.testCase = TestPlan_download.TestCase(testCaseInstance)
    
    def tearDown(self):
        pass;
        
    
    
    def test_testCaseDescription(self):
        value = self.testCase.descriptionField();
        self.assertEqual(value, u'TestCase LS_main1 description\n\n')
        pass
    
    def test_testCaseName(self):
        value = self.testCase.nameField()
        self.assertEqual(value, u'LS_main1');
        pass;
    
    def test_testCaseResponsible(self):
        value = self.testCase.responsibleField()
        self.assertEqual(value, u'stefaniszyn')
        pass;
    
    def test_getAttachment(self):
        
        attachmentList = self.testCase.getAttachment(self.testPlanDownload.path);
        for att in attachmentList.split(";"):
            if att is not '':
                self.failUnless(os.path.exists(att), "Attachment from TestCase was not save correct: %s"%str(att));
        pass;
    
        
class TestAllSteps(unittest.TestCase):
     
    AllSteps = None;
    
    def __init__(self, testname, testPlanDownload):
        super(TestAllSteps, self).__init__(testname);
        self.qc = testPlanDownload.qc;
        self.tm = testPlanDownload.tm;
        self.root = testPlanDownload.root;
        self.testPlanDownload = testPlanDownload
        
        
    def setUp(self):
        allTestCases = TestPlan_download.AllTestCases(self.testPlanDownload)
        testCaseDir = r"Subject\G TL1-Wroclaw\LS_Test_Main";
        testCaseName = r"LS_main1";
        testCaseInstanceList = allTestCases.mainGetTestCase(testCaseDir, testCaseName)
        testCaseInstance = testCaseInstanceList[0]; 
        self.AllSteps = TestPlan_download.AllSteps(testCaseInstance)
    
    def tearDown(self):
        pass;


    def test_getAllSteps(self):
        allSteps = self.AllSteps.getAllSteps();
        compareStepsName = ['Step1', 'Step2', 'Step3'];
        allStepsName = [step.StepName for step in allSteps];
        self.assertEqual(allStepsName, compareStepsName);
            

class TestStep(unittest.TestCase):
    
    
    Step = None;
    
    def __init__(self, testname, testPlanDownload):
        super(TestStep, self).__init__(testname);
        self.qc = testPlanDownload.qc;
        self.tm = testPlanDownload.tm;
        self.root = testPlanDownload.root;
        self.testPlanDownload = testPlanDownload
        
        
    def setUp(self):
        allTestCases = TestPlan_download.AllTestCases(self.testPlanDownload)
        testCaseDir = r"Subject\G TL1-Wroclaw\LS_Test_Main";
        testCaseName = r"LS_main1";
        testCaseInstanceList = allTestCases.mainGetTestCase(testCaseDir, testCaseName)
        testCaseInstance = testCaseInstanceList[0]; 
        dsf = testCaseInstance.DesignStepFactory;
        allSteps = dsf.NewList("");
        stepInstance = allSteps[0];
        self.Step = TestPlan_download.Step(stepInstance, testCaseInstance.Name);
    
    def tearDown(self):
        pass;
        
    
    def test_nameField(self):
        value = self.Step.nameField();
        self.assertEqual(value, "Step1");
        pass;
    
    def test_descriptionField(self):
        value = self.Step.descriptionField();
        self.assertEqual(value, u'Description for Step1\n\n')
        pass
    
    def test_expectedField(self):
        value = self.Step.expectedField();
        self.assertEqual(value, u'Expected for Step1\n\n')
        pass;
    
    def test_getAttachment(self):
        
        attachmentList = self.Step.getAttachment(self.testPlanDownload.path);
        for att in attachmentList.split(";"):
            if att is not '':
                self.failUnless(os.path.exists(att), "Attachment from TestCase was not save correct: %s"%str(att));
        pass;



if __name__ == "__main__":
    
    FULLTEST=True;
    ##Setup
    qcOperation = start();
    
    
    qc, tmp, root = qcOperation.qc, qcOperation.tm, qcOperation.root;
    
    testPlanDownload = TestPlan_download.TestPlanDownload(qcOperation.qcInstance);
    
    suite = unittest.TestSuite();
    suite.addTest(TestAllTestCases("test_mainGetTestCase", testPlanDownload));
    
    suite.addTest(TestTestPlanDownload("test_makeTargetDir", testPlanDownload));
    
    suite.addTest(TestTestCase("test_testCaseDescription", testPlanDownload))
    suite.addTest(TestTestCase("test_testCaseName", testPlanDownload))
    suite.addTest(TestTestCase("test_testCaseResponsible", testPlanDownload))
    suite.addTest(TestTestCase("test_getAttachment", testPlanDownload))
    
    suite.addTest(TestAllSteps("test_getAllSteps", testPlanDownload))
    
    suite.addTest(TestStep("test_nameField", testPlanDownload))
    suite.addTest(TestStep("test_descriptionField", testPlanDownload))
    suite.addTest(TestStep("test_expectedField", testPlanDownload))
    suite.addTest(TestStep("test_getAttachment", testPlanDownload))
    
    suite.addTest(TestMain("test_main", qcOperation.qcInstance));
    
    
    testResult = unittest.TextTestRunner().run(suite)
    
    
    ##TearDown
    end(qcOperation);
#    del qcOperation.qcInstance;
    
    print "testResult:", testResult.wasSuccessful();
#    print "TestResult.errors", testResult.errors;
#    print "TestResult.failures", testResult.failures;
#    print "TestResult.testsRun", testResult.testsRun;
    
    
    
    
#from win32com.client import gencache, DispatchWithEvents, constants;
#gencache.EnsureModule('{F645BD06-E1B4-4E6A-82FB-E97D027FD456}', 0, 1, 0);
#
#import os;
#os.chdir(r"d:\Work\svn_checkout\lukasz_repository_Fiona\QualityCenter\src\lib");
#import qc_connect_disconnect;
#
#server = "http://muvmp034.nsn-intra.net/qcbin/";
#userName="stefaniszyn";
#domainName="HIT7300";
#projectName="hiT73_R43x";
#password = "Lukste12!";
#
#qcConnect = qc_connect_disconnect.QC_connect(server, userName, password, domainName,projectName);
#qcConnect.connect();
#qc = qcConnect.qc;
#tm = qc.TreeManager;
# 
    
    
    