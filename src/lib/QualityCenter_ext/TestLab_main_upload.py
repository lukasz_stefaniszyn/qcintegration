# -*- coding: utf-8 -*-
#!/usr/bin/env python

'''
Created on 2009-09-30

@author: Lukasz Stefaniszyn
'''
import sys
import os.path


def QC_92():
    
    import lib.qc_connect_disconnect as qc_connect_disconnect 
    import lib.read_arguments as read_arguments
    import lib.TestLab_upload as TestLab_upload;
    #    import getpass
#    server = "http://muvmp034.nsn-intra.net/qcbin/" 
#    UserName="stefaniszyn"
#    #Password = getpass.getpass('Password for %s: '%UserName);
#    
#    DomainName="HIT7300"
#    ProjectName="hiT73_R42x"
#    f_excel_qc_TestLab = 'd:\\Work\\svn_checkout\\lukasz_repository_Fiona\\QC\\qc_import_export_xls_QC92.xls'
#    f_excel_qc_TestLab = 'c:\\tmp\\QC\\qc_import_export_xls_copy.xls'
    
    usage_text = "usage: %prog <options> Excel_filename\n"\
                    "\nExcel_filename - this is excel filename from which Steps, TestCases will be updated to Quality Center\n";
    server, UserName, Password, DomainName, ProjectName, args = read_arguments.arguments(usage_text);
    
    print "\n";
    print "#"*60;
    print "server:", server;
    print "UserName:", UserName;
    print "DomainName:", DomainName;
    print "ProjectName", ProjectName;
    
    f_excel_qc_TestLab = '';
    if len(args)>0:
        f_excel_qc_TestLab = args[0];
    else:
        print "Excel file is not in input arguments\nExiting";
        sys.exit(1);
    
    ##Add working directory to Excel file if only file as input arguments
    f_excel_qc_TestLab = os.path.abspath(f_excel_qc_TestLab); 
    
    
    print "Excel filename:",f_excel_qc_TestLab; 
    print "#"*60;
    print "\n";

    if not TestLab_upload.checkFile(f_excel_qc_TestLab):
        print "\nDisconnecting from QC server."\
                "\n"\
                "\nThank You for using QC_import_export script.\n"\
                "\t\t\t\n"\
                "\t\t\t  Lukasz Stefaniszyn\n"\
                "\n\t\t\t\t\tPowered by Python 2.5";
        sys.exit(1);    
    
    
    qcConnect = qc_connect_disconnect.QC_connect(server, UserName, Password, DomainName, ProjectName);
    if qcConnect.connect():
        print("Connected to QC");
    else:
        print("Not connected to QC server, exiting");
        sys.exit(1);
    
#    return qcConnect.qc;
    if not TestLab_upload.runTestLab_update(f_excel_qc_TestLab, qcConnect):
        print ("\nERR: Fault in reading worksheets from file:"), f_excel_qc_TestLab;
    

    if qcConnect.disconnect_QC():
        print "\nDisconnecting from QC server."\
                "\n"\
                "\nThank You for using QC_import_export script.\n"\
                "\t\t\t\n"\
                "\t\t\t  Lukasz Stefaniszyn\n"\
                "\n\t\t\t\t\tPowered by Python 2.5";
    sys.exit(0);
    
#------------------------------------------------------------------------------ 
    
class flushfile(object):
    def __init__(self, f):
        self.f = f
    def write(self, x):
        self.f.write(x)
        self.f.flush()

def set_flush_for_out():
    sys.stdout = flushfile(sys.stdout)
    sys.stderr = flushfile(sys.stderr)
    
if __name__ == "__main__":
    set_flush_for_out()
    QC_92();
