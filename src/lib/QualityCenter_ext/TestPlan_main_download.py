# -*- coding: utf-8 -*-
#!/usr/bin/env python
'''
Created on 2010-06-23

@author: Lukasz Stefaniszyn
'''

import sys


def QC_92():
    import lib.qc_connect_disconnect as qc_connect_disconnect 
    import lib.read_arguments as read_arguments
    import lib.TestPlan_download as TestPlan_download;  
    #    import getpass
#    server = "http://muvmp034.nsn-intra.net/qcbin/" 
#    UserName="stefaniszyn"
#    #Password = getpass.getpass('Password for %s: '%UserName);
#    
#    DomainName="HIT7300"
#    ProjectName="hiT73_R42x"
   
#    TSdir = r'Root\4.25.10_hit7300_TestLab\D_Control_Management\D07 Control - OpticalLinkControl\D07.01 Basic SNMP Commands';
#    TSname = r'D07.01.01 ONNT';
    
    usage_text = "usage: %prog <options> [TestCases directory] [TestCase name]\n"\
                    "\nTestSets directory - this is full path to the TestCases, empty means all directories under Subject\\.\n"\
                    "\tExample: Subject\\4.25.10_hit7300_TestLab\\D_Control_Management\\D07 Control - OpticalLinkControl\\D07.01 Basic SNMP Commands\n"\
                    "TestCase name - this is TestCase name from TestCase directory\n"\
                    "\tExample: D07.01.01 ONNT\n\n";
    server, UserName, Password, DomainName, ProjectName, args = read_arguments.arguments(usage_text);
    
    
    
    
    print "\n";
    print "#"*60;
    print "server:", server;
    print "UserName:", UserName;
    print "DomainName:", DomainName;
    print "ProjectName", ProjectName;
    
    
    testCaseDir = None;
    testCaseName = None;
    if len(args)>0:
        testCaseDir = args[0];
        try:
            testCaseName = args[1];
        except IndexError:
            print "No TestCase name in input arguments";
    
        
    print "TestCases directory:",testCaseDir
    print "TestCase name:", testCaseName;
    print "#"*60;
    print "\n";
    
    
    qcConnect = qc_connect_disconnect.QC_connect(server, UserName, Password, DomainName, ProjectName);
    if qcConnect.connect()[0]:
        print("Connected to QC");
    else:
        print("Not connected to QC server, exiting");
        sys.exit(1);
    
    resultPath = TestPlan_download.main(qcConnect, testCaseDir, testCaseName);
    print resultPath; 
 
    if qcConnect.disconnect_QC():
        print "\nDisconnecting from QC server."\
                "\n"\
                "\nThank You for using QC_import_export script.\n"\
                "\t\t\t\n"\
                "\t\t\t  Lukasz Stefaniszyn\n"\
                "\n\t\t\t\t\tPowered by Python 2.5";
                
#------------------------------------------------------------------------------ 
                
if __name__ == "__main__":
    
    

    
    QC_92();