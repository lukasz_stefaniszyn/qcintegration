'''
Created on 13-04-2011

@author: Lucas
'''
# This is only needed for Python v2 but is harmless for Python v3.
#import sip
#import PyQt4
#sip.setapi('QString', 2)

from PyQt4 import QtCore, QtGui
import sys

    
class ProgressDialog(QtGui.QProgressDialog):
    
        def __init__(self, max, parent=None):
            super(ProgressDialog, self).__init__(parent)

            self.setCancelButtonText("&Cancel")
            self.max = max;
            self.setRange(0, max)
            self.setWindowTitle("Quality Center progress")

        def updateProgress(self, value):
            
            value = self.value() + value
            self.setValue(value)
            self.setLabelText("Progress %d of %d..." % (value, self.max));
            self.repaint();
            QtGui.qApp.processEvents()
            
            
            if value == self.max:
                self.close();


class MainWindow(QtGui.QMainWindow):
    
    def __init__(self):
        super(MainWindow, self).__init__();
        
        
        self.colorButton = QtGui.QPushButton("Modify...", parent=self)
        self.colorButton.setBackgroundRole(QtGui.QPalette.Button)
        self.colorButton.setMinimumSize(32, 32)
        
        self.colorButton.clicked.connect(self.setProgressDialog)

    def setProgressDialog(self):
        import time
        max = 10;
        progressDialog = ProgressDialog(max);
        i = 0;
        while range(max):
            progressDialog.updateProgress(i)
            i +=1;
            time.sleep(0.5);
            if progressDialog.wasCanceled():
                ##This needs to be interaction between ProgeressDialog and the main application 
#                print "Progress stop";
                break
        progressDialog.close();
        
if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv);
    
    mainWindow = MainWindow()
    mainWindow.show();
    
    app.exec_();