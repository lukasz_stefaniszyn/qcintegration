'''
Created on 2010-03-15

@author: Lucas
'''
from lib.logfile import Logger
from lib.QCTestX.guiLib.ui_QCTestX_TestX_main import Ui_Form as Ui_QcTestX_Main
from lib.QCTestX.qCTestX_tabDownload import MyTreeViewQCgraphSpecificTestResult, \
    MyTreeViewQCgraphSpecificTestTestPlan, \
    MyTreeViewQCgraphSpecificTestChooseTestCase, \
    MyTreeViewQCgraphSpecificTestTestLab, \
    MyTreeViewQCgraphSpecificTestChooseTestSet, GuiQcTestXDownload
from lib.QCTestX.qCTestX_tabUpload import ViewTestXChooseFile, GuiQcTestXUpload
from PyQt4 import QtCore, QtGui, Qt
from globalVariables import GlobalVariables
from lib.QCTestX.qcTestX_download_chooseFields import \
    QcTestXDownloadChooseFields
from lib.QCTestX.qcTestX_executionDialog import ProgressExecutionDialog
from lib.QCTestX.qcTestX_upload_chooseFields import QcTestXUploadChooseFields
from lib.QCgraph.treeQcModelTestLab import getDirTestSet
from lib.QCgraph.treeQcModelTestPlan import getDirTestCase
from lib.QualityCenter_ext.lib import csvOperation
from lib.file_viewer import MainWindow
from lib.progressDialog import ProgressDialog
from lib.runQCgraph import GraphRunTestLab, GraphRunTestPlan
import lib.QualityCenter_ext.lib.TestLab_download as TestLab_download
import lib.QualityCenter_ext.lib.TestLab_upload as TestLab_upload
import lib.QualityCenter_ext.lib.TestPlan_download as TestPlan_download
import lib.QualityCenter_ext.lib.TestPlan_upload as TestPlan_upload
import os








log = Logger(loggername="lib.QCTestX.QCTestX_gui", resetefilelog=False).log;



class UserFieldMissingExecption(Exception):
    def __init__(self, mainWindowUI, currentTabName):
        log.debug(("User started 'Run':%s button, without mapping columns"%currentTabName));
        self.text = "Please choose fields which You would like to use in Upload/Download. Please push button 'Map columns' button";
        log.exception(("UserFieldMissingExecption: ", repr(self.text)))
        QtGui.QMessageBox.information(mainWindowUI.main_QCTestLab, 
                                      "Run %s"%currentTabName,
                                      self.text);
    def __str__(self):
        return self.text;


class QcTestXtabDownload(object):
    
    def __init__(self, parent, objectname=None, dirResult=None):
        self.objectname = objectname
        self.guiQcTestX = GuiQcTestXDownload(parent)

        self.qCTestXTestResult = MyTreeViewQCgraphSpecificTestResult(self.guiQcTestX.treeView_QCTestX_Result, 
                                                                     dir=dirResult);
        if self.objectname == "tab_QCTestLab_Download":
            self.qCTestX = MyTreeViewQCgraphSpecificTestTestLab(self.guiQcTestX.treeView_QCTestX_User)
            self.qCTestXTestChooseTest = MyTreeViewQCgraphSpecificTestChooseTestSet(self.guiQcTestX.treeView_QCTestX_Qc)
        elif self.objectname == "tab_QCTestPlan_Download":
            self.qCTestX = MyTreeViewQCgraphSpecificTestTestPlan(self.guiQcTestX.treeView_QCTestX_User)
            self.qCTestXTestChooseTest = MyTreeViewQCgraphSpecificTestChooseTestCase(self.guiQcTestX.treeView_QCTestX_Qc)
            

        
        ##Specific Test - TestLab tab
        self.connect(self.guiQcTestX.pushButton_QCTestX_Refresh, QtCore.SIGNAL("clicked()"),
             self.__pushButton_Refresh_QCTestX_clicked);
        self.connect(self.guiQcTestX.pushButton_QCTestX_Add, QtCore.SIGNAL("clicked()"),
             self.__pushButton_QCTestXTestLab_Add_clicked);
        self.connect(self.guiQcTestX.pushButton_QCTestX_Remove, QtCore.SIGNAL("clicked()"),
             self.__pushButton_QCTestXTestLab_Remove_clicked);
        self.connect(self.guiQcTestX.pushButton_QCTestX_Clear, QtCore.SIGNAL("clicked()"),
             self.__pushButton_QCTestXTestLab_Clear_clicked);

        ##Specific test - Test Lab tab
    def __pushButton_QCTestXTestLab_Add_clicked(self):
        log.debug("Clicked pushButton_QCgraphSpecificTestTestX_Add");
        index = self.qCTestX.treeView.currentIndex()
        item = index.internalPointer();
        if item is not None:
            self.qCTestXTestChooseTest.addItem(item)
        else:
            log.warning("WRN: User did not selected TestSet/Directory to be moved into Chosen TestSets");
            pass;
    def __pushButton_QCTestXTestLab_Remove_clicked(self):
        log.debug("Clicked pushButton_QCgraphSpecificTestTestX_Remove");
        index = self.qCTestXTestChooseTest.treeView.currentIndex()
        item = index.internalPointer();
        if item is not None:
            self.qCTestXTestChooseTest.removeItem(index)
        else:
            log.warning("WRN: User did not selected TestSet/Directory to be remove from Chosen TestSets");
    def __pushButton_QCTestXTestLab_Clear_clicked(self):
        log.debug("Clicked pushButton_QCgraphSpecificTestTestSet_Clear");
        self.qCTestXTestChooseTest.clearItem();
    
    def __pushButton_Refresh_QCTestX_clicked(self):
        log.debug("Clicked QCGraph Refresh");
        self.qCTestXTestResult.refresh(); ##refresh Result directory
        
        ##Refresh treeView of TestLab/TestPlan catalogs list
        if self.objectname == "tab_QCTestLab_Download":
            dataTestLab = getDirTestSet(self.qc)
            self.qCTestX.updateStarterModel(dataTestLab)    
        elif self.objectname == "tab_QCTestPlan_Download":
            dataTestPlan = getDirTestCase(self.qc)
            self.qCTestX.updateStarterModel(dataTestPlan)
        
        
        
#    
class QcTestXtabUpload(GuiQcTestXUpload, ViewTestXChooseFile):
    
    def __init__(self, parent, dirResult=None):
        self.guiQcTestX = GuiQcTestXUpload(parent);
        self.dirResult = dirResult;
#        guiQcgraphCrossTest = GuiQcgraphCrossTest(tab_CrossTest)
#        self.qCTestXTestChooseFile = ViewTestXChooseFile();
        
        self.guiQcTestX.toolButton_findFile.clicked.connect(self.toolButton_findFile)

    def get_upload_file(self):
        return self.guiQcTestX.lineEdit_uploadFile.text()
    def set_upload_file(self, value):
        lineEdit = self.guiQcTestX.lineEdit_uploadFile
        lineEdit.setText(value);
    def del_upload_file(self):
        lineEdit = self.guiQcTestX.lineEdit_uploadFile
        lineEdit.clear();
    uploadFile = property(get_upload_file, set_upload_file, del_upload_file, "lineEdit_uploadFile")
        
    def toolButton_findFile(self):
        fileName = QtGui.QFileDialog.getOpenFileName(self, self.tr("Open File"),
                                                     self.dirResult,
                                                     self.tr("Text (*.csv *.xls *.xlsx);;All files (*.*)"));
#        if not fileName.isEmpty():
        if fileName != '':
            self.loadFile(fileName)    
   
    def loadFile(self, fileName):
        log.info(("fileName:", fileName));
        file = QtCore.QFile(fileName)
        
        file.open( QtCore.QFile.ReadOnly | QtCore.QFile.Text); 
        if not file:
            QtGui.QMessageBox.warning(self, self.tr("Warning"),
                    self.tr("Cannot read file %1:\n%2.").arg(fileName).arg(file.errorString()))
            return;  
        else:
            self.uploadFile = fileName; 
        file.close();


class GuiQcTestXMain(Ui_QcTestX_Main):
    def __init__(self, main_QCTestX_TestLab):
        Ui_QcTestX_Main.setupUi(self, main_QCTestX_TestLab);


#class SetQCTestX_TestLab(QcTestX_TestLab_tabDownload, QcTestX_TestLab_tabUpload, ProgressDialog):
class SetQCTestX_TestLab(QcTestXtabDownload, QcTestXtabUpload, ProgressDialog, GuiQcTestXMain, QtGui.QWidget):
    '''
    Set values for QCgraph tab
    '''
    
    userFieldsDict =  {'Step': {}, 'TestCase': {}, 'TestSet': {}};
    
    def __init__(self, MainWindow):
        '''
        In QCgraph tab, fill all widgets with basic data
        '''
        
        QtGui.QWidget.__init__(self);
        self.__MainWindow = MainWindow;
        self.__MainWindowUI = MainWindow.ui;
        dirResult= os.path.join(GlobalVariables.DIR_RESULT, 'TestLab');
        
        self.guiQcTestXMain = GuiQcTestXMain(self.mainWindowUI.main_QCTestLab)

        ##Specific Test - Test Plan tab
        objectname = str(self.mainWindowUI.tab_QCTestLab_Download.objectName());
        QcTestXtabDownload.__init__(self, self.mainWindowUI.tab_QCTestLab_Download, objectname, dirResult);
        ##Specific Test - Test Lab tab
        QcTestXtabUpload.__init__(self, self.mainWindowUI.tab_QCTestLab_Upload, dirResult)
        
        
        ##Button Handlers:
        self.guiQcTestXMain.phB_Run_QCTestXMapFields.clicked.connect(self.phB_Run_QCTestLabMapFields_clicked);  
#        self.guiQcTestXMain.checkbox_createCMD.clicked.connect(self.checkbox_Create_CMD_set);
        self.guiQcTestXMain.phB_Run_QCTestX.clicked.connect(self.phB_Run_QCTestLab_clicked);
        
        
        
    #-----------------------------------------------------------------------------        
    def __get_MainWindowUI(self):
        return self.__MainWindowUI;
    mainWindowUI = property(__get_MainWindowUI, None, None, "Instance of MainWindow GUI")
    
    def __get_MainWindow(self):
        return self.__MainWindow;
    mainWindow = property(__get_MainWindow, None, None, "Instance of MainWindow")
    
    def __get_qcConnect(self):
        return self.mainWindow.qcConnect.qc;
    qc = property(__get_qcConnect, None, None, "Instance of QC connection");

    def __get_QCconnection_status(self):
        return self.mainWindow.QCconnection_status;
    QCconnection_status = property(__get_QCconnection_status, None, None, "Information of Quality Center connection status (True, 'Connected')");
    
    def __get__currentTabName(self):
        index = self.mainWindowUI.tabWidget_QCTestLab.currentIndex();
        return self.mainWindowUI.tabWidget_QCTestLab.tabText(index);
    currentTabName = property(__get__currentTabName, None, None, "This is text of current open tab name in the QCgraph");

    #----------------------------------------------------------------------------- 
    ##QCTestX_TestLab_main
    def phB_Run_QCTestLabMapFields_clicked(self):
        log.debug(("Clicked phB_Run_QCTestLabMapFields_clicked"));
        
        
#        main = QtGui.QWidget()
        if self.currentTabName == "Download from QC":
            log.debug("Run QcTestXDownloadChooseFields");
            self.qcTestXDownloadChooseFields = QcTestXDownloadChooseFields(type="TestLab",
                                                                           parent=self.mainWindow, 
                                                                           qcConnect=self.mainWindow.qcConnect
                                                                           );
            
        elif self.currentTabName == "Upload to QC":
            log.debug("Run QcTestXUploadChooseFields");
            self.qcTestXUploadChooseFields = QcTestXUploadChooseFields(type="TestLab",
                                                                           parent=self.mainWindow, 
                                                                           qcConnect=self.mainWindow.qcConnect
                                                                           );
            
            ##Get headers from the CSV file and update treeViewUser
            filename = self.uploadFile;
            readCsv = csvOperation.ReadCSV(filename);
            mappingFields = readCsv.makeMappingFields();
            log.debug(("readCsv.makeMappingFields(): ", mappingFields)); 
            qCTestXUserField = self.qcTestXUploadChooseFields.qCTestXUserField
            qCTestXUserField.updateModel(mappingFields);
            qCTestXUserField.mappingFieldsUserOrg = qCTestXUserField.get_mappingFields();
            self.qcTestXUploadChooseFields.refresh();
            
            
            
        
        
    def checkbox_Create_CMD_set(self):
        log.debug(("Clicked checkbox_Create_CMD_set"));
        pass

    def run_QCTestLab_download(self):
        '''
        This will start TestLab download
        '''
        
        try: 
            userFieldsDict = self.qcTestXDownloadChooseFields.userFieldsDict
        except AttributeError: 
            raise UserFieldMissingExecption(self.mainWindowUI, self.currentTabName)
            return;
        
        if userFieldsDict == {'Step': {}, 'TestCase': {}, 'TestSet': {}}: ##Check if user has chosen fields, which will be used in run operation
            raise UserFieldMissingExecption(self.mainWindowUI, self.currentTabName)
            return;
        else:
            log.debug('User has selected/mapped fields QC_upload/_download');

        ##calculate how many Test items will be executed
        maxItemsNumber = 0;
        testSetsInstList = [];
        model = self.qCTestXTestChooseTest.treeView.model(); ##get items from ChooseTestPlan model
        graphRunTestLab = GraphRunTestLab(); ##create object of TestPlan
        for dir, nondir in model.walk():
            if (dir == (None,None)) and (nondir == []):
                log.warning(("WRN: User did not selected any TestCase/Directory to be run in the Chosen TestCase`s"));
                continue;
            if not nondir:
                testCases = graphRunTestLab.getAllTestSets(dir);
                maxItemsNumber += len(testCases);
                testSetsInstList.append(testCases);
            else:
                testCaseID= graphRunTestLab.getTestSetID(nondir);
                maxItemsNumber += len(testCaseID);
                testSetsInstList.append(testCaseID);
        
        ##Initialize ProgressDialog
        log.debug(("Count items:", maxItemsNumber)); 
        if maxItemsNumber==0:
            text = "Please select any Tests and/or Directory to be run in the given tab"
            QtGui.QMessageBox.information(self.mainWindow, "Quality Center statistic", text);
            self.mainWindow.setCursor(QtCore.Qt.ArrowCursor)
            return;
        
        ##Open Progress Dialog, where user will see execution progress
        progressExecutionDialog = ProgressExecutionDialog(nbr_of_tests=maxItemsNumber, 
                                                               parent=self);

        ##create main instance of QualityCenter execution
        mainExecution = TestLab_download.MainExecution(gui=True, 
                                                        qc=self.qc, 
                                                        mappingFields=userFieldsDict,
                                                        progressExecutionDialog=progressExecutionDialog,  
                                                        );
        
        for folderOfTestCases in testSetsInstList:
            if progressExecutionDialog.cancelStatus: break;##check if user did not pressed Cancel button. If yes, then stop execution
            for testSetName, testSetInst, testSetID in folderOfTestCases:
                if progressExecutionDialog.cancelStatus: 
                    progressExecutionDialog.enableCloseButton(); ##now user can close Widget window
                    break;##check if user did not pressed Cancel button. If yes, then stop execution
                mainExecution.main(testSetInst); ##script is executed on one QulityCenter TestCaseInstace
        progressExecutionDialog.enableCloseButton();##now user can close Widget window
        progressExecutionDialog.disableCancelButton(); ##now everythng was downloaded, so CAncel button should be disabled
                
        
    def run_QCTestLab_upload(self):
        '''
        This will start TestPlan upload
        '''
        try: 
            userFieldsDict = self.qcTestXUploadChooseFields.userFieldsDict
        except AttributeError: 
            raise UserFieldMissingExecption(self.mainWindowUI, self.currentTabName)
            return;
        
        if userFieldsDict == {'Step': {}, 'TestCase': {}, 'TestSet': {}}: ##Check if user has chosen fields, which will be used in run operation
            raise UserFieldMissingExecption(self.mainWindowUI, self.currentTabName)
            return;
        else:
            log.debug('User has selected/mapped fields QC_upload/_download');

        ##Check if given file exists
        filename = self.uploadFile
        if not TestLab_upload.checkFile(filename) or filename=="":
            log.debug(("Given file does not exists or it was not chose: %s", repr(filename)));
            text = "Please choose file which will be used in TestLab upload"
            QtGui.QMessageBox.information(self.mainWindow, "Unknown file", text);
            return;
        
        ##Calculate how many Test items will be executed
        readCsv = csvOperation.ReadCSV(filename);
        maxItemsNumber = readCsv.countTestsInFile(mappingFields=userFieldsDict);
        log.debug(("Number of items which will be executed: ", maxItemsNumber));
        del readCsv 
        
        
        ##Open Progress Dialog, where user will see execution progress
        progressExecutionDialog = ProgressExecutionDialog(nbr_of_tests=maxItemsNumber, 
                                                               parent=self);
#
#        ##create main instance of QualityCenter execution
        mainExecution = TestLab_upload.MainExecution(gui=True, 
                                                     qc=self.qc, 
                                                     filename=filename, 
                                                     mappingFields=userFieldsDict,
                                                     progressExecutionDialog=progressExecutionDialog,  
                                                     );
        progressExecutionDialog.enableCloseButton();
        progressExecutionDialog.disableCancelButton();


    def phB_Run_QCTestLab_clicked(self):
        log.debug(("Clicked phB_Run_QCTestLab_clicked"));
        
        if self.currentTabName == "Download from QC":
            log.debug("Run QCTestLabDownload");
            self.mainWindow.setCursor(QtCore.Qt.WaitCursor)##set waiting cursor on screen
            try: self.run_QCTestLab_download();##start execution TestPlan download
            except Exception, err: log.exception((err));
            self.mainWindow.setCursor(QtCore.Qt.ArrowCursor)##set normal cursor on screen
        elif self.currentTabName == "Upload to QC":
            log.debug("Run QCTestLabUpload");
            self.mainWindow.setCursor(QtCore.Qt.WaitCursor)##set waiting cursor on screen
            try: self.run_QCTestLab_upload();##start execution TestPlan download
            except Exception, err: log.exception((err));
            self.mainWindow.setCursor(QtCore.Qt.ArrowCursor)##set normal cursor on screen
        self.qCTestXTestResult.refresh(); ##refresh Result directory after operation

class GuiQcTestPlanMain(Ui_QcTestX_Main):
    def __init__(self, main_QCTestX_TestPlan):
        Ui_QcTestX_Main.setupUi(self, main_QCTestX_TestPlan);





#class SetQCTestX_TestLab(QcTestX_TestLab_tabDownload, QcTestX_TestLab_tabUpload, ProgressDialog):
class SetQCTestX_TestPlan(QcTestXtabDownload, QcTestXtabUpload, ProgressDialog, GuiQcTestPlanMain, QtGui.QWidget):
    '''
    Set values for QCgraph tab
    '''
    
    def __init__(self, MainWindow):
        '''
        In QCgraph tab, fill all widgets with basic data
        '''
        
        QtGui.QWidget.__init__(self);
        self.__MainWindow = MainWindow;
        self.__MainWindowUI = MainWindow.ui;
        dirResult= os.path.join(GlobalVariables.DIR_RESULT, 'TestPlan');
        
        self.guiQcTestXMain = GuiQcTestPlanMain(self.mainWindowUI.main_QCTestPlan)
        ##Specific Test - Test Plan tab
        objectname = str(self.mainWindowUI.tab_QCTestPlan_Download.objectName());
        QcTestXtabDownload.__init__(self, self.mainWindowUI.tab_QCTestPlan_Download, objectname, dirResult);
        ##Specific Test - Test Lab tab
        QcTestXtabUpload.__init__(self, self.mainWindowUI.tab_QCTestPlan_Upload, dirResult)
        
        
        ##Button Handlers:
        self.guiQcTestXMain.phB_Run_QCTestXMapFields.clicked.connect(self.phB_Run_QCTestPlanMapFields_clicked);  
#        self.guiQcTestXMain.checkbox_createCMD.clicked.connect(self.checkbox_Create_CMD_set);
        self.guiQcTestXMain.phB_Run_QCTestX.clicked.connect(self.phB_Run_QCTestPlan_clicked);

        
    #-----------------------------------------------------------------------------        
    def __get_MainWindowUI(self):
        return self.__MainWindowUI;
    mainWindowUI = property(__get_MainWindowUI, None, None, "Instance of MainWindow GUI")
    
    def __get_MainWindow(self):
        return self.__MainWindow;
    mainWindow = property(__get_MainWindow, None, None, "Instance of MainWindow")
    
    def __get_qcConnect(self):
        return self.mainWindow.qcConnect.qc;
    qc = property(__get_qcConnect, None, None, "Instance of QC connection");

    def __get_QCconnection_status(self):
        return self.mainWindow.QCconnection_status;
    QCconnection_status = property(__get_QCconnection_status, None, None, "Information of Quality Center connection status (True, 'Connected')");
    
    def __get__currentTabName(self):
        index = self.mainWindowUI.tabWidget_QCTestPlan.currentIndex();
        return self.mainWindowUI.tabWidget_QCTestPlan.tabText(index);
    currentTabName = property(__get__currentTabName, None, None, "This is text of current open tab name in the QCgraph");

    #----------------------------------------------------------------------------- 
    ##QCTestX_TestLab_main
    def phB_Run_QCTestPlanMapFields_clicked(self):
        log.debug(("Clicked phB_Run_QCTestPlanMapFields_clicked"));
        
        
#        main = QtGui.QWidget()
        
        if self.currentTabName == "Download from QC":
            log.debug("Run QcTestXDownloadChooseFields");
            self.qcTestXDownloadChooseFields = QcTestXDownloadChooseFields(type="TestPlan",
                                                                           parent=self.mainWindow, 
                                                                           qcConnect=self.mainWindow.qcConnect
                                                                           );

        elif self.currentTabName == "Upload to QC":
            log.debug("Run QcTestXUploadChooseFields");
            filename = self.uploadFile
            if filename=="":
                log.debug(("Given file was not chose: %s", repr(filename)));
                text = "Please choose file which will be used in TestPlan upload"
                QtGui.QMessageBox.information(self.mainWindow, "Unknown file", text);
                return;
            self.qcTestXUploadChooseFields = QcTestXUploadChooseFields(type="TestPlan",
                                                                           parent=self.mainWindow, 
                                                                           qcConnect=self.mainWindow.qcConnect
                                                                           );
            
            ##Get headers from the CSV file and update treeViewUser
            filename = self.uploadFile;
            readCsv = csvOperation.ReadCSV(filename);
            mappingFields = readCsv.makeMappingFields();
            del readCsv;
            log.debug(("readCsv.makeMappingFields(): ", mappingFields)); 
            qCTestXUserField = self.qcTestXUploadChooseFields.qCTestXUserField
            qCTestXUserField.updateModel(mappingFields);
            qCTestXUserField.mappingFieldsUserOrg = qCTestXUserField.get_mappingFields();
    
    def checkbox_Create_CMD_set(self):
        log.debug(("Clicked checkbox_Create_CMD_set"));
        pass

    def run_QCTestPlan_download(self):
        '''
        This will start TestPlan download
        '''
        
        try: 
            userFieldsDict = self.qcTestXDownloadChooseFields.userFieldsDict;
        except AttributeError: 
            raise UserFieldMissingExecption(self.mainWindowUI, self.currentTabName)
            return;
        
        if userFieldsDict == {'Step': {}, 'TestCase': {}, 'TestSet': {}}: ##Check if user has chosen fields, which will be used in run operation
            raise UserFieldMissingExecption(self.mainWindowUI, self.currentTabName)
            return;
        else:
            log.debug('User has selected/mapped fields QC_upload/_download');

        ##calculate how many Test items will be executed
        maxItemsNumber = 0;
        testCasesInstList = [];
        model = self.qCTestXTestChooseTest.treeView.model(); ##get items from ChooseTestPlan model
        graphRunTestPlan = GraphRunTestPlan(); ##create object of TestPlan
        for dir, nondir in model.walk():
            if (dir == (None,None)) and (nondir == []):
                log.warning(("WRN: User did not selected any TestCase/Directory to be run in the Chosen TestCase`s"));
                continue;
            if not nondir:
                testCases = graphRunTestPlan.getAllTestCases(dir);
                maxItemsNumber += len(testCases);
                testCasesInstList.append(testCases);
            else:
                testCaseID= graphRunTestPlan.getTestCaseID(nondir);
                maxItemsNumber += len(testCaseID);
                testCasesInstList.append(testCaseID);
        
        ##Initialize ProgressDialog
        log.debug(("Count items:", maxItemsNumber)); 
        if maxItemsNumber==0:
            text = "Please select any Tests and/or Directory to be run in the given tab"
            QtGui.QMessageBox.information(self.mainWindow, "Quality Center statistic", text);
            self.mainWindow.setCursor(QtCore.Qt.ArrowCursor)
            return;
        
        ##Open Progress Dialog, where user will see execution progress
        progressExecutionDialog = ProgressExecutionDialog(nbr_of_tests=maxItemsNumber, 
                                                               parent=self);

        ##create main instance of QualityCenter execution
        mainExecution = TestPlan_download.MainExecution(gui=True, 
                                                        qc=self.qc, 
                                                        mappingFields=userFieldsDict,
                                                        progressExecutionDialog=progressExecutionDialog,  
                                                        );
        
        for folderOfTestCases in testCasesInstList:
            if progressExecutionDialog.cancelStatus: break;##check if user did not pressed Cancel button. If yes, then stop execution
            for testCaseName, testCaseInst, testCaseID in folderOfTestCases:
                if progressExecutionDialog.cancelStatus: 
                    progressExecutionDialog.enableCloseButton();##now user can close Widget window
                    break;##check if user did not pressed Cancel button. If yes, then stop execution
                mainExecution.main(testCaseInst); ##script is executed on one QulityCenter TestCaseInstace
        progressExecutionDialog.enableCloseButton();##now user can close Widget window
        progressExecutionDialog.disableCancelButton(); ##now everythng was downloaded, so CAncel button should be disabled
        
    def run_QCTestPlan_upload(self):
        '''
        This will start TestPlan upload
        '''
        try: 
            userFieldsDict = self.qcTestXUploadChooseFields.userFieldsDict
        except AttributeError: 
            raise UserFieldMissingExecption(self.mainWindowUI, self.currentTabName)
            return;
        
        if userFieldsDict == {'Step': {}, 'TestCase': {}, 'TestSet': {}}: ##Check if user has chosen fields, which will be used in run operation
            raise UserFieldMissingExecption(self.mainWindowUI, self.currentTabName)
            return;
        else:
            log.debug('User has selected/mapped fields QC_upload/_download');

        ##Check if given file exists
        filename = self.uploadFile
        if not TestPlan_upload.checkFile(filename) or filename=="":
            log.debug(("Given file does not exists or it was not chose: %s", repr(filename)));
            text = "Please choose file which will be used in TestPlan upload"
            QtGui.QMessageBox.information(self.mainWindow, "Unknown file", text);
            return;
        
        ##Calculate how many Test items will be executed
        readCsv = csvOperation.ReadCSV(filename);
        maxItemsNumber = readCsv.countTestsInFile(mappingFields=userFieldsDict);
        log.debug(("Number of items which will be executed: ", maxItemsNumber));
        del readCsv 
        
        
        ##Open Progress Dialog, where user will see execution progress
        progressExecutionDialog = ProgressExecutionDialog(nbr_of_tests=maxItemsNumber, 
                                                               parent=self);



#
#        ##create main instance of QualityCenter execution
        mainExecution = TestPlan_upload.MainExecution(gui=True, 
                                                      qc=self.qc, 
                                                      filename=filename, 
                                                      mappingFields=userFieldsDict,
                                                      progressExecutionDialog=progressExecutionDialog,  
                                                     );
        progressExecutionDialog.enableCloseButton();
        progressExecutionDialog.disableCancelButton();



    def phB_Run_QCTestPlan_clicked(self):
        log.debug(("Clicked phB_Run_QCTestPlan_clicked"));
        
        if self.currentTabName == "Download from QC":
            log.debug("Run QCTestPlanDownload");
            self.mainWindow.setCursor(QtCore.Qt.WaitCursor)##set waiting cursor on screen
            try: self.run_QCTestPlan_download();##start execution TestPlan download
            except Exception, err: log.exception((err));
            self.mainWindow.setCursor(QtCore.Qt.ArrowCursor)##set normal cursor on screen
        elif self.currentTabName == "Upload to QC":
            log.debug("Run QCTestPlanUpload");
            self.mainWindow.setCursor(QtCore.Qt.WaitCursor)##set waiting cursor on screen
            try: self.run_QCTestPlan_upload();##start execution TestPlan download
            except Exception, err: log.exception((err));
            self.mainWindow.setCursor(QtCore.Qt.ArrowCursor)##set normal cursor on screen
        self.qCTestXTestResult.refresh(); ##refresh Result directory after operation
