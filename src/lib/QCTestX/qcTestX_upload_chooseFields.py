'''
Created on 21-11-2011

@author: qc
'''
from PyQt4 import QtCore, QtGui
from copy import deepcopy

from lib.QCTestX.qCTestX_upload import GuiQcTestXUploadField,\
                            MyTreeViewQcTestXUploadQCField,\
                            MyTreeViewQcTestXUploadUserField
from lib.QCTestX.guiLib.ui_QCTestX_main import Ui_Form as Ui_QcGraph_Main
from lib.progressDialog import ProgressDialog
import lib.QCTestX.field_file_operation as  field_file_operation
from lib.QCTestX.qcTestX_download_chooseFields import QcTestXCommonChooseFields as QcTestXCommonChooseFields

from lib.logfile import Logger
log = Logger(loggername="lib.QCTestX.qcTestX_upload_chooseFields", resetefilelog=False).log;


class QcTestXUploadChooseFields(QtGui.QWidget, QcTestXCommonChooseFields):
    '''
    This class collects all information for Gui, when user will start selecting fields to upload
    '''


    def __init__(self, type, parent=None, qcConnect=None):
        '''
        
        @param type: TestLab  or  TestPlan
        @param parent: QtWidget
        @param qcConnect: 
        '''
        
        super(QcTestXUploadChooseFields, self).__init__(parent);
        QcTestXCommonChooseFields.__init__(self, savingType="UL", qcType=type);
        
        
        self.type = type; ##this is type of ChooseField. Different is for TestLab and TestPlan
        self.qcConnect = qcConnect;
        
        self.guiQcTestXField = GuiQcTestXUploadField(parent);
        self.qCTestXQcField = MyTreeViewQcTestXUploadQCField(self.guiQcTestXField.treeView_QCTestX_QcFields)
        self.qCTestXUserField = MyTreeViewQcTestXUploadUserField(self.guiQcTestXField.treeView_QCTestX_UserFields)
        
        self.treeViewQcField = self.qCTestXQcField.treeView
        self.treeViewUserField = self.qCTestXUserField.treeView
        
        
        try: self.pushButton_QCTestX_RefreshFieldFile_clicked(); ##refresh list of items from saved Fields settings
        except: pass
        
        
        
        ##QC Test (Lab/Set) Upload - tab 
        self.connect(self.guiQcTestXField.pushButton_QCTestX_Refresh, QtCore.SIGNAL("clicked()"),
             self.pushButton_QCTestXUpload_Refresh_clicked);
        self.connect(self.guiQcTestXField.pushButton_QCTestX_Add, QtCore.SIGNAL("clicked()"),
             self.pushButton_QCTestXUpload_Add_clicked);
        self.connect(self.guiQcTestXField.pushButton_QCTestX_Remove, QtCore.SIGNAL("clicked()"),
             self.pushButton_QCTestXUpload_Remove_clicked);
        self.connect(self.guiQcTestXField.pushButton_QCTestX_Clear, QtCore.SIGNAL("clicked()"),
             self.pushButton_QCTestXUpload_Clear_clicked);
        self.connect(self.guiQcTestXField.pushButton_QCTestX_Up, QtCore.SIGNAL("clicked()"),
             self.pushButton_QCTestX_Up_clicked);
        self.connect(self.guiQcTestXField.pushButton_QCTestX_Down, QtCore.SIGNAL("clicked()"),
             self.pushButton_QCTestX_Down_clicked);
        self.connect(self.guiQcTestXField.pushButton_QCTestX_SaveFieldFile, QtCore.SIGNAL("clicked()"),
             self.pushButton_QCTestX_SaveFieldFile_clicked);
        self.connect(self.guiQcTestXField.pushButton_QCTestX_OpenFieldFile, QtCore.SIGNAL("clicked()"),
             self.pushButton_QCTestX_OpenFieldFile_clicked);
        self.connect(self.guiQcTestXField.pushButton_QCTestX_RefreshFieldFile, QtCore.SIGNAL("clicked()"),
             self.pushButton_QCTestX_RefreshFieldFile_clicked);
        self.connect(self.guiQcTestXField.pushButton_QCTestX_DeleteFieldFile, QtCore.SIGNAL("clicked()"),
             self.pushButton_QCTestX_DeleteFieldFile_clicked);
        self.guiQcTestXField.listWidget_fieldSavedSettings.itemSelectionChanged.connect(self.update_lineEditSettingName);
#        self.treeViewQcField.selectionChanged.connect(self.clearSelectionTreeViewUserField);
        self.guiQcTestXField.pushButton_NextFields.clicked.connect(self.pushButton_NextField);
        self.guiQcTestXField.pushButton_CancelField.clicked.connect(self.pushButton_CancelField);

        
        
        self.guiQcTestXField.show();
        self.pushButton_QCTestXUpload_Refresh_clicked();
        
        
    def __get_qcConnect(self):
        return self.qcConnect.qc;
    qc = property(__get_qcConnect, None, None, "Instance of QC connection");

    def __get_QCconnection_status(self):
        try: status = self.qcConnect.qc.ProjectConnected;
        except: status = False;
        return status;
    QCconnection_status = property(__get_QCconnection_status, None, None, "Information of Quality Center connection status (True)");    
    
    
    def refresh(self):
        try: self.pushButton_QCTestX_RefreshFieldFile_clicked(); ##refresh list of items from saved Fields settings
        except: pass;
        
        self.pushButton_QCTestXUpload_Refresh_clicked();
    
    def pushButton_NextField(self):
        log.debug("Clicked pushButton_NextFields");
        ##Get userMappingFields, which will be used in the QualityCenter TestLab Upload main file
        self.userFieldsDict = self.qCTestXUserField.get_mappingFields()
        self.guiQcTestXField.close();
        
    def pushButton_CancelField(self):
        log.debug("Clicked pushButton_CancelField");
        self.guiQcTestXField.close();
    
    def pushButton_QCTestXUpload_Clear_clicked(self):
        log.debug("Clicked pushButton_QCTestXUpload_Clear_clicked");
        self.qCTestXUserField.clearItemData();
#        self.pushButton_QCTestXDownload_Refresh_clicked(); ##we will need to update QCFiled columns, because we have removed every item from UserField column
    def pushButton_QCTestXUpload_Refresh_clicked(self):
        log.debug("Clicked pushButton_QCTestXUpload_Refresh_clicked()");
        
        if not self.QCconnection_status:
            log.warning("QCconnection_status is False. Unable to connect to QC server");
            return
        
        self.treeViewQcField.model().reset();
        mapping_fields  = self.get_fields();
        log.debug(("mapping_fields :", mapping_fields));
        if self.type == "TestLab":
            self.qCTestXQcField.updateModelTestLab(mapping_fields);
        elif self.type == "TestPlan":
            self.qCTestXQcField.updateModelTestPlan(mapping_fields);
        self.treeViewUserField.reset();
        
#        self.treeViewUserField.model().reset();
#        self.qCTestXUserField.resetModel();
        
        
    def pushButton_QCTestXUpload_Add_clicked(self):
        log.debug("Clicked pushButton_QCTestXUpload_Add_clicked");
        
        itemQcFieldIndex = self.treeViewQcField.currentIndex() 
        itemUserFieldIndex = self.treeViewUserField.currentIndex()
        itemUserField = itemUserFieldIndex.internalPointer();
        itemQcField = itemQcFieldIndex.internalPointer();
        
        
        
        if (itemQcField is None) or (itemUserField is None):
            log.warning("WRN: User did not selected QC Field to be moved");
            return;
        
        if itemQcField.parentItem.itemData[0] != itemUserField.parentItem.itemData[0]: ##When You want to add QcItem to the UserFields, but the parents are not the same, then should be deny case 
            log.warning("WRN: User started to add QcField not to correct parent UserField");
            return;
        
        if (itemQcField is not None) and (itemUserField is not None):
            ##Add selected item to the Tree of User Fields
            self.qCTestXUserField.editItem(itemUserField, itemUserFieldIndex, 
                                           itemQcField, itemQcFieldIndex);
            #==================================================================
            #For operation reasons it would be better never remove Item from TreeView_QcFields
            #operation reasons - when You move item from QcField to UserField and the open XML setting, then those moved items will not present QcField  
            # ##Remove selected item from the Tree of Qc Fields
            # index = self.treeViewQcField.currentIndex()
            # self.qCTestXQcField.removeItem(index);
            #==================================================================
        else:
            log.warning("WRN: User did not selected QC Field to be moved");
            return;
    def pushButton_QCTestXUpload_Remove_clicked(self):
        log.debug("Clicked pushButton_QCTestXDownload_Remove");
        item = self.treeViewUserField.currentIndex().internalPointer();
        if item is not None:
            #==================================================================
            #For operation reasons it would be better never Add Item to TreeView_QcFields
            #operation reasons - when remove item from QCField is disabled, not neede to run this add
            # ##Add selected item to the Tree of Qc Fields
            # self.qCTestXQcField.addItem(item);
            #==================================================================
            ##Remove selected item from the Tree of User Fields
            index = self.treeViewUserField.currentIndex()
            self.qCTestXUserField.removeItem(index);
        else:
            log.warning("WRN: User did not selected QC Field to be moved");
            pass;



class GuiQcgraphMain(Ui_QcGraph_Main):
    def __init__(self, main_QCgraph):
        Ui_QcGraph_Main.setupUi(self, main_QCgraph);


class SetQcTest(QcTestXUploadChooseFields, GuiQcgraphMain):
    '''
    Main class for all TestLab/TestPlan Upload/Upload
    '''
    
    def __init__(self, MainWindow):
        '''
        In QCTEstX window, fill all widgets with basic data
        '''
        
        self.__MainWindow = MainWindow;
        self.__MainWindowUI = MainWindow.ui;
        
        self.guiQcgraphMain = GuiQcgraphMain(self.mainWindowUI.main_QCgraph)
        
        QcTestXUploadChooseFields.__init__(self, self.mainWindowUI.tab_CrossTest);
        
    
    
    def __get_MainWindowUI(self):
        return self.__MainWindowUI;
    mainWindowUI = property(__get_MainWindowUI, None, None, "Instance of MainWindow GUI")
    
    def __get_MainWindow(self):
        return self.__MainWindow;
    mainWindow = property(__get_MainWindow, None, None, "Instance of MainWindow")
    
    def __get_qcConnect(self):
        return self.mainWindow.qcConnect.qc;
    qc = property(__get_qcConnect, None, None, "Instance of QC connection");

    def __get_QCconnection_status(self):
        return self.mainWindow.QCconnection_status;
    QCconnection_status = property(__get_QCconnection_status, None, None, "Information of Quality Center connection status (True, 'Connected')");    
        
     



if __name__ == "__main__":
    QcTestXUploadChooseFields();
    