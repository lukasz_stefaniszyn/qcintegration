'''
Created on 18-11-2011

@author: qc
'''
import unittest
from PyQt4 import QtCore, QtGui
from time import sleep
import sys
import os

from lib.QCTestX.qcTestX_upload_chooseFields import QcTestXUploadChooseFields

from lib.QCTestX.treeView.treeTestXModel import TreeItem as QcTreeItemUpload
from lib.QCTestX.field_file_operation import OpenFile, SaveFile;

FILENAME = r'D:\Eclipse_workspace\QC_gui\src\testUnit\testxml.xml';


app = QtGui.QApplication(sys.argv);

class TestTreeButtonOperation(unittest.TestCase):
    '''
    Check if in ChooseFields basic buttons are working: Add,Remove,MoveUp, MoveDown, Refresh
    '''


    def setUp(self):
        self.filename = r'D:\Eclipse_workspace\QC_gui\src\testUnit\testxml.xml';
        ##to be sure that there was no other files created before
        try: os.remove(self.filename);
        except: pass;

        self.savingType = "UL"
        self.qcType = "TestLab"
        self.main = QtGui.QWidget()
        self.gui = QcTestXUploadChooseFields(type=self.qcType, parent=self.main)
        
        self.treeViewQcField = self.gui.qCTestXQcField.treeView
        self.treeViewUserField = self.gui.qCTestXUserField.treeView
        
        mappingFieldsQc = {
                           "CYCLE": {'Test Set Name':{'FieldQcName':'TS_NAME', 
                                                          'FieldType':'char', 
                                                          'FieldIsRequired':'True'
                                                          },
                                     'Test Set Directory': {'FieldQcName':'TS_DIR', 
                                                          'FieldType':'char', 
                                                          'FieldIsRequired':'True'
                                                          },
                                     'Test Set Description':{'FieldQcName':'TS_DESC', 
                                                          'FieldType':'char', 
                                                          'FieldIsRequired':'True'
                                                          }, 
                                     },
                          "TESTCYCL": {'Test Case Name': {'FieldQcName':'TC_NAME', 
                                                          'FieldType':'char', 
                                                          'FieldIsRequired':'True'
                                                          },
                                      'Test Case Directory':{'FieldQcName':'TC_DIR', 
                                                          'FieldType':'char', 
                                                          'FieldIsRequired':'True'
                                                          }, 
                                      'Test Case Status': {'FieldQcName':'TC_STATUS', 
                                                          'FieldType':'char', 
                                                          'FieldIsRequired':'True'
                                                          },
                                      },
                          "STEP": {'Step Name': {'FieldQcName':'ST_NAME', 
                                                  'FieldType':'char', 
                                                  'FieldIsRequired':'True'
                                                  },
                                   'Step Description': {'FieldQcName':'ST_DESC', 
                                                          'FieldType':'char', 
                                                          'FieldIsRequired':'True'
                                                          }
                                   }
                          };
        self.gui.qCTestXQcField.updateModelTestLab(mappingFieldsQc);
        
        self.main.show();
        pass;

    def tearDown(self):
        self.treeViewQcField.expandAll();
        self.treeViewUserField.expandAll();
        self.main.repaint();
#        sleep(5);
        self.main.close();
        
        
    def __addUserFieldsTreeView(self):
        self.mappingFieldsUser_1 = {'TestCase': {'Planned Language': {'FieldOrder': '28'}, 
                                       'Plan: Template': {'FieldOrder': '13'}, 
                                       'Plan: Subject': {'FieldOrder': '14'}, 
                                       'Iterations': {'FieldOrder': '40'}, 
                                       'Plan: Modified': {'FieldOrder': '41'}, 
                                       'Test Version': {'FieldOrder': '17'}, 
                                       'Planned Browser': {'FieldOrder': '20'}, 
                                       'Modified': {'FieldOrder': '18'}, 
                                       'Responsible Tester': {'FieldOrder': '21'}, 
                                       'Planned Host Name': {'FieldOrder': '22'}, 
                                       'Planned Exec Time': {'FieldOrder': '23'}, 
                                       'Time': {'FieldOrder': '16'}, 
                                       'Test': {'FieldOrder': '25'}, 
                                       'Plan: Type': {'FieldOrder': '30'}, 
                                       'Plan: Creation Date': {'FieldOrder': '19'}, 
                                       'Plan: Test': {'FieldOrder': '26'}, 
                                       'Plan: Test Name': {'FieldOrder': '27'}, 
                                       'Status': {'FieldOrder': '12'}, 
                                       'Plan: Reviewed': {'FieldOrder': '29'}, 
                                       'Plan: Description': {'FieldOrder': '31'}, 
                                       'Tester': {'FieldOrder': '32'}, 
                                       'Plan: Level': {'FieldOrder': '33'}, 
                                       'Plan: Execution Status': {'FieldOrder': '34'}, 
                                       'Exec Date': {'FieldOrder': '35'}, 
                                       'Plan: Status': {'FieldOrder': '36'}, 
                                       'Plan: Path': {'FieldOrder': '37'}, 
                                       'Plan: Estimated DevTime': {'FieldOrder': '38'}, 
                                       'Plan: Reviewer': {'FieldOrder': '39'}, 
                                       'Plan: Designer': {'FieldOrder': '15'}, 
                                       'Plan: Priority': {'FieldOrder': '24'}, 
                                       'Planned Exec Date': {'FieldOrder': '42'}, 
                                       'Attachment': {'FieldOrder': '43'}}, 
                              'Step': {
                                       'Status': {'FieldOrder': '44'},
                                       'Source Test': {'FieldOrder': '45'}, 
                                       'Actual': {'FieldOrder': '46'}, 
                                       'Description': {'FieldOrder': '47'}, 
                                       'Attachment': {'FieldOrder': '52'}, 
                                       'Expected': {'FieldOrder': '48'}, 
                                       'Exec Date': {'FieldOrder': '49'}, 
                                       'Step Name': {'FieldOrder': '50'}, 
                                       'Exec Time': {'FieldOrder': '51'}}, 
                              'TestSet': {
                                          'Test Set Name': {'FieldOrder': '0'},  
                                          'Status': {'FieldOrder': '1'}, 
                                          'Close Date': {'FieldOrder': '2'}, 
                                          'Test Set': {'FieldOrder': '7'}, 
                                          'Description': {'FieldOrder': '4'}, 
                                          'Modified': {'FieldOrder': '8'}, 
                                          'ITG Request Id': {'FieldOrder': '9'}, 
                                          'Open Date': {'FieldOrder': '10'}, 
                                          'Dupa': {'FieldOrder': '3'}, 
                                          'Attachment': {'FieldOrder': '11'}, 
                                          'Test Set Folder': {'FieldOrder': '5'}, 
                                          'test': {'FieldOrder': '6'}}};
        self.mappingFieldsUser ={
                                 "TestSet": {
                                             'Test Set Name':{'FieldOrder':'0',
                                                       'FieldQcName':'TS_NAME',
                                                       'FieldName':'QC Test Set Name',
                                                       'FieldType':'char',
                                                       'FieldIsRequired':'True'},
                                      'Test Set Directory': {'FieldOrder':'1',
                                                             'FieldQcName':'TS_DIR',
                                                             'FieldName':'QC Test Set Directory',
                                                             'FieldType':'char',
                                                             'FieldIsRequired':'True'},
                                      'Test Set Description': {'FieldOrder':'2',
                                                               'FieldQcName':'TS_DESC',
                                                               'FieldName':'QC Test Set Description',
                                                               'FieldType':'char',
                                                               'FieldIsRequired':'True'}
                                      },
                               "TestCase": {'Test Case Name': {'FieldOrder':'0',
                                                          'FieldQcName':'TC_NAME',
                                                          'FieldName':'QC Test Case Name',
                                                          'FieldType':'char',
                                                          'FieldIsRequired':'True'},
                                            'Test Case Directory': {'FieldOrder':'1',
                                                              'FieldQcName':'TC_DIR',
                                                              'FieldName':'QC Test Case Directory',
                                                              'FieldType':'char',
                                                              'FieldIsRequired':'True'},
                                            'Test Case Status': {'FieldOrder':'2',
                                                              'FieldQcName':'TC_STATUS',
                                                              'FieldName':'QC Test Case Status',
                                                              'FieldType':'char',
                                                              'FieldIsRequired':'True'},
                                        },
                               "Step": {'Step Name': {'FieldOrder':'0',
                                                  'FieldQcName':'ST_NAME',
                                                  'FieldName':'QC Step Name',
                                                  'FieldType':'char',
                                                  'FieldIsRequired':'True'},
                                        'Step Description': {'FieldOrder':'1',
                                                  'FieldQcName':'ST_DESC',
                                                  'FieldName':'QC Step Description',
                                                  'FieldType':'char',
                                                  'FieldIsRequired':'True'}
                                    }
                             };
        self.gui.qCTestXUserField.updateModel(self.mappingFieldsUser_1);

    def __addQcFieldsTreeView(self):
        mapping_fields = {
                            "CYCLE": {'Test Set Name':{'FieldQcName':'TS_NAME',
                                                       'FieldType':'char',
                                                       'FieldIsRequired':'True'},
                                      'Test Set Directory': {'FieldQcName':'TS_DIR',
                                                             'FieldType':'char',
                                                             'FieldIsRequired':'True'},
                                      'Test Set Description': {'FieldQcName':'TS_DESC',
                                                               'FieldType':'char',
                                                               'FieldIsRequired':'True'}
                                      },
                               "TESTCYCL": {'Test Case Name': {'FieldQcName':'TC_NAME',
                                                          'FieldType':'char',
                                                          'FieldIsRequired':'True'},
                                            'Test Case Directory': {'FieldQcName':'TC_DIR',
                                                              'FieldType':'char',
                                                              'FieldIsRequired':'True'},
                                            'Test Case Status': {'FieldQcName':'TC_STATUS',
                                                              'FieldType':'char',
                                                              'FieldIsRequired':'True'},
                                        },
                               "STEP": {'Step Name': {'FieldQcName':'ST_NAME',
                                                  'FieldType':'char',
                                                  'FieldIsRequired':'True'},
                                        'Step Description': {'FieldQcName':'ST_DESC',
                                                  'FieldType':'char',
                                                  'FieldIsRequired':'True'}
                                    }
                             };
        self.gui.qCTestXQcField.updateModelTestLab(mapping_fields)


    def testOpenTree(self):
        self.__addUserFieldsTreeView();
        pass
    
    def testAddButton_1(self):
        '''
        Try to add item from Qc Tree Field to Qc Tree User. 
        User did not selected any item
        '''
        
        ##Clear all selections
        self.treeViewUserField.selectionModel().reset()
        self.treeViewQcField.selectionModel().reset()
        self.treeViewUserField.clearSelection();
        self.treeViewQcField.clearSelection();
        
        status = False
        ##Start add and removing operation on selected item to the Tree of User Fields
        itemQcField = self.treeViewQcField.currentIndex().internalPointer();
        itemUserField = self.treeViewUserField.currentIndex().internalPointer();
        if (itemQcField is not None) and (itemUserField is not None):
            ##Add selected item to the Tree of User Fields
            self.gui.qCTestXUserField.editItem(itemUserField, itemQcField);
        else:
            print("WRN: User did not selected QC Field and/or QC User Field to be moved");
#            log.warning("WRN: User did not selected QC Field to be moved");
            status = True
            pass;
        
        self.assertEqual(True, status)
    
    
    def testAddButton_2(self):
        '''
        Add one item from Qc Tree Field to Qc Tree User
        '''
        self.__addUserFieldsTreeView();
       
        ##Clear all selections
        self.treeViewUserField.selectionModel().reset()
        self.treeViewQcField.selectionModel().reset()
        self.treeViewUserField.clearSelection();
        self.treeViewQcField.clearSelection();
       
        
        ##Set Selection on item in the TreeView
        modelQcField = self.treeViewQcField.model()
        modelUserField = self.treeViewUserField.model();
       
        parentQcIndex = modelQcField.index(0,0, self.treeViewQcField.rootIndex());
        itemQcIndex = modelQcField.index(0,0, parentQcIndex);
        self.treeViewQcField.setCurrentIndex(itemQcIndex)
        
        parentUserIndex = modelUserField.index(0,0, self.treeViewUserField.rootIndex());
        itemUserIndex = modelUserField.index(0,0, parentUserIndex);
        self.treeViewUserField.setCurrentIndex(itemUserIndex);
        
        ##Check if correct item was selected
        self.assertEqual(self.treeViewQcField.currentIndex().internalPointer().itemData, 
                         ('Test Set Name', {'FieldIsRequired': 'True', 'FieldType': 'char', 'FieldQcName': 'TS_NAME'}));
        self.assertEqual(self.treeViewUserField.currentIndex().internalPointer().itemData, 
                         ('Test Set Name', {'FieldOrder': '0'}, '', {'FieldIsRequired': '', 'FieldType': '', 'FieldQcName': ''}));
        ##Check if 'pushButton_QCTestXUpload_Add_clicked()' is adding correct value
        itemQcField = self.treeViewQcField.currentIndex().internalPointer();
        itemUserField = self.treeViewUserField.currentIndex().internalPointer();
        self.gui.pushButton_QCTestXUpload_Add_clicked()
        
        ##Get all data information from item QC Fields
        fieldName, fieldValue = itemQcField.itemData;
        fieldQcName = fieldValue.get('FieldQcName');
        fieldType = fieldValue.get('FieldType');
        fieldIsRequired = fieldValue.get('FieldIsRequired');
        fieldValues = {'FieldQcName': fieldQcName, 
                       'FieldType':fieldType,
                       'FieldIsRequired':fieldIsRequired,
                       'FieldUserName': ''
                       };
        ##create itemData
        dataField = itemUserField.itemData;
        data = (dataField[0], dataField[1], fieldName, fieldValues);
        ##Check
        self.assertEqual(data, itemUserField.itemData) 
        
    def testAddButton_3(self):
        '''
        Add more than one item from Qc Tree Field to Qc Tree User
        '''
        
        self.__addUserFieldsTreeView();
        
        ##Clear all selections
        self.treeViewUserField.selectionModel().reset()
        self.treeViewQcField.selectionModel().reset()
        self.treeViewUserField.clearSelection();
        self.treeViewQcField.clearSelection();
        
        #======================================================================
        # Add one
        #======================================================================
        
        ##Set Selection on item in the TreeView
        modelQcField = self.treeViewQcField.model()
        modelUserField = self.treeViewUserField.model();
        parentQcIndex = modelQcField.index(0,0, self.treeViewQcField.rootIndex());
        itemQcIndex = modelQcField.index(0,0, parentQcIndex);
        self.treeViewQcField.setCurrentIndex(itemQcIndex)
        parentUserIndex = modelUserField.index(0,0, self.treeViewUserField.rootIndex());
        itemUserIndex = modelUserField.index(0,0, parentUserIndex);
        self.treeViewUserField.setCurrentIndex(itemUserIndex);
        
        ##Add item
        itemQcField = self.treeViewQcField.currentIndex().internalPointer();
        itemUserField = self.treeViewUserField.currentIndex().internalPointer();
        self.gui.pushButton_QCTestXUpload_Add_clicked()
        
        
        ##Check if Item was added to the correct place in the Tree User Fields 
        ##Get all data information from item QC Fields
        fieldName, fieldValue = itemQcField.itemData;
        fieldQcName = fieldValue.get('FieldQcName');
        fieldType = fieldValue.get('FieldType');
        fieldIsRequired = fieldValue.get('FieldIsRequired');
        fieldValues = {'FieldQcName': fieldQcName, 
                       'FieldType':fieldType,
                       'FieldIsRequired':fieldIsRequired,
                       'FieldUserName': ''
                       };
        ##create itemData
        dataField = itemUserField.itemData;
        data = (dataField[0], dataField[1], fieldName, fieldValues);
        ##Check
        self.assertEqual(data, itemUserField.itemData) 
        
        #======================================================================
        # Add second item
        #======================================================================
        
        ##Set Selection on item in the TreeView
        itemQcIndex = modelQcField.index(1,0, parentQcIndex);
        self.treeViewQcField.setCurrentIndex(itemQcIndex)
        itemUserIndex = modelUserField.index(1,0, parentUserIndex);
        self.treeViewUserField.setCurrentIndex(itemUserIndex);
        
        ##Add item
        itemQcField = self.treeViewQcField.currentIndex().internalPointer();
        itemUserField = self.treeViewUserField.currentIndex().internalPointer();
        self.gui.pushButton_QCTestXUpload_Add_clicked()
        
        ##Check if Item was added to the correct place in the Tree User Fields 
        ##Get all data information from item QC Fields
        fieldName, fieldValue = itemQcField.itemData;
        fieldQcName = fieldValue.get('FieldQcName');
        fieldType = fieldValue.get('FieldType');
        fieldIsRequired = fieldValue.get('FieldIsRequired');
        fieldValues = {'FieldQcName': fieldQcName, 
                       'FieldType':fieldType,
                       'FieldIsRequired':fieldIsRequired,
                       'FieldUserName': ''
                       };
        ##create itemData
        dataField = itemUserField.itemData;
        data = (dataField[0], dataField[1], fieldName, fieldValues);
        ##Check
        self.assertEqual(data, itemUserField.itemData) 
        
        
        #======================================================================
        # Add third item
        #======================================================================
        
        ##Set Selection on item in the TreeView
        itemQcIndex = modelQcField.index(2,0, parentQcIndex);
        self.treeViewQcField.setCurrentIndex(itemQcIndex);
        itemUserIndex = modelUserField.index(2,0, parentUserIndex);
        self.treeViewUserField.setCurrentIndex(itemUserIndex);
        
        itemQcField = self.treeViewQcField.currentIndex().internalPointer();
        itemUserField = self.treeViewUserField.currentIndex().internalPointer();
        self.gui.pushButton_QCTestXUpload_Add_clicked()
        
        ##Check if Item was added to the correct place in the Tree User Fields 
        ##Get all data information from item QC Fields
        fieldName, fieldValue = itemQcField.itemData;
        fieldQcName = fieldValue.get('FieldQcName');
        fieldType = fieldValue.get('FieldType');
        fieldIsRequired = fieldValue.get('FieldIsRequired');
        fieldValues = {'FieldQcName': fieldQcName, 
                       'FieldType':fieldType,
                       'FieldIsRequired':fieldIsRequired,
                       'FieldUserName': ''
                       };
        ##create itemData
        dataField = itemUserField.itemData;
        data = (dataField[0], dataField[1], fieldName, fieldValues);
        ##Check
        self.assertEqual(data, itemUserField.itemData)  

    def testRemoveButton_1(self):
        '''
        Try to remove item from User Tree Field to Qc Tree User. 
        User did not selected any item
        '''
        
#        self.testAddButton_2();
        self.__addUserFieldsTreeView();
        self.__addQcFieldsTreeView();
        
        ##Clear all selections
        self.treeViewUserField.selectionModel().reset()
        self.treeViewQcField.selectionModel().reset()
        self.treeViewUserField.clearSelection();
        self.treeViewQcField.clearSelection();
        
        
        
        status = False
        ##Start add and removing operation on selected item to the Tree of User Fields
        item = self.treeViewUserField.currentIndex().internalPointer();
        if item is not None:
            ##Remove selected item from the Tree of User Fields
            index = self.treeViewUserField.currentIndex()
            self.gui.qCTestXUserField.removeItem(index);
        else:
            print("WRN: User did not selected QC Field to be moved");
#            log.warning("WRN: User did not selected QC Field to be moved");
            status = True
            pass;
        
        self.assertEqual(True, status)
    
    
    def testRemoveButton_2(self):
        '''
        Remove one item from User Tree Field to Qc Tree User
        '''
        
#        self.testAddButton_3();
        self.__addUserFieldsTreeView();
        self.__addQcFieldsTreeView();
        self.__addFromQCtoUser();
        ##Clear all selections
        self.treeViewUserField.selectionModel().reset()
        self.treeViewQcField.selectionModel().reset()
        self.treeViewUserField.clearSelection();
        self.treeViewQcField.clearSelection();
        
        
        
        ##Set Selection on item in the TreeView
        modelField = self.treeViewUserField.model();
        parentIndex = modelField.index(0,0, self.treeViewUserField.rootIndex());
        itemIndex = modelField.index(0,0, parentIndex);
        self.treeViewUserField.setCurrentIndex(itemIndex)
        
        ##Check if correct item was selected
        self.assertEqual(self.treeViewUserField.currentIndex().internalPointer().itemData, 
                         ('Test Set Name', {'FieldOrder': '0'}, 'Test Set Name', {'FieldIsRequired': 'True', 'FieldUserName': '', 'FieldType': 'char', 'FieldQcName': 'TS_NAME'}));
        
        
        ##Before execute ""pushButton_QCTestXDownload_Remove_clicked" read current QcField TreeView items
        mappingFieldsQcFields = self.gui.qCTestXQcField.get_mappingFields();
        
        
        ##Run "pushButton_QCTestXDownload_Remove_clicked" to check if it is working in the correct way
        self.gui.pushButton_QCTestXUpload_Remove_clicked();
        
        ##Check if Item was added to the correct place in the Tree User Fields 
        mappingFieldsUserFields = self.gui.qCTestXUserField.get_mappingFields();

        mappingFieldsGolden = {
                               'TestCase': {
                                            'Planned Language': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '28'}, 'Plan: Template': {'FieldIsRequired': 'True', 'FieldQcName': 'TC_STATUS', 'FieldName': 'Test Case Status', 'FieldType': 'char', 'FieldOrder': '13', 'FieldUserName': ''},
                                            'Plan: Subject': {'FieldIsRequired': 'True', 'FieldQcName': 'TC_NAME', 'FieldName': 'Test Case Name', 'FieldType': 'char', 'FieldOrder': '14', 'FieldUserName': ''},
                                            'Iterations': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '40'},
                                            'Plan: Modified': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '41'},
                                            'Test Version': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '17'},
                                            'Planned Browser': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '20'},
                                            'Modified': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '18'},
                                            'Responsible Tester': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '21'},
                                            'Planned Host Name': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '22'},
                                            'Planned Exec Time': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '23'},
                                            'Time': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '16'},
                                            'Test': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '25'},
                                            'Plan: Type': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '30'},
                                            'Plan: Creation Date': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '19'},
                                            'Plan: Test': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '26'},
                                            'Plan: Test Name': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '27'},
                                            'Status': {'FieldIsRequired': 'True', 'FieldQcName': 'TC_DIR', 'FieldName': 'Test Case Directory', 'FieldType': 'char', 'FieldOrder': '12', 'FieldUserName': ''},
                                            'Plan: Reviewed': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '29'},
                                            'Plan: Description': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '31'},
                                            'Tester': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '32'},
                                            'Plan: Level': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '33'},
                                            'Plan: Execution Status': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '34'},
                                            'Exec Date': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '35'},
                                            'Plan: Status': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '36'},
                                            'Plan: Path': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '37'},
                                            'Plan: Estimated DevTime': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '38'},
                                            'Plan: Reviewer': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '39'},
                                            'Plan: Designer': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '15'},
                                            'Plan: Priority': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '24'},
                                            'Planned Exec Date': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '42'},
                                            'Attachment': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '43'}},
                                        'Step': {
                                                    'Status': {'FieldIsRequired': 'True', 'FieldQcName': 'ST_DESC', 'FieldName': 'Step Description', 'FieldType': 'char', 'FieldOrder': '44', 'FieldUserName': ''},
                                                    'Source Test': {'FieldIsRequired': 'True', 'FieldQcName': 'ST_NAME', 'FieldName': 'Step Name', 'FieldType': 'char', 'FieldOrder': '45', 'FieldUserName': ''},
                                                    'Actual': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '46'},
                                                    'Description': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '47'},
                                                    'Attachment': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '52'},
                                                    'Expected': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '48'},
                                                    'Exec Date': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '49'},
                                                    'Step Name': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '50'},
                                                    'Exec Time': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '51'}},
                                        'TestSet': {
                                                    'Test Set Name': {'FieldIsRequired': '', 'FieldName': '', 'FieldOrder': '0', 'FieldQcName': '', 'FieldType': ''},
                                                    'Status': {'FieldIsRequired': 'True', 'FieldQcName': 'TS_DESC', 'FieldName': 'Test Set Description', 'FieldType': 'char', 'FieldOrder': '1', 'FieldUserName': ''},
                                                    'Close Date': {'FieldIsRequired': 'True', 'FieldQcName': 'TS_DIR', 'FieldName': 'Test Set Directory', 'FieldType': 'char', 'FieldOrder': '2', 'FieldUserName': ''},
                                                    'Test Set': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '7'},
                                                    'Description': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '4'},
                                                    'Modified': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '8'},
                                                    'ITG Request Id': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '9'},
                                                    'Open Date': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '10'},
                                                    'Dupa': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '3'},
                                                    'Attachment': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '11'},
                                                    'Test Set Folder': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '5'},
                                                    'test': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '6'}
                                                    }
                               }
                
        
        
        
        
        {
                                'TestCase': {
                                    'Test Case Directory': {'FieldIsRequired': 'True', 'FieldQcName': 'TC_DIR', 'FieldType': 'char', 'FieldOrder': '12', 'FieldUserName': 'Status'}, 
                                    'Test Case Status': {'FieldIsRequired': 'True', 'FieldQcName': 'TC_STATUS', 'FieldType': 'char', 'FieldOrder': '13', 'FieldUserName': 'Plan: Template'}, 
                                    'Test Case Name': {'FieldIsRequired': 'True', 'FieldQcName': 'TC_NAME', 'FieldType': 'char', 'FieldOrder': '14', 'FieldUserName': 'Plan: Subject'}}, 
                                'Step': {
                                    'Step Description': {'FieldIsRequired': 'True', 'FieldQcName': 'ST_DESC', 'FieldType': 'char', 'FieldOrder': '44', 'FieldUserName': 'Status'}, 
                                    'Step Name': {'FieldIsRequired': 'True', 'FieldQcName': 'ST_NAME', 'FieldType': 'char', 'FieldOrder': '45', 'FieldUserName': 'Source Test'}}, 
                                'TestSet': {
                                    'Test Set Description': {'FieldIsRequired': 'True', 'FieldQcName': 'TS_DESC', 'FieldType': 'char', 'FieldOrder': '1', 'FieldUserName': 'Status'}, 
                                    'Test Set Directory': {'FieldIsRequired': 'True', 'FieldQcName': 'TS_DIR', 'FieldType': 'char', 'FieldOrder': '2', 'FieldUserName': 'Close Date'}
                                    }
                                }
        

        
        
        
        self.assertEqual(mappingFieldsGolden, mappingFieldsUserFields);

        ##Check if Item is not moved to the QcFileds_TreView, to check if name is not doubled
        mappingFieldsQcFields_cmp = self.gui.qCTestXQcField.get_mappingFields();
        self.assertEqual(mappingFieldsQcFields_cmp, mappingFieldsQcFields);

    def testRemoveButton_3(self):
        '''
        Try remove root catalog item from User Tree Field to Qc Tree User
        '''
        
#        self.testAddButton_3();
        self.__addUserFieldsTreeView();
        self.__addQcFieldsTreeView();
        ##Clear all selections
        self.treeViewUserField.selectionModel().reset()
        self.treeViewQcField.selectionModel().reset()
        self.treeViewUserField.clearSelection();
        self.treeViewQcField.clearSelection();
        
        
        
        ##Set Selection on item in the TreeView, in that case is Root item
        modelField = self.treeViewUserField.model();
        parentIndex = modelField.index(0,0, self.treeViewUserField.rootIndex());
        self.treeViewUserField.setCurrentIndex(parentIndex)
        
        ##Check if correct item was selected
        self.assertEqual(self.treeViewUserField.currentIndex().internalPointer().itemData, 
                         ('TestSet', '', '', ''));
        
        item = self.treeViewUserField.currentIndex().internalPointer();
        if item is not None:
            ##Remove selected item from the Tree of Qc Fields
            index = self.treeViewUserField.currentIndex()
            self.assertFalse(self.gui.qCTestXUserField.removeItem(index), 
                             "Root item was removed, but it should NOT be possible");
        else:
            print("WRN: User did not selected QC Field to be moved");
#            log.warning("WRN: User did not selected QC Field to be moved");
            pass;

    def __addFromQCtoUser(self):
        
        self.__addUserFieldsTreeView();
        
        ##Clear all selections
        self.treeViewUserField.selectionModel().reset()
        self.treeViewQcField.selectionModel().reset()
        self.treeViewUserField.clearSelection();
        self.treeViewQcField.clearSelection();
        
        modelQcField = self.treeViewQcField.model()
        modelUserField = self.treeViewUserField.model();
        for col in range(3):
            parentQcIndex = modelQcField.index(col, 0,self.treeViewQcField.rootIndex());
            parentUserIndex = modelUserField.index(col, 0, self.treeViewUserField.rootIndex());
            for row in range(3): 
                itemQcIndex = modelQcField.index(row, 0, parentQcIndex);
                self.treeViewQcField.setCurrentIndex(itemQcIndex)
                
                itemUserIndex = modelUserField.index(row, 0, parentUserIndex);
                self.treeViewUserField.setCurrentIndex(itemUserIndex);
                
                ##Add item
                itemQcField = self.treeViewQcField.currentIndex().internalPointer();
                itemUserField = self.treeViewUserField.currentIndex().internalPointer();
                try:self.gui.pushButton_QCTestXUpload_Add_clicked()
                except: continue;
        
#        #======================================================================
#        # Add one
#        #======================================================================
#        
#        ##Set Selection on item in the TreeView
#        modelQcField = self.treeViewQcField.model()
#        modelUserField = self.treeViewUserField.model();
#        parentQcIndex = modelQcField.index(0,0, self.treeViewQcField.rootIndex());
#        itemQcIndex = modelQcField.index(0,0, parentQcIndex);
#        self.treeViewQcField.setCurrentIndex(itemQcIndex)
#        parentUserIndex = modelUserField.index(0,0, self.treeViewUserField.rootIndex());
#        itemUserIndex = modelUserField.index(0,0, parentUserIndex);
#        self.treeViewUserField.setCurrentIndex(itemUserIndex);
#        
#        ##Add item
#        itemQcField = self.treeViewQcField.currentIndex().internalPointer();
#        itemUserField = self.treeViewUserField.currentIndex().internalPointer();
#        self.gui.pushButton_QCTestXUpload_Add_clicked()
#        
#        #======================================================================
#        # Add second item
#        #======================================================================
#        
#        ##Set Selection on item in the TreeView
#        itemQcIndex = modelQcField.index(1,0, parentQcIndex);
#        self.treeViewQcField.setCurrentIndex(itemQcIndex)
#        itemUserIndex = modelUserField.index(1,0, parentUserIndex);
#        self.treeViewUserField.setCurrentIndex(itemUserIndex);
#        
#        ##Add item
#        itemQcField = self.treeViewQcField.currentIndex().internalPointer();
#        itemUserField = self.treeViewUserField.currentIndex().internalPointer();
#        self.gui.pushButton_QCTestXUpload_Add_clicked()
#        
#        
#        #======================================================================
#        # Add third item
#        #======================================================================
#        
#        ##Set Selection on item in the TreeView
#        itemQcIndex = modelQcField.index(2,0, parentQcIndex);
#        self.treeViewQcField.setCurrentIndex(itemQcIndex);
#        itemUserIndex = modelUserField.index(2,0, parentUserIndex);
#        self.treeViewUserField.setCurrentIndex(itemUserIndex);
#        
#        itemQcField = self.treeViewQcField.currentIndex().internalPointer();
#        itemUserField = self.treeViewUserField.currentIndex().internalPointer();
#        self.gui.pushButton_QCTestXUpload_Add_clicked()
        
        
    def testClearButton_1(self):
        '''
        Clear item from User Tree Field
        '''
        ##Add some data to User Filed, which will be removed
        self.__addUserFieldsTreeView();
        self.__addQcFieldsTreeView();
        self.__addFromQCtoUser();
        
        ##Set Selection on item in the TreeView
        modelUserField = self.treeViewUserField.model();
        parentUserIndex = modelUserField.index(0,0, self.treeViewUserField.rootIndex());
        childCounting = parentUserIndex.internalPointer().childCount();
        self.assert_(childCounting>0, "Item was not added")

        ##Check if Fields are present in USerField_TreeView; 
        mapping_fields = {
                          'TestCase': {
                                        'Planned Language': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '28'},
'Plan: Template': {'FieldIsRequired': 'True', 'FieldName': 'Test Case Status', 'FieldType': 'char', 'FieldQcName': 'TC_STATUS', 'FieldUserName': '', 'FieldOrder': '13'},
                                        'Plan: Subject': {'FieldIsRequired': 'True', 'FieldName': 'Test Case Name', 'FieldType': 'char', 'FieldQcName': 'TC_NAME', 'FieldUserName': '', 'FieldOrder': '14'},
                                        'Iterations': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '40'},
                                        'Plan: Modified': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '41'},
                                        'Test Version': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '17'},
                                        'Planned Browser': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '20'},
                                        'Modified': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '18'},
                                        'Responsible Tester': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '21'},
                                        'Planned Host Name': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '22'},
                                        'Planned Exec Time': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '23'},
                                        'Time': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '16'},
                                        'Test': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '25'},
                                        'Plan: Type': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '30'},
                                        'Plan: Creation Date': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '19'},
                                        'Plan: Test': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '26'},
                                        'Plan: Test Name': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '27'},
                                        'Status': {'FieldIsRequired': 'True', 'FieldName': 'Test Case Directory', 'FieldType': 'char', 'FieldQcName': 'TC_DIR', 'FieldUserName': '', 'FieldOrder': '12'},
                                        'Plan: Reviewed': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '29'},
                                        'Plan: Description': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '31'},
                                        'Tester': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '32'},
                                        'Plan: Level': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '33'},
                                        'Plan: Execution Status': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '34'},
                                        'Exec Date': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '35'},
                                        'Plan: Status': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '36'},
                                        'Plan: Path': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '37'},
                                        'Plan: Estimated DevTime': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '38'},
                                        'Plan: Reviewer': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '39'},
                                        'Plan: Designer': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '15'},
                                        'Plan: Priority': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '24'},
                                        'Planned Exec Date': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '42'},
                                        'Attachment': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '43'}
                                        },
                                'Step': {
                                            'Status': {'FieldIsRequired': 'True', 'FieldName': 'Step Description', 'FieldType': 'char', 'FieldQcName': 'ST_DESC', 'FieldUserName': '', 'FieldOrder': '44'},
                                            'Source Test': {'FieldIsRequired': 'True', 'FieldName': 'Step Name', 'FieldType': 'char', 'FieldQcName': 'ST_NAME', 'FieldUserName': '', 'FieldOrder': '45'},
                                            'Actual': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '46'},
                                            'Description': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '47'},
                                            'Attachment': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '52'},
                                            'Expected': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '48'},
                                            'Exec Date': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '49'},
                                            'Step Name': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '50'},
                                            'Exec Time': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '51'}},
                                'TestSet': {
                                            'Test Set Name': {'FieldIsRequired': 'True', 'FieldName': 'Test Set Name', 'FieldType': 'char', 'FieldQcName': 'TS_NAME', 'FieldUserName': '', 'FieldOrder': '0'},
                                            'Status': {'FieldIsRequired': 'True', 'FieldName': 'Test Set Description', 'FieldType': 'char', 'FieldQcName': 'TS_DESC', 'FieldUserName': '', 'FieldOrder': '1'},
                                            'Close Date': {'FieldIsRequired': 'True', 'FieldName': 'Test Set Directory', 'FieldType': 'char', 'FieldQcName': 'TS_DIR', 'FieldUserName': '', 'FieldOrder': '2'},
                                            'Test Set': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '7'},
                                            'Description': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '4'},
                                            'Modified': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '8'},
                                            'ITG Request Id': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '9'},
                                            'Open Date': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '10'},
                                            'Dupa': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '3'},
                                            'Attachment': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '11'},
                                            'Test Set Folder': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '5'},
                                            'test': {'FieldIsRequired': '', 'FieldQcName': '', 'FieldName': '', 'FieldType': '', 'FieldOrder': '6'}
                                            }
                          }
        
        mappingFieldsUserFields = self.gui.qCTestXUserField.get_mappingFields();
        self.assertEqual(mapping_fields, mappingFieldsUserFields);
        
        ##Check if 'pushButton_QCTestXDownload_Clear_clicked()' is Clearing correct value
        self.gui.pushButton_QCTestXUpload_Clear_clicked()
        
        
        mappingFieldsGolden = { 
                               'TestCase': {
                                            'Planned Language': {'FieldIsRequired': '', 'FieldName': '', 'FieldOrder': '28', 'FieldQcName': '', 'FieldType': ''},
                                            'Plan: Template': {'FieldIsRequired': '', 'FieldName': '', 'FieldOrder': '13', 'FieldQcName': '', 'FieldType': ''},
                                            'Plan: Subject': {'FieldIsRequired': '', 'FieldName': '', 'FieldOrder': '14', 'FieldQcName': '', 'FieldType': ''},
                                            'Iterations': {'FieldIsRequired': '', 'FieldName': '', 'FieldOrder': '40', 'FieldQcName': '', 'FieldType': ''},
                                            'Plan: Modified': {'FieldIsRequired': '', 'FieldName': '', 'FieldOrder': '41', 'FieldQcName': '', 'FieldType': ''},
                                            'Test Version': {'FieldIsRequired': '', 'FieldName': '', 'FieldOrder': '17', 'FieldQcName': '', 'FieldType': ''},
                                            'Planned Browser': {'FieldIsRequired': '', 'FieldName': '', 'FieldOrder': '20', 'FieldQcName': '', 'FieldType': ''},
                                            'Modified': {'FieldIsRequired': '', 'FieldName': '', 'FieldOrder': '18', 'FieldQcName': '', 'FieldType': ''},
                                            'Responsible Tester': {'FieldIsRequired': '', 'FieldName': '', 'FieldOrder': '21', 'FieldQcName': '', 'FieldType': ''},
                                            'Planned Host Name': {'FieldIsRequired': '', 'FieldName': '', 'FieldOrder': '22', 'FieldQcName': '', 'FieldType': ''},
                                            'Planned Exec Time': {'FieldIsRequired': '', 'FieldName': '', 'FieldOrder': '23', 'FieldQcName': '', 'FieldType': ''},
                                            'Time': {'FieldIsRequired': '', 'FieldName': '', 'FieldOrder': '16', 'FieldQcName': '', 'FieldType': ''},
                                            'Test': {'FieldIsRequired': '', 'FieldName': '', 'FieldOrder': '25', 'FieldQcName': '', 'FieldType': ''},
                                            'Plan: Type': {'FieldIsRequired': '', 'FieldName': '', 'FieldOrder': '30', 'FieldQcName': '', 'FieldType': ''},
                                            'Plan: Creation Date': {'FieldIsRequired': '', 'FieldName': '', 'FieldOrder': '19', 'FieldQcName': '', 'FieldType': ''},
                                            'Plan: Test': {'FieldIsRequired': '', 'FieldName': '', 'FieldOrder': '26', 'FieldQcName': '', 'FieldType': ''},
                                            'Plan: Test Name': {'FieldIsRequired': '', 'FieldName': '', 'FieldOrder': '27', 'FieldQcName': '', 'FieldType': ''},
                                            'Status': {'FieldIsRequired': '', 'FieldName': '', 'FieldOrder': '12', 'FieldQcName': '', 'FieldType': ''},
                                            'Plan: Reviewed': {'FieldIsRequired': '', 'FieldName': '', 'FieldOrder': '29', 'FieldQcName': '', 'FieldType': ''},
                                            'Plan: Description': {'FieldIsRequired': '', 'FieldName': '', 'FieldOrder': '31', 'FieldQcName': '', 'FieldType': ''},
                                            'Tester': {'FieldIsRequired': '', 'FieldName': '', 'FieldOrder': '32', 'FieldQcName': '', 'FieldType': ''},
                                            'Plan: Level': {'FieldIsRequired': '', 'FieldName': '', 'FieldOrder': '33', 'FieldQcName': '', 'FieldType': ''},
                                            'Plan: Execution Status': {'FieldIsRequired': '', 'FieldName': '', 'FieldOrder': '34', 'FieldQcName': '', 'FieldType': ''},
                                            'Exec Date': {'FieldIsRequired': '', 'FieldName': '', 'FieldOrder': '35', 'FieldQcName': '', 'FieldType': ''},
                                            'Plan: Status': {'FieldIsRequired': '', 'FieldName': '', 'FieldOrder': '36', 'FieldQcName': '', 'FieldType': ''},
                                            'Plan: Path': {'FieldIsRequired': '', 'FieldName': '', 'FieldOrder': '37', 'FieldQcName': '', 'FieldType': ''},
                                            'Plan: Estimated DevTime': {'FieldIsRequired': '', 'FieldName': '', 'FieldOrder': '38', 'FieldQcName': '', 'FieldType': ''},
                                            'Plan: Reviewer': {'FieldIsRequired': '', 'FieldName': '', 'FieldOrder': '39', 'FieldQcName': '', 'FieldType': ''},
                                            'Plan: Designer': {'FieldIsRequired': '', 'FieldName': '', 'FieldOrder': '15', 'FieldQcName': '', 'FieldType': ''},
                                            'Plan: Priority': {'FieldIsRequired': '', 'FieldName': '', 'FieldOrder': '24', 'FieldQcName': '', 'FieldType': ''},
                                            'Planned Exec Date': {'FieldIsRequired': '', 'FieldName': '', 'FieldOrder': '42', 'FieldQcName': '', 'FieldType': ''},
                                            'Attachment': {'FieldIsRequired': '', 'FieldName': '', 'FieldOrder': '43', 'FieldQcName': '', 'FieldType': ''}},
                                'Step': {
                                            'Status': {'FieldIsRequired': '', 'FieldName': '', 'FieldOrder': '44', 'FieldQcName': '', 'FieldType': ''},
                                            'Source Test': {'FieldIsRequired': '', 'FieldName': '', 'FieldOrder': '45', 'FieldQcName': '', 'FieldType': ''},
                                            'Actual': {'FieldIsRequired': '', 'FieldName': '', 'FieldOrder': '46', 'FieldQcName': '', 'FieldType': ''},
                                            'Description': {'FieldIsRequired': '', 'FieldName': '', 'FieldOrder': '47', 'FieldQcName': '', 'FieldType': ''},
                                            'Attachment': {'FieldIsRequired': '', 'FieldName': '', 'FieldOrder': '52', 'FieldQcName': '', 'FieldType': ''},
                                            'Expected': {'FieldIsRequired': '', 'FieldName': '', 'FieldOrder': '48', 'FieldQcName': '', 'FieldType': ''},
                                            'Exec Date': {'FieldIsRequired': '', 'FieldName': '', 'FieldOrder': '49', 'FieldQcName': '', 'FieldType': ''},
                                            'Step Name': {'FieldIsRequired': '', 'FieldName': '', 'FieldOrder': '50', 'FieldQcName': '', 'FieldType': ''},
                                            'Exec Time': {'FieldIsRequired': '', 'FieldName': '', 'FieldOrder': '51', 'FieldQcName': '', 'FieldType': ''}},
                                'TestSet': {
                                            'Test Set Name': {'FieldIsRequired': '', 'FieldName': '', 'FieldOrder': '0', 'FieldQcName': '', 'FieldType': ''},
                                            'Status': {'FieldIsRequired': '', 'FieldName': '', 'FieldOrder': '1', 'FieldQcName': '', 'FieldType': ''},
                                            'Close Date': {'FieldIsRequired': '', 'FieldName': '', 'FieldOrder': '2', 'FieldQcName': '', 'FieldType': ''},
                                            'Test Set': {'FieldIsRequired': '', 'FieldName': '', 'FieldOrder': '7', 'FieldQcName': '', 'FieldType': ''},
                                            'Description': {'FieldIsRequired': '', 'FieldName': '', 'FieldOrder': '4', 'FieldQcName': '', 'FieldType': ''},
                                            'Modified': {'FieldIsRequired': '', 'FieldName': '', 'FieldOrder': '8', 'FieldQcName': '', 'FieldType': ''},
                                            'ITG Request Id': {'FieldIsRequired': '', 'FieldName': '', 'FieldOrder': '9', 'FieldQcName': '', 'FieldType': ''},
                                            'Open Date': {'FieldIsRequired': '', 'FieldName': '', 'FieldOrder': '10', 'FieldQcName': '', 'FieldType': ''},
                                            'Dupa': {'FieldIsRequired': '', 'FieldName': '', 'FieldOrder': '3', 'FieldQcName': '', 'FieldType': ''},
                                            'Attachment': {'FieldIsRequired': '', 'FieldName': '', 'FieldOrder': '11', 'FieldQcName': '', 'FieldType': ''},
                                            'Test Set Folder': {'FieldIsRequired': '', 'FieldName': '', 'FieldOrder': '5', 'FieldQcName': '', 'FieldType': ''},
                                            'test': {'FieldIsRequired': '', 'FieldName': '', 'FieldOrder': '6', 'FieldQcName': '', 'FieldType': ''}
                                            }
                               };
        
        mappingFieldsUserFields = self.gui.qCTestXUserField.get_mappingFields();
        
        self.assertEqual(mappingFieldsGolden, mappingFieldsUserFields);


    def testRefreshFieldButton_1(self):
        '''
        Refresh Field button
        '''
        ##to be sure that there was no other files created before
        try: os.remove(r'D:\Eclipse_workspace\QC_gui\src\testUnit\testxml.xml');
        except: pass;
        
        self.gui.guiQcTestXField.lineEdit_settingName.setText("Setting_1")##input name of setting to be save
        self.gui.pushButton_QCTestX_SaveFieldFile_clicked(); ##this will make xml file
        
        self.assertTrue(self.gui.pushButton_QCTestX_RefreshFieldFile_clicked());##check if Refresh operation was OK
        listWidgetField = self.gui.guiQcTestXField.listWidget_fieldSavedSettings;##get current ListWidget with all present Settings
        currentList = [];
        for i in range(listWidgetField.count()):
            currentList.append(listWidgetField.item(i).text()); ##create list of items from ListWidget of Settings 
        
        
        openXMLfile = OpenFile(self.savingType, qcType="TestLab"); ##openXML file with settings
        settingList = openXMLfile.itemSettings.keys() ##Dictionary of all seetings. {str(name):etree(eleme
        
        self.assertEqual(currentList, settingList); ##now compare if Refresh button really updated in correct way the ListWidget with the data from XML file   
        

    def testRefreshTreeViewUserField_1(self):
        '''
        Check if TreeView_UserField is updated with correct way
        '''
        
        self.__addUserFieldsTreeView();
        self.__addQcFieldsTreeView();
        self.__addFromQCtoUser();
        
        mapping_fields = self.gui.qCTestXUserField.get_mappingFields();
        
        mapping_fields_cmp = {}; 
        for fieldGroup, itemInfo in self.gui.qCTestXUserField.mainUserItemsNames.items():
#            {'Step': (<lib.QCTestX.treeView.treeTestXModel.TreeItem instance at 0x01BB3198>,
#                      <PyQt4.QtCore.QModelIndex at 0x1bab5a8>),
#             'TestCase': (<lib.QCTestX.treeView.treeTestXModel.TreeItem instance at 0x01BB3148>,
#                           <PyQt4.QtCore.QModelIndex at 0x1bab570>),
#             'TestSet': (<lib.QCTestX.treeView.treeTestXModel.TreeItem instance at 0x01BB30F8>,
#                          <PyQt4.QtCore.QModelIndex at 0x1bab538>)
#             }
            mapping_fields_cmp.update({fieldGroup:{}});
            parentUserIndex = itemInfo[1];
            childCounting = parentUserIndex.internalPointer().childCount();
            for i in range(childCounting):
                ##dataUserField = ('fieldUserName', ('fieldName', 'char', 'True'))
                fieldUserName, fieldValues, fieldName, fieldQcValues = itemInfo[0].child(i).itemData;
#                if fieldName == '': continue;
                fieldValues.update(fieldQcValues);
                fieldValues.update({'FieldName':fieldName});
                mapping_fields_cmp[fieldGroup].update({fieldUserName: fieldValues});
        
        
        print "mapping_fields_cmp:", mapping_fields_cmp;
        
        self.assertEqual(mapping_fields, mapping_fields_cmp);
        

def load_tests(loader, tests, pattern):
    suite = unittest.TestSuite()
#    suite.addTest(unittest.makeSuite(Test));  ##Run all tests
    return suite

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()

#if __name__ == "__main__":
#    #import sys;sys.argv = ['', 'Test.testOpenTree']
#    
#    app = QtGui.QApplication(sys.argv)
#    
#
#    suite = unittest.TestSuite()
#
##    suite.addTest(TestTreeButtonOperation('testClearButton_1'));  ##run only one test
#    
#    suite.addTest(unittest.makeSuite(TestTreeButtonOperation));  ##Run all tests
#    unittest.TextTestRunner(verbosity=2).run(suite)
#    
#    sys.exit(app.exec_())