'''
Created on 18-11-2011

@author: qc
'''
import unittest
from PyQt4 import QtCore, QtGui
from time import sleep
import sys
import xml.etree.ElementTree as etree
from functools import partial

from lib.QCTestX.qcTestX_download_chooseFields import QcTestXDownloadChooseFields, RefreshButtonException
from lib.QCTestX.treeView.treeTestXModel import TreeItem as QcTreeItemDownload

from lib.QCTestX.qcTestX_upload_chooseFields import QcTestXUploadChooseFields
from lib.QCTestX.treeView.treeTestXModel import TreeItem as QcTreeItemUpload

from lib.QCTestX.field_file_operation import *  


from win32com.client import gencache, DispatchWithEvents, constants
gencache.EnsureModule('{D66ADC20-B070-11D3-8604-0050046B8F4A}', 0, 1, 0);

import os;
os.chdir(r"D:\Eclipse_workspace\QC_gui\src");
import lib.QualityCenter_ext.lib.qc_connect_disconnect as qc_connect_disconnect


server = "http://qc:8080/qcbin/";
userName="alex_qc";
domainName="DEFAULT";
projectName="QualityCenter_Demo";
password = ""; ##Put the right password




app = QtGui.QApplication(sys.argv);


class TestXMLDownloadSettingOperation(unittest.TestCase):
    '''
    Check if in ChooseFields, XML operation, like Open,Save,Delete,Refresh are working
    '''

    def get_mapping_fields_user(self):
        self.__mappingFieldsUser={"TestSet": {'Test Set Name':{'FieldOrder':'0',
                                                       'FieldQcName':'TS_NAME',
                                                       'FieldType':'char',
                                                       'FieldIsRequired':'True'},
                                      'Test Set Directory': {'FieldOrder':'1',
                                                             'FieldQcName':'TS_DIR',
                                                             'FieldType':'char',
                                                             'FieldIsRequired':'True'},
                                      'Test Set Description': {'FieldOrder':'2',
                                                               'FieldQcName':'TS_DESC',
                                                               'FieldType':'char',
                                                               'FieldIsRequired':'True'}
                                      },
                               "TestCase": {'Test Case Name': {'FieldOrder':'0',
                                                          'FieldQcName':'TC_NAME',
                                                          'FieldType':'char',
                                                          'FieldIsRequired':'True'},
                                            'Test Case Directory': {'FieldOrder':'1',
                                                              'FieldQcName':'TC_DIR',
                                                              'FieldType':'char',
                                                              'FieldIsRequired':'True'},
                                            'Test Case Status': {'FieldOrder':'2',
                                                              'FieldQcName':'TC_STATUS',
                                                              'FieldType':'char',
                                                              'FieldIsRequired':'True'},
                                        },
                               "Step": {'Step Name': {'FieldOrder':'0',
                                                  'FieldQcName':'ST_NAME',
                                                  'FieldType':'char',
                                                  'FieldIsRequired':'True'},
                                        'Step Description': {'FieldOrder':'1',
                                                  'FieldQcName':'ST_DESC',
                                                  'FieldType':'char',
                                                  'FieldIsRequired':'True'}
                                    }
                             };
        return self.__mappingFieldsUser


    def set_mapping_fields_user(self, value):
        self.__mappingFieldsUser = value


    def del_mapping_fields_user(self):
        del self.__mappingFieldsUser
    mappingFieldsUser = property(get_mapping_fields_user, set_mapping_fields_user, del_mapping_fields_user, "mappingFieldsUser's docstring")

    def setUp(self):
        self.savingType = "DL";
        self.qcConnect = None;
        self.qcType = "TestLab"
#        self.qcConnect = qc_connect_disconnect.QC_connect(server, userName, password, domainName,projectName);
#        for result in self.qcConnect.connect():
#            print result;
#        self.qc = self.qcConnect.qc;
#        
        self.filename = r'D:\Eclipse_workspace\QC_gui\src\testUnit\testxml.xml';
        ##to be sure that there was no other files created before
        try: os.remove(self.filename);
        except: pass;
        
        
        self.main = QtGui.QWidget()
        self.gui = QcTestXDownloadChooseFields(type="TestLab",
                                               parent=self.main, 
                                               qcConnect=self.qcConnect)
        
        self.treeViewQcField = self.gui.qCTestXQcField.treeView
        self.treeViewUserField = self.gui.qCTestXUserField.treeView
        modelQcField = self.treeViewQcField.model();

        
        self.gui.qCTestXUserField.updateModel(self.mappingFieldsUser);
        
        
        
        self.mappingFieldsQc = {
                            "CYCLE": {'Test Set Name':{'FieldQcName':'TS_NAME',
                                                       'FieldType':'char',
                                                       'FieldIsRequired':'True'},
                                      'Test Set Directory': {'FieldQcName':'TS_DIR',
                                                             'FieldType':'char',
                                                             'FieldIsRequired':'True'},
                                      'Test Set Description': {'FieldQcName':'TS_DESC',
                                                               'FieldType':'char',
                                                               'FieldIsRequired':'True'}
                                      },
                               "TESTCYCL": {'Test Case Name': {'FieldQcName':'TC_NAME',
                                                          'FieldType':'char',
                                                          'FieldIsRequired':'True'},
                                            'Test Case Directory': {'FieldQcName':'TC_DIR',
                                                              'FieldType':'char',
                                                              'FieldIsRequired':'True'},
                                            'Test Case Status': {'FieldQcName':'TC_STATUS',
                                                              'FieldType':'char',
                                                              'FieldIsRequired':'True'},
                                        },
                               "STEP": {'Step Name': {'FieldQcName':'ST_NAME',
                                                  'FieldType':'char',
                                                  'FieldIsRequired':'True'},
                                        'Step Description': {'FieldQcName':'ST_DESC',
                                                  'FieldType':'char',
                                                  'FieldIsRequired':'True'}
                                    }
                             };
        self.gui.qCTestXQcField.updateModelTestLab(self.mappingFieldsQc );
        

        
        self.main.show();

    def tearDown(self):
        self.treeViewQcField.expandAll();
        self.treeViewUserField.expandAll();
        self.main.repaint();
#        sleep(5);
        self.main.close();
        
        
#        os.remove(os.getcwd()+'\\'+self.filename);
        
#        self.qcConnect.disconnect_QC();
        

    def __addFields(self):
        
        ##Set Selection on item in the TreeView
        modelQcField = self.treeViewQcField.model()
        modelUserField = self.treeViewUserField.model();
        #======================================================================
        # Add one
        #======================================================================
        parentQcIndex = modelQcField.index(0,0, self.treeViewQcField.rootIndex());
        itemQcIndex = modelQcField.index(0,0, parentQcIndex);
        self.treeViewQcField.setCurrentIndex(itemQcIndex)
        
        item = self.treeViewQcField.currentIndex().internalPointer();
        if item is not None:
            ##Add selected item to the Tree of User Fields
            self.gui.qCTestXUserField.addItem(item);
            ##Remove selected item from the Tree of Qc Fields
            index = self.treeViewQcField.currentIndex()
            self.gui.qCTestXQcField.removeItem(index);
        else:
            print("WRN: User did not selected QC Field to be moved");
#            log.warning("WRN: User did not selected QC Field to be moved");
            pass;
        

        #======================================================================
        # Add second item
        #======================================================================
        
        ##Set Selection on item in the TreeView
        itemQcIndex = modelQcField.index(0,0, parentQcIndex);
        self.treeViewQcField.setCurrentIndex(itemQcIndex)
        
        item = self.treeViewQcField.currentIndex().internalPointer();
        if item is not None:
            ##Add selected item to the Tree of User Fields
            self.gui.qCTestXUserField.addItem(item);
            ##Remove selected item from the Tree of Qc Fields
            index = self.treeViewQcField.currentIndex()
            self.gui.qCTestXQcField.removeItem(index);
        else:
            print("WRN: User did not selected QC Field to be moved");
#            log.warning("WRN: User did not selected QC Field to be moved");
            pass;


        #======================================================================
        # Add third item
        #======================================================================
        
        ##Set Selection on item in the TreeView
        itemQcIndex = modelQcField.index(0,0, parentQcIndex);
        self.treeViewQcField.setCurrentIndex(itemQcIndex)
        
        item = self.treeViewQcField.currentIndex().internalPointer();
        if item is not None:
            ##Add selected item to the Tree of User Fields
            self.gui.qCTestXUserField.addItem(item);
            ##Remove selected item from the Tree of Qc Fields
            index = self.treeViewQcField.currentIndex()
            self.gui.qCTestXQcField.removeItem(index);
        else:
            print("WRN: User did not selected QC Field to be moved");
#            log.warning("WRN: User did not selected QC Field to be moved");
            pass;

        #======================================================================
        # Fourth third item
        #======================================================================
        
        ##Set Selection on item in the TreeView
        parentQcIndex = modelQcField.index(1,0, self.treeViewQcField.rootIndex());
        itemQcIndex = modelQcField.index(0,0, parentQcIndex);
        self.treeViewQcField.setCurrentIndex(itemQcIndex)
        
        item = self.treeViewQcField.currentIndex().internalPointer();
        if item is not None:
            ##Add selected item to the Tree of User Fields
            self.gui.qCTestXUserField.addItem(item);
            ##Remove selected item from the Tree of Qc Fields
            index = self.treeViewQcField.currentIndex()
            self.gui.qCTestXQcField.removeItem(index);
        else:
            print("WRN: User did not selected QC Field to be moved");
#            log.warning("WRN: User did not selected QC Field to be moved");
            pass;
        
        #======================================================================
        # Fifth third item
        #======================================================================
        
        ##Set Selection on item in the TreeView
        itemQcIndex = modelQcField.index(0,0, parentQcIndex);
        self.treeViewQcField.setCurrentIndex(itemQcIndex)
        
        item = self.treeViewQcField.currentIndex().internalPointer();
        if item is not None:
            ##Add selected item to the Tree of User Fields
            self.gui.qCTestXUserField.addItem(item);
            ##Remove selected item from the Tree of Qc Fields
            index = self.treeViewQcField.currentIndex()
            self.gui.qCTestXQcField.removeItem(index);
        else:
            print("WRN: User did not selected QC Field to be moved");
#            log.warning("WRN: User did not selected QC Field to be moved");
            pass;
        
        #======================================================================
        # Sixth third item
        #======================================================================
        
        ##Set Selection on item in the TreeView
        parentQcIndex = modelQcField.index(2,0, self.treeViewQcField.rootIndex());
        itemQcIndex = modelQcField.index(0,0, parentQcIndex);
        self.treeViewQcField.setCurrentIndex(itemQcIndex)
        
        item = self.treeViewQcField.currentIndex().internalPointer();
        if item is not None:
            ##Add selected item to the Tree of User Fields
            self.gui.qCTestXUserField.addItem(item);
            ##Remove selected item from the Tree of Qc Fields
            index = self.treeViewQcField.currentIndex()
            self.gui.qCTestXQcField.removeItem(index);
        else:
            print("WRN: User did not selected QC Field to be moved");
#            log.warning("WRN: User did not selected QC Field to be moved");
            pass;


    def testSaveFile_1(self):
        '''
        Save fields items to the XML file 
        There will be two settings to save with field items.
        Check if in proper place and if in correct number fields was saved into the XML file
        '''
        
        filename = self.filename;
        saveFile = SaveFile(self.savingType, self.qcType);
        
        saveFile.makesetting("Setting_1", qcType="TestLab")
        fieldItem = {'fieldGroup':'TestCase',
                     'fieldName':"TestUserName_01",
                     'fieldQcName':"TC_User_01",
                     'fieldType':"char",
                     'fieldIsRequired':'True',
                     'fieldOrder':'1'
                    };
        saveFile.makeFieldItem(fieldGroup=fieldItem['fieldGroup'],\
                               fieldName=fieldItem['fieldName'],\
                               fieldQcName=fieldItem['fieldQcName'],\
                               fieldType=fieldItem['fieldType'],\
                               fieldIsRequired=fieldItem['fieldIsRequired'],\
                               fieldOrder=fieldItem['fieldOrder']
                               );
        
        
        saveFile.makesetting("Setting_2", qcType="TestLab")
        fieldItem = {'fieldGroup':'TestCase',
                     'fieldName':"TestUserName_02",
                     'fieldQcName':"TC_User_02",
                     'fieldType':"char",
                     'fieldIsRequired':'True',
                     'fieldOrder':'1'
                    };
        saveFile.makeFieldItem(fieldGroup=fieldItem['fieldGroup'],\
                               fieldName=fieldItem['fieldName'],\
                               fieldQcName=fieldItem['fieldQcName'],\
                               fieldType=fieldItem['fieldType'],\
                               fieldIsRequired=fieldItem['fieldIsRequired'],\
                               fieldOrder=fieldItem['fieldOrder']
                               );
        saveFile.makeFieldItem(fieldGroup='TestCase',
                               fieldName="Regression", 
                               fieldQcName="TC_User_03", 
                               fieldType="char", 
                               fieldIsRequired='True',
                               fieldOrder='2');
        saveFile.makeFieldItem(fieldGroup='TestSet',
                               fieldName="TestUserName_02", 
                               fieldQcName="TC_User_02", 
                               fieldType="char", 
                               fieldIsRequired='True',
                               fieldOrder='1');
        self.gui.guiQcTestXField.lineEdit_settingName.setText("Setting_1")##input name of setting to be save
        del saveFile ##this will write file and close it    
        
        
        tree = etree.parse(filename)
        root = tree.getroot();
        self.assertEqual(root.tag, 'xml');
        
        rootSettings = root.find('Settings');
        self.assertEqual(rootSettings.tag, 'Settings');
        
        rootSettingType = rootSettings.find('SettingsDownload');
        self.assertEqual(rootSettingType.tag, 'SettingsDownload');
        
        rootCRC = root.find('CRCsum');
        self.assertEqual(rootCRC.tag, 'CRCsum');
    
        allsetting = rootSettingType.findall('Setting');
        allsettingNames = [el.attrib['name']for el in allsetting];
        self.assertEqual(2, len(allsetting));
        
        self.assertEqual(allsetting[0].attrib['name'], 'Setting_1');
        self.assertEqual(allsetting[1].attrib['name'], 'Setting_2');
        
        setting_1 = allsetting[allsettingNames.index('Setting_1')];
        self.assertEqual(setting_1[0].tag, 'TestSet')
        self.assertEqual(setting_1[1].tag, 'TestCase')
        self.assertEqual(setting_1[2].tag, 'Step')
    
        setting_2 = allsetting[allsettingNames.index('Setting_2')];
        self.assertEqual(setting_2[0].tag, 'TestSet')
        self.assertEqual(setting_2[1].tag, 'TestCase')
        self.assertEqual(setting_2[2].tag, 'Step')
    
        self.assertEqual(len(setting_1[1]), 1); ##only one Field item in the TestCase - Setting_1
        fieldItem = setting_1[1][0];
        fieldItemValue = {'FieldIsRequired':'True',
                          'FieldName':"TestUserName_01",
                          'FieldQcName':"TC_User_01",
                          'FieldType':"char",
                          'FieldOrder':'1',
                          'FieldUserName': ''
                          };
        self.assertEqual(fieldItem.attrib,fieldItemValue); 

        self.assertEqual(len(setting_2[0]), 1); ##only one Field item in the TestCase - Setting_2
        self.assertEqual(len(setting_2[1]), 2); ##only two Field item in the TestCase - Setting_2
        self.assertEqual(len(setting_2[2]), 0); ##none Field item in the TestCase - Setting_2
        fieldItem = setting_2[1][0];##field Item of Setting_2 -> TestCase -> first
        fieldItemValue = {'FieldIsRequired':'True',
                          'FieldName':"TestUserName_02",
                          'FieldQcName':"TC_User_02",
                          'FieldType':"char",
                          'FieldOrder':'1', 
                          'FieldUserName': ''
                          };
        self.assertEqual(fieldItem.attrib,fieldItemValue);




        
    

    def testSaveFile_2(self):
        '''
        Save fields, but by direct operation.
        Field item should not be saved, due to the wrong FieldGroup
        '''
        
        filename = 'testxml.xml';
        saveFile = SaveFile(self.savingType, self.qcType);
        
        saveFile.makesetting("Setting_1", qcType='TestPlan')
        fieldItem = {'fieldGroup':'Dupa', ##Field item will not be created, fieldGroup is wrong
                     'fieldName':"TestUserName_01",
                     'fieldQcName':"TC_User_01",
                     'fieldType':"char",
                     'fieldIsRequired':'True',
                     'fieldOrder':'1'
                    };
        saveFile.makeFieldItem(fieldGroup=fieldItem['fieldGroup'],\
                               fieldName=fieldItem['fieldName'],\
                               fieldQcName=fieldItem['fieldQcName'],\
                               fieldType=fieldItem['fieldType'],\
                               fieldIsRequired=fieldItem['fieldIsRequired'],\
                               fieldOrder=fieldItem['fieldOrder']
                               );
        self.gui.guiQcTestXField.lineEdit_settingName.setText("Setting_1")##input name of setting to be save
        del saveFile ##this will write file and close it
        
        
        tree = etree.parse(self.filename)
        root = tree.getroot();
        self.assertEqual(root.tag, 'xml');
        
        rootSettings = root.find('Settings');
        self.assertEqual(rootSettings.tag, 'Settings');
        
        rootSettingType = rootSettings.find('SettingsDownload');
        self.assertEqual(rootSettingType.tag, 'SettingsDownload');
        
        rootCRC = root.find('CRCsum');
        self.assertEqual(rootCRC.tag, 'CRCsum');
    
        allsetting = rootSettingType.findall('Setting');
        allsettingNames = [el.attrib['name']for el in allsetting];
        self.assertEqual(1, len(allsetting));
        
        self.assertEqual(allsetting[0].attrib['name'], 'Setting_1');
        self.assertEqual(allsetting[0].attrib['QCtype'], 'TestPlan');
        
        setting_1 = allsetting[allsettingNames.index('Setting_1')];
        self.assertEqual(len(setting_1),3);
        
        self.assertEqual(len(setting_1[0]), 0); ##none Field item in the TestCase - Setting_2
        self.assertEqual(len(setting_1[1]), 0); ##none Field item in the TestCase - Setting_2
        self.assertEqual(len(setting_1[2]), 0); ##none Field item in the TestCase - Setting_2

    def testSaveFile_3(self):
        '''
        Save double the same file, to check if previous saved setting are still present or overwritten
        '''
        self.testSaveFile_1(); ##create xml file with two saved settings
        
        
        filename = self.filename;
        saveFile = SaveFile(self.savingType, self.qcType);
        
        saveFile.makesetting("Setting_1", qcType='TestPlan' )
        fieldItem = {'fieldGroup':'TestSet',
                     'fieldName':"TestSetUserName_03",
                     'fieldQcName':"TS_Name_01",
                     'fieldType':"char",
                     'fieldIsRequired':'True',
                     'fieldOrder':'2'
                    };
        saveFile.makeFieldItem(fieldGroup=fieldItem['fieldGroup'],\
                               fieldName=fieldItem['fieldName'],\
                               fieldQcName=fieldItem['fieldQcName'],\
                               fieldType=fieldItem['fieldType'],\
                               fieldIsRequired=fieldItem['fieldIsRequired'],\
                               fieldOrder=fieldItem['fieldOrder']
                               );
        saveFile.makesetting("Setting_3", qcType='TestPlan')
        fieldItem = {'fieldGroup':'TestSet',
                     'fieldName':"TestSetUserName_03",
                     'fieldQcName':"TS_Name_01",
                     'fieldType':"char",
                     'fieldIsRequired':'True',
                     'fieldOrder':'2'
                    };
        saveFile.makeFieldItem(fieldGroup=fieldItem['fieldGroup'],\
                               fieldName=fieldItem['fieldName'],\
                               fieldQcName=fieldItem['fieldQcName'],\
                               fieldType=fieldItem['fieldType'],\
                               fieldIsRequired=fieldItem['fieldIsRequired'],\
                               fieldOrder=fieldItem['fieldOrder']
                               );
        self.gui.guiQcTestXField.lineEdit_settingName.setText("Setting_1")##input name of setting to be save
        del saveFile;                       
                               
        tree = etree.parse(filename)
        root = tree.getroot();
        self.assertEqual(root.tag, 'xml');
        
        
        rootSettings = root.find('Settings');
        self.assertEqual(rootSettings.tag, 'Settings');
        
        rootSettingType = rootSettings.find('SettingsDownload');
        self.assertEqual(rootSettingType.tag, 'SettingsDownload');
        
        
        rootCRC = root.find('CRCsum');
        self.assertEqual(rootCRC.tag, 'CRCsum');
    
        allsetting = rootSettingType.findall('Setting');
        allsettingNames = [el.attrib['name']for el in allsetting];
        self.assertEqual(3, len(allsetting));
        
        self.assertTrue('Setting_1' in [el.attrib['name']for el in allsetting]);
        
        
        setting_1 = allsetting[allsettingNames.index('Setting_1')];
        self.assertEqual(len(setting_1),3);
        self.assertEqual(len(setting_1[0]), 1); ##only one Field item in the TestSet - Setting_1
        self.assertEqual(len(setting_1[1]), 0); ##none Field item in the TestCase - Setting_1
        self.assertEqual(len(setting_1[2]), 0); ##none Field item in the Step - Setting_1

        setting_2 = allsetting[allsettingNames.index('Setting_2')];
        self.assertEqual(len(setting_2),3);
        self.assertEqual(len(setting_2[0]), 1); ##only one Field item in the TestSet - Setting_2
        self.assertEqual(len(setting_2[1]), 2); ##only two Field item in the TestCase - Setting_2
        self.assertEqual(len(setting_2[2]), 0); ##none Field item in the Step - Setting_2

        setting_3 = allsetting[allsettingNames.index('Setting_3')];
        self.assertEqual(len(setting_3),3);
        self.assertEqual(len(setting_3[0]), 1); ##only one Field item in the TestSet - Setting_3
        self.assertEqual(len(setting_3[1]), 0); ##none Field item in the TestCase - Setting_3
        self.assertEqual(len(setting_3[2]), 0); ##none Field item in the Step - Setting_3
        
        
    def testSaveFile_4(self):
        '''
        Save field from TreeView to XML file
        '''
        self.gui.guiQcTestXField.lineEdit_settingName.setText("Setting_1")##input name of setting to be save
        self.gui.pushButton_QCTestX_SaveFieldFile_clicked();
        ##dataUserField = ('fieldUserName', ('fieldName', 'char', 'True'))
        
        tree = etree.parse(self.filename)
        root = tree.getroot();
        goldenXML = "<xml><Settings><SettingsDownload><Setting QCtype=\"TestLab\" name=\"Setting_1\"><TestSet><Field FieldIsRequired=\"True\" FieldName=\"Test Set Name\" FieldOrder=\"0\" FieldQcName=\"TS_NAME\" FieldType=\"char\" FieldUserName=\"\" /><Field FieldIsRequired=\"True\" FieldName=\"Test Set Description\" FieldOrder=\"2\" FieldQcName=\"TS_DESC\" FieldType=\"char\" FieldUserName=\"\" /><Field FieldIsRequired=\"True\" FieldName=\"Test Set Directory\" FieldOrder=\"1\" FieldQcName=\"TS_DIR\" FieldType=\"char\" FieldUserName=\"\" /></TestSet><TestCase><Field FieldIsRequired=\"True\" FieldName=\"Test Case Directory\" FieldOrder=\"1\" FieldQcName=\"TC_DIR\" FieldType=\"char\" FieldUserName=\"\" /><Field FieldIsRequired=\"True\" FieldName=\"Test Case Status\" FieldOrder=\"2\" FieldQcName=\"TC_STATUS\" FieldType=\"char\" FieldUserName=\"\" /><Field FieldIsRequired=\"True\" FieldName=\"Test Case Name\" FieldOrder=\"0\" FieldQcName=\"TC_NAME\" FieldType=\"char\" FieldUserName=\"\" /></TestCase><Step><Field FieldIsRequired=\"True\" FieldName=\"Step Description\" FieldOrder=\"1\" FieldQcName=\"ST_DESC\" FieldType=\"char\" FieldUserName=\"\" /><Field FieldIsRequired=\"True\" FieldName=\"Step Name\" FieldOrder=\"0\" FieldQcName=\"ST_NAME\" FieldType=\"char\" FieldUserName=\"\" /></Step></Setting></SettingsDownload></Settings><CRCsum>1868192671</CRCsum></xml>"
        print 'testSaveFile_4 GoldenXML:', etree.tostring(root, 'UTF-8');
        self.assertTrue(goldenXML in etree.tostring(root, 'UTF-8'));
        
    def testSaveFile_5(self):
        '''
        Save field from TreeView to XML file. Check if saving the same SeetingName will save double or not  
        '''
        self.gui.guiQcTestXField.lineEdit_settingName.setText("Setting_1")##input name of setting to be save
        self.gui.pushButton_QCTestX_SaveFieldFile_clicked();
        ##dataUserField = ('fieldUserName', ('fieldName', 'char', 'True'))
        
        self.gui.guiQcTestXField.lineEdit_settingName.setText("Setting_1")##input name of setting to be save
        self.gui.pushButton_QCTestX_SaveFieldFile_clicked();
        
        
        tree = etree.parse(self.filename)
        root = tree.getroot();
        goldenXML = "<xml><Settings><SettingsDownload><Setting QCtype=\"TestLab\" name=\"Setting_1\"><TestSet><Field FieldIsRequired=\"True\" FieldName=\"Test Set Name\" FieldOrder=\"0\" FieldQcName=\"TS_NAME\" FieldType=\"char\" FieldUserName=\"\" /><Field FieldIsRequired=\"True\" FieldName=\"Test Set Description\" FieldOrder=\"2\" FieldQcName=\"TS_DESC\" FieldType=\"char\" FieldUserName=\"\" /><Field FieldIsRequired=\"True\" FieldName=\"Test Set Directory\" FieldOrder=\"1\" FieldQcName=\"TS_DIR\" FieldType=\"char\" FieldUserName=\"\" /></TestSet><TestCase><Field FieldIsRequired=\"True\" FieldName=\"Test Case Directory\" FieldOrder=\"1\" FieldQcName=\"TC_DIR\" FieldType=\"char\" FieldUserName=\"\" /><Field FieldIsRequired=\"True\" FieldName=\"Test Case Status\" FieldOrder=\"2\" FieldQcName=\"TC_STATUS\" FieldType=\"char\" FieldUserName=\"\" /><Field FieldIsRequired=\"True\" FieldName=\"Test Case Name\" FieldOrder=\"0\" FieldQcName=\"TC_NAME\" FieldType=\"char\" FieldUserName=\"\" /></TestCase><Step><Field FieldIsRequired=\"True\" FieldName=\"Step Description\" FieldOrder=\"1\" FieldQcName=\"ST_DESC\" FieldType=\"char\" FieldUserName=\"\" /><Field FieldIsRequired=\"True\" FieldName=\"Step Name\" FieldOrder=\"0\" FieldQcName=\"ST_NAME\" FieldType=\"char\" FieldUserName=\"\" /></Step></Setting></SettingsDownload></Settings><CRCsum>1868192671</CRCsum></xml>"
        print 'testSaveFile_5 GoldenXML:', etree.tostring(root, 'UTF-8');
        self.assertTrue(goldenXML in etree.tostring(root, 'UTF-8'));

        
        
    def testOpenFile_1(self): 
        '''
        Open xml file which does not exists
        '''
        callableFunction = partial(OpenFile, 
                                   self.savingType,
                                   self.qcType);
        self.assertRaises(OpenFileException, callableFunction);
        
    def testOpenFile_2(self): 
        '''
        Open xml file which exists but has wrong xml style
        '''
        self.testSaveFile_4()##create xml file
        
        
        f = open(self.filename, 'r+')
        f.write('wrongXMLword');##write wrong xml style 
        f.flush();
        f.close();
        callableFunction = partial(OpenFile, 
                                   self.savingType,
                                   self.qcType);
        self.assertRaises(ParserException, callableFunction);
        
    def testOpenFile_3(self): 
        '''
        Open xml file which exists but has wrong CRC checksum
        '''
        self.testSaveFile_4()##create xml file
        saveFile = SaveFile(self.savingType, self.qcType);
        
        saveFile.makesetting("Setting_10", qcType="TestLab")
        fieldItem = {'fieldGroup':'TestSet',
                     'fieldName':"TestSetUserName_03",
                     'fieldQcName':"TS_Name_01",
                     'fieldType':"char",
                     'fieldIsRequired':'True',
                     'fieldOrder':'2'
                    };
        saveFile.makeFieldItem(fieldGroup=fieldItem['fieldGroup'],\
                               fieldName=fieldItem['fieldName'],\
                               fieldQcName=fieldItem['fieldQcName'],\
                               fieldType=fieldItem['fieldType'],\
                               fieldIsRequired=fieldItem['fieldIsRequired'],\
                               fieldOrder=fieldItem['fieldOrder']
                               );
        try:
            outFile = open(self.filename, 'wb');
        except IOError, err:
            log.error("ERR: Unable to create file. ", err);
            return
#        self.genCRC();
        saveFile.tree.write(outFile, encoding="UTF-8")
        outFile.close();
        
        callableFunction = partial(OpenFile, 
                                   self.savingType,
                                   self.qcType);
        self.assertRaises(CRCexception, callableFunction);
    
    def testOpenFile_4(self): 
        '''
        Check if "pushButton_QCTestX_OpenFieldFile_clicked()" is working good. 
        1. Update and save TreeView_UserFiles with some data to XML file 
        2. Open XML file with setting "Setting_1" and update TreeView_UserFiled with data from XML
            Data from XML file setting "Setting_1" are the same as current seen TreeView_UserFields 
        3. Get current TreeView_UserField and compare this with XML file 
        '''
        self.testSaveFile_4()##create xml file
        saveFile = SaveFile(self.savingType, self.qcType);
        
        saveFile.makesetting("Setting_2", qcType="TestLab")
        fieldItem = {'fieldGroup':'TestSet',
                     'fieldName':"Dupa",
                     'fieldQcName':"TS_Name_01",
                     'fieldType':"char",
                     'fieldIsRequired':'True',
                     'fieldOrder':'2'
                    };
        saveFile.makeFieldItem(fieldGroup=fieldItem['fieldGroup'],\
                               fieldName=fieldItem['fieldName'],\
                               fieldQcName=fieldItem['fieldQcName'],\
                               fieldType=fieldItem['fieldType'],\
                               fieldIsRequired=fieldItem['fieldIsRequired'],\
                               fieldOrder=fieldItem['fieldOrder']
                               );
        del saveFile

        self.gui.guiQcTestXField.lineEdit_settingName.setText("Setting_1")##input name of setting to be save
        self.gui.pushButton_QCTestX_OpenFieldFile_clicked();
        
        
        mapping_fields = self.gui.qCTestXUserField.get_mappingFields()
        
        ##Now open xml file with setting name "Setting_1" and get mapping of this setting
        openXmlFile = OpenFile(self.savingType, self.qcType);
        mapping_fields_cmp = openXmlFile.get_mappingfields("Setting_1", self.savingType);
        
        ##Check if TreeView_UserField has the same data as XML "Setting_1"
        self.assertEqual(mapping_fields, mapping_fields_cmp);

    def testOpenFile_5(self): 
        '''
        Check if "pushButton_QCTestX_OpenFieldFile_clicked()" is working good. 
        1. Update and save TreeView_UserFiles with some data to XML file 
        2. Open XML file with setting "Setting_2" and update TreeView_UserFiled with data from XML
            Data from XML file setting "Setting_2" are different as current seen TreeView_UserFields
            Additional, one of the Fields in the XML is not in the QC Fields dict(). That is why it will not be put into TreeView_UserFields 
        3. Get current TreeView_UserField and compare this with XML file 
        '''
        self.testSaveFile_4()##create xml file
        saveFile = SaveFile(self.savingType, self.qcType);
        
        saveFile.makesetting("Setting_2", qcType="TestLab")
        fieldItem = {'fieldGroup':'TestSet',
                     'fieldName':"Dupa",
                     'fieldQcName':"TS_Name_01",
                     'fieldType':"char",
                     'fieldIsRequired':'True',
                     'fieldOrder':'2'
                    };
        saveFile.makeFieldItem(fieldGroup=fieldItem['fieldGroup'],\
                               fieldName=fieldItem['fieldName'],\
                               fieldQcName=fieldItem['fieldQcName'],\
                               fieldType=fieldItem['fieldType'],\
                               fieldIsRequired=fieldItem['fieldIsRequired'],\
                               fieldOrder=fieldItem['fieldOrder']
                               );
        fieldItem = {'fieldGroup':'TestSet',
                     'fieldName':"Test Set Name",
                     'fieldQcName':"TS_NAME",
                     'fieldType':"char",
                     'fieldIsRequired':'True',
                     'fieldOrder':'0'
                    };
        saveFile.makeFieldItem(fieldGroup=fieldItem['fieldGroup'],\
                               fieldName=fieldItem['fieldName'],\
                               fieldQcName=fieldItem['fieldQcName'],\
                               fieldType=fieldItem['fieldType'],\
                               fieldIsRequired=fieldItem['fieldIsRequired'],\
                               fieldOrder=fieldItem['fieldOrder']
                               );
        del saveFile

        self.gui.guiQcTestXField.lineEdit_settingName.setText("Setting_2")##input name of setting to be save
        self.gui.pushButton_QCTestX_OpenFieldFile_clicked();
        
        
        mapping_fields = self.gui.qCTestXUserField.get_mappingFields()
        
        ##Now open xml file with setting name "Setting_1" and get mapping of this setting
        openXmlFile = OpenFile(self.savingType, self.qcType);
        mapping_fields_cmp = openXmlFile.get_mappingfields("Setting_2", self.savingType);
        ##Because 'FieldName'='Dupa' is not present in the QualityCenter Field dict(), then this filed from XML file will not insert to TreeView_UserField
        mapping_fields_cmp = {'TestCase': {}, 
                              'Step': {}, 
                              'TestSet': {
                                          'Test Set Name': {'FieldOrder':'0',
                                                       'FieldQcName':'TS_NAME',
                                                       'FieldType':'char',
                                                       'FieldIsRequired':'True'}
                                          }
                              }
        ##Check if TreeView_UserField has the same data as XML "Setting_1"
        self.assertEqual(mapping_fields, mapping_fields_cmp);
        

    
    def testOpenFile_getSetting_1(self):
        '''
        Try to find saved setting with wrong name. Exception will be raised
        '''
        
        self.testSaveFile_4()##create xml file
        
        openXmlFile = OpenFile(self.savingType, self.qcType);
        
        callableFunction = partial(openXmlFile.get_mappingfields, "Setting_WrongName", self.savingType);
        self.assertRaises(SettingNameException, callableFunction);
    
    def testOpenFile_getSetting_2(self):
        '''
        Check if function get_mappingfields() is working correct and is returning the same dict(), as it was saved previously to the file
        '''
        
        self.testSaveFile_4()##create xml file
        
        openXmlFile = OpenFile(self.savingType, self.qcType);
        
        mapping_fields_cmp = openXmlFile.get_mappingfields("Setting_1", self.savingType);
        print self.mappingFieldsUser
        print mapping_fields_cmp; 
        self.assertEqual(self.mappingFieldsUser, mapping_fields_cmp);
    
    
    def testOpenFile_getSetting_3(self):
        '''
        Check if function get_mappingfields() is working correct and is returnig the same dict(), as it was saved previously to the file
        '''
        
        self.testSaveFile_4()##create xml file with 'Setting_1'
        
        
        ##add to created xml file 'Setting_2'                
        saveFile = SaveFile(self.savingType, self.qcType);
        saveFile.makesetting("Setting_2", qcType="TestLab")
        fieldItem = {'fieldGroup':'TestSet', ##Field item will not be created, fieldGroup is wrong
                     'fieldName':"TestUserName_01",
                     'fieldQcName':"TC_User_01",
                     'fieldType':"char",
                     'fieldIsRequired':'True',
                     'fieldOrder':'0'
                    };
        saveFile.makeFieldItem(fieldGroup=fieldItem['fieldGroup'],\
                               fieldName=fieldItem['fieldName'],\
                               fieldQcName=fieldItem['fieldQcName'],\
                               fieldType=fieldItem['fieldType'],\
                               fieldIsRequired=fieldItem['fieldIsRequired'],\
                               fieldOrder=fieldItem['fieldOrder']
                               );
        self.gui.guiQcTestXField.lineEdit_settingName.setText("Setting_2")##input name of setting to be save
        del saveFile ##this will write file and close it
        
        
        mapping_fields =  {"TestSet": "",
                          "TestCase": "",
                          "Step": "",
                           };
        self.gui.qCTestXUserField.updateModel(mapping_fields) ##Clear TreeView_UserFields
        
        ##Now open xml file with setting name "Setting_2" and get mapping of this setting
        openXmlFile = OpenFile(self.savingType, self.qcType);
        mapping_fields_cmp = openXmlFile.get_mappingfields("Setting_2", self.savingType);
        print "mapping_fields_cmp:", mapping_fields_cmp; 
        
        
        ##Update TreeView_UserFiled  with the given mapping from xml file
        self.gui.qCTestXUserField.updateModel(mapping_fields_cmp) 
        
    def testDeleteFile_1(self):
        '''
        Check if given settingName was removed, by button "pushButton_QCTestX_DeleteFieldFile_clicked()"
        '''
        
        ##create xml file
        self.gui.guiQcTestXField.lineEdit_settingName.setText("Setting_1")##input name of setting to be save
        self.gui.pushButton_QCTestX_SaveFieldFile_clicked();
        
        
        ##input good settingName to be delete
        self.gui.guiQcTestXField.lineEdit_settingName.setText("Setting_1")
        self.gui.pushButton_QCTestX_DeleteFieldFile_clicked()

        ##Now open xml file
        openXmlFile = OpenFile(self.savingType, self.qcType);
        ##Check if given settingName was removed
        self.assertFalse(openXmlFile.itemSettings.has_key("Setting_1"));
        listWidgetSettings = self.gui.guiQcTestXField.listWidget_fieldSavedSettings
        listSettingNames = []; 
        for i in range(listWidgetSettings.count()):
            itemName = str(listWidgetSettings.item(i).text());
            listSettingNames.append(itemName);
            
        self.assertFalse("Setting_1" in listSettingNames);

    def testDeleteFile_2(self):
        '''
        Try to remove not existing settingName
        '''
        
        ##create xml file
        self.gui.guiQcTestXField.lineEdit_settingName.setText("Setting_1")##input name of setting to be save
        self.gui.pushButton_QCTestX_SaveFieldFile_clicked();
        
        
        ##input wrong settingName to be delete
        self.gui.guiQcTestXField.lineEdit_settingName.setText("Setting_2")
        self.assertFalse(self.gui.pushButton_QCTestX_DeleteFieldFile_clicked())

        ##Now open xml file
        openXmlFile = OpenFile(self.savingType, self.qcType);
        ##Check if given settingName was removed
        self.assertTrue(openXmlFile.itemSettings.has_key("Setting_1"));
        
    def testDeleteFile_3(self):
        '''
        Try to remove empty settingName
        '''
        
        ##create xml file
        self.gui.guiQcTestXField.lineEdit_settingName.setText("Setting_1")##input name of setting to be save
        self.gui.pushButton_QCTestX_SaveFieldFile_clicked();
        
        
        ##input wrong settingName to be delete
        self.gui.guiQcTestXField.lineEdit_settingName.clear();
        self.assertFalse(self.gui.pushButton_QCTestX_DeleteFieldFile_clicked())

        ##Now open xml file
        openXmlFile = OpenFile(self.savingType, self.qcType);
        ##Check if given settingName was removed
        self.assertTrue(openXmlFile.itemSettings.has_key("Setting_1"));
    
    def testRefresh_1_NOK(self):
        '''
        Try to push button Refresh on the setting file. 
        XML setting file does not exists
        '''
#        self.gui.pushButton_QCTestX_RefreshFieldFile_clicked();
#        self.assertFalse(status, "Refresh button run, even if given setting XML file does not exists")
        
        callableFunction = partial(self.gui.pushButton_QCTestX_RefreshFieldFile_clicked);
        self.assertRaises(RefreshButtonException, callableFunction);
        
        
    def testRefresh_2_OK(self):
        '''
        Try to push button Refresh on the setting file
        '''
        
#        callableFunction = partial(self.gui.pushButton_QCTestX_RefreshFieldFile_clicked);
#        self.assertRaises(OpenFileException, callableFunction);
        filename = 'testxml.xml';
        saveFile = SaveFile(self.savingType, self.qcType);
        
        saveFile.makesetting("Setting_1", qcType="TestLab")
        fieldItem = {'fieldGroup':'Dupa', ##Field item will not be created, fieldGroup is wrong
                     'fieldName':"TestUserName_01",
                     'fieldQcName':"TC_User_01",
                     'fieldType':"char",
                     'fieldIsRequired':'True',
                     'fieldOrder':'1'
                    };
        saveFile.makeFieldItem(fieldGroup=fieldItem['fieldGroup'],\
                               fieldName=fieldItem['fieldName'],\
                               fieldQcName=fieldItem['fieldQcName'],\
                               fieldType=fieldItem['fieldType'],\
                               fieldIsRequired=fieldItem['fieldIsRequired'],\
                               fieldOrder=fieldItem['fieldOrder']
                               );
        self.gui.guiQcTestXField.lineEdit_settingName.setText("Setting_1")##input name of setting to be save
        del saveFile ##this will write file and close it
        
        
        
        
        status = self.gui.pushButton_QCTestX_RefreshFieldFile_clicked();
        self.assertTrue(status, "Refresh button run, even if given setting XML file does not exists")        
    
    def testRefresh_3_NOK(self):
        '''
        Try to push button Refresh on the setting file
        Saved before XML file has only rootSetting=UL. 
        Should not get rootSetting=DL, so the setting list will be empty
        '''
        
#        callableFunction = partial(self.gui.pushButton_QCTestX_RefreshFieldFile_clicked);
#        self.assertRaises(OpenFileException, callableFunction);
        savingType = "UL";
        saveFile = SaveFile(savingType, self.qcType);
        
        saveFile.makesetting("Setting_1", qcType="TestLab")
        fieldItem = {'fieldGroup':'Dupa', ##Field item will not be created, fieldGroup is wrong
                     'fieldName':"TestUserName_01",
                     'fieldQcName':"TC_User_01",
                     'fieldType':"char",
                     'fieldIsRequired':'True',
                     'fieldOrder':'1'
                    };
        saveFile.makeFieldItem(fieldGroup=fieldItem['fieldGroup'],\
                               fieldName=fieldItem['fieldName'],\
                               fieldQcName=fieldItem['fieldQcName'],\
                               fieldType=fieldItem['fieldType'],\
                               fieldIsRequired=fieldItem['fieldIsRequired'],\
                               fieldOrder=fieldItem['fieldOrder']
                               );
        self.gui.guiQcTestXField.lineEdit_settingName.setText("Setting_1")##input name of setting to be save
        del saveFile ##this will write file and close it

        
        callableFunction = partial(self.gui.pushButton_QCTestX_RefreshFieldFile_clicked);
#        self.assertFalse(self.gui.pushButton_QCTestX_RefreshFieldFile_clicked())
        self.assertRaises(RefreshButtonException, callableFunction);
#        status = self.gui.pushButton_QCTestX_RefreshFieldFile_clicked();
#        self.assertFalse(status, "Refresh button run, even if given setting XML file does not exists")    



    def testRefresh_4_NOK(self):
        '''
        Try to push button Refresh on the setting file
        Write something to the setting XML file. 
        In this situation file should not open 
        '''
        
#        callableFunction = partial(self.gui.pushButton_QCTestX_RefreshFieldFile_clicked);
#        self.assertRaises(OpenFileException, callableFunction);
        filename = 'testxml.xml';
        saveFile = SaveFile(self.savingType, self.qcType);
        
        saveFile.makesetting("Setting_1", qcType="TestLab")
        fieldItem = {'fieldGroup':'Dupa', ##Field item will not be created, fieldGroup is wrong
                     'fieldName':"TestUserName_01",
                     'fieldQcName':"TC_User_01",
                     'fieldType':"char",
                     'fieldIsRequired':'True',
                     'fieldOrder':'1'
                    };
        saveFile.makeFieldItem(fieldGroup=fieldItem['fieldGroup'],\
                               fieldName=fieldItem['fieldName'],\
                               fieldQcName=fieldItem['fieldQcName'],\
                               fieldType=fieldItem['fieldType'],\
                               fieldIsRequired=fieldItem['fieldIsRequired'],\
                               fieldOrder=fieldItem['fieldOrder']
                               );
        self.gui.guiQcTestXField.lineEdit_settingName.setText("Setting_1")##input name of setting to be save
        del saveFile ##this will write file and close it
        f = open(self.filename, 'r+')
        f.write('wrongXMLword');##write wrong xml style 
        f.flush();
        f.close();
        
        
        callableFunction = partial(self.gui.pushButton_QCTestX_RefreshFieldFile_clicked);
        self.assertRaises(RefreshButtonException, callableFunction);
        

    def testRefresh_5_NOK(self):
        '''
        Try to push button Refresh on the setting file
        Write something to the already created file, without of regenerate new CRC sum. 
        In this situation file should not open 
        '''
        
#        callableFunction = partial(self.gui.pushButton_QCTestX_RefreshFieldFile_clicked);
#        self.assertRaises(OpenFileException, callableFunction);
        filename = 'testxml.xml';
        saveFile = SaveFile(self.savingType, self.qcType);
        
        saveFile.makesetting("Setting_1", qcType="TestLab")
        fieldItem = {'fieldGroup':'Dupa', ##Field item will not be created, fieldGroup is wrong
                     'fieldName':"TestUserName_01",
                     'fieldQcName':"TC_User_01",
                     'fieldType':"char",
                     'fieldIsRequired':'True',
                     'fieldOrder':'1'
                    };
        saveFile.makeFieldItem(fieldGroup=fieldItem['fieldGroup'],\
                               fieldName=fieldItem['fieldName'],\
                               fieldQcName=fieldItem['fieldQcName'],\
                               fieldType=fieldItem['fieldType'],\
                               fieldIsRequired=fieldItem['fieldIsRequired'],\
                               fieldOrder=fieldItem['fieldOrder']
                               );
        self.gui.guiQcTestXField.lineEdit_settingName.setText("Setting_1")##input name of setting to be save
        del saveFile ##this will write file and close it


        ##for one saved file add some more lines and update CRC sum
        saveFile = SaveFile(self.savingType, self.qcType);
        saveFile.makesetting("Setting_10", qcType="TestLab")
        fieldItem = {'fieldGroup':'TestSet',
                     'fieldName':"TestSetUserName_03",
                     'fieldQcName':"TS_Name_01",
                     'fieldType':"char",
                     'fieldIsRequired':'True',
                     'fieldOrder':'2'
                    };
        saveFile.makeFieldItem(fieldGroup=fieldItem['fieldGroup'],\
                               fieldName=fieldItem['fieldName'],\
                               fieldQcName=fieldItem['fieldQcName'],\
                               fieldType=fieldItem['fieldType'],\
                               fieldIsRequired=fieldItem['fieldIsRequired'],\
                               fieldOrder=fieldItem['fieldOrder']
                               );
        try:
            outFile = open(self.filename, 'wb');
        except IOError, err:
            log.error("ERR: Unable to create file. ", err);
            return
#        self.genCRC();
        saveFile.tree.write(outFile, encoding="UTF-8")
        outFile.close();
        
        
        
        callableFunction = partial(self.gui.pushButton_QCTestX_RefreshFieldFile_clicked);
        self.assertRaises(RefreshButtonException, callableFunction);







class TestXMLUploadSettingOperation(unittest.TestCase):
    '''
    Check if in ChooseFields, XML operation, like Open,Save,Delete,Refresh are working
    '''


    def setUp(self):
        self.savingType = "UL";
        self.qcConnect = None;
        self.qcType = "TestLab"
#        self.qcConnect = qc_connect_disconnect.QC_connect(server, userName, password, domainName,projectName);
#        for result in self.qcConnect.connect():
#            print result;
#        self.qc = self.qcConnect.qc;
#        
        self.filename = r'D:\Eclipse_workspace\QC_gui\src\testUnit\testxml.xml';
        ##to be sure that there was no other files created before
        try: os.remove(self.filename);
        except: pass;
        
        
        self.main = QtGui.QWidget()
        self.gui = QcTestXUploadChooseFields(type="TestLab", parent=self.main, qcConnect=self.qcConnect)
        
        self.treeViewQcField = self.gui.qCTestXQcField.treeView
        self.treeViewUserField = self.gui.qCTestXUserField.treeView
        modelQcField = self.treeViewQcField.model();
        
        
        
        self.mappingFieldsUser = {"TestSet": {'Test Set Name':{'FieldOrder':'0',
                                                       'FieldQcName':'TS_NAME',
                                                       'FieldName':'QC Test Set Name',
                                                       'FieldType':'char',
                                                       'FieldIsRequired':'True'},
                                      'Test Set Directory': {'FieldOrder':'1',
                                                             'FieldQcName':'TS_DIR',
                                                             'FieldName':'QC Test Set Directory',
                                                             'FieldType':'char',
                                                             'FieldIsRequired':'True'},
                                      'Test Set Description': {'FieldOrder':'2',
                                                               'FieldQcName':'TS_DESC',
                                                               'FieldName':'QC Test Set Description',
                                                               'FieldType':'char',
                                                               'FieldIsRequired':'True'}
                                      },
                           "TestCase": {'Test Case Name': {'FieldOrder':'0',
                                                          'FieldQcName':'TC_NAME',
                                                          'FieldName':'QC Test Case Name',
                                                          'FieldType':'char',
                                                          'FieldIsRequired':'True'},
                                        'Test Case Directory': {'FieldOrder':'1',
                                                              'FieldQcName':'TC_DIR',
                                                              'FieldName':'QC Test Case Directory',
                                                              'FieldType':'char',
                                                              'FieldIsRequired':'True'},
                                        'Test Case Status': {'FieldOrder':'2',
                                                              'FieldQcName':'TC_STATUS',
                                                              'FieldName':'QC Test Case Status',
                                                              'FieldType':'char',
                                                              'FieldIsRequired':'True'},
                                        },
                           "Step": {'Step Name': {'FieldOrder':'0',
                                                  'FieldQcName':'ST_NAME',
                                                  'FieldName':'QC Step Name',
                                                  'FieldType':'char',
                                                  'FieldIsRequired':'True'},
                                    'Step Description': {'FieldOrder':'1',
                                                  'FieldQcName':'ST_DESC',
                                                  'FieldName':'QC Step Description',
                                                  'FieldType':'char',
                                                  'FieldIsRequired':'True'}
                                    }
                           };
        self.gui.qCTestXUserField.updateModel(self.mappingFieldsUser);
        
        
        
        self.mappingFieldsQc = {
                            "CYCLE": {'Test Set Name':{'FieldQcName':'TS_NAME',
                                                       'FieldType':'char',
                                                       'FieldIsRequired':'True'},
                                      'Test Set Directory': {'FieldQcName':'TS_DIR',
                                                             'FieldType':'char',
                                                             'FieldIsRequired':'True'},
                                      'Test Set Description': {'FieldQcName':'TS_DESC',
                                                               'FieldType':'char',
                                                               'FieldIsRequired':'True'}
                                      },
                               "TESTCYCL": {'Test Case Name': {'FieldQcName':'TC_NAME',
                                                          'FieldType':'char',
                                                          'FieldIsRequired':'True'},
                                            'Test Case Directory': {'FieldQcName':'TC_DIR',
                                                              'FieldType':'char',
                                                              'FieldIsRequired':'True'},
                                            'Test Case Status': {'FieldQcName':'TC_STATUS',
                                                              'FieldType':'char',
                                                              'FieldIsRequired':'True'},
                                        },
                               "STEP": {'Step Name': {'FieldQcName':'ST_NAME',
                                                  'FieldType':'char',
                                                  'FieldIsRequired':'True'},
                                        'Step Description': {'FieldQcName':'ST_DESC',
                                                  'FieldType':'char',
                                                  'FieldIsRequired':'True'}
                                    }
                             };
        self.gui.qCTestXQcField.updateModelTestLab(self.mappingFieldsQc);
        
        ##to be sure that there was no other files created before
        try: os.remove(self.filename);
        except: pass;
        
        self.main.show();
        
        

    def tearDown(self):
        self.treeViewQcField.expandAll();
        self.treeViewUserField.expandAll();
        self.main.repaint();
#        sleep(5);
        self.main.close();
        
        
#        os.remove(os.getcwd()+'\\'+self.filename);
        
#        self.qcConnect.disconnect_QC();
        

    def __addFields(self):
        
        ##Set Selection on item in the TreeView
        modelQcField = self.treeViewQcField.model()
        modelUserField = self.treeViewUserField.model();
        #======================================================================
        # Add one
        #======================================================================
        parentQcIndex = modelQcField.index(0,0, self.treeViewQcField.rootIndex());
        itemQcIndex = modelQcField.index(0,0, parentQcIndex);
        self.treeViewQcField.setCurrentIndex(itemQcIndex)
        
        ##Start add and removing operation on selected item to the Tree of User Fields
        itemQcField = self.treeViewQcField.currentIndex().internalPointer();
        itemUserField = self.treeViewUserField.currentIndex().internalPointer();
        if (itemQcField is not None) and (itemUserField is not None):
            ##Add selected item to the Tree of User Fields
            self.gui.qCTestXUserField.editItem(itemUserField, itemQcField);
        else:
            print("WRN: User did not selected QC Field and/or QC User Field to be moved");
#            log.warning("WRN: User did not selected QC Field to be moved");
            status = True
            pass;
        

        #======================================================================
        # Add second item
        #======================================================================
        
        ##Set Selection on item in the TreeView
        itemQcIndex = modelQcField.index(0,0, parentQcIndex);
        self.treeViewQcField.setCurrentIndex(itemQcIndex)
        
        ##Start add and removing operation on selected item to the Tree of User Fields
        itemQcField = self.treeViewQcField.currentIndex().internalPointer();
        itemUserField = self.treeViewUserField.currentIndex().internalPointer();
        if (itemQcField is not None) and (itemUserField is not None):
            ##Add selected item to the Tree of User Fields
            self.gui.qCTestXUserField.editItem(itemUserField, itemQcField);
        else:
            print("WRN: User did not selected QC Field and/or QC User Field to be moved");
#            log.warning("WRN: User did not selected QC Field to be moved");
            status = True
            pass;


        #======================================================================
        # Add third item
        #======================================================================
        
        ##Set Selection on item in the TreeView
        itemQcIndex = modelQcField.index(0,0, parentQcIndex);
        self.treeViewQcField.setCurrentIndex(itemQcIndex)
        
        ##Start add and removing operation on selected item to the Tree of User Fields
        itemQcField = self.treeViewQcField.currentIndex().internalPointer();
        itemUserField = self.treeViewUserField.currentIndex().internalPointer();
        if (itemQcField is not None) and (itemUserField is not None):
            ##Add selected item to the Tree of User Fields
            self.gui.qCTestXUserField.editItem(itemUserField, itemQcField);
        else:
            print("WRN: User did not selected QC Field and/or QC User Field to be moved");
#            log.warning("WRN: User did not selected QC Field to be moved");
            status = True
            pass;

        #======================================================================
        # Fourth item
        #======================================================================
        
        ##Set Selection on item in the TreeView
        parentQcIndex = modelQcField.index(1,0, self.treeViewQcField.rootIndex());
        itemQcIndex = modelQcField.index(0,0, parentQcIndex);
        self.treeViewQcField.setCurrentIndex(itemQcIndex)
        
        ##Start add and removing operation on selected item to the Tree of User Fields
        itemQcField = self.treeViewQcField.currentIndex().internalPointer();
        itemUserField = self.treeViewUserField.currentIndex().internalPointer();
        if (itemQcField is not None) and (itemUserField is not None):
            ##Add selected item to the Tree of User Fields
            self.gui.qCTestXUserField.editItem(itemUserField, itemQcField);
        else:
            print("WRN: User did not selected QC Field and/or QC User Field to be moved");
#            log.warning("WRN: User did not selected QC Field to be moved");
            status = True
            pass;
        
        #======================================================================
        # Fifth item
        #======================================================================
        
        ##Set Selection on item in the TreeView
        itemQcIndex = modelQcField.index(0,0, parentQcIndex);
        self.treeViewQcField.setCurrentIndex(itemQcIndex)
        
        ##Start add and removing operation on selected item to the Tree of User Fields
        itemQcField = self.treeViewQcField.currentIndex().internalPointer();
        itemUserField = self.treeViewUserField.currentIndex().internalPointer();
        if (itemQcField is not None) and (itemUserField is not None):
            ##Add selected item to the Tree of User Fields
            self.gui.qCTestXUserField.editItem(itemUserField, itemQcField);
        else:
            print("WRN: User did not selected QC Field and/or QC User Field to be moved");
#            log.warning("WRN: User did not selected QC Field to be moved");
            status = True
            pass;
        
        #======================================================================
        # Sixth item
        #======================================================================
        
        ##Set Selection on item in the TreeView
        parentQcIndex = modelQcField.index(2,0, self.treeViewQcField.rootIndex());
        itemQcIndex = modelQcField.index(0,0, parentQcIndex);
        self.treeViewQcField.setCurrentIndex(itemQcIndex)
        
        ##Start add and removing operation on selected item to the Tree of User Fields
        itemQcField = self.treeViewQcField.currentIndex().internalPointer();
        itemUserField = self.treeViewUserField.currentIndex().internalPointer();
        if (itemQcField is not None) and (itemUserField is not None):
            ##Add selected item to the Tree of User Fields
            self.gui.qCTestXUserField.editItem(itemUserField, itemQcField);
        else:
            print("WRN: User did not selected QC Field and/or QC User Field to be moved");
#            log.warning("WRN: User did not selected QC Field to be moved");
            status = True
            pass;


    def testSaveFile_1(self):
        '''
        Save fields items to the XML file 
        There will be two settings to save with field items.
        Check if in proper place and if in correct number fields was saved into the XML file
        '''
        
        filename = self.filename;
        saveFile = SaveFile(self.savingType, self.qcType);
        
        saveFile.makesetting("Setting_1", qcType='TestLab')
        fieldItem = {'fieldGroup':'TestCase',
                     'fieldUserName':"TestUserName_01",
                     'fieldName':"TC_User_01",
                     'fieldType':"char",
                     'fieldIsRequired':'True',
                     'fieldOrder':'1',
                     'fieldQcName':'TC_NAME',
                    };
        saveFile.makeFieldItem(fieldGroup=fieldItem['fieldGroup'],\
                               fieldUserName=fieldItem['fieldUserName'],\
                               fieldName=fieldItem['fieldName'],\
                               fieldType=fieldItem['fieldType'],\
                               fieldIsRequired=fieldItem['fieldIsRequired'],\
                               fieldOrder=fieldItem['fieldOrder'],
                               fieldQcName=fieldItem['fieldQcName']
                               );
        
        
        saveFile.makesetting("Setting_2", qcType='TestPlan')
        fieldItem = {'fieldGroup':'TestCase',
                     'fieldUserName':"TestUserName_02",
                     'fieldName':"TC_User_02",
                     'fieldType':"char",
                     'fieldIsRequired':'True',
                     'fieldOrder':'1',
                     'fieldQcName':'TC_NAME',
                    };
        saveFile.makeFieldItem(fieldGroup=fieldItem['fieldGroup'],\
                               fieldUserName=fieldItem['fieldUserName'],\
                               fieldName=fieldItem['fieldName'],\
                               fieldType=fieldItem['fieldType'],\
                               fieldIsRequired=fieldItem['fieldIsRequired'],\
                               fieldOrder=fieldItem['fieldOrder'],
                               fieldQcName=fieldItem['fieldQcName']
                               );
        saveFile.makeFieldItem(fieldGroup='TestCase',
                               fieldUserName="Regression", 
                               fieldName="TC_User_03", 
                               fieldType="char", 
                               fieldIsRequired='True',
                               fieldOrder='2',
                               fieldQcName='TC_NAME');
        saveFile.makeFieldItem(fieldGroup='TestSet',
                               fieldUserName="TestUserName_02", 
                               fieldName="TC_User_02", 
                               fieldType="char", 
                               fieldIsRequired='True',
                               fieldOrder='1',
                               fieldQcName='TS_NAME');
        self.gui.guiQcTestXField.lineEdit_settingName.setText("Setting_1")##input name of setting to be save
        del saveFile ##this will write file and close it    
        
        
        tree = etree.parse(filename)
        root = tree.getroot();
        self.assertEqual(root.tag, 'xml');
        
        rootSettings = root.find('Settings');
        self.assertEqual(rootSettings.tag, 'Settings');
        
        rootSettingType = rootSettings.find('SettingsUpload');
        self.assertEqual(rootSettingType.tag, 'SettingsUpload');
        
        rootCRC = root.find('CRCsum');
        self.assertEqual(rootCRC.tag, 'CRCsum');
    
        allsetting = rootSettingType.findall('Setting');
        allsettingNames = [el.attrib['name']for el in allsetting];
        self.assertEqual(2, len(allsetting));
        
        self.assertEqual(allsetting[0].attrib['name'], 'Setting_1');
        self.assertEqual(allsetting[1].attrib['name'], 'Setting_2');
        
        setting_1 = allsetting[allsettingNames.index('Setting_1')];
        self.assertEqual(setting_1[0].tag, 'TestSet')
        self.assertEqual(setting_1[1].tag, 'TestCase')
        self.assertEqual(setting_1[2].tag, 'Step')
    
        setting_2 = allsetting[allsettingNames.index('Setting_2')];
        self.assertEqual(setting_2[0].tag, 'TestSet')
        self.assertEqual(setting_2[1].tag, 'TestCase')
        self.assertEqual(setting_2[2].tag, 'Step')
    
        self.assertEqual(len(setting_1[1]), 1); ##only one Field item in the TestCase - Setting_1
        fieldItem = setting_1[1][0];
        fieldItemValue = {'FieldIsRequired':'True',
                          'FieldUserName':"TestUserName_01",
                          'FieldName':"TC_User_01",
                          'FieldType':"char",
                          'FieldOrder':'1',
                          'FieldQcName':'TC_NAME',
                          };
        self.assertEqual(fieldItem.attrib,fieldItemValue); 

        self.assertEqual(len(setting_2[0]), 1); ##only one Field item in the TestCase - Setting_2
        self.assertEqual(len(setting_2[1]), 2); ##only two Field item in the TestCase - Setting_2
        self.assertEqual(len(setting_2[2]), 0); ##none Field item in the TestCase - Setting_2
        fieldItem = setting_2[1][0];##field Item of Setting_2 -> TestCase -> first
        fieldItemValue = {'FieldIsRequired':'True',
                          'FieldUserName':"TestUserName_02",
                          'FieldName':"TC_User_02",
                          'FieldType':"char",
                          'FieldOrder':'1',
                          'FieldQcName':'TC_NAME',
                          };
        self.assertEqual(fieldItem.attrib,fieldItemValue);




        
    

    def testSaveFile_2(self):
        '''
        Save fields, but by direct operation.
        Field item should not be saved, due to the wrong FieldGroup
        '''
        
        filename = 'testxml.xml';
        saveFile = SaveFile(self.savingType, self.qcType);
        
        saveFile.makesetting("Setting_1", qcType='TestLab')
        fieldItem = {'fieldGroup':'Dupa', ##Field item will not be created, fieldGroup is wrong
                     'fieldUserName':"TestUserName_01",
                     'fieldName':"TC_User_01",
                     'fieldType':"char",
                     'fieldIsRequired':'True',
                     'fieldOrder':'1',
                     'fieldQcName':'TC_NAME',
                    };
        saveFile.makeFieldItem(fieldGroup=fieldItem['fieldGroup'],\
                               fieldUserName=fieldItem['fieldUserName'],\
                               fieldName=fieldItem['fieldName'],\
                               fieldType=fieldItem['fieldType'],\
                               fieldIsRequired=fieldItem['fieldIsRequired'],\
                               fieldOrder=fieldItem['fieldOrder'],
                               fieldQcName=fieldItem['fieldQcName']
                               );
        self.gui.guiQcTestXField.lineEdit_settingName.setText("Setting_1")##input name of setting to be save
        del saveFile ##this will write file and close it
        
        
        tree = etree.parse(self.filename)
        root = tree.getroot();
        self.assertEqual(root.tag, 'xml');
        
        rootSettings = root.find('Settings');
        self.assertEqual(rootSettings.tag, 'Settings');
        
        rootSettingType = rootSettings.find('SettingsUpload');
        self.assertEqual(rootSettingType.tag, 'SettingsUpload');
        
        rootCRC = root.find('CRCsum');
        self.assertEqual(rootCRC.tag, 'CRCsum');
    
        allsetting = rootSettingType.findall('Setting');
        allsettingNames = [el.attrib['name']for el in allsetting];
        self.assertEqual(1, len(allsetting));
        
        self.assertEqual(allsetting[0].attrib['name'], 'Setting_1');
        self.assertEqual(allsetting[0].attrib['name'], 'Setting_1');
        
        
        setting_1 = allsetting[allsettingNames.index('Setting_1')];
        self.assertEqual(len(setting_1),3);
        
        self.assertEqual(len(setting_1[0]), 0); ##none Field item in the TestCase - Setting_2
        self.assertEqual(len(setting_1[1]), 0); ##none Field item in the TestCase - Setting_2
        self.assertEqual(len(setting_1[2]), 0); ##none Field item in the TestCase - Setting_2

    def testSaveFile_3(self):
        '''
        Save double the same file, to check if previous saved setting are still present or overwritten
        '''
        self.testSaveFile_1(); ##create xml file with two saved settings
        
        
        filename = self.filename;
        saveFile = SaveFile(self.savingType, self.qcType);
        
        saveFile.makesetting("Setting_1", qcType='TestLab')
        fieldItem = {'fieldGroup':'TestSet',
                     'fieldUserName':"TestSetUserName_03",
                     'fieldName':"TS_Name_01",
                     'fieldType':"char",
                     'fieldIsRequired':'True',
                     'fieldOrder':'2',
                     'fieldQcName':'TC_NAME',
                    };
        saveFile.makeFieldItem(fieldGroup=fieldItem['fieldGroup'],\
                               fieldUserName=fieldItem['fieldUserName'],\
                               fieldName=fieldItem['fieldName'],\
                               fieldType=fieldItem['fieldType'],\
                               fieldIsRequired=fieldItem['fieldIsRequired'],\
                               fieldOrder=fieldItem['fieldOrder'],
                               fieldQcName=fieldItem['fieldQcName']
                               );
        saveFile.makesetting("Setting_3", qcType='TestLab')
        fieldItem = {'fieldGroup':'TestSet',
                     'fieldUserName':"TestSetUserName_03",
                     'fieldName':"TS_Name_01",
                     'fieldType':"char",
                     'fieldIsRequired':'True',
                     'fieldOrder':'2',
                     'fieldQcName':'TC_NAME',
                    };
        saveFile.makeFieldItem(fieldGroup=fieldItem['fieldGroup'],\
                               fieldUserName=fieldItem['fieldUserName'],\
                               fieldName=fieldItem['fieldName'],\
                               fieldType=fieldItem['fieldType'],\
                               fieldIsRequired=fieldItem['fieldIsRequired'],\
                               fieldOrder=fieldItem['fieldOrder'],
                               fieldQcName=fieldItem['fieldQcName']
                               );
        self.gui.guiQcTestXField.lineEdit_settingName.setText("Setting_1")##input name of setting to be save
        del saveFile;                       
                               
        tree = etree.parse(filename)
        root = tree.getroot();
        self.assertEqual(root.tag, 'xml');
        
        
        
        rootSettings = root.find('Settings');
        self.assertEqual(rootSettings.tag, 'Settings');
        
        rootSettingType = rootSettings.find('SettingsUpload');
        self.assertEqual(rootSettingType.tag, 'SettingsUpload');
        
        rootCRC = root.find('CRCsum');
        self.assertEqual(rootCRC.tag, 'CRCsum');
    
        allsetting = rootSettingType.findall('Setting');
        allsettingNames = [el.attrib['name']for el in allsetting];
        self.assertEqual(3, len(allsetting));
        
        self.assertTrue('Setting_1' in [el.attrib['name']for el in allsetting]);
        
        
        setting_1 = allsetting[allsettingNames.index('Setting_1')];
        self.assertEqual(len(setting_1),3);
        self.assertEqual(len(setting_1[0]), 1); ##only one Field item in the TestSet - Setting_1
        self.assertEqual(len(setting_1[1]), 0); ##none Field item in the TestCase - Setting_1
        self.assertEqual(len(setting_1[2]), 0); ##none Field item in the Step - Setting_1

        setting_2 = allsetting[allsettingNames.index('Setting_2')];
        self.assertEqual(len(setting_2),3);
        self.assertEqual(len(setting_2[0]), 1); ##only one Field item in the TestSet - Setting_2
        self.assertEqual(len(setting_2[1]), 2); ##only two Field item in the TestCase - Setting_2
        self.assertEqual(len(setting_2[2]), 0); ##none Field item in the Step - Setting_2

        setting_3 = allsetting[allsettingNames.index('Setting_3')];
        self.assertEqual(len(setting_3),3);
        self.assertEqual(len(setting_3[0]), 1); ##only one Field item in the TestSet - Setting_3
        self.assertEqual(len(setting_3[1]), 0); ##none Field item in the TestCase - Setting_3
        self.assertEqual(len(setting_3[2]), 0); ##none Field item in the Step - Setting_3
        
        
    def testSaveFile_4(self):
        '''
        Save field from TreeView to XML file
        '''
        self.gui.guiQcTestXField.lineEdit_settingName.setText("Setting_1")##input name of setting to be save
        self.gui.pushButton_QCTestX_SaveFieldFile_clicked();
        ##dataUserField = ('fieldUserName', ('fieldName', 'char', 'True'))
        
        tree = etree.parse(self.filename)
        root = tree.getroot();
        goldenXML = "<xml><Settings><SettingsUpload><Setting QCtype=\"TestLab\" name=\"Setting_1\"><TestSet><Field FieldIsRequired=\"True\" FieldName=\"QC Test Set Name\" FieldOrder=\"0\" FieldQcName=\"TS_NAME\" FieldType=\"char\" FieldUserName=\"Test Set Name\" /><Field FieldIsRequired=\"True\" FieldName=\"QC Test Set Description\" FieldOrder=\"2\" FieldQcName=\"TS_DESC\" FieldType=\"char\" FieldUserName=\"Test Set Description\" /><Field FieldIsRequired=\"True\" FieldName=\"QC Test Set Directory\" FieldOrder=\"1\" FieldQcName=\"TS_DIR\" FieldType=\"char\" FieldUserName=\"Test Set Directory\" /></TestSet><TestCase><Field FieldIsRequired=\"True\" FieldName=\"QC Test Case Directory\" FieldOrder=\"1\" FieldQcName=\"TC_DIR\" FieldType=\"char\" FieldUserName=\"Test Case Directory\" /><Field FieldIsRequired=\"True\" FieldName=\"QC Test Case Status\" FieldOrder=\"2\" FieldQcName=\"TC_STATUS\" FieldType=\"char\" FieldUserName=\"Test Case Status\" /><Field FieldIsRequired=\"True\" FieldName=\"QC Test Case Name\" FieldOrder=\"0\" FieldQcName=\"TC_NAME\" FieldType=\"char\" FieldUserName=\"Test Case Name\" /></TestCase><Step><Field FieldIsRequired=\"True\" FieldName=\"QC Step Description\" FieldOrder=\"1\" FieldQcName=\"ST_DESC\" FieldType=\"char\" FieldUserName=\"Step Description\" /><Field FieldIsRequired=\"True\" FieldName=\"QC Step Name\" FieldOrder=\"0\" FieldQcName=\"ST_NAME\" FieldType=\"char\" FieldUserName=\"Step Name\" /></Step></Setting></SettingsUpload></Settings><CRCsum>3108727816</CRCsum></xml>"
        
        print 'GoldenXML:', etree.tostring(root, 'UTF-8');
        self.assertTrue(goldenXML in etree.tostring(root, 'UTF-8'));
    
        
    def testSaveFile_5(self):
        '''
        Save field from TreeView to XML file. Check if saving the same SeetingName will save double or not  
        '''
        self.gui.guiQcTestXField.lineEdit_settingName.setText("Setting_1")##input name of setting to be save
        self.gui.pushButton_QCTestX_SaveFieldFile_clicked();
        ##dataUserField = ('fieldUserName', ('fieldName', 'char', 'True'))
        
        self.gui.guiQcTestXField.lineEdit_settingName.setText("Setting_1")##input name of setting to be save
        self.gui.pushButton_QCTestX_SaveFieldFile_clicked();
        
        
        tree = etree.parse(self.filename)
        root = tree.getroot();
        goldenXML = "<xml><Settings><SettingsUpload><Setting QCtype=\"TestLab\" name=\"Setting_1\"><TestSet><Field FieldIsRequired=\"True\" FieldName=\"QC Test Set Name\" FieldOrder=\"0\" FieldQcName=\"TS_NAME\" FieldType=\"char\" FieldUserName=\"Test Set Name\" /><Field FieldIsRequired=\"True\" FieldName=\"QC Test Set Description\" FieldOrder=\"2\" FieldQcName=\"TS_DESC\" FieldType=\"char\" FieldUserName=\"Test Set Description\" /><Field FieldIsRequired=\"True\" FieldName=\"QC Test Set Directory\" FieldOrder=\"1\" FieldQcName=\"TS_DIR\" FieldType=\"char\" FieldUserName=\"Test Set Directory\" /></TestSet><TestCase><Field FieldIsRequired=\"True\" FieldName=\"QC Test Case Directory\" FieldOrder=\"1\" FieldQcName=\"TC_DIR\" FieldType=\"char\" FieldUserName=\"Test Case Directory\" /><Field FieldIsRequired=\"True\" FieldName=\"QC Test Case Status\" FieldOrder=\"2\" FieldQcName=\"TC_STATUS\" FieldType=\"char\" FieldUserName=\"Test Case Status\" /><Field FieldIsRequired=\"True\" FieldName=\"QC Test Case Name\" FieldOrder=\"0\" FieldQcName=\"TC_NAME\" FieldType=\"char\" FieldUserName=\"Test Case Name\" /></TestCase><Step><Field FieldIsRequired=\"True\" FieldName=\"QC Step Description\" FieldOrder=\"1\" FieldQcName=\"ST_DESC\" FieldType=\"char\" FieldUserName=\"Step Description\" /><Field FieldIsRequired=\"True\" FieldName=\"QC Step Name\" FieldOrder=\"0\" FieldQcName=\"ST_NAME\" FieldType=\"char\" FieldUserName=\"Step Name\" /></Step></Setting></SettingsUpload></Settings><CRCsum>3108727816</CRCsum></xml>"
        print 'testSaveFile_5 GoldenXML:', etree.tostring(root, 'UTF-8');
        self.assertTrue(goldenXML in etree.tostring(root, 'UTF-8'));

    def testSaveFile_6(self):
        '''
        Save field from TreeView to XML file. In this case user did not mapped any FieldUserName
        '''

        mappingFieldsUser = {"TestSet": {'Test Set Name':{'FieldOrder':'0'},
                                      'Test Set Directory': {'FieldOrder':'1'},
                                      'Test Set Description': {'FieldOrder':'2'}
                                      },
                           "TestCase": {'Test Case Name': {'FieldOrder':'0'},
                                        'Test Case Directory': {'FieldOrder':'1'},
                                        'Test Case Status': {'FieldOrder':'2'},
                                        },
                           "Step": {'Step Name': {'FieldOrder':'0'},
                                    'Step Description': {'FieldOrder':'1'}
                                    }
                           };
        self.gui.qCTestXUserField.updateModel(mappingFieldsUser);
        
        
        self.gui.guiQcTestXField.lineEdit_settingName.setText("Setting_1")##input name of setting to be save
        self.gui.pushButton_QCTestX_SaveFieldFile_clicked();
        ##dataUserField = ('fieldUserName', ('fieldName', 'char', 'True'))
        
        tree = etree.parse(self.filename)
        root = tree.getroot();
        goldenXML = "<xml><Settings><SettingsUpload><Setting QCtype=\"TestLab\" name=\"Setting_1\"><TestSet /><TestCase /><Step /></Setting></SettingsUpload></Settings><CRCsum>743608019</CRCsum></xml>"
        print 'testSaveFile_6 GoldenXML:', etree.tostring(root, 'UTF-8');
        self.assertTrue(goldenXML in etree.tostring(root, 'UTF-8'));
        
    def testOpenFile_1(self): 
        '''
        Open xml file which does not exists
        '''
        callableFunction = partial(OpenFile, 
                                   self.savingType,
                                   self.qcType);
        self.assertRaises(OpenFileException, callableFunction);
        
    def testOpenFile_2(self): 
        '''
        Open xml file which exists but has wrong xml style
        '''
        self.testSaveFile_4()##create xml file
        
        
        f = open(self.filename, 'r+')
        f.write('wrongXMLword');##write wrong xml style 
        f.flush();
        f.close();
        callableFunction = partial(OpenFile, 
                                   self.savingType,
                                   self.qcType
                                   );
        self.assertRaises(ParserException, callableFunction);
        
    def testOpenFile_3(self): 
        '''
        Open xml file which exists but has wrong CRC checksum
        '''
        self.testSaveFile_4()##create xml file
        saveFile = SaveFile(self.savingType, self.qcType);
        
        saveFile.makesetting("Setting_10", qcType='TestLab')
        fieldItem = {'fieldGroup':'TestSet',
                     'fieldUserName':"TestSetUserName_03",
                     'fieldName':"TS_Name_01",
                     'fieldType':"char",
                     'fieldIsRequired':'True',
                     'fieldOrder':'2',
                     'fieldQcName':'TC_NAME',
                    };
        saveFile.makeFieldItem(fieldGroup=fieldItem['fieldGroup'],\
                               fieldUserName=fieldItem['fieldUserName'],\
                               fieldName=fieldItem['fieldName'],\
                               fieldType=fieldItem['fieldType'],\
                               fieldIsRequired=fieldItem['fieldIsRequired'],\
                               fieldOrder=fieldItem['fieldOrder'],
                               fieldQcName=fieldItem['fieldQcName']
                               );
        try:
            outFile = open(self.filename, 'wb');
        except IOError, err:
            log.error("ERR: Unable to create file. ", err);
            return
#        self.genCRC();
        saveFile.tree.write(outFile, encoding="UTF-8")
        outFile.close();
        
        callableFunction = partial(OpenFile, 
                                   self.savingType,
                                   self.qcType
                                   );
        self.assertRaises(CRCexception, callableFunction);
    

    def mappingFields_translate(self, mappingFields):
        mappingFields_translate={};
        for fieldGroup, fieldValues in mappingFields.items():
            mappingFields_translate.update({fieldGroup:{}});#
            for fieldUserName, fieldValues in fieldValues.items():
                if fieldValues.get('FieldQcName') is None:
                    continue;
                fieldName = fieldValues.get('FieldName');
#                fieldUserName = fieldValues.get('FieldUserName');
                fieldType = fieldValues.get('FieldType');
                fieldIsRequired = fieldValues.get('FieldIsRequired');
                fieldQcName = fieldValues.get('FieldQcName');
                fieldOrder = fieldValues.get('FieldOrder');
                fieldValues = {'FieldOrder':fieldOrder,   
                               'FieldUserName':fieldUserName,
                               'FieldType':fieldType,
                               'FieldQcName':fieldQcName,
                               'FieldIsRequired':fieldIsRequired
                               };
                mappingFields_translate[fieldGroup].update({fieldName: fieldValues});
        return mappingFields_translate;
    
    def testOpenFile_4(self): 
        '''
        Check if "pushButton_QCTestX_OpenFieldFile_clicked()" is working good. 
        1. Update and save TreeView_UserFiles with some data to XML file 
        2. Open XML file with setting "Setting_1" and update TreeView_UserFiled with data from XML
            Data from XML file setting "Setting_1" are the same as current seen TreeView_UserFields 
        3. Get current TreeView_UserField and compare this with XML file 
        '''
        self.testSaveFile_4()##create xml file
        saveFile = SaveFile(self.savingType, self.qcType);
        
        saveFile.makesetting("Setting_2", qcType='TestLab')
        fieldItem = {'fieldGroup':'TestSet',
                     'fieldUserName':"QC Test Set Name",
                     'fieldName': "Test Set Name",
                     'fieldType':"char",
                     'fieldIsRequired':'True',
                     'fieldOrder':'2',
                     'fieldQcName':'TS_NAME',
                    };
                    
        saveFile.makeFieldItem(fieldGroup=fieldItem['fieldGroup'],\
                               fieldUserName=fieldItem['fieldUserName'],\
                               fieldName=fieldItem['fieldName'],\
                               fieldType=fieldItem['fieldType'],\
                               fieldIsRequired=fieldItem['fieldIsRequired'],\
                               fieldOrder=fieldItem['fieldOrder'],
                               fieldQcName=fieldItem['fieldQcName']
                               );
        del saveFile

        self.gui.guiQcTestXField.lineEdit_settingName.setText("Setting_2")##input name of setting to be save
        self.gui.pushButton_QCTestX_OpenFieldFile_clicked();
        
        
        mapping_fields = self.gui.qCTestXUserField.get_mappingFields()
        
        ##Now open xml file with setting name "Setting_1" and get mapping of this setting
        openXmlFile = OpenFile(self.savingType, self.qcType);
        mapping_fields_cmp = openXmlFile.get_mappingfields("Setting_2", self.savingType);
        
        mapping_fields_cmp= self.mappingFields_translate(mapping_fields_cmp)
        
        print "MAP1:", mapping_fields_cmp;
        print "CMP_to:", mapping_fields; 
        ##Check if TreeView_UserField has the same data as XML "Setting_1"
#        self.assertTrue(mapping_fields_cmp['TestSet'] in mapping_fields['TestSet']);
        
        fs = frozenset(mapping_fields_cmp['TestSet']);
        self.assertTrue( fs.issubset(mapping_fields['TestSet']));

    def testOpenFile_5(self): 
        '''
        Check if "pushButton_QCTestX_OpenFieldFile_clicked()" is working good. 
        1. Update and save TreeView_UserFiles with some data to XML file 
        2. Open XML file with setting "Setting_2" and update TreeView_UserFiled with data from XML
            Data from XML file setting "Setting_2" are different as current seen TreeView_UserFields
            Additional, one of the Fields in the XML is not in the QC Fields dict(). That is why it will not be put into TreeView_UserFields 
        3. Get current TreeView_UserField and compare this with XML file 
        '''
        self.testSaveFile_4()##create xml file
        saveFile = SaveFile(self.savingType, self.qcType);
        
        saveFile.makesetting("Setting_2", qcType='TestLab')
        fieldItem = {'fieldGroup':'TestSet',
                     'fieldUserName':"Dupa",
                     'fieldName':"TS_Name_01",
                     'fieldType':"char",
                     'fieldIsRequired':'True',
                     'fieldOrder':'2',
                     'fieldQcName':'TC_NAME',
                    };
        saveFile.makeFieldItem(fieldGroup=fieldItem['fieldGroup'],\
                               fieldUserName=fieldItem['fieldUserName'],\
                               fieldName=fieldItem['fieldName'],\
                               fieldType=fieldItem['fieldType'],\
                               fieldIsRequired=fieldItem['fieldIsRequired'],\
                               fieldOrder=fieldItem['fieldOrder'],
                               fieldQcName=fieldItem['fieldQcName']
                               );
        fieldItem = {'fieldGroup':'TestSet',
                     'fieldUserName':"Test Set Name",
                     'fieldName':"QC Test Set Name",
                     'fieldType':"char",
                     'fieldIsRequired':'True',
                     'fieldOrder':'0',
                     'fieldQcName':'TS_NAME',
                    };
        saveFile.makeFieldItem(fieldGroup=fieldItem['fieldGroup'],\
                               fieldUserName=fieldItem['fieldUserName'],\
                               fieldName=fieldItem['fieldName'],\
                               fieldType=fieldItem['fieldType'],\
                               fieldIsRequired=fieldItem['fieldIsRequired'],\
                               fieldOrder=fieldItem['fieldOrder'],
                               fieldQcName=fieldItem['fieldQcName']
                               );
        del saveFile

        self.gui.guiQcTestXField.lineEdit_settingName.setText("Setting_2")##input name of setting to be save
        self.gui.pushButton_QCTestX_OpenFieldFile_clicked();
        
        
        mapping_fields = self.gui.qCTestXUserField.get_mappingFields()
        
        ##Now open xml file with setting name "Setting_1" and get mapping of this setting
        openXmlFile = OpenFile(self.savingType, self.qcType);
        mapping_fields_cmp = openXmlFile.get_mappingfields("Setting_2", self.savingType);
        ##Because 'FieldUserName'='Dupa' is not present in the QualityCenter Field dict(), then this filed from XML file will not insert to TreeView_UserField
#        mapping_fields_cmp = {
#                              'TestCase': {}, 
#                              'Step': {}, 
#                              'TestSet': {}, 
#                              };
                              
        
        
        
        ##Check if TreeView_UserField has the same data as XML "Setting_1"
        fs = frozenset(mapping_fields_cmp['TestSet']);
        self.assertFalse( fs.issubset(mapping_fields['TestSet']));
        
        fs = frozenset(mapping_fields_cmp['TestSet']['Test Set Name']);
        self.assertTrue( fs.issubset(mapping_fields['TestSet']['Test Set Name']));
        
        

    
    def testOpenFile_getSetting_1(self):
        '''
        Try to find saved setting with wrong name. Exception will be raised
        '''
        
        self.testSaveFile_4()##create xml file
        
        openXmlFile = OpenFile(self.savingType, self.qcType);
        
        callableFunction = partial(openXmlFile.get_mappingfields, "Setting_WrongName", self.savingType);
        self.assertRaises(SettingNameException, callableFunction);
    
    def testOpenFile_getSetting_2(self):
        '''
        Check if function get_mappingfields() is working correct and is returning the same dict(), as it was saved previously to the file
        '''
        
        self.testSaveFile_4()##create xml file
        
        openXmlFile = OpenFile(self.savingType, self.qcType);
        
        mapping_fields_cmp = openXmlFile.get_mappingfields("Setting_1", self.savingType);
#        mapping_fields_cmp= self.mappingFields_translate(mapping_fields_cmp)
        golden = {'TestCase': {'Test Case Directory': {'FieldIsRequired': 'True', 'FieldName': 'QC Test Case Directory', 'FieldQcName': 'TC_DIR', 'FieldOrder': '1', 'FieldType': 'char'}, 'Test Case Status': {'FieldIsRequired': 'True', 'FieldName': 'QC Test Case Status', 'FieldQcName': 'TC_STATUS', 'FieldOrder': '2', 'FieldType': 'char'}, 'Test Case Name': {'FieldIsRequired': 'True', 'FieldName': 'QC Test Case Name', 'FieldQcName': 'TC_NAME', 'FieldOrder': '0', 'FieldType': 'char'}}, 'Step': {'Step Description': {'FieldIsRequired': 'True', 'FieldName': 'QC Step Description', 'FieldQcName': 'ST_DESC', 'FieldOrder': '1', 'FieldType': 'char'}, 'Step Name': {'FieldIsRequired': 'True', 'FieldName': 'QC Step Name', 'FieldQcName': 'ST_NAME', 'FieldOrder': '0', 'FieldType': 'char'}}, 'TestSet': {'Test Set Name': {'FieldIsRequired': 'True', 'FieldName': 'QC Test Set Name', 'FieldQcName': 'TS_NAME', 'FieldOrder': '0', 'FieldType': 'char'}, 'Test Set Description': {'FieldIsRequired': 'True', 'FieldName': 'QC Test Set Description', 'FieldQcName': 'TS_DESC', 'FieldOrder': '2', 'FieldType': 'char'}, 'Test Set Directory': {'FieldIsRequired': 'True', 'FieldName': 'QC Test Set Directory', 'FieldQcName': 'TS_DIR', 'FieldOrder': '1', 'FieldType': 'char'}}}
        
        print mapping_fields_cmp; 
        
        self.assertEqual(golden, mapping_fields_cmp);
    
    
    def testOpenFile_getSetting_3(self):
        '''
        Check if function get_mappingfields() is working correct and is returnig the same dict(), as it was saved previously to the file
        '''
        
        self.testSaveFile_4()##create xml file with 'Setting_1'
        
        
        ##add to created xml file 'Setting_2'                
        saveFile = SaveFile(self.savingType, self.qcType);
        saveFile.makesetting("Setting_2", qcType='TestLab')
        fieldItem = {'fieldGroup':'TestSet', ##Field item will not be created, fieldGroup is wrong
                     'fieldUserName':"TestUserName_01",
                     'fieldName':"TC_User_01",
                     'fieldType':"char",
                     'fieldIsRequired':'True',
                     'fieldOrder':'0',
                     'fieldQcName':'TS_NAME'
                    };
        saveFile.makeFieldItem(fieldGroup=fieldItem['fieldGroup'],\
                               fieldUserName=fieldItem['fieldUserName'],\
                               fieldName=fieldItem['fieldName'],\
                               fieldType=fieldItem['fieldType'],\
                               fieldIsRequired=fieldItem['fieldIsRequired'],\
                               fieldOrder=fieldItem['fieldOrder'],\
                               fieldQcName=fieldItem['fieldQcName']
                               );
        self.gui.guiQcTestXField.lineEdit_settingName.setText("Setting_2")##input name of setting to be save
        del saveFile ##this will write file and close it
        
        
        mapping_fields =  {"TestSet": "",
                          "TestCase": "",
                          "Step": "",
                           };
        self.gui.qCTestXUserField.updateModel(mapping_fields) ##Clear TreeView_UserFields
        
        ##Now open xml file with setting name "Setting_2" and get mapping of this setting
        openXmlFile = OpenFile(self.savingType, self.qcType);
        mapping_fields_cmp = openXmlFile.get_mappingfields("Setting_2", self.savingType);
        print "mapping_fields_cmp:", mapping_fields_cmp; 
        
        
        ##Update TreeView_UserFiled  with the given mapping from xml file
        self.gui.qCTestXUserField.updateModel(mapping_fields_cmp) 
        
    def testDeleteFile_1(self):
        '''
        Check if given settingName was removed, by button "pushButton_QCTestX_DeleteFieldFile_clicked()"
        '''
        
        ##create xml file
        self.gui.guiQcTestXField.lineEdit_settingName.setText("Setting_1")##input name of setting to be save
        self.gui.pushButton_QCTestX_SaveFieldFile_clicked();
        
        
        ##input good settingName to be delete
        self.gui.guiQcTestXField.lineEdit_settingName.setText("Setting_1")
        self.gui.pushButton_QCTestX_DeleteFieldFile_clicked()

        ##Now open xml file
        openXmlFile = OpenFile(self.savingType, self.qcType);
        ##Check if given settingName was removed
        self.assertFalse(openXmlFile.itemSettings.has_key("Setting_1"));
        listWidgetSettings = self.gui.guiQcTestXField.listWidget_fieldSavedSettings
        listSettingNames = []; 
        for i in range(listWidgetSettings.count()):
            itemName = str(listWidgetSettings.item(i).text());
            listSettingNames.append(itemName);
            
        self.assertFalse("Setting_1" in listSettingNames);

    def testDeleteFile_2(self):
        '''
        Try to remove not existing settingName
        '''
        
        ##create xml file
        self.gui.guiQcTestXField.lineEdit_settingName.setText("Setting_1")##input name of setting to be save
        self.gui.pushButton_QCTestX_SaveFieldFile_clicked();
        
        
        ##input wrong settingName to be delete
        self.gui.guiQcTestXField.lineEdit_settingName.setText("Setting_2")
        self.assertFalse(self.gui.pushButton_QCTestX_DeleteFieldFile_clicked())

        ##Now open xml file
        openXmlFile = OpenFile(self.savingType, self.qcType);
        ##Check if given settingName was removed
        self.assertTrue(openXmlFile.itemSettings.has_key("Setting_1"));
        
    def testDeleteFile_3(self):
        '''
        Try to remove empty settingName
        '''
        
        ##create xml file
        self.gui.guiQcTestXField.lineEdit_settingName.setText("Setting_1")##input name of setting to be save
        self.gui.pushButton_QCTestX_SaveFieldFile_clicked();
        
        
        ##input wrong settingName to be delete
        self.gui.guiQcTestXField.lineEdit_settingName.clear();
        self.assertFalse(self.gui.pushButton_QCTestX_DeleteFieldFile_clicked())

        ##Now open xml file
        openXmlFile = OpenFile(self.savingType, self.qcType);
        ##Check if given settingName was removed
        self.assertTrue(openXmlFile.itemSettings.has_key("Setting_1"));

    def testRefresh_1_NOK(self):
        '''
        Try to push button Refresh on the setting file. 
        XML setting file does not exists
        '''
#        self.gui.pushButton_QCTestX_RefreshFieldFile_clicked();
#        self.assertFalse(status, "Refresh button run, even if given setting XML file does not exists")
        
        callableFunction = partial(self.gui.pushButton_QCTestX_RefreshFieldFile_clicked);
        self.assertRaises(RefreshButtonException, callableFunction);
        
        
    def testRefresh_2_OK(self):
        '''
        Try to push button Refresh on the setting file
        '''
        
#        callableFunction = partial(self.gui.pushButton_QCTestX_RefreshFieldFile_clicked);
#        self.assertRaises(OpenFileException, callableFunction);
        filename = 'testxml.xml';
        saveFile = SaveFile(self.savingType, self.qcType);
        
        saveFile.makesetting("Setting_1", qcType='TestLab')
        fieldItem = {'fieldGroup':'Dupa', ##Field item will not be created, fieldGroup is wrong
                     'fieldName':"TestUserName_01",
                     'fieldQcName':"TC_User_01",
                     'fieldType':"char",
                     'fieldIsRequired':'True',
                     'fieldOrder':'1'
                    };
        saveFile.makeFieldItem(fieldGroup=fieldItem['fieldGroup'],\
                               fieldName=fieldItem['fieldName'],\
                               fieldQcName=fieldItem['fieldQcName'],\
                               fieldType=fieldItem['fieldType'],\
                               fieldIsRequired=fieldItem['fieldIsRequired'],\
                               fieldOrder=fieldItem['fieldOrder']
                               );
        self.gui.guiQcTestXField.lineEdit_settingName.setText("Setting_1")##input name of setting to be save
        del saveFile ##this will write file and close it
        
        
        
        
        status = self.gui.pushButton_QCTestX_RefreshFieldFile_clicked();
        self.assertTrue(status, "Refresh button run, even if given setting XML file does not exists")        
    
    def testRefresh_3_NOK(self):
        '''
        Try to push button Refresh on the setting file
        Saved before XML file has only rootSetting=UL. 
        Should not get rootSetting=DL, so the setting list will be empty
        '''
        
#        callableFunction = partial(self.gui.pushButton_QCTestX_RefreshFieldFile_clicked);
#        self.assertRaises(OpenFileException, callableFunction);
        savingType = "DL";
        saveFile = SaveFile(savingType, self.qcType);
        
        saveFile.makesetting("Setting_1", qcType='TestLab')
        fieldItem = {'fieldGroup':'Dupa', ##Field item will not be created, fieldGroup is wrong
                     'fieldName':"TestUserName_01",
                     'fieldQcName':"TC_User_01",
                     'fieldType':"char",
                     'fieldIsRequired':'True',
                     'fieldOrder':'1'
                    };
        saveFile.makeFieldItem(fieldGroup=fieldItem['fieldGroup'],\
                               fieldName=fieldItem['fieldName'],\
                               fieldQcName=fieldItem['fieldQcName'],\
                               fieldType=fieldItem['fieldType'],\
                               fieldIsRequired=fieldItem['fieldIsRequired'],\
                               fieldOrder=fieldItem['fieldOrder']
                               );
        self.gui.guiQcTestXField.lineEdit_settingName.setText("Setting_1")##input name of setting to be save
        del saveFile ##this will write file and close it

        
        callableFunction = partial(self.gui.pushButton_QCTestX_RefreshFieldFile_clicked);
        self.assertRaises(RefreshButtonException, callableFunction);
#        status = self.gui.pushButton_QCTestX_RefreshFieldFile_clicked();
#        self.assertFalse(status, "Refresh button run, even if given setting XML file does not exists")    



    def testRefresh_4_NOK(self):
        '''
        Try to push button Refresh on the setting file
        Write something to the setting XML file. 
        In this situation file should not open 
        '''
        
#        callableFunction = partial(self.gui.pushButton_QCTestX_RefreshFieldFile_clicked);
#        self.assertRaises(OpenFileException, callableFunction);
        filename = 'testxml.xml';
        saveFile = SaveFile(self.savingType, self.qcType);
        
        saveFile.makesetting("Setting_1", qcType='TestLab')
        fieldItem = {'fieldGroup':'Dupa', ##Field item will not be created, fieldGroup is wrong
                     'fieldName':"TestUserName_01",
                     'fieldQcName':"TC_User_01",
                     'fieldType':"char",
                     'fieldIsRequired':'True',
                     'fieldOrder':'1'
                    };
        saveFile.makeFieldItem(fieldGroup=fieldItem['fieldGroup'],\
                               fieldName=fieldItem['fieldName'],\
                               fieldQcName=fieldItem['fieldQcName'],\
                               fieldType=fieldItem['fieldType'],\
                               fieldIsRequired=fieldItem['fieldIsRequired'],\
                               fieldOrder=fieldItem['fieldOrder']
                               );
        self.gui.guiQcTestXField.lineEdit_settingName.setText("Setting_1")##input name of setting to be save
        del saveFile ##this will write file and close it
        f = open(self.filename, 'r+')
        f.write('wrongXMLword');##write wrong xml style 
        f.flush();
        f.close();
        
        
        callableFunction = partial(self.gui.pushButton_QCTestX_RefreshFieldFile_clicked);
        self.assertRaises(RefreshButtonException, callableFunction);
    
    def testRefresh_5_NOK(self):
        '''
        Try to push button Refresh on the setting file
        Write something to the already created file, without of regenerate new CRC sum. 
        In this situation file should not open 
        '''
        
#        callableFunction = partial(self.gui.pushButton_QCTestX_RefreshFieldFile_clicked);
#        self.assertRaises(OpenFileException, callableFunction);
        filename = 'testxml.xml';
        saveFile = SaveFile(self.savingType, self.qcType);
        
        saveFile.makesetting("Setting_1", qcType='TestLab')
        fieldItem = {'fieldGroup':'Dupa', ##Field item will not be created, fieldGroup is wrong
                     'fieldName':"TestUserName_01",
                     'fieldQcName':"TC_User_01",
                     'fieldType':"char",
                     'fieldIsRequired':'True',
                     'fieldOrder':'1'
                    };
        saveFile.makeFieldItem(fieldGroup=fieldItem['fieldGroup'],\
                               fieldName=fieldItem['fieldName'],\
                               fieldQcName=fieldItem['fieldQcName'],\
                               fieldType=fieldItem['fieldType'],\
                               fieldIsRequired=fieldItem['fieldIsRequired'],\
                               fieldOrder=fieldItem['fieldOrder']
                               );
        self.gui.guiQcTestXField.lineEdit_settingName.setText("Setting_1")##input name of setting to be save
        del saveFile ##this will write file and close it


        ##for one saved file add some more lines and update CRC sum
        saveFile = SaveFile(self.savingType, self.qcType);
        saveFile.makesetting("Setting_10", qcType='TestLab')
        fieldItem = {'fieldGroup':'TestSet',
                     'fieldName':"TestSetUserName_03",
                     'fieldQcName':"TS_Name_01",
                     'fieldType':"char",
                     'fieldIsRequired':'True',
                     'fieldOrder':'2'
                    };
        saveFile.makeFieldItem(fieldGroup=fieldItem['fieldGroup'],\
                               fieldName=fieldItem['fieldName'],\
                               fieldQcName=fieldItem['fieldQcName'],\
                               fieldType=fieldItem['fieldType'],\
                               fieldIsRequired=fieldItem['fieldIsRequired'],\
                               fieldOrder=fieldItem['fieldOrder']
                               );
        try:
            outFile = open(self.filename, 'wb');
        except IOError, err:
            log.error("ERR: Unable to create file. ", err);
            return
#        self.genCRC();
        saveFile.tree.write(outFile, encoding="UTF-8")
        outFile.close();
        
        
        
        callableFunction = partial(self.gui.pushButton_QCTestX_RefreshFieldFile_clicked);
        self.assertRaises(RefreshButtonException, callableFunction);

def load_tests(loader, tests, pattern):
    suite = unittest.TestSuite()
#    suite.addTest(unittest.makeSuite(Test));  ##Run all tests
    return suite

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()


#if __name__ == "__main__":
#    #import sys;sys.argv = ['', 'Test.testOpenTree']
#    
#    app = QtGui.QApplication(sys.argv)
#    
#
#    suite = unittest.TestSuite()
#
##    suite.addTest(TestXMLDownloadSettingOperation('testSaveFile_2'));  ##run only one test
##    suite.addTest(TestXMLDownloadSettingOperation('testRefresh_2_OK'));  ##run only one test
##    suite.addTest(TestXMLDownloadSettingOperation('testRefresh_3_NOK'));  ##run only one test
##    suite.addTest(TestXMLDownloadSettingOperation('testRefresh_3_NOK'));  ##run only one test
##    suite.addTest(TestXMLDownloadSettingOperation('testRefresh_4_NOK'));  ##run only one test
##    suite.addTest(TestXMLDownloadSettingOperation('testRefresh_5_NOK'));  ##run only one test
##    suite.addTest(TestXMLUploadSettingOperation('testRefresh'));  ##run only one test
#    
#    
#    suite.addTest(unittest.makeSuite(TestXMLDownloadSettingOperation));  ##Run all tests
#    suite.addTest(unittest.makeSuite(TestXMLUploadSettingOperation));  ##Run all tests
#    unittest.TextTestRunner(verbosity=2).run(suite)
#    
#    sys.exit(app.exec_())