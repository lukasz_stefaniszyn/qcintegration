'''
Created on 18-11-2011

@author: qc
'''
import unittest
from PyQt4 import QtCore, QtGui
from time import sleep
import sys

from lib.QCTestX.treeView.treeTestXModel import TreeView, TreeModel, TreeItem



app = QtGui.QApplication(sys.argv);


class TestTreeViewBasic(unittest.TestCase):
    '''
    Test basic functionality of TreeView, TreeModel, TreeItem
    '''


    def setUp(self):
        self.tView = TreeView(QtGui.QTreeView());

    def tearDown(self):
        self.tView.show();
        self.tView.expandAll();
        self.tView.repaint();
#        sleep(2);
        
        self.tView.close();
        del self.tView;

    def testOpenTree(self):
        
        ##Set empty model
        data = ([], []);
        model = TreeModel(data)
        self.tView.setModel(model);
        
        self.assertEqual(model.rootItem.childCount(), 0);
        
    def testOpenTreeWithData(self):
        
        data = (
                [
                 ('A system Design', "A-qcInstance"), 
                 ('B Sevice and Transpoders',"B-qcinstance"), 
                 ('G TL1 Wroclaw', "G-qcinstance")
                 ], 
                [
                 ('G01 TestCase', 'TestCase-qcInstance')
                 ]
                );
        model = TreeModel(data)
        self.tView.setModel(model);
        
        self.assertEqual(model.rootItem.childCount(), 4);
        

    def testAddOneItem(self):
        
        data = ([('A system Design', "A-qcInstance"),('B Sevice and Transpoders',"B-qcinstance"),('G TL1 Wroclaw', "G-qcinstance")],[('G01 TestCase', 'TestCase-qcInstance')]);
        model = TreeModel(data)
        self.tView.setModel(model);
        
        parent = model.rootItem.child(0).parent();
        data = ('S test', "S-qcInstance")
        item = TreeItem(data, parent)
        model.rootItem.appendChild(item)
        
        self.assertEqual(model.rootItem.childCount(), 5);

    def testNewStucture(self):
        
        data = ([
             ('Test Case', ""), 
             ('Step', "")
             ],
                []
            ); 
        model = TreeModel(data)
        self.tView.setModel(model);
        
        self.assertEqual(model.rootItem.childCount(), 2);

    def testNewStuctureAddItem(self):
        
        data = ([
             ('Test Case', ""), 
             ('Step', "")
             ],
                []
            ); 
        model = TreeModel(data)
        self.tView.setModel(model);
        self.assertEqual(model.rootItem.childCount(), 2);
        
        
        data = ('Test Case Name', 'qcInstance_TCname', 'Excel column')
        isDirectory = False; 
        parent = model.rootItem.child(0);
        
        ##create TreeItem of the given data
        item = TreeItem(data, parent, isDirectory);
#        childCounting = parent_pointer.childCount();
        ##start adding item into Model, under correct Parents Row
#        model.beginInsertRows(parent, 
#                              childCounting, 
#                              childCounting);#
        model.beginInsertRows(self.tView.rootIndex().child(0,0), 
                              0, 
                              0);#        
        ##add item to the Data tree
        parent.appendChild(item);
        ##finish adding item into Model
        model.endInsertRows();
        
        
        self.assertEqual(model.rootItem.childCount(), 2);
        self.assertEqual(model.rootItem.child(0).childCount(), 1);
        
    def testNewStuctureAddMoreItem(self):
        
        data = ([
             ('Test Case', ""), 
             ('Step', "")
             ],
                []
            ); 
        model = TreeModel(data)
        self.tView.setModel(model);
        self.assertEqual(model.rootItem.childCount(), 2);
        
        
        parent = model.rootItem.child(0);

        model.beginInsertRows(self.tView.rootIndex().child(0,0), 
                              0, 
                              0);#        
        ##add item to the Data tree
        data = ('Test Case Name', 'qcInstance_TCname', 'Excel column')
        isDirectory = False; 
        item = TreeItem(data, parent, isDirectory);
        parent.appendChild(item);
        
        data = ('Test Case Dir', 'qcInstance_TCname', 'Excel column')
        isDirectory = False; 
        item = TreeItem(data, parent, isDirectory);
        parent.appendChild(item);
        
        
        ##finish adding item into Model
        model.endInsertRows();




        
        
        self.assertEqual(model.rootItem.childCount(), 2);
        self.assertEqual(model.rootItem.child(0).childCount(), 2);

#        ("Test Case Directory", "qcInstance_TCdir", "Excel column")


    def testMoveItems(self):
        
        
        data = ([
             ('Test Case', ""), 
             ('Step', "")
             ],
              []
                );
        model = TreeModel(data)
        self.tView.setModel(model);
        self.assertEqual(model.rootItem.childCount(), 2);
        
        parent = model.rootItem.child(0);

#        model.beginInsertRows(self.tView.rootIndex().child(0,0), 
#                              0, 
#                              0);#     
        data = ('Test Case Name', 'qcInstance_TCname', 'Excel column')
        isDirectory = False; 
        item = TreeItem(data, parent, isDirectory);
        parent.appendChild(item);
        
        data = ('Test Case Dir', 'qcInstance_TCname', 'Excel column')
        isDirectory = False; 
        item = TreeItem(data, parent, isDirectory);
        parent.appendChild(item);
        ##finish adding item into Model
#        model.endInsertRows();
        


        ##Moving item in the new place
        rootIndex = self.tView.rootIndex();
        parentIndex = rootIndex.child(0,0)
        srcStart, srcEnd = 1, 1;
        dst = 0;
        model.beginMoveRows(parentIndex, srcStart, srcEnd, 
                            parentIndex, dst)
        parent.removeChild(srcStart);
        
        data = ('Test Case Dir', 'qcInstance_TCname', 'Excel column')
        isDirectory = False; 
        item = TreeItem(data, parent, isDirectory);
        
        parent.appendChildInRow(dst, item)
        model.endMoveRows()



def load_tests(loader, tests, pattern):
    suite = unittest.TestSuite()
#    suite.addTest(unittest.makeSuite(Test));  ##Run all tests
    return suite

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()

#if __name__ == "__main__":
#    #import sys;sys.argv = ['', 'Test.testOpenTree']
#    
#    app = QtGui.QApplication(sys.argv)
#    
#    
#    
#    tc = ['testOpenTree', 
#          'testOpenTreeWithData',
#          'testAddOneItem',
#          'testNewStucture',
#          'testNewStuctureAddItem',
#          'testNewStuctureAddMoreItem',
#          'testMoveItems'
#          ]
#    suite = unittest.TestSuite(map(TestTreeViewBasic, tc))
#    unittest.TextTestRunner(verbosity=2).run(suite)
#    
#    
#    sys.exit(app.exec_())
#    
#    