'''
Created on 18-11-2011

@author: qc
'''
import unittest
from PyQt4 import QtCore, QtGui
from time import sleep
import sys
import os

from lib.QCTestX.qcTestX_download_chooseFields import QcTestXDownloadChooseFields

from lib.QCTestX.treeView.treeTestXModel import TreeItem as QcTreeItemDownload
from lib.QCTestX.field_file_operation import OpenFile, SaveFile;

FILENAME = r'D:\Eclipse_workspace\QC_gui\src\testUnit\testxml.xml';



app = QtGui.QApplication(sys.argv);

class TestTreeButtonOperation(unittest.TestCase):
    '''
    Check if in ChooseFields basic buttons are working: Add,Remove,MoveUp, MoveDown, Refresh
    '''


    def setUp(self):
        self.filename = r'D:\Eclipse_workspace\QC_gui\src\testUnit\testxml.xml';
        ##to be sure that there was no other files created before
        try: os.remove(self.filename);
        except: pass;

        self.savingType = "DL"
        self.qcType = "TestLab"
        self.main = QtGui.QWidget()
        self.gui = QcTestXDownloadChooseFields(type=self.qcType, parent=self.main)
        
        self.treeViewQcField = self.gui.qCTestXQcField.treeView
        self.treeViewUserField = self.gui.qCTestXUserField.treeView
        
        self.mappingFieldsQc = {
                           "CYCLE": {'Test Set Name':{'FieldQcName':'TS_NAME', 
                                                          'FieldType':'char', 
                                                          'FieldIsRequired':'True'
                                                          },
                                     'Test Set Directory': {'FieldQcName':'TS_DIR', 
                                                          'FieldType':'char', 
                                                          'FieldIsRequired':'True'
                                                          },
                                     'Test Set Description':{'FieldQcName':'TS_DESC', 
                                                          'FieldType':'char', 
                                                          'FieldIsRequired':'True'
                                                          }, 
                                     },
                          "TESTCYCL": {'Test Case Name': {'FieldQcName':'TC_NAME', 
                                                          'FieldType':'char', 
                                                          'FieldIsRequired':'True'
                                                          },
                                      'Test Case Directory':{'FieldQcName':'TC_DIR', 
                                                          'FieldType':'char', 
                                                          'FieldIsRequired':'True'
                                                          }, 
                                      'Test Case Status': {'FieldQcName':'TC_STATUS', 
                                                          'FieldType':'char', 
                                                          'FieldIsRequired':'True'
                                                          },
                                      },
                          "STEP": {'Step Name': {'FieldQcName':'ST_NAME', 
                                                  'FieldType':'char', 
                                                  'FieldIsRequired':'True'
                                                  },
                                   'Step Description': {'FieldQcName':'ST_DESC', 
                                                          'FieldType':'char', 
                                                          'FieldIsRequired':'True'
                                                          }
                                   }
                          };
                            
                            
                          
        self.gui.qCTestXQcField.updateModelTestLab(self.mappingFieldsQc);
        
        self.main.show();
        pass;

    def tearDown(self):
        self.treeViewQcField.expandAll();
        self.treeViewUserField.expandAll();
        self.main.repaint();
#        sleep(5);
        self.main.close();
        
        
    def __addUserFieldsTreeView(self):
        self.mappingFieldsUser =    {"TestSet": {'Test Set Name':{'FieldOrder':'0',
                                                       'FieldQcName':'TS_NAME',
                                                       'FieldType':'char',
                                                       'FieldIsRequired':'True'},
                                      'Test Set Directory': {'FieldOrder':'1',
                                                             'FieldQcName':'TS_DIR',
                                                             'FieldType':'char',
                                                             'FieldIsRequired':'True'},
                                      'Test Set Description': {'FieldOrder':'2',
                                                               'FieldQcName':'TS_DESC',
                                                               'FieldType':'char',
                                                               'FieldIsRequired':'True'}
                                      },
                               "TestCase": {'Test Case Name': {'FieldOrder':'0',
                                                          'FieldQcName':'TC_NAME',
                                                          'FieldType':'char',
                                                          'FieldIsRequired':'True'},
                                            'Test Case Directory': {'FieldOrder':'1',
                                                              'FieldQcName':'TC_DIR',
                                                              'FieldType':'char',
                                                              'FieldIsRequired':'True'},
                                            'Test Case Status': {'FieldOrder':'2',
                                                              'FieldQcName':'TC_STATUS',
                                                              'FieldType':'char',
                                                              'FieldIsRequired':'True'},
                                        },
                               "Step": {'Step Name': {'FieldOrder':'0',
                                                  'FieldQcName':'ST_NAME',
                                                  'FieldType':'char',
                                                  'FieldIsRequired':'True'},
                                        'Step Description': {'FieldOrder':'1',
                                                  'FieldQcName':'ST_DESC',
                                                  'FieldType':'char',
                                                  'FieldIsRequired':'True'}
                                    }
                             };
        self.gui.qCTestXUserField.updateModel(self.mappingFieldsUser);

    def __addQcFieldsTreeView(self):
        mapping_fields = {
                            "CYCLE": {'Test Set Name':{'FieldQcName':'TS_NAME',
                                                       'FieldType':'char',
                                                       'FieldIsRequired':'True'},
                                      'Test Set Directory': {'FieldQcName':'TS_DIR',
                                                             'FieldType':'char',
                                                             'FieldIsRequired':'True'},
                                      'Test Set Description': {'FieldQcName':'TS_DESC',
                                                               'FieldType':'char',
                                                               'FieldIsRequired':'True'}
                                      },
                               "TESTCYCL": {'Test Case Name': {'FieldQcName':'TC_NAME',
                                                          'FieldType':'char',
                                                          'FieldIsRequired':'True'},
                                            'Test Case Directory': {'FieldQcName':'TC_DIR',
                                                              'FieldType':'char',
                                                              'FieldIsRequired':'True'},
                                            'Test Case Status': {'FieldQcName':'TC_STATUS',
                                                              'FieldType':'char',
                                                              'FieldIsRequired':'True'},
                                        },
                               "STEP": {'Step Name': {'FieldQcName':'ST_NAME',
                                                  'FieldType':'char',
                                                  'FieldIsRequired':'True'},
                                        'Step Description': {'FieldQcName':'ST_DESC',
                                                  'FieldType':'char',
                                                  'FieldIsRequired':'True'}
                                    }
                             };
        self.gui.qCTestXQcField.updateModelTestLab(mapping_fields)


    def testOpenTree(self):
        
        pass
    
    def testAddButton_1(self):
        '''
        Try to add item from Qc Tree Field to Qc Tree User. 
        User did not selected any item
        '''
        
        ##Clear all selections
        self.treeViewUserField.selectionModel().reset()
        self.treeViewQcField.selectionModel().reset()
        self.treeViewUserField.clearSelection();
        self.treeViewQcField.clearSelection();
        
        status = False
        ##Start add and removing operation on selected item to the Tree of User Fields
        item = self.treeViewQcField.currentIndex().internalPointer();
        if item is not None:
            ##Add selected item to the Tree of User Fields
            self.gui.qCTestXUserField.addItem(item);
            ##Remove selected item from the Tree of Qc Fields
            index = self.treeViewQcField.currentIndex()
            self.gui.qCTestXQcField.removeItem(index);
        else:
            print("WRN: User did not selected QC Field to be moved");
#            log.warning("WRN: User did not selected QC Field to be moved");
            status = True
            pass;
        
        self.assertEqual(True, status)
    
    
    def testAddButton_2(self):
        '''
        Add one item from Qc Tree Field to Qc Tree User
        '''
        
        ##Set Selection on item in the TreeView
        modelQcField = self.treeViewQcField.model()
        modelUserField = self.treeViewUserField.model();
        parentQcIndex = modelQcField.index(0,0, self.treeViewQcField.rootIndex());
        itemQcIndex = modelQcField.index(0,0, parentQcIndex);
        self.treeViewQcField.setCurrentIndex(itemQcIndex)
        
        ##Check if correct item was selected
        self.assertEqual(self.treeViewQcField.currentIndex().internalPointer().itemData, 
                         ('Test Set Name',
                          {'FieldQcName':'TS_NAME','FieldType':'char','FieldIsRequired':'True'})
                         );
        
        ##Check if 'pushButton_QCTestXDownload_Add_clicked()' is adding correct value
        item = self.treeViewQcField.currentIndex().internalPointer();
        self.gui.pushButton_QCTestXDownload_Add_clicked()

        
        ##Check if Item was added to the correct place in the Tree User Fields 
        parentUserIndex = modelUserField.index(0,0, self.treeViewQcField.rootIndex());
        childCounting = parentUserIndex.internalPointer().childCount();
        itemIndex = modelQcField.index(childCounting-1,0, parentUserIndex);
        itemUserField =  itemIndex.internalPointer()
        self.assertEqual(itemUserField.itemData, item.itemData) 
        
    def testAddButton_3(self):
        '''
        Add more than one item from Qc Tree Field to Qc Tree User
        '''

        #======================================================================
        # Add one
        #======================================================================
        
        ##Set Selection on item in the TreeView
        modelQcField = self.treeViewQcField.model()
        modelUserField = self.treeViewUserField.model();
        parentQcIndex = modelQcField.index(0,0, self.treeViewQcField.rootIndex());
        itemQcIndex = modelQcField.index(0,0, parentQcIndex);
        self.treeViewQcField.setCurrentIndex(itemQcIndex)
        
        item = self.treeViewQcField.currentIndex().internalPointer();
        if item is not None:
            ##Add selected item to the Tree of User Fields
            self.gui.qCTestXUserField.addItem(item);
            ##Remove selected item from the Tree of Qc Fields
            index = self.treeViewQcField.currentIndex()
            self.gui.qCTestXQcField.removeItem(index);
        else:
            print("WRN: User did not selected QC Field to be moved");
#            log.warning("WRN: User did not selected QC Field to be moved");
            pass;
        
        
        ##Check if Item was added to the correct place in the Tree User Fields 
        parentUserIndex = modelUserField.index(0,0, self.treeViewQcField.rootIndex());
        childCounting = parentUserIndex.internalPointer().childCount();
        itemIndex = modelQcField.index(childCounting-1,0, parentUserIndex);
        itemUserField =  itemIndex.internalPointer()
        self.assertEqual(itemUserField.itemData, item.itemData) 
        
        #======================================================================
        # Add second item
        #======================================================================
        
        ##Set Selection on item in the TreeView
        itemQcIndex = modelQcField.index(0,0, parentQcIndex);
        self.treeViewQcField.setCurrentIndex(itemQcIndex)
        
        item = self.treeViewQcField.currentIndex().internalPointer();
        if item is not None:
            ##Add selected item to the Tree of User Fields
            self.gui.qCTestXUserField.addItem(item);
            ##Remove selected item from the Tree of Qc Fields
            index = self.treeViewQcField.currentIndex()
            self.gui.qCTestXQcField.removeItem(index);
        else:
            print("WRN: User did not selected QC Field to be moved");
#            log.warning("WRN: User did not selected QC Field to be moved");
            pass;
        
        ##Check if Item was added to the correct place in the Tree User Fields 
        parentUserIndex = modelUserField.index(0,0, self.treeViewQcField.rootIndex());
        childCounting = parentUserIndex.internalPointer().childCount();
        itemIndex = modelQcField.index(childCounting-1,0, parentUserIndex);
        itemUserField =  itemIndex.internalPointer()
        self.assertEqual(itemUserField.itemData, item.itemData) 
        
        
        #======================================================================
        # Add third item
        #======================================================================
        
        ##Set Selection on item in the TreeView
        itemQcIndex = modelQcField.index(0,0, parentQcIndex);
        self.treeViewQcField.setCurrentIndex(itemQcIndex)
        
        item = self.treeViewQcField.currentIndex().internalPointer();
        if item is not None:
            ##Add selected item to the Tree of User Fields
            self.gui.qCTestXUserField.addItem(item);
            ##Remove selected item from the Tree of Qc Fields
            index = self.treeViewQcField.currentIndex()
            self.gui.qCTestXQcField.removeItem(index);
        else:
            print("WRN: User did not selected QC Field to be moved");
#            log.warning("WRN: User did not selected QC Field to be moved");
            pass;
        
        ##Check if Item was added to the correct place in the Tree User Fields 
        parentUserIndex = modelUserField.index(0,0, self.treeViewQcField.rootIndex());
        childCounting = parentUserIndex.internalPointer().childCount();
        itemIndex = modelQcField.index(childCounting-1,0, parentUserIndex);
        itemUserField =  itemIndex.internalPointer()
        self.assertEqual(itemUserField.itemData, item.itemData) 

    def testRemoveButton_1(self):
        '''
        Try to remove item from User Tree Field to Qc Tree User. 
        User did not selected any item
        '''
        
#        self.testAddButton_2();
        self.__addUserFieldsTreeView();
        self.__addQcFieldsTreeView();
        
        ##Clear all selections
        self.treeViewUserField.selectionModel().reset()
        self.treeViewQcField.selectionModel().reset()
        self.treeViewUserField.clearSelection();
        self.treeViewQcField.clearSelection();
        
        
        
        status = False
        ##Start add and removing operation on selected item to the Tree of User Fields
        item = self.treeViewUserField.currentIndex().internalPointer();
        if item is not None:
            ##Add selected item to the Tree of Qc Fields
            self.gui.qCTestXQcField.addItem(item);
            ##Remove selected item from the Tree of User Fields
            index = self.treeViewUserField.currentIndex()
            self.gui.qCTestXUserField.removeItem(index);
        else:
            print("WRN: User did not selected QC Field to be moved");
#            log.warning("WRN: User did not selected QC Field to be moved");
            status = True
            pass;
        
        self.assertEqual(True, status)
    
    
    def testRemoveButton_2(self):
        '''
        Remove one item from User Tree Field to Qc Tree User
        '''
        
#        self.testAddButton_3();
        self.__addUserFieldsTreeView();
        self.__addQcFieldsTreeView();
        ##Clear all selections
        self.treeViewUserField.selectionModel().reset()
        self.treeViewQcField.selectionModel().reset()
        self.treeViewUserField.clearSelection();
        self.treeViewQcField.clearSelection();
        
        
        
        ##Set Selection on item in the TreeView
        modelField = self.treeViewUserField.model();
        parentIndex = modelField.index(0,0, self.treeViewUserField.rootIndex());
        itemIndex = modelField.index(0,0, parentIndex);
        self.treeViewUserField.setCurrentIndex(itemIndex)
        
        ##Check if correct item was selected
        self.assertEqual(self.treeViewUserField.currentIndex().internalPointer().itemData, 
                         ('Test Set Name',
                          {'FieldQcName':'TS_NAME','FieldType':'char','FieldIsRequired':'True', 'FieldOrder': '0'})
                         );
        ##Before execute ""pushButton_QCTestXDownload_Remove_clicked" read current QcField TreeView items
        mappingFieldsQcFields = self.gui.qCTestXQcField.get_mappingFields();
        
        
        ##Run "pushButton_QCTestXDownload_Remove_clicked" to check if it is working in the correct way
        self.gui.pushButton_QCTestXDownload_Remove_clicked();
        
        ##Check if Item was added to the correct place in the Tree User Fields 
        mappingFieldsUserFields = self.gui.qCTestXUserField.get_mappingFields();
        
        mappingFieldsGolden={"TestSet": {
                                      'Test Set Directory': {'FieldOrder':'0',
                                                             'FieldQcName':'TS_DIR',
                                                             'FieldType':'char',
                                                             'FieldIsRequired':'True'},
                                      'Test Set Description': {'FieldOrder':'1',
                                                               'FieldQcName':'TS_DESC',
                                                               'FieldType':'char',
                                                               'FieldIsRequired':'True'}
                                      },
                               "TestCase": {'Test Case Name': {'FieldOrder':'0',
                                                          'FieldQcName':'TC_NAME',
                                                          'FieldType':'char',
                                                          'FieldIsRequired':'True'},
                                            'Test Case Directory': {'FieldOrder':'1',
                                                              'FieldQcName':'TC_DIR',
                                                              'FieldType':'char',
                                                              'FieldIsRequired':'True'},
                                            'Test Case Status': {'FieldOrder':'2',
                                                              'FieldQcName':'TC_STATUS',
                                                              'FieldType':'char',
                                                              'FieldIsRequired':'True'},
                                        },
                               "Step": {'Step Name': {'FieldOrder':'0',
                                                  'FieldQcName':'ST_NAME',
                                                  'FieldType':'char',
                                                  'FieldIsRequired':'True'},
                                        'Step Description': {'FieldOrder':'1',
                                                  'FieldQcName':'ST_DESC',
                                                  'FieldType':'char',
                                                  'FieldIsRequired':'True'}
                                    }
                             };

        self.assertEqual(mappingFieldsGolden, mappingFieldsUserFields);

        ##Check if Item is not moved to the QcFileds_TreView, to check if name is not doubled
        mappingFieldsQcFields_cmp = self.gui.qCTestXQcField.get_mappingFields();
        self.assertEqual(mappingFieldsQcFields_cmp, mappingFieldsQcFields);

    def testRemoveButton_3(self):
        '''
        Try remove root catalog item from User Tree Field to Qc Tree User
        '''
        
#        self.testAddButton_3();
        self.__addUserFieldsTreeView();
        self.__addQcFieldsTreeView();
        ##Clear all selections
        self.treeViewUserField.selectionModel().reset()
        self.treeViewQcField.selectionModel().reset()
        self.treeViewUserField.clearSelection();
        self.treeViewQcField.clearSelection();
        
        
        
        ##Set Selection on item in the TreeView, in that case is Root item
        modelField = self.treeViewUserField.model();
        parentIndex = modelField.index(0,0, self.treeViewUserField.rootIndex());
        self.treeViewUserField.setCurrentIndex(parentIndex)
        
        ##Check if correct item was selected
        self.assertEqual(self.treeViewUserField.currentIndex().internalPointer().itemData, 
                         ('TestSet', ''));
        
        item = self.treeViewUserField.currentIndex().internalPointer();
        if item is not None:
            ##Add selected item to the Tree of User Fields
            
            self.assertFalse(self.gui.qCTestXQcField.addItem(item), 
                             "Root item was added, but it should NOT be possible");
            
            ##Remove selected item from the Tree of Qc Fields
            index = self.treeViewUserField.currentIndex()
            self.assertFalse(self.gui.qCTestXUserField.removeItem(index), 
                             "Root item was removed, but it should NOT be possible");
        else:
            print("WRN: User did not selected QC Field to be moved");
#            log.warning("WRN: User did not selected QC Field to be moved");
            pass;


    def testClearButton_1(self):
        '''
        Clear item from User Tree Field
        '''
        ##Add some data to User Filed, which will be removed
        self.__addUserFieldsTreeView();
        self.__addQcFieldsTreeView();
        
        ##Set Selection on item in the TreeView
        modelUserField = self.treeViewUserField.model();
        parentUserIndex = modelUserField.index(0,0, self.treeViewUserField.rootIndex());
        childCounting = parentUserIndex.internalPointer().childCount();
        self.assert_(childCounting>0, "Item was not added")

        ##Check if Fields are present in USerField_TreeView; 
        mapping_fields = self.mappingFieldsUser
                           
                           
                           
        mappingFieldsUserFields = self.gui.qCTestXUserField.get_mappingFields();
        self.assertEqual(mapping_fields, mappingFieldsUserFields);
        
        ##Check if 'pushButton_QCTestXDownload_Clear_clicked()' is Clearing correct value
        self.gui.pushButton_QCTestXDownload_Clear_clicked()
        
        mappingFieldsGolden = {'TestCase': {},'Step': {},'TestSet': {}};
        mappingFieldsUserFields = self.gui.qCTestXUserField.get_mappingFields();
        
        self.assertEqual(mappingFieldsGolden, mappingFieldsUserFields);


    def testMoveUpButton_1(self):
        '''
        Move Up item from User Tree Field
        '''
        ##Add some data to User Filed, which will be removed
        self.__addUserFieldsTreeView();
        self.__addQcFieldsTreeView();

        ##Clear all selections
        self.treeViewUserField.selectionModel().reset()
        self.treeViewQcField.selectionModel().reset()
        self.treeViewUserField.clearSelection();
        self.treeViewQcField.clearSelection();

        ##Set Selection on item in the TreeView
        modelField = self.treeViewUserField.model();
        parentIndex = modelField.index(0,0, self.treeViewUserField.rootIndex());
        itemIndex = modelField.index(1,0, parentIndex);
        self.treeViewUserField.setCurrentIndex(itemIndex)
        itemBefore = self.treeViewUserField.currentIndex().internalPointer().itemData;
        
        ##Check if 'pushButton_QCTestXDownload_Up_clicked()' is moving up the item position
        self.gui.pushButton_QCTestX_Up_clicked();
        
        ##Check if Item was moved up to the correct place in the Tree User Fields 
        itemIndex = modelField.index(0,0, parentIndex);
        itemAfter = itemIndex.internalPointer().itemData;
        self.assertEqual(itemBefore, itemAfter);
        self.assertEqual(itemIndex.row(), 0, "Position of moved up item is incorrect")
        
        
    def testMoveUpButton_2(self):
        '''
        Move Up item from User Tree Field from top of list
        '''
        ##Add some data to User Filed, which will be removed
        self.__addUserFieldsTreeView();
        self.__addQcFieldsTreeView();

        ##Clear all selections
        self.treeViewUserField.selectionModel().reset()
        self.treeViewQcField.selectionModel().reset()
        self.treeViewUserField.clearSelection();
        self.treeViewQcField.clearSelection();

        ##Set Selection on item in the TreeView
        modelField = self.treeViewUserField.model();
        parentIndex = modelField.index(0,0, self.treeViewUserField.rootIndex());
        itemIndex = modelField.index(0,0, parentIndex);
        self.treeViewUserField.setCurrentIndex(itemIndex)
        itemBefore = self.treeViewUserField.currentIndex().internalPointer().itemData;
        
        ##Check if 'pushButton_QCTestXDownload_Up_clicked()' is moving up the item position
        self.gui.pushButton_QCTestX_Up_clicked();
        
        ##Check if Item was moved up to the correct place in the Tree User Fields 
        itemIndex = modelField.index(0,0, parentIndex);
        itemAfter = itemIndex.internalPointer().itemData;
        self.assertEqual(itemBefore, itemAfter);
        self.assertEqual(itemIndex.row(), 0, "Position of moved up item is incorrect")

    def testMoveUpButton_3(self):
        '''
        Move Up main item from User Tree Field
        '''
        ##Add some data to User Filed, which will be removed
        self.__addUserFieldsTreeView();
        self.__addQcFieldsTreeView();

        ##Clear all selections
        self.treeViewUserField.selectionModel().reset()
        self.treeViewQcField.selectionModel().reset()
        self.treeViewUserField.clearSelection();
        self.treeViewQcField.clearSelection();

        ##Set Selection on item in the TreeView
        modelField = self.treeViewUserField.model();
        parentIndex = modelField.index(0,0, self.treeViewUserField.rootIndex());
        self.treeViewUserField.setCurrentIndex(parentIndex)
        
        ##Check if 'pushButton_QCTestXDownload_Up_clicked()' is moving up the item position
        self.assertFalse(self.gui.pushButton_QCTestX_Up_clicked());
        

        
    def testMoveDownButton_1(self):
        '''
        Move Down item from User Tree Field
        '''
        ##Add some data to User Filed, which will be removed
        self.__addUserFieldsTreeView();
        self.__addQcFieldsTreeView();

        ##Clear all selections
        self.treeViewUserField.selectionModel().reset()
        self.treeViewQcField.selectionModel().reset()
        self.treeViewUserField.clearSelection();
        self.treeViewQcField.clearSelection();

        ##Set Selection on item in the TreeView
        modelField = self.treeViewUserField.model();
        parentIndex = modelField.index(0,0, self.treeViewUserField.rootIndex());
        itemIndex = modelField.index(1,0, parentIndex);
        self.treeViewUserField.setCurrentIndex(itemIndex)
        itemBefore = self.treeViewUserField.currentIndex().internalPointer().itemData;
        
        ##Check if 'pushButton_QCTestXDownload_Up_clicked()' is moving up the item position
        self.gui.pushButton_QCTestX_Down_clicked();
        
        ##Check if Item was moved down to the correct place in the Tree User Fields 
        itemIndex = modelField.index(2,0, parentIndex);
        itemAfter = itemIndex.internalPointer().itemData;
        self.assertEqual(itemBefore, itemAfter);
        self.assertEqual(itemIndex.row(), 2, "Position of moved down item is incorrect")

    def testMoveDownButton_2(self):
        '''
        Move Down item from User Tree Field from the bottom of list
        '''
        ##Add some data to User Filed, which will be removed
        self.__addUserFieldsTreeView();
        self.__addQcFieldsTreeView();

        ##Clear all selections
        self.treeViewUserField.selectionModel().reset()
        self.treeViewQcField.selectionModel().reset()
        self.treeViewUserField.clearSelection();
        self.treeViewQcField.clearSelection();

        ##Set Selection on item in the TreeView
        modelField = self.treeViewUserField.model();
        parentIndex = modelField.index(0,0, self.treeViewUserField.rootIndex());
        childCount = parentIndex.internalPointer().childCount();
        itemIndex = modelField.index(childCount-1,0, parentIndex); ##Select last item from the list
        self.treeViewUserField.setCurrentIndex(itemIndex)
        itemBefore = self.treeViewUserField.currentIndex().internalPointer().itemData;
        
        ##Check if 'pushButton_QCTestXDownload_Up_clicked()' is moving up the item position
        self.gui.pushButton_QCTestX_Down_clicked();
        
        ##Check if Item was moved down to the correct place in the Tree User Fields 
        itemIndex = modelField.index(childCount-1,0, parentIndex);
        itemAfter = itemIndex.internalPointer().itemData;
        self.assertEqual(itemBefore, itemAfter);
        self.assertEqual(itemIndex.row(), childCount-1, "Position of moved down item is incorrect")
    def testMoveDownButton_3(self):
        '''
        Move Down main item from User Tree Field
        '''
        ##Add some data to User Filed, which will be removed
        self.__addUserFieldsTreeView();
        self.__addQcFieldsTreeView();

        ##Clear all selections
        self.treeViewUserField.selectionModel().reset()
        self.treeViewQcField.selectionModel().reset()
        self.treeViewUserField.clearSelection();
        self.treeViewQcField.clearSelection();

        ##Set Selection on item in the TreeView
        modelField = self.treeViewUserField.model();
        parentIndex = modelField.index(0,0, self.treeViewUserField.rootIndex());
        self.treeViewUserField.setCurrentIndex(parentIndex)
        
        ##Check if 'pushButton_QCTestXDownload_Up_clicked()' is moving up the item position
        self.assertFalse(self.gui.pushButton_QCTestX_Down_clicked());


    def testRefreshFieldButton_1(self):
        '''
        Refresh Field button
        '''
        ##to be sure that there was no other files created before
        try: os.remove(r'D:\Eclipse_workspace\QC_gui\src\testUnit\testxml.xml');
        except: pass;
        
        self.gui.guiQcTestXField.lineEdit_settingName.setText("Setting_1")##input name of setting to be save
        self.gui.pushButton_QCTestX_SaveFieldFile_clicked(); ##this will make xml file
        
        self.assertTrue(self.gui.pushButton_QCTestX_RefreshFieldFile_clicked());##check if Refresh operation was OK
        listWidgetField = self.gui.guiQcTestXField.listWidget_fieldSavedSettings;##get current ListWidget with all present Settings
        currentList = [];
        for i in range(listWidgetField.count()):
            currentList.append(listWidgetField.item(i).text()); ##create list of items from ListWidget of Settings 
        
        
        openXMLfile = OpenFile(self.savingType, self.qcType); ##openXML file with settings
        settingList = openXMLfile.itemSettings.keys() ##Dictionary of all seetings. {str(name):etree(eleme
        
        self.assertEqual(currentList, settingList); ##now compare if Refresh button really updated in correct way the ListWidget with the data from XML file   
        

    def testRefreshTreeViewUserField_1(self):
        '''
        Check if TreeView_UserField is updated with correct way
        '''
        
        self.__addUserFieldsTreeView();
        self.__addQcFieldsTreeView();
        
        mapping_fields = self.gui.qCTestXUserField.get_mappingFields();
        
        mapping_fields_cmp = {}; 
        for fieldGroup, itemInfo in self.gui.qCTestXUserField.mainUserItemsNames.items():
#            {'Step': (<lib.QCTestX.treeView.treeTestXModel.TreeItem instance at 0x01BB3198>,
#                      <PyQt4.QtCore.QModelIndex at 0x1bab5a8>),
#             'TestCase': (<lib.QCTestX.treeView.treeTestXModel.TreeItem instance at 0x01BB3148>,
#                           <PyQt4.QtCore.QModelIndex at 0x1bab570>),
#             'TestSet': (<lib.QCTestX.treeView.treeTestXModel.TreeItem instance at 0x01BB30F8>,
#                          <PyQt4.QtCore.QModelIndex at 0x1bab538>)
#             }
            mapping_fields_cmp.update({fieldGroup:{}});
            parentUserIndex = itemInfo[1];
            childCounting = parentUserIndex.internalPointer().childCount();
            for i in range(childCounting):
                ##dataUserField = ('fieldUserName', ('fieldName', 'char', 'True'))
                fieldName, fieldValues = itemInfo[0].child(i).itemData;
                mapping_fields_cmp[fieldGroup].update({fieldName: fieldValues});
#                fieldQcName = fieldValues[0] 
#                fieldType = fieldValues[1]
#                fieldIsRequired = fieldValues[2]
#                fieldOrder = fieldValues[3]
#                mapping_fields_cmp[fieldGroup].update(
#                                                      {fieldName: (fieldQcName, 
#                                                                  fieldType, 
#                                                                  fieldIsRequired, 
#                                                                  fieldOrder
#                                                                  )
#                                                       }
#                                                      );
        print "mapping_fields_cmp:", mapping_fields_cmp;
        
        self.assertEqual(mapping_fields, mapping_fields_cmp);
        

def load_tests(loader, tests, pattern):
    suite = unittest.TestSuite()
#    suite.addTest(unittest.makeSuite(Test));  ##Run all tests
    return suite

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()

#if __name__ == "__main__":
#    #import sys;sys.argv = ['', 'Test.testOpenTree']
#    
#    app = QtGui.QApplication(sys.argv)
#    
#
#    suite = unittest.TestSuite()
#
##    suite.addTest(TestTreeButtonOperation('testRefreshTreeViewUserField_1'));  ##run only one test
#    
#    suite.addTest(unittest.makeSuite(TestTreeButtonOperation));  ##Run all tests
#    unittest.TextTestRunner(verbosity=2).run(suite)
#    
#    sys.exit(app.exec_())