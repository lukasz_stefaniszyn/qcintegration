'''
Created on 18-11-2011

@author: qc
'''
import unittest
from PyQt4 import QtCore, QtGui
from time import sleep
import sys

from lib.QCTestX.qcTestX_download_chooseFields import QcTestXDownloadChooseFields

from lib.QCTestX.treeView.treeTestXModel import TreeItem as QcTreeItemDownload


from win32com.client import gencache, DispatchWithEvents, constants
gencache.EnsureModule('{D66ADC20-B070-11D3-8604-0050046B8F4A}', 0, 1, 0);

import os;
os.chdir(r"D:\Eclipse_workspace\QC_gui\src");
import lib.QualityCenter_ext.lib.qc_connect_disconnect as qc_connect_disconnect

server = "http://qc:8080/qcbin/";
userName="alex_qc";
domainName="DEFAULT";
projectName="QualityCenter_Demo";
password = ""; ##Put the right password




app = QtGui.QApplication(sys.argv);


class TestChooseFieldQc(unittest.TestCase):
    '''
    Check if in Choose Fields, QualityCenter operation are working correct
    '''


    def setUp(self):
        self.qcConnect = qc_connect_disconnect.QC_connect(server, userName, password, domainName,projectName);
        for result in self.qcConnect.connect():
            print result;
        self.qc = self.qcConnect.qc;
        
        
        self.main = QtGui.QWidget()
        self.gui = QcTestXDownloadChooseFields(type="TestLab",parent=self.main, qcConnect=self.qcConnect)
        
        self.treeViewQcField = self.gui.qCTestXQcField.treeView
        self.treeViewUserField = self.gui.qCTestXUserField.treeView
        
        self.main.show();

    def tearDown(self):
        self.treeViewQcField.expandAll();
        self.treeViewUserField.expandAll();
        self.main.repaint();
#        sleep(5);
        self.main.close();
        
        self.qcConnect.disconnect_QC();
        

    def testGetFields(self):
        '''
        Check if "get_fields()" is getting fields from QualityCenter 
        '''
        
        mappingFields = self.gui.get_fields();
        print "mappingFields:", mappingFields;

        self.assertTrue(mappingFields.has_key('TEST'));
        self.assertTrue(mappingFields.has_key('TESTCYCL'));
        self.assertTrue(mappingFields.has_key('STEP'));
        
        ##TestPlan
        dictTest = mappingFields['TEST']; ##TestPLan include TestCase
        dictDesStep = mappingFields['DESSTEPS']; ##TestPlan include Step
        
        ##TestLab
        dictCycle = mappingFields['CYCLE']; ##TestLab include TestSet+RunStatus
        dictTestcycl = mappingFields['TESTCYCL']; ##TestLab include TestCase+RunStatus
        dictStep = mappingFields['STEP']; ##TestLab include Step+RunStatus
        
        
        ##TestPlan
#        self.assertTrue(dictTest.has_key('TS_NAME')); ##This is TestCase name from TestPlan and TestLab
#        self.assertTrue(dictDesStep.has_key('DS_STEP_NAME')); ##This is Step name from TestPlan
        self.assertTrue(dictTest.has_key('Test Name')); ##This is TestCase name from TestPlan and TestLab
        self.assertTrue(dictDesStep.has_key('Step Name')); ##This is Step name from TestPlan

        ##TestLab
#        self.assertTrue(dictCycle.has_key('CY_CYCLE')); ##This is TestSet name from TestLab
#        self.assertTrue(dictTestcycl.has_key('TS_NAME')); ##This is TestCase name from TestPlan and TestLab
#        self.assertTrue(dictStep.has_key('ST_STEP_NAME')); ##This is Step name from TestLab
        self.assertTrue(dictCycle.has_key('Test Set')); ##This is TestSet name from TestLab
        self.assertTrue(dictTestcycl.has_key('Plan: Test Name')); ##This is TestCase name from TestPlan and TestLab
        self.assertTrue(dictStep.has_key('Step Name')); ##This is Step name from TestLab

    def testTranslateGetField(self):
        '''
        Check if "translate_mappingFields()" is translating in the correct way 
        '''
        mappingFields = self.gui.get_fields();
        TmappingFields = self.gui.translate_mappingFields(mappingFields, type="DL");
        self.assertTrue(TmappingFields.has_key('TEST'));
        self.assertTrue(TmappingFields.has_key('TESTCYCL'));
        self.assertTrue(TmappingFields.has_key('STEP'));
        
        ##TestPlan
        dictTest = TmappingFields['TEST']; ##TestPLan include TestCase
        dictDesStep = TmappingFields['DESSTEPS']; ##TestPlan include Step
        
        ##TestLab
        dictCycle = TmappingFields['CYCLE']; ##TestLab include TestSet+RunStatus
        dictTestcycl = TmappingFields['TESTCYCL']; ##TestLab include TestCase+RunStatus
        dictStep = TmappingFields['STEP']; ##TestLab include Step+RunStatus
        
        ##TestPlan
        self.assertTrue(dictTest.has_key('TS_NAME')); ##This is TestCase name from TestPlan and TestLab
        self.assertTrue(dictDesStep.has_key('DS_STEP_NAME')); ##This is Step name from TestPlan

        ##TestLab
        self.assertTrue(dictCycle.has_key('CY_CYCLE')); ##This is TestSet name from TestLab
        self.assertTrue(dictTestcycl.has_key('TS_NAME')); ##This is TestCase name from TestPlan and TestLab
        self.assertTrue(dictStep.has_key('ST_STEP_NAME')); ##This is Step name from TestLab
        
    
    def testRefreshButton_1(self):
        '''
        Try to refresh items in Qc Tree Field. 
        User did not selected any item
        '''
        ##Refresh all QC fields in the TreeViewQc
        self.gui.pushButton_QCTestXDownload_Refresh_clicked();
        
        
        #======================================================================
        # Compare number of recieved QualityCenter fields to the 
        # added/refreshed TreeViewQc number of items. Number must be equal 
        #======================================================================
        mappingFields = self.gui.get_fields();
        lenMapping = len(mappingFields.get('CYCLE'));
        parentFieldItem, fieldindex = self.gui.qCTestXQcField.mainQcItemsNames.get('TestSet');
        lenTree = parentFieldItem.childCount();
        self.assertEqual(lenTree, lenMapping);
        
        
        #======================================================================
        # Check if the names from Mapping Qc are the same on the TreeView
        #======================================================================
        modelQcField = self.treeViewQcField.model()
        modelUserField = self.treeViewUserField.model();

        ##Check first item
        i = 0; 
        fieldQcUserName = mappingFields.get('CYCLE');##Here You will have ['Plan: Test Name', u'char', False]
        parentQcIndex = modelQcField.index(0,0, self.treeViewQcField.rootIndex());
        itemQcIndex = modelQcField.index(i,0, parentQcIndex);
        itemQcData = itemQcIndex.internalPointer().itemData
        self.assertTrue(fieldQcUserName.has_key(itemQcData[0]));
        
        ##Check Last item
        i = lenMapping-1; 
        fieldQcUserName = mappingFields.get('CYCLE');##Here You will have ['Plan: Test Name', u'char', False]
        parentQcIndex = modelQcField.index(0,0, self.treeViewQcField.rootIndex());
        itemQcIndex = modelQcField.index(i,0, parentQcIndex);
        itemQcData = itemQcIndex.internalPointer().itemData
        self.assertTrue(fieldQcUserName.has_key(itemQcData[0]));
    
    
def load_tests(loader, tests, pattern):
    suite = unittest.TestSuite()
#    suite.addTest(unittest.makeSuite(Test));  ##Run all tests
    return suite

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()

#if __name__ == "__main__":
#    #import sys;sys.argv = ['', 'Test.testOpenTree']
#    
#    app = QtGui.QApplication(sys.argv)
#    
#
#    tc_basic = ['testOpenTree',
#                ]
#    
#    tc_addButton = [
#                    'testAddButton_1',
#                    'testAddButton_2',
#                    'testAddButton_3',
#                    ]
#    
#    tc_removeButton = ['testRemoveButton_1',
#                       'testRemoveButton_2',
#                       'testRemoveButton_3',
#                       ]
#    
#    tc_clearButton = ['testClearButton_1',
#                      ]
#    
#    tc_testMoveButton = ['testMoveUpButton_1',
#                         'testMoveDownButton_1',
#                         ]
#    
#    suite = unittest.TestSuite()
#
##    suite.addTest(TestTreeViewBasic('testRefreshButton_1'));  ##run only one test
#    
#    suite.addTest(unittest.makeSuite(TestChooseFieldQc));  ##Run all tests
#    unittest.TextTestRunner(verbosity=2).run(suite)
#    
#    sys.exit(app.exec_())