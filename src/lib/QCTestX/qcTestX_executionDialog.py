'''
Created on 01-05-2012

@author: lucst
'''
# This is only needed for Python v2 but is harmless for Python v3.
import sip
import PyQt4
#sip.setapi('QString', 2)

from PyQt4 import QtCore, QtGui
import sys


from guiLib.ui_QCTestX_RunQc import Ui_Dialog


from lib.logfile import Logger
log = Logger(loggername="lib.QCTestX.qcTestX_executionDialog", resetefilelog=False).log;

class GuiDialog(QtGui.QDialog, Ui_Dialog):
    '''
    '''
    def __init__(self, parent):
        ##Add Widget responsible for tab_CrossTest
        ##This initialize QcGraph tab Cross Test  
        super(GuiDialog, self).__init__(parent)
        self.setupUi(self);

    
class ProgressWidget(object):
    
        def __init__(self, max, gui):
            
            self.progressBarVisual = QtGui.QProgressBar(parent=gui.progressBarFrame_1);

            self.progressBarVisual.setSizePolicy(gui.progressBarFrame_1.sizePolicy())
            self.progressBarVisual.setMinimumWidth(gui.progressBarFrame_1.width());
            
            self._setRange = 0, max;
            self.progressBarVisual.setFormat("%v of %m progress");
            self.updateProgress();

        def set_set_range(self, value):
            min, max = value;
            self.progressBarVisual.setRange(min, max);
        _setRange = property(None, set_set_range, None, "_setRange's docstring")
#            print "isTextVisible: ", self.isTextVisible(); 

        def updateProgress(self):
            value = self.progressBarVisual.value();
            self.progressBarVisual.setValue(value+1);
            QtGui.qApp.processEvents()




class ProgressExecutionDialog(QtGui.QDialog):
    
    cancelStatus=False ##if user will press Cancel button, then it has to cancel current execution
    
    def __init__(self, nbr_of_tests=0, parent=None):
        super(ProgressExecutionDialog, self).__init__(parent);
        self.gui = GuiDialog(parent)
        
        self.progress = ProgressWidget(nbr_of_tests, self.gui);        

        self.gui.phb_Close.clicked.connect(self.phB_Close_clicked);
        self.gui.phb_Cancel.clicked.connect(self.phB_Cancel_clicked);
        self.gui.phb_Close.setEnabled(False) ##when this Widget will be started user can only push Cancel. When he push CAncles then CLose will be enabled, or when process will be finished
        self.gui.show();
        
        
    def enableCloseButton(self):
        self.gui.phb_Close.setEnabled(True);
        
    def disableCancelButton(self):
        self.gui.phb_Cancel.setEnabled(False);
        
    def updateTextDialog(self, text):
        '''
        Write new line to Dialog Gui 
        *input*
        @param text: str()
        *output*:
        None
        '''
        self.gui.textBrowser.append(text);
        self.gui.textBrowser.repaint();
    def updateProgressBar(self):
        '''
        Update number+1 on Dialog Gui of ProgressBar
        *input*
        None
        *output*
        None
        '''
        self.progress.updateProgress();
        
    def phB_Close_clicked(self):
        log.debug(("phB_Close_clicked"));
        self.phB_Cancel_clicked();##before closing this dialog, make sure, that execution is finished
        self.gui.close();
    def phB_Cancel_clicked(self):
        log.debug(("phB_Cancel_clicked"));
        self.gui.phb_Close.setEnabled(True);##now user after pushing Cancel can push Close this Widget
        
        self.cancelStatus = True;
