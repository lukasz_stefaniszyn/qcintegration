'''
Created on 21-11-2011

@author: qc
'''
from PyQt4 import QtCore, QtGui
from copy import deepcopy

from lib.QCTestX.qCTestX_download import GuiQcTestXDownloadField,\
                              MyTreeViewQcTestXDownloadQCField,\
                              MyTreeViewQcTestXDownloadUserField

from lib.QCTestX.guiLib.ui_QCTestX_main import Ui_Form as Ui_QcGraph_Main
from lib.progressDialog import ProgressDialog
import lib.QCTestX.field_file_operation as  field_file_operation


from lib.logfile import Logger
log = Logger(loggername="lib.QCTestX.qcTestX_download_chooseFields", resetefilelog=False).log;


class RefreshButtonException(Exception):
    def __init__(self, text):
        log.exception(("RefreshButton will exit now with error:", text));
        self.text = text;
        pass
    def __str__(self):
        return self.text;



class QcTestXCommonChooseFields(object):
    def __init__(self, savingType, qcType):
        '''
        
        :param savingType:str(), this is information is it DL or UL
        :param qcType: str(), this is information is it QC  TestLab or TestPlan
        '''
        self.savingType = savingType
        self.qcType = qcType
    
    def clearSelectionTreeViewUserField(self, selected, deselected):
        log.debug("clearSelectionTreeViewUserField");
        self.qCTestXQcField.treeView_QCTestX_UserFields.clearSelection();

#        self.qCgraphSpecificTestChooseTestSet.clearItem();
    def pushButton_QCTestX_Up_clicked(self):
        index = self.treeViewUserField.currentIndex();
        if index.internalPointer() is not None:
            ##Add selected item to the Tree of User Fields
            return self.qCTestXUserField.moveItem(index, direction=0);
        else:
            log.warning("WRN: User did not selected QC Field to be moved");
            return False
    def pushButton_QCTestX_Down_clicked(self):
        index = self.treeViewUserField.currentIndex();
        if index.internalPointer() is not None:
            ##Add selected item to the Tree of User Fields
            return self.qCTestXUserField.moveItem(index, direction=1);
        else:
            log.warning("WRN: User did not selected QC Field to be moved");
            return False;
        

    
    
    def update_lineEditSettingName(self):
        itemName = self.guiQcTestXField.listWidget_fieldSavedSettings.currentItem().text();
        log.debug(("Update lineEditSettingName, with value:", itemName)); 
        self.guiQcTestXField.lineEdit_settingName.setText(itemName); 
    
    def pushButton_QCTestX_SaveFieldFile_clicked(self):
        '''
        Pushed Save Fields for user selected fields 
        '''
        log.debug("Clicked pushButton_QCTestX_SaveFieldFile_clicked, savingType: %s"%self.savingType);
        log.debug("Clicked pushButton_QCTestX_SaveFieldFile_clicked, qcType: %s"%self.qcType);
        
        ##Here I will have to make additional Widget to get SettingName
        settingName = str(self.guiQcTestXField.lineEdit_settingName.text());
        self.guiQcTestXField.lineEdit_settingName.setText(settingName);
        if settingName == "":
            text = "Please input 'Setting name' to save";
            QtGui.QMessageBox.information(self, 
                                      "Save settings field file",text);
            log.debug((text));
            return
        
        mappingFields = self.qCTestXUserField.get_mappingFields();
        
        saveFile = field_file_operation.SaveFile(self.savingType, self.qcType);
        
        ##Check if user is trying to overwrite current SettingName
        if saveFile.itemSettings.has_key(settingName):
            text = "Overwrite saved settings?"
            log.debug((text));
            reply = QtGui.QMessageBox.question(self, 
                                              "Save settings field file",
                                              text, 
                                              buttons=QtGui.QMessageBox.Yes | QtGui.QMessageBox.No, 
                                              defaultButton=QtGui.QMessageBox.No
                                              );
            if reply == QtGui.QMessageBox.Yes:
                pass;
            elif reply == QtGui.QMessageBox.No:
                return;
            else:
                return

        ##save setting with data from TreeView_UserFields
        saveFile.write_mappingfields(settingName, self.qcType, mappingFields, self.savingType);
        del saveFile
        
        self.pushButton_QCTestX_RefreshFieldFile_clicked();
        
    
    def pushButton_QCTestX_OpenFieldFile_clicked(self):
        '''
        Pushed Open Fields for user selected fields
        '''
        log.debug("Clicked pushButton_QCTestX_OpenFieldFile_clicked");
        
        ##Clear all selections
        self.treeViewUserField.selectionModel().reset()
        self.treeViewQcField.selectionModel().reset()
        self.treeViewUserField.clearSelection();
        self.treeViewQcField.clearSelection();
        
        ##Here I will have to make additional Widget to get SettingName
        settingName = str(self.guiQcTestXField.lineEdit_settingName.text());
        if settingName == "":
            text = "Please input 'Setting name' to open";
            QtGui.QMessageBox.information(self, 
                                      "Open settings file",text);
            log.debug((text));
            return
        
        try: 
            field_file_operation.OpenFile(self.savingType, self.qcType);
        except (field_file_operation.ParserException, 
                field_file_operation.OpenFileException,
                field_file_operation.CRCexception),\
                text:
            QtGui.QMessageBox.information(self, 
                                      "Open settings file",text);
            return
         
        ##Now open xml file with setting name "Setting_2" and get mapping of this setting
        openXmlFile = field_file_operation.OpenFile(self.savingType, self.qcType);
        mappingFieldsXML = openXmlFile.get_mappingfields(settingName, self.savingType);
        log.debug(("mappingFieldsXML:", mappingFieldsXML)); 
        
        mappingFieldsQcDict = self.qCTestXQcField.get_mappingFields();
        log.debug(("mappingFieldsQc:", mappingFieldsQcDict)); 
        ##Translate dict() to have fieldQcName instead of fieldUserName as key() 
        mappingFieldsQc= self.translate_mappingFields(mappingFieldsQcDict, type="DL")##this is operation the same as operation on DL type data, that is why it is forced to use type="DL"
        del mappingFieldsQcDict;

        ##Here compare if all Fields from open XML Setting (mappingFieldsXML) are possible to run in QualityCenter (mappingFieldsQc)
        ##When Field from XML is possible to run in QC, then update the dict() "mappingFieldsXML_checked"
        mappingFieldsXML_checked = {};
        if self.savingType=="DL":
            mappingFieldsXML= self.translate_mappingFields(mappingFieldsXML, self.savingType)
            for fieldGroup, fieldValue in mappingFieldsXML.items():
                if mappingFieldsQc.has_key(fieldGroup):
                    mappingFieldsXML_checked.update({fieldGroup:{}});#
                    for fieldQcName, fieldValues in fieldValue.items():
                        if mappingFieldsQc[fieldGroup].has_key(fieldQcName):
                            fieldType = fieldValues.get("FieldType");
                            fieldIsRequired = fieldValues.get("FieldIsRequired");
                            fieldOrder = fieldValues.get("FieldOrder");
                            fieldName = fieldValues.get('FieldName');
                            fieldValues = {'FieldQcName':fieldQcName, 
                                           'FieldType':fieldType, 
                                           'FieldIsRequired':fieldIsRequired, 
                                           'FieldOrder':fieldOrder};
                            ##Change place of fieldUserName and fieldName
                            mappingFieldsXML_checked[fieldGroup].update({fieldName: fieldValues});
                        else:
                            print "Given fieldName: %s is not in mappingFieldsQc"%(fieldQcName);
            log.debug(("mappingFieldsXML_checked:", mappingFieldsXML_checked))
            ##Update TreeView_UserFiled  with the given mapping from xml file
            self.qCTestXUserField.updateModel(mappingFieldsXML_checked);
            del mappingFieldsXML_checked; 
        elif self.savingType=="UL":
#            mappingFieldsUser = self.qCTestXUserField.get_mappingFields();
            mappingFieldsUser = deepcopy(self.qCTestXUserField.mappingFieldsUserOrg);  
            
            
            #==================================================================
            # ##You will have FieldUserName:{FieldQcName, FieldName} not FieldName:{FieldQcName, FieldUserName} This is because TreeView of user Fields will have empty FieldNames. During opening of Settings from xml file program is checking by FieldUserName instead FieldName
            #==================================================================
            
            for fieldGroup, fieldValues in mappingFieldsXML.items():
                if mappingFieldsQc.has_key(fieldGroup) and mappingFieldsUser.has_key(fieldGroup):
                    mappingFieldsXML_checked.update({fieldGroup:{}});#
                    for fieldUserName, fieldValue in fieldValues.iteritems():
                        try: fieldQcName = fieldValue.get('FieldQcName');##used did not mapped QCField with UserField
                        except: 
#                            if qcFieldName is "":##used did not mapped QCField with UserField
                            fieldType = '';
                            fieldIsRequired= '';
                            fieldName = '';
                            fieldQcName = ''; 
                            fieldOrder = fieldValue.get("FieldOrder");
                        if not mappingFieldsUser[fieldGroup].has_key(fieldUserName):##check if given FieldUserName from XML file is in the TreeViewUser
                            continue; ##if not then, take the next field from XML file
                        else:
                            if mappingFieldsQc[fieldGroup].has_key(fieldQcName):
                                fieldName = fieldValue.get("FieldName");
                                fieldType = fieldValue.get("FieldType");
                                fieldIsRequired = fieldValue.get("FieldIsRequired");
                                fieldOrder = fieldValue.get("FieldOrder");
                                fieldQcName = fieldQcName;
                            else:
                                print "Given fieldQcName: %s is not in mappingFieldsQc"%(fieldQcName)
                                fieldType = '';
                                fieldIsRequired= '';
                                fieldName = '';
                                fieldQcName = ''; 
                                fieldOrder = fieldValue.get("FieldOrder");
                            ##Change place of fieldUserName and fieldName
                        fieldValue = {'FieldOrder':fieldOrder,   
                                      'FieldQcName':fieldQcName,
                                      'FieldName':fieldName,
                                      'FieldType':fieldType,
                                      'FieldIsRequired':fieldIsRequired
                                      };
                        mappingFieldsXML_checked[fieldGroup].update({fieldUserName: fieldValue});
            log.debug(("mappingFieldsXML_checked:", mappingFieldsXML_checked))
            ##now make correlation of mappingFieldsXML_checked and mappingFieldsUser. Because user would like add some more QCField to not linked fields ans save this under new name
            mappingFieldsJoin = self.joinDict(mappingFieldsXML_checked, mappingFieldsUser);
            ##Update TreeView_UserFiled  with the given mapping from xml file
            self.qCTestXUserField.updateModel(mappingFieldsJoin);
            del mappingFieldsJoin;
    
    def pushButton_QCTestX_DeleteFieldFile_clicked(self):
        '''
        Pushed Delete saved fields settings 
        '''
        log.debug("Clicked pushButton_QCTestX_DeleteFieldFile_clicked");
        
        ##Here I will have to make additional Widget to get SettingName
        settingName = str(self.guiQcTestXField.lineEdit_settingName.text());
        if settingName == "":
            text = "Please input 'Setting name' to delete";
            QtGui.QMessageBox.information(self, 
                                      "Open settings file",text);
            log.debug((text));
            return False
        
        try: 
            field_file_operation.OpenFile(self.savingType, self.qcType);
        except (field_file_operation.ParserException, 
                field_file_operation.OpenFileException,
                field_file_operation.CRCexception),\
                text:
            QtGui.QMessageBox.information(self, 
                                      "Open settings file",text);
            return False;
        ##Now open xml file with setting name "Setting_2" and get mapping of this setting
        saveXmlFile = field_file_operation.SaveFile(self.savingType, self.qcType);
        saveXmlFile.deletesetting(settingName);
        del saveXmlFile
        
        self.pushButton_QCTestX_RefreshFieldFile_clicked();
        
        
    def pushButton_QCTestX_RefreshFieldFile_clicked(self):
        '''
        Pushed refresh saved fields settings 
        '''
        log.debug("Clicked pushButton_QCTestX_RefreshFieldFile_clicked");
        
        
        listWidget = self.guiQcTestXField.listWidget_fieldSavedSettings;
        listWidget.clear();
        try: openXMLfile = field_file_operation.OpenFile(self.savingType, self.qcType);
        except field_file_operation.OpenFileException, err: 
            raise RefreshButtonException(err);
            return False;
        except field_file_operation.ParserException, err:
            raise RefreshButtonException(err);
            return False;
        except field_file_operation.CRCexception, err:
            raise RefreshButtonException(err);
            return False;
        except field_file_operation.SettingException, err:
            raise RefreshButtonException(err);
            return False;
        except field_file_operation.SettingNameException, err:
            raise RefreshButtonException(err);
            return False;
        except field_file_operation.SettingRootException, err:
            raise RefreshButtonException(err);
            return False;
         
        settingList = openXMLfile.itemSettings ##"Dictionaty of all seetings. {str(name):etree(element)}"
        if settingList is not None:
            settingList = settingList.keys(); ##"Dictionaty of all seetings. {str(name):etree(element)}"
        else:
            raise RefreshButtonException("Setting list is empty. No critical, but more information");
            return False
        
        log.debug(("From OpenXML file, settingList:",settingList));
        listWidget.addItems(settingList);
        listWidget.sortItems();
        
        return True;
    def translate_mappingFields(self, mapping_fields, type):
        '''
        Change structure of given dict() mapping_fields 
        
        *Input*
        @mapping_fields: dict() mapped fields names
                             {
                             'STEP':{'Step Name': {'FieldQcName':'ST_NAME', 
                                                  'FieldType':'char', 
                                                  'FieldIsRequired':'True'
                                                  }
                                    'Step Description': {'FieldQcName':'ST_DESC', 
                                                          'FieldType':'char', 
                                                          'FieldIsRequired':'True'
                                                          }
                                    },
                            'TEST':{'Test Case Name': {'FieldQcName':'TS_NAME', 
                                                          'FieldType':'char', 
                                                          'FieldIsRequired':'True'
                                                          }
                                    }
                             } 
                             
                             
                              
        *Output*
        @mapping_fields: dict() translated mapped fields names
                             {
                             'STEP':{'ST_NAME': {'FieldName':'Step Name', 
                                                  'FieldType':'char', 
                                                  'FieldIsRequired':'True'
                                                  }
                                    'ST_DESC': {'FieldName':'Step Description', 
                                                          'FieldType':'char', 
                                                          'FieldIsRequired':'True'
                                                          }
                                    },
                            'TEST':{'TS_NAME': {'FieldName':'Test Case Name', 
                                                'FieldType':'char', 
                                               'FieldIsRequired':'True'
                                             }
                                    }
                             } 
        '''
        mappingFields_translate = {};
        
        if type=="DL":
            for fieldGroup, fieldValues in mapping_fields.items():
                mappingFields_translate.update({fieldGroup:{}});#
                for fieldName, fieldValues in fieldValues.items():
#                    fieldValues_copy = deepcopy(fieldValues);
#                    fieldQcName = fieldValues.get("FieldQcName");
#                    fieldValues_copy.update({'FieldName':fieldName});
#                    try: del fieldValues_copy['FieldQcName'];
#                    except: log.exception("Error in: del fieldValues_copy['FieldQcName']");
                    fieldUserName = fieldValues.get('FieldUserName');
                    fieldType = fieldValues.get('FieldType');
                    fieldIsRequired = fieldValues.get('FieldIsRequired');
                    fieldQcName = fieldValues.get('FieldQcName');
                    fieldOrder = fieldValues.get('FieldOrder');
                    fieldValues = {'FieldOrder':fieldOrder,   
                                   'FieldName':fieldName,
                                   'FieldUserName':fieldUserName,
                                   'FieldType':fieldType,
                                   'FieldIsRequired':fieldIsRequired
                                   };
#                    fieldName = fieldValues[0]; 
#                    fieldType = fieldValues[1];
#                    fieldIsRequired = fieldValues[2];
#                    try: fieldOrder = fieldValues[3];
#                    except IndexError: fieldOrder = '';  
                    

                    ##Change place of fieldName and fieldQcName
                    mappingFields_translate[fieldGroup].update({fieldQcName: fieldValues});
        elif type=="UL":
            for fieldGroup, fieldValues in mapping_fields.items():
                mappingFields_translate.update({fieldGroup:{}});#
                for fieldName, fieldValues in fieldValues.items():
                    if fieldValues.get('FieldQcName') is None:
                        continue;
#                    fieldName = fieldValues.get('FieldName');
                    fieldUserName = fieldValues.get('FieldUserName');
                    fieldType = fieldValues.get('FieldType');
                    fieldIsRequired = fieldValues.get('FieldIsRequired');
                    fieldQcName = fieldValues.get('FieldQcName');
                    fieldOrder = fieldValues.get('FieldOrder');
                    fieldValues = {'FieldOrder':fieldOrder,   
                                   'FieldName':fieldName,
                                   'FieldUserName':fieldUserName,
                                   'FieldType':fieldType,
                                   'FieldIsRequired':fieldIsRequired
                                   };
                    mappingFields_translate[fieldGroup].update({fieldQcName: fieldValues});
        return mappingFields_translate;

    def joinDict(self, mappingFieldsXML_checked, mappingFieldsUser):
        '''
        Now make correlation of mappingFieldsXML_checked and mappingFieldsUser. Because user would like add some more QCField to not linked fields ans save this under new name
        :param mappingFieldsXML_checked:
        :param mappingFieldsUser:
        '''
        
        
#        mappingFieldsXML_checked = self.translate_mappingFields(mappingFieldsXML_checked, type="DL")##this is operation the same as operation on DL type data, that is why it is forced to use type="DL"
        
        
        for fieldGroup, fieldValues in mappingFieldsUser.items():
            if (not mappingFieldsXML_checked.has_key(fieldGroup)) or (mappingFieldsXML_checked[fieldGroup] == {}): 
#                mappingFieldsJoin.update({fieldGroup:fieldValues});#
                continue;
            else:
                for fieldUserName, fieldValue in fieldValues.iteritems():
                    if mappingFieldsXML_checked[fieldGroup].has_key(fieldUserName):
                        fieldValue.update(mappingFieldsXML_checked[fieldGroup].get(fieldUserName));
                        
        return mappingFieldsUser;
#                    mappingFieldsXML_checked[fieldGroup].update({fieldUserName: fieldValue});

        
    def get_fields(self):
        """Get Fields names used for TestLab/TestPlan upload/download
        Make mapping of DataBaseName to UserName field
        
        
        *Input*
        None
        
        *Output*
        @mapping_fields: dict() get QualityCenter fields names
                            {
                            'STEP':{'Step Name': {'FieldQcName':'ST_NAME', 
                                                  'FieldType':'char', 
                                                  'FieldIsRequired':'True'
                                                  }
                                    'Step Description': {'FieldQcName':'ST_DESC', 
                                                          'FieldType':'char', 
                                                          'FieldIsRequired':'True'
                                                          }
                                    },
                            'TEST':{'Test Case Name': {'FieldQcName':'TS_NAME', 
                                                          'FieldType':'char', 
                                                          'FieldIsRequired':'True'
                                                          }
                                    }
                             } 
        
        
        ##TestPlan
        dictTest = mappingFields['TEST']; ##TestPLan include TestCase
        dictDesStep = mappingFields['DESSTEPS']; ##TestPlan include Step
        
        ##TestLab
        dictCycle = mappingFields['CYCLE']; ##TestLab include TestSet+RunStatus
        dictTestcycl = mappingFields['TESTCYCL']; ##TestLab include TestCase+RunStatus
        dictStep = mappingFields['STEP']; ##TestLab include Step+RunStatus
        
        
        ##TestPlan
        dictTest.has_key('TS_NAME'); ##This is TestCase name from TestPlan and TestLab
        dictDesStep.has_key('DS_STEP_NAME'); ##This is Step name from TestPlan

        ##TestLab
        dictCycle.has_key('CY_CYCLE'); ##This is TestSet name from TestLab
        dictTestcycl.has_key('TS_NAME'); ##This is TestCase name from TestPlan and TestLab
        dictStep.has_key('ST_STEP_NAME'); ##This is Step name from TestLab
        """
        
        
        
        
        mapping_fields = {};
        
        for name in ["TEST", "DESSTEPS", "CYCLE", "TESTCYCL", "STEP"]:
            fieldList = self.qc.Fields(name);
            log.debug(("self.qc.Fields(name)=", fieldList));
            i = 0;
            mapping_fields.update({name:{}});
            
            mapping_fields[name].update({"Attachment":{'FieldQcName':"ATTACHMENT", 
                                                       'FieldType':"char",
                                                       'FieldIsRequired':"False"
                                                       }
                                         }); ##because Attachment is not in the Fields list, I have to add this here 
            for aField in fieldList:
                fieldProp = aField.Property;
                fieldQcName = aField.Name;
                fieldName=str(fieldProp.UserLabel);
                fieldIsActive=fieldProp.IsActive;
                fieldIsCanFilter=fieldProp.IsCanFilter;
                fieldType = fieldProp.UserColumnType; ##what type of data,  char,date, time
                fieldIsRequired = str(fieldProp.IsRequired) ##is this fields is in the list of required fields  
                if fieldIsActive: 
                    if mapping_fields[name].has_key(fieldName): ##it might be possible that in the TreeView are two, or more items with the same naming 
                        fieldName = fieldName+"_"+str(i); ##to distinguish those doubled naming add some independence numbering
                        log.double(("In the TreeView are the same named items, creating then fieldName:",fieldName));
                        i += 1;
                        
                    fieldValues = {'FieldQcName':fieldQcName,
                                   'FieldType':fieldType,
                                   'FieldIsRequired':fieldIsRequired
                                   };
                    mapping_fields[name].update({fieldName: fieldValues});
            
        return mapping_fields;

class QcTestXDownloadChooseFields(QtGui.QWidget, QcTestXCommonChooseFields):
    '''
    This class collects all information for Gui, when user will start selecting fields to download
    '''


    def __init__(self, type, parent=None, qcConnect=None):
        '''
        
        @param type: TestLab  or  TestPlan
        @param parent: QtWidget
        @param qcConnect: 
        '''
        
        
        super(QcTestXDownloadChooseFields, self).__init__(parent);
        QcTestXCommonChooseFields.__init__(self, savingType="DL", qcType=type);
        
        self.type = type; ##this is type of ChooseField. Different is for TestLab and TestPlan
        self.qcConnect = qcConnect;
        
        self.guiQcTestXField = GuiQcTestXDownloadField(parent);
        self.qCTestXQcField = MyTreeViewQcTestXDownloadQCField(self.guiQcTestXField.treeView_QCTestX_QcFields)
        self.qCTestXUserField = MyTreeViewQcTestXDownloadUserField(self.guiQcTestXField.treeView_QCTestX_UserFields)
        
        self.treeViewQcField = self.qCTestXQcField.treeView
        self.treeViewUserField = self.qCTestXUserField.treeView
        

        try: self.pushButton_QCTestX_RefreshFieldFile_clicked(); ##refresh list of items from saved Fields settings
        except: pass
        
        
        
        ##QC Test (Lab/Set) Download - tab 
        self.connect(self.guiQcTestXField.pushButton_QCTestX_Refresh, QtCore.SIGNAL("clicked()"),
             self.pushButton_QCTestXDownload_Refresh_clicked);
        self.connect(self.guiQcTestXField.pushButton_QCTestX_Add, QtCore.SIGNAL("clicked()"),
             self.pushButton_QCTestXDownload_Add_clicked);
        self.connect(self.guiQcTestXField.pushButton_QCTestX_Remove, QtCore.SIGNAL("clicked()"),
             self.pushButton_QCTestXDownload_Remove_clicked);
        self.connect(self.guiQcTestXField.pushButton_QCTestX_Clear, QtCore.SIGNAL("clicked()"),
             self.pushButton_QCTestXDownload_Clear_clicked);
        self.connect(self.guiQcTestXField.pushButton_QCTestX_Up, QtCore.SIGNAL("clicked()"),
             self.pushButton_QCTestX_Up_clicked);
        self.connect(self.guiQcTestXField.pushButton_QCTestX_Down, QtCore.SIGNAL("clicked()"),
             self.pushButton_QCTestX_Down_clicked);
        self.connect(self.guiQcTestXField.pushButton_QCTestX_SaveFieldFile, QtCore.SIGNAL("clicked()"),
             self.pushButton_QCTestX_SaveFieldFile_clicked);
        self.connect(self.guiQcTestXField.pushButton_QCTestX_OpenFieldFile, QtCore.SIGNAL("clicked()"),
             self.pushButton_QCTestX_OpenFieldFile_clicked);
        self.connect(self.guiQcTestXField.pushButton_QCTestX_RefreshFieldFile, QtCore.SIGNAL("clicked()"),
             self.pushButton_QCTestX_RefreshFieldFile_clicked);
        self.connect(self.guiQcTestXField.pushButton_QCTestX_DeleteFieldFile, QtCore.SIGNAL("clicked()"),
             self.pushButton_QCTestX_DeleteFieldFile_clicked);
        self.guiQcTestXField.listWidget_fieldSavedSettings.itemSelectionChanged.connect(self.update_lineEditSettingName);
#        self.treeViewQcField.selectionChanged.connect(self.clearSelectionTreeViewUserField);
        
        self.guiQcTestXField.pushButton_NextFields.clicked.connect(self.pushButton_NextField);
        self.guiQcTestXField.pushButton_CancelField.clicked.connect(self.pushButton_CancelField);
        
        self.guiQcTestXField.show();
        self.pushButton_QCTestXDownload_Refresh_clicked();
             
    
        
    def __get_qcConnect(self):
        return self.qcConnect.qc;
    qc = property(__get_qcConnect, None, None, "Instance of QC connection");

    def __get_QCconnection_status(self):
        try: status = self.qcConnect.qc.ProjectConnected;
        except: status = False;
        return status;
    QCconnection_status = property(__get_QCconnection_status, None, None, "Information of Quality Center connection status (True)");    
    
    def pushButton_NextField(self):
        log.debug("Clicked pushButton_NextFields");
        ##Get userMappingFields, which will be used in the QualityCenter TestLab Download main file
        self.userFieldsDict = self.qCTestXUserField.get_mappingFields();
        self.guiQcTestXField.close();
    
    def pushButton_CancelField(self):
        log.debug("Clicked pushButton_CancelField");
        self.guiQcTestXField.close();
    
    def pushButton_QCTestXDownload_Clear_clicked(self):
        log.debug("Clicked pushButton_QCTestXDownload_Clear_clicked");
        self.qCTestXUserField.clearItem();
#        self.pushButton_QCTestXDownload_Refresh_clicked(); ##we will need to update QCFiled columns, because we have removed every item from UserField column

    def pushButton_QCTestXDownload_Refresh_clicked(self):
        log.debug("Clicked pushButton_QCTestXDownload_Refresh_clicked()");
        
        if not self.QCconnection_status:
            log.warning("QCconnection_status is False. Unable to connect to QC server");
            return

        
        self.treeViewQcField.model().reset();
        mapping_fields  = self.get_fields();
        log.debug(("mapping_fields :", mapping_fields));
        if self.type == "TestLab":
            self.qCTestXQcField.updateModelTestLab(mapping_fields);
        elif self.type == "TestPlan":
            self.qCTestXQcField.updateModelTestPlan(mapping_fields);
        self.treeViewUserField.reset();
            
    def pushButton_QCTestXDownload_Add_clicked(self):
        log.debug("Clicked pushButton_QCTestXDownload_Add_clicked");
        item = self.treeViewQcField.currentIndex().internalPointer();
        if item is not None:
            ##Add selected item to the Tree of User Fields
            self.qCTestXUserField.addItem(item);
            self.treeViewUserField.repaint();
            #==================================================================
            #For operation reasons it would be better never remove Item from TreeView_QcFields
            #operation reasons - when You move item from QcField to UserField and the open XML setting, then those moved items will not present QcField  
            # ##Remove selected item from the Tree of Qc Fields
            # index = self.treeViewQcField.currentIndex()
            # self.qCTestXQcField.removeItem(index);
            #==================================================================
        else:
            log.warning("WRN: User did not selected QC Field to be moved");
            pass;
    def pushButton_QCTestXDownload_Remove_clicked(self):
        log.debug("Clicked pushButton_QCTestXDownload_Remove");
        item = self.treeViewUserField.currentIndex().internalPointer();
        if item is not None:
            #==================================================================
            #For operation reasons it would be better never Add Item to TreeView_QcFields
            #operation reasons - when remove item from QCField is disabled, not neede to run this add
            # ##Add selected item to the Tree of Qc Fields
            # self.qCTestXQcField.addItem(item);
            #==================================================================
            ##Remove selected item from the Tree of User Fields
            index = self.treeViewUserField.currentIndex()
            self.qCTestXUserField.removeItem(index);
        else:
            log.warning("WRN: User did not selected QC Field to be moved");
            pass;


class GuiQcgraphMain(Ui_QcGraph_Main):
    def __init__(self, main_QCgraph):
        Ui_QcGraph_Main.setupUi(self, main_QCgraph);


class SetQcTest(QcTestXDownloadChooseFields, GuiQcgraphMain, QtGui.QWidget):
    '''
    Main class for all TestLab/TestPlan Upload/Download
    '''
    
    def __init__(self, MainWindow):
        '''
        In QCTEstX window, fill all widgets with basic data
        '''
        QtGui.QWidget.__init__(self);
        
        self.__MainWindow = MainWindow;
        self.__MainWindowUI = MainWindow.ui;
        
        self.guiQcgraphMain = GuiQcgraphMain(self.mainWindowUI.main_QCgraph)
        
        QcTestXDownloadChooseFields.__init__(self, self.mainWindowUI.tab_CrossTest);
        
    
    
    def __get_MainWindowUI(self):
        return self.__MainWindowUI;
    mainWindowUI = property(__get_MainWindowUI, None, None, "Instance of MainWindow GUI")
    
    def __get_MainWindow(self):
        return self.__MainWindow;
    mainWindow = property(__get_MainWindow, None, None, "Instance of MainWindow")
    
    def __get_qcConnect(self):
        return self.mainWindow.qcConnect.qc;
    qc = property(__get_qcConnect, None, None, "Instance of QC connection");

    def __get_QCconnection_status(self):
        return self.mainWindow.QCconnection_status;
    QCconnection_status = property(__get_QCconnection_status, None, None, "Information of Quality Center connection status (True, 'Connected')");    
        
     



if __name__ == "__main__":
    
    
    
    
    QcTestXDownloadChooseFields();
    