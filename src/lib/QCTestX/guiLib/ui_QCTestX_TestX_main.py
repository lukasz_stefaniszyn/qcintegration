# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'QCTestX_TestX_main.ui'
#
# Created: Sat Dec 08 18:02:33 2012
#      by: PyQt4 UI code generator 4.9.1
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName(_fromUtf8("Form"))
        Form.resize(604, 573)
        self.gridLayout = QtGui.QGridLayout(Form)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.verticalLayout = QtGui.QVBoxLayout()
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.groupBox_QCGraphAxis = QtGui.QGroupBox(Form)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.groupBox_QCGraphAxis.sizePolicy().hasHeightForWidth())
        self.groupBox_QCGraphAxis.setSizePolicy(sizePolicy)
        self.groupBox_QCGraphAxis.setTitle(_fromUtf8(""))
        self.groupBox_QCGraphAxis.setObjectName(_fromUtf8("groupBox_QCGraphAxis"))
        self.verticalLayout_9 = QtGui.QVBoxLayout(self.groupBox_QCGraphAxis)
        self.verticalLayout_9.setObjectName(_fromUtf8("verticalLayout_9"))
        self.verticalLayout_5 = QtGui.QVBoxLayout()
        self.verticalLayout_5.setObjectName(_fromUtf8("verticalLayout_5"))
        self.phB_Run_QCTestXMapFields = QtGui.QPushButton(self.groupBox_QCGraphAxis)
        palette = QtGui.QPalette()
        brush = QtGui.QBrush(QtGui.QColor(175, 19, 19))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.ButtonText, brush)
        brush = QtGui.QBrush(QtGui.QColor(175, 19, 19))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.ButtonText, brush)
        brush = QtGui.QBrush(QtGui.QColor(118, 116, 108))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.ButtonText, brush)
        self.phB_Run_QCTestXMapFields.setPalette(palette)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.phB_Run_QCTestXMapFields.setFont(font)
        self.phB_Run_QCTestXMapFields.setAutoFillBackground(False)
        self.phB_Run_QCTestXMapFields.setObjectName(_fromUtf8("phB_Run_QCTestXMapFields"))
        self.verticalLayout_5.addWidget(self.phB_Run_QCTestXMapFields)
        self.verticalLayout_9.addLayout(self.verticalLayout_5)
        self.verticalLayout.addWidget(self.groupBox_QCGraphAxis)
        spacerItem = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem)
        self.phB_Run_QCTestX = QtGui.QPushButton(Form)
        palette = QtGui.QPalette()
        brush = QtGui.QBrush(QtGui.QColor(175, 19, 19))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.ButtonText, brush)
        brush = QtGui.QBrush(QtGui.QColor(175, 19, 19))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.ButtonText, brush)
        brush = QtGui.QBrush(QtGui.QColor(118, 116, 108))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.ButtonText, brush)
        self.phB_Run_QCTestX.setPalette(palette)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.phB_Run_QCTestX.setFont(font)
        self.phB_Run_QCTestX.setAutoFillBackground(False)
        self.phB_Run_QCTestX.setObjectName(_fromUtf8("phB_Run_QCTestX"))
        self.verticalLayout.addWidget(self.phB_Run_QCTestX)
        spacerItem1 = QtGui.QSpacerItem(20, 288, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem1)
        self.gridLayout.addLayout(self.verticalLayout, 0, 0, 1, 1)

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        Form.setWindowTitle(QtGui.QApplication.translate("Form", "Form", None, QtGui.QApplication.UnicodeUTF8))
        self.phB_Run_QCTestXMapFields.setText(QtGui.QApplication.translate("Form", "Map columns", None, QtGui.QApplication.UnicodeUTF8))
        self.phB_Run_QCTestX.setText(QtGui.QApplication.translate("Form", "RUN", None, QtGui.QApplication.UnicodeUTF8))

import resource_rc
