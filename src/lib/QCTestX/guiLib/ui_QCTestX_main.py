# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'QCgraph_main.ui'
#
# Created: Sat Dec 08 18:02:33 2012
#      by: PyQt4 UI code generator 4.9.1
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName(_fromUtf8("Form"))
        Form.resize(230, 670)
        self.gridLayout = QtGui.QGridLayout(Form)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.verticalLayout = QtGui.QVBoxLayout()
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.groupBox_QCGraphAxis = QtGui.QGroupBox(Form)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.groupBox_QCGraphAxis.sizePolicy().hasHeightForWidth())
        self.groupBox_QCGraphAxis.setSizePolicy(sizePolicy)
        self.groupBox_QCGraphAxis.setObjectName(_fromUtf8("groupBox_QCGraphAxis"))
        self.verticalLayout_9 = QtGui.QVBoxLayout(self.groupBox_QCGraphAxis)
        self.verticalLayout_9.setObjectName(_fromUtf8("verticalLayout_9"))
        self.verticalLayout_5 = QtGui.QVBoxLayout()
        self.verticalLayout_5.setObjectName(_fromUtf8("verticalLayout_5"))
        self.gridLayout_8 = QtGui.QGridLayout()
        self.gridLayout_8.setObjectName(_fromUtf8("gridLayout_8"))
        self.horizontalLayout_3 = QtGui.QHBoxLayout()
        self.horizontalLayout_3.setObjectName(_fromUtf8("horizontalLayout_3"))
        spacerItem = QtGui.QSpacerItem(58, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_3.addItem(spacerItem)
        self.label = QtGui.QLabel(self.groupBox_QCGraphAxis)
        self.label.setObjectName(_fromUtf8("label"))
        self.horizontalLayout_3.addWidget(self.label)
        self.gridLayout_8.addLayout(self.horizontalLayout_3, 0, 0, 1, 1)
        self.comboBox_Xaxis = QtGui.QComboBox(self.groupBox_QCGraphAxis)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(12)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.comboBox_Xaxis.sizePolicy().hasHeightForWidth())
        self.comboBox_Xaxis.setSizePolicy(sizePolicy)
        self.comboBox_Xaxis.setMinimumSize(QtCore.QSize(60, 0))
        self.comboBox_Xaxis.setMaximumSize(QtCore.QSize(180, 16777215))
        self.comboBox_Xaxis.setInsertPolicy(QtGui.QComboBox.InsertAlphabetically)
        self.comboBox_Xaxis.setSizeAdjustPolicy(QtGui.QComboBox.AdjustToMinimumContentsLength)
        self.comboBox_Xaxis.setMinimumContentsLength(22)
        self.comboBox_Xaxis.setObjectName(_fromUtf8("comboBox_Xaxis"))
        self.comboBox_Xaxis.addItem(_fromUtf8(""))
        self.gridLayout_8.addWidget(self.comboBox_Xaxis, 1, 0, 1, 1)
        self.verticalLayout_5.addLayout(self.gridLayout_8)
        self.gridLayout_10 = QtGui.QGridLayout()
        self.gridLayout_10.setObjectName(_fromUtf8("gridLayout_10"))
        self.horizontalLayout_4 = QtGui.QHBoxLayout()
        self.horizontalLayout_4.setObjectName(_fromUtf8("horizontalLayout_4"))
        spacerItem1 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_4.addItem(spacerItem1)
        self.label_2 = QtGui.QLabel(self.groupBox_QCGraphAxis)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.horizontalLayout_4.addWidget(self.label_2)
        self.gridLayout_10.addLayout(self.horizontalLayout_4, 0, 0, 1, 1)
        self.comboBox_Yaxis = QtGui.QComboBox(self.groupBox_QCGraphAxis)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.comboBox_Yaxis.sizePolicy().hasHeightForWidth())
        self.comboBox_Yaxis.setSizePolicy(sizePolicy)
        self.comboBox_Yaxis.setMinimumSize(QtCore.QSize(60, 0))
        self.comboBox_Yaxis.setMaximumSize(QtCore.QSize(180, 16777215))
        self.comboBox_Yaxis.setInsertPolicy(QtGui.QComboBox.InsertAlphabetically)
        self.comboBox_Yaxis.setSizeAdjustPolicy(QtGui.QComboBox.AdjustToMinimumContentsLength)
        self.comboBox_Yaxis.setMinimumContentsLength(22)
        self.comboBox_Yaxis.setObjectName(_fromUtf8("comboBox_Yaxis"))
        self.comboBox_Yaxis.addItem(_fromUtf8(""))
        self.comboBox_Yaxis.addItem(_fromUtf8(""))
        self.gridLayout_10.addWidget(self.comboBox_Yaxis, 1, 0, 1, 1)
        self.verticalLayout_5.addLayout(self.gridLayout_10)
        self.gridLayout_14 = QtGui.QGridLayout()
        self.gridLayout_14.setObjectName(_fromUtf8("gridLayout_14"))
        self.horizontalLayout_5 = QtGui.QHBoxLayout()
        self.horizontalLayout_5.setObjectName(_fromUtf8("horizontalLayout_5"))
        spacerItem2 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_5.addItem(spacerItem2)
        self.label_3 = QtGui.QLabel(self.groupBox_QCGraphAxis)
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.horizontalLayout_5.addWidget(self.label_3)
        self.gridLayout_14.addLayout(self.horizontalLayout_5, 0, 0, 1, 1)
        self.comboBox_GraphType = QtGui.QComboBox(self.groupBox_QCGraphAxis)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.comboBox_GraphType.sizePolicy().hasHeightForWidth())
        self.comboBox_GraphType.setSizePolicy(sizePolicy)
        self.comboBox_GraphType.setMinimumSize(QtCore.QSize(60, 0))
        self.comboBox_GraphType.setMaximumSize(QtCore.QSize(180, 16777215))
        self.comboBox_GraphType.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.comboBox_GraphType.setObjectName(_fromUtf8("comboBox_GraphType"))
        self.comboBox_GraphType.addItem(_fromUtf8(""))
        self.comboBox_GraphType.addItem(_fromUtf8(""))
        self.gridLayout_14.addWidget(self.comboBox_GraphType, 1, 0, 1, 1)
        self.verticalLayout_5.addLayout(self.gridLayout_14)
        spacerItem3 = QtGui.QSpacerItem(20, 18, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout_5.addItem(spacerItem3)
        self.horizontalLayout_6 = QtGui.QHBoxLayout()
        self.horizontalLayout_6.setObjectName(_fromUtf8("horizontalLayout_6"))
        spacerItem4 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_6.addItem(spacerItem4)
        self.pushButton_Refresh_QCGraphAxis = QtGui.QPushButton(self.groupBox_QCGraphAxis)
        palette = QtGui.QPalette()
        brush = QtGui.QBrush(QtGui.QColor(0, 0, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.ButtonText, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 0, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.ButtonText, brush)
        brush = QtGui.QBrush(QtGui.QColor(118, 116, 108))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.ButtonText, brush)
        self.pushButton_Refresh_QCGraphAxis.setPalette(palette)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(_fromUtf8(":/icon/icon/gtk-refresh.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.pushButton_Refresh_QCGraphAxis.setIcon(icon)
        self.pushButton_Refresh_QCGraphAxis.setObjectName(_fromUtf8("pushButton_Refresh_QCGraphAxis"))
        self.horizontalLayout_6.addWidget(self.pushButton_Refresh_QCGraphAxis)
        self.verticalLayout_5.addLayout(self.horizontalLayout_6)
        self.verticalLayout_9.addLayout(self.verticalLayout_5)
        self.verticalLayout.addWidget(self.groupBox_QCGraphAxis)
        spacerItem5 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem5)
        self.phB_Run_QCGraph = QtGui.QPushButton(Form)
        palette = QtGui.QPalette()
        brush = QtGui.QBrush(QtGui.QColor(175, 19, 19))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.ButtonText, brush)
        brush = QtGui.QBrush(QtGui.QColor(175, 19, 19))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.ButtonText, brush)
        brush = QtGui.QBrush(QtGui.QColor(118, 116, 108))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.ButtonText, brush)
        self.phB_Run_QCGraph.setPalette(palette)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.phB_Run_QCGraph.setFont(font)
        self.phB_Run_QCGraph.setAutoFillBackground(False)
        self.phB_Run_QCGraph.setObjectName(_fromUtf8("phB_Run_QCGraph"))
        self.verticalLayout.addWidget(self.phB_Run_QCGraph)
        spacerItem6 = QtGui.QSpacerItem(20, 288, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem6)
        self.gridLayout.addLayout(self.verticalLayout, 0, 0, 1, 1)

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        Form.setWindowTitle(QtGui.QApplication.translate("Form", "Form", None, QtGui.QApplication.UnicodeUTF8))
        self.groupBox_QCGraphAxis.setTitle(QtGui.QApplication.translate("Form", "Axis", None, QtGui.QApplication.UnicodeUTF8))
        self.label.setText(QtGui.QApplication.translate("Form", "X-axis", None, QtGui.QApplication.UnicodeUTF8))
        self.comboBox_Xaxis.setItemText(0, QtGui.QApplication.translate("Form", "None", None, QtGui.QApplication.UnicodeUTF8))
        self.label_2.setText(QtGui.QApplication.translate("Form", "Y-axis", None, QtGui.QApplication.UnicodeUTF8))
        self.comboBox_Yaxis.setItemText(0, QtGui.QApplication.translate("Form", "None", None, QtGui.QApplication.UnicodeUTF8))
        self.comboBox_Yaxis.setItemText(1, QtGui.QApplication.translate("Form", "12345678901234567890", None, QtGui.QApplication.UnicodeUTF8))
        self.label_3.setText(QtGui.QApplication.translate("Form", "Graph Type", None, QtGui.QApplication.UnicodeUTF8))
        self.comboBox_GraphType.setItemText(0, QtGui.QApplication.translate("Form", "Summary", None, QtGui.QApplication.UnicodeUTF8))
        self.comboBox_GraphType.setItemText(1, QtGui.QApplication.translate("Form", "Progress", None, QtGui.QApplication.UnicodeUTF8))
        self.pushButton_Refresh_QCGraphAxis.setText(QtGui.QApplication.translate("Form", "Refresh", None, QtGui.QApplication.UnicodeUTF8))
        self.phB_Run_QCGraph.setText(QtGui.QApplication.translate("Form", "RUN", None, QtGui.QApplication.UnicodeUTF8))

import resource_rc
