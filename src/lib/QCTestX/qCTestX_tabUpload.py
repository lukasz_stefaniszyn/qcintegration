from PyQt4 import QtCore, QtGui



#from treeView.treeResultModel import TreeView as ResultTreeView
#from treeView.treeResultModel import MyDirModel as ResultMyDirModel

from guiLib.ui_QCgraph_tabTestX_upload import Ui_Form as Ui_TestX_upload

from lib.logfile import Logger
log = Logger(loggername="lib.QCTestX.qCTestX_tabUpload", resetefilelog=False).log;

 

class GuiQcTestXUpload(Ui_TestX_upload):
    '''This class is used to initialize, control
     TreeViews and buttons which are on the whole tab
    '''
    def __init__(self, parent):
        ##Add Widget responsible for tab_CrossTest
        ##This initialize QcGraph tab Cross Test  
        Ui_TestX_upload.setupUi(self, parent);


class ViewTestXChooseFile():
    '''
    Operation in the QCgraph -> Cross test -> Result directories
    Used only for Directory view, and open result files in those directories. Based on  self.dir   
    '''
    
    
    def __init__(self):
        ##Add Widget responsible for tab_CrossTest
        ##This initialize QcGraph tab Cross Test  
        
        pass;
     
    
