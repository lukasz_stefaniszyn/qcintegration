'''
Created on 18-04-2011

@author: wro50026
'''
from PyQt4 import QtGui

import xml.etree.ElementTree as etree
import xml.parsers.expat


import zlib
import os

from lib.logfile import Logger
from globalVariables import GlobalVariables
#import QCinter
log = Logger(loggername="lib.QCTestX.field_file_operation", resetefilelog=False).log;



global FILENAME 
FILENAME = os.path.join(GlobalVariables.DIR_CONF, r"mappingfields.xml");


class CRCexception(Exception):
    def __init__(self, text):
        log.exception(("CRCexception: ", text))
        self.text = text;
        pass
    def __str__(self):
        return self.text;

class ParserException(Exception):
    text = "";
    def __init__(self, text):
        log.exception(("ParserException: ", text))
        self.text = text
        pass
    def __str__(self):
        return self.text;

class OpenFileException(Exception):
    text = "";
    def __init__(self, text):
        log.exception(("OpenFileException: ", text))
        self.text = text
        pass
    def __str__(self):
        return self.text;

class SettingNameException(Exception):
    def __init__(self, text):
        log.exception(("SettingNameException: ", text))
        self.text = text;
        pass
    def __str__(self):
        return self.text;

class SettingException(Exception):
    def __init__(self, text):
        log.exception(("SettingException: ", text))
        self.text = text;
        pass
    def __str__(self):
        return self.text;

class SettingTypeException(Exception):
    def __init__(self, text):
        log.exception(("SettingTypeException: ", text))
        self.text = text;
        pass
    def __str__(self):
        return self.text;


class SettingRootException(Exception):
    def __init__(self, text):
        log.exception(("SettingRootException: ", text))
        self.text = text;
        pass
    def __str__(self):
        return self.text;

class OpenFile(object):
    '''
    Open XML file
    '''



    __parent=None

    def __init__(self, type, qcType):
        ##type= "DL"  or "UL" this will give information what XML setting will be used 
        try: 
            self.__tree = etree.parse(FILENAME)
        except IOError, err:
            log.exception("Not able to open file: %s. Error in: %s"%(FILENAME, err));
            raise OpenFileException("Not able to open file: %s"%(FILENAME));
        except xml.parsers.expat.ExpatError, err:
            log.exception("XML parser failed on file: %s. Error in: %s"%(FILENAME, err));
            raise ParserException("XML parser failed on file: %s"%(FILENAME));
        self.__root = self.tree.getroot();
        self.__rootSettings = self.root.find('Settings'); 
        if self.rootSettings is None:
            raise SettingException("main \'Settings\' tag is missing")
        
        self.__qcType = qcType; 
        
        
        self.__rootCRC = self.root.find('CRCsum');
        if not self.checkCRC():
            raise CRCexception("CRCsum is incorrect. Manual changes has been made on setting file");
        
        if type=="DL":
            self.__rootSettingType = self.rootSettings.find('SettingsDownload');
        elif type=="UL":
            self.__rootSettingType = self.rootSettings.find('SettingsUpload');
        else:
            raise SettingTypeException("Wrong main Setting Type name: %s"%type);
        
#        if self.rootSettingType is None:
#            raise SettingTypeException("main \'SettingsType\' tag is missing")

        


        
        
    def __str__(self):
        return etree.tostring(self.root, 'UTF-8');
    #----------------------------------------------------------------------------- 

    def get_root_settings(self):
        return self.__rootSettings
    rootSettings = property(get_root_settings, None, None, "rootSettings's docstring")

    def get_qc_type(self):
        return self.__qcType
    qcType = property(get_qc_type, None, None, "qcType's docstring")
    
    def __get__root(self):
        return self.__root;
    root = property(__get__root, None, None, "Root of xml structure") 
    def __get__rootSettingType(self):
        return self.__rootSettingType;
    rootSettingType = property(__get__rootSettingType, None, None, "Root setttings type (DL or UL)of xml structure") 
    def __get__rootCRC(self):
        return self.__rootCRC;
    rootCRC = property(__get__rootCRC, None, None, "Root CRC of xml structure") 
    def __get__tree(self):
        return self.__tree;
    tree = property(__get__tree, None, None, "Main tree of the xml");
    def __get__parent(self):
        log.debug(("Current get parent:", self.__parent))
        if self.__parent is None:
            log.exception("Current read parent is NONE");
            raise Exception("Current read parent is NONE")
        else:
            return self.__parent;
    def __set__parent(self, parent):
        self.__parent = parent;
        log.debug(("Current set parent:", self.__parent))
    parent = property(__get__parent, __set__parent, None, "This is parent of Settings")
    def __get__itemSettings(self):
        '''
        Return dictionary list of current settings saved in xml file
        *Output*
        dict(settingName:etree(element)) 
        '''
        if self.rootSettingType is None:##if there is non of this SettingType element, then return nothing
            return None;
        
        itemSettings = {};
        for item in self.rootSettingType.findall("Setting"):
            if item.get('QCtype') == self.qcType:
                itemSettings[item.get('name')]=item;
        return itemSettings;
    itemSettings = property(__get__itemSettings, None, None, "Dictionaty of all seetings. {str(name):etree(element)}");
    #----------------------------------------------------------------------------- 
    def checkCRC(self):
        textrootSettings = etree.tostring(self.rootSettings, 'UTF-8');
        crcsumFileCalculated = int(zlib.crc32(textrootSettings)&0xffffffff);
        log.debug(("CRC sum of file, new calculation:", crcsumFileCalculated));

        crcsumFile = int(self.rootCRC.text) ;
        log.debug(("CRC sum of saved value in file:", crcsumFile));
        if crcsumFileCalculated != crcsumFile:
            return False;
        else:
            return True

    def get_mappingfields(self, settingName,type):
        if type=="DL":
            return self.__get_mappingfieldsDownload(settingName);
        elif type=="UL":
            return self.__get_mappingfieldsUpload(settingName);
        else:
            raise Exception("Wrong Type value in write_mappingFields: %s"%type);


    def __get_mappingfieldsUpload(self, settingName):
        '''
        This will return mapping fields, used in TreeView_UserFields
        *Input*
        @param settingName: str(), this is saved setting name, e.g. "Setting_1"
        
        *Output*
        mapping_fields = dict(), 
                                 {"TestSet": {'Test Set Name':{'FieldOrder':'1',
                                                              'FieldQcName':'TS_NAME',
                                                              'FieldUSerName':'Test Set Name',
                                                              'FieldType':'char',
                                                              'FieldIsRequired':'True',
                                                              }
                                            },
                                             'Test Set Directory': {'FieldOrder':'1',
                                                              'FieldQcName':'TS_DIR',
                                                              'FieldUserName':'Test Set Directory',
                                                              'FieldType':'char',
                                                              'FieldIsRequired':'True'},
                                            },
                                 "TestCase": {'Test Case Name': {'FieldOrder':'1',
                                                              'FieldQcName':'TC_NAME',
                                                              'FieldUserName':'Test Case Name',
                                                              'FieldType':'char',
                                                              'FieldIsRequired':'True'},
                                              'Test Case Directory': {'FieldOrder':'1',
                                                              'FieldQcName':'TC_DIR',
                                                              'FieldUserName':'Test Case Directory',
                                                              'FieldType':'char',
                                                              'FieldIsRequired':'True'}
                                             },
                                 "Step": {'Step Name': {'FieldOrder':'1',
                                                        'FieldQcName':'ST_NAME',
                                                        'FieldUserName':'Step Name',
                                                        'FieldType':'char',
                                                        'FieldIsRequired':'True'}
                                          }
                                }
        '''
        if not self.itemSettings.has_key(settingName):
            raise SettingNameException("Given setting name does not exists. Skipping: %s"%str(settingName));
        
        elementParent = self.itemSettings[settingName];
        
        mapping_fields = {};
        for elementGroup in elementParent.getchildren():
            mapping_fields.update({elementGroup.tag:{}});
            for elementField in elementGroup.getchildren():
                fieldName = elementField.get('FieldName');
                fieldUserName = elementField.get('FieldUserName');
                fieldType = elementField.get('FieldType');
                fieldIsRequired = elementField.get('FieldIsRequired');
                fieldQcName = elementField.get('FieldQcName');
                fieldOrder = elementField.get('FieldOrder');
                fieldValues = {'FieldOrder':fieldOrder,   
                               'FieldQcName':fieldQcName,
                               'FieldName':fieldName,
                               'FieldType':fieldType,
                               'FieldIsRequired':fieldIsRequired,
                               "FieldUserName":fieldUserName
                               };
                mapping_fields[elementGroup.tag].update({fieldName: fieldValues});
        return mapping_fields;

    def __get_mappingfieldsDownload(self, settingName):
        '''
        This will return mapping fields, used in TreeView_UserFields
        *Input*
        @param settingName: str(), this is saved setting name, e.g. "Setting_1"
        
        *Output*
        mapping_fields = dict(), 
                          {
                             'Step':{'Step Name': {'FieldQcName':'ST_NAME', 
                                                  'FieldType':'char', 
                                                  'FieldIsRequired':'True',
                                                  'FieldOrder':'1'
                                                  }
                                    'Step Description': {'FieldQcName':'ST_DESC', 
                                                          'FieldType':'char', 
                                                          'FieldIsRequired':'True',
                                                          'FieldOrder':'1'
                                                          }
                                    },
                            'TestSet':{'Test Case Name': {'FieldQcName':'TS_NAME', 
                                                          'FieldType':'char', 
                                                          'FieldIsRequired':'True',
                                                          'FieldOrder':'1'
                                                          }
                                    }
                             } 
        '''
        
        if not self.itemSettings.has_key(settingName):
            raise SettingNameException("Given setting name does not exists. Skipping: %s"%str(settingName));
        
        elementParent = self.itemSettings[settingName];
        
        mapping_fields = {};
        for elementGroup in elementParent.getchildren():
            mapping_fields.update({elementGroup.tag:{}});
            for elementField in elementGroup.getchildren():
                fieldQcName = elementField.get('FieldQcName');
                fieldName = elementField.get('FieldName');
                fieldType = elementField.get('FieldType');
                fieldIsRequired = elementField.get('FieldIsRequired');
                fieldOrder = elementField.get('FieldOrder');
                fieldValues = {'FieldOrder':fieldOrder,   
                               'FieldQcName':fieldQcName,
                               'FieldType':fieldType,
                               'FieldIsRequired':fieldIsRequired
                               };
                mapping_fields[elementGroup.tag].update({fieldName: fieldValues});
        return mapping_fields;
    


#    def getparent(self, settingName):
#        self.parent = self.rootSettingType.find(settingName);
#        log.debug(("Set parent element to:", self.parent.tag));
#
#    
#    
#    def readParents(self):
#        settingList = [];
#        for element in self.rootSettingType.getchildren():
#            settingList.append(element.tag)
#        return settingList;



class SaveFile():
    '''
    classdocs
    '''

    parent=None

    def __init__(self, type, qcType):
        ##type= "DL"  or "UL" this will give information what XML setting will be used 
        if os.path.exists(FILENAME):
            openFile = OpenFile(type, qcType);
            self.__root = openFile.root;
            self.__tree = openFile.tree;
            self.__rootSettings = openFile.rootSettings;
            self.__rootSettingType = openFile.rootSettingType;
            if self.__rootSettingType is None: 
                if type=="DL":
                    self.__rootSettingType = etree.SubElement(self.rootSettings, 'SettingsDownload')
                elif type=="UL":
                    self.__rootSettingType = etree.SubElement(self.rootSettings, 'SettingsUpload')
                else:
                    raise SettingRootException("Not able to find __rootSetting");
            
            self.__rootCRC = openFile.rootCRC;
            
        else:
            self.__root = etree.Element ('xml')
            self.__tree = etree.ElementTree(self.root)
            self.__rootSettings = etree.SubElement(self.root, 'Settings')
            if type=="DL":
                self.__rootSettingType = etree.SubElement(self.rootSettings, 'SettingsDownload')
            elif type=="UL":
                self.__rootSettingType = etree.SubElement(self.rootSettings, 'SettingsUpload')
            else:
                raise SettingException("Wrong main Setting Type name: %s"%type);
            self.__rootCRC = etree.SubElement(self.root, 'CRCsum')

#        FILENAME = filename;
        
    def __del__(self):
        self.writeFile();
        
    def __str__(self):
        return etree.tostring(self.root, 'UTF-8');
    
    
    def get_root_settings(self):
        return self.__rootSettings
    rootSettings = property(get_root_settings, None, None, "rootSettings's docstring")
    
    def __get__root(self):
        return self.__root;
    root = property(__get__root, None, None, "Root of xml structure") 
    def __get__rootSettingType(self):
        log.debug(("self.__rootSettingType:", self.__rootSettingType.tag));
        return self.__rootSettingType;
    rootSettingType = property(__get__rootSettingType, None, None, "Root setttings of xml structure") 
    def __get__rootCRC(self):
        return self.__rootCRC;
    rootCRC = property(__get__rootCRC, None, None, "Root CRC of xml structure") 
    def __get__tree(self):
        return self.__tree;
    tree = property(__get__tree, None, None, "Main Tree of the xml");
    def __get__itemSettings(self):
        '''
        Return dictionary list of current settings saved in xml file
        *Output*
        @param itemSettings: dict(settingName:etree(element)) 
        '''
        itemSettings = {};
        for item in self.rootSettingType.findall("Setting"):
            itemSettings[item.get('name')]=item;
        return itemSettings;
    itemSettings = property(__get__itemSettings, None, None, "Dictionaty of all seetings. {str(name):etree(element)}");
    
    
    
    
    def __get__parent(self):
        log.debug(("Current read parent:", self.__parent))
        if self.__parent is None:
            log.exception("Current read parent is NONE");
            raise Exception("Current read parent is NONE")
        else:
            return self.__parent;
    def __set__parent(self, parent):
        self.__parent = parent;
        log.debug(("Current write parent:", self.__parent))
    parent = property(__get__parent, __set__parent, None, "This is parent of Settings")
    
    
    def genCRC(self):
        '''
        Generate CRC sum on the given XML root structure
        '''
        
        textRoot = etree.tostring(self.rootSettings, 'UTF-8'); ##generate CRC on all Settings 
        crcsum = str(zlib.crc32(textRoot)&0xffffffff);
        log.debug(("CRC sum:", crcsum));
        self.rootCRC.text = crcsum; ##save generated CRC sum into to root XML structure
    

    
    
    def deletesetting(self, settingName):
        '''
        Delete one setting
        *Input*
        @param settingName: str() name of setting
        
        *Output*
        True/False 
        '''
        if settingName == "":
            log.debug("Did not find element")
            return False;
        if self.itemSettings.has_key(settingName):
            self.rootSettingType.remove(self.itemSettings[settingName]); ##remove setting
            return True;
        else:
            log.debug(("Did not find settingName: %s to remove in xml file"%settingName));
            return False;
    
    def makesetting(self, settingName, qcType):
        '''
        This will create root xml tag for writing settings.
               
        *input*
        @param settingName: str(), this is setting name which will be seen for user
        @param qcType: str(), this is information of QC type. Is is TestPlan=TP or TestLab=TL 
                
        *output*
        None or  Exceptions
        '''
        self.deletesetting(settingName);##in case when given settingName was created, then remove it and create new one
        ##create given settingName, then make new XML setting
        self.parent = etree.SubElement(self.rootSettingType, "Setting");
        self.parent.attrib['name'] = settingName
        self.parent.attrib['QCtype'] = qcType
        self.makeTemplateSettings()
        
        
    def makeTemplateSettings(self):
        '''
        Create template of save field values as one Setting 
        '''
        log.debug(("Create child for one Setting-Test Set of parent:", self.parent.tag))
        self.elementTestSet = etree.SubElement(self.parent, 'TestSet');
        
        log.debug(("Create child for one Setting-Test Case of parent:", self.parent.tag))
        self.elementTestCase = etree.SubElement(self.parent, 'TestCase');
        
        log.debug(("Create child for one Setting-Step of parent:", self.parent.tag))
        self.elementStep = etree.SubElement(self.parent, 'Step');
    
    def __setElement(self, parent, elementName, elementValue):
        '''
        Create and set new Element item in the XML tree under given parent.
        Main purpose of this function is to prevent inserting 'elementValue' with wrong strings, like NoneType
        
        *input*
        @param parent: etree(), this is elementTree parent, under which element will be created
        @param elementName: str(), this is name of the new created ElementTree 
        @param elementValue: str(), this is value of the new created ElementTree
        
        *output*
        '''
        if elementValue is None: elementValue=''; 
        parent.set(elementName, elementValue);
    
    def makeFieldItem(self, 
                      fieldGroup='',
                      fieldUserName='', 
                      fieldName='',
                      fieldType='',
                      fieldIsRequired='',
                      fieldOrder='', 
                      fieldQcName=''
                      ):
        '''
        Create one item of Field selected by user
        @param fieldGroup: this is group name where this field belongs, example TestSet, TestCase, Step
        @param fieldUserName: this is field user name, example 'Test Set Name', 'Regression'
        @param fieldName: this is field qc name, example 'TS_NAME', 'TC_USER_01' 
        @param fieldType: this is field type of data, example 'char', 'data', 'intiger'
        @param fieldIsRequired: this is field status if is requered from qc,  example 'True', 'False'
        @param fieldOrder: this is order number of the selected fields by user in the fieldGroup, example  int(1), int(10) 
        '''
        if fieldGroup=="TestSet":
            parent = self.elementTestSet;
        elif fieldGroup=='TestCase':
            parent = self.elementTestCase;
        elif fieldGroup=='Step':
            parent = self.elementStep;
        else:
            log.debug("Unknown fieldGroup, skipping add Field in setting");
            return False;
        
        
        log.debug(("Create child for FieldName of parent:", self.parent.tag))
        parent = etree.SubElement(parent, 'Field');
        self.__setElement(parent, 'FieldName', fieldName);
        self.__setElement(parent, 'FieldUserName', fieldUserName);
        self.__setElement(parent, 'FieldType', fieldType);
        self.__setElement(parent, 'FieldIsRequired', fieldIsRequired);
        self.__setElement(parent, 'FieldOrder', str(fieldOrder));
        self.__setElement(parent, 'FieldQcName', str(fieldQcName));
        
        
#    
    def writeFile(self):

        try:
            outFile = open(FILENAME, 'wb');
        except IOError, err:
            log.error("ERR: Unable to create file. ", err);
            return
        self.genCRC();
        self.tree.write(outFile, encoding="UTF-8")
        outFile.flush();
        outFile.close();

    def write_mappingfields(self, settingName, qcType, mappingFields, type):
        if type=="DL":
            self.__write_mappingfieldsDownload(settingName, qcType, mappingFields);
        elif type=="UL":
            self.__write_mappingfieldsUpload(settingName, qcType, mappingFields);
        else:
            raise Exception("Wrong Type value in write_mappingFields: %s"%type);

    def __write_mappingfieldsDownload(self, settingName, qcType, mappingFields):
        '''
        This will write all current Download UserFields in the XML file under one Setting
        
        *Input*
        @param settingName: str(),  setting name e.g. "Setting_1"
        @param qcType: str(), this is Qc type , like TestPlan or TestLab 
        @param mappingFields: dict(), this is list of Download UserFiled
                          {
                             'Step':{'Step Name': {'FieldQcName':'ST_NAME', 
                                                  'FieldType':'char', 
                                                  'FieldIsRequired':'True',
                                                  'FieldOrder':'1'
                                                  }
                                    'Step Description': {'FieldQcName':'ST_DESC', 
                                                          'FieldType':'char', 
                                                          'FieldIsRequired':'True',
                                                          'FieldOrder':'1'
                                                          }
                                    },
                            'TestSet':{'Test Case Name': {'FieldQcName':'TS_NAME', 
                                                          'FieldType':'char', 
                                                          'FieldIsRequired':'True',
                                                          'FieldOrder':'1'
                                                          }
                                    }
                             } 
                                 
        '''
        
        self.makesetting(settingName, qcType);
        
        
        for fieldTreeGroupName, filedValues in mappingFields.items():
            for fieldName, fieldValue in filedValues.iteritems():
                if not fieldValue.has_key('FieldOrder'): fieldValue.update({'FieldOrder':''});
                if not fieldValue.has_key('FieldQcName'): fieldValue.update({'FieldQcName':''});
                if not fieldValue.has_key('FieldName'): fieldValue.update({'FieldName':''});
                if not fieldValue.has_key('FieldType'): fieldValue.update({'FieldType':''});
                if not fieldValue.has_key('FieldIsRequired'): fieldValue.update({'FieldIsRequired':''});
                
                fieldQcName = fieldValue.get('FieldQcName');
                fieldType = fieldValue.get('FieldType');
                fieldIsRequired = fieldValue.get('FieldIsRequired');
                fieldOrder = fieldValue.get('FieldOrder');
                self.makeFieldItem(
                                   fieldGroup=fieldTreeGroupName,\
                                   fieldName=fieldName,\
                                   fieldQcName=fieldQcName,\
                                   fieldType=fieldType,\
                                   fieldIsRequired=fieldIsRequired,\
                                   fieldOrder=fieldOrder
                                   );
    
    def __write_mappingfieldsUpload(self, settingName, qcType, mappingFields):
        '''
        This will write all current Upload UserFields in the XML file under one Setting
        
        *Input*
        @param settingName: str(),  setting name e.g. "Setting_1"
        @param qcType: str(), this is Qc type , like TestPlan or TestLab
        @param mappingFields: dict(), this is list of Upload UserFiled
                                 {"TestSet": {'Test Set Name':{'FieldOrder':'1',
                                                              'FieldQcName':'TS_NAME',
                                                              'FieldName':'Test Set Name',
                                                              'FieldType':'char',
                                                              'FieldIsRequired':'True',
                                                              }
                                            },
                                             'Test Set Directory': {'FieldOrder':'1',
                                                              'FieldQcName':'TS_DIR',
                                                              'FieldName':'Test Set Directory',
                                                              'FieldType':'char',
                                                              'FieldIsRequired':'True'},
                                            },
                                 "TestCase": {'Test Case Name': {'FieldOrder':'1',
                                                              'FieldQcName':'TC_NAME',
                                                              'FieldName':'Test Case Name',
                                                              'FieldType':'char',
                                                              'FieldIsRequired':'True'},
                                              'Test Case Directory': {'FieldOrder':'1',
                                                              'FieldQcName':'TC_DIR',
                                                              'FieldName':'Test Case Directory',
                                                              'FieldType':'char',
                                                              'FieldIsRequired':'True'}
                                             },
                                 "Step": {'Step Name': {'FieldOrder':'1',
                                                        'FieldQcName':'ST_NAME',
                                                        'FieldName':'Step Name',
                                                        'FieldType':'char',
                                                        'FieldIsRequired':'True'}
                                          }
                                }
        *Output*
        None
        '''
        self.makesetting(settingName, qcType);
        
        
        for fieldTreeGroupName, fieldValues in mappingFields.items():
            for fieldUserName, fieldValue in fieldValues.iteritems():
                if not fieldValue.has_key('FieldOrder'): fieldValue.update({'FieldOrder':''});
                if not fieldValue.has_key('FieldQcName'): fieldValue.update({'FieldQcName':''});
                if not fieldValue.has_key('FieldName'): fieldValue.update({'FieldName':''});
                if not fieldValue.has_key('FieldType'): fieldValue.update({'FieldType':''});
                if not fieldValue.has_key('FieldIsRequired'): fieldValue.update({'FieldIsRequired':''});
                
                fieldName = fieldValue.get("FieldName");
                fieldType = fieldValue.get("FieldType");
                fieldIsRequired = fieldValue.get("FieldIsRequired");
                fieldOrder = fieldValue.get("FieldOrder");
                fieldQcName = fieldValue.get("FieldQcName");
                if fieldName == "":
                    continue;
                self.makeFieldItem(
                                   fieldGroup=fieldTreeGroupName,\
                                   fieldName=fieldName,\
                                   fieldQcName=fieldQcName,\
                                   fieldUserName=fieldUserName,\
                                   fieldType=fieldType,\
                                   fieldIsRequired=fieldIsRequired,\
                                   fieldOrder=fieldOrder
                                   );
        


if __name__ == "__main__":
    import os
    print os.getcwd()
    
    saveFile = SaveFile('testxml.xml');
    
    saveFile.makesetting("Setting_1")
    saveFile.tabHiddenColumn({'Title': 13});
    saveFile.tabHiddenRow({'Title': (13, 15)});
    
    saveFile.makesetting("Setting_2")
    saveFile.tabHiddenColumn({'Customer requestdate (PO)': (33, True), 'Lead Fulfillment': (41, False)});
    saveFile.tabHiddenRow({'Cos': (33, 35, True)});

    
    print saveFile;
    del saveFile
    
    openFile = OpenFile('testxml.xml')
    openFile.getparent("Setting_1")
    print openFile.tabHiddenColumn()
    print openFile.tabHiddenRow();
    
    openFile.getparent("Setting_2")
    print openFile.tabHiddenColumn()
    print openFile.tabHiddenRow();
    print openFile;

    print openFile.readParents();
#    
#    saveFile = SaveFile('testxml.xml');
#    
#    saveFile.makesetting("Setting_1")
#    saveFile.tabHiddenColumn({u'Customer requestdate (PO)': 33, u'Lead Fulfillment': 41});
#    saveFile.tabHiddenRow({'Dupa': (12, 33)});
#    del saveFile
##    
##    saveFile.makesetting("Setting_2")
##    saveFile.tabHiddenColumn('tab column2');
##    saveFile.tabHiddenRow('tab row2');
#
#    saveFile = SaveFile("testxml.xml");
#    saveFile.deletesetting("Setting_1");
    

#    print saveFile;
#    del saveFile
