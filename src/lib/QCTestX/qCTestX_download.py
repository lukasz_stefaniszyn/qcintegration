'''
Created on 27-09-2010

@author: wro50026
'''
from PyQt4 import QtGui


#import lib.QCgraph.qCgraphSpecificTest as qCTestX_download ##This will be the same gui as from QcGraph_Specific

from treeView.treeTestXModel import TreeView as QcTreeViewDownload 
from treeView.treeTestXModel import TreeModel as QcTreeModelDownload
from treeView.treeTestXModel import TreeItem as QcTreeItemDownload

from guiLib.ui_QCgraph_tabTestX_fields import Ui_Form as Ui_TestXDownloadField
 

from lib.logfile import Logger
log = Logger(loggername="lib.QCTestX.qCTestX_TestLab_download", resetefilelog=False).log;





GROUPNAMETRANSLATE = {'TestLab': {'TestSet': 'CYCLE', \
                                  'TestCase': 'TESTCYCL', \
                                  'Step': 'STEP'
                                  },
                     'TestPlan': {'TestSet': 'CYCLE', \
                                  'TestCase': 'TEST', 
                                  'Step': 'DESSTEPS'
                                 }
                     };


class GuiQcTestXDownloadField(QtGui.QDialog, Ui_TestXDownloadField):
    '''This class is used to initialize, control
     TreeViews and buttons which are on the whole tab
    '''
    def __init__(self, parent):
        ##Add Widget responsible for tab_CrossTest
        ##This initialize QcGraph tab Cross Test  
        super(GuiQcTestXDownloadField, self).__init__(parent)
        self.setupUi(self);
        
class MyTreeViewQcTestXDownloadQCField(QcTreeViewDownload, QcTreeModelDownload, QcTreeItemDownload):

    def __init__(self, parent):
        QcTreeViewDownload.__init__(self, parent);
        
        ##Set empty model
        data = ([('TestSet', ""),
                 ('TestCase', ""),
                 ('Step',"")], 
                []);
        self.model = QcTreeModelDownload(data)
        self.treeView.setModel(self.model);
        
        self.mainQcItemsNames = {}
        for item in self.model.rootItem.childItems:
            self.mainQcItemsNames[item.itemData[0]]=(item, 
                                self.model.index(item.row(), 0, self.treeView.rootIndex()))

    def __updateModel(self, fieldTreeGroupName, fieldQCGroupName, mapping_fields):
        '''
        Update TreeView of QualityCenter field group but only dedidcated field name, e.g. 'TestSet'
        
        *Input*
        @param fieldTreeGroupName: str('Test Set') this is TreeView main field group name, already created, e.g 'Test Set', 'Test Case' or 'Step'
        @param fieldQCGroupName: str('CYCLE') this is QualityCenter main field group name, like 'CYCLE', 'TESTCYCLES', 'STEP', etc. 
        @param mapping_fields: dict(), this is dictionary of all possible field group, field name and it's values
                            {
                            'STEP':{'Step Name': {'FieldQcName':'ST_NAME', 
                                                  'FieldType':'char', 
                                                  'FieldIsRequired':'True'
                                                  }
                                    'Step Description': {'FieldQcName':'ST_DESC', 
                                                          'FieldType':'char', 
                                                          'FieldIsRequired':'True'
                                                          }
                                    },
                            'TEST':{'Test Case Name': {'FieldQcName':'TS_NAME', 
                                                          'FieldType':'char', 
                                                          'FieldIsRequired':'True'
                                                          }
                                    }
                             } 
        
        *Output*
        None
        '''
        
        parentFieldItem, fieldindex = self.mainQcItemsNames.get(fieldTreeGroupName)
        parentFieldItem.childItems = []; ##remove all previous fields from the list
        childCounting = len(mapping_fields.get(fieldQCGroupName))-1;
        ##start adding item into Model, under correct Parents Row
        self.model.beginInsertRows(fieldindex, 0, childCounting);#
        for fieldName, fieldValue in mapping_fields.get(fieldQCGroupName).iteritems():
            ##create TreeItem of the given data
            dataUserField = (fieldName, fieldValue)
            item = QcTreeItemDownload(dataUserField, parentFieldItem);
            ##add item to the Data tree
            parentFieldItem.appendChild(item);
        ##finish adding item into Model
        self.model.endInsertRows();
        
    def updateModelTestLab(self, mapping_fields):
        '''
        Update TreeView of Quality Center field group from TestLab
        The full TreeView update is executed in the private function __updateModel()
        
        *Input*        
        @param mapping_fields: this is dictionary of all possible field group, field name and it's values
                             {
                            'STEP':{'Step Name': {'FieldQcName':'ST_NAME', 
                                                  'FieldType':'char', 
                                                  'FieldIsRequired':'True'
                                                  }
                                    'Step Description': {'FieldQcName':'ST_DESC', 
                                                          'FieldType':'char', 
                                                          'FieldIsRequired':'True'
                                                          }
                                    },
                            'TEST':{'Test Case Name': {'FieldQcName':'TS_NAME', 
                                                          'FieldType':'char', 
                                                          'FieldIsRequired':'True'
                                                          }
                                    }
                             } 
        
        *Output*
        None
        '''
        
        for fieldTreeGroupName, fieldQCGroupName in GROUPNAMETRANSLATE.get("TestLab").items():
            self.__updateModel(fieldTreeGroupName, fieldQCGroupName, mapping_fields)
        
        
#        ##TestSet
#        fieldTreeGroupName, fieldQCGroupName = 'TestSet', 'CYCLE'
#        self.__updateModel(fieldTreeGroupName, fieldQCGroupName, mapping_fields)
#        
#        ##TestCase
#        fieldTreeGroupName, fieldQCGroupName = 'TestCase', 'TESTCYCL'
#        self.__updateModel(fieldTreeGroupName, fieldQCGroupName, mapping_fields)
#        
#        ##Step
#        fieldTreeGroupName, fieldQCGroupName = 'Step', 'STEP'
#        self.__updateModel(fieldTreeGroupName, fieldQCGroupName, mapping_fields)


    def updateModelTestPlan(self, mapping_fields):
        '''
        Update TreeView of Quality Center field group from TestPlan
        The full TreeView update is executed in the private function __updateModel()
        
        *Input*        
        @param mapping_fields: this is dictionary of all possible field group, field name and it's values
                             {
                            'STEP':{'Step Name': {'FieldQcName':'ST_NAME', 
                                                  'FieldType':'char', 
                                                  'FieldIsRequired':'True'
                                                  }
                                    'Step Description': {'FieldQcName':'ST_DESC', 
                                                          'FieldType':'char', 
                                                          'FieldIsRequired':'True'
                                                          }
                                    },
                            'TEST':{'Test Case Name': {'FieldQcName':'TS_NAME', 
                                                          'FieldType':'char', 
                                                          'FieldIsRequired':'True'
                                                          }
                                    }
                             } 
        
        *Output*
        None
        '''
        
        for fieldTreeGroupName, fieldQCGroupName in GROUPNAMETRANSLATE.get("TestPlan").items():
            self.__updateModel(fieldTreeGroupName, fieldQCGroupName, mapping_fields)
        
        
#        ##TestSet
#        fieldTreeGroupName, fieldQCGroupName = 'TestSet', 'CYCLE'
#        self.__updateModel(fieldTreeGroupName, fieldQCGroupName, mapping_fields)
#        
#        ##TestCase
#        fieldTreeGroupName, fieldQCGroupName = 'TestCase', 'TEST'
#        self.__updateModel(fieldTreeGroupName, fieldQCGroupName, mapping_fields)
#        
#        ##Step
#        fieldTreeGroupName, fieldQCGroupName = 'Step', 'DESSTEPS'
#        self.__updateModel(fieldTreeGroupName, fieldQCGroupName, mapping_fields)


    def get_mappingFields(self):
        '''
        This will return mapping fields of TreeView_QCFields
        *Input*
        None
        
        *Output*
        mapping_fields = dict(), 
                         {
                            "Step":{'Step Name': {'FieldQcName':'ST_NAME', 
                                                  'FieldType':'char', 
                                                  'FieldIsRequired':'True'
                                                  }
                                    'Step Description': {'FieldQcName':'ST_DESC', 
                                                          'FieldType':'char', 
                                                          'FieldIsRequired':'True'
                                                          }
                                    },
                            "TestSet":{'Test Case Name': {'FieldQcName':'TS_NAME', 
                                                          'FieldType':'char', 
                                                          'FieldIsRequired':'True'
                                                          }
                                    }
                             } 
        '''
        mapping_fields = {};
        for fieldGroup, itemInfo in self.mainQcItemsNames.items():
            mapping_fields.update({fieldGroup:{}});#            
#             {'Step': (<lib.QCTestX.treeView.treeTestXModel.TreeItem instance at 0x01BB3198>,
#                      <PyQt4.QtCore.QModelIndex at 0x1bab5a8>),
#             'TestCase': (<lib.QCTestX.treeView.treeTestXModel.TreeItem instance at 0x01BB3148>,
#                           <PyQt4.QtCore.QModelIndex at 0x1bab570>),
#             'TestSet': (<lib.QCTestX.treeView.treeTestXModel.TreeItem instance at 0x01BB30F8>,
#                          <PyQt4.QtCore.QModelIndex at 0x1bab538>)
#             }
            parentUserIndex = itemInfo[1];
            childCounting = parentUserIndex.internalPointer().childCount();
            for i in range(childCounting):
                ##dataUserField = ('fieldName', ('fieldName', 'char', 'True'))
                fieldName, fieldValues = itemInfo[0].child(i).itemData;
#                fieldQcName = fieldValues.get("FieldQcName");
#                fieldType = fieldValues.get("FieldType");
#                fieldIsRequired = fieldValues.get("FieldIsRequired");
                fieldValues.update({'FieldOrder':str(i)});
                
                if mapping_fields[fieldGroup].has_key(fieldName): ##it might be possible that in the TreeView are two, or more items with the same naming 
                    fieldName = fieldName+"_"+str(i); ##to distinguish those doubled naming add some independence numbering
                    log.double(("In the TreeView are the same named items, creating then fieldName:",fieldName));
                mapping_fields[fieldGroup].update({fieldName: fieldValues});
        return mapping_fields;

            
    def addItem(self, itemUserField):
        '''Add icon. Add one selected item from Specific TestCase into the Chose Test Case
        input = TreeItem(item)
        output = None'''
        if self.mainQcItemsNames.has_key(itemUserField.itemData[0]):
            log.warning('User can not move main catalog, one dedicated items')
            return False
        ##Add new Item under the root tree
        
        ##create item, but do not put this item into model
        dataUserField = itemUserField.itemData;
        parentUserField = itemUserField.parent();
        parentUserFieldName = parentUserField.itemData[0];
        parentFieldItem, fieldindex = self.mainQcItemsNames.get(parentUserFieldName)
        
        ##If given item is already present, do not add
        mappingFields = self.get_mappingFields();
        fieldGroupName= parentUserFieldName;
        fieldName = dataUserField[0];
        if mappingFields[fieldGroupName].has_key(fieldName):
            log.info("Given item is already present. AddItem is aborted")
            return False
        
        
        ##create TreeItem of the given data
        item = QcTreeItemDownload(dataUserField, parentFieldItem);
        childCounting = parentFieldItem.childCount();
        
        #======================================================================
        # Start adding Item to destination
        #======================================================================
        ##start adding item into Model, under correct Parents Row
        self.model.beginInsertRows(fieldindex, childCounting, childCounting);#        
        ##add item to the Data tree
        parentFieldItem.appendChild(item);
        ##finish adding item into Model
        self.model.endInsertRows();
        return True;
            
        
    
    def removeItem(self, index):
        '''Remove icon. Remove one selected item from the Chose Test Case
        input = QModelIndex(index)
        output = None'''
        
        ##get parent as a QModelIndex, of this market index
        parentIndex = index.parent();
        ##check if given parent is root and then get TreeItem of this parent
        if not parentIndex.isValid():
            log.warning('User can not remove main Catalogs');
            return False

        parentItem = parentIndex.internalPointer();
        ##get row position of the Item/Index which will be removed    
        row = index.row();
        ##start removing item/index from the Parents Row
        self.model.beginRemoveRows(parentIndex, row, row);
        ##remove item/index from the Data tree
        parentItem.removeChild(row);
        ##finish removing item/index from Model
        self.model.endRemoveRows();
        return True

    

        
    


    
class MyTreeViewQcTestXDownloadUserField(QcTreeViewDownload, QcTreeModelDownload, QcTreeItemDownload):

    mappingFieldsUserOrg = {} ##this will be used as orginal mappingFieldUser just after opening CSV file 
    
    def __init__(self, parent):
        QcTreeViewDownload.__init__(self, parent);
        
        ##Set empty model
        data = ([('TestSet', ""),
                 ('TestCase', ""),
                 ('Step',"")], 
                []);
        self.model = QcTreeModelDownload(data)
        self.treeView.setModel(self.model);
        
        self.mainUserItemsNames = {}
        for item in self.model.rootItem.childItems:
            self.mainUserItemsNames[item.itemData[0]]=(item, 
                                                       self.model.index(item.row(), 
                                                        0, 
                                                        self.treeView.rootIndex())
                                                       );
    
    def __updateModel(self, fieldTreeGroupName, fieldValues):
        '''
        Update TreeView of User field group but only dedidcated field name, e.g. 'TestSet'
        
        *Input*
        @fieldTreeGroupName: str(), this is field group name, e.g. "TestCase", "TestSet"
        @param mapping_fields: dict(), this is dictionary of all possible field group, field name and it's values
                                {'Test Set Name':{'FieldOrder':'0',
                                                       'FieldQcName':'TS_NAME',
                                                       'FieldType':'char',
                                                       'FieldIsRequired':'True'},
                                  'Test Set Directory': {'FieldOrder':'1',
                                                         'FieldQcName':'TS_DIR',
                                                         'FieldType':'char',
                                                         'FieldIsRequired':'True'},
                                      }
        *Output*
        None
        '''
#        self.model.reset();
        parentFieldItem, fieldindex = self.mainUserItemsNames.get(fieldTreeGroupName)
        parentFieldItem.childItems = []; ##remove all previous fields from the list
        
        ##If You choose not to input any data in the dict(), then all current itemFields will be removed 
        if (fieldValues is "") or (fieldValues is None):
            parentFieldItem.childItems = [];
            return; 
        
        ##First I will have to know the biggest value for fieldOrder
        fieldOrder_max = 0;
        for fieldName, fieldValue in fieldValues.iteritems():
            fieldOrder = int(fieldValue.get('FieldOrder'));
            if fieldOrder > fieldOrder_max: 
                fieldOrder_max = fieldOrder; 
        
        ##Based on the biggest fieldOrder value I will have to create blank, parent child list()
        item = None;
        for i in xrange(fieldOrder_max+1):
            parentFieldItem.appendChild(item);
        
        ##And now put the item to correct parent child list place, based on the OrderField number
        self.model.beginInsertRows(fieldindex, 0, fieldOrder_max);#
        for fieldName, fieldValue in fieldValues.iteritems():
            ##create TreeItem of the given data
            fieldOrder = int(fieldValue.get('FieldOrder'));
            dataUserField = (fieldName, fieldValue)
            item = QcTreeItemDownload(dataUserField, parentFieldItem);
            ##add item to the correctData tree
            parentFieldItem.childItems[fieldOrder] = item;
        ##finish adding item into Model
        self.model.endInsertRows();
    
    
    
    def updateModel(self, mapping_fields):
        '''
        Update TreeView of Quality Center field group 
        The full TreeView update is executed in the private function __updateModel()
        
        *Input*
        @param mapping_fields: dict(), this is dictionary of all possible field group, field name and it's values
                                {"TestSet": {'Test Set Name':{'FieldOrder':'0',
                                                       'FieldQcName':'TS_NAME',
                                                       'FieldType':'char',
                                                       'FieldIsRequired':'True'},
                                      'Test Set Directory': {'FieldOrder':'1',
                                                             'FieldQcName':'TS_DIR',
                                                             'FieldType':'char',
                                                             'FieldIsRequired':'True'},
                                      'Test Set Description': {'FieldOrder':'2',
                                                               'FieldQcName':'TS_DESC',
                                                               'FieldType':'char',
                                                               'FieldIsRequired':'True'}
                                      },
                               "TestCase": {'Test Case Name': {'FieldOrder':'0',
                                                          'FieldQcName':'TC_NAME',
                                                          'FieldType':'char',
                                                          'FieldIsRequired':'True'},
                                            'Test Case Directory': {'FieldOrder':'1',
                                                              'FieldQcName':'TC_DIR',
                                                              'FieldType':'char',
                                                              'FieldIsRequired':'True'},
                                            'Test Case Status': {'FieldOrder':'2',
                                                              'FieldQcName':'TC_STATUS',
                                                              'FieldType':'char',
                                                              'FieldIsRequired':'True'},
                                        },
                               "Step": {'Step Name': {'FieldOrder':'0',
                                                  'FieldQcName':'ST_NAME',
                                                  'FieldType':'char',
                                                  'FieldIsRequired':'True'},
                                        'Step Description': {'FieldOrder':'1',
                                                  'FieldQcName':'ST_DESC',
                                                  'FieldType':'char',
                                                  'FieldIsRequired':'True'}
                                    }
                                
        
        *Output*
        None
        '''
#        self.model.reset()
        for fieldTreeGroupName, fieldValues in mapping_fields.items():
            self.__updateModel(fieldTreeGroupName, fieldValues);
        
        
    def get_mappingFields(self):
        '''
        This will return mapping fields of TreeView_UserFields
        *Input*
        None
        
        *Output*
        mapping_fields = dict(), 
                         {"TestSet": {'Test Set Name':{'FieldOrder':'0',
                                                       'FieldQcName':'TS_NAME',
                                                       'FieldType':'char',
                                                       'FieldIsRequired':'True'},
                                      'Test Set Directory': {'FieldOrder':'1',
                                                             'FieldQcName':'TS_DIR',
                                                             'FieldType':'char',
                                                             'FieldIsRequired':'True'},
                                      'Test Set Description': {'FieldOrder':'2',
                                                               'FieldQcName':'TS_DESC',
                                                               'FieldType':'char',
                                                               'FieldIsRequired':'True'}
                                      },
                               "TestCase": {'Test Case Name': {'FieldOrder':'0',
                                                          'FieldQcName':'TC_NAME',
                                                          'FieldType':'char',
                                                          'FieldIsRequired':'True'},
                                            'Test Case Directory': {'FieldOrder':'1',
                                                              'FieldQcName':'TC_DIR',
                                                              'FieldType':'char',
                                                              'FieldIsRequired':'True'},
                                            'Test Case Status': {'FieldOrder':'2',
                                                              'FieldQcName':'TC_STATUS',
                                                              'FieldType':'char',
                                                              'FieldIsRequired':'True'},
                                        },
                               "Step": {'Step Name': {'FieldOrder':'0',
                                                  'FieldQcName':'ST_NAME',
                                                  'FieldType':'char',
                                                  'FieldIsRequired':'True'},
                                        'Step Description': {'FieldOrder':'1',
                                                  'FieldQcName':'ST_DESC',
                                                  'FieldType':'char',
                                                  'FieldIsRequired':'True'}
                                    }
                         
        '''
        mapping_fields = {};
        for fieldGroup, itemInfo in self.mainUserItemsNames.items():
            mapping_fields.update({fieldGroup:{}});#            
#             {'Step': (<lib.QCTestX.treeView.treeTestXModel.TreeItem instance at 0x01BB3198>,
#                      <PyQt4.QtCore.QModelIndex at 0x1bab5a8>),
#             'TestCase': (<lib.QCTestX.treeView.treeTestXModel.TreeItem instance at 0x01BB3148>,
#                           <PyQt4.QtCore.QModelIndex at 0x1bab570>),
#             'TestSet': (<lib.QCTestX.treeView.treeTestXModel.TreeItem instance at 0x01BB30F8>,
#                          <PyQt4.QtCore.QModelIndex at 0x1bab538>)
#             }
            parentUserIndex = itemInfo[1];
            childCounting = parentUserIndex.internalPointer().childCount();
            for i in range(childCounting):
                ##dataUserField = ('fieldName', ('fieldName', 'char', 'True'))
                if itemInfo[0].child(i) is None:
                    continue;
                fieldName, fieldValues = itemInfo[0].child(i).itemData;
#                fieldQcName = fieldValues.get("FieldQcName");
#                fieldType = fieldValues.get("FieldType");
#                fieldIsRequired = fieldValues.get("FieldIsRequired");
                fieldValues.update({'FieldOrder':str(i)});
                if mapping_fields[fieldGroup].has_key(fieldName): ##it might be possible that in the TreeView are two, or more items with the same naming 
                    fieldName = fieldName+"_"+str(i); ##to distinguish those doubled naming add some independence numbering
                    log.double(("In the TreeView are the same named items, creating then fieldName:",fieldName)); 
                mapping_fields[fieldGroup].update({fieldName: fieldValues});
        return mapping_fields;
        
        
    def addItem(self, itemQcField):
        '''Add icon. Add one selected item from Specific TestCase into the Chose Test Case
        input = TreeItem(item)
        output = None'''
        if self.mainUserItemsNames.has_key(itemQcField.itemData[0]):
            log.warning('User can not move main catalog, one dedicated items')
            return False
        
        ##Add new Item under the root tree
        dataQcField = itemQcField.itemData;
        parentQcField = itemQcField.parent();
        parentQcFieldName = parentQcField.itemData[0];
        parentFieldItem, userFieldindex = self.mainUserItemsNames.get(parentQcFieldName)
        
        ##If given item is already present, do not add
        mappingFields = self.get_mappingFields();
        fieldGroupName= parentQcFieldName;
        fieldName = dataQcField[0];
        if mappingFields[fieldGroupName].has_key(fieldName):
            log.info("Given item is already present. AddItem is aborted")
            return False
        
        ##create TreeItem of the given data
        item = QcTreeItemDownload(dataQcField, parentFieldItem);
        childCounting = parentFieldItem.childCount();
        
        #======================================================================
        # Start adding Item to destination
        #======================================================================
        ##start adding item into Model, under correct Parents Row
        self.model.beginInsertRows(userFieldindex, childCounting, childCounting);#        
        ##add item to the Data tree
        parentFieldItem.appendChild(item);
        ##finish adding item into Model
        self.model.endInsertRows();
        return True
        
    
    def removeItem(self, index):
        '''Remove icon. Remove one selected item from the Chose Test Case
        input = QModelIndex(index)
        output = None'''
        
        ##get parent as a QModelIndex, of this market index
        parentIndex = index.parent();
        ##check if given parent is root and then get TreeItem of this parent
        if not parentIndex.isValid():
            log.warning('User can not remove main Catalogs');
            return False;
        parentItem = parentIndex.internalPointer();
        ##get row position of the Item/Index which will be removed    
        row = index.row();
        ##start removing item/index from the Parents Row
        self.model.beginRemoveRows(parentIndex, row, row);
        ##remove item/index from the Data tree
        parentItem.removeChild(row);
        ##finish removing item/index from Model
        self.model.endRemoveRows();
        return True

    def clearItem(self):
        '''Trash icon. Clear all items in the Chose Test Case
        input = None
        output = None;'''
        ##get parent as a QModelIndex, of this market index
        
        
        countChilds = self.model.rootItem.childCount();
        
        for i in range(0, countChilds): 
            parentIndex = self.model.index(i,0, self.treeView.rootIndex());
            parentItem = self.model.rootItem.child(i);
            
            countChilds = parentItem.childCount();
            ##start removing items from the root Parent
            self.model.beginRemoveRows(parentIndex, 0, countChilds);
            ##remove all items from the Data tree
            parentItem.removeAllChilds();
            ##finish removing items from the Model
            self.model.endRemoveRows();

    def moveItem(self, index, direction):
        '''
        Move position of the given index
        @param index: QModelIndex of moved item
        @param direction: move item Up=0 or Down=1 
        '''
        
        item = index.internalPointer()
        ##get parent as a QModelIndex, of this market index
        parentIndex = index.parent();
        parentItem = parentIndex.internalPointer();
        ##check if given parent is root and then get TreeItem of this parent
        if not parentIndex.isValid():
            log.warning('User can not move up/down main Catalogs');
            return False
        ##get row position of the Item/Index which will be removed    
        row = index.row();
        ##start removing item/index from the Parents Row
        
        if direction==0: ##move Up 
            if row == 0: ##if user will try to move the first item in the parent, then no movement will be done
                return True
            self.model.beginMoveRows(parentIndex, row, row, parentIndex, row-1);
            
            parentItem.removeChild(row);
            parentItem.appendChildInRow(row-1, item)
            
            
        elif direction==1: ##move Down:
            if row+1 == parentItem.childCount(): ##if user will try to move the last item in the parent, then no movement will be done
                return True 
            self.model.beginMoveRows(parentIndex, row, row, parentIndex, row+2);
            parentItem.removeChild(row);
            parentItem.appendChildInRow(row+1, item)
        ##finish removing item/index from Model
        self.model.endMoveRows();
        return True
    

    


    
    
