'''
Created on 2010-06-07

@author: Lukasz Stefaniszyn
'''
#!/usr/bin/env python


'''
This is file used for creation TreeView from  QualityCenter
'''

import os
from PyQt4 import QtCore, QtGui

import lib.resource_rc

from lib.logfile import Logger
log = Logger(loggername="lib.QCgraph.treeQcModelTestPlan", resetefilelog=False).log;



class TreeItem:
    '''Here are all operation on one item which is given to  -> TreeModel  and then as model to -> TreeView'''
    
    def __init__(self, data, parent=None, iconDirectory=False):
        self.parentItem = parent
        self.itemData = data
        self.childItems = []
        self.columnText = 0  ##this information about place in data where is text, which will be displayed in TreeView;
        self.iconDirectoryItem = iconDirectory; ##is item Directory or TestSet/TestCase

    def __str__(self):
        return "TreeItem, item name:%s"%str(self.itemData);

    def appendChild(self, item):
        '''Append new child into current child.
        input = TreeItem(newChild, parent)'''
        self.childItems.append(item)
        
    def removeChild(self, row):
        '''Remove child form current childs
        input = int(row)
        '''
        del self.childItems[row];
        
    def removeAllChilds(self):
        '''Remove all childs form current childs'''
        self.childItems = [];


    def child(self, row):
        '''Return child instance,by row number, from upper child
        input= int(row)''' 
        return self.childItems[row]

    def childCount(self):
        '''Give number of childrens as int()'''
        return len(self.childItems)

    def columnCount(self):
        '''Return number of columns  int()'''
        return len(self.itemData)

    def data(self, column):
        return self.itemData[column]

    def parent(self):
        '''Return childs parent'''
        return self.parentItem

    def row(self):
        '''Return row number as int()'''
        if self.parentItem:
            return self.parentItem.childItems.index(self)
        return 0
    
    


class TreeModel(QtCore.QAbstractItemModel):
    '''This will create model of data'''
    
    def __init__(self, data, parent=None):
        QtCore.QAbstractItemModel.__init__(self, parent)
        
        rootData = []
        rootData.append(QtCore.QVariant("Subject"))
        self.rootItem = TreeItem(rootData);
        ##tutaj beda tworzone i dodawane liniji do tablicy
#        data = QCstart.getDirTestCase();
        updatemodeltree(self.rootItem, data);

    
    def columnCount(self, parent):
        if parent.isValid():
            return parent.internalPointer().columnCount();
        else:
            return self.rootItem.columnCount();

    def data(self, index, role):
        '''
        This is a heart of TreeModel. 
        By information from data() item from TreeItem will be modeled/visible.
        Decision of what to model/show are in Role attribute.  
        '''
        
        if not index.isValid():
            return QtCore.QVariant()

        if role == QtCore.Qt.DisplayRole:
            ##this will show text of item from TreeItem
            item = index.internalPointer();
            return QtCore.QVariant(item.data(item.columnText));
        elif role == QtCore.Qt.DecorationRole:
            ##this will show icon item from TreeItem
            item = index.internalPointer();
            if item.iconDirectoryItem:
                ##This will sets what icon will be used to TreeView for Directory and Test. 
                pixmap = QtGui.QPixmap(":/icon/icon/gtk-directory.png");
            else:
                ##This will sets what icon will be used to TreeView for Directory and Test.
                pixmap = QtGui.QPixmap(":/icon/icon/gtk-file.png");
            if pixmap.isNull():
                return QtCore.QVariant();
            return QtCore.QVariant(pixmap);
        else:
            return QtCore.QVariant()
        
    def flags(self, index):
        '''This function is triggered every time when user points on some item from tree view'''
        if not index.isValid():
            return QtCore.Qt.ItemIsEnabled

        return QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable

    def headerData(self, section, orientation, role):
        if orientation == QtCore.Qt.Horizontal and role == QtCore.Qt.DisplayRole:
            return self.rootItem.data(section)

        return QtCore.QVariant()

    def index(self, row, column, parent):
        '''To prawdopobodbie pozwala umiescic child w odpowiednim parent
        poprzez self.createIndex(row, column, childItem)'''
        
        if row < 0 or column < 0 or row >= self.rowCount(parent) or column >= self.columnCount(parent):
            #print "QModelIndex:", QtCore.QModelIndex();
            return QtCore.QModelIndex()
        if not parent.isValid():
            parentItem = self.rootItem
        else:
            parentItem = parent.internalPointer()
        childItem = parentItem.child(row)
        if childItem:
            #print "createIndex(row, column, childItem):",(row, column, childItem) 
            return self.createIndex(row, column, childItem)
        else:
            return QtCore.QModelIndex()

    def parent(self, index):
        '''To tworzy parent w odpowiednim miejscu 
        self.createIndex(parentItem.row(), 0, parentItem)'''
        
        if not index.isValid():
            return QtCore.QModelIndex()
        childItem = index.internalPointer()
        parentItem = childItem.parent()
        if parentItem == self.rootItem:
            return QtCore.QModelIndex()
        return self.createIndex(parentItem.row(), 0, parentItem)

    def rowCount(self, parent):
        if parent.column() > 0:
            return 0
        if not parent.isValid():
            parentItem = self.rootItem
        else:
            ##To zwraca polozenie parent
            parentItem = parent.internalPointer()
        return parentItem.childCount()

class TreeView(QtGui.QTreeView):
    '''Give all operation made on user tree view. Like action expand/collapse, double clicked on item view
    This inherit also QAbstractItemModel
    '''
    def __init__(self, parent=None, qc=None):
        self.treeView = parent;
        QtGui.QTreeView.__init__(self)
        self.qc = qc
#        self.model = _model;
    
        ##Connect Signals Expanding/Collapsing/DoubleClicked tree to slots
        self.connect(self.treeView, QtCore.SIGNAL("expanded (const QModelIndex &)"),
             self.expanded);
        self.connect(self.treeView, QtCore.SIGNAL("collapsed (const QModelIndex &)"),
             self.collapsed);
        self.connect(self.treeView, QtCore.SIGNAL("doubleClicked (const QModelIndex &)"),
             self.doubleClicked); 

    def doubleClicked(self, index):
        '''Connect to the QC to get all folders/test instances. 
        Put results on the user tree view
        
        Signal trigged by user, double click on one of the items from tree view
        input =  QModelIndex(index)
        output = None  
        ''' 
        
        log.debug("doubleClicked");     
        
        item = index.internalPointer();
        log.debug(("Index data:", item.itemData)); 
        childrenCount = item.childCount();
        
#        print "Project Connected?:",self.qc.ProjectConnected;
        
        if childrenCount==0:            
            ##Before add childs in the dataTree, there is need to add childs in modelTree
            self.treeView.model().beginInsertRows(index, childrenCount, childrenCount+2);
#            data = ([
#                     ('G test', "G-qcInstance"), 
#                     ('H test', "H-qcInstance")
#                     ],
#                     []
#                     );
            if item.iconDirectoryItem:
                ##If item is DirectoryItem then make update TreeView
                data = getDirTestCase(self.qc, item.itemData[1])
                updatemodeltree(index, data);
            else:
                ##If item is not DirectoryItem , then do nothing
                pass;
            ##When you add childs in dataTree, there is need to update those changes in modelTree
            self.treeView.model().endInsertRows();
    

        
    def expanded(self, index):
        '''Signal trigged by user, expand on one of the items from tree view
        input =  QModelIndex(index)
        output = None
        ''' 
        log.debug("expanded");
        
        
    def collapsed(self, index):
        '''Signal trigged by user, collapse on one of the items from tree view
        input =  QModelIndex(index)
        output = None
        '''
        
        log.debug("collapsed");
    
def updatemodeltree(index, data):
    '''
    This will update tree view structure of Directories, TestCases/TestSets under the given Index
    input = (index=TreeItem(),  
            data= ( [ 
                    ( str(DirectoryName), qcInstace(Directory) )
                    ], 
                    [
                    (str(TestName), qcInstance(Test) )
                    ] );
    output = None
    '''
    
    
    
    try: 
        ##parent position in model
        parent = index.internalPointer();
    except AttributeError:
        ## rootItem does not have internalPointer() method. By it self is parent 
        parent = index; ##rootItem
    
    
    ## new data list, from Quality Center

    catalogs = data[0];
    testsets = data[1];
#    print "catalogs",catalogs; 
    ##create item, but do not put this item into model
    if catalogs is not None:
        for catalog in catalogs:
#            print "catalog", catalog;
            item = TreeItem(catalog, parent, iconDirectory=True);
            ##put item into model
            parent.appendChild(item);
    if testsets is not None:
        for testset in testsets:
            item = TreeItem(testset, parent, iconDirectory=False);            
            ##put item into model
            parent.appendChild(item);
 


def getDirTestCase(qc, ItemQcInstance=None):
    
    if ItemQcInstance is None:
        ItemQcInstance = getRootTestPlan(qc);
    return getListDirTestCase(ItemQcInstance);


def getRootTestPlan(qc):
    tm = qc.TreeManager;
    root = tm.TreeRoot("Subject");
    return root;



def getListDirTestCase(dirInstance):
    
    if dirInstance is None:
        log.debug(("Given catalog is not present in QualityCenter:", unicode(dir)))
        return None;
    
    dirInstance.Refresh();
    dirListInstance = dirInstance.NewList();
    #print "dirListInstance:", dirListInstance;
    if len(dirListInstance) > 0:
        dirList = [(unicode(dir.Name),dir) for dir in dirListInstance]
        dirList.sort();
    else:
        dirList = [];
    
    tcListInstance = dirInstance.TestFactory.NewList("");
    if len(tcListInstance) > 0:
        tcList = [(unicode(tc.Name),tc) for tc in tcListInstance];
        tcList.sort();
    else:
        tcList = [];
    
    return dirList, tcList;

