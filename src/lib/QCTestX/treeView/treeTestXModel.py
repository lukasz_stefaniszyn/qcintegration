'''
Created on 2010-06-07

@author: Lukasz Stefaniszyn
'''
#!/usr/bin/env python

'''
This is file used for creation Filed TreeView from  QualityCenter and User selection
'''



import os
import pywintypes
from PyQt4 import QtCore, QtGui

from lib.QCTestX.guiLib import resource_rc

from lib.logfile import Logger
log = Logger(loggername="lib.QCTestX.treeView.treeTestXModel", resetefilelog=False).log;

class TreeItem():
    '''Here are all operation on one item which is given to  -> TreeModel  and then as model to -> TreeView'''
    
    def __init__(self, data, parent=None, iconDirectory=False):
        self.parentItem = parent
        self.itemData = data
        self.childItems = []
        
        self.columnText = []; 
        for columnNumber in range(0, len(data), 2):##every 2nd item in the given data can be column text
            self.columnText.append(columnNumber); ##this information about place in data where is text, which will be displayed in TreeView;
         
        self.iconDirectoryItem = iconDirectory; ##is item Directory or TestSet/TestCase
        
        

    def __str__(self):
        return "TreeItem, item name:%s"%str(self.itemData);

    def appendChild(self, item):
        '''Append new child into current child.
        input = TreeItem(newChild, parent)'''
        self.childItems.append(item)

    def appendChildInRow(self, row, item):
        '''Append new child into given row place of child.
        input = int(row), TreeItem(newChild, parent)'''
        self.childItems.insert(row, item)
    
    def removeChild(self, row):
        '''Remove child form current childs
        input = int(row)
        '''
        del self.childItems[row];
        
    def removeAllChilds(self):
        '''Remove all childs form current childs'''
        self.childItems = [];

    def child(self, row):
        '''Return child instance,by row number, from upper child
        input= int(row)''' 
        return self.childItems[row]

    def childCount(self):
        '''Give number of childrens as int()'''
        return len(self.childItems)

    def columnCount(self):
        '''Return number of columns  int()'''
        return len(self.itemData)

    def data(self, column):
        return self.itemData[column]

    def parent(self):
        '''Return childs parent'''
        return self.parentItem

    def row(self):
        '''Return row number as int()'''
        if self.parentItem:
            return self.parentItem.childItems.index(self)
        return 0
    
    def setData(self, data):
        if len(data) < 0 or len(data) > len(self.itemData):
            return False
        self.itemData= data
        return True
    
    
    


class TreeModel(QtCore.QAbstractItemModel):
    '''This will create model of data'''
    
    def __init__(self, data, moreColumns=False,parent=None):
        QtCore.QAbstractItemModel.__init__(self, parent)
        
        rootData = []
        rootData.append(QtCore.QVariant("Column group"))
        if moreColumns: rootData.append(QtCore.QVariant("User Field"));
        self.rootItem = TreeItem(rootData);
        ##tutaj beda tworzone i dodawane liniji do tablicy
#        data = QCstart.getDirTestCase();
        updatemodeltree(self.rootItem, data);
        
        self.dataChanged.connect(self.newData);

    
    def newData(self, indexStart, indexEnd):
        '''
        This is signal emited whenever data in Model will be changed
        :param indexStart: QModelIndex & topLeft
        :param indexEnd:QModelIndex & bottomRight
        '''
        log.debug("Data Changed signal");
        self.submit();
        
        
    
    def columnCount(self, parent):
        if parent.isValid():
            return parent.internalPointer().columnCount();
        else:
            return self.rootItem.columnCount();

    def data(self, index, role):
        '''
        This is a heart of TreeModel. 
        By information from data() item from TreeItem will be modeled/visible.
        Decision of what to model/show are in Role attribute.  
        '''
        
        if not index.isValid():
            return QtCore.QVariant()

        if role == QtCore.Qt.DisplayRole:##this will show text of item from TreeItem
            item = index.internalPointer();
            column = index.column();
            if column == 0:
                return QtCore.QVariant(item.data(item.columnText[column]));
            elif column == 1:
                try:item.data((item.columnText[column]));##If given item has second column data, then Show this second data 
                except: return QtCore.QVariant(item.data((item.columnText[column-1])));##but if this does not have it, then show the previous column data 
                else: return QtCore.QVariant(item.data((item.columnText[column])));##Show this second data
            else:
                return;
        elif role == QtCore.Qt.DecorationRole:
            ##this will show icon item from TreeItem
            item = index.internalPointer();
            if item.iconDirectoryItem:
                ##This will sets what icon will be used to TreeView for Directory and Test. 
                pixmap = QtGui.QPixmap(":/icon/icon/gtk-directory.png");
            else:
                ##This will sets what icon will be used to TreeView for Directory and Test.
                pixmap = QtGui.QPixmap(":/icon/icon/gtk-file.png");
            if pixmap.isNull():
                return QtCore.QVariant();
            return QtCore.QVariant(pixmap);
        else:
            return QtCore.QVariant()
        
    def flags(self, index):
        '''This function is triggered every time when user points on some item from tree view'''
        if not index.isValid():
            return QtCore.Qt.ItemIsEnabled

        return QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable

    def headerData(self, section, orientation, role):
        if orientation == QtCore.Qt.Horizontal and role == QtCore.Qt.DisplayRole:
            return self.rootItem.data(section)

        return QtCore.QVariant()

    def index(self, row, column, parent):
        '''To prawdopobodbie pozwala umiescic child w odpowiednim parent
        poprzez self.createIndex(row, column, childItem)'''
        
        if row < 0 or column < 0 or row >= self.rowCount(parent) or column >= self.columnCount(parent):
            #print "QModelIndex:", QtCore.QModelIndex();
            return QtCore.QModelIndex()
        if not parent.isValid():
            parentItem = self.rootItem
        else:
            parentItem = parent.internalPointer()
        childItem = parentItem.child(row)
        if childItem:
            #print "createIndex(row, column, childItem):",(row, column, childItem) 
            return self.createIndex(row, column, childItem)
        else:
            return QtCore.QModelIndex()

    def parent(self, index):
        '''To tworzy parent w odpowiednim miejscu 
        self.createIndex(parentItem.row(), 0, parentItem)'''
        
        if not index.isValid():
            return QtCore.QModelIndex()
        childItem = index.internalPointer()
        parentItem = childItem.parent()
        if parentItem == self.rootItem:
            return QtCore.QModelIndex()
        return self.createIndex(parentItem.row(), 0, parentItem)

    def rowCount(self, parent):
        if parent.column() > 0:
            return 0
        if not parent.isValid():
            parentItem = self.rootItem
        else:
            ##To zwraca polozenie parent
            parentItem = parent.internalPointer()
        return parentItem.childCount()

    def walkGenerator(self, parent, topdown = True):
        '''This will print only TestSets/TestCases list in the TreeView '''
        if not parent.parent():
            top = None, None; 
        else:
            top = parent.itemData
        try:
            names = parent.childItems;
            
    #        print "listdir:", names
        except:
            log.debug("ERR: not possible to get Dir List");
            return
    
        dirs, nondirs = [], [];
        if len(names)==0:
#            print "len(0) This is dir with TreeView 0 childs:", top; 
            yield top, [], [];
            return;
       
        for name in names:
            if name.iconDirectoryItem:
#                print "This is dir with TreeView 0 childs:", name;
                dirs.append(name); 
            else:
                nondirs.append(name)
#                print "This is TestSet/TestCase item from TreeView:", name;
                
        if topdown:
    #        print "top, dirs, nondirs", top, dirs, nondirs;
            yield top, [dir.itemData for dir in dirs], [nondir.itemData for nondir in nondirs]
        for name in dirs:
            for x in self.walkGenerator(name):
    #            print "x", x
                yield x

                
    
    def walk(self):
        '''Return all childs from Model''' 
        parent = self.rootItem;
        for top, dirs, nondirs in self.walkGenerator(parent):
#            print "Top:", top,
#            print "Dirs:", dirs;
#            print "NonDirs:", nondirs;
            if nondirs == [] and dirs == []:
#                print "!!!\tKatalog dla QC i wszystkie jego TS/TC:", top;
                pass;
            elif nondirs != []:
#                print "!!!\tPOjedyncze TS/TC:", nondirs
                pass;
            else:
                continue;
            yield top, nondirs;
            
    def setData(self, index, item, value, role=QtCore.Qt.EditRole):
        '''
        Set new data information to the item
        
        *Input*
        :param index:  QModelIndex & index,
        :param item: TreeItem()
        :param value: new value data, like (dataField[0], dataField[1], fieldName, fieldValue)
        :param role: ? do not know yet what value I will set here
        
        *Outpu*
        :param result: True/False  how setData accomplished finished  
        '''

        self.layoutAboutToBeChanged.emit();
        result = item.setData(value)
        self.layoutChanged.emit();
        if result:
            self.dataChanged.emit(index, index)
        return result
    
    def resetModel(self):
        '''
        This will reset model and the view
        '''
        
        self.layoutAboutToBeChanged.emit();
        self.reset();
        self.layoutChanged.emit();
        
    
class TreeView(QtGui.QTreeView):
    '''Give all operation made on user tree view. Like action expand/collapse, double clicked on item view
    This inherit also QAbstractItemModel
    '''
    def __init__(self, parent=None, qc=None):
        self.treeView = parent;
        QtGui.QTreeView.__init__(self)
        
        self.qc = qc
#        self.model = _model;
    
        ##Connect Signals Expanding/Collapsing/DoubleClicked tree to slots
        self.treeView.expanded.connect(self.expanded);
        self.treeView.collapsed.connect(self.collapsed);
        self.treeView.doubleClicked.connect(self.doubleClicked);

    def expanded(self, index):
        '''Signal trigged by user, expand on one of the items from tree view
        input =  QModelIndex(index)
        output = None
        ''' 
        log.debug("expanded");
        
        
    def collapsed(self, index):
        '''Signal trigged by user, collapse on one of the items from tree view
        input =  QModelIndex(index)
        output = None
        '''
        
        log.debug("collapsed");
    
def updatemodeltree(index, data):
    '''
    This will update tree view structure of Directories, TestCases/TestSets under the given Index
    input = (index=TreeItem(),  
            data= ( [ 
                    ( str(DirectoryName), qcInstace(Directory) )
                    ], 
                    [
                    (str(TestName), qcInstance(Test) )
                    ] );
    output = None
    '''
    
    
    
    try: 
        ##parent position in model
        parent = index.internalPointer();
    except AttributeError:
        ## rootItem does not have internalPointer() method. By it self is parent 
        parent = index; ##rootItem
    
    
    ## new data list, from Quality Center

    catalogs = data[0];
    testsets = data[1];
#    print "catalogs",catalogs; 
    ##create item, but do not put this item into model
    if catalogs is not None:
        for catalog in catalogs:
#            print "catalog", catalog;
            item = TreeItem(catalog, parent, iconDirectory=True);
            ##put item into model
            parent.appendChild(item);
    if testsets is not None:
        for testset in testsets:
            item = TreeItem(testset, parent, iconDirectory=False);            
            ##put item into model
            parent.appendChild(item);
 

def getRootTestLab(qc):
    tm = qc.TestSetTreeManager
    root = tm.Root
    return root;



