'''
Created on 17-11-2012

@author: lucst
'''

import os

from lib.logfile import Logger
log = Logger(loggername="globalVariables", resetefilelog=False).log;



class GlobalVariables(object):
    '''
    This is class with global variable settings
    '''
    DIR_RESULT= os.path.join(os.getcwd(), 'Result');
    DIR_CONF= os.path.join(os.getcwd(), 'conf');
#    __dir_result = os.path.join(os.getcwd(), 'Result');
#    __dir_conf = os.path.join(os.getcwd(), 'conf');
    
#    def __init__(self):
#        pass;
#
#    def get_dir_conf(self):
#        print "get_dir_conf:", self.__dir_conf
#        return self.__dir_conf
##    def set_dir_conf(self, value):
##        log.debug(("DIR_CONF directory has been changed to:", repr(value)));
##        self.__dir_conf = value
#
#
#    def get_dir_result(self):
#        return self.__dir_result
##    def set_dir_result(self, value):
##        log.debug(("DIR_RESULT directory has been changed to:", repr(value)));
##        self.__dir_result = value
#
#    DIR_RESULT = property(get_dir_result, None, None, "dir_result's docstring")
#    DIR_CONF = property(get_dir_conf, None, None, "dir_conf's docstring")
    