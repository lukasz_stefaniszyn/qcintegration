'''
Created on 30-07-2010

@author: wro50026
'''

import lib.QualityCenter_ext.lib.qc_connect_disconnect as qc_connect_disconnect
import lib.crypt_pass as crypt_pass 

class QCOperation(object):

    
    def __init__(self, server, userName, password, domainName, projectName): 
        
        self.server = server; 
        self.userName= userName;
        self.password = password;
        self.domainName= domainName;
        self.projectName= projectName;
    
#------------------------------------------------------------------------------ 
    def __get_qcInstance(self):
        return self.qcConnect;
    def __set_qcInstance(self, qcConnection):
        if qcConnection is not None:
            self.qcConnect = qcConnection;
            self.qc = qcConnection.qc;
            self.tm = self.qc.TreeManager;
            self.root = self.tm.TreeRoot("Subject");
        else:
            print "Unable to set qc connection";
    def __del_qcInstance(self):
        print "Disconnecting"
        self.qcDisconnect();
    qcInstance = property(__get_qcInstance,__set_qcInstance,__del_qcInstance,"QC connection instance");
#------------------------------------------------------------------------------ 
    
    def _qcConnect(self):
        
        
        
        qcConnect = qc_connect_disconnect.QC_connect(self.server, 
                                                     self.userName, 
                                                     self.password, 
                                                     self.domainName, 
                                                     self.projectName);
        
        for result in qcConnect.connect():
            print result;
        if result[0]:
            print "Connected OK";
            #print self.qcInstance; 
            self.qcInstance = qcConnect;
        else:
            print "ERR: Not connected"
            self.qcInstance = None;
        
    def qcDisconnect(self):
        if self.qcInstance is None:
            return False;
        
        if self.qcInstance.disconnect_QC():
            print "\nDisconnecting from QC server."\
                    "\n"\
                    "\nThank You for using QC_import_export script.\n"\
                    "\t\t\t\n"\
                    "\t\t\t  Lukasz Stefaniszyn\n"\
                    "\n\t\t\t\t\tPowered by Python 2.5";
            return True;
        else:
            print "Unable to to disconnect";
            return False




    def getFields(self):
        
        fields_1 = ["TESTCYCL", "CYCLE"]  #Test Lab and Run
        fields_2 = ["TEST"]  #Test Plan
        fields = fields_1 + fields_2;

        mapping_QCGraph = {};
        for name in fields:
            fieldList = self.qc.Fields(name);
            for aField in fieldList:
                FieldProp = aField.Property;
                fieldName = aField.Name;
                fieldUserLabel=FieldProp.UserLabel;
                if "TS_" in fieldName:
                    fieldUserLabel = "TS_"+fieldUserLabel;
                elif "TC_" in fieldName:
                    fieldUserLabel = "TC_"+fieldUserLabel;
                elif "CY_" in fieldName:
                    fieldUserLabel = "Cycle_"+fieldUserLabel;
                fieldIsActive=FieldProp.IsActive;
                fieldIsCanFilter=FieldProp.IsCanFilter;
                if fieldIsActive and fieldIsCanFilter: 
                    mapping_QCGraph[str(fieldName)]= str(fieldUserLabel);
                    
        return mapping_QCGraph;

    def make_summary_graph(self, Xaxis, Yaxis, TestSetFactory, ID=-1):
        '''This will create Progress Graph from the excution 
           input = qc_instance(qc), str(Xaxis), str(Yaxis)
           output = qc_graph_instance();
        '''

        ##create graph from X,Y axis
        #print "Building summry graph for x=%s, y=%s"%(Xaxis, Yaxis);
        TSF = TestSetFactory;
        
        try:
            graph= TSF.BuildSummaryGraph(ID, Xaxis, Yaxis, "", 0, TSF.Filter, False, False);
        except:
            return False;
        
        return graph;

    def fieldcombination(self, mapping_QCGraph):
        
        for Xaxis in mapping_QCGraph.iterkeys():
            for Yaxis in mapping_QCGraph.iterkeys():
                yield Xaxis, Yaxis 

    def main(self, mapping_QCGraph):
        GraphsSummary = ({"CY_CYCLE":"TestSet" , "TC_STATUS":"Status"}, {"TC_TESTER_NAME":"ResposibleTester", "TC_STATUS":"Status"});
        ##Generate graph 
        
        
        ##Take TestSetFactory
        tm = self.qc.TestSetTreeManager
        root = tm.Root
        dirList = root.NewList()
        TSF = dirList[0].TestSetFactory
        
        
        for Xaxis, Yaxis in self.fieldcombination(mapping_QCGraph):
            print "x, y:", Xaxis, Yaxis
            try:
                graph = self.make_summary_graph(Xaxis, Yaxis, TSF);
            except BaseException, err:
                print "!!!!Err:", err
                
            pass;
            
#        for graphType in GraphsSummary:
#            x, y = graphType.keys();
#            print "Building summary graph for:",graphType;
#            print "x, y:", x, y
#            graph = self.make_summary_graph(x, y, TSF);


def QC92_Munich():
    
    server = "http://muvmp034.nsn-intra.net/qcbin/"; 
    userName="stefaniszyn";
    pass_ = "e8dbf113946b941a4a34b573363674a3c58520";
    password = crypt_pass.Heslo(pass_)._Heslo__readHeslo();
    domainName="HIT7300";
    projectName="hiT73_R43x";

    domainName="HIT7500";
    projectName="hiT7500_R330_R340";

    qcOperation = QCOperation(server, userName, password, domainName, projectName);
    qcOperation._qcConnect();
    
    mapping_QCGraph = qcOperation.getFields();
    for item in mapping_QCGraph.items():
        print item;
        
    
    
    qcOperation.qcDisconnect();


def QC92_Lisbon():
    
    server = "http://10.152.152.22:8080/qcbin/"; 
    userName="tbraz";
    password = r"asd2!#zx";
    domainName="IPT_DWDM";
    projectName="hit7500_350_SIT";

    qcOperation = QCOperation(server, userName, password, domainName, projectName);
    qcOperation._qcConnect();
    
    mapping_QCGraph = qcOperation.getFields();
    for item in mapping_QCGraph.items():
        print item;
        
    
    
    qcOperation.qcDisconnect();



def QC90_Finnish():
    
    server = r"http://qualitycenter.inside.com/qcbin/"; 
    userName="wro50026";
    pass_ = "e8dbf113946b941a504c0889b93aaba9d7c760";
    password = crypt_pass.Heslo(pass_)._Heslo__readHeslo();
    domainName="BCS_DWDM";
    projectName="hiT7300";
    projectName="hiT7500";

    qcOperation = QCOperation(server, userName, password, domainName, projectName);
    qcOperation._qcConnect();
    
    mapping_QCGraph = qcOperation.getFields();
    for item in mapping_QCGraph.items():
        print item;
        
    
    
    qcOperation.qcDisconnect();

def QC100_Finnish():
    
    server = r"http://qc.inside.com/qcbin/"; 
    userName="wro50026";
    pass_ = "e8dbf113946b941a5f86584200c53539056d00";
    password = crypt_pass.Heslo(pass_)._Heslo__readHeslo();
    domainName="NWS_ON";
    projectName="hiT7300";

    qcOperation = QCOperation(server, userName, password, domainName, projectName);
    qcOperation._qcConnect();
    
#    try:
    mapping_QCGraph = qcOperation.getFields();
    print mapping_QCGraph;
    for item in mapping_QCGraph.items():
        print item;
        
    qcOperation.main(mapping_QCGraph);
#    except Any, Err:
#        print Err;
#        qcOperation.qcDisconnect();
#    
    qcOperation.qcDisconnect();


if __name__ == '__main__':
#    QC92_Lisbon();
#    QC92_Munich();
#    QC90_Finnish()
    QC100_Finnish();