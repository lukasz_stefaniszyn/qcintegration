'''
Created on 29-08-2010

@author: wro50026
'''


import lib.qc_connect_disconnect as qc_connect_disconnect
import lib.crypt_pass as crypt_pass 

class QCOperation(object):

    
    def __init__(self, server, userName, password, domainName, projectName): 
        
        self.server = server; 
        self.userName= userName;
        self.password = password;
        self.domainName= domainName;
        self.projectName= projectName;
    
#------------------------------------------------------------------------------ 
    def __get_qcInstance(self):
        return self.qcConnect;
    def __set_qcInstance(self, qcConnection):
        if qcConnection is not None:
            self.qcConnect = qcConnection;
            self.qc = qcConnection.qc;
            self.tm = self.qc.TreeManager;
            self.root = self.tm.TreeRoot("Subject");
        else:
            print "Unable to set qc connection";
    def __del_qcInstance(self):
        print "Disconnecting"
        self.qcDisconnect();
    qcInstance = property(__get_qcInstance,__set_qcInstance,__del_qcInstance,"QC connection instance");
#------------------------------------------------------------------------------ 
    
    def _qcConnect(self):
        
#        print self.dupa;
#        self.dupa = "gowno";
#        
#        print self.aValue;
#        self.aValue = 3;
#        
        
        
        qcConnect = qc_connect_disconnect.QC_connect(self.server, 
                                                     self.userName, 
                                                     self.password, 
                                                     self.domainName, 
                                                     self.projectName);
        if qcConnect.connect():
            print "Connected OK";
            #print self.qcInstance; 
            self.qcInstance = qcConnect;
        else:
            print "ERR: Not connected"
            self.qcInstance = None;
        
    def qcDisconnect(self):
        if self.qcInstance is None:
            return False;
        
        if self.qcInstance.disconnect_QC():
            print "\nDisconnecting from QC server."\
                    "\n"\
                    "\nThank You for using QC_import_export script.\n"\
                    "\t\t\t\n"\
                    "\t\t\t  Lukasz Stefaniszyn\n"\
                    "\n\t\t\t\t\tPowered by Python 2.5";
            return True;
        else:
            print "Unable to to disconnect";
            return False




    def getFields(self, type):
        '''
        Get fields names 
        
        '''
        
        if type == 'TestPlan':
            fields = ["TEST", "DESSTEPS", "STEP"]  #Test Plan
        elif type == 'TestLab':
            fields = ["CYCLE", "TESTCYCL"]  #Test Lab and Run
        
        
        
        fieldDict = {};
        num = 0;
        for name in fields:
            fieldDict.update(dict({name:{}}));
            fieldList = self.qc.Fields(name);
            for aField in fieldList:
                FieldProp = aField.Property;
                fieldName = aField.Name;
                fieldUserLabel=FieldProp.UserLabel;
                fieldIsActive=FieldProp.IsActive;
                fieldIsCanFilter=FieldProp.IsCanFilter;
#                if filedIsActive and filedIsCanFilter: 
                if str(fieldUserLabel) == '':
                    continue;
                colName = self.makeColumnName(name, fieldUserLabel);
                fieldDict[name].update(dict({fieldName:dict({'UserLabel':fieldUserLabel, 'ColName':colName, 'ColNum':None})}));
                num += 1;
            if fields.index(name) == 0:
                startColumnNumber = 0;
                groupName = name;
            else:
                group = fields.index(name);
                groupName = fields[group];
                group = fields.index(name)-1;
                prevGroupName = fields[group];
                startColumnNumber += len(fieldDict[prevGroupName]);
            self.makeColumnNumber(startColumnNumber, groupName, fieldDict)
        return fieldDict;

    def makeColumnName(self, groupName, fieldUserLabel):
        
        if groupName == 'TEST':
            fieldUserLabel = 'TP_TC_' + fieldUserLabel;
        elif groupName == 'DESSTEPS':
            fieldUserLabel = 'TP_ST_' + fieldUserLabel;
        elif groupName == 'STEP':
            fieldUserLabel = 'TL_ST_' + fieldUserLabel;
        elif groupName == 'CYCLE':
            fieldUserLabel = 'TL_TS_' + fieldUserLabel;
        elif groupName == 'TESTCYCL':
            fieldUserLabel = 'TL_TC_' + fieldUserLabel;
        else:
            print 'WRN: Did not find groupName in makeColumnName()'
        
        return fieldUserLabel

    
    def updateColNum(self, groupList, groupDict, startColumnNumber):
        '''
        Based on the groupList order, the ColumnNumber will be given
        input = list(groupList), example ["DS_STEP_NAME", "DS_ATTACHMENT"]
                dict(groupDict), example {u'ST_ID': {'UserLabel': u'Step ID', 'ColNum': 44, 'ColName': u'TL_ST_Step ID'}, u'ST_ACTUAL': {'UserLabel': u'Actual', 'ColNum': 43, 'ColName': u'TL_ST_Actual'}, u'ST_STEP_ORDER': {'UserLabel': u'Step Order', 'ColNum': 42, 'ColName': u'TL_ST_Step Order'}, u'ST_ATTACHMENT': {'UserLabel': u'Attachment', 'ColNum': 41, 'ColName': u'TL_ST_Attachment'}, u'ST_STEP_ID': {'UserLabel': u'Step No', 'ColNum': 40, 'ColName': u'TL_ST_Step No'}, u'ST_PATH': {'UserLabel': u'Path', 'ColNum': 39, 'ColName': u'TL_ST_Path'}, u'ST_TEST_ID': {'UserLabel': u'Source Test', 'ColNum': 38, 'ColName': u'TL_ST_Source Test'}, u'ST_OBJ_ID': {'UserLabel': u'Level', 'ColNum': 37, 'ColName': u'TL_ST_Level'}, u'ST_COMPONENT_DATA': {'UserLabel': u'Component Step Data', 'ColNum': 36, 'ColName': u'TL_ST_Component Step Data'}, u'ST_STEP_NAME': {'UserLabel': u'Step Name', 'ColNum': 35, 'ColName': u'TL_ST_Step Name'}, u'ST_STATUS': {'UserLabel': u'Status', 'ColNum': 34, 'ColName': u'TL_ST_Status'}, u'ST_DESCRIPTION': {'UserLabel': u'Description', 'ColNum': 33, 'ColName': u'TL_ST_Description'}, u'ST_RUN_ID': {'UserLabel': u'Run No', 'ColNum': 32, 'ColName': u'TL_ST_Run No'}, u'ST_LEVEL': {'UserLabel': u'Level', 'ColNum': 31, 'ColName': u'TL_ST_Level'}, u'ST_DESSTEP_ID': {'UserLabel': u'DesignStep ID', 'ColNum': 30, 'ColName': u'TL_ST_DesignStep ID'}, u'ST_PARENT_ID': {'UserLabel': u'Step Parent ID', 'ColNum': 29, 'ColName': u'TL_ST_Step Parent ID'}, u'ST_EXPECTED': {'UserLabel': u'Expected', 'ColNum': 28, 'ColName': u'TL_ST_Expected'}, u'ST_DESIGN_ID': {'UserLabel': u'From Step Id', 'ColNum': 27, 'ColName': u'TL_ST_From Step Id'}, u'ST_EXTENDED_REFERENCE': {'UserLabel': u'Extended Reference', 'ColNum': 26, 'ColName': u'TL_ST_Extended Reference'}, u'ST_EXECUTION_DATE': {'UserLabel': u'Exec Date', 'ColNum': 25, 'ColName': u'TL_ST_Exec Date'}, u'ST_LINE_NO': {'UserLabel': u'Line_no', 'ColNum': 24, 'ColName': u'TL_ST_Line_no'}, u'ST_EXECUTION_TIME': {'UserLabel': u'Exec Time', 'ColNum': 23, 'ColName': u'TL_ST_Exec Time'}}
                int(startColumnNumber)
        output = int(startColumnNumber);
        '''
        for fieldName in groupList:
            number= startColumnNumber;
            groupDict[fieldName].update({'ColNum':number});
            startColumnNumber += 1;
        return startColumnNumber;
    
    def makeColumnNumber(self, startColumnNumber, groupName, fieldDict):
        '''
        Make first column numbering, based on fixed groupLists
        input = int(startColumnNumber)
                str(groupName),  example  "TEST",   "DESSTEPS"
                dict(fieldDict), example {'TEST': {u'TS_TEST_ID': {'UserLabel': u'Test ID', 'ColNum': 25, 'ColName': u'TP_TC_Test ID'}, u'TS_TEMPLATE': {'UserLabel': u'Template', 'ColNum': 24, 'ColName': u'TP_TC_Template'}, u'TS_TYPE': {'UserLabel': u'Type', 'ColNum': 23, 'ColName': u'TP_TC_Type'}, u'TS_VTS': {'UserLabel': u'Modified', 'ColNum': 22, 'ColName': u'TP_TC_Modified'}, u'TS_STATUS': {'UserLabel': u'Status', 'ColNum': 21, 'ColName': u'TP_TC_Status'}, u'TS_VC_CUR_VER': {'UserLabel': u'Test Current Version', 'ColNum': 20, 'ColName': u'TP_TC_Test Current Version'}, u'TS_CREATION_DATE': {'UserLabel': u'Creation Date', 'ColNum': 19, 'ColName': u'TP_TC_Creation Date'}, u'TS_RUNTIME_DATA': {'UserLabel': u'Test Runtime Data', 'ColNum': 18, 'ColName': u'TP_TC_Test Runtime Data'}, u'TS_STEP_PARAM': {'UserLabel': u'Step Param', 'ColNum': 17, 'ColName': u'TP_TC_Step Param'}, u'TS_RESPONSIBLE': {'UserLabel': u'Designer', 'ColNum': 4, 'ColName': u'TP_TC_Designer'}, u'TS_TASK_STATUS': {'UserLabel': u'Task Status', 'ColNum': 16, 'ColName': u'TP_TC_Task Status'}, u'TS_NAME': {'UserLabel': u'Test Name', 'ColNum': 3, 'ColName': u'TP_TC_Test Name'}, u'TS_EXEC_STATUS': {'UserLabel': u'Execution Status', 'ColNum': 15, 'ColName': u'TP_TC_Execution Status'}, u'TS_PATH': {'UserLabel': u'Path', 'ColNum': 0, 'ColName': u'TP_TC_Path'}, u'TS_ATTACHMENT': {'UserLabel': u'Attachment', 'ColNum': 2, 'ColName': u'TP_TC_Attachment'}, u'TS_SUBJECT': {'UserLabel': u'Subject', 'ColNum': 14, 'ColName': u'TP_TC_Subject'}, u'TS_DESCRIPTION': {'UserLabel': u'Description', 'ColNum': 1, 'ColName': u'TP_TC_Description'}, u'TS_DEV_COMMENTS': {'UserLabel': u'Comments', 'ColNum': 13, 'ColName': u'TP_TC_Comments'}, u'TS_USER_04': {'UserLabel': u'Test Fields', 'ColNum': 12, 'ColName': u'TP_TC_Test Fields'}, u'TS_USER_05': {'UserLabel': u'Bringup Test Group', 'ColNum': 11, 'ColName': u'TP_TC_Bringup Test Group'}, u'TS_USER_06': {'UserLabel': u'Bringup Test Sub Group', 'ColNum': 10, 'ColName': u'TP_TC_Bringup Test Sub Group'}, u'TS_USER_03': {'UserLabel': u'Priority', 'ColNum': 9, 'ColName': u'TP_TC_Priority'}, u'TS_ESTIMATE_DEVTIME': {'UserLabel': u'Estimated DevTime', 'ColNum': 8, 'ColName': u'TP_TC_Estimated DevTime'}, u'TS_USER_01': {'UserLabel': u'RegressionTest', 'ColNum': 7, 'ColName': u'TP_TC_RegressionTest'}, u'TS_USER_02': {'UserLabel': u'Test4Version', 'ColNum': 6, 'ColName': u'TP_TC_Test4Version'}, u'TS_STEPS': {'UserLabel': u'Steps', 'ColNum': 5, 'ColName': u'TP_TC_Steps'}}, 'DESSTEPS': {u'DS_TEST_ID': {'UserLabel': u'Test ID', 'ColNum': 19, 'ColName': u'TP_ST_Test ID'}, u'DS_LINK_TEST': {'UserLabel': u'Link Test', 'ColNum': 18, 'ColName': u'TP_ST_Link Test'}, u'DS_STEP_ID': {'UserLabel': u'Step ID', 'ColNum': 17, 'ColName': u'TP_ST_Step ID'}, u'DS_STEP_ORDER': {'UserLabel': u'Step#', 'ColNum': 16, 'ColName': u'TP_ST_Step#'}, u'DS_DESCRIPTION': {'UserLabel': u'Description', 'ColNum': 15, 'ColName': u'TP_ST_Description'}, u'DS_HAS_PARAMS': {'UserLabel': u'Has Params', 'ColNum': 14, 'ColName': u'TP_ST_Has Params'}, u'DS_ID': {'UserLabel': u'Step ID', 'ColNum': 13, 'ColName': u'TP_ST_Step ID'}, u'DS_STEP_NAME': {'UserLabel': u'Step Name', 'ColNum': 11, 'ColName': u'TP_ST_Step Name'}, u'DS_ATTACHMENT': {'UserLabel': u'Attachment', 'ColNum': 11, 'ColName': u'TP_ST_Attachment'}, u'DS_EXPECTED': {'UserLabel': u'Expected Result', 'ColNum': 12, 'ColName': u'TP_ST_Expected Result'}}, 'STEP': {u'ST_ID': {'UserLabel': u'Step ID', 'ColNum': 44, 'ColName': u'TL_ST_Step ID'}, u'ST_ACTUAL': {'UserLabel': u'Actual', 'ColNum': 43, 'ColName': u'TL_ST_Actual'}, u'ST_STEP_ORDER': {'UserLabel': u'Step Order', 'ColNum': 42, 'ColName': u'TL_ST_Step Order'}, u'ST_ATTACHMENT': {'UserLabel': u'Attachment', 'ColNum': 41, 'ColName': u'TL_ST_Attachment'}, u'ST_STEP_ID': {'UserLabel': u'Step No', 'ColNum': 40, 'ColName': u'TL_ST_Step No'}, u'ST_PATH': {'UserLabel': u'Path', 'ColNum': 39, 'ColName': u'TL_ST_Path'}, u'ST_TEST_ID': {'UserLabel': u'Source Test', 'ColNum': 38, 'ColName': u'TL_ST_Source Test'}, u'ST_OBJ_ID': {'UserLabel': u'Level', 'ColNum': 37, 'ColName': u'TL_ST_Level'}, u'ST_COMPONENT_DATA': {'UserLabel': u'Component Step Data', 'ColNum': 36, 'ColName': u'TL_ST_Component Step Data'}, u'ST_STEP_NAME': {'UserLabel': u'Step Name', 'ColNum': 35, 'ColName': u'TL_ST_Step Name'}, u'ST_STATUS': {'UserLabel': u'Status', 'ColNum': 34, 'ColName': u'TL_ST_Status'}, u'ST_DESCRIPTION': {'UserLabel': u'Description', 'ColNum': 33, 'ColName': u'TL_ST_Description'}, u'ST_RUN_ID': {'UserLabel': u'Run No', 'ColNum': 32, 'ColName': u'TL_ST_Run No'}, u'ST_LEVEL': {'UserLabel': u'Level', 'ColNum': 31, 'ColName': u'TL_ST_Level'}, u'ST_DESSTEP_ID': {'UserLabel': u'DesignStep ID', 'ColNum': 30, 'ColName': u'TL_ST_DesignStep ID'}, u'ST_PARENT_ID': {'UserLabel': u'Step Parent ID', 'ColNum': 29, 'ColName': u'TL_ST_Step Parent ID'}, u'ST_EXPECTED': {'UserLabel': u'Expected', 'ColNum': 28, 'ColName': u'TL_ST_Expected'}, u'ST_DESIGN_ID': {'UserLabel': u'From Step Id', 'ColNum': 27, 'ColName': u'TL_ST_From Step Id'}, u'ST_EXTENDED_REFERENCE': {'UserLabel': u'Extended Reference', 'ColNum': 26, 'ColName': u'TL_ST_Extended Reference'}, u'ST_EXECUTION_DATE': {'UserLabel': u'Exec Date', 'ColNum': 25, 'ColName': u'TL_ST_Exec Date'}, u'ST_LINE_NO': {'UserLabel': u'Line_no', 'ColNum': 24, 'ColName': u'TL_ST_Line_no'}, u'ST_EXECUTION_TIME': {'UserLabel': u'Exec Time', 'ColNum': 23, 'ColName': u'TL_ST_Exec Time'}}}
        output = None
        '''
        
        
        groupDict = fieldDict[groupName];
        fields = groupDict.keys();
        
        groupTEST = ["TS_PATH", "TS_NAME", "TS_ATTACHMENT", "TS_DESCRIPTION", "TS_RESPONSIBLE"]
        groupDESSTEPS = ["DS_STEP_NAME", "DS_ATTACHMENT"];
        groupSTEP = ["ST_STEP_NAME", "ST_STATUS", "ST_ATTACHMENT", "ST_DESCRIPTION", "ST_EXPECTED", "ST_ACTUAL"];
#        groupTESTCYCL = [""]
        
        
        if groupName == "TEST":
            startColumnNumber = self.updateColNum(groupTEST, groupDict, startColumnNumber)
        elif groupName == "DESSTEPS":
            startColumnNumber = self.updateColNum(groupDESSTEPS, groupDict, startColumnNumber)
        elif groupName == "STEP":
            startColumnNumber = self.updateColNum(groupSTEP, groupDict, startColumnNumber)
        
        for fieldName in fields:
            print groupDict[fieldName].get('ColName'), fieldName;
            if groupDict[fieldName].get('ColNum') is None:
                number = startColumnNumber;
                groupDict[fieldName].update({'ColNum':number});
                startColumnNumber += 1;

def QC92_Munich():
    
    server = "http://muvmp034.nsn-intra.net/qcbin/"; 
    userName="stefaniszyn";
    pass_ = "e8dbf113946b941a4a34b573363674a3c58520";
    password = crypt_pass.Heslo(pass_)._Heslo__readHeslo();
    domainName="HIT7300";
    projectName="hiT73_R43x";
#
#    domainName="HIT7500";
#    projectName="hiT7500_R330_R340";

    qcOperation = QCOperation(server, userName, password, domainName, projectName);
    qcOperation._qcConnect();
    
    fieldDict = qcOperation.getFields(type='TestPlan');
    print fieldDict
    for item in fieldDict.items():
        print item;
        
    
    
    qcOperation.qcDisconnect();


def QC92_Lisbon():
    
    server = "http://10.152.152.22:8080/qcbin/"; 
    userName="tbraz";
    password = r"asd2!#zx";
    domainName="IPT_DWDM";
    projectName="hit7500_350_SIT";

    qcOperation = QCOperation(server, userName, password, domainName, projectName);
    qcOperation._qcConnect();
    
    fieldDict = qcOperation.getFields(type='TestPlan');
    for item in fieldDict.items():
        print item;
        
    
    
    qcOperation.qcDisconnect();



def QC90_Finnish():
    
    server = r"http://qualitycenter.inside.com/qcbin/"; 
    userName="wro50026";
    pass_ = "e8dbf113946b941a504c0889b93aaba9d7c760";
    password = crypt_pass.Heslo(pass_)._Heslo__readHeslo();
    domainName="BCS_DWDM";
    projectName="hiT7300";
    projectName="hiT7500";

    qcOperation = QCOperation(server, userName, password, domainName, projectName);
    qcOperation._qcConnect();
    
    fieldDict = qcOperation.getFields(type='TestPlan');
    for item in fieldDict.items():
        print item;
        
    
    
    qcOperation.qcDisconnect();



if __name__ == '__main__':
#    QC92_Lisbon();
    QC92_Munich();
#    QC90_Finnish()