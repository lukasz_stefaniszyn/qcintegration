'''
Created on 14-07-2012

@author: lucst
'''
import unittest
import sys
from PyQt4.QtGui import *
from PyQt4.QtCore import *

app = QApplication(sys.argv)



class DialogWindows(QMessageBox):
    
    def __init__(self, parent):
        super(DialogWindows, self).__init__(parent);
        self.setWindowTitle('License Manager')
    
    def criticalWindow(self, text):
        
        self.setIcon(self.Critical);
        self.setStandardButtons(QMessageBox.Ok);
        self.setText(text);
        
    def informationWindow(self, text):
        self.setIcon(self.Information);
        self.setStandardButtons(QMessageBox.Ok);
        self.setText(text);
        
class TestMessageBox(object):
    
    def __init__(self):
        self.status = False
              
        
    def get_status(self):
        return self.__status
    def set_status(self, value):
        self.__status = value
    status = property(get_status, set_status, None, "status's docstring")
    
    def test_method(self):
        
        if True: 
            self.status= True
            for dialogmessage in self.messagebox(): yield dialogmessage
            
    def messagebox(self):
        dialogWindows = DialogWindows(None);
        dialogWindows.informationWindow('Description');
        yield dialogWindows
        dialogWindows.exec_();


#==============================================================================
# UnitTest
#==============================================================================
class TestReadReg(unittest.TestCase):

    
    
    def setUp(self):
        pass

    def tearDown(self):
        pass;
    

    def test_1(self):
        testMessagebox = TestMessageBox()
        
        for dialogWindows in testMessagebox.test_method():
            QTimer.singleShot(1*1000, dialogWindows, SLOT('accept()')); ##this will push OK button after 5 sec
    

def load_tests(loader, tests, pattern):
    suite = unittest.TestSuite()

    return suite    
    
if __name__ == '__main__':
    sys.exit(app.exec_())
    del app;
