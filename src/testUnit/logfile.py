'''
Created on 20-06-2011

@author: wro50026
'''
import os
import logging

class Logger(object):
    def __init__(self, loggername='root', logfilename='logfile.log', resetefilelog=False):
        if resetefilelog:
            logfilename = os.path.join(os.getcwd(), logfilename);
            if os.path.exists(logfilename):
                try:
                    os.remove(logfilename);
                except WindowsError, err:
                    print ("unable remove previous log file:", err);
        
        
        self.log = logging.getLogger(loggername)
        #formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
        
       
        
        if logfilename is not None:
            logfilename = os.path.join(os.getcwd(), logfilename)
            logging.basicConfig(filename=logfilename, level=logging.DEBUG, 
                                format="%(asctime)s - %(name)s - %(levelname)s - %(message)s")
        #logger1 = logging.getLogger('module1')
        
        streamhandler = logging.StreamHandler()
        streamhandler.setLevel(logging.NOTSET)
        formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
        streamhandler.setFormatter(formatter)
        self.log.addHandler(streamhandler)
        
    #------------------------------------------------------------------------------ 
    def __get_logger(self):
        return self.__logger;
    def __set_logger(self, logger):
        self.__logger = logger;
    def __del_logger(self):
        print "zamykanie Loga";
        self.__logger.shutdown() 
    log = property(__get_logger, __set_logger, __del_logger, "This is logger instance");
    #------------------------------------------------------------------------------ 


    
if __name__ == "__main__":
#    from logfile import Logger
    log = Logger(loggername="old_lib.test", resetefilelog=True).log;
#    print logger.__logger
   
    log.info("cos");