import os
import sys
os.chdir(r"D:\Eclipse_workspace\QC_gui\src")
from PyQt4 import QtCore, QtGui
from lib.QCTestX.qcTestX_upload_chooseFields import QcTestXUploadChooseFields as Choose
from lib.QCTestX.treeView.treeTestXModel import TreeView, TreeModel, TreeItem
import lib.QualityCenter_ext.lib.qc_connect_disconnect as qc_connect_disconnect


server = "http://qc:8080/qcbin/";
userName="alex_qc";
domainName="DEFAULT";
projectName="QualityCenter_Demo";
password = ""; ##Put the right password

qcConnect = qc_connect_disconnect.QC_connect(server, userName, password, domainName,projectName);
for result in qcConnect.connect():
    print result;
qc = qcConnect.qc;

def __addUserFieldsTreeView():
    mapping_fields = {'TestCase': {'Planned Language': {'FieldOrder': '28'}, 
                                   'Plan: Template': {'FieldOrder': '13'}, 
                                   'Plan: Subject': {'FieldOrder': '14'}, 
                                   'Iterations': {'FieldOrder': '40'}, 
                                   'Plan: Modified': {'FieldOrder': '41'}, 
                                   'Test Version': {'FieldOrder': '17'}, 
                                   'Planned Browser': {'FieldOrder': '20'}, 
                                   'Modified': {'FieldOrder': '18'}, 
                                   'Responsible Tester': {'FieldOrder': '21'}, 
                                   'Planned Host Name': {'FieldOrder': '22'}, 
                                   'Planned Exec Time': {'FieldOrder': '23'}, 
                                   'Time': {'FieldOrder': '16'}, 
                                   'Test': {'FieldOrder': '25'}, 
                                   'Plan: Type': {'FieldOrder': '30'}, 
                                   'Plan: Creation Date': {'FieldOrder': '19'}, 
                                   'Plan: Test': {'FieldOrder': '26'}, 
                                   'Plan: Test Name': {'FieldOrder': '27'}, 
                                   'Status': {'FieldOrder': '12'}, 
                                   'Plan: Reviewed': {'FieldOrder': '29'}, 
                                   'Plan: Description': {'FieldOrder': '31'}, 
                                   'Tester': {'FieldOrder': '32'}, 
                                   'Plan: Level': {'FieldOrder': '33'}, 
                                   'Plan: Execution Status': {'FieldOrder': '34'}, 
                                   'Exec Date': {'FieldOrder': '35'}, 
                                   'Plan: Status': {'FieldOrder': '36'}, 
                                   'Plan: Path': {'FieldOrder': '37'}, 
                                   'Plan: Estimated DevTime': {'FieldOrder': '38'}, 
                                   'Plan: Reviewer': {'FieldOrder': '39'}, 
                                   'Plan: Designer': {'FieldOrder': '15'}, 
                                   'Plan: Priority': {'FieldOrder': '24'}, 
                                   'Planned Exec Date': {'FieldOrder': '42'}, 
                                   'Attachment': {'FieldOrder': '43'}}, 
                      'Step': {
                               'Status': {'FieldOrder': '44'},
                               'Source Test': {'FieldOrder': '45'}, 
                               'Actual': {'FieldOrder': '46'}, 
                               'Description': {'FieldOrder': '47'}, 
                               'Attachment': {'FieldOrder': '52'}, 
                               'Expected': {'FieldOrder': '48'}, 
                               'Exec Date': {'FieldOrder': '49'}, 
                               'Step Name': {'FieldOrder': '50'}, 
                               'Exec Time': {'FieldOrder': '51'}}, 
                      'TestSet': {
                                  'Status': {'FieldOrder': '1'}, 
                                  'Close Date': {'FieldOrder': '2'}, 
                                  'Test Set': {'FieldOrder': '7'}, 
                                  'Description': {'FieldOrder': '4'}, 
                                  'Modified': {'FieldOrder': '8'}, 
                                  'ITG Request Id': {'FieldOrder': '9'}, 
                                  'Open Date': {'FieldOrder': '10'}, 
                                  'Dupa': {'FieldOrder': '3'}, 
                                  'Attachment': {'FieldOrder': '11'}, 
                                  'Test Set Folder': {'FieldOrder': '5'}, 
                                  'test': {'FieldOrder': '6'}}};
                      
                      
    mapping_fields_2 = {"TestSet": {'Test Set Name':{'FieldOrder':'0',
                                                   'FieldQcName':'TS_NAME',
                                                   'FieldName':'QC Test Set Name',
                                                   'FieldType':'char',
                                                   'FieldIsRequired':'True'},
                                  'Test Set Directory': {'FieldOrder':'1',
                                                         'FieldQcName':'TS_DIR',
                                                         'FieldName':'QC Test Set Directory',
                                                         'FieldType':'char',
                                                         'FieldIsRequired':'True'},
                                  'Test Set Description': {'FieldOrder':'2',
                                                           'FieldQcName':'TS_DESC',
                                                           'FieldName':'QC Test Set Description',
                                                           'FieldType':'char',
                                                           'FieldIsRequired':'True'}
                                  },
                       "TestCase": {'Test Case Name': {'FieldOrder':'0',
                                                      'FieldQcName':'TC_NAME',
                                                      'FieldName':'QC Test Case Name',
                                                      'FieldType':'char',
                                                      'FieldIsRequired':'True'},
                                    'Test Case Directory': {'FieldOrder':'1',
                                                          'FieldQcName':'TC_DIR',
                                                          'FieldName':'QC Test Case Directory',
                                                          'FieldType':'char',
                                                          'FieldIsRequired':'True'},
                                    'Test Case Status': {'FieldOrder':'2',
                                                          'FieldQcName':'TC_STATUS',
                                                          'FieldName':'QC Test Case Status',
                                                          'FieldType':'char',
                                                          'FieldIsRequired':'True'},
                                    },
                       "Step": {'Step Name': {'FieldOrder':'0',
                                              'FieldQcName':'ST_NAME',
                                              'FieldName':'QC Step Name',
                                              'FieldType':'char',
                                              'FieldIsRequired':'True'},
                                'Step Description': {'FieldOrder':'1',
                                              'FieldQcName':'ST_DESC',
                                              'FieldName':'QC Step Description',
                                              'FieldType':'char',
                                              'FieldIsRequired':'True'}
                                }
                       };
    gui.qCTestXUserField.updateModel(mapping_fields);








app = QtGui.QApplication(sys.argv)
main= QtGui.QWidget()
gui = Choose(type="TestPlan", parent=main, qcConnect=qcConnect)

treeViewQcField = gui.qCTestXQcField.treeView
treeViewUserField = gui.qCTestXUserField.treeView


#
#data = ([('TestSet', "A-qcInstance", "", ""),
#         ('TestCase',"B-qcinstance", "", ""),
#         ('Step', "G-qcinstance", "", "")],
#        [('G01 TestCase', 'TestCase-qcInstance', 'USER TestCase', 'User TestCase-qcInstance')
#         ]
#        );
#model = TreeModel(data, moreColumns=True)run

#treeViewUserField.setModel(model);

#mappingFieldsQc = {
#                   "CYCLE": {'Test Set Name':('TS_NAME', 'char', 'True'),
#                             'Test Set Directory': ('TS_DIR', 'char', 'True'),
#                             'Test Set Description': ('TS_DESC', 'char', 'True')
#                             },
#                  "TESTCYCL": {'Test Case Name': ('TC_NAME', 'char', 'True'),
#                              'Test Case Directory': ('TC_DIR', 'char', 'True'),
#                              'Test Case Status': ('TC_STATUS', 'char', 'True'),
#                              },
#                  "STEP": {'Step Name': ('ST_NAME', 'char', 'True'),
#                           'Step Description': ('ST_DESC', 'char', 'True')
#                           }
#                  };
#gui.qCTestXDownloadQcField.updateModel(mappingFieldsQc);
__addUserFieldsTreeView();
gui.pushButton_QCTestXUpload_Refresh_clicked()

main.show()

print "Use variable=gui for QcTestXUploadChooseFields"
print "Use variable=main for Main Window application"




