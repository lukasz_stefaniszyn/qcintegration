'''
Created on 24-12-2011

@author: qc
'''

from win32com.client import gencache, DispatchWithEvents, constants
gencache.EnsureModule('{D66ADC20-B070-11D3-8604-0050046B8F4A}', 0, 1, 0);

import os;
os.chdir(r"D:\Eclipse_workspace\QC_gui\src");
import lib.QualityCenter_ext.lib.qc_connect_disconnect as qc_connect_disconnect

server = "http://qc:8080/qcbin/";
userName="alex_qc";
domainName="DEFAULT";
projectName="QualityCenter_Demo";
password = ""; ##Put the right password


qcConnect = qc_connect_disconnect.QC_connect(server, userName, password, domainName,projectName);
for result in qcConnect.connect():
    print result;
#qcConnect.connect();
qc = qcConnect.qc;


testSetDir = r'Root\Mercury Tours (HTML Edition)\Version 1.0\Functionality And UI'
testSetName = r'Mercury Tours Sanity'

tm = qc.TestSetTreeManager
root = qc.TestSetTreeManager.Root

dirInstance = tm.NodeByPath(Path=testSetDir)
ts = dirInstance.FindTestSets(Pattern=testSetName, MatchCase=True)[0];
print "dirInstance:",dirInstance
print "ts:", ts;





def get_fields():
    """Get Fields names used for TestLab/TestPlan upload/download
    Make mapping of DataBaseName to UserName field
    {'TC_CYCLE':'Test Set'} 
    """
    
    #======================================================================
    # fields = ["TESTCYCL", "CYCLE"]  #Test Lab and Run
    # fields = ["TEST"]  #Test Plan
    # "TESTCYCL" - is for TestCase fields 
    # "TEST" - is for TestSet fields
    # "STEP" - is for Step fields
    #======================================================================
    
    
    
    mapping_fields = {};
#    for name in ["TESTCYCL", "TEST", "STEP"]:
    fieldList = qc.Fields("STEP");
    for aField in fieldList:
        fieldProp = aField.Property;
        fieldName = aField.Name;
        fieldUserLabel=fieldProp.UserLabel;
        fieldIsActive=fieldProp.IsActive;
        fieldIsCanFilter=fieldProp.IsCanFilter;
        type = fieldProp.UserColumnType; ##what type of data,  char,date, time
        isRequired = fieldProp.IsRequired ##is this fields is in the list of required fields  
 
        mapping_fields[str(fieldName)]= str(fieldUserLabel);
    return mapping_fields;
