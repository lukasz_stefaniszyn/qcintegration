'''
Created on 19-10-2011

@author: wro50026
'''


def all_perms(str):
    if len(str)<=1:
        yield str;
    else:
        for perm in all_perms(str[1:]):
            for i in range(len(perm)+1):
                yield perm[:i] + str[0:1] + perm[i:]

if __name__ == "__name__":
    import itertools
    s = itertools.permutations(['a', 'b', 'c']);
    print s;
    
    for p in all_perms(['a', 'b', 'c']):
        print p;