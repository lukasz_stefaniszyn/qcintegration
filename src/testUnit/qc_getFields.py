'''
Created on 30-07-2010

@author: wro50026
'''

import lib.qc_connect_disconnect as qc_connect_disconnect
import lib.crypt_pass as crypt_pass 

class QCOperation(object):

    
    def __init__(self, server, userName, password, domainName, projectName): 
        
        self.server = server; 
        self.userName= userName;
        self.password = password;
        self.domainName= domainName;
        self.projectName= projectName;
    
#------------------------------------------------------------------------------ 
    def __get_qcInstance(self):
        return self.qcConnect;
    def __set_qcInstance(self, qcConnection):
        if qcConnection is not None:
            self.qcConnect = qcConnection;
            self.qc = qcConnection.qc;
            self.tm = self.qc.TreeManager;
            self.root = self.tm.TreeRoot("Subject");
        else:
            print "Unable to set qc connection";
    def __del_qcInstance(self):
        print "Disconnecting"
        self.qcDisconnect();
    qcInstance = property(__get_qcInstance,__set_qcInstance,__del_qcInstance,"QC connection instance");
#------------------------------------------------------------------------------ 
    
    def _qcConnect(self):
        
#        print self.dupa;
#        self.dupa = "gowno";
#        
#        print self.aValue;
#        self.aValue = 3;
#        
        
        
        qcConnect = qc_connect_disconnect.QC_connect(self.server, 
                                                     self.userName, 
                                                     self.password, 
                                                     self.domainName, 
                                                     self.projectName);
        if qcConnect.connect():
            print "Connected OK";
            #print self.qcInstance; 
            self.qcInstance = qcConnect;
        else:
            print "ERR: Not connected"
            self.qcInstance = None;
        
    def qcDisconnect(self):
        if self.qcInstance is None:
            return False;
        
        if self.qcInstance.disconnect_QC():
            print "\nDisconnecting from QC server."\
                    "\n"\
                    "\nThank You for using QC_import_export script.\n"\
                    "\t\t\t\n"\
                    "\t\t\t  Lukasz Stefaniszyn\n"\
                    "\n\t\t\t\t\tPowered by Python 2.5";
            return True;
        else:
            print "Unable to to disconnect";
            return False




    def getFields(self):
        
        fields = ["TESTCYCL", "CYCLE"]  #Test Lab and Run
        fields = ["TEST"]  #Test Plan
        
        
        mapping_QCGraph = {};
        for name in fields:
            fieldList = self.qc.Fields(name);
            for aField in fieldList:
                FieldProp = aField.Property;
                filedName = aField.Name;
                filedUserLabel=FieldProp.UserLabel;
                filedIsActive=FieldProp.IsActive;
                filedIsCanFilter=FieldProp.IsCanFilter;
                if filedIsActive and filedIsCanFilter: 
                    mapping_QCGraph[str(filedName)]= str(filedUserLabel);
                    
        return mapping_QCGraph;



def QC92_Munich():
    
    server = "http://muvmp034.nsn-intra.net/qcbin/"; 
    userName="stefaniszyn";
    pass_ = "e8dbf113946b941a4a34b573363674a3c58520";
    password = crypt_pass.Heslo(pass_)._Heslo__readHeslo();
    domainName="HIT7300";
    projectName="hiT73_R43x";

    domainName="HIT7500";
    projectName="hiT7500_R330_R340";

    qcOperation = QCOperation(server, userName, password, domainName, projectName);
    qcOperation._qcConnect();
    
    mapping_QCGraph = qcOperation.getFields();
    for item in mapping_QCGraph.items():
        print item;
        
    
    
    qcOperation.qcDisconnect();


def QC92_Lisbon():
    
    server = "http://10.152.152.22:8080/qcbin/"; 
    userName="tbraz";
    password = r"asd2!#zx";
    domainName="IPT_DWDM";
    projectName="hit7500_350_SIT";

    qcOperation = QCOperation(server, userName, password, domainName, projectName);
    qcOperation._qcConnect();
    
    mapping_QCGraph = qcOperation.getFields();
    for item in mapping_QCGraph.items():
        print item;
        
    
    
    qcOperation.qcDisconnect();



def QC90_Finnish():
    
    server = r"http://qualitycenter.inside.com/qcbin/"; 
    userName="wro50026";
    pass_ = "e8dbf113946b941a504c0889b93aaba9d7c760";
    password = crypt_pass.Heslo(pass_)._Heslo__readHeslo();
    domainName="BCS_DWDM";
    projectName="hiT7300";
    projectName="hiT7500";

    qcOperation = QCOperation(server, userName, password, domainName, projectName);
    qcOperation._qcConnect();
    
    mapping_QCGraph = qcOperation.getFields();
    for item in mapping_QCGraph.items():
        print item;
        
    
    
    qcOperation.qcDisconnect();



if __name__ == '__main__':
#    QC92_Lisbon();
#    QC92_Munich();
    QC90_Finnish()