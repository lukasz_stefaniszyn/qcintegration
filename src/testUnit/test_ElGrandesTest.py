'''
Created on 10-01-2012

@author: qc
'''
import unittest
import sys
from PyQt4 import QtGui 

from test_QcTestX import TestTreeViewBasic
from test_QcTestX2_1 import TestTreeButtonOperation as TestTreeButtonOperationDownload
from test_QcTestX2_2 import TestTreeButtonOperation as TestTreeButtonOperationUpload
from test_QcTestX3_withQCconnection import TestChooseFieldQc
from test_QcTestX4_fieldFileOperation import TestXMLDownloadSettingOperation, TestXMLUploadSettingOperation
import test_TestLabDownload 
import test_TestLabUpload
import test_TestPlanDownload
from test_csvOperation import TestReadCSVfile_init, TestWriteCSVfile_init



if __name__ == "__main__":
    
    app = QtGui.QApplication(sys.argv)
    suite = unittest.TestSuite()

    suite.addTest(unittest.makeSuite(TestTreeViewBasic));  ##Run all tests
    suite.addTest(unittest.makeSuite(TestTreeButtonOperationDownload));  ##Run all tests
    suite.addTest(unittest.makeSuite(TestTreeButtonOperationUpload));  ##Run all tests
    suite.addTest(unittest.makeSuite(TestChooseFieldQc));  ##Run all tests
    suite.addTest(unittest.makeSuite(TestXMLDownloadSettingOperation));  ##Run all tests
    suite.addTest(unittest.makeSuite(TestXMLUploadSettingOperation));  ##Run all tests
    
    
    suite.addTest(unittest.makeSuite(TestReadCSVfile_init));  ##Run all tests
    suite.addTest(unittest.makeSuite(TestWriteCSVfile_init));  ##Run all tests
    
    
    suite.addTest(unittest.makeSuite(test_TestLabDownload.TestTestLabDownload_AllTestSet));  ##Run all tests
    suite.addTest(unittest.makeSuite(test_TestLabDownload.TestTestLabDownload_TestSet));  ##Run all tests
    suite.addTest(unittest.makeSuite(test_TestLabDownload.TestTestLabDownload_AllTestCases));  ##Run all tests
    suite.addTest(unittest.makeSuite(test_TestLabDownload.TestTestLabDownload_TestCase));  ##Run all tests
    suite.addTest(unittest.makeSuite(test_TestLabDownload.TestTestLabDownload_AllSteps));  ##Run all tests
    suite.addTest(unittest.makeSuite(test_TestLabDownload.TestTestLabDownload_Step));  ##Run all tests
    suite.addTest(unittest.makeSuite(test_TestLabDownload.TestTestLabDownload_main));  ##Run all tests

    suite.addTest(unittest.makeSuite(test_TestLabUpload.TestTestLabUpload_TestSet));  ##Run all tests
    suite.addTest(unittest.makeSuite(test_TestLabUpload.TestTestLabUpload_AllTestCases));  ##Run all tests
    suite.addTest(unittest.makeSuite(test_TestLabUpload.TestTestLabUpload_TestCase));  ##Run all tests
    suite.addTest(unittest.makeSuite(test_TestLabUpload.TestTestLabUpload_AllSteps));  ##Run all tests
    suite.addTest(unittest.makeSuite(test_TestLabUpload.TestTestLabUpload_Step));  ##Run all tests
    suite.addTest(unittest.makeSuite(test_TestLabUpload.TestTestLabUpload_main));  ##Run all tests
#    
    suite.addTest(unittest.makeSuite(test_TestPlanDownload.TestTestPlanDownload_AllTestCases));  ##Run all tests
    suite.addTest(unittest.makeSuite(test_TestPlanDownload.TestTestPlanDownload_TestCase));  ##Run all tests
    suite.addTest(unittest.makeSuite(test_TestPlanDownload.TestTestPlanDownload_AllSteps));  ##Run all tests
    suite.addTest(unittest.makeSuite(test_TestPlanDownload.TestTestPlanDownload_Step));  ##Run all tests
    suite.addTest(unittest.makeSuite(test_TestPlanDownload.TestTestPlanDownload_main));  ##Run all tests
    
    unittest.TextTestRunner(verbosity=2).run(suite)
    sys.exit(app.exec_())
    
    
    