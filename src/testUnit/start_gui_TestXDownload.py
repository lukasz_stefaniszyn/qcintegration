import os
import sys
os.chdir(r"D:\Eclipse_workspace\QC_gui\src")
from PyQt4 import QtCore, QtGui
from lib.QCTestX.qcTestX_download_chooseFields import QcTestXDownloadChooseFields as Choose
from lib.QCTestX.treeView.treeTestXModel import TreeView, TreeModel, TreeItem
import lib.QualityCenter_ext.lib.qc_connect_disconnect as qc_connect_disconnect


server = "http://qc:8080/qcbin/";
userName="alex_qc";
domainName="DEFAULT";
projectName="QualityCenter_Demo";
password = ""; ##Put the right password

qcConnect = qc_connect_disconnect.QC_connect(server, userName, password, domainName,projectName);
for result in qcConnect.connect():
    print result;
qc = qcConnect.qc;


app = QtGui.QApplication(sys.argv)
main= QtGui.QWidget()
gui = Choose(type="TestLab", parent=main, qcConnect=qcConnect)

#treeViewQcField = gui.qCTestXDownloadQcField.treeView
#treeViewUserField = gui.qCTestXDownloadUserField.treeView


#mappingFieldsQc = {
#                   "CYCLE": {'Test Set Name':('TS_NAME', 'char', 'True'),
#                             'Test Set Directory': ('TS_DIR', 'char', 'True'),
#                             'Test Set Description': ('TS_DESC', 'char', 'True')
#                             },
#                  "TESTCYCL": {'Test Case Name': ('TC_NAME', 'char', 'True'),
#                              'Test Case Directory': ('TC_DIR', 'char', 'True'),
#                              'Test Case Status': ('TC_STATUS', 'char', 'True'),
#                              },
#                  "STEP": {'Step Name': ('ST_NAME', 'char', 'True'),
#                           'Step Description': ('ST_DESC', 'char', 'True')
#                           }
#                  };
#gui.qCTestXDownloadQcField.updateModel(mappingFieldsQc);
gui.pushButton_QCTestXDownload_Refresh_clicked()

main.show()

print "Use variable=gui for QcTestXDownloadChooseFields"
print "Use variable=main for Main Window application"







