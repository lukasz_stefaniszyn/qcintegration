'''
Created on 11-01-2011

@author: wro50026
'''
import unittest
import sys

sys.path.append(r"d:\Work\svn_checkout\lukasz_repository_Fiona\QC_GUI\src\lib\QC_graph_ext\lib");
import makeFile



class TestMakeFile(unittest.TestCase):

    def __init__(self, testname):
        super(TestMakeFile, self).__init__(testname);

    def setUp(self):
        self.compareColNames = {};
        colNames = ['Design', 'Imported', 'Ready', 'Repair'];
        colNames.sort();
        self.colNames = colNames;
        
        i = 0
        for name in self.colNames:
            name = name.encode('ascii', 'replace');
            self.compareColNames.update({name:i});
            i += 1;

    def tearDown(self):
        pass


    def test_makeColumnNames(self):
        createDataTable = makeFile.CreateDataTable(self.colNames);
        createDataTable.colNames
        self.assertEqual(createDataTable.colNames, self.compareColNames);
        
    def test_getColName(self, colName="Design"):
        createDataTable = makeFile.CreateDataTable(self.colNames);
        
        colName, colNumber = createDataTable.getColName(colName);
        compareColName, compareColNumber = colName ,self.compareColNames[colName]
        print ((colName, colNumber), (compareColName, compareColNumber));
        self.assertEqual((colName, colNumber), (compareColName, compareColNumber));


        ##test when the column was not present yet. At the end this column should be added       
        colName="NotPresent"
        colName, colNumber = createDataTable.getColName(colName);
        print createDataTable.colNames;
        self.compareColNames.update({colName:len(self.colNames)});
        compareColName, compareColNumber = colName ,self.compareColNames[colName]
        self.assertEqual((colName, colNumber), (compareColName, compareColNumber));
        
    def test_addRow(self, rowName=None, colName=None):
        createDataTable = makeFile.CreateDataTable(self.colNames);
        
        ##add first Row to empty DataTable
        rowName="APRM without CCxP";
        colName="Design";
        createDataTable.addRow(rowName, colName)
        print createDataTable.data
        compareData = {rowName:{colName:1}};
        self.assertEqual(createDataTable.data, compareData);
        
        ##add second Row to not empty DataTable
        rowName="TC_new2";
        colName="Design";
        createDataTable.addRow(rowName, colName)
        print createDataTable.data
        compareData.update({rowName:{colName:1}});
        self.assertEqual(createDataTable.data, compareData);
        
        
        ##add third Row to not empty DataTable 
        rowName="TC_new3";
        colName="Imported";
        createDataTable.addRow(rowName, colName)
        print createDataTable.data
        compareData.update({rowName:{colName:1}});
        self.assertEqual(createDataTable.data, compareData);
                
        ##add fourth Row to not empty DataTable, new Column
        rowName="TC_new4";
        colName="NotPresent";
        createDataTable.addRow(rowName, colName)
        print createDataTable.data
        compareData.update({rowName:{colName:1}});
        self.assertEqual(createDataTable.data, compareData);
        
        ##add fifth Row , row Name already present
        rowName="TC_new4";
        colName="Design";
        createDataTable.addRow(rowName, colName)
        print createDataTable.data
        compareData.get(rowName).update({colName:1})
        self.assertEqual(createDataTable.data, compareData);
        
        ##add fifth Row , row Name already present, to sum Columns
        rowName="TC_new4";
        colName="Design";
        createDataTable.addRow(rowName, colName)
        print createDataTable.data
        compareData.get(rowName).update({colName:2})
        self.assertEqual(createDataTable.data, compareData);
        
        ##add fifth Row , row Name already present, to sum Columns
        rowName="TC_new4";
        colName="Design";
        createDataTable.addRow(rowName, colName)
        print createDataTable.data
        compareData.get(rowName).update({colName:3})
        self.assertEqual(createDataTable.data, compareData);


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
#    unittest.main()
    
    suite = unittest.TestSuite();
    suite.addTest(TestMakeFile("test_makeColumnNames"));
    suite.addTest(TestMakeFile("test_getColName"));
    suite.addTest(TestMakeFile("test_addRow"));
    
    testResult = unittest.TextTestRunner().run(suite)
    print "testResult:", testResult.wasSuccessful();