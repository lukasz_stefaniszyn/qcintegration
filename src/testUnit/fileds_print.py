


TSF = qc.TestSetFactory;

##Print TestSet to TestSet Execution
graph= TSF.BuildSummaryGraph(-1, "TC_CYCLE_NAME", "TC_STATUS", "", 0, TSF.Filter, False, False)

##Print Execution Tester to TestSet Execution 
graph = TSF.BuildSummaryGraph(-1, "TC_ACTUAL_TESTER", "TC_STATUS", "", 0, TSF.Filter, False, False)



def listFields(qc, name="ALL_LISTS"):
	f_fields =file("fields_%s.txt"%name, "wb");
	fieldList = qc.Fields(name);
	for aField in fieldList:
		FieldProp = aField.Property;
		f_fields.write("aFiled.Name: %s \n"%aField.Name);
		f_fields.write("FieldProperty.UserLabel: %s\n"%FieldProp.UserLabel);
		f_fields.write("FieldProperty.UserColumnType: %s\n"%FieldProp.UserColumnType); 
		f_fields.write("FieldProperty.DBColumnName: %s\n"%FieldProp.DBColumnName);
		f_fields.write("FieldProperty.DBColumnType: %s\n"%FieldProp.DBColumnType);
		f_fields.write(str("-"*60 + "\n"));
		f_fields.flush();
	print f_fields.name;
	f_fields.close();
	