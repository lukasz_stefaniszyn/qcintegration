from subprocess import Popen, PIPE
import os
import shutil


## Make ui_QCinter_mainWindow
print "Make ui_QCinter_mainWindow"

##copy to \src\lib\
make_py = Popen(r"c:\Python26\Lib\site-packages\PyQt4\uic\pyuic.py -o ui_QCinter_mainWindow.py QCinter_mainWindow.ui",shell=True , stdout=PIPE).stdout;
print make_py.read()
make_py.close();

##copy to \src\lib\QCgraph\
make_py = Popen(r"c:\Python26\Lib\site-packages\PyQt4\uic\pyuic.py -o ui_QCgraph_tabCrossTest.py QCgraph_tabCrossTest.ui",shell=True , stdout=PIPE).stdout;
print make_py.read()
make_py.close();

make_py = Popen(r"c:\Python26\Lib\site-packages\PyQt4\uic\pyuic.py -o ui_QCgraph_tabSpecificTest_TestLab.py QCgraph_tabSpecificTest_TestLab.ui",shell=True , stdout=PIPE).stdout;
print make_py.read()
make_py.close();

make_py = Popen(r"c:\Python26\Lib\site-packages\PyQt4\uic\pyuic.py -o ui_QCgraph_tabSpecificTest_TestPlan.py QCgraph_tabSpecificTest_TestPlan.ui",shell=True , stdout=PIPE).stdout;
print make_py.read()
make_py.close();

make_py = Popen(r"c:\Python26\Lib\site-packages\PyQt4\uic\pyuic.py -o ui_QCgraph_main.py QCgraph_main.ui",shell=True , stdout=PIPE).stdout;
print make_py.read()
make_py.close();

##copy to \src\lib\QCTestX\guiLib\
make_py = Popen(r"c:\Python26\Lib\site-packages\PyQt4\uic\pyuic.py -o ui_QCTestX_TestX_main.py QCTestX_TestX_main.ui",shell=True , stdout=PIPE).stdout;
print make_py.read()
make_py.close();

make_py = Popen(r"c:\Python26\Lib\site-packages\PyQt4\uic\pyuic.py -o ui_QCgraph_tabTestX_download.py QCgraph_tabTestX_download.ui",shell=True , stdout=PIPE).stdout;
print make_py.read()
make_py.close();

make_py = Popen(r"c:\Python26\Lib\site-packages\PyQt4\uic\pyuic.py -o ui_QCgraph_tabTestX_fields.py QCgraph_tabTestX_fields.ui",shell=True , stdout=PIPE).stdout;
print make_py.read()
make_py.close();

make_py = Popen(r"c:\Python26\Lib\site-packages\PyQt4\uic\pyuic.py -o ui_QCTestX_tabUpload.py QCTestX_tabUpload.ui",shell=True , stdout=PIPE).stdout;
print make_py.read()
make_py.close();

make_py = Popen(r"c:\Python26\Lib\site-packages\PyQt4\uic\pyuic.py -o ui_QCTestX_RunQc.py QCTestX_RunQc.ui",shell=True , stdout=PIPE).stdout;
print make_py.read()
make_py.close();


make_py = Popen(r"c:\Python26\Lib\site-packages\PyQt4\uic\pyuic.py -o ui_QCinter_license.py QCinter_license.ui",shell=True , stdout=PIPE).stdout;
print make_py.read()
make_py.close();


##Make resource file
print "Make resource_rc.py"
make_py = Popen(r"pyrcc4 -o resource_rc.py ..\Graphic_Icon\resource.qrc",shell=True , stdout=PIPE).stdout;
print make_py.read()
make_py.close();

##Copy created files to the Eclipse directory
eclipse_dirHome_2 = r"D:\Eclipse_workspace\QC_gui"
eclipse_dirHome = r"D:\Work\svn\QC_gui\trunk"
eclipse_dirWork = r"c:\tmp\svn\QC_gui\trunk"

if os.path.exists(eclipse_dirHome):
    dir = eclipse_dirHome;    
elif os.path.exists(eclipse_dirWork):
    dir = eclipse_dirWork;
elif os.path.exists(eclipse_dirHome_2):
    dir = eclipse_dirHome_2;
##copy to \src\lib\QCgraph\
shutil.copy(r"ui_QCgraph_main.py", r"%s\src\lib\QCgraph\ui_QCgraph_main.py"%dir);
shutil.copy(r"ui_QCgraph_tabCrossTest.py", r"%s\src\lib\QCgraph\ui_QCgraph_tabCrossTest.py"%dir);
shutil.copy(r"ui_QCgraph_tabSpecificTest_TestLab.py", r"%s\src\lib\QCgraph\ui_QCgraph_tabSpecificTest_TestLab.py"%dir);
shutil.copy(r"ui_QCgraph_tabSpecificTest_TestPlan.py", r"%s\src\lib\QCgraph\ui_QCgraph_tabSpecificTest_TestPlan.py"%dir);
shutil.copy(r"resource_rc.py", r"%s\src\lib\QCgraph\resource_rc.py"%dir);
##copy to \src\lib\QCTestX\guiLib\
shutil.copy(r"ui_QCgraph_main.py", r"%s\src\lib\QCTestX\guiLib\ui_QCTestX_main.py"%dir);
shutil.copy(r"ui_QCTestX_TestX_main.py", r"%s\src\lib\QCTestX\guiLib\ui_QCTestX_TestX_main.py"%dir);
shutil.copy(r"ui_QCgraph_tabTestX_download.py", r"%s\src\lib\QCTestX\guiLib\ui_QCgraph_tabTestX_download.py"%dir);
shutil.copy(r"ui_QCgraph_tabTestX_fields.py", r"%s\src\lib\QCTestX\guiLib\ui_QCgraph_tabTestX_fields.py"%dir);
shutil.copy(r"ui_QCTestX_tabUpload.py", r"%s\src\lib\QCTestX\guiLib\ui_QCgraph_tabTestX_upload.py"%dir);
shutil.copy(r"ui_QCTestX_RunQc.py", r"%s\src\lib\QCTestX\guiLib\ui_QCTestX_RunQc.py"%dir);
shutil.copy(r"resource_rc.py", r"%s\src\lib\QCTestX\guiLib\resource_rc.py"%dir);

##copy to \src\lib\
shutil.copy(r"resource_rc.py", r"%s\src\lib\resource_rc.py"%dir);
shutil.copy(r"ui_QCinter_mainWindow.py", r"%s\src\lib\ui_QCinter_mainWindow.py"%dir);
    

##copy to \src\lib\QClicense
shutil.copy(r"ui_QCinter_license.py ", r"%s\src\lib\QClicense\guiLib\ui_QCinter_license.py "%dir);


# os.remove(r".\edit_ui_QCinter_mainWindow.py");

