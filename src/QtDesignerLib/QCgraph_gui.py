'''
Created on 2010-03-15

@author: Lucas
'''

from PyQt4 import QtCore, QtGui
import win32com.client
import pywintypes

import file_viewer







class ExcelApp(object):
    '''
    Excel operation
    '''    

    #------------------------------------------------------------------------------ 
    def __init__(self):
        try:
            self.excel_object = win32com.client.Dispatch("Excel.Application");
        except pywintypes.com_error:
            print "ERR: Unable to run Microsoft Excel program. \n Please install first Microsoft Excel"
            self.excel_object = None;
    
    def __del__(self):
        del self.__excel_object;
        
    #------------------------------------------------------------------------------
    def __get_excel_object(self):
        return self.__excel_object;
    def __set_excel_object(self, excel_object):
        self.__excel_object = excel_object;
    def __del_excel_object(self):
        del self.__excel_object;
    excel_object = property(__get_excel_object, 
                            __set_excel_object, 
                            __del_excel_object, 
                            "This is an Excel Object Application");
    #------------------------------------------------------------------------------
        
    def openFile(self, path):
        print "Open Excel file:", path;
        self.excel_object.Workbooks.Open(path, ReadOnly=False); 
        self.excel_object.Visible= True;
        
        
        
class FileViewer(file_viewer.MainWindow):
    '''
    File_viewer operation. By this open/save any file
    '''        
    
    def __init__(self):
        super(FileViewer, self).__init__();
        #self.Scribble_window = scribble.MainWindow()
        
        
    def openFile(self, path):
        print "Open Any file under scribble:", path;
        path = QtCore.QString(path);
        self.open(path)
        self.show();
        
        
        
        

class MyTreeView(QtGui.QTreeView):
    '''
    SubClass of QtGui.QTreeView
    '''
    
    def __init__(self, parent=None):
        super(MyTreeView, self).__init__(parent)
        self.DirModel = MyDirModel(parent);
        self.ExcelApp = ExcelApp();
        self.FileViewer = FileViewer(); 
                
        self.setModel(self.DirModel) # we have to set the listView to use this DirModel
        dir = QtCore.QString(r".\Result\QCgraph");
        self.setRootIndex(self.DirModel.index(dir)) # set the default to load
        
        self._setView(); #set View on the catalog, width, size, etc; 

        self.connect(self, QtCore.SIGNAL("doubleClicked(QModelIndex)"), self._DoubleClicked);

    def getModelItemCollection(self):
        '''
        From this list get the QModelIndex that we set with .setRootIndex
        Using QModelIndex and DirModel we can get all the elements at that Index
        '''
        ModelIndex = self.rootIndex()
        ModelIndexCollection = []
        for i in range(0, self.DirModel.rowCount(ModelIndex)):
            ModelIndexCollection.append(ModelIndex.child(i,0))
        return ModelIndexCollection
     
    def _DoubleClicked(self):
        if not self.DirModel.isDir(self.currentIndex()):
            path = self.DirModel.filePath(self.currentIndex());
            print "File path:", path; 
            ext = str(path).rsplit(".", 1)[1];
            self.openExtFile(ext, path)
            
    def _setView(self):
        
        #set width for column "Name" 
        self.setColumnWidth(0, 320);
        #set width for column "Size"
        self.setColumnWidth(1, 60);
        #set width for column "Type"
        self.resizeColumnToContents(2);
    
    def openExtFile(self, ext, path):
        '''
        Open file based on the extention
        '''
        print "ext:", ext;
        if (ext == "xls") or (ext == "xlsx"):
            if self.ExcelApp.excel_object:
                self.ExcelApp.openFile(path);
            else:
                self.FileViewer.openFile(path);
        elif ext == "txt":
            self.FileViewer.openFile(path);

        
        
class MyDirModel(QtGui.QDirModel):
    '''
    SubClass of QtGui.QDirModel
    '''

    def __init__(self, parent=None):
        super(MyDirModel, self).__init__(parent)
                
    def getData(self, ModelIndex):
        '''
        Using QModelIndex I can get data via the data() method or
        via any other method that support a QModelIndex
        '''
        paths = []
        if isinstance(ModelIndex, list):
            for items in ModelIndex:
                #print self.data(items).toString()
                paths.append(self.filePath(items))
            return paths
        else:
            raise ValueError("getData() requires a list (QtGui.QModelIndexs)")     



class SetQCgraph(MyTreeView, MyDirModel):
    '''
    Set values for QCgraph tab
    '''


    def __init__(self, MainWindow):
        '''
        Constructor
        '''
        
        super(SetQCgraph, self).__init__()
        self.__MainWindow = MainWindow;
        self.treeView = MainWindow.treeView; 
        #self.QObject_CrossTest_DirResultView.show();
        
        #self.__qcConnect = qcConnect;
        self.crossTest_DirResultView();
        
        self.dict_GraphType = {};
        
        
        

        
    #-----------------------------------------------------------------------------        
    def __get_MainWindow(self):
        return self.__MainWindow;
    mainWindow = property(__get_MainWindow, None, None, "Instance of MainWindow")
    
    def __get_qcConnect(self):
        return self.__qcConnect.qc;
    def __set_qcConnect(self, qcConnect):
        self.__qcConnect = qcConnect;
    qc = property(__get_qcConnect, __set_qcConnect, None, "Instance of QC connection");    
    #----------------------------------------------------------------------------- 
    
    def set_comboBox_Xaxis(self, AxisList):
        comboBox_Xaxis = self.mainWindow.comboBox_Xaxis;
        comboBox_Xaxis.clear();
        comboBox_Xaxis.addItems(QtCore.QStringList(AxisList));
        
    def set_comboBox_Yaxis(self, AxisList):
        comboBox_Yaxis = self.mainWindow.comboBox_Yaxis;
        comboBox_Yaxis.clear();
        comboBox_Yaxis.addItems(QtCore.QStringList(AxisList));
        
    
    def update_comboBox_items(self, GraphType):
        
        print "GraphType:", GraphType;
        if not self.dict_GraphType:
            self.mainWindow.comboBox_Xaxis.clear();
            self.mainWindow.comboBox_Yaxis.clear();
            self.set_comboBox_Xaxis(['None']);
            self.set_comboBox_Yaxis(['None']);
            return False;
        
        if "Summary" in GraphType:
            Xaxis = self.dict_GraphType.get("Summary")[0].values();
            self.set_comboBox_Xaxis(Xaxis);
            Yaxis = self.dict_GraphType.get("Summary")[1].values();
            self.set_comboBox_Yaxis(Yaxis);
        elif "Progress" in GraphType:
            Xaxis = self.dict_GraphType.get("Progress")[0].values();
            self.set_comboBox_Xaxis(Xaxis);
            Yaxis = self.dict_GraphType.get("Progress")[1].values();
            self.set_comboBox_Yaxis(Yaxis);
        else:
            print "Unknown GraphType";
            return False
            
    
    def create_map_comboBox(self):
        """Creates all possible items used in Axis tabWidget
        output=  {"Summary": (dict{Xaxis}, dict(Yaxi)), "Progress":(dict{Xaxis}, dict(Yaxi))}
        """
        axisFields = self.get_AxisFields();
        for GraphType in ["Summary", "Progress"]:
            if "Summary" is GraphType:
                Xaxis, Yaxis = axisFields, axisFields;
            elif "Progress" is GraphType:
                Yaxis={};
                Yaxis.update({"CY_STATUS":"Status"});
                Yaxis.update({"CY_ASSIGN_RCYC ":"Target Cycle"});
                Xaxis=axisFields;
            else:
                print "Unknow GraphType"
                return False;
            self.dict_GraphType[GraphType]= (Xaxis, Yaxis);
        print "self.dict_GraphType:", self.dict_GraphType;
        

    def get_AxisFields(self):
        """Get Fields names used for generating Graph
        Make mapping of DataBaseName to UserName field
        {'TC_CYCLE':'Test Set'} 
        """
        
        mapping_QCGraph = {};
        
        for name in ["TESTCYCL", "CYCLE"]:
            fieldList = self.qc.Fields(name);
            for aField in fieldList:
                FieldProp = aField.Property;
                filedName = aField.Name;
                filedUserLabel=FieldProp.UserLabel;
                filedIsActive=FieldProp.IsActive;
                filedIsCanFilter=FieldProp.IsCanFilter;
                if filedIsActive and filedIsCanFilter: 
                    mapping_QCGraph[str(filedName)]= str(filedUserLabel);
                    
        return mapping_QCGraph;
                
    def crossTest_DirResultView(self):
        '''
        Create directory view from Result of QCgraph
        '''

        tree = MyTreeView(self.treeView)

        width = self.treeView.width();
        height = self.treeView.height();
        tree.resize(width, height);  ##set size as the Tab size
        tree.show();


        
    
        



    
