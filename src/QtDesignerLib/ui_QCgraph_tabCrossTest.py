# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'QCgraph_tabCrossTest.ui'
#
# Created: Sat Dec 08 18:02:32 2012
#      by: PyQt4 UI code generator 4.9.1
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName(_fromUtf8("Form"))
        Form.setEnabled(False)
        Form.resize(846, 538)
        self.gridLayout = QtGui.QGridLayout(Form)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.gridLayout_17 = QtGui.QGridLayout()
        self.gridLayout_17.setSizeConstraint(QtGui.QLayout.SetMaximumSize)
        self.gridLayout_17.setObjectName(_fromUtf8("gridLayout_17"))
        spacerItem = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.gridLayout_17.addItem(spacerItem, 1, 0, 1, 1)
        self.groupBox_4 = QtGui.QGroupBox(Form)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.MinimumExpanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.groupBox_4.sizePolicy().hasHeightForWidth())
        self.groupBox_4.setSizePolicy(sizePolicy)
        self.groupBox_4.setMinimumSize(QtCore.QSize(0, 400))
        self.groupBox_4.setObjectName(_fromUtf8("groupBox_4"))
        self.verticalLayout_10 = QtGui.QVBoxLayout(self.groupBox_4)
        self.verticalLayout_10.setObjectName(_fromUtf8("verticalLayout_10"))
        self.treeView_QCgraphCrossTest = QtGui.QTreeView(self.groupBox_4)
        self.treeView_QCgraphCrossTest.setObjectName(_fromUtf8("treeView_QCgraphCrossTest"))
        self.verticalLayout_10.addWidget(self.treeView_QCgraphCrossTest)
        self.gridLayout_17.addWidget(self.groupBox_4, 0, 0, 1, 1)
        self.gridLayout.addLayout(self.gridLayout_17, 0, 0, 1, 1)

        self.retranslateUi(Form)
        QtCore.QObject.connect(self.treeView_QCgraphCrossTest, QtCore.SIGNAL(_fromUtf8("doubleClicked(QModelIndex)")), self.treeView_QCgraphCrossTest.update)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        Form.setWindowTitle(QtGui.QApplication.translate("Form", "Form", None, QtGui.QApplication.UnicodeUTF8))
        self.groupBox_4.setTitle(QtGui.QApplication.translate("Form", "Result", None, QtGui.QApplication.UnicodeUTF8))

import resource_rc
