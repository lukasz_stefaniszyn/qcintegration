# -*- coding: utf-8 -*-
#!/usr/bin/env python

'''
Created on 2009-09-30

@author: Lukasz Stefaniszyn
'''
from copy import deepcopy
from lib.QCTestX.field_file_operation import OpenFile
from lib.QCTestX.qcTestX_download_chooseFields import QcTestXCommonChooseFields
from lib.QClicense.checkLicence import DataStartupLicenceCheck
from lib.crypt_pass import Heslo
from lib.logfile import Logger
import lib.QCTestX.field_file_operation as  field_file_operation
import lib.QualityCenter_ext.lib.TestLab_download as TestLab_download
import lib.QualityCenter_ext.lib.TestLab_upload as TestLab_upload
import lib.QualityCenter_ext.lib.TestPlan_download as TestPlan_download
import lib.QualityCenter_ext.lib.TestPlan_upload as TestPlan_upload
import lib.QualityCenter_ext.lib.qc_connect_disconnect as qc_connect_disconnect

from lib.QCTestX.qCTestX_download import GROUPNAMETRANSLATE

import os.path
import sys



from optparse import OptionParser;



log = Logger(loggername="QCinterCMD", resetefilelog=False).log;







class VerifyArguments(object):
    '''
    Class used for verify if given arguments are OK for other components
    '''
    
    
    def __init__(self, parser):
        self.parser = parser;
        self.outputData = {};##this will be output arguments. 
        
    def ver_testplanUpload(self, args):
        '''
        Verify if given sys.argv arguments are in good shape for TestPlan Upload
        *Input*
        :param args:
        
        *Output*:
        True/False
        '''
        filename = '';
        if len(args)==0 or len(args)>1: ##wrong number of arguments
            text =  "Please input arguments <FilenameToUpload.csv>"\
                    "\nExample: %s <options> --TestPlanUpload <FilenameToUpload.csv>"%(self.parser.get_prog_name());
            print(text);
            return False
        else:
            ##Add working directory to Excel file if only file as input arguments
            try: filename = os.path.abspath(args[0]);
            except Exception, err:
                print err;
                return False
        
        if not TestPlan_upload.checkFile(filename):
            return False
        
        ##If all point will be verified, then retur as True
        self.outputData = {'filename':filename};
        return True

    
    def ver_testplanDownload(self, args):
        testCaseDir = None;
        testCaseName = None;
        
        text = "Example: %s <options> --TestPlanDownload [TestCases_directory] [TestCase_name]\n"\
                    "\n"\
                    "  TestCase directory - this is full path to the TestCases, empty means all directories under Subject\\. "\
                    "Example: Subject\\Catalog\\Catalog1_1\n"\
                    "  TestCase name - this is TestCase name from TestCase directory. "\
                    "Example: TestCaseName\n\n"%(self.parser.get_prog_name());
        
        if len(args)>2:
            print(text);
            return False
        if len(args)==0:
            pass ##User will download all folders and TestCases
        elif len(args)==1:
            testCaseDir = args[0];
        else:
            testCaseDir = args[0];
            testCaseName = args[1];

        self.outputData = {'testCaseDir':testCaseDir,
                           'testCaseName': testCaseName};
        return True

    
    def ver_testlabUpload(self, args):
        '''
        Verify if given sys.argv arguments are in good shape for TestLab Upload
        *Input*
        :param args:
        
        *Output*:
        True/False
        '''
        filename = '';
        if len(args)==0 or len(args)>1: ##wrong number of arguments
            text =  "Please input arguments <FilenameToUpload.csv>"\
                    "\nExample: %s <options> --TestLabUpload <FilenameToUpload.csv>"%(self.parser.get_prog_name());
            print(text);
            return False
        else:
            ##Add working directory to Excel file if only file as input arguments
            try: filename = os.path.abspath(args[0]);
            except Exception, err:
                print err;
                return False
        
        if not TestLab_upload.checkFile(filename):
            return False
        
        ##If all point will be verified, then return as True
        self.outputData = {'filename':filename};
        return True

    
    def ver_testlabDownload(self, args):
        testSetDir = None;
        testSetName = None;

        text = "Example: %s <options> --TestLabDownload [TestSets_directory] [TestSet_name]\n"\
                "\n"\
                "  TestSets directory - this is full path to TestSets, empty means all directories under Root\\. "\
                "Example: Root\\Catalog\\Catalog1_1\n"\
                "  TestSet name - this is TestSet name from TestSet directory. "\
                "Example: TestSetName\n\n"%(self.parser.get_prog_name());
        
        if len(args)>2:
            print(text);
            return False
        elif len(args)==0:
            pass; ##User will download all folders and TestCases 
        elif len(args)==1:
            testSetDir = args[0];
        else:
            testSetDir = args[0];
            testSetName = args[1];

        self.outputData = {'testSetDir':testSetDir,
                           'testSetName': testSetName};
        return True
    

class ReadArguments(VerifyArguments):
    
    
    usage_text = "usage: %prog <options>";

    

    
    def __init__(self):
        self.parser = OptionParser(self.usage_text);

        self.outInf = {'testplanUpload': (False, {'filename':None}),
                      'testplanDownload': (False, {'testCaseDir':None, 'testCaseName':None}),
                      'testlabUpload': (False, {'filename':None}),
                      'testlabDownload': (False, {'testSetDir':None, 'testSetName':None}),
                      }; ##this is output Data filled by reading arguments. False status will give information if given data has been changed
        self.username= '';
        self.password="";
        self.server = "";
        self.domainName = "";
        self.projectName = "";
        self.parameterFile = None;
        self.mappingFieldName = None;
    
    
    
    def get_output(self):
        ##this will return only changed values in Type
        for key, values in self.outInf.items():
            if values[0]: 
                return key, values[1];
        return None;
    def set_output(self, value):
        try: typeName, valueDict = value;
        except Exception, err: 
            print err;
            return
        if self.outInf.has_key(typeName):
            self.outInf[typeName] = (True, valueDict);
        else: print("ERR: unknown typeName in parsing arguments");
    output = property(get_output, set_output, None, "output's docstring")
    
    def arguments_setup(self):
        self.parser.add_option("", "--TestPlanUpload", 
                          action="store_true", 
                          dest="testplanUpload",
                          help = "Connect to QualityCenter 'Reg U.S. Pat & TM Off.' and update TestPlan with given data from local file",
                          default = False);
        self.parser.add_option("", "--TestPlanDownload", 
                          dest="testplanDownload",
                          action="store_true",
                          help = "Connect to QualityCenter 'Reg U.S. Pat & TM Off.' and download TestPlan to local disk",
                          default = False);
        self.parser.add_option("", "--TestLabUpload", 
                          dest="testlabUpload",
                          action="store_true",
                          help = "Connect to QualityCenter 'Reg U.S. Pat & TM Off.' and update TestLab with given data from local file",
                            default = False);
        self.parser.add_option("", "--TestLabDownload", 
                          dest="testlabDownload",
                          action="store_true",
                          help = "Connect to QualityCenter 'Reg U.S. Pat & TM Off.' and download TestLab to local disk",
                          default = False);
        self.parser.add_option("-m", "--mappingFieldName", 
                          dest="mappingFieldName",
                          help = "This is mapping of user fields with fields in QualityCenter 'Reg U.S. Pat & TM Off.'. User put here name of mappingField created in GUI version of QCinter",
                          default=None);                  
        self.parser.add_option("-s", "--server", dest="server",
                           help = "input server http address, the same as it is used in browser, Example: http://yourQClink.net/qcbin/",
                           default = self.server);
        self.parser.add_option("-u", "--user", dest="username",
                          help = "input user name used in QualityCenter 'Reg U.S. Pat & TM Off.'",
                          default = self.username);
        self.parser.add_option("-p", '--pass', dest = "password",
                          help = "input password used in QualityCenter 'Reg U.S. Pat & TM Off.'",
                          default = "");
        self.parser.add_option("-d", "--domain", dest = "domainName",
                           help = "input Domain name used in QualityCenter 'Reg U.S. Pat & TM Off.'",
                           default = self.domainName);
        self.parser.add_option("-r", "--project", dest = "projectName",
                           help = "input Project name used in QualityCenter 'Reg U.S. Pat & TM Off.'",
                           default = self.projectName);
        self.parser.add_option("-f", "--file", dest = "parameterFile", 
                          help = "parameter filename with UserName, DomainName, ProjectName",
                          default = '');
    
        (self.options, self.args) = self.parser.parse_args()
        
    
    def read_parameterFile(self):
        import os.path
        import re
        
#        ##There is need to make blank server, UserName, etc. to have possible overwrite by option.UserName, etc.
#        server = '';
#        UserName = '';
#        Password = '';
#        DomainName = '';
#        ProjectName = '';
        
        parameterFile = self.options.parameterFile;
        if os.path.exists(parameterFile):
            fo_parameterFile = file(parameterFile, 'rb');
            for line in fo_parameterFile.readlines():
                #print line;
                if re.compile('^[Ss]erver[\s]*:').match(line):
                    server = line.split(':',1)[1].strip();
                    self.server = server.rstrip("start_a.htm");
                    continue;
                elif re.compile('^UserName[\s]*:').match(line):
                    self.username = line.split(':',1)[1].strip();
                    continue;
                elif re.compile('^Password[\s]*:').match(line):
                    Password = line.split(':',1)[1].strip();
                    try: 
                        int(Password, 16);
                        self.password = Heslo(Password)._Heslo__readHeslo();
                        continue;
                    except ValueError:
                        continue;
                elif re.compile('^DomainName[\s]*:').match(line):
                    self.domainName = line.split(':',1)[1].strip();
                    continue;
                elif re.compile('^ProjectName[\s]*:').match(line):
                    self.projectName = line.split(':',1)[1].strip();
                    continue;
    
    def arguments_operation(self):
        if (self.options.parameterFile or self.options.server) == "":
                self.parser.error("Please use --file or --user with --server, --domain and --project");
        
            
        if self.options.parameterFile !='':
            self.read_parameterFile();
            
        ##if there was no password, then ask ask for password
        if self.options.password == '' and self.password =="":
            import getpass;
            self.password = getpass.getpass('Password for %s: '%self.options.username);
            
        verarg = VerifyArguments(self.parser);
        if (self.options.testplanUpload):
            if not verarg.ver_testplanUpload(self.args):
                self.parser.exit(1);
            else: 
#                filename = verarg.outputData['filename'];
                savingType='UL';
                qcType='TestPlan';
                self.output = ('testplanUpload', verarg.outputData); 
        elif self.options.testplanDownload:
            if not verarg.ver_testplanDownload(self.args):
                self.parser.exit(1);
            else:
#                testCaseDir = verarg.outputData['testCaseDir'];
#                testCaseName = verarg.outputData['testCaseName'];
                savingType='DL';
                qcType='TestPlan';
                self.output = ('testplanDownload', verarg.outputData);
        elif self.options.testlabUpload:
            if not verarg.ver_testlabUpload(self.args):
                self.parser.exit(1);
            else: 
#                filename = verarg.outputData['filename'];
                savingType='UL';
                qcType='TestLab';
                self.output = ('testlabUpload', verarg.outputData);
        elif self.options.testlabDownload:
            if not verarg.ver_testlabDownload(self.args):
                self.parser.exit(1);
            else:
#                testSetDir = verarg.outputData['testSetDir'];
#                testSetName = verarg.outputData['testSetName'];
                savingType='DL';
                qcType='TestLab';
                self.output = ('testlabDownload', verarg.outputData);
        else:
            self.parser.error("Declare which type of service will be used: --TestPlanDownload or --TestPlanUpload or --TestLabDownload or --TestLabUpload.\n Example: %s <options> --TestPlanUpload <FilenameToUpload.csv>"%(self.parser.get_prog_name()))


        if self.options.mappingFieldName is None:
            self.parser.exit(1, "Please use --mappingFieldName parameter");
    
        if self.output is not None:
            try: 
                openXmlFile = OpenFile(type=savingType, qcType=qcType);
                openXmlFile.get_mappingfields(self.options.mappingFieldName, savingType);
            except Exception, err:
                print err;
                self.parser.exit(1)
            else:
                self.mappingFieldName = self.options.mappingFieldName;
    

class CheckLicense(DataStartupLicenceCheck):
    
    def __init__(self, datetimeNow=None):
        DataStartupLicenceCheck.__init__(self, datetimeNow=datetimeNow)
        self.runLicense();

    def get_licensestatus(self):
        return self.status
    licensestatus = property(get_licensestatus, None, None, "License Status's ")
    
    def runLicense(self):
        ##Check license 
        for message in self.dataStartupCheck(cmd=True):
            pass; 


class MappingField(QcTestXCommonChooseFields):
    '''
    This is class for all MappingField operation, starting from reading fields in QC and also reading those Fields from saved XML files
    '''
    
    def  __init__(self, qc, savingType, qcType):
        self.qc = qc
        self.savingType = savingType
        self.qcType = qcType
        
        
        self.result = None ##Here is result of mappingField after final operations
        
        
        QcTestXCommonChooseFields.__init__(self, savingType, qcType);
        
    
    def translate_getFields(self, qcType, mappingFields):
    
        newmappingFields = {};
        for fieldTreeGroupName, fieldQCGroupName in GROUPNAMETRANSLATE.get(qcType).items():
            newmappingFields[fieldTreeGroupName] = mappingFields.get(fieldQCGroupName);
            
        return newmappingFields;
    

    def get_mappingFieldXml(self, settingName):
        ##Now open xml file with setting name "Setting_2" and get mapping of this setting
        openXmlFile = field_file_operation.OpenFile(self.savingType, self.qcType)
        mappingFieldsXML = openXmlFile.get_mappingfields(settingName, self.savingType)
#        mappingFieldsXML= self.translate_mappingFields(mappingFieldsXML, self.savingType)
        log.debug(("mappingFieldsXML:", mappingFieldsXML))
        return mappingFieldsXML


    def get_mappingFieldQC(self):
        mappingFieldsQc = self.get_fields()
        log.debug(("mappingFieldsQc:", mappingFieldsQc))
    ##Translate dict() to have fieldQcName instead of fieldUserName as key()
        mappingFieldsQc = self.translate_getFields(self.qcType, mappingFieldsQc)
#        mappingFieldsQc = self.translate_mappingFields(mappingFieldsQc, self.savingType)
        log.debug(("mappingFieldsQc:", mappingFieldsQc))
        return mappingFieldsQc

    def openFieldFile(self, settingName):
        '''
        Open Fields for user selected fields, saved in XML file
        
        *Input*
        :param settingName:str(), this is information of saved settingName in XML file
        
        *Output*
        :self   self.result: dict() of verified Fields, which will be used later
                {
                'TestCase':{
                    'Designer': {'FieldIsRequired': 'False', 
                                'FieldOrder': '1', 
                                'FieldType': 'char', 
                                'FieldQcName': 'TS_RESPONSIBLE'}, 
                    'Test Name': {'FieldIsRequired': 'True', 
                                'FieldOrder': '0', 
                                'FieldType': 'char', 
                                'FieldQcName': 'TS_NAME'}
                                }, 
                'Step': {
                    'Step Name': {'FieldIsRequired': 'False', 
                                'FieldOrder': '0', 
                                'FieldType': 'char', 
                                'FieldQcName': 'DS_STEP_NAME'}}, 
                'TestSet': {
                    'Status': {'FieldIsRequired': 'False', 
                                'FieldOrder': '0', 
                                'FieldType': 'char', 
                                'FieldQcName': 'CY_STATUS'}, 
                    'Test': {'FieldIsRequired': 'False', 
                            'FieldOrder': '1', 
                            'FieldType': 'char', 
                            'FieldQcName': 'CY_USER_01'}}
                }
        
        
        '''
        
        
        
        ##Here I will have to make additional Widget to get SettingName
        if settingName == "":
            text = "Open settings file. Please input 'Setting name' to open";
            log.info(text);
            return
        
        try: 
            field_file_operation.OpenFile(self.savingType, self.qcType);
        except (field_file_operation.ParserException, 
                field_file_operation.OpenFileException,
                field_file_operation.CRCexception),\
                text:
            log.info(("Open settings file:", text));
            raise Exception("Unable to open file")
            return
         
        mappingFieldsXML = self.get_mappingFieldXml(settingName) 
        
        mappingFieldsQc = self.get_mappingFieldQC() 

        ##Here compare if all Fields from open XML Setting (mappingFieldsXML) are possible to run in QualityCenter (mappingFieldsQc)
        ##When Field from XML is possible to run in QC, then update the dict() "mappingFieldsXML_checked"
        mappingFieldsXML_checked = {};
        if self.savingType=="DL":
            for fieldGroup, fieldValue in mappingFieldsXML.items():
                if mappingFieldsQc.has_key(fieldGroup):
                    mappingFieldsXML_checked.update({fieldGroup:{}});#
                    for fieldName, fieldValues in fieldValue.items():
                        if mappingFieldsQc[fieldGroup].has_key(fieldName):
                            fieldType = fieldValues.get("FieldType");
                            fieldIsRequired = fieldValues.get("FieldIsRequired");
                            fieldOrder = fieldValues.get("FieldOrder");
                            fieldQcName = fieldValues.get('FieldQcName');
                            fieldValues = {'FieldQcName':fieldQcName, 
                                           'FieldType':fieldType, 
                                           'FieldIsRequired':fieldIsRequired, 
                                           'FieldOrder':fieldOrder};
                            ##Change place of fieldUserName and fieldName
                            mappingFieldsXML_checked[fieldGroup].update({fieldName: fieldValues});
                        else:
                            print "Given fieldName: %s is not in mappingFieldsQc"%(fieldQcName);
            log.debug(("mappingFieldsXML_checked:", mappingFieldsXML_checked))
            ##Update TreeView_UserFiled  with the given mapping from xml file
            self.result = mappingFieldsXML_checked; 
        elif self.savingType=="UL":
            #==================================================================
            # ##You will have FieldUserName:{FieldQcName, FieldName} not FieldName:{FieldQcName, FieldUserName} This is because TreeView of user Fields will have empty FieldNames. During opening of Settings from xml file program is checking by FieldUserName instead FieldName
            #==================================================================
            
            for fieldGroup, fieldValues in mappingFieldsXML.items():
                if mappingFieldsQc.has_key(fieldGroup):
                    mappingFieldsXML_checked.update({fieldGroup:{}});#
                    for fieldName, fieldValue in fieldValues.iteritems():
                        try: fieldQcName = fieldValue.get('FieldQcName');##used did not mapped QCField with UserField
                        except: 
#                            if qcFieldName is "":##used did not mapped QCField with UserField
                            fieldType = '';
                            fieldIsRequired= '';
                            fieldUserName = '';
                            fieldQcName = ''; 
                            fieldOrder = fieldValue.get("FieldOrder");
                        if mappingFieldsQc[fieldGroup].has_key(fieldName):
                            fieldUserName = fieldValue.get("FieldUserName");
                            fieldType = fieldValue.get("FieldType");
                            fieldIsRequired = fieldValue.get("FieldIsRequired");
                            fieldOrder = fieldValue.get("FieldOrder");
                            fieldQcName = fieldQcName;
                        else:
                            if mappingFieldsQc[fieldGroup].has_key(fieldName):
                                fieldUserName = fieldValue.get("FieldUserName");
                                fieldType = fieldValue.get("FieldType");
                                fieldIsRequired = fieldValue.get("FieldIsRequired");
                                fieldOrder = fieldValue.get("FieldOrder");
                                fieldQcName = fieldQcName;
                            else:
                                print "Given fieldQcName: %s is not in mappingFieldsQc"%(fieldQcName)
                                fieldType = '';
                                fieldIsRequired= '';
                                fieldUserName = '';
                                fieldQcName = ''; 
                                fieldOrder = fieldValue.get("FieldOrder");
                            ##Change place of fieldUserName and fieldName
                        fieldValue = {'FieldOrder':fieldOrder,   
                                      'FieldQcName':fieldQcName,
                                      'FieldUserName':fieldUserName,
                                      'FieldType':fieldType,
                                      'FieldIsRequired':fieldIsRequired
                                      };
                        mappingFieldsXML_checked[fieldGroup].update({fieldName: fieldValue});
            log.debug(("mappingFieldsXML_checked:", mappingFieldsXML_checked))
            ##now make correlation of mappingFieldsXML_checked and mappingFieldsUser. Because user would like add some more QCField to not linked fields ans save this under new name
#            mappingFieldsJoin = self.joinDict(mappingFieldsXML_checked, mappingFieldsUser);
            ##Update TreeView_UserFiled  with the given mapping from xml file
            mappingFieldsXML_checked
            self.result = mappingFieldsXML_checked;
    

class MainExecution(MappingField):
    
    def __init__(self, readArguments):
        self.mappingFieldName = readArguments.mappingFieldName
        
        print "\n";
        print "#"*60;
        print "Server:", repr(readArguments.server);
        print "UserName:", repr(readArguments.username);
        print "DomainName:", repr(readArguments.domainName);
        print "ProjectName:", repr(readArguments.projectName);
        print "MappingFieldName:", repr(readArguments.mappingFieldName)
        print "#"*60;
        print "\n";
        
        ##Get already checked arguments 
        self.typeName, self.args = readArguments.output
        
        
        self.qcConnect = qc_connect_disconnect.QC_connect(readArguments.server, 
                                                         readArguments.username, 
                                                         readArguments.password, 
                                                         readArguments.domainName, 
                                                         readArguments.projectName);
        for connectionstatus, msg in self.qcConnect.connect():
            print msg 
#            print("Connected to QC");
        
        
        if not connectionstatus:
            print("Not connected to QC server, exiting");
            try: del self.qcConnect;
            except: pass;
            finally: sys.exit(1);

    def __del__(self):
        try:self.qcConnect.disconnect_QC();
        except: pass;
        finally:    
            print "\nDisconnecting from QC server."\
                    "\n"\
                    "\nThank You for using QCinter.\n"\
                    "\tSoftware Automation www.QCinter.com\n"
        try: sys.exit(0);
        except: pass;
    
    
    def main(self):
        if self.typeName == "testplanUpload":
            self.start_testplanupload();
        elif self.typeName == "testplanDownload":
            self.start_testplandownload();
        elif self.typeName == "testlabUpload":
            self.start_testlabupload();
        elif self.typeName == "testlabDownload":
            self.start_testlabdownload();
    
    def start_testplanupload(self):
        try: filename = self.args['filename'];
        except: filename=None;
        
        print "\n";
        print "#"*60;
        print "Filename:",filename 
        print "#"*60;
        print "\n";

        mappingFieldObj = MappingField(qc=self.qcConnect.qc, 
                                     savingType="UL", 
                                     qcType="TestPlan"
                                     );
        mappingFieldObj.openFieldFile(settingName=self.mappingFieldName);
#        try:
#            mappingFieldObj.openFieldFile(settingName=self.mappingFieldName);
#        except Exception, err:
#            print err
#            return
        
        TestPlan_upload.MainExecution(cmd=True, 
                                      qc = self.qcConnect.qc, 
                                      mappingFields=mappingFieldObj.result, 
                                      filename=filename)

    def start_testplandownload(self):
        try: testCaseDir = self.args['testCaseDir'];
        except: testCaseDir=None;
        try: testCaseName = self.args['testCaseName'];
        except: testCaseName = None;
    
        print "\n";
        print "#"*60;
        print "TestCaseDir:",repr(testCaseDir);
        print "TestCaseName:", repr(testCaseName);
        print "#"*60;
        print "\n";
        
        mappingFieldObj = MappingField(qc=self.qcConnect.qc, 
                                     savingType="DL", 
                                     qcType="TestPlan"
                                     );
        try:
            mappingFieldObj.openFieldFile(settingName=self.mappingFieldName);
        except Exception, err:
            print err
            return
        
        TestPlan_download.MainExecution(cmd=True, 
                                        qc= self.qcConnect.qc, 
                                        testCaseDir=testCaseDir, 
                                        testCaseName=testCaseName,
                                        mappingFields=mappingFieldObj.result
                                        );
    
    def start_testlabupload(self):
        try: filename = self.args['filename'];
        except: filename=None;
        
        print "\n";
        print "#"*60;
        print "Filename:",filename 
        print "#"*60;
        print "\n"; 
        
        mappingFieldObj = MappingField(qc=self.qcConnect.qc, 
                                     savingType="UL", 
                                     qcType="TestLab"
                                     );
        try:
            mappingFieldObj.openFieldFile(settingName=self.mappingFieldName);
        except Exception, err:
            print err
            return
        
        TestLab_upload.MainExecution(cmd=True, 
                                      qc = self.qcConnect.qc, 
                                      mappingFields=mappingFieldObj.result, 
                                      filename=filename);
                                      
    def start_testlabdownload(self):
        try: testSetDir = self.args['testSetDir'];
        except: testSetDir=None;
        try: testSetName = self.args['testSetName'];
        except: testSetName = None;

        print "\n";
        print "#"*60;
        print "TestSetDir:",repr(testSetDir);
        print "TestSetName:", repr(testSetName);
        print "#"*60;
        print "\n";
        
        mappingFieldObj = MappingField(qc=self.qcConnect.qc, 
                                     savingType="DL", 
                                     qcType="TestLab"
                                     );
        try:
            mappingFieldObj.openFieldFile(settingName=self.mappingFieldName);
        except Exception, err:
            print err
            return
        
        TestLab_download.MainExecution(cmd=True, 
                                       qc=self.qcConnect.qc, 
                                       testSetDir=testSetDir, 
                                       testSetName=testSetName, 
                                       mappingFields=mappingFieldObj.result
                                       );



    



    

def main():
    
    #==========================================================================
    # Read user parameters
    #==========================================================================
    readArguments = ReadArguments();
    readArguments.arguments_setup();
    readArguments.arguments_operation();
    
    #==========================================================================
    # Check license
    #==========================================================================
    checkLicense = CheckLicense();
    if not checkLicense.licensestatus:
        sys.exit(0);
    
    #==========================================================================
    # Execute chosen service type
    #==========================================================================
    mainExecution = MainExecution(readArguments);
    mainExecution.main();
    
    
    
class flushfile(object):
    def __init__(self, f):
        self.f = f
    def write(self, x):
        self.f.write(x)
        self.f.flush()

def set_flush_for_out():
    sys.stdout = flushfile(sys.stdout)
    sys.stderr = flushfile(sys.stderr)
    
    
  
#------------------------------------------------------------------------------ 
                
if __name__ == "__main__":
    set_flush_for_out()
    main();
