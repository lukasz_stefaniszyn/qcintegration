'''
Created on 20-10-2012

@author: lucst
'''
from QCinterCMD import *
from functools import partial
from lib.QClicense import registerReadWrite
import datetime
import distutils.file_util
import sys
import unittest

from lib.QualityCenter_ext.unittest.uTest_TestPlanDowload import QCOperation


class TestReadArguments_Setup(unittest.TestCase):


    def setUp(self):
        self.readArguments = ReadArguments()
        
        self.parameterFile = 'parameterFile.txt'
        
        sys.argv = ['D:\\Eclipse_workspace\\QC_gui\\src\\QCinterCMD.py', 
                    '-f', 
                    self.parameterFile,
                    '-m', 'TPDL',
                    '--TestPlanUpload', 
                    'parameterFile.txt'];
        self.readArguments.arguments_setup();


    def tearDown(self):
        pass


    def test_arguments_setup(self):
        self.assertEqual(self.readArguments.options.parameterFile, 
                         self.parameterFile);
         

class TestReadArguments_Operation(unittest.TestCase):


    def setUp(self):
        self.readArguments = ReadArguments()

    def tearDown(self):
        sys.argv = [];
        del self.readArguments;
        pass;
    
    def set(self, t_type, args):
        self.parameterFile = r'D:\Eclipse_workspace\QC_gui\src\unittest1\parameterFile.txt';
        self.mappingName = 'TPUL';
        
        sys.argv = ['D:\\Eclipse_workspace\\QC_gui\\src\\QCinterCMD.py', 
                    '-f', 
                    self.parameterFile,
                    '-m', self.mappingName,
                    t_type];
        sys.argv.extend(args); 
                    
        self.readArguments.arguments_setup();
        

    
    
    def test_arguments_operation_1(self):
        '''
        Test standard TestPlanUpload parameter
        '''
        
        args = [r'D:\Eclipse_workspace\QC_gui\src\unittest1\Download_11-03-2012_08-46-18.xls']
        self.set(t_type = '--TestPlanUpload', args=args);
        self.readArguments.arguments_operation();
        
        self.assertEqual(self.readArguments.output, 
                         ('testplanUpload', {'filename':args[0]})
                         );

    def test_arguments_operation_1_1NOK(self):
        
        args = [];
        self.set(t_type = '--TestPlanUpload', args=args);
         
        try:
            self.readArguments.arguments_operation()
        except SystemExit, e:
            self.assertEquals(type(e), type(SystemExit()))
            self.assertEquals(e.code, 1)
        except Exception, e:
            self.fail('unexpected exception: %s' % e)
        else:
            self.fail('SystemExit exception expected')
            
    def test_arguments_operation_1_2NOK(self):
        
        args = ['parameterFile.txt', 'OneMOreParameter'];
        self.set(t_type = '--TestPlanUpload', args=args);
         
        try:
            self.readArguments.arguments_operation()
        except SystemExit, e:
            self.assertEquals(type(e), type(SystemExit()))
            self.assertEquals(e.code, 1)
        except Exception, e:
            self.fail('unexpected exception: %s' % e)
        else:
            self.fail('SystemExit exception expected')
        
    def test_arguments_operation_2_1(self):
        '''
        Test standard TestPlanDownload parameter
        '''
        
        args = ['test_TestCaseDir', 'test_TestCaseName'];
        self.set(t_type = '--TestPlanDownload', args=args);
        self.readArguments.options.mappingFieldName='TPDL';
        self.readArguments.arguments_operation();
        
        self.assertEqual(self.readArguments.output, 
                         ('testplanDownload', {'testCaseDir':args[0],
                                               'testCaseName':args[1]
                                               }
                          )
                         );
                         
    def test_arguments_operation_2_2(self):
        
        args = ['test_TestCaseDir'];
        self.set(t_type = '--TestPlanDownload', args=args);
        self.readArguments.options.mappingFieldName='TPDL';
        self.readArguments.arguments_operation();
        
        self.assertEqual(self.readArguments.output, 
                         ('testplanDownload', {'testCaseDir':args[0],
                                               'testCaseName':None
                                               }
                          )
                         );
    def test_arguments_operation_2_3NOK(self):
        
        args = ['test_TestCaseDir', 'test_TestCaseName', 'additionalParam'];
        self.set(t_type = '--TestPlanDownload', args=args);
        self.readArguments.options.mappingFieldName='TPDL';
         
        try:
            self.readArguments.arguments_operation()
        except SystemExit, e:
            self.assertEquals(type(e), type(SystemExit()))
            self.assertEquals(e.code, 1)
        except Exception, e:
            self.fail('unexpected exception: %s' % e)
        else:
            self.fail('SystemExit exception expected')
    
    def test_arguments_operation_2_4NOK(self):
        
        args = [];
        self.set(t_type = '--TestPlanDownload', args=args);
        self.readArguments.options.mappingFieldName='TPDLwrong';
         
        try:
            self.readArguments.arguments_operation()
        except SystemExit, e:
            self.assertEquals(type(e), type(SystemExit()))
            self.assertEquals(e.code, 1)
        except Exception, e:
            self.fail('unexpected exception: %s' % e)
        else:
            self.fail('SystemExit exception expected')
        
    def test_arguments_operation_3(self):
        '''
        Test standard TestLabUpload parameter
        '''
        
        args = [r'D:\Eclipse_workspace\QC_gui\src\unittest1\Download_11-03-2012_08-46-18.xls']
        self.set(t_type = '--TestLabUpload', args=args);
        self.readArguments.options.mappingFieldName='TLUL';
        self.readArguments.arguments_operation();
        
        self.assertEqual(self.readArguments.output, 
                         ('testlabUpload', {'filename':args[0]})
                         );

    def test_arguments_operation_4(self):
        '''
        Test standard TestPlanDownload parameter
        '''
        
        args = ['test_TestSetDir', 'test_TestSetName'];
        self.set(t_type = '--TestLabDownload', args=args);
        self.readArguments.options.mappingFieldName='TLDL';
        self.readArguments.arguments_operation();
        
        self.assertEqual(self.readArguments.output, 
                         ('testlabDownload', {'testSetDir':args[0],
                                               'testSetName':args[1]
                                               }
                          )
                         );
    
    def test_arguments_mappingFieldName_1NOK(self):
        '''
        Checkin option  -m  --mappingField. In this scenario parameter is missing
        '''
        self.parameterFile = r'D:\Eclipse_workspace\QC_gui\src\unittest1\parameterFile.txt';
#        self.mappingName = 'TPUL';
        args=[r'D:\Eclipse_workspace\QC_gui\src\unittest1\Download_11-03-2012_08-46-18.xls']
        t_type = '--TestPlanUpload';
        sys.argv = ['D:\\Eclipse_workspace\\QC_gui\\src\\QCinterCMD.py', 
                    '-f', 
                    self.parameterFile,
#                    '-m', self.mappingName,
                    t_type];
        sys.argv.extend(args); 
        self.readArguments.arguments_setup();
        
        try:
            self.readArguments.arguments_operation()
        except SystemExit, e:
            self.assertEquals(type(e), type(SystemExit()))
            self.assertEquals(e.code, 1)
        except Exception, e:
            self.fail('unexpected exception: %s' % e)
        else:
            self.fail('SystemExit exception expected')
        
    def test_arguments_mappingFieldName_2NOK(self):
        '''
        Checkin option  -m  --mappingField. In this scenario parameter -m mappingName is wrong
        '''
        self.parameterFile = r'D:\Eclipse_workspace\QC_gui\src\unittest1\parameterFile.txt';
        self.mappingName = 'WRONGname';
        args=[r'D:\Eclipse_workspace\QC_gui\src\unittest1\Download_11-03-2012_08-46-18.xls']
        t_type = '--TestPlanUpload';
        sys.argv = ['D:\\Eclipse_workspace\\QC_gui\\src\\QCinterCMD.py', 
                    '-f', 
                    self.parameterFile,
                    '-m', self.mappingName,
                    t_type];
        sys.argv.extend(args); 
        self.readArguments.arguments_setup();
        
        try:
            self.readArguments.arguments_operation()
        except SystemExit, e:
            self.assertEquals(type(e), type(SystemExit()))
            self.assertEquals(e.code, 1)
        except Exception, e:
            self.fail('unexpected exception: %s' % e)
        else:
            self.fail('SystemExit exception expected')
    

class TestCheckLicense(unittest.TestCase):

    dictRegValue = {'tillWhen':  '468958511800', 
                    'instalDate':'468955919800',
                    'runDate':   '468955919800',
                    };
                    
    register = registerReadWrite.Register()


    def setUp(self):
#        self.register = registerReadWrite.Register()
        ##Save register value, which will be used in the checkRegisterValue()
        for regname, regvalue in self.dictRegValue.items():
            self.register.writeRegister(regname, regvalue);
        
        dateNow = datetime.datetime(2012, 7, 13, 21, 17, 34, 0);
        self.dataStartupLicenceCheck = CheckLicense(datetimeNow=dateNow)
        
        for regname, regvalue in self.dictRegValue.items():
            print "%s=%s"%(regname, self.dataStartupLicenceCheck.uncodeCrcBasicDate(int(regvalue)));
            

    def tearDown(self):
        del self.dataStartupLicenceCheck
        
#        self.register.deleteRegisterKey();
        for regname, regvalue in self.dictRegValue.items():
            self.register.deleteRegisterVariable(regname)
        


    
    def test_dataStartupCheck_OK_1(self):
        '''
        Check if in standard operation data licence will work correctly. 
        Those 'standard' operation is: 
        1. registerValue has not been modified, 
        2. data is not expired, 
        '''
#        text = "Your license will expire in 29 day(s)";
        self.assertTrue(self.dataStartupLicenceCheck.status);
        
    def test_dataStartupCheck_OK_2_1(self):
        '''
        Check if in standard operation data licence will work correctly. 
        Those 'standard' operation is: 
        1. registerValue has not been modified, 
        2. data is not expired,
        '''
        datetimeNow = datetime.datetime(2012, 8, 2, 21, 17, 33, 0);
        dataStartupLicenceCheck = CheckLicense(datetimeNow=datetimeNow)
        
        date_min, date_max = dataStartupLicenceCheck.listData();
        print "date_min=%s  date_max=%s"%(dataStartupLicenceCheck.uncodeCrcBasicDate(int(date_min)), 
                                          dataStartupLicenceCheck.uncodeCrcBasicDate(int(date_max)));
        
        dataStartupLicenceCheck.dataStartupCheck();
        self.assertTrue(dataStartupLicenceCheck.status);

    def test_dataStartupCheck_OK_2_3(self):
        '''
        Check if in standard operation data licence will work correctly. 
        Those 'standard' operation is: 
        1. registerValue has not been modified, 
        2. data is not expired,
        '''
        datetimeNow = datetime.datetime(2012, 9, 3, 0, 0, 1, 0);
        dataStartupLicenceCheck = CheckLicense(datetimeNow=datetimeNow)
        
        dataStartupLicenceCheck.dataStartupCheck();
        self.assertFalse(dataStartupLicenceCheck.status);


    def test_dataStartupCheck_OK_2_4(self):
        '''
        Check if in standard operation data licence will work correctly. 
        Those 'standard' operation is: 
        1. registerValue has not been modified, 
        2. data is not expired,
        '''
        dataStartupLicenceCheck = CheckLicense()
        
        dataStartupLicenceCheck.dataStartupCheck();
        self.assertFalse(dataStartupLicenceCheck.status);

class TestMainExecution(unittest.TestCase, QCOperation):
    
    def setUp(self):

#        readArguments = ReadArguments()
#        args=['Flight Reservation']
#        sys.argv = ['D:\\Eclipse_workspace\\QC_gui\\src\\QCinterCMD.py', 
#                    '-f', r'D:\Eclipse_workspace\QC_gui\src\unittest1\parameterFile.txt',
#                    '-m', 'TPDL',
#                    '--TestPlanDownload'];
#        sys.argv.extend(args); 
#        readArguments.arguments_setup();
#        readArguments.arguments_operation();
#        
#        self.mainExecution = MainExecution(readArguments)
        pass
        
    def tearDown(self):
        try: del self.mainExecution;
        except: pass;
        
        
#        end(start());
        
        

    
    
    def test_main_testplandownload_1(self):
        readArguments = ReadArguments()
#        args=['Flight Reservation']
        sys.argv = ['D:\\Eclipse_workspace\\QC_gui\\src\\QCinterCMD.py', 
                    '-f', r'D:\Eclipse_workspace\QC_gui\src\unittest1\parameterFile.txt',
                    '-m', 'TPDL',
                    '--TestPlanDownload'];
#        sys.argv.extend(args); 
        readArguments.arguments_setup();
        readArguments.arguments_operation();
        
        mainExecution = MainExecution(readArguments)
        mainExecution.main()
        
    def test_main_testplandownload_2(self):
        readArguments = ReadArguments()
        args=[r'Subject\Flight Reservation']
        sys.argv = ['D:\\Eclipse_workspace\\QC_gui\\src\\QCinterCMD.py', 
                    '-f', r'D:\Eclipse_workspace\QC_gui\src\unittest1\parameterFile.txt',
                    '-m', 'TPDL',
                    '--TestPlanDownload'];
        sys.argv.extend(args); 
        readArguments.arguments_setup();
        readArguments.arguments_operation();
        
        mainExecution = MainExecution(readArguments)
        mainExecution.main()
        
    def test_main_testplandownload_3(self):
        readArguments = ReadArguments()
        args=[r'Subject\Flight Reservation\Flight Confirmation', 'Flight Confirmation']
        sys.argv = ['D:\\Eclipse_workspace\\QC_gui\\src\\QCinterCMD.py', 
                    '-f', r'D:\Eclipse_workspace\QC_gui\src\unittest1\parameterFile.txt',
                    '-m', 'TPDL',
                    '--TestPlanDownload'];
        sys.argv.extend(args); 
        readArguments.arguments_setup();
        readArguments.arguments_operation();
        
        mainExecution = MainExecution(readArguments)
        mainExecution.main()
    
    def test_main_testplandownload_4(self):
        '''
        Wrong TestCaseName
        '''
        
        readArguments = ReadArguments()
        args=[r'Subject\Flight Reservation\Flight Confirmation', 'Flight Confirmat']
        sys.argv = ['D:\\Eclipse_workspace\\QC_gui\\src\\QCinterCMD.py', 
                    '-f', r'D:\Eclipse_workspace\QC_gui\src\unittest1\parameterFile.txt',
                    '-m', 'TPDL',
                    '--TestPlanDownload'];
        sys.argv.extend(args); 
        readArguments.arguments_setup();
        readArguments.arguments_operation();
        
        mainExecution = MainExecution(readArguments)
        mainExecution.main()
        
    def test_main_testplanupload_1(self):
        readArguments = ReadArguments()
        args=[r'D:\Eclipse_workspace\QC_gui\src\Result\TestPlan\TP_UL_org.xls']
        sys.argv = ['D:\\Eclipse_workspace\\QC_gui\\src\\QCinterCMD.py', 
                    '-f', r'D:\Eclipse_workspace\QC_gui\src\unittest1\parameterFile.txt',
                    '-m', 'TPUL',
                    '--TestPlanUpload'];
        sys.argv.extend(args); 
        readArguments.arguments_setup();
        readArguments.arguments_operation();
        
        mainExecution = MainExecution(readArguments)
        mainExecution.main()
        
        qcOperation = QCOperation();
        qcOperation.qcInstance = mainExecution.qcConnect
        end(qcOperation);
        
        
        
    def test_main_testlabdownload_1(self):
        readArguments = ReadArguments()
        args=[]
        sys.argv = ['D:\\Eclipse_workspace\\QC_gui\\src\\QCinterCMD.py', 
                    '-f', r'D:\Eclipse_workspace\QC_gui\src\unittest1\parameterFile.txt',
                    '-m', 'TLDL',
                    '--TestLabDownload'];
        sys.argv.extend(args); 
        readArguments.arguments_setup();
        readArguments.arguments_operation();
        
        mainExecution = MainExecution(readArguments)
        mainExecution.main()
    
    def test_main_testlabupload_1(self):
        readArguments = ReadArguments()
        args=[r'D:\Eclipse_workspace\QC_gui\src\Result\TestLab\TL_UL.xls']
        sys.argv = ['D:\\Eclipse_workspace\\QC_gui\\src\\QCinterCMD.py', 
                    '-f', r'D:\Eclipse_workspace\QC_gui\src\unittest1\parameterFile.txt',
                    '-m', 'TLUL_2',
                    '--TestLabUpload'];
        sys.argv.extend(args); 
        readArguments.arguments_setup();
        readArguments.arguments_operation();
        
        mainExecution = MainExecution(readArguments)
        mainExecution.main()

class TestMappingField(unittest.TestCase):
    
    testName = r'D:\Eclipse_workspace\QC_gui\src\unittest1\testxml.xml'
    org = r'D:\Eclipse_workspace\QC_gui\src\mappingfields.xml';
    orgtmp = r'D:\Eclipse_workspace\QC_gui\src\mappingfields_TMP.xml';
    
    
    def qcconnect(self):
        self.qcConnect = qc_connect_disconnect.QC_connect(server="http://qc:8080/qcbin/", 
                                                         UserName="alex_qc", 
                                                         Password="", 
                                                         DomainName="DEFAULT", 
                                                         ProjectName="QualityCenter_Demo")
        for connectionstatus, msg in self.qcConnect.connect():
            print msg 
        
        if not connectionstatus:
            print("Not connected to QC server, exiting");
            try: del self.qcConnect;
            except: pass;
            finally: sys.exit(1);

    
    
    def setUp(self):
        
        os.rename(self.org, self.orgtmp); ##save temporary the main file
        distutils.file_util.copy_file(self.testName, self.org); ##open testFile
        
        
        self.settingName = "TLDL"
        self.savingType = "DL"
        self.qcType = "TestLab"
        
        self.qcconnect();
                
        
        
        self.mappingField = MappingField(qc=self.qcConnect.qc, 
                                         savingType=self.savingType, 
                                         qcType=self.qcType)
    
    def tearDown(self):
        
        os.remove(self.org); ##remove temporary test file
        os.rename(self.orgtmp, self.org); ##recover original file
        
        
        try:self.qcConnect.disconnect_QC();
        except: pass;
        finally:    
            print "\nDisconnecting from QC server."\
                    "\n"\
                    "\nThank You for using QCinter.\n"\
                    "\tSoftware Automation www.QCinter.com\n"
        try: sys.exit(0);
        except: pass;
    
    
    def test_openXML(self):
        
        ##Now open xml file with setting name "Setting_2" and get mapping of this setting
        openXmlFile = field_file_operation.OpenFile(self.savingType, self.qcType);
        mappingFieldsXML = openXmlFile.get_mappingfields(self.settingName, self.savingType);
        log.debug(("mappingFieldsXML:", mappingFieldsXML)); 
        return mappingFieldsXML
        
    def test_getFields(self):
        
        mappingFieldsQcDict = self.mappingField.get_fields()
        log.debug(("mappingFieldsQcDict:", mappingFieldsQcDict)); 
        
        mappingFieldsQcDict_golden= {'TEST': {'Status': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TS_STATUS'}, 'Estimated DevTime': {'FieldIsRequired': 'False', 'FieldType': u'number', 'FieldQcName': u'TS_ESTIMATE_DEVTIME'}, 'Designer': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TS_RESPONSIBLE'}, 'Execution Status': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TS_EXEC_STATUS'}, 'Description': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TS_DESCRIPTION'}, 'Level': {'FieldIsRequired': 'True', 'FieldType': u'char', 'FieldQcName': u'TS_USER_04'}, 'Creation Date': {'FieldIsRequired': 'False', 'FieldType': u'date', 'FieldQcName': u'TS_CREATION_DATE'}, 'Modified': {'FieldIsRequired': 'False', 'FieldType': u'Date', 'FieldQcName': u'TS_VTS'}, 'Reviewed': {'FieldIsRequired': 'True', 'FieldType': u'char', 'FieldQcName': u'TS_USER_01'}, 'Priority': {'FieldIsRequired': 'True', 'FieldType': u'char', 'FieldQcName': u'TS_USER_03'}, 'Test Name': {'FieldIsRequired': 'True', 'FieldType': u'char', 'FieldQcName': u'TS_NAME'}, 'Attachment': {'FieldIsRequired': 'False', 'FieldType': 'char', 'FieldQcName': 'ATTACHMENT'}, 'Template': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TS_TEMPLATE'}, 'Reviewer': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TS_USER_02'}, 'Test': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TS_USER_05'}, 'Path': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TS_PATH'}, 'Type': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TS_TYPE'}, 'Subject': {'FieldIsRequired': 'False', 'FieldType': u'number', 'FieldQcName': u'TS_SUBJECT'}}, 'DESSTEPS': {'Expected Result': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'DS_EXPECTED'}, 'Attachment': {'FieldIsRequired': 'False', 'FieldType': 'char', 'FieldQcName': 'ATTACHMENT'}, 'Step Name': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'DS_STEP_NAME'}, 'Description': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'DS_DESCRIPTION'}}, 'STEP': {'Status': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'ST_STATUS'}, 'Source Test': {'FieldIsRequired': 'False', 'FieldType': u'number', 'FieldQcName': u'ST_TEST_ID'}, 'Actual': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'ST_ACTUAL'}, 'Attachment': {'FieldIsRequired': 'False', 'FieldType': 'char', 'FieldQcName': 'ATTACHMENT'}, 'Expected': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'ST_EXPECTED'}, 'Description': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'ST_DESCRIPTION'}, 'Exec Date': {'FieldIsRequired': 'False', 'FieldType': u'date', 'FieldQcName': u'ST_EXECUTION_DATE'}, 'Step Name': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'ST_STEP_NAME'}, 'Exec Time': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'ST_EXECUTION_TIME'}}, 'TESTCYCL': {'Status': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TC_STATUS'}, 'Plan: Template': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TS_TEMPLATE'}, 'Plan: Subject': {'FieldIsRequired': 'False', 'FieldType': u'number', 'FieldQcName': u'TS_SUBJECT'}, 'Plan: Designer': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TS_RESPONSIBLE'}, 'Time': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TC_EXEC_TIME'}, 'Test Version': {'FieldIsRequired': 'False', 'FieldType': u'number', 'FieldQcName': u'TC_TEST_VERSION'}, 'Modified': {'FieldIsRequired': 'False', 'FieldType': u'Date', 'FieldQcName': u'TC_VTS'}, 'Plan: Creation Date': {'FieldIsRequired': 'False', 'FieldType': u'date', 'FieldQcName': u'TS_CREATION_DATE'}, 'Planned Browser': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TC_USER_02'}, 'Responsible Tester': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TC_TESTER_NAME'}, 'Planned Host Name': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TC_HOST_NAME'}, 'Attachment': {'FieldIsRequired': 'False', 'FieldType': 'char', 'FieldQcName': 'ATTACHMENT'}, 'Plan: Priority': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TS_USER_03'}, 'Plan: Test': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TS_USER_05'}, 'Plan: Test Name': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TS_NAME'}, 'Planned Language': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TC_USER_03'}, 'Plan: Reviewed': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TS_USER_01'}, 'Plan: Type': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TS_TYPE'}, 'Plan: Description': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TS_DESCRIPTION'}, 'Tester': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TC_ACTUAL_TESTER'}, 'Plan: Level': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TS_USER_04'}, 'Plan: Execution Status': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TS_EXEC_STATUS'}, 'Exec Date': {'FieldIsRequired': 'False', 'FieldType': u'date', 'FieldQcName': u'TC_EXEC_DATE'}, 'Plan: Status': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TS_STATUS'}, 'Plan: Path': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TS_PATH'}, 'Plan: Estimated DevTime': {'FieldIsRequired': 'False', 'FieldType': u'number', 'FieldQcName': u'TS_ESTIMATE_DEVTIME'}, 'Plan: Reviewer': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TS_USER_02'}, 'Iterations': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TC_ITERATIONS'}, 'Plan: Modified': {'FieldIsRequired': 'False', 'FieldType': u'Date', 'FieldQcName': u'TS_VTS'}, 'Planned Exec Date': {'FieldIsRequired': 'False', 'FieldType': u'date', 'FieldQcName': u'TC_PLAN_SCHEDULING_DATE'}, 'Planned Exec Time': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TC_PLAN_SCHEDULING_TIME'}}, 'CYCLE': {'Status': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'CY_STATUS'}, 'Test': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'CY_USER_01'}, 'Close Date': {'FieldIsRequired': 'False', 'FieldType': u'date', 'FieldQcName': u'CY_CLOSE_DATE'}, 'Test Set': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'CY_CYCLE'}, 'Attachment': {'FieldIsRequired': 'False', 'FieldType': 'char', 'FieldQcName': 'ATTACHMENT'}, 'Test Set Folder': {'FieldIsRequired': 'False', 'FieldType': u'number', 'FieldQcName': u'CY_FOLDER_ID'}, 'Open Date': {'FieldIsRequired': 'False', 'FieldType': u'date', 'FieldQcName': u'CY_OPEN_DATE'}, 'Modified': {'FieldIsRequired': 'False', 'FieldType': u'Date', 'FieldQcName': u'CY_VTS'}, 'ITG Request Id': {'FieldIsRequired': 'False', 'FieldType': u'number', 'FieldQcName': u'CY_REQUEST_ID'}, 'Description': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'CY_COMMENT'}}}
        
        for key, value in mappingFieldsQcDict.items():
            self.assertTrue(mappingFieldsQcDict_golden.has_key(key));
            self.assertEqual(mappingFieldsQcDict_golden[key],
                             value) 
        ##Translate dict() to have fieldQcName instead of fieldUserName as key() 
#        mappingFieldsQc= self.mappingField.translate_mappingFields(mappingFieldsQcDict, type="DL")##this is operation the same as operation on DL type data, that is why it is forced to use type="DL"
#        log.debug(("mappingFieldsQc:", mappingFieldsQc));
        return mappingFieldsQcDict
        
    def test_translate_getFields(self):
        
        
        mappingFields= {'TEST': {'Status': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TS_STATUS'}, 'Estimated DevTime': {'FieldIsRequired': 'False', 'FieldType': u'number', 'FieldQcName': u'TS_ESTIMATE_DEVTIME'}, 'Designer': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TS_RESPONSIBLE'}, 'Execution Status': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TS_EXEC_STATUS'}, 'Description': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TS_DESCRIPTION'}, 'Level': {'FieldIsRequired': 'True', 'FieldType': u'char', 'FieldQcName': u'TS_USER_04'}, 'Creation Date': {'FieldIsRequired': 'False', 'FieldType': u'date', 'FieldQcName': u'TS_CREATION_DATE'}, 'Modified': {'FieldIsRequired': 'False', 'FieldType': u'Date', 'FieldQcName': u'TS_VTS'}, 'Reviewed': {'FieldIsRequired': 'True', 'FieldType': u'char', 'FieldQcName': u'TS_USER_01'}, 'Priority': {'FieldIsRequired': 'True', 'FieldType': u'char', 'FieldQcName': u'TS_USER_03'}, 'Test Name': {'FieldIsRequired': 'True', 'FieldType': u'char', 'FieldQcName': u'TS_NAME'}, 'Attachment': {'FieldIsRequired': 'False', 'FieldType': 'char', 'FieldQcName': 'ATTACHMENT'}, 'Template': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TS_TEMPLATE'}, 'Reviewer': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TS_USER_02'}, 'Test': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TS_USER_05'}, 'Path': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TS_PATH'}, 'Type': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TS_TYPE'}, 'Subject': {'FieldIsRequired': 'False', 'FieldType': u'number', 'FieldQcName': u'TS_SUBJECT'}}, 'DESSTEPS': {'Expected Result': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'DS_EXPECTED'}, 'Attachment': {'FieldIsRequired': 'False', 'FieldType': 'char', 'FieldQcName': 'ATTACHMENT'}, 'Step Name': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'DS_STEP_NAME'}, 'Description': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'DS_DESCRIPTION'}}, 'STEP': {'Status': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'ST_STATUS'}, 'Source Test': {'FieldIsRequired': 'False', 'FieldType': u'number', 'FieldQcName': u'ST_TEST_ID'}, 'Actual': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'ST_ACTUAL'}, 'Attachment': {'FieldIsRequired': 'False', 'FieldType': 'char', 'FieldQcName': 'ATTACHMENT'}, 'Expected': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'ST_EXPECTED'}, 'Description': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'ST_DESCRIPTION'}, 'Exec Date': {'FieldIsRequired': 'False', 'FieldType': u'date', 'FieldQcName': u'ST_EXECUTION_DATE'}, 'Step Name': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'ST_STEP_NAME'}, 'Exec Time': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'ST_EXECUTION_TIME'}}, 'TESTCYCL': {'Status': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TC_STATUS'}, 'Plan: Template': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TS_TEMPLATE'}, 'Plan: Subject': {'FieldIsRequired': 'False', 'FieldType': u'number', 'FieldQcName': u'TS_SUBJECT'}, 'Plan: Designer': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TS_RESPONSIBLE'}, 'Time': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TC_EXEC_TIME'}, 'Test Version': {'FieldIsRequired': 'False', 'FieldType': u'number', 'FieldQcName': u'TC_TEST_VERSION'}, 'Modified': {'FieldIsRequired': 'False', 'FieldType': u'Date', 'FieldQcName': u'TC_VTS'}, 'Plan: Creation Date': {'FieldIsRequired': 'False', 'FieldType': u'date', 'FieldQcName': u'TS_CREATION_DATE'}, 'Planned Browser': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TC_USER_02'}, 'Responsible Tester': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TC_TESTER_NAME'}, 'Planned Host Name': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TC_HOST_NAME'}, 'Attachment': {'FieldIsRequired': 'False', 'FieldType': 'char', 'FieldQcName': 'ATTACHMENT'}, 'Plan: Priority': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TS_USER_03'}, 'Plan: Test': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TS_USER_05'}, 'Plan: Test Name': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TS_NAME'}, 'Planned Language': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TC_USER_03'}, 'Plan: Reviewed': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TS_USER_01'}, 'Plan: Type': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TS_TYPE'}, 'Plan: Description': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TS_DESCRIPTION'}, 'Tester': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TC_ACTUAL_TESTER'}, 'Plan: Level': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TS_USER_04'}, 'Plan: Execution Status': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TS_EXEC_STATUS'}, 'Exec Date': {'FieldIsRequired': 'False', 'FieldType': u'date', 'FieldQcName': u'TC_EXEC_DATE'}, 'Plan: Status': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TS_STATUS'}, 'Plan: Path': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TS_PATH'}, 'Plan: Estimated DevTime': {'FieldIsRequired': 'False', 'FieldType': u'number', 'FieldQcName': u'TS_ESTIMATE_DEVTIME'}, 'Plan: Reviewer': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TS_USER_02'}, 'Iterations': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TC_ITERATIONS'}, 'Plan: Modified': {'FieldIsRequired': 'False', 'FieldType': u'Date', 'FieldQcName': u'TS_VTS'}, 'Planned Exec Date': {'FieldIsRequired': 'False', 'FieldType': u'date', 'FieldQcName': u'TC_PLAN_SCHEDULING_DATE'}, 'Planned Exec Time': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TC_PLAN_SCHEDULING_TIME'}}, 'CYCLE': {'Status': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'CY_STATUS'}, 'Test': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'CY_USER_01'}, 'Close Date': {'FieldIsRequired': 'False', 'FieldType': u'date', 'FieldQcName': u'CY_CLOSE_DATE'}, 'Test Set': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'CY_CYCLE'}, 'Attachment': {'FieldIsRequired': 'False', 'FieldType': 'char', 'FieldQcName': 'ATTACHMENT'}, 'Test Set Folder': {'FieldIsRequired': 'False', 'FieldType': u'number', 'FieldQcName': u'CY_FOLDER_ID'}, 'Open Date': {'FieldIsRequired': 'False', 'FieldType': u'date', 'FieldQcName': u'CY_OPEN_DATE'}, 'Modified': {'FieldIsRequired': 'False', 'FieldType': u'Date', 'FieldQcName': u'CY_VTS'}, 'ITG Request Id': {'FieldIsRequired': 'False', 'FieldType': u'number', 'FieldQcName': u'CY_REQUEST_ID'}, 'Description': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'CY_COMMENT'}}}
        Tmapping = self.mappingField.translate_getFields("TestLab", mappingFields);
        Tmapping_gold= {'TestCase': {'Status': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TC_STATUS'}, 'Plan: Template': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TS_TEMPLATE'}, 'Plan: Subject': {'FieldIsRequired': 'False', 'FieldType': u'number', 'FieldQcName': u'TS_SUBJECT'}, 'Plan: Designer': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TS_RESPONSIBLE'}, 'Time': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TC_EXEC_TIME'}, 'Test Version': {'FieldIsRequired': 'False', 'FieldType': u'number', 'FieldQcName': u'TC_TEST_VERSION'}, 'Modified': {'FieldIsRequired': 'False', 'FieldType': u'Date', 'FieldQcName': u'TC_VTS'}, 'Plan: Creation Date': {'FieldIsRequired': 'False', 'FieldType': u'date', 'FieldQcName': u'TS_CREATION_DATE'}, 'Planned Browser': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TC_USER_02'}, 'Responsible Tester': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TC_TESTER_NAME'}, 'Planned Host Name': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TC_HOST_NAME'}, 'Attachment': {'FieldIsRequired': 'False', 'FieldType': 'char', 'FieldQcName': 'ATTACHMENT'}, 'Plan: Priority': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TS_USER_03'}, 'Plan: Test': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TS_USER_05'}, 'Plan: Test Name': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TS_NAME'}, 'Planned Language': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TC_USER_03'}, 'Plan: Reviewed': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TS_USER_01'}, 'Plan: Type': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TS_TYPE'}, 'Plan: Description': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TS_DESCRIPTION'}, 'Tester': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TC_ACTUAL_TESTER'}, 'Plan: Level': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TS_USER_04'}, 'Plan: Execution Status': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TS_EXEC_STATUS'}, 'Exec Date': {'FieldIsRequired': 'False', 'FieldType': u'date', 'FieldQcName': u'TC_EXEC_DATE'}, 'Plan: Status': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TS_STATUS'}, 'Plan: Path': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TS_PATH'}, 'Plan: Estimated DevTime': {'FieldIsRequired': 'False', 'FieldType': u'number', 'FieldQcName': u'TS_ESTIMATE_DEVTIME'}, 'Plan: Reviewer': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TS_USER_02'}, 'Iterations': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TC_ITERATIONS'}, 'Plan: Modified': {'FieldIsRequired': 'False', 'FieldType': u'Date', 'FieldQcName': u'TS_VTS'}, 'Planned Exec Date': {'FieldIsRequired': 'False', 'FieldType': u'date', 'FieldQcName': u'TC_PLAN_SCHEDULING_DATE'}, 'Planned Exec Time': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'TC_PLAN_SCHEDULING_TIME'}}, 'Step': {'Status': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'ST_STATUS'}, 'Source Test': {'FieldIsRequired': 'False', 'FieldType': u'number', 'FieldQcName': u'ST_TEST_ID'}, 'Actual': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'ST_ACTUAL'}, 'Attachment': {'FieldIsRequired': 'False', 'FieldType': 'char', 'FieldQcName': 'ATTACHMENT'}, 'Expected': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'ST_EXPECTED'}, 'Description': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'ST_DESCRIPTION'}, 'Exec Date': {'FieldIsRequired': 'False', 'FieldType': u'date', 'FieldQcName': u'ST_EXECUTION_DATE'}, 'Step Name': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'ST_STEP_NAME'}, 'Exec Time': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'ST_EXECUTION_TIME'}}, 'TestSet': {'Status': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'CY_STATUS'}, 'Test': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'CY_USER_01'}, 'Close Date': {'FieldIsRequired': 'False', 'FieldType': u'date', 'FieldQcName': u'CY_CLOSE_DATE'}, 'Test Set': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'CY_CYCLE'}, 'Attachment': {'FieldIsRequired': 'False', 'FieldType': 'char', 'FieldQcName': 'ATTACHMENT'}, 'Test Set Folder': {'FieldIsRequired': 'False', 'FieldType': u'number', 'FieldQcName': u'CY_FOLDER_ID'}, 'Open Date': {'FieldIsRequired': 'False', 'FieldType': u'date', 'FieldQcName': u'CY_OPEN_DATE'}, 'Modified': {'FieldIsRequired': 'False', 'FieldType': u'Date', 'FieldQcName': u'CY_VTS'}, 'ITG Request Id': {'FieldIsRequired': 'False', 'FieldType': u'number', 'FieldQcName': u'CY_REQUEST_ID'}, 'Description': {'FieldIsRequired': 'False', 'FieldType': u'char', 'FieldQcName': u'CY_COMMENT'}}}
        
        fs = frozenset(Tmapping_gold);
        self.assertEqual(Tmapping, Tmapping_gold, ("Difference:",fs.difference(Tmapping)));
        
    def test_compareMappingFields(self):
        
        mappingFieldsXML = self.test_openXML();
        mappingFieldsQc = self.test_getFields();
        mappingFieldsQc = self.mappingField.translate_getFields(self.qcType, mappingFieldsQc);
        mappingFieldsQc = self.mappingField.translate_mappingFields(mappingFieldsQc, self.savingType);
        
        
        mappingFieldsXML_checked = {};
        if self.savingType=="DL":
            mappingFieldsXML= self.mappingField.translate_mappingFields(mappingFieldsXML, self.savingType)
            for fieldGroup, fieldValue in mappingFieldsXML.items():
                if mappingFieldsQc.has_key(fieldGroup):
                    mappingFieldsXML_checked.update({fieldGroup:{}});#
                    for fieldQcName, fieldValues in fieldValue.items():
                        if mappingFieldsQc[fieldGroup].has_key(fieldQcName):
                            fieldType = fieldValues.get("FieldType");
                            fieldIsRequired = fieldValues.get("FieldIsRequired");
                            fieldOrder = fieldValues.get("FieldOrder");
                            fieldName = fieldValues.get('FieldName');
                            fieldValues = {'FieldQcName':fieldQcName, 
                                           'FieldType':fieldType, 
                                           'FieldIsRequired':fieldIsRequired, 
                                           'FieldOrder':fieldOrder};
                            ##Change place of fieldUserName and fieldName
                            mappingFieldsXML_checked[fieldGroup].update({fieldName: fieldValues});
                        else:
                            print "Given fieldName: %s is not in mappingFieldsQc"%(fieldQcName);
            log.debug(("mappingFieldsXML_checked:", mappingFieldsXML_checked))
            
            mappingFieldsXML_checked_org= {'TestCase': {'Status': {'FieldIsRequired': 'False', 'FieldOrder': '0', 'FieldType': 'char', 'FieldQcName': 'TC_STATUS'}, 'Plan: Path': {'FieldIsRequired': 'False', 'FieldOrder': '4', 'FieldType': 'char', 'FieldQcName': 'TS_PATH'}, 'Plan: Test': {'FieldIsRequired': 'False', 'FieldOrder': '2', 'FieldType': 'char', 'FieldQcName': 'TS_USER_05'}, 'Plan: Subject': {'FieldIsRequired': 'False', 'FieldOrder': '1', 'FieldType': 'number', 'FieldQcName': 'TS_SUBJECT'}, 'Plan: Status': {'FieldIsRequired': 'False', 'FieldOrder': '5', 'FieldType': 'char', 'FieldQcName': 'TS_STATUS'}, 'Plan: Test Name': {'FieldIsRequired': 'False', 'FieldOrder': '3', 'FieldType': 'char', 'FieldQcName': 'TS_NAME'}}, 'Step': {'Expected': {'FieldIsRequired': 'False', 'FieldOrder': '3', 'FieldType': 'char', 'FieldQcName': 'ST_EXPECTED'}, 'Status': {'FieldIsRequired': 'False', 'FieldOrder': '0', 'FieldType': 'char', 'FieldQcName': 'ST_STATUS'}, 'Actual': {'FieldIsRequired': 'False', 'FieldOrder': '4', 'FieldType': 'char', 'FieldQcName': 'ST_ACTUAL'}, 'Attachment': {'FieldIsRequired': 'False', 'FieldOrder': '2', 'FieldType': 'char', 'FieldQcName': 'ATTACHMENT'}, 'Step Name': {'FieldIsRequired': 'False', 'FieldOrder': '1', 'FieldType': 'char', 'FieldQcName': 'ST_STEP_NAME'}}, 'TestSet': {'Status': {'FieldIsRequired': 'False', 'FieldOrder': '0', 'FieldType': 'char', 'FieldQcName': 'CY_STATUS'}, 'Test': {'FieldIsRequired': 'False', 'FieldOrder': '1', 'FieldType': 'char', 'FieldQcName': 'CY_USER_01'}, 'Test Set': {'FieldIsRequired': 'False', 'FieldOrder': '2', 'FieldType': 'char', 'FieldQcName': 'CY_CYCLE'}, 'Description': {'FieldIsRequired': 'False', 'FieldOrder': '4', 'FieldType': 'char', 'FieldQcName': 'CY_COMMENT'}, 'Test Set Folder': {'FieldIsRequired': 'False', 'FieldOrder': '3', 'FieldType': 'number', 'FieldQcName': 'CY_FOLDER_ID'}}}
            
            self.assertEqual(mappingFieldsXML_checked, mappingFieldsXML_checked_org);
        
    def test_openFieldFile_TLDL(self):
        
        
        self.mappingField.openFieldFile(settingName=self.settingName);
        
        print "Result:", self.mappingField.result;

    def test_openFieldFile_TPDL(self):
        self.settingName = "TPDL"
        self.savingType = "DL"
        self.qcType = "TestPlan"
        
        
        self.mappingField = MappingField(qc=self.qcConnect.qc, 
                                         savingType=self.savingType, 
                                         qcType=self.qcType)
        
        
        
        self.mappingField.openFieldFile(settingName=self.settingName);
        
        print "Result:", self.mappingField.result;
        result_gold = {'TestCase': {'Status': {'FieldIsRequired': 'False', 'FieldOrder': '0', 'FieldType': 'char', 'FieldQcName': 'TS_STATUS'}, 'Priority': {'FieldIsRequired': 'True', 'FieldOrder': '1', 'FieldType': 'char', 'FieldQcName': 'TS_USER_03'}, 'Test Name': {'FieldIsRequired': 'True', 'FieldOrder': '2', 'FieldType': 'char', 'FieldQcName': 'TS_NAME'}, 'Test': {'FieldIsRequired': 'False', 'FieldOrder': '3', 'FieldType': 'char', 'FieldQcName': 'TS_USER_05'}, 'Path': {'FieldIsRequired': 'False', 'FieldOrder': '4', 'FieldType': 'char', 'FieldQcName': 'TS_PATH'}, 'Subject': {'FieldIsRequired': 'False', 'FieldOrder': '5', 'FieldType': 'number', 'FieldQcName': 'TS_SUBJECT'}}, 'Step': {'Expected Result': {'FieldIsRequired': 'False', 'FieldOrder': '0', 'FieldType': 'char', 'FieldQcName': 'DS_EXPECTED'}, 'Description': {'FieldIsRequired': 'False', 'FieldOrder': '2', 'FieldType': 'char', 'FieldQcName': 'DS_DESCRIPTION'}, 'Step Name': {'FieldIsRequired': 'False', 'FieldOrder': '1', 'FieldType': 'char', 'FieldQcName': 'DS_STEP_NAME'}, 'Attachment': {'FieldIsRequired': 'False', 'FieldOrder': '3', 'FieldType': 'char', 'FieldQcName': 'ATTACHMENT'}}, 'TestSet': {'Status': {'FieldIsRequired': 'False', 'FieldOrder': '0', 'FieldType': 'char', 'FieldQcName': 'CY_STATUS'}, 'Test': {'FieldIsRequired': 'False', 'FieldOrder': '1', 'FieldType': 'char', 'FieldQcName': 'CY_USER_01'}, 'Test Set': {'FieldIsRequired': 'False', 'FieldOrder': '2', 'FieldType': 'char', 'FieldQcName': 'CY_CYCLE'}, 'Description': {'FieldIsRequired': 'False', 'FieldOrder': '4', 'FieldType': 'char', 'FieldQcName': 'CY_COMMENT'}, 'Test Set Folder': {'FieldIsRequired': 'False', 'FieldOrder': '3', 'FieldType': 'number', 'FieldQcName': 'CY_FOLDER_ID'}}} 
        
        self.assertEqual(self.mappingField.result, 
                         result_gold);
        

def start():
    readArguments = ReadArguments()

    parameterFile = r'D:\Eclipse_workspace\QC_gui\src\unittest1\parameterFile.txt';
    mappingName = 'TPDL';
    
    sys.argv = ['D:\\Eclipse_workspace\\QC_gui\\src\\QCinterCMD.py', 
                '-f', 
                parameterFile,
                '-m', mappingName,
                '--TestPlanDownload'];
                
    readArguments.arguments_setup();
    readArguments.arguments_operation();
    
    
    qcOperation = QCOperation();
    
    qcOperation.server = readArguments.server;
    qcOperation.userName = readArguments.username;
    qcOperation.password = readArguments.password;
    qcOperation.domainName = readArguments.domainName;
    qcOperation.projectName = readArguments.projectName;
    
    
    
    qcOperation._qcConnect();
    return qcOperation 

def end(qcOperation):
        
#    qcOperation.deleteTPtestcases()
    folderName = "ToDeleteDD";
    qcOperation.deleteTPfolders(folderName)
    del qcOperation.qcInstance;


def load_tests(loader, tests, pattern):
    suite = unittest.TestSuite()
#    suite.addTest(unittest.makeSuite(Test));  ##Run all tests
    return suite


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()