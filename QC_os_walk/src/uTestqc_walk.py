'''
Created on 2010-06-05

@author: wro50026
'''
# from numpy.ma.testutils import assert_equal, assert_almost_equal

import unittest
import time
import qc_walk

from win32com.client import gencache, DispatchWithEvents, constants
gencache.EnsureModule('{F645BD06-E1B4-4E6A-82FB-E97D027FD456}', 0, 1, 0);


class TestSequenceFunctions(unittest.TestCase):
    
    def setUp(self):
        print "setUp"
        
        import qc_connect_disconnect
        import crypt_pass
        
        server = "http://muvmp034.nsn-intra.net/qcbin/"; 
        UserName="stefaniszyn";
        pass_ = "e8dbf113946b941a4a34b573363674a3c58520";
        Password = crypt_pass.Heslo(pass_)._Heslo__readHeslo();
        
        DomainName="HIT7300";
        ProjectName="hiT73_R43x";
        
        qcConnect = qc_connect_disconnect.QC_connect(server, UserName, Password, DomainName, ProjectName);
        if qcConnect.connect():
            print "Connected OK";
            self.qcConnect = qcConnect;
        else:
            print "ERR: Not connected"
            self.qcConnect = None;
        
        

    def tearDown(self):
        print "tearDown"
        
        if self.qcConnect is None:
            return;
        
        if self.qcConnect.disconnect_QC():
            print "\nDisconnecting from QC server."\
                    "\n"\
                    "\nThank You for using QC_import_export script.\n"\
                    "\t\t\t\n"\
                    "\t\t\t  Lukasz Stefaniszyn\n"\
                    "\n\t\t\t\t\tPowered by Python 2.5";
            return True;
        else:
            print "Unable to to disconnect";
            return False
        
    def testgetDirTestCase(self):
        
        root = qc_walk.getRoot(self.qcConnect.qc);
        start = time.time();
        
        
        desired_dirTcList = ([u'A System Design', u'B Services and Transponders', u'C Optical Performance Test', u'D Control and Management', u'E Documentation Test', u'G TL1-Wroclaw', u'H Long Single Span', u'I Passive CWDM/DWDM', u'M MR-Tool Test Inputs'], [])
        getDirTestCase = qc_walk.getDirTestCase(root)
        try:
            actual_dirTcList = ([dir[0] for dir in getDirTestCase[0]], [tc[0] for tc in getDirTestCase[1]]);
        except TypeError:
            print "Directory and TC list is None"
        self.assertEqual(actual_dirTcList, desired_dirTcList)
        
        dir = "Subject\\G TL1-Wroclaw";
        dirName, dirInstance = qc_walk.getDir(self.qcConnect.qc, dir)
        desired_dirTcList = ([u'G01_Fault_Management', u'G02_Configuration_Management', u'G03_Security_Management', u'G04_Performance_Management', u'G05_Load_and_Stress', u'G06_General'], [])
        getDirTestCase = qc_walk.getDirTestCase(dirInstance)
        try:
            actual_dirTcList = ([dir[0] for dir in getDirTestCase[0]], [tc[0] for tc in getDirTestCase[1]]);
        except TypeError:
            print "Directory and TC list is None"
        self.assertEqual(actual_dirTcList, desired_dirTcList);
        #assert_equal(actual_dirTcList, desired_dirTcList)
        
        dir = "Subject\\B Services and Transponders\\B01 - I01T10G";
        dirName, dirInstance = qc_walk.getDir(self.qcConnect.qc, dir)
        desired_dirTcList = ([u'B01.01 - Configuration', u'B01.02 - Performance', u'B01.03 - Fault'], [u'B01 PM_General-tests_I01T10G-1_Long_Term_Test'])
        getDirTestCase = qc_walk.getDirTestCase(dirInstance)
        try:
            actual_dirTcList = ([dir[0] for dir in getDirTestCase[0]], [tc[0] for tc in getDirTestCase[1]]);
        except TypeError:
            print "Directory and TC list is None"
        self.assertEqual(actual_dirTcList, desired_dirTcList)
        
        dir = "Subject\\B Services and Transponders\\B01 - I01T10G\\B01.02 - Performance";
        dirName, dirInstance = qc_walk.getDir(self.qcConnect.qc, dir)
        desired_dirTcList = ([], [u'B01.02.01 PM I01T10G Line-OTU Defect insertion - ODU TCM Layer', u'B01.02.02 PM I01T10G Line-OTU TCM Mode', u'B01.02.03 PM I01T10G Client-OTU Control PM Config and State', u'B01.02.04 PM I01T10G Client-OTU NEperformanceMonitoringMode', u'B01.02.05 I01T10G Enable PRBS'])
        getDirTestCase = qc_walk.getDirTestCase(dirInstance)
        try:
            actual_dirTcList = ([dir[0] for dir in getDirTestCase[0]], [tc[0] for tc in getDirTestCase[1]]);
        except TypeError:
            print "Directory and TC list is None"
        self.assertEqual(actual_dirTcList, desired_dirTcList)
        
        
        
        end = time.time();
        print "finished, time:", end -start;
        
    def testgetRoot(self):
        
        root = qc_walk.getRoot(self.qcConnect.qc);
        self.assertEqual(unicode(root.Name), unicode("Subject"))
        

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testgetDirsWalk']
    
    
    # Create TestSuite by putting inside to it given TestCases
    suiteFew = unittest.TestSuite()
    suiteFew.addTests([TestSequenceFunctions('testgetDirTestCase'), 
                       TestSequenceFunctions('testgetRoot')])
    
    
    testResult = unittest.TextTestRunner(verbosity=2).run(suiteFew)
    print "testResult:", testResult.wasSuccessful();
    print "TestResult.errors", testResult.errors;
    print "TestResult.failures", testResult.failures;
    print "TestResult.testsRun", testResult.testsRun;