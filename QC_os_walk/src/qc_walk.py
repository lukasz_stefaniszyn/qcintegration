'''
Created on 2010-06-04

@author: wro50026
'''


import sys
import time



def getRoot(qc, TestLab=True):
    
    if TestLab:
        tstm = qc.TestSetTreeManager;
        root = tstm.Root();
    else:
        tm = qc.TreeManager;
        root = tm.TreeRoot("Subject");
    return root;

def getDirTestCase(dirInstance, TestLab=True, TestSet=False):
    
    if dirInstance is None:
        print "Given catalog is not present in QualityCenter:", unicode(dir)
        return None;
    
#    data = ([
#             ('A system Design', "A-qcInstance"), 
#             ('B Sevice and Transpoders',"B-qcinstance"), 
#             ('G TL1 Wroclaw', "G-qcinstance")
#            ], 
#             [
#              ('G01 TestCase', 'TestCase-qcInstance')
#              ]);
    
    if TestLab:
        if TestSet:
            pass;
        else:
            pass;
        pass
    
    else:
        dirInstance.Refresh();
        dirListInstance = dirInstance.NewList();
        #print "dirListInstance:", dirListInstance;
        if len(dirListInstance) > 0:
            dirList = [(unicode(dir.Name),dir) for dir in dirListInstance]
            dirList.sort();
        else:
            dirList = [];
        
        tcListInstance = dirInstance.TestFactory.NewList("");
        if len(tcListInstance) > 0:
            tcList = [(unicode(tc.Name),tc) for tc in tcListInstance];
            tcList.sort();
        else:
            tcList = [];
    
    return dirList, tcList;
    


#def getDir(qc, dir):
#    """
#    Give directory instance from QualityCenter 
#    input = qcInstance(qc),  srt("Subject\\testDirectory")
#    output = (str(unicode(dirName)), dirInstance(dir)) OR None
#    """
#    
#    tm = qc.TreeManager;
#    try:
#        dirInstance = tm.NodeByPath(dir)
#    except:
#        print "Node not found"
#        return (None, None); 
#    dirName = unicode(dirInstance.Name);
#    return (dirName, dirInstance) 

#def walkTestCase(folder):
#    
#    tcList = folder.FindTests(Pattern="");
#    return tcList;
#    
#
#def walkDir(folderInstance, topdown=True, onerror=None):
#
#    top = folderInstance.Name
#    try:
#        # Note that listdir and error are globals in this module due
#        # to earlier import-*.
#        names = folderInstance.NewList()
#        #print "listdir:", [dir.Name for dir in names]
#    except:
#        print "ERR: not possible to get Dir List";
#        return
#
#    dirs = []
#    for name in names:
#        dirs.append(name)
#        
#
#    if topdown:
#        #print "top, dirs:", top, [dir.Name for dir in dirs];
#        yield top, [dir.Name for dir in dirs]
#    for name in dirs:
#        for x in walk(name, topdown, onerror):
#            #print "x", x
#            yield x
    

#def QC_connect():
#    
#    from win32com.client import gencache, DispatchWithEvents, constants
#    gencache.EnsureModule('{F645BD06-E1B4-4E6A-82FB-E97D027FD456}', 0, 1, 0);
#    
#    import qc_connect_disconnect
#    import crypt_pass 
#    
#    
#    server = "http://muvmp034.nsn-intra.net/qcbin/"; 
#    UserName="stefaniszyn";
#    pass_ = "e8dbf113946b941a4a34b573363674a3c58520";
#    Password = crypt_pass.Heslo(pass_)._Heslo__readHeslo();
#    
#    DomainName="HIT7300";
#    ProjectName="hiT73_R43x";
#
#    print "\n";
#    print "#"*60;
#    print "server:", server;
#    print "UserName:", UserName;
#    print "DomainName:", DomainName;
#    print "ProjectName", ProjectName;
#    
#    qcConnect = qc_connect_disconnect.QC_connect(server, UserName, Password, DomainName, ProjectName);
#    if qcConnect.connect():
#        print("Connected to QC");
#        return qcConnect;
#    else:
#        print("Not connected to QC server, exiting");
#        return False;
#        
#def QC_disconnect(qcConnect):
#    
#    if qcConnect.disconnect_QC():
#        print "\nDisconnecting from QC server."\
#                "\n"\
#                "\nThank You for using QC_import_export script.\n"\
#                "\t\t\t\n"\
#                "\t\t\t  Lukasz Stefaniszyn\n"\
#                "\n\t\t\t\t\tPowered by Python 2.5";
#        return True;
#    else:
#        print "Unable to to disconnect";
#        return False
   


    

#def getTestCase():
#    qcConnect = QC_connect();
#    
#    if qcConnect:
#        fw_file = open(r"d:/Work/tmp/tmp/TestSet_List.txt", "wb");
#        root = getRoot(qcConnect.qc);
#        start = time.time();
#        i = 1;
#        for tcInstance in walkTestCase(root):
#            #print unicode(tcInstance.Name), unicode(tcInstance.Field("TS_SUBJECT").Path);
#            subjectPath = tcInstance.Field("TS_SUBJECT");
#            if subjectPath is not None:
#                fw_file.write("Name: %s \t Path: %s"%(unicode(tcInstance.Name), unicode(subjectPath.Path)));
#                fw_file.write("\n");
#            else:
#                continue;
#            fw_file.flush();
#        end = time.time();
#        QC_disconnect(qcConnect);
#        fw_file.close();
#        print "finished, time:", end -start;
#        return True
#    else:
#        "not connected";
#        return False
    
     

#def getDirsWalk():
#    
#    
#    
#    qcConnect = QC_connect();
#    
#    if qcConnect:
#        fw_file = open(r"d:/Work/tmp/tmp/TestSet_dirList.txt", "wb");
#        root = getRoot(qcConnect.qc);
#        start = time.time();
#        for root, dirs in walkDir(root):
#            pass;
#            fw_file.write("Root: %s \t Dirs: %s\n"%(repr(root), repr(dirs)));
#            fw_file.flush();
##            print "Top:", root,
##            print "Dirs:", dirs;
#        end = time.time();
#        QC_disconnect(qcConnect);
#        fw_file.close();
#        print "finished, time:", end -start;
#        return True
#    else:
#        "not connected";
#        return False
        
        
        
    



if __name__ == '__main__':
    pass;