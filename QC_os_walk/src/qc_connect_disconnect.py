# -*- coding: utf-8 -*-
#!/usr/bin/env python

import win32com.client as w32c
import pywintypes


class QC_connect(object):
    '''Connect to the Quality server '''
    
    ##QC connection instance
    qc = None;
    
    
    def __init__(self, server, UserName, Password, DomainName, ProjectName):
        self.__server = server;
        self.__UserName = UserName;
        self.__Password = Password;
        self.__DomainName = DomainName;
        self.__ProjectName = ProjectName;

        
        
    #-----------------------------------------------------------------------------        
    def __get_server(self):
        return self.__server;
    server = property(__get_server);
    
    def __get_login(self):
        login = self.__UserName, self.__Password;
        return login;
    login = property(__get_login);
    
    def __get_project(self):
        project = self.__DomainName, self.__ProjectName;
        return project;
    project = property(__get_project);    
#------------------------------------------------------------------------------ 
    

        
        
    
    def QC_instance(self):
        '''Create QualityServer instance under QC_connect.qc
        input = None
        output = None'''
        try:
            QC_connect.qc = w32c.Dispatch("TDApiole80.TDConnection");
            return True
        except:
            text = "Unable to find QualityCenter installation files.\nPlease connect first to QualityCenter by web page to install needed files" 
            print text;
            return False;
            
       
    def connect_server(self, server):
        '''Connect to QC server
        input = str(http adress)
        output = bool(connected) TRUE/FALSE  '''
        try:
            QC_connect.qc.InitConnectionEx(server);
        except:
            text = "Unable connect to Quality Center database: '%s'"%(server); 
            print "ERR: ",text; 
        return QC_connect.qc.Connected;
    
    def connect_login(self, login):
        '''Login to QC server
        input = str(UserName), str(Password)
        output = bool(Logged) TRUE/FALSE  '''
        UserName, Password = login;
        try:
            QC_connect.qc.Login(UserName, Password);
        except pywintypes.com_error, err:
            text = unicode(err[2][2]);
            print "ERR:", text;
        return QC_connect.qc.LoggedIn;
    
    def connect_project(self, project):
        '''Connect to Project in QC server
        input = str(DomainName), str(ProjectName)
        output = bool(ProjectConnected) TRUE/FALSE  '''
        DomainName, ProjectName = project
        try:
            QC_connect.qc.Connect(DomainName, ProjectName)
        except pywintypes.com_error, err:
            text = "Repository of project '%s' in domain '%s' doesn't exist or is not accessible. Please contact your Site Administrator"%(ProjectName,DomainName); 
            print "ERR: ", unicode(err[2][2]);
            print "ERR: ",text
        return QC_connect.qc.ProjectConnected;
    
    def connect(self):
        if self.QC_instance():
            if self.connect_server(self.server):
                ##connected to server
                if self.connect_login(self.login):
                    if self.connect_project(self.project):
                        text = "Connected"
                        connected = True;
                        return connected, text;
                    else:
                        text = "Not connected to Project in QC server.\nPlease, correct DomainName and/or ProjectName";
                        print text 
                        connected = False;
                        return connected, text;
                else:
                    text = "Not logged to QC server.\nPlease, correct UserName and/or Password";
                    print text
                    connected = False;
                    return connected, text;
            else:
                text = "Not connected to QC server.\nPlease, correct server http address"; 
                print text
                connected = False;
                return connected, text;
        else:
            connected = False;
            text = "Unable to find QualityCenter installation files.\nPlease connect first to QualityCenter by web page to install needed files" 
            return connected, text;
    
    def disconnect_QC(self):
        '''Disconnect from QC server
        input = None
        output = bool(^disconnected);'''
        
        try:
            ##Disconnect from Project
            self.qc.Disconnect();
            ##Logout user
            self.qc.Logout();
            ##Disconnect from QC server;
            self.qc.ReleaseConnection();
        except:
            return False
        
        return not self.qc.Connected;
        