# -*- coding: mbcs -*-
# Created by makepy.py version 0.5.00
# By python version 2.5.4 (r254:67916, Dec 23 2008, 15:10:54) [MSC v.1310 32 bit (Intel)]
# From type library 'OTAClient.dll'
# On Wed Sep 16 08:28:58 2009
"""OTA COM 9.0 Type Library"""
makepy_version = '0.5.00'
python_version = 0x20504f0

import win32com.client.CLSIDToClass, pythoncom, pywintypes
import win32com.client.util
from pywintypes import IID
from win32com.client import Dispatch

# The following 3 lines may need tweaking for the particular server
# Candidates are pythoncom.Missing, .Empty and .ArgNotFound
defaultNamedOptArg=pythoncom.Empty
defaultNamedNotOptArg=pythoncom.Empty
defaultUnnamedArg=pythoncom.Empty

CLSID = IID('{F645BD06-E1B4-4E6A-82FB-E97D027FD456}')
MajorVersion = 1
MinorVersion = 0
LibraryFlags = 8
LCID = 0x0

class constants:
	ALERT_TYPE_COMMON             =2          # from enum tagALERT_TYPE
	ALERT_TYPE_FOLLOWUP           =1          # from enum tagALERT_TYPE
	ALERT_TYPE_TRACEABILITY_ALERT =0          # from enum tagALERT_TYPE
	BPPARAM_TYPE_CONSTANT         =0          # from enum tagBPPARAM_TYPE
	BPPARAM_TYPE_REFERENCE        =1          # from enum tagBPPARAM_TYPE
	BPPARAM_TYPE_RUNTIME          =2          # from enum tagBPPARAM_TYPE
	BPT_VERSION_3                 =3          # from enum tagBPT_VERSION
	BPT_VERSION_4                 =4          # from enum tagBPT_VERSION
	OPERATION_FINISHED            =2          # from enum tagESTORAGE_STATISTIC_ACTION_STATUS
	OPERATION_IN_PROGRESS         =1          # from enum tagESTORAGE_STATISTIC_ACTION_STATUS
	OPERTION_INITIALIZING         =0          # from enum tagESTORAGE_STATISTIC_ACTION_STATUS
	FILE_CHANGE                   =4          # from enum tagESTORAGE_STATISTIC_FILE_STATUS
	FILE_IN_CLIENT                =1          # from enum tagESTORAGE_STATISTIC_FILE_STATUS
	FILE_IN_SERVER                =2          # from enum tagESTORAGE_STATISTIC_FILE_STATUS
	FILE_MOVED                    =8          # from enum tagESTORAGE_STATISTIC_FILE_STATUS
	FILE_MOVING                   =16         # from enum tagESTORAGE_STATISTIC_FILE_STATUS
	EXTENDEDSTORAGE_ENTITY_PICTURE=1          # from enum tagEXTENDEDSTORAGE_ENTITY
	EXTENDEDSTORAGE_ENTITY_SCRIPT =0          # from enum tagEXTENDEDSTORAGE_ENTITY
	DELETE_BOTH                   =3          # from enum tagEXTENDED_STORAGE_DELETE
	DELETE_CLIENT                 =1          # from enum tagEXTENDED_STORAGE_DELETE
	DELETE_SERVER                 =2          # from enum tagEXTENDED_STORAGE_DELETE
	NO_DELETE                     =0          # from enum tagEXTENDED_STORAGE_DELETE
	CIENT_FILES_ONLY              =1          # from enum tagEXTENDED_STORAGE_PUT_STATISTICS_MODE
	FILES_FOR_MOVE                =4          # from enum tagEXTENDED_STORAGE_PUT_STATISTICS_MODE
	NO_STATISTICS                 =0          # from enum tagEXTENDED_STORAGE_PUT_STATISTICS_MODE
	SERVER_FILES_ONLY             =2          # from enum tagEXTENDED_STORAGE_PUT_STATISTICS_MODE
	EStorage_MirrorGet            =1          # from enum tagEXTENDED_STORAGE_SETTING
	EStorage_MirrorSet            =0          # from enum tagEXTENDED_STORAGE_SETTING
	GRAPH_GROUP_BY                =1          # from enum tagGRAPH_PROPERTIES
	GRAPH_GROUP_BY_SHOW_FULL_PATH =9          # from enum tagGRAPH_PROPERTIES
	GRAPH_SHOW_NOT_COVERED_PARENTS=4          # from enum tagGRAPH_PROPERTIES
	GRAPH_START_DATE              =3          # from enum tagGRAPH_PROPERTIES
	GRAPH_SUM_OF                  =2          # from enum tagGRAPH_PROPERTIES
	GRAPH_TEST_SET_ID             =5          # from enum tagGRAPH_PROPERTIES
	GRAPH_XAXIS                   =0          # from enum tagGRAPH_PROPERTIES
	GRAPH_XAXIS_SHOW_FULL_PATH    =8          # from enum tagGRAPH_PROPERTIES
	FIELD_MAIL_CONDITION          =1          # from enum tagMAIL_CONDITIONS_TYPE
	USER_MAIL_CONDITION           =0          # from enum tagMAIL_CONDITIONS_TYPE
	SEARCH_IN_STEPS               =0          # from enum tagSEARCH_OPTIONS
	TDATT_COPY                    =1          # from enum tagTDAPI_ATTACH
	TDATT_COPY_ASINCHRONE         =4          # from enum tagTDAPI_ATTACH
	TDATT_LINK                    =3          # from enum tagTDAPI_ATTACH
	TDATT_MOVE                    =2          # from enum tagTDAPI_ATTACH
	TDATT_FILE                    =1          # from enum tagTDAPI_ATTACH_TYPE
	TDATT_INTERNET                =2          # from enum tagTDAPI_ATTACH_TYPE
	BP_STEP_PARAM_IN              =0          # from enum tagTDAPI_BP_STEP_PARAM_TYPE
	BP_STEP_PARAM_OUT             =1          # from enum tagTDAPI_BP_STEP_PARAM_TYPE
	BP_STEP_PARAM_RUNTIME         =2          # from enum tagTDAPI_BP_STEP_PARAM_TYPE
	KW                            =2          # from enum tagTDAPI_COMPONENT_AUTO_TYPE
	MANUAL                        =1          # from enum tagTDAPI_COMPONENT_AUTO_TYPE
	SCRIPTED                      =3          # from enum tagTDAPI_COMPONENT_AUTO_TYPE
	TDCOND_FAILED                 =3          # from enum tagTDAPI_CONDSTAT
	TDCOND_FINISHED               =1          # from enum tagTDAPI_CONDSTAT
	TDCOND_PASSED                 =2          # from enum tagTDAPI_CONDSTAT
	TDCOND_RUN                    =1          # from enum tagTDAPI_CONDTYPE
	TDOLE_DATE                    =5          # from enum tagTDAPI_DATATYPES
	TDOLE_FLOAT                   =2          # from enum tagTDAPI_DATATYPES
	TDOLE_HOST_LIST               =10         # from enum tagTDAPI_DATATYPES
	TDOLE_LONG                    =0          # from enum tagTDAPI_DATATYPES
	TDOLE_MEMO                    =4          # from enum tagTDAPI_DATATYPES
	TDOLE_STRING                  =3          # from enum tagTDAPI_DATATYPES
	TDOLE_SUBJECT_TREENODE        =11         # from enum tagTDAPI_DATATYPES
	TDOLE_TESTSET_FOLDER          =12         # from enum tagTDAPI_DATATYPES
	TDOLE_TESTSET_LIST            =9          # from enum tagTDAPI_DATATYPES
	TDOLE_TIMESTAMP               =6          # from enum tagTDAPI_DATATYPES
	TDOLE_TREENODE                =7          # from enum tagTDAPI_DATATYPES
	TDOLE_ULONG                   =1          # from enum tagTDAPI_DATATYPES
	TDOLE_USER_LIST               =8          # from enum tagTDAPI_DATATYPES
	TDOLE_ATTACH_DIRECTORY        =3          # from enum tagTDAPI_DIRECTORY
	TDOLE_BIN_DIRECTORY           =1          # from enum tagTDAPI_DIRECTORY
	TDOLE_CHECKOUT_DIRECTORY      =8          # from enum tagTDAPI_DIRECTORY
	TDOLE_PROGECT_DIRECTORY       =0          # from enum tagTDAPI_DIRECTORY
	TDOLE_PROJECT_DIRECTORY       =0          # from enum tagTDAPI_DIRECTORY
	TDOLE_SHARED_DIRECTORY        =128        # from enum tagTDAPI_DIRECTORY
	TDOLE_SITE_REPOS_DIRECTORY    =64         # from enum tagTDAPI_DIRECTORY
	TDOLE_TEST_DIRECTORY          =2          # from enum tagTDAPI_DIRECTORY
	TDOLE_USER_DIRECTORIES        =4          # from enum tagTDAPI_DIRECTORY
	TDOLE_VCSDB_DIRECTORY         =32         # from enum tagTDAPI_DIRECTORY
	TDOLE_VIEW_DIRECTORY          =16         # from enum tagTDAPI_DIRECTORY
	ATT_E_CREATE                  =1461       # from enum tagTDAPI_ERRORCODES
	ATT_E_INVALID_REQUEST         =1463       # from enum tagTDAPI_ERRORCODES
	ATT_E_NOT_FOUND               =1460       # from enum tagTDAPI_ERRORCODES
	ATT_E_OBJECT_HAS_ATTACHMENT   =1465       # from enum tagTDAPI_ERRORCODES
	ATT_E_RENAME                  =1464       # from enum tagTDAPI_ERRORCODES
	BUG_E_CREATE                  =1262       # from enum tagTDAPI_ERRORCODES
	BUG_E_DELETE                  =1261       # from enum tagTDAPI_ERRORCODES
	BUG_E_NOT_FOUND               =1260       # from enum tagTDAPI_ERRORCODES
	COL_E_NOT_FOUND               =1240       # from enum tagTDAPI_ERRORCODES
	COMPONENTFOLDER_E_CREATE      =7005       # from enum tagTDAPI_ERRORCODES
	COMPONENTPARAM_E_CREATE       =7006       # from enum tagTDAPI_ERRORCODES
	COMPONENTSTEP_E_CREATE        =7008       # from enum tagTDAPI_ERRORCODES
	COMPONENT_E_CREATE            =7004       # from enum tagTDAPI_ERRORCODES
	COND_E_CREATE                 =1441       # from enum tagTDAPI_ERRORCODES
	COND_E_INVALIDTYPE            =1442       # from enum tagTDAPI_ERRORCODES
	COND_E_NOT_FOUND              =1440       # from enum tagTDAPI_ERRORCODES
	COPY_SUBJECT_E_TO_ITSELF      =1660       # from enum tagTDAPI_ERRORCODES
	DESSTEP_E_CREATE              =1501       # from enum tagTDAPI_ERRORCODES
	DESSTEP_E_DELETE              =1502       # from enum tagTDAPI_ERRORCODES
	DESSTEP_E_NOT_FOUND           =1500       # from enum tagTDAPI_ERRORCODES
	EVENT_ERR_DISABLE             =1800       # from enum tagTDAPI_ERRORCODES
	EXSTORAGE_CANCEL              =1580       # from enum tagTDAPI_ERRORCODES
	EXSTORAGE_CANT_CREATE         =1586       # from enum tagTDAPI_ERRORCODES
	EXSTORAGE_EMPTY_ClIENT_PATH   =1589       # from enum tagTDAPI_ERRORCODES
	EXSTORAGE_EMPTY_SERVER_PATH   =1588       # from enum tagTDAPI_ERRORCODES
	EXSTORAGE_ERROR_PERMISSION    =1591       # from enum tagTDAPI_ERRORCODES
	EXSTORAGE_ERROR_SERVER_PATH   =1590       # from enum tagTDAPI_ERRORCODES
	EXSTORAGE_INTERNET            =1587       # from enum tagTDAPI_ERRORCODES
	EXSTORAGE_INVALID_FILTER      =1582       # from enum tagTDAPI_ERRORCODES
	EXSTORAGE_INVALID_PATH        =1585       # from enum tagTDAPI_ERRORCODES
	EXSTORAGE_IO_OPERATION_DISABLED=1584       # from enum tagTDAPI_ERRORCODES
	EXSTORAGE_OCCIPIED            =1581       # from enum tagTDAPI_ERRORCODES
	FIELD_E_IS_KEY                =1303       # from enum tagTDAPI_ERRORCODES
	FIELD_E_NOT_ACTIVE            =1302       # from enum tagTDAPI_ERRORCODES
	FIELD_E_NO_EDIT               =1304       # from enum tagTDAPI_ERRORCODES
	FIELD_E_NO_PERMISSION         =1300       # from enum tagTDAPI_ERRORCODES
	FIELD_E_REQUIRED              =1307       # from enum tagTDAPI_ERRORCODES
	FIELD_E_SYS_FIELD             =1301       # from enum tagTDAPI_ERRORCODES
	FIELD_E_TRANSITION            =1306       # from enum tagTDAPI_ERRORCODES
	FIELD_E_VERIFIED              =1305       # from enum tagTDAPI_ERRORCODES
	FIELD_INVALID                 =1308       # from enum tagTDAPI_ERRORCODES
	FILTER_E_INVALIDNAME          =1342       # from enum tagTDAPI_ERRORCODES
	FILTER_E_INVALIDPATH_ATTR     =1345       # from enum tagTDAPI_ERRORCODES
	FILTER_E_INVALIDVALUE         =1341       # from enum tagTDAPI_ERRORCODES
	FILTER_E_NOVALUE              =1340       # from enum tagTDAPI_ERRORCODES
	FILTER_S_DATE_COMPLEX         =1343       # from enum tagTDAPI_ERRORCODES
	FILTER_S_LIST_BY_CODE         =1344       # from enum tagTDAPI_ERRORCODES
	GRAPH_DATA_EMPTY              =1700       # from enum tagTDAPI_ERRORCODES
	HOSTGROUP_E_NOT_FOUND         =1640       # from enum tagTDAPI_ERRORCODES
	HOST_E_DELETE                 =1620       # from enum tagTDAPI_ERRORCODES
	HOST_E_NOT_FOUND              =1621       # from enum tagTDAPI_ERRORCODES
	IMPEXP_RESOURCE_DOESNT_EXIST  =7100       # from enum tagTDAPI_ERRORCODES
	ITEM_E_NOT_FOUND              =1420       # from enum tagTDAPI_ERRORCODES
	ITEM_E_NO_ADD                 =1421       # from enum tagTDAPI_ERRORCODES
	ITEM_E_NO_REMOVE              =1422       # from enum tagTDAPI_ERRORCODES
	LINK_E_CREATE                 =1970       # from enum tagTDAPI_ERRORCODES
	LIST_E_EMPTY                  =1220       # from enum tagTDAPI_ERRORCODES
	LIST_E_FILTER                 =1221       # from enum tagTDAPI_ERRORCODES
	MAILCONDITION_E_NOT_FOUND     =1850       # from enum tagTDAPI_ERRORCODES
	PARAM_E_INVALID               =1202       # from enum tagTDAPI_ERRORCODES
	PARAM_E_NOT_FOUND             =1201       # from enum tagTDAPI_ERRORCODES
	PARAM_E_WRONG_NUM             =1200       # from enum tagTDAPI_ERRORCODES
	REQ_E_CREATE                  =1364       # from enum tagTDAPI_ERRORCODES
	REQ_E_DELETE                  =1366       # from enum tagTDAPI_ERRORCODES
	REQ_E_INVALIDFATHERID         =1362       # from enum tagTDAPI_ERRORCODES
	REQ_E_INVALIDMODE             =1365       # from enum tagTDAPI_ERRORCODES
	REQ_E_INVALIDORDER            =1361       # from enum tagTDAPI_ERRORCODES
	REQ_E_INVALIDTYPE             =1363       # from enum tagTDAPI_ERRORCODES
	REQ_E_NOT_FOUND               =1360       # from enum tagTDAPI_ERRORCODES
	RUN_E_CREATE                  =1541       # from enum tagTDAPI_ERRORCODES
	RUN_E_DELETE                  =1542       # from enum tagTDAPI_ERRORCODES
	RUN_E_NOT_FOUND               =1540       # from enum tagTDAPI_ERRORCODES
	SERVER_NOT_CONNECTED          =1600       # from enum tagTDAPI_ERRORCODES
	STEP_E_COPY_STEPS_OF_RUN      =1563       # from enum tagTDAPI_ERRORCODES
	STEP_E_CREATE                 =1561       # from enum tagTDAPI_ERRORCODES
	STEP_E_DELETE                 =1562       # from enum tagTDAPI_ERRORCODES
	STEP_E_NOT_FOUND              =1560       # from enum tagTDAPI_ERRORCODES
	TDAPI_DISCONNECTED_BY_ADMIN   =1077       # from enum tagTDAPI_ERRORCODES
	TDAPI_ERROR_PATH_PERMISSION   =1071       # from enum tagTDAPI_ERRORCODES
	TDAPI_E_DEFECT_LINKED_TO_ITSELF=1972       # from enum tagTDAPI_ERRORCODES
	TDAPI_E_LINK_EXISTS           =1971       # from enum tagTDAPI_ERRORCODES
	TDAPI_E_LINK_TEST_CIRCLUATION =1287       # from enum tagTDAPI_ERRORCODES
	TDAPI_FAILED_TO_RECONNECT     =1078       # from enum tagTDAPI_ERRORCODES
	TDAPI_OBJECT_NOT_EXISTS       =10750      # from enum tagTDAPI_ERRORCODES
	TDAPI_REQ_EXISTS_ABORT        =2056       # from enum tagTDAPI_ERRORCODES
	TDAPI_TEXT_SEARCH_INDEX_MISSING=2205       # from enum tagTDAPI_ERRORCODES
	TDOLE_ADMIN_NOT_CONNECTED     =10740      # from enum tagTDAPI_ERRORCODES
	TDOLE_ATBOF                   =1016       # from enum tagTDAPI_ERRORCODES
	TDOLE_ATEOF                   =1015       # from enum tagTDAPI_ERRORCODES
	TDOLE_BAD_ATT_STREAM          =1066       # from enum tagTDAPI_ERRORCODES
	TDOLE_BAD_FILTER_1            =1101       # from enum tagTDAPI_ERRORCODES
	TDOLE_BAD_FILTER_2            =1102       # from enum tagTDAPI_ERRORCODES
	TDOLE_BAD_FILTER_3            =1103       # from enum tagTDAPI_ERRORCODES
	TDOLE_BAD_FILTER_4            =1104       # from enum tagTDAPI_ERRORCODES
	TDOLE_BAD_FILTER_5            =1105       # from enum tagTDAPI_ERRORCODES
	TDOLE_BAD_FILTER_6            =1106       # from enum tagTDAPI_ERRORCODES
	TDOLE_BAD_FILTER_7            =1107       # from enum tagTDAPI_ERRORCODES
	TDOLE_BAD_FILTER_END          =1110       # from enum tagTDAPI_ERRORCODES
	TDOLE_BAD_FILTER_START        =1100       # from enum tagTDAPI_ERRORCODES
	TDOLE_BAD_REQUEST             =1065       # from enum tagTDAPI_ERRORCODES
	TDOLE_CALL_ORDER              =5021       # from enum tagTDAPI_ERRORCODES
	TDOLE_CANNTBUILDTREE          =1031       # from enum tagTDAPI_ERRORCODES
	TDOLE_CANNTOPENCYCLETB        =1028       # from enum tagTDAPI_ERRORCODES
	TDOLE_CANNTOPENDESSTEPTB      =1029       # from enum tagTDAPI_ERRORCODES
	TDOLE_CANNTOPENHISTTB         =1030       # from enum tagTDAPI_ERRORCODES
	TDOLE_CANNTOPENRUNTB          =1026       # from enum tagTDAPI_ERRORCODES
	TDOLE_CANNTOPENSTEPTB         =1025       # from enum tagTDAPI_ERRORCODES
	TDOLE_CANNTOPENTESTCYCLTB     =1027       # from enum tagTDAPI_ERRORCODES
	TDOLE_CANNTOPENTESTTB         =1024       # from enum tagTDAPI_ERRORCODES
	TDOLE_CAN_UPDATE_UDFS         =1076       # from enum tagTDAPI_ERRORCODES
	TDOLE_CHANGED                 =1020       # from enum tagTDAPI_ERRORCODES
	TDOLE_COMPONENT_EXISTS        =7001       # from enum tagTDAPI_ERRORCODES
	TDOLE_COMPONENT_FOLDER_EXISTS =7002       # from enum tagTDAPI_ERRORCODES
	TDOLE_COMPONENT_PARAM_EXISTS  =7003       # from enum tagTDAPI_ERRORCODES
	TDOLE_CORRUPTED_DATA_HIDING   =9001       # from enum tagTDAPI_ERRORCODES
	TDOLE_CYCLE_EXISTS            =1082       # from enum tagTDAPI_ERRORCODES
	TDOLE_DATA_CORRUPTION         =5020       # from enum tagTDAPI_ERRORCODES
	TDOLE_DBLIMIT                 =1008       # from enum tagTDAPI_ERRORCODES
	TDOLE_DBNOTCONNECTED          =1021       # from enum tagTDAPI_ERRORCODES
	TDOLE_DBVERMISMATCH           =1009       # from enum tagTDAPI_ERRORCODES
	TDOLE_DB_TABLE_NAME           =2000       # from enum tagTDAPI_ERRORCODES
	TDOLE_DIFFERENT_UDFS          =1950       # from enum tagTDAPI_ERRORCODES
	TDOLE_DUPLICATENODENAME       =1080       # from enum tagTDAPI_ERRORCODES
	TDOLE_ERROR_FILESYSTEM        =1069       # from enum tagTDAPI_ERRORCODES
	TDOLE_ERROR_PATH_NOT_FOUND    =1070       # from enum tagTDAPI_ERRORCODES
	TDOLE_E_BPT_COMPONENT_STEP_VALIDATION_FAILED=7010       # from enum tagTDAPI_ERRORCODES
	TDOLE_E_BPT_ITERATIONS_MISMATCH=7009       # from enum tagTDAPI_ERRORCODES
	TDOLE_E_CHACHE                =6700       # from enum tagTDAPI_ERRORCODES
	TDOLE_E_CREATE_LOG_FILE       =1112       # from enum tagTDAPI_ERRORCODES
	TDOLE_E_INITIALIZE            =5200       # from enum tagTDAPI_ERRORCODES
	TDOLE_E_NON_PROJECT_SPECIFIC_BPT=7007       # from enum tagTDAPI_ERRORCODES
	TDOLE_E_PERMISSION            =6100       # from enum tagTDAPI_ERRORCODES
	TDOLE_E_START_RUNS            =1113       # from enum tagTDAPI_ERRORCODES
	TDOLE_FAILED_LOCK_REMOVE      =10710      # from enum tagTDAPI_ERRORCODES
	TDOLE_FAILED_TO_GET_CUR_VER   =1044       # from enum tagTDAPI_ERRORCODES
	TDOLE_FAILED_TO_SET_CUR_VER   =1043       # from enum tagTDAPI_ERRORCODES
	TDOLE_FIELD_ONLY_FOR_VIRTUAL  =6032       # from enum tagTDAPI_ERRORCODES
	TDOLE_GENERROR                =1002       # from enum tagTDAPI_ERRORCODES
	TDOLE_ILLEGALDBNAME           =1005       # from enum tagTDAPI_ERRORCODES
	TDOLE_ILLEGALPASSWORD         =1004       # from enum tagTDAPI_ERRORCODES
	TDOLE_ILLEGALSUBJECTPATH      =1018       # from enum tagTDAPI_ERRORCODES
	TDOLE_ILLEGALUSERNAME         =1152       # from enum tagTDAPI_ERRORCODES
	TDOLE_INTERNAL                =5010       # from enum tagTDAPI_ERRORCODES
	TDOLE_INVALIDATTRNAME         =1017       # from enum tagTDAPI_ERRORCODES
	TDOLE_INVALIDNODEID           =1022       # from enum tagTDAPI_ERRORCODES
	TDOLE_INVALID_INPROCESS       =5220       # from enum tagTDAPI_ERRORCODES
	TDOLE_INVALID_OBJECT          =6000       # from enum tagTDAPI_ERRORCODES
	TDOLE_INVALID_OBJECT_KEY      =6001       # from enum tagTDAPI_ERRORCODES
	TDOLE_INVALID_PARAM           =5100       # from enum tagTDAPI_ERRORCODES
	TDOLE_INVALID_PARAM_ENUM      =5105       # from enum tagTDAPI_ERRORCODES
	TDOLE_INVALID_PARAM_FORMAT    =5104       # from enum tagTDAPI_ERRORCODES
	TDOLE_INVALID_PARAM_NUM       =5103       # from enum tagTDAPI_ERRORCODES
	TDOLE_INVALID_PARAM_RANGE     =5102       # from enum tagTDAPI_ERRORCODES
	TDOLE_INVALID_PARAM_TYPE      =5101       # from enum tagTDAPI_ERRORCODES
	TDOLE_INVALID_REQUEST         =5210       # from enum tagTDAPI_ERRORCODES
	TDOLE_LICENSE_DENIED          =1056       # from enum tagTDAPI_ERRORCODES
	TDOLE_LICENSE_DOMAIN_NOT_FOUND=1061       # from enum tagTDAPI_ERRORCODES
	TDOLE_LICENSE_GENERROR        =1059       # from enum tagTDAPI_ERRORCODES
	TDOLE_LICENSE_KEY_EXPIRED     =1060       # from enum tagTDAPI_ERRORCODES
	TDOLE_LICENSE_MUID_NOT_FOUND  =1062       # from enum tagTDAPI_ERRORCODES
	TDOLE_LICENSE_NOT_INITILIZED  =1058       # from enum tagTDAPI_ERRORCODES
	TDOLE_LICENSE_OVERFLOW_ALERT  =1057       # from enum tagTDAPI_ERRORCODES
	TDOLE_LOGIN_INPUT_MANDATORY_PROPERTY_MISSING=30004      # from enum tagTDAPI_ERRORCODES
	TDOLE_LOGIN_INPUT_PROPERTY_DOES_NOT_EXIST=30002      # from enum tagTDAPI_ERRORCODES
	TDOLE_LOGIN_INPUT_VALUE_EXCEED_MAX_LENGTH=30003      # from enum tagTDAPI_ERRORCODES
	TDOLE_MAIL_BAD_FROM_FIELD     =1052       # from enum tagTDAPI_ERRORCODES
	TDOLE_MAIL_ERROR_BAD_ENTITY   =1081       # from enum tagTDAPI_ERRORCODES
	TDOLE_MAIL_ERR_PARSING_PARAMS =1048       # from enum tagTDAPI_ERRORCODES
	TDOLE_MAIL_ERR_SENDING        =1051       # from enum tagTDAPI_ERRORCODES
	TDOLE_MAIL_GENERROR           =1049       # from enum tagTDAPI_ERRORCODES
	TDOLE_MAIL_LOGIN_FAILURE      =1050       # from enum tagTDAPI_ERRORCODES
	TDOLE_MAIL_NO_ERROR           =1045       # from enum tagTDAPI_ERRORCODES
	TDOLE_MAIL_NO_TO_FIELD        =1047       # from enum tagTDAPI_ERRORCODES
	TDOLE_MANUALTEST              =1019       # from enum tagTDAPI_ERRORCODES
	TDOLE_NOABSOLUTEPATH          =1054       # from enum tagTDAPI_ERRORCODES
	TDOLE_NOASSOCIATEDROOT        =1032       # from enum tagTDAPI_ERRORCODES
	TDOLE_NOCONFIGFILE            =1003       # from enum tagTDAPI_ERRORCODES
	TDOLE_NODBDIR                 =1006       # from enum tagTDAPI_ERRORCODES
	TDOLE_NODE_EXISTS             =1661       # from enum tagTDAPI_ERRORCODES
	TDOLE_NODIRECTORY             =1014       # from enum tagTDAPI_ERRORCODES
	TDOLE_NOFIELD                 =1012       # from enum tagTDAPI_ERRORCODES
	TDOLE_NONODE                  =1023       # from enum tagTDAPI_ERRORCODES
	TDOLE_NOTAUTOTEST             =1013       # from enum tagTDAPI_ERRORCODES
	TDOLE_NOTESTDIR               =1007       # from enum tagTDAPI_ERRORCODES
	TDOLE_NOT_LOGGED_IN           =30001      # from enum tagTDAPI_ERRORCODES
	TDOLE_NOT_VIRTUAL             =6030       # from enum tagTDAPI_ERRORCODES
	TDOLE_NO_IDAPI_DLL            =1010       # from enum tagTDAPI_ERRORCODES
	TDOLE_NO_MAIL_DLL             =1046       # from enum tagTDAPI_ERRORCODES
	TDOLE_OBJECT_DELETED          =6002       # from enum tagTDAPI_ERRORCODES
	TDOLE_OBJECT_LOCKED           =10720      # from enum tagTDAPI_ERRORCODES
	TDOLE_OBJECT_NOT_INITIALIZED  =6003       # from enum tagTDAPI_ERRORCODES
	TDOLE_OBJNOTINITIALIZED       =1001       # from enum tagTDAPI_ERRORCODES
	TDOLE_RECCOVER_EXISTS         =1068       # from enum tagTDAPI_ERRORCODES
	TDOLE_RECNOTFOUND             =1011       # from enum tagTDAPI_ERRORCODES
	TDOLE_REQ_EXISTS              =2055       # from enum tagTDAPI_ERRORCODES
	TDOLE_SERVER_LOCKS_REMOVED    =10700      # from enum tagTDAPI_ERRORCODES
	TDOLE_TESTSET_FOLDER_EXISTS   =3055       # from enum tagTDAPI_ERRORCODES
	TDOLE_TESTS_ALREADY_RUN       =1111       # from enum tagTDAPI_ERRORCODES
	TDOLE_TEST_EXISTS             =1055       # from enum tagTDAPI_ERRORCODES
	TDOLE_UNEXPECTED              =5000       # from enum tagTDAPI_ERRORCODES
	TDOLE_VCS_CANNOTLOGIN         =1035       # from enum tagTDAPI_ERRORCODES
	TDOLE_VCS_LOCKEDMETEST        =1037       # from enum tagTDAPI_ERRORCODES
	TDOLE_VCS_LOCKEDTEST          =1038       # from enum tagTDAPI_ERRORCODES
	TDOLE_VCS_NOTCHECKEDOUT       =1040       # from enum tagTDAPI_ERRORCODES
	TDOLE_VCS_NOTCONNECTED        =1039       # from enum tagTDAPI_ERRORCODES
	TDOLE_VCS_NOTDEFVERFILE       =1042       # from enum tagTDAPI_ERRORCODES
	TDOLE_VCS_NOTINVCSDB          =1036       # from enum tagTDAPI_ERRORCODES
	TDOLE_VCS_NOTLOCKED           =1041       # from enum tagTDAPI_ERRORCODES
	TDOLE_VCS_NOTVCSDB            =1034       # from enum tagTDAPI_ERRORCODES
	TDOLE_VCS_NOVCS               =1033       # from enum tagTDAPI_ERRORCODES
	TDOLE_VCS_VERSIONEXISTS       =1063       # from enum tagTDAPI_ERRORCODES
	TDOLE_VCS_VERSIONNOTEXISTS    =1064       # from enum tagTDAPI_ERRORCODES
	TDOLE_VIRTUAL                 =6020       # from enum tagTDAPI_ERRORCODES
	TDOLE_VIRTUAL_FATHER          =6010       # from enum tagTDAPI_ERRORCODES
	TDOLE_VIRTUAL_FIELD           =6040       # from enum tagTDAPI_ERRORCODES
	TDOLE_WARNING_FROM_NOT_FOUND  =1053       # from enum tagTDAPI_ERRORCODES
	TDOLE_WRONG_ADMIN_PWD         =10730      # from enum tagTDAPI_ERRORCODES
	TDPARAM_NOT_FOUND             =1900       # from enum tagTDAPI_ERRORCODES
	TESTSET_E_CREATE              =1381       # from enum tagTDAPI_ERRORCODES
	TESTSET_E_DELETE              =1382       # from enum tagTDAPI_ERRORCODES
	TESTSET_E_DELETE_DEFAULT      =1383       # from enum tagTDAPI_ERRORCODES
	TESTSET_E_NOT_FOUND           =1380       # from enum tagTDAPI_ERRORCODES
	TEST_E_COV_DELETE             =1283       # from enum tagTDAPI_ERRORCODES
	TEST_E_CREATE                 =1281       # from enum tagTDAPI_ERRORCODES
	TEST_E_DELETE                 =1282       # from enum tagTDAPI_ERRORCODES
	TEST_E_NOT_FOUND              =1280       # from enum tagTDAPI_ERRORCODES
	TEST_E_REPOSITORY             =1284       # from enum tagTDAPI_ERRORCODES
	TEST_E_TEMPLATE_TYPE          =1285       # from enum tagTDAPI_ERRORCODES
	TREE_E_CREATE                 =1521       # from enum tagTDAPI_ERRORCODES
	TREE_E_DELETE                 =1522       # from enum tagTDAPI_ERRORCODES
	TREE_E_NOT_FOUND              =1520       # from enum tagTDAPI_ERRORCODES
	TREE_E_RENAME                 =1523       # from enum tagTDAPI_ERRORCODES
	TSTEST_E_CREATE               =1402       # from enum tagTDAPI_ERRORCODES
	TSTEST_E_DELETE               =1401       # from enum tagTDAPI_ERRORCODES
	TSTEST_E_NOT_FOUND            =1400       # from enum tagTDAPI_ERRORCODES
	USERSGROUP_E_CREATE           =1482       # from enum tagTDAPI_ERRORCODES
	USERSGROUP_E_NOT_FOUND        =1480       # from enum tagTDAPI_ERRORCODES
	USERSGROUP_E_SYSTEM           =1481       # from enum tagTDAPI_ERRORCODES
	USER_E_CREATE                 =1321       # from enum tagTDAPI_ERRORCODES
	USER_E_DELETE                 =1322       # from enum tagTDAPI_ERRORCODES
	USER_E_IN_GROUP               =1323       # from enum tagTDAPI_ERRORCODES
	USER_E_NOT_FOUND              =1320       # from enum tagTDAPI_ERRORCODES
	USER_E_NOT_IN_GROUP           =1324       # from enum tagTDAPI_ERRORCODES
	TDOLE_LAST_ORDER              =-4         # from enum tagTDAPI_EUM_ORDER
	EXECEVENT_ENVIRONMENTFAIL     =3          # from enum tagTDAPI_EXECUTIONEVENT
	EXECEVENT_MANUAL_LAUNCH       =5          # from enum tagTDAPI_EXECUTIONEVENT
	EXECEVENT_RUNTIMEDOUT         =4          # from enum tagTDAPI_EXECUTIONEVENT
	EXECEVENT_TESTFAIL            =1          # from enum tagTDAPI_EXECUTIONEVENT
	EXECEVENT_TESTSETFINISH       =2          # from enum tagTDAPI_EXECUTIONEVENT
	EXECEVENTACTION_DEFAULT       =0          # from enum tagTDAPI_EXECUTIONEVENTACTION
	EXECEVENTACTION_DONOTHING     =1          # from enum tagTDAPI_EXECUTIONEVENTACTION
	EXECEVENTACTION_RESTART       =3          # from enum tagTDAPI_EXECUTIONEVENTACTION
	EXECEVENTACTION_STOP          =2          # from enum tagTDAPI_EXECUTIONEVENTACTION
	EXECEVENTACTIONTARGET_TEST    =1          # from enum tagTDAPI_EXECUTIONEVENTACTIONTARGET
	EXECEVENTACTIONTARGET_TESTSET =2          # from enum tagTDAPI_EXECUTIONEVENTACTIONTARGET
	TDOLE_DELETE_LOCAL            =1          # from enum tagTDAPI_EXSTOR_DELMODE
	TDOLE_DELETE_REMOTE           =2          # from enum tagTDAPI_EXSTOR_DELMODE
	TDOLE_FILE                    =1          # from enum tagTDAPI_FDATA_TYPE
	TDOLE_FOLDER                  =0          # from enum tagTDAPI_FDATA_TYPE
	TDOLE_FIRST_LEVEL             =0          # from enum tagTDAPI_FETCH_LEVEL
	TDOLE_SECOND_LEVEL            =1          # from enum tagTDAPI_FETCH_LEVEL
	TDOLE_THIRD_LEVEL             =2          # from enum tagTDAPI_FETCH_LEVEL
	TDOLE_ASCENDING               =0          # from enum tagTDAPI_FILTERORDER
	TDOLE_DESCENDING              =1          # from enum tagTDAPI_FILTERORDER
	GRANT_MODIFY_BY_OWNER_ONLY    =2          # from enum tagTDAPI_GRANT_MODIFY
	GRANT_MODIFY_DENY             =1          # from enum tagTDAPI_GRANT_MODIFY
	GRANT_MODIFY_PERMIT           =0          # from enum tagTDAPI_GRANT_MODIFY
	GRAPH_AGE                     =1          # from enum tagTDAPI_GRAPH_TYPE
	GRAPH_PROGRESS                =2          # from enum tagTDAPI_GRAPH_TYPE
	GRAPH_SUMMARY                 =0          # from enum tagTDAPI_GRAPH_TYPE
	GRAPH_TREND                   =3          # from enum tagTDAPI_GRAPH_TYPE
	LIC_COLLABORATION             =6          # from enum tagTDAPI_LICENSE
	LIC_COMPONENTS                =13         # from enum tagTDAPI_LICENSE
	LIC_DASHBOARD                 =12         # from enum tagTDAPI_LICENSE
	LIC_DEFECT                    =1          # from enum tagTDAPI_LICENSE
	LIC_OTA_CLIENT                =4          # from enum tagTDAPI_LICENSE
	LIC_REQUIREMENT               =3          # from enum tagTDAPI_LICENSE
	LIC_TEST_LAB                  =2          # from enum tagTDAPI_LICENSE
	MODULE_COLLABORATION          =4          # from enum tagTDAPI_MODULE
	MODULE_COMPONENTS             =6          # from enum tagTDAPI_MODULE
	MODULE_DASHBOARD              =5          # from enum tagTDAPI_MODULE
	MODULE_DEFECT                 =0          # from enum tagTDAPI_MODULE
	MODULE_REQUIREMENT            =3          # from enum tagTDAPI_MODULE
	MODULE_TEST_EXECUTION         =2          # from enum tagTDAPI_MODULE
	MODULE_TEST_PLANNING          =1          # from enum tagTDAPI_MODULE
	TDOLE_NODE_CHANGEABLE         =2          # from enum tagTDAPI_NODEATTRIBUTE
	TDOLE_NODE_FULLACCESS         =3          # from enum tagTDAPI_NODEATTRIBUTE
	TDOLE_NODE_READONLY           =1          # from enum tagTDAPI_NODEATTRIBUTE
	TDOLE_NODE_SYSTEM             =0          # from enum tagTDAPI_NODEATTRIBUTE
	PARAM_IN                      =1          # from enum tagTDAPI_PARAM_IN_OUT_TYPE
	PARAM_INOUT_UNDEFINED         =0          # from enum tagTDAPI_PARAM_IN_OUT_TYPE
	PARAM_OUT                     =2          # from enum tagTDAPI_PARAM_IN_OUT_TYPE
	TDPOSITION_LAST               =-4         # from enum tagTDAPI_POS_ORDER
	TDDATE_PURGE                  =5          # from enum tagTDAPI_PURGE_RUNS
	TDDAY_PURGE                   =1          # from enum tagTDAPI_PURGE_RUNS
	TDMONTH_PURGE                 =3          # from enum tagTDAPI_PURGE_RUNS
	TDWEEK_PURGE                  =2          # from enum tagTDAPI_PURGE_RUNS
	TDYEAR_PURGE                  =4          # from enum tagTDAPI_PURGE_RUNS
	TDREQMODE_FIND_ANYWHERE       =32         # from enum tagTDAPI_REQMODE
	TDREQMODE_FIND_EXACT          =16         # from enum tagTDAPI_REQMODE
	TDREQMODE_FIND_START_WITH     =8          # from enum tagTDAPI_REQMODE
	TDREQMODE_REC                 =1          # from enum tagTDAPI_REQMODE
	TDREQMODE_REM_REM_ALL         =4          # from enum tagTDAPI_REQMODE
	TDREQMODE_SMART               =2          # from enum tagTDAPI_REQMODE
	TDOLE_ALL                     =0          # from enum tagTDAPI_ROOTLIST
	TDOLE_NOT_SUBJECT             =2          # from enum tagTDAPI_ROOTLIST
	TDOLE_SUBJECT                 =1          # from enum tagTDAPI_ROOTLIST
	PARAM_SOURCE_PRE_RUN          =1          # from enum tagTDAPI_RUN_PARAM_MODE
	PARAM_SOURCE_STEPS            =2          # from enum tagTDAPI_RUN_PARAM_MODE
	TDAPI_STARTVIRTUAL_VALUE      =-10        # from enum tagTDAPI_SETTINGS
	TDOLE_SKIP_DAYS               =0          # from enum tagTDAPI_SKIP
	TDOLE_SKIP_MONTHS             =2          # from enum tagTDAPI_SKIP
	TDOLE_SKIP_WEEKS              =1          # from enum tagTDAPI_SKIP
	TDOLE_SKIP_YEARS              =3          # from enum tagTDAPI_SKIP
	TDOLE_STEP_CREATE             =0          # from enum tagTDAPI_STEP_CREATE
	TDOLE_STEP_UPDATE             =2          # from enum tagTDAPI_STEP_CREATE
	STEP_PARAM_BASE               =4          # from enum tagTDAPI_STEP_PARAM_TYPE
	STEP_PARAM_NULL               =2          # from enum tagTDAPI_STEP_PARAM_TYPE
	STEP_PARAM_PREDEF             =1          # from enum tagTDAPI_STEP_PARAM_TYPE
	STEP_PARAM_VALUE              =3          # from enum tagTDAPI_STEP_PARAM_TYPE
	TDOLE_LEAF_TYPE               =0          # from enum tagTDAPI_TREETYPE
	TDOLE_LIST_TYPE               =1          # from enum tagTDAPI_TREETYPE
	TDOLE_TREE_TYPE               =2          # from enum tagTDAPI_TREETYPE
	TDLICENSE_BUG                 =1          # from enum tagTDLICENSE_CLIENT
	TDLICENSE_COLLAB              =32         # from enum tagTDLICENSE_CLIENT
	TDLICENSE_COMPONENTS          =4096       # from enum tagTDLICENSE_CLIENT
	TDLICENSE_DASHBOARD           =2048       # from enum tagTDLICENSE_CLIENT
	TDLICENSE_LAB                 =2          # from enum tagTDLICENSE_CLIENT
	TDLICENSE_REQ                 =4          # from enum tagTDLICENSE_CLIENT
	TDMAIL_ATTACHMENT             =1          # from enum tagTDMAIL_FLAGS
	TDMAIL_COMMENT_AS_BODY        =64         # from enum tagTDMAIL_FLAGS
	TDMAIL_COVER_TEST             =16         # from enum tagTDMAIL_FLAGS
	TDMAIL_DES_STEP               =8          # from enum tagTDMAIL_FLAGS
	TDMAIL_HISTORY                =2          # from enum tagTDMAIL_FLAGS
	TDMAIL_SINGLEMAIL             =32         # from enum tagTDMAIL_FLAGS
	TDMAIL_TEXT                   =4          # from enum tagTDMAIL_FLAGS
	UNIT_TEST_DISABLE             =-1         # from enum tagUNITTEST_OPTIONS
	UNIT_TEST_EXECUTE             =0          # from enum tagUNITTEST_OPTIONS
	UNIT_TEST_RECORD              =1          # from enum tagUNITTEST_OPTIONS

from win32com.client import DispatchBaseClass
class IActionPermission(DispatchBaseClass):
	"""Services to manage user permissions for actions."""
	CLSID = IID('{1EE78B77-A68F-49D8-9707-9A7C8CEA10D2}')
	coclass_clsid = IID('{8D7BCC69-04CB-422A-8F85-2654E3539340}')

	# The method ActionEnabled is actually a property, but must be used as a method to correctly pass the arguments
	def ActionEnabled(self, ActionIdentity=defaultNamedNotOptArg, ActionTarget=defaultNamedOptArg):
		"""Checks if the user has rights to perform the specified action."""
		return self._oleobj_.InvokeTypes(1, LCID, 2, (11, 0), ((12, 1), (12, 17)),ActionIdentity
			, ActionTarget)

	_prop_map_get_ = {
	}
	_prop_map_put_ = {
	}

class IAlert(DispatchBaseClass):
	"""Represents a notification sent to the user."""
	CLSID = IID('{A1314B45-32F6-4841-9EB6-922EC4A76CB2}')
	coclass_clsid = IID('{0A29F8B6-0FFE-4783-B3B0-71FB3FB8D6D9}')

	_prop_map_get_ = {
		"AlertDate": (2, 2, (7, 0), (), "AlertDate", None),
		"AlertType": (4, 2, (3, 0), (), "AlertType", None),
		"Description": (1, 2, (8, 0), (), "Description", None),
		"ID": (3, 2, (3, 0), (), "ID", None),
		"ParentEntityURL": (7, 2, (8, 0), (), "ParentEntityURL", None),
		"Subject": (6, 2, (8, 0), (), "Subject", None),
		"Unread": (5, 2, (11, 0), (), "Unread", None),
	}
	_prop_map_put_ = {
		"Unread": ((5, LCID, 4, 0),()),
	}

class IAlertManager(DispatchBaseClass):
	"""Services for managing alerts."""
	CLSID = IID('{7A0B3B0B-60C4-4B84-8A35-1E9337AD055E}')
	coclass_clsid = IID('{7F04BEF8-6460-4BF2-825A-F758E8657F51}')

	# The method Alert is actually a property, but must be used as a method to correctly pass the arguments
	def Alert(self, ID=defaultNamedNotOptArg):
		"""Gets the Alert object for the alert with the specified ID."""
		ret = self._oleobj_.InvokeTypes(3, LCID, 2, (9, 0), ((3, 1),),ID
			)
		if ret is not None:
			ret = Dispatch(ret, u'Alert', None)
		return ret

	# Result is of type IList
	# The method AlertList is actually a property, but must be used as a method to correctly pass the arguments
	def AlertList(self, EntityType=defaultNamedNotOptArg, NeedRefresh=False):
		"""The list of user alerts."""
		ret = self._oleobj_.InvokeTypes(4, LCID, 2, (9, 0), ((8, 1), (11, 49)),EntityType
			, NeedRefresh)
		if ret is not None:
			ret = Dispatch(ret, u'AlertList', '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')
		return ret

	def CleanAllAlerts(self):
		"""Removes all current alerts from the database."""
		return self._oleobj_.InvokeTypes(2, LCID, 1, (24, 0), (),)

	def DeleteAlert(self, IDs=defaultNamedNotOptArg):
		"""Removes an alert or alerts related to the current object from the database."""
		return self._oleobj_.InvokeTypes(1, LCID, 1, (24, 0), ((12, 1),),IDs
			)

	_prop_map_get_ = {
	}
	_prop_map_put_ = {
	}

class IAlertable(DispatchBaseClass):
	"""Services for managing auto-alerts."""
	CLSID = IID('{101CD251-91FB-4FB0-A440-DE755D905584}')
	coclass_clsid = IID('{488D6BE8-A962-4958-8E11-6E2EE9A20EFF}')

	def CleanAllAlerts(self):
		"""Removes all current object alerts from the database."""
		return self._oleobj_.InvokeTypes(1610678273, LCID, 1, (24, 0), (),)

	def DeleteAlert(self, IDs=defaultNamedNotOptArg):
		"""Removes an alert or alerts associated with the current object from the database."""
		return self._oleobj_.InvokeTypes(1610678272, LCID, 1, (24, 0), ((12, 1),),IDs
			)

	def GetAlert(self, ID=defaultNamedNotOptArg):
		"""Gets the alert specified by the alert ID."""
		ret = self._oleobj_.InvokeTypes(1610678274, LCID, 1, (9, 0), ((3, 1),),ID
			)
		if ret is not None:
			ret = Dispatch(ret, u'GetAlert', None)
		return ret

	# Result is of type IList
	def GetAlertList(self, NeedRefresh=False):
		"""Returns a list of all alerts associated with the object."""
		ret = self._oleobj_.InvokeTypes(1610678275, LCID, 1, (9, 0), ((11, 49),),NeedRefresh
			)
		if ret is not None:
			ret = Dispatch(ret, u'GetAlertList', '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')
		return ret

	def HasAlerts(self):
		"""Checks if any alerts are associated with the object."""
		return self._oleobj_.InvokeTypes(1610678276, LCID, 1, (11, 0), (),)

	def HasNewAlerts(self):
		"""New, unread alerts are associated with the object."""
		return self._oleobj_.InvokeTypes(1610678277, LCID, 1, (11, 0), (),)

	_prop_map_get_ = {
	}
	_prop_map_put_ = {
	}

class IAlertableEntityFactory(DispatchBaseClass):
	"""IAlertableEntityFactory Interface."""
	CLSID = IID('{FF0182C5-D203-4875-BC2C-48FF8DA7266D}')
	coclass_clsid = IID('{1586DCBE-08C3-430F-98F1-D3F078A01E34}')

	_prop_map_get_ = {
	}
	_prop_map_put_ = {
	}

class IAmarillusHash(DispatchBaseClass):
	"""Support for hash generation."""
	CLSID = IID('{5073A186-2A46-4C93-A3F5-F8C0AA66694F}')
	coclass_clsid = IID('{61C395DB-BDD5-4431-995D-E5F38E8FAC70}')

	def GenerateHash(self, Value=defaultNamedNotOptArg):
		"""Returns a Hash generated from the input value."""
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(1, LCID, 1, (8, 0), ((8, 1),),Value
			)

	_prop_map_get_ = {
	}
	_prop_map_put_ = {
	}

class IAnalysis(DispatchBaseClass):
	"""IAnalysis Interface."""
	CLSID = IID('{557AF6E9-FB13-4934-9A8F-6620BA38C547}')
	coclass_clsid = IID('{E8D39BCF-F7AF-4DE0-82B2-8B51AE2C8427}')

	def GetDistinctValues(self, Fields=defaultNamedNotOptArg):
		"""Gets distinct values for specified fields."""
		return self._ApplyTypes_(3, 1, (12, 0), ((12, 1),), u'GetDistinctValues', None,Fields
			)

	def GetSummaryData(self, Fields=defaultNamedNotOptArg, DataFields=defaultNamedNotOptArg):
		"""Gets summary data."""
		return self._ApplyTypes_(4, 1, (12, 0), ((12, 1), (12, 1)), u'GetSummaryData', None,Fields
			, DataFields)

	_prop_map_get_ = {
		"FilterText": (1, 2, (8, 0), (), "FilterText", None),
		"JoinCondition": (5, 2, (8, 0), (), "JoinCondition", None),
		"Type": (2, 2, (8, 0), (), "Type", None),
	}
	_prop_map_put_ = {
		"FilterText": ((1, LCID, 4, 0),()),
		"JoinCondition": ((5, LCID, 4, 0),()),
		"Type": ((2, LCID, 4, 0),()),
	}

class IAttachment(DispatchBaseClass):
	"""Represents a single file or Internet address attached to a field object."""
	CLSID = IID('{613E9BF2-7888-438F-979F-D05DAB87B9C8}')
	coclass_clsid = IID('{46A97504-127F-4A93-BCD0-889AF362754E}')

	# The method Field is actually a property, but must be used as a method to correctly pass the arguments
	def Field(self, FieldName=defaultNamedNotOptArg):
		"""The value of the specified field."""
		return self._ApplyTypes_(0, 2, (12, 0), ((8, 0),), u'Field', None,FieldName
			)

	# The method GetName is actually a property, but must be used as a method to correctly pass the arguments
	def GetName(self, ViewFormat=0):
		"""The attachment name."""
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(12, LCID, 2, (8, 0), ((3, 49),),ViewFormat
			)

	def Load(self, synchronize=defaultNamedNotOptArg, RootPath=defaultNamedNotOptArg):
		"""Downloads an attachment file to a client machine."""
		return self._oleobj_.InvokeTypes(16, LCID, 1, (24, 0), ((11, 0), (16392, 0)),synchronize
			, RootPath)

	def LockObject(self):
		"""Locks the object. Returns True if lock successful."""
		return self._oleobj_.InvokeTypes(2, LCID, 1, (11, 0), (),)

	def Post(self):
		"""Posts all changed values into database."""
		return self._oleobj_.InvokeTypes(6, LCID, 1, (24, 0), (),)

	def Refresh(self):
		"""Reads saved values, overwriting values in memory."""
		return self._oleobj_.InvokeTypes(7, LCID, 1, (24, 0), (),)

	def Rename(self, NewName=defaultNamedNotOptArg):
		"""Renames the attachment on the server. The attachment must be uploaded before calling this method."""
		return self._oleobj_.InvokeTypes(23, LCID, 1, (24, 0), ((8, 1),),NewName
			)

	def Save(self, synchronize=defaultNamedNotOptArg):
		"""Uploads a file to the server."""
		return self._oleobj_.InvokeTypes(17, LCID, 1, (24, 0), ((11, 0),),synchronize
			)

	# The method SetField is actually a property, but must be used as a method to correctly pass the arguments
	def SetField(self, FieldName=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The value of the specified field."""
		return self._oleobj_.InvokeTypes(0, LCID, 4, (24, 0), ((8, 0), (12, 1)),FieldName
			, arg1)

	def UnLockObject(self):
		"""Unlocks the object."""
		return self._oleobj_.InvokeTypes(3, LCID, 1, (24, 0), (),)

	def Undo(self):
		"""Undoes changes to field values that have not been posted."""
		return self._oleobj_.InvokeTypes(8, LCID, 1, (24, 0), (),)

	_prop_map_get_ = {
		"AttachmentStorage": (18, 2, (9, 0), (), "AttachmentStorage", None),
		"AutoPost": (5, 2, (11, 0), (), "AutoPost", None),
		"Data": (14, 2, (12, 0), (), "Data", None),
		"Description": (13, 2, (8, 0), (), "Description", None),
		"DirectLink": (19, 2, (8, 0), (), "DirectLink", None),
		"FileName": (15, 2, (8, 0), (), "FileName", None),
		"FileSize": (21, 2, (3, 0), (), "FileSize", None),
		"ID": (4, 2, (12, 0), (), "ID", None),
		"IsLocked": (1, 2, (11, 0), (), "IsLocked", None),
		"LastModified": (22, 2, (7, 0), (), "LastModified", None),
		"Modified": (9, 2, (11, 0), (), "Modified", None),
		"Name": (12, 2, (8, 0), ((3, 49),), "Name", None),
		"ServerFileName": (20, 2, (8, 0), (), "ServerFileName", None),
		"Type": (11, 2, (3, 0), (), "Type", None),
		"Virtual": (10, 2, (11, 0), (), "Virtual", None),
	}
	_prop_map_put_ = {
		"AutoPost": ((5, LCID, 4, 0),()),
		"Description": ((13, LCID, 4, 0),()),
		"FileName": ((15, LCID, 4, 0),()),
		"Type": ((11, LCID, 4, 0),()),
	}
	# Default method for this class is 'Field'
	def __call__(self, FieldName=defaultNamedNotOptArg):
		"""The value of the specified field."""
		return self._ApplyTypes_(0, 2, (12, 0), ((8, 0),), '__call__', None,FieldName
			)

	# str(ob) and int(ob) will use __call__
	def __unicode__(self, *args):
		try:
			return unicode(self.__call__(*args))
		except pythoncom.com_error:
			return repr(self)
	def __str__(self, *args):
		return str(self.__unicode__(*args))
	def __int__(self, *args):
		return int(self.__call__(*args))

class IAttachmentFactory(DispatchBaseClass):
	"""Services to manage attachments of the current field object."""
	CLSID = IID('{C691D4B4-3924-4488-B554-A7E08842C625}')
	coclass_clsid = IID('{9D02F17C-BECD-4845-AA04-5326832104AA}')

	def AddItem(self, ItemData=defaultNamedNotOptArg):
		"""Creates a new item object."""
		ret = self._oleobj_.InvokeTypes(3, LCID, 1, (9, 0), ((12, 0),),ItemData
			)
		if ret is not None:
			ret = Dispatch(ret, u'AddItem', None)
		return ret

	def FactoryProperties(self, OwnerType=pythoncom.Missing, OwnerKey=pythoncom.Missing):
		"""Gets the owner type and key for the AttachmentFactory object."""
		return self._ApplyTypes_(9, 1, (24, 0), ((16392, 2), (16396, 2)), u'FactoryProperties', None,OwnerType
			, OwnerKey)

	# The method FetchLevel is actually a property, but must be used as a method to correctly pass the arguments
	def FetchLevel(self, FieldName=defaultNamedNotOptArg):
		"""The Fetch level for a field."""
		return self._oleobj_.InvokeTypes(7, LCID, 2, (2, 0), ((8, 1),),FieldName
			)

	# The method Item is actually a property, but must be used as a method to correctly pass the arguments
	def Item(self, ItemKey=defaultNamedNotOptArg):
		"""Gets an object managed by the factory by its key."""
		ret = self._oleobj_.InvokeTypes(0, LCID, 2, (9, 0), ((12, 0),),ItemKey
			)
		if ret is not None:
			ret = Dispatch(ret, u'Item', None)
		return ret

	# Result is of type IList
	def NewList(self, Filter=defaultNamedNotOptArg):
		"""Creates a list of objects according to the specified filter."""
		ret = self._oleobj_.InvokeTypes(1, LCID, 1, (9, 0), ((8, 0),),Filter
			)
		if ret is not None:
			ret = Dispatch(ret, u'NewList', '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')
		return ret

	def RemoveItem(self, ItemKey=defaultNamedNotOptArg):
		"""Removes item from the database. Removal takes place immediately, without a Post."""
		return self._oleobj_.InvokeTypes(4, LCID, 1, (24, 0), ((12, 0),),ItemKey
			)

	# The method SetFetchLevel is actually a property, but must be used as a method to correctly pass the arguments
	def SetFetchLevel(self, FieldName=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The Fetch level for a field."""
		return self._oleobj_.InvokeTypes(7, LCID, 4, (24, 0), ((8, 1), (2, 1)),FieldName
			, arg1)

	_prop_map_get_ = {
		"AttachmentStorage": (8, 2, (9, 0), (), "AttachmentStorage", None),
		# Method 'Fields' returns object of type 'IList'
		"Fields": (2, 2, (9, 0), (), "Fields", '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}'),
		"Filter": (5, 2, (9, 0), (), "Filter", None),
		"History": (6, 2, (9, 0), (), "History", None),
	}
	_prop_map_put_ = {
	}
	# Default method for this class is 'Item'
	def __call__(self, ItemKey=defaultNamedNotOptArg):
		"""Gets an object managed by the factory by its key."""
		ret = self._oleobj_.InvokeTypes(0, LCID, 2, (9, 0), ((12, 0),),ItemKey
			)
		if ret is not None:
			ret = Dispatch(ret, '__call__', None)
		return ret

	# str(ob) and int(ob) will use __call__
	def __unicode__(self, *args):
		try:
			return unicode(self.__call__(*args))
		except pythoncom.com_error:
			return repr(self)
	def __str__(self, *args):
		return str(self.__unicode__(*args))
	def __int__(self, *args):
		return int(self.__call__(*args))

class IAttachmentVcs(DispatchBaseClass):
	"""IAttachmentVcs Interface"""
	CLSID = IID('{CE4BEF63-1922-4C81-8C81-DC18FF94D8E7}')
	coclass_clsid = IID('{6C25C267-173B-45CB-8150-7BD1F9584981}')

	_prop_map_get_ = {
	}
	_prop_map_put_ = {
	}

class IAuditProperty(DispatchBaseClass):
	"""A property associated with an AuditRecord."""
	CLSID = IID('{DD1C7F94-5C11-416D-B11D-DA062309D43A}')
	coclass_clsid = IID('{64162251-9AE0-45BE-892D-535D8C5DD062}')

	# The method Field is actually a property, but must be used as a method to correctly pass the arguments
	def Field(self, FieldName=defaultNamedNotOptArg):
		"""The value of the specified field."""
		return self._ApplyTypes_(0, 2, (12, 0), ((8, 0),), u'Field', None,FieldName
			)

	def LockObject(self):
		"""Locks the object. Returns True if lock successful."""
		return self._oleobj_.InvokeTypes(2, LCID, 1, (11, 0), (),)

	def Post(self):
		"""Posts all changed values into database."""
		return self._oleobj_.InvokeTypes(6, LCID, 1, (24, 0), (),)

	def Refresh(self):
		"""Reads saved values, overwriting values in memory."""
		return self._oleobj_.InvokeTypes(7, LCID, 1, (24, 0), (),)

	# The method SetField is actually a property, but must be used as a method to correctly pass the arguments
	def SetField(self, FieldName=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The value of the specified field."""
		return self._oleobj_.InvokeTypes(0, LCID, 4, (24, 0), ((8, 0), (12, 1)),FieldName
			, arg1)

	def UnLockObject(self):
		"""Unlocks the object."""
		return self._oleobj_.InvokeTypes(3, LCID, 1, (24, 0), (),)

	def Undo(self):
		"""Undoes changes to field values that have not been posted."""
		return self._oleobj_.InvokeTypes(8, LCID, 1, (24, 0), (),)

	_prop_map_get_ = {
		"Action": (17, 2, (8, 0), (), "Action", None),
		"ActionID": (16, 2, (3, 0), (), "ActionID", None),
		"AutoPost": (5, 2, (11, 0), (), "AutoPost", None),
		"EntityID": (22, 2, (8, 0), (), "EntityID", None),
		"EntityType": (21, 2, (8, 0), (), "EntityType", None),
		"FieldName": (12, 2, (8, 0), (), "FieldName", None),
		"ID": (4, 2, (12, 0), (), "ID", None),
		"IsLocked": (1, 2, (11, 0), (), "IsLocked", None),
		"Modified": (9, 2, (11, 0), (), "Modified", None),
		"NewValue": (15, 2, (12, 0), (), "NewValue", None),
		"OldValue": (14, 2, (12, 0), (), "OldValue", None),
		"PropertyName": (13, 2, (8, 0), (), "PropertyName", None),
		"SessionID": (19, 2, (3, 0), (), "SessionID", None),
		"TableName": (11, 2, (8, 0), (), "TableName", None),
		"Time": (20, 2, (8, 0), (), "Time", None),
		"UserName": (18, 2, (8, 0), (), "UserName", None),
		"Virtual": (10, 2, (11, 0), (), "Virtual", None),
	}
	_prop_map_put_ = {
		"AutoPost": ((5, LCID, 4, 0),()),
	}
	# Default method for this class is 'Field'
	def __call__(self, FieldName=defaultNamedNotOptArg):
		"""The value of the specified field."""
		return self._ApplyTypes_(0, 2, (12, 0), ((8, 0),), '__call__', None,FieldName
			)

	# str(ob) and int(ob) will use __call__
	def __unicode__(self, *args):
		try:
			return unicode(self.__call__(*args))
		except pythoncom.com_error:
			return repr(self)
	def __str__(self, *args):
		return str(self.__unicode__(*args))
	def __int__(self, *args):
		return int(self.__call__(*args))

class IAuditPropertyFactory(DispatchBaseClass):
	"""Services for managing AuditProperty objects."""
	CLSID = IID('{A3D667D7-2285-4FE1-A799-8581C6D8DE70}')
	coclass_clsid = IID('{012F18E6-986E-4C65-A5D8-5F1696BC220E}')

	def AddItem(self, ItemData=defaultNamedNotOptArg):
		"""Creates a new item object."""
		ret = self._oleobj_.InvokeTypes(3, LCID, 1, (9, 0), ((12, 0),),ItemData
			)
		if ret is not None:
			ret = Dispatch(ret, u'AddItem', None)
		return ret

	# The method FetchLevel is actually a property, but must be used as a method to correctly pass the arguments
	def FetchLevel(self, FieldName=defaultNamedNotOptArg):
		"""The Fetch level for a field."""
		return self._oleobj_.InvokeTypes(7, LCID, 2, (2, 0), ((8, 1),),FieldName
			)

	# The method Item is actually a property, but must be used as a method to correctly pass the arguments
	def Item(self, ItemKey=defaultNamedNotOptArg):
		"""Gets an object managed by the factory by its key."""
		ret = self._oleobj_.InvokeTypes(0, LCID, 2, (9, 0), ((12, 0),),ItemKey
			)
		if ret is not None:
			ret = Dispatch(ret, u'Item', None)
		return ret

	# Result is of type IList
	def NewList(self, Filter=defaultNamedNotOptArg):
		"""Creates a list of objects according to the specified filter."""
		ret = self._oleobj_.InvokeTypes(1, LCID, 1, (9, 0), ((8, 0),),Filter
			)
		if ret is not None:
			ret = Dispatch(ret, u'NewList', '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')
		return ret

	def RemoveItem(self, ItemKey=defaultNamedNotOptArg):
		"""Removes item from the database. Removal takes place immediately, without a Post."""
		return self._oleobj_.InvokeTypes(4, LCID, 1, (24, 0), ((12, 0),),ItemKey
			)

	# The method SetFetchLevel is actually a property, but must be used as a method to correctly pass the arguments
	def SetFetchLevel(self, FieldName=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The Fetch level for a field."""
		return self._oleobj_.InvokeTypes(7, LCID, 4, (24, 0), ((8, 1), (2, 1)),FieldName
			, arg1)

	_prop_map_get_ = {
		# Method 'Fields' returns object of type 'IList'
		"Fields": (2, 2, (9, 0), (), "Fields", '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}'),
		"Filter": (5, 2, (9, 0), (), "Filter", None),
		"History": (6, 2, (9, 0), (), "History", None),
	}
	_prop_map_put_ = {
	}
	# Default method for this class is 'Item'
	def __call__(self, ItemKey=defaultNamedNotOptArg):
		"""Gets an object managed by the factory by its key."""
		ret = self._oleobj_.InvokeTypes(0, LCID, 2, (9, 0), ((12, 0),),ItemKey
			)
		if ret is not None:
			ret = Dispatch(ret, '__call__', None)
		return ret

	# str(ob) and int(ob) will use __call__
	def __unicode__(self, *args):
		try:
			return unicode(self.__call__(*args))
		except pythoncom.com_error:
			return repr(self)
	def __str__(self, *args):
		return str(self.__unicode__(*args))
	def __int__(self, *args):
		return int(self.__call__(*args))

class IAuditRecord(DispatchBaseClass):
	"""A data change that is tracked."""
	CLSID = IID('{1754ECE8-1386-456C-AA7C-AD448412EDA3}')
	coclass_clsid = IID('{9BD93246-17D5-4A2C-9318-41B5FDBF0B51}')

	# The method Field is actually a property, but must be used as a method to correctly pass the arguments
	def Field(self, FieldName=defaultNamedNotOptArg):
		"""The value of the specified field."""
		return self._ApplyTypes_(0, 2, (12, 0), ((8, 0),), u'Field', None,FieldName
			)

	def LockObject(self):
		"""Locks the object. Returns True if lock successful."""
		return self._oleobj_.InvokeTypes(2, LCID, 1, (11, 0), (),)

	def Post(self):
		"""Posts all changed values into database."""
		return self._oleobj_.InvokeTypes(6, LCID, 1, (24, 0), (),)

	def Refresh(self):
		"""Reads saved values, overwriting values in memory."""
		return self._oleobj_.InvokeTypes(7, LCID, 1, (24, 0), (),)

	# The method SetField is actually a property, but must be used as a method to correctly pass the arguments
	def SetField(self, FieldName=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The value of the specified field."""
		return self._oleobj_.InvokeTypes(0, LCID, 4, (24, 0), ((8, 0), (12, 1)),FieldName
			, arg1)

	def UnLockObject(self):
		"""Unlocks the object."""
		return self._oleobj_.InvokeTypes(3, LCID, 1, (24, 0), (),)

	def Undo(self):
		"""Undoes changes to field values that have not been posted."""
		return self._oleobj_.InvokeTypes(8, LCID, 1, (24, 0), (),)

	_prop_map_get_ = {
		"Action": (12, 2, (8, 0), (), "Action", None),
		"ActionID": (20, 2, (3, 0), (), "ActionID", None),
		"AuditPropertyFactory": (11, 2, (9, 0), (), "AuditPropertyFactory", None),
		"AutoPost": (5, 2, (11, 0), (), "AutoPost", None),
		"ContextID": (18, 2, (3, 0), (), "ContextID", None),
		"Description": (19, 2, (8, 0), (), "Description", None),
		"EntityID": (14, 2, (8, 0), (), "EntityID", None),
		"EntityType": (13, 2, (8, 0), (), "EntityType", None),
		"ID": (4, 2, (12, 0), (), "ID", None),
		"IsLocked": (1, 2, (11, 0), (), "IsLocked", None),
		"Modified": (9, 2, (11, 0), (), "Modified", None),
		"SessionID": (16, 2, (3, 0), (), "SessionID", None),
		"Time": (17, 2, (8, 0), (), "Time", None),
		"UserName": (15, 2, (8, 0), (), "UserName", None),
		"Virtual": (10, 2, (11, 0), (), "Virtual", None),
	}
	_prop_map_put_ = {
		"AutoPost": ((5, LCID, 4, 0),()),
	}
	# Default method for this class is 'Field'
	def __call__(self, FieldName=defaultNamedNotOptArg):
		"""The value of the specified field."""
		return self._ApplyTypes_(0, 2, (12, 0), ((8, 0),), '__call__', None,FieldName
			)

	# str(ob) and int(ob) will use __call__
	def __unicode__(self, *args):
		try:
			return unicode(self.__call__(*args))
		except pythoncom.com_error:
			return repr(self)
	def __str__(self, *args):
		return str(self.__unicode__(*args))
	def __int__(self, *args):
		return int(self.__call__(*args))

class IAuditRecordFactory(DispatchBaseClass):
	"""Services for managing AuditRecord objects."""
	CLSID = IID('{10747648-0DF0-4B8C-8853-7408C70D883A}')
	coclass_clsid = IID('{43A652C9-63BA-44DE-BDF4-642D0596EDD2}')

	def AddItem(self, ItemData=defaultNamedNotOptArg):
		"""Creates a new item object."""
		ret = self._oleobj_.InvokeTypes(3, LCID, 1, (9, 0), ((12, 0),),ItemData
			)
		if ret is not None:
			ret = Dispatch(ret, u'AddItem', None)
		return ret

	# The method FetchLevel is actually a property, but must be used as a method to correctly pass the arguments
	def FetchLevel(self, FieldName=defaultNamedNotOptArg):
		"""The Fetch level for a field."""
		return self._oleobj_.InvokeTypes(7, LCID, 2, (2, 0), ((8, 1),),FieldName
			)

	# The method Item is actually a property, but must be used as a method to correctly pass the arguments
	def Item(self, ItemKey=defaultNamedNotOptArg):
		"""Gets an object managed by the factory by its key."""
		ret = self._oleobj_.InvokeTypes(0, LCID, 2, (9, 0), ((12, 0),),ItemKey
			)
		if ret is not None:
			ret = Dispatch(ret, u'Item', None)
		return ret

	# Result is of type IList
	def NewList(self, Filter=defaultNamedNotOptArg):
		"""Creates a list of objects according to the specified filter."""
		ret = self._oleobj_.InvokeTypes(1, LCID, 1, (9, 0), ((8, 0),),Filter
			)
		if ret is not None:
			ret = Dispatch(ret, u'NewList', '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')
		return ret

	def RemoveItem(self, ItemKey=defaultNamedNotOptArg):
		"""Removes item from the database. Removal takes place immediately, without a Post."""
		return self._oleobj_.InvokeTypes(4, LCID, 1, (24, 0), ((12, 0),),ItemKey
			)

	# The method SetFetchLevel is actually a property, but must be used as a method to correctly pass the arguments
	def SetFetchLevel(self, FieldName=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The Fetch level for a field."""
		return self._oleobj_.InvokeTypes(7, LCID, 4, (24, 0), ((8, 1), (2, 1)),FieldName
			, arg1)

	_prop_map_get_ = {
		# Method 'Fields' returns object of type 'IList'
		"Fields": (2, 2, (9, 0), (), "Fields", '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}'),
		"Filter": (5, 2, (9, 0), (), "Filter", None),
		"History": (6, 2, (9, 0), (), "History", None),
	}
	_prop_map_put_ = {
	}
	# Default method for this class is 'Item'
	def __call__(self, ItemKey=defaultNamedNotOptArg):
		"""Gets an object managed by the factory by its key."""
		ret = self._oleobj_.InvokeTypes(0, LCID, 2, (9, 0), ((12, 0),),ItemKey
			)
		if ret is not None:
			ret = Dispatch(ret, '__call__', None)
		return ret

	# str(ob) and int(ob) will use __call__
	def __unicode__(self, *args):
		try:
			return unicode(self.__call__(*args))
		except pythoncom.com_error:
			return repr(self)
	def __str__(self, *args):
		return str(self.__unicode__(*args))
	def __int__(self, *args):
		return int(self.__call__(*args))

class IAuditable(DispatchBaseClass):
	"""Services to allow changes to an entity to be tracked."""
	CLSID = IID('{3DE03C28-EC6B-4953-96C2-E3216051C907}')
	coclass_clsid = IID('{D70D7E57-CD6F-4FCA-954F-D73C30B9FD90}')

	_prop_map_get_ = {
		"AuditPropertyFactory": (2, 2, (9, 0), (), "AuditPropertyFactory", None),
		"AuditRecordFactory": (1, 2, (9, 0), (), "AuditRecordFactory", None),
	}
	_prop_map_put_ = {
	}

class IBPComponent(DispatchBaseClass):
	"""A Business Process Component."""
	CLSID = IID('{42AD6542-9DBC-4A66-BC3F-7692832D33CB}')
	coclass_clsid = IID('{C64F7478-FF48-49A7-8260-251EAB8D5B7C}')

	def AddIteration(self):
		"""AddIteration"""
		ret = self._oleobj_.InvokeTypes(23, LCID, 1, (9, 0), (),)
		if ret is not None:
			ret = Dispatch(ret, u'AddIteration', None)
		return ret

	def DeleteIteration(self, pVal=defaultNamedNotOptArg):
		"""DeleteIteration"""
		return self._oleobj_.InvokeTypes(24, LCID, 1, (24, 0), ((9, 1),),pVal
			)

	# The method Field is actually a property, but must be used as a method to correctly pass the arguments
	def Field(self, FieldName=defaultNamedNotOptArg):
		"""The value of the specified field."""
		return self._ApplyTypes_(0, 2, (12, 0), ((8, 0),), u'Field', None,FieldName
			)

	def LockObject(self):
		"""Locks the object. Returns True if lock successful."""
		return self._oleobj_.InvokeTypes(2, LCID, 1, (11, 0), (),)

	def Post(self):
		"""Posts all changed values into database."""
		return self._oleobj_.InvokeTypes(6, LCID, 1, (24, 0), (),)

	def Refresh(self):
		"""Reads saved values, overwriting values in memory."""
		return self._oleobj_.InvokeTypes(7, LCID, 1, (24, 0), (),)

	# The method SetField is actually a property, but must be used as a method to correctly pass the arguments
	def SetField(self, FieldName=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The value of the specified field."""
		return self._oleobj_.InvokeTypes(0, LCID, 4, (24, 0), ((8, 0), (12, 1)),FieldName
			, arg1)

	def UnLockObject(self):
		"""Unlocks the object."""
		return self._oleobj_.InvokeTypes(3, LCID, 1, (24, 0), (),)

	def Undo(self):
		"""Undoes changes to field values that have not been posted."""
		return self._oleobj_.InvokeTypes(8, LCID, 1, (24, 0), (),)

	_prop_map_get_ = {
		"Attachments": (12, 2, (9, 0), (), "Attachments", None),
		"AutoPost": (5, 2, (11, 0), (), "AutoPost", None),
		# Method 'BPParams' returns object of type 'IList'
		"BPParams": (21, 2, (9, 0), (), "BPParams", '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}'),
		"Component": (14, 2, (9, 0), (), "Component", None),
		"FailureCondition": (17, 2, (8, 0), (), "FailureCondition", None),
		"Group": (19, 2, (9, 0), (), "Group", None),
		"HasAttachment": (13, 2, (11, 0), (), "HasAttachment", None),
		"History": (11, 2, (9, 0), (), "History", None),
		"ID": (4, 2, (12, 0), (), "ID", None),
		"IsLocked": (1, 2, (11, 0), (), "IsLocked", None),
		# Method 'Iterations' returns object of type 'IList'
		"Iterations": (22, 2, (9, 0), (), "Iterations", '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}'),
		"Modified": (9, 2, (11, 0), (), "Modified", None),
		"Name": (18, 2, (8, 0), (), "Name", None),
		"Order": (15, 2, (3, 0), (), "Order", None),
		"Test": (16, 2, (9, 0), (), "Test", None),
		"Virtual": (10, 2, (11, 0), (), "Virtual", None),
	}
	_prop_map_put_ = {
		"AutoPost": ((5, LCID, 4, 0),()),
		"FailureCondition": ((17, LCID, 4, 0),()),
		"GroupID": ((20, LCID, 4, 0),()),
		"Order": ((15, LCID, 4, 0),()),
	}
	# Default method for this class is 'Field'
	def __call__(self, FieldName=defaultNamedNotOptArg):
		"""The value of the specified field."""
		return self._ApplyTypes_(0, 2, (12, 0), ((8, 0),), '__call__', None,FieldName
			)

	# str(ob) and int(ob) will use __call__
	def __unicode__(self, *args):
		try:
			return unicode(self.__call__(*args))
		except pythoncom.com_error:
			return repr(self)
	def __str__(self, *args):
		return str(self.__unicode__(*args))
	def __int__(self, *args):
		return int(self.__call__(*args))

class IBPGroup(DispatchBaseClass):
	"""Support for grouping business process components so that they iterate as a unit."""
	CLSID = IID('{DB466018-78FF-4645-9B45-B32F823C07F3}')
	coclass_clsid = IID('{0940D6D1-1A04-4274-8568-7F1C82BFCAFA}')

	def AddBPComponent(self, pBPComponent=defaultNamedNotOptArg):
		"""Add a business process component to the group."""
		return self._oleobj_.InvokeTypes(2, LCID, 1, (24, 0), ((9, 1),),pBPComponent
			)

	# Result is of type IList
	def BPComponents(self):
		"""A list of the BPComponent objects that belong to the group."""
		ret = self._oleobj_.InvokeTypes(1, LCID, 1, (9, 0), (),)
		if ret is not None:
			ret = Dispatch(ret, u'BPComponents', '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')
		return ret

	def DeleteBPComponent(self, pBPComponent=defaultNamedNotOptArg):
		"""Removes a component from the group."""
		return self._oleobj_.InvokeTypes(3, LCID, 1, (24, 0), ((9, 1),),pBPComponent
			)

	_prop_map_get_ = {
		"ID": (4, 2, (3, 0), (), "ID", None),
	}
	_prop_map_put_ = {
	}

class IBPHistoryRecord(DispatchBaseClass):
	"""IBPHistoryRecord Interface"""
	CLSID = IID('{E1582AF7-780E-4D93-9771-E196205F96C7}')
	coclass_clsid = IID('{27F5B75D-9DB6-44A3-AAA7-0713836A15A6}')

	_prop_map_get_ = {
		"Changer": (3, 2, (8, 0), (), "Changer", None),
		"Description": (1, 2, (8, 0), (), "Description", None),
		"Time": (2, 2, (8, 0), (), "Time", None),
	}
	_prop_map_put_ = {
	}

class IBPIteration(DispatchBaseClass):
	"""The design-time definition of a Business Process Component iteration."""
	CLSID = IID('{F96FB8D9-0CD2-4B82-85FA-AB07C435F87F}')
	coclass_clsid = IID('{6E67A18A-81DD-4546-9D64-10701F5E9558}')

	def AddParam(self, pVal=defaultNamedNotOptArg):
		"""Add a BPParameter Object to the iteration."""
		ret = self._oleobj_.InvokeTypes(16, LCID, 1, (9, 0), ((9, 1),),pVal
			)
		if ret is not None:
			ret = Dispatch(ret, u'AddParam', None)
		return ret

	def DeleteIterationParams(self):
		"""Delete all the parameters from the iteration."""
		return self._oleobj_.InvokeTypes(18, LCID, 1, (24, 0), (),)

	def DeleteParam(self, pVal=defaultNamedNotOptArg):
		"""Delete a BPParameter from the iteration."""
		return self._oleobj_.InvokeTypes(17, LCID, 1, (24, 0), ((9, 1),),pVal
			)

	# The method Field is actually a property, but must be used as a method to correctly pass the arguments
	def Field(self, FieldName=defaultNamedNotOptArg):
		"""The value of the specified field."""
		return self._ApplyTypes_(0, 2, (12, 0), ((8, 0),), u'Field', None,FieldName
			)

	def LockObject(self):
		"""Locks the object. Returns True if lock successful."""
		return self._oleobj_.InvokeTypes(2, LCID, 1, (11, 0), (),)

	def Post(self):
		"""Posts all changed values into database."""
		return self._oleobj_.InvokeTypes(6, LCID, 1, (24, 0), (),)

	def Refresh(self):
		"""Reads saved values, overwriting values in memory."""
		return self._oleobj_.InvokeTypes(7, LCID, 1, (24, 0), (),)

	# The method SetField is actually a property, but must be used as a method to correctly pass the arguments
	def SetField(self, FieldName=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The value of the specified field."""
		return self._oleobj_.InvokeTypes(0, LCID, 4, (24, 0), ((8, 0), (12, 1)),FieldName
			, arg1)

	def UnLockObject(self):
		"""Unlocks the object."""
		return self._oleobj_.InvokeTypes(3, LCID, 1, (24, 0), (),)

	def Undo(self):
		"""Undoes changes to field values that have not been posted."""
		return self._oleobj_.InvokeTypes(8, LCID, 1, (24, 0), (),)

	_prop_map_get_ = {
		"AutoPost": (5, 2, (11, 0), (), "AutoPost", None),
		"BPComponent": (14, 2, (9, 0), (), "BPComponent", None),
		"ID": (4, 2, (12, 0), (), "ID", None),
		"IsLocked": (1, 2, (11, 0), (), "IsLocked", None),
		# Method 'IterationParams' returns object of type 'IList'
		"IterationParams": (15, 2, (9, 0), (), "IterationParams", '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}'),
		"Modified": (9, 2, (11, 0), (), "Modified", None),
		"Order": (19, 2, (3, 0), (), "Order", None),
		"Virtual": (10, 2, (11, 0), (), "Virtual", None),
	}
	_prop_map_put_ = {
		"AutoPost": ((5, LCID, 4, 0),()),
		"Order": ((19, LCID, 4, 0),()),
	}
	# Default method for this class is 'Field'
	def __call__(self, FieldName=defaultNamedNotOptArg):
		"""The value of the specified field."""
		return self._ApplyTypes_(0, 2, (12, 0), ((8, 0),), '__call__', None,FieldName
			)

	# str(ob) and int(ob) will use __call__
	def __unicode__(self, *args):
		try:
			return unicode(self.__call__(*args))
		except pythoncom.com_error:
			return repr(self)
	def __str__(self, *args):
		return str(self.__unicode__(*args))
	def __int__(self, *args):
		return int(self.__call__(*args))

class IBPIterationParam(DispatchBaseClass):
	"""An instance of a Business Process Component parameter that belongs to a single interation of the component."""
	CLSID = IID('{B40742AD-3BED-40B4-BD58-0F24E41ACDD2}')
	coclass_clsid = IID('{F0D306D2-2BE8-43E8-B229-9B8EA279B8D5}')

	# The method Field is actually a property, but must be used as a method to correctly pass the arguments
	def Field(self, FieldName=defaultNamedNotOptArg):
		"""The value of the specified field."""
		return self._ApplyTypes_(0, 2, (12, 0), ((8, 0),), u'Field', None,FieldName
			)

	def LockObject(self):
		"""Locks the object. Returns True if lock successful."""
		return self._oleobj_.InvokeTypes(2, LCID, 1, (11, 0), (),)

	def Post(self):
		"""Posts all changed values into database."""
		return self._oleobj_.InvokeTypes(6, LCID, 1, (24, 0), (),)

	def Refresh(self):
		"""Reads saved values, overwriting values in memory."""
		return self._oleobj_.InvokeTypes(7, LCID, 1, (24, 0), (),)

	# The method SetField is actually a property, but must be used as a method to correctly pass the arguments
	def SetField(self, FieldName=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The value of the specified field."""
		return self._oleobj_.InvokeTypes(0, LCID, 4, (24, 0), ((8, 0), (12, 1)),FieldName
			, arg1)

	def UnLockObject(self):
		"""Unlocks the object."""
		return self._oleobj_.InvokeTypes(3, LCID, 1, (24, 0), (),)

	def Undo(self):
		"""Undoes changes to field values that have not been posted."""
		return self._oleobj_.InvokeTypes(8, LCID, 1, (24, 0), (),)

	_prop_map_get_ = {
		"AutoPost": (5, 2, (11, 0), (), "AutoPost", None),
		"BPParameter": (20, 2, (9, 0), (), "BPParameter", None),
		"ID": (4, 2, (12, 0), (), "ID", None),
		"IsLocked": (1, 2, (11, 0), (), "IsLocked", None),
		"Modified": (9, 2, (11, 0), (), "Modified", None),
		"Value": (21, 2, (8, 0), (), "Value", None),
		"Virtual": (10, 2, (11, 0), (), "Virtual", None),
	}
	_prop_map_put_ = {
		"AutoPost": ((5, LCID, 4, 0),()),
		"Value": ((21, LCID, 4, 0),()),
	}
	# Default method for this class is 'Field'
	def __call__(self, FieldName=defaultNamedNotOptArg):
		"""The value of the specified field."""
		return self._ApplyTypes_(0, 2, (12, 0), ((8, 0),), '__call__', None,FieldName
			)

	# str(ob) and int(ob) will use __call__
	def __unicode__(self, *args):
		try:
			return unicode(self.__call__(*args))
		except pythoncom.com_error:
			return repr(self)
	def __str__(self, *args):
		return str(self.__unicode__(*args))
	def __int__(self, *args):
		return int(self.__call__(*args))

class IBPParam(DispatchBaseClass):
	"""A Business Process Component parameter."""
	CLSID = IID('{51A0C1D6-F748-41D1-988F-B0B93013F1BC}')
	coclass_clsid = IID('{67CDC9EF-6EEB-4E19-80E1-77855970409A}')

	# The method Field is actually a property, but must be used as a method to correctly pass the arguments
	def Field(self, FieldName=defaultNamedNotOptArg):
		"""The value of the specified field."""
		return self._ApplyTypes_(0, 2, (12, 0), ((8, 0),), u'Field', None,FieldName
			)

	def LockObject(self):
		"""Locks the object. Returns True if lock successful."""
		return self._oleobj_.InvokeTypes(2, LCID, 1, (11, 0), (),)

	def Post(self):
		"""Posts all changed values into database."""
		return self._oleobj_.InvokeTypes(6, LCID, 1, (24, 0), (),)

	def Refresh(self):
		"""Reads saved values, overwriting values in memory."""
		return self._oleobj_.InvokeTypes(7, LCID, 1, (24, 0), (),)

	# The method SetField is actually a property, but must be used as a method to correctly pass the arguments
	def SetField(self, FieldName=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The value of the specified field."""
		return self._oleobj_.InvokeTypes(0, LCID, 4, (24, 0), ((8, 0), (12, 1)),FieldName
			, arg1)

	def UnLockObject(self):
		"""Unlocks the object."""
		return self._oleobj_.InvokeTypes(3, LCID, 1, (24, 0), (),)

	def Undo(self):
		"""Undoes changes to field values that have not been posted."""
		return self._oleobj_.InvokeTypes(8, LCID, 1, (24, 0), (),)

	_prop_map_get_ = {
		"AutoPost": (5, 2, (11, 0), (), "AutoPost", None),
		"BPComponent": (24, 2, (9, 0), (), "BPComponent", None),
		"ComponentParam": (22, 2, (9, 0), (), "ComponentParam", None),
		"ComponentParamIsOut": (26, 2, (3, 0), (), "ComponentParamIsOut", None),
		"ComponentParamName": (23, 2, (8, 0), (), "ComponentParamName", None),
		"ComponentParamOrder": (25, 2, (3, 0), (), "ComponentParamOrder", None),
		"ID": (4, 2, (12, 0), (), "ID", None),
		"IsLocked": (1, 2, (11, 0), (), "IsLocked", None),
		"Modified": (9, 2, (11, 0), (), "Modified", None),
		"Reference": (20, 2, (9, 0), (), "Reference", None),
		"Type": (21, 2, (3, 0), (), "Type", None),
		"Virtual": (10, 2, (11, 0), (), "Virtual", None),
	}
	_prop_map_put_ = {
		"AutoPost": ((5, LCID, 4, 0),()),
		"Reference": ((20, LCID, 4, 0),()),
	}
	# Default method for this class is 'Field'
	def __call__(self, FieldName=defaultNamedNotOptArg):
		"""The value of the specified field."""
		return self._ApplyTypes_(0, 2, (12, 0), ((8, 0),), '__call__', None,FieldName
			)

	# str(ob) and int(ob) will use __call__
	def __unicode__(self, *args):
		try:
			return unicode(self.__call__(*args))
		except pythoncom.com_error:
			return repr(self)
	def __str__(self, *args):
		return str(self.__unicode__(*args))
	def __int__(self, *args):
		return int(self.__call__(*args))

class IBPStepParam(DispatchBaseClass):
	"""Services for managing parameters for manual runs of Business Process Component tests."""
	CLSID = IID('{6BA5E493-366C-4068-B572-93D27D405DFE}')
	coclass_clsid = IID('{2824DF5E-75A6-475C-AE4D-3DC4E62EA4F5}')

	# The method Field is actually a property, but must be used as a method to correctly pass the arguments
	def Field(self, FieldName=defaultNamedNotOptArg):
		"""The value of the specified field."""
		return self._ApplyTypes_(0, 2, (12, 0), ((8, 0),), u'Field', None,FieldName
			)

	def LockObject(self):
		"""Locks the object. Returns True if lock successful."""
		return self._oleobj_.InvokeTypes(2, LCID, 1, (11, 0), (),)

	def Post(self):
		"""Posts all changed values into database."""
		return self._oleobj_.InvokeTypes(6, LCID, 1, (24, 0), (),)

	def Refresh(self):
		"""Reads saved values, overwriting values in memory."""
		return self._oleobj_.InvokeTypes(7, LCID, 1, (24, 0), (),)

	# The method SetField is actually a property, but must be used as a method to correctly pass the arguments
	def SetField(self, FieldName=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The value of the specified field."""
		return self._oleobj_.InvokeTypes(0, LCID, 4, (24, 0), ((8, 0), (12, 1)),FieldName
			, arg1)

	def UnLockObject(self):
		"""Unlocks the object."""
		return self._oleobj_.InvokeTypes(3, LCID, 1, (24, 0), (),)

	def Undo(self):
		"""Undoes changes to field values that have not been posted."""
		return self._oleobj_.InvokeTypes(8, LCID, 1, (24, 0), (),)

	_prop_map_get_ = {
		"AutoPost": (5, 2, (11, 0), (), "AutoPost", None),
		"ID": (4, 2, (12, 0), (), "ID", None),
		"IsLocked": (1, 2, (11, 0), (), "IsLocked", None),
		"Modified": (9, 2, (11, 0), (), "Modified", None),
		"Name": (14, 2, (8, 0), (), "Name", None),
		"ReferencedParamID": (17, 2, (3, 0), (), "ReferencedParamID", None),
		"StepID": (18, 2, (3, 0), (), "StepID", None),
		"Type": (16, 2, (3, 0), (), "Type", None),
		"Value": (15, 2, (8, 0), (), "Value", None),
		"Virtual": (10, 2, (11, 0), (), "Virtual", None),
	}
	_prop_map_put_ = {
		"AutoPost": ((5, LCID, 4, 0),()),
		"Name": ((14, LCID, 4, 0),()),
		"ReferencedParamID": ((17, LCID, 4, 0),()),
		"Type": ((16, LCID, 4, 0),()),
		"Value": ((15, LCID, 4, 0),()),
	}
	# Default method for this class is 'Field'
	def __call__(self, FieldName=defaultNamedNotOptArg):
		"""The value of the specified field."""
		return self._ApplyTypes_(0, 2, (12, 0), ((8, 0),), '__call__', None,FieldName
			)

	# str(ob) and int(ob) will use __call__
	def __unicode__(self, *args):
		try:
			return unicode(self.__call__(*args))
		except pythoncom.com_error:
			return repr(self)
	def __str__(self, *args):
		return str(self.__unicode__(*args))
	def __int__(self, *args):
		return int(self.__call__(*args))

class IBaseFactory(DispatchBaseClass):
	"""Provides basic object basic factory services, such as adding and removing factory child objects and creating lists of child objects."""
	CLSID = IID('{F13E4E0F-2BF2-41A2-97B1-06AD03204518}')
	coclass_clsid = IID('{396C2A71-28E7-4410-8BD9-380953D4B606}')

	def AddItem(self, ItemData=defaultNamedNotOptArg):
		"""Creates a new item object."""
		ret = self._oleobj_.InvokeTypes(3, LCID, 1, (9, 0), ((12, 0),),ItemData
			)
		if ret is not None:
			ret = Dispatch(ret, u'AddItem', None)
		return ret

	# The method FetchLevel is actually a property, but must be used as a method to correctly pass the arguments
	def FetchLevel(self, FieldName=defaultNamedNotOptArg):
		"""The Fetch level for a field."""
		return self._oleobj_.InvokeTypes(7, LCID, 2, (2, 0), ((8, 1),),FieldName
			)

	# The method Item is actually a property, but must be used as a method to correctly pass the arguments
	def Item(self, ItemKey=defaultNamedNotOptArg):
		"""Gets an object managed by the factory by its key."""
		ret = self._oleobj_.InvokeTypes(0, LCID, 2, (9, 0), ((12, 0),),ItemKey
			)
		if ret is not None:
			ret = Dispatch(ret, u'Item', None)
		return ret

	# Result is of type IList
	def NewList(self, Filter=defaultNamedNotOptArg):
		"""Creates a list of objects according to the specified filter."""
		ret = self._oleobj_.InvokeTypes(1, LCID, 1, (9, 0), ((8, 0),),Filter
			)
		if ret is not None:
			ret = Dispatch(ret, u'NewList', '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')
		return ret

	def RemoveItem(self, ItemKey=defaultNamedNotOptArg):
		"""Removes item from the database. Removal takes place immediately, without a Post."""
		return self._oleobj_.InvokeTypes(4, LCID, 1, (24, 0), ((12, 0),),ItemKey
			)

	# The method SetFetchLevel is actually a property, but must be used as a method to correctly pass the arguments
	def SetFetchLevel(self, FieldName=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The Fetch level for a field."""
		return self._oleobj_.InvokeTypes(7, LCID, 4, (24, 0), ((8, 1), (2, 1)),FieldName
			, arg1)

	_prop_map_get_ = {
		# Method 'Fields' returns object of type 'IList'
		"Fields": (2, 2, (9, 0), (), "Fields", '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}'),
		"Filter": (5, 2, (9, 0), (), "Filter", None),
		"History": (6, 2, (9, 0), (), "History", None),
	}
	_prop_map_put_ = {
	}
	# Default method for this class is 'Item'
	def __call__(self, ItemKey=defaultNamedNotOptArg):
		"""Gets an object managed by the factory by its key."""
		ret = self._oleobj_.InvokeTypes(0, LCID, 2, (9, 0), ((12, 0),),ItemKey
			)
		if ret is not None:
			ret = Dispatch(ret, '__call__', None)
		return ret

	# str(ob) and int(ob) will use __call__
	def __unicode__(self, *args):
		try:
			return unicode(self.__call__(*args))
		except pythoncom.com_error:
			return repr(self)
	def __str__(self, *args):
		return str(self.__unicode__(*args))
	def __int__(self, *args):
		return int(self.__call__(*args))

class IBaseFactory2(DispatchBaseClass):
	"""Provides basic object basic factory services, such as adding and removing factory child objects and creating lists of child objects."""
	CLSID = IID('{3E46BE58-4943-48AA-BA08-38EEBA837A04}')
	coclass_clsid = IID('{396C2A71-28E7-4410-8BD9-380953D4B606}')

	_prop_map_get_ = {
		"GroupingManager": (1, 2, (9, 0), (), "GroupingManager", None),
		"GroupingSupported": (2, 2, (11, 0), (), "GroupingSupported", None),
	}
	_prop_map_put_ = {
	}

class IBaseFactoryEx(DispatchBaseClass):
	"""Enhances the IBaseFactory interface to support mailing."""
	CLSID = IID('{B1F47936-EFAC-4AEE-9876-8110B16F037D}')
	coclass_clsid = None

	def AddItem(self, ItemData=defaultNamedNotOptArg):
		"""Creates a new item object."""
		ret = self._oleobj_.InvokeTypes(3, LCID, 1, (9, 0), ((12, 0),),ItemData
			)
		if ret is not None:
			ret = Dispatch(ret, u'AddItem', None)
		return ret

	# The method FetchLevel is actually a property, but must be used as a method to correctly pass the arguments
	def FetchLevel(self, FieldName=defaultNamedNotOptArg):
		"""The Fetch level for a field."""
		return self._oleobj_.InvokeTypes(7, LCID, 2, (2, 0), ((8, 1),),FieldName
			)

	# The method Item is actually a property, but must be used as a method to correctly pass the arguments
	def Item(self, ItemKey=defaultNamedNotOptArg):
		"""Gets an object managed by the factory by its key."""
		ret = self._oleobj_.InvokeTypes(0, LCID, 2, (9, 0), ((12, 0),),ItemKey
			)
		if ret is not None:
			ret = Dispatch(ret, u'Item', None)
		return ret

	def Mail(self, Items=defaultNamedNotOptArg, SendTo=defaultNamedNotOptArg, SendCc=u'', Option=0
			, Subject=u'', Comment=u''):
		"""Mails the list of IBase Factory Items. 'Items' is a list of ID numbers."""
		return self._ApplyTypes_(8, 1, (24, 32), ((12, 1), (8, 1), (8, 49), (3, 49), (8, 49), (8, 49)), u'Mail', None,Items
			, SendTo, SendCc, Option, Subject, Comment
			)

	# Result is of type IList
	def NewList(self, Filter=defaultNamedNotOptArg):
		"""Creates a list of objects according to the specified filter."""
		ret = self._oleobj_.InvokeTypes(1, LCID, 1, (9, 0), ((8, 0),),Filter
			)
		if ret is not None:
			ret = Dispatch(ret, u'NewList', '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')
		return ret

	def RemoveItem(self, ItemKey=defaultNamedNotOptArg):
		"""Removes item from the database. Removal takes place immediately, without a Post."""
		return self._oleobj_.InvokeTypes(4, LCID, 1, (24, 0), ((12, 0),),ItemKey
			)

	# The method SetFetchLevel is actually a property, but must be used as a method to correctly pass the arguments
	def SetFetchLevel(self, FieldName=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The Fetch level for a field."""
		return self._oleobj_.InvokeTypes(7, LCID, 4, (24, 0), ((8, 1), (2, 1)),FieldName
			, arg1)

	_prop_map_get_ = {
		# Method 'Fields' returns object of type 'IList'
		"Fields": (2, 2, (9, 0), (), "Fields", '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}'),
		"Filter": (5, 2, (9, 0), (), "Filter", None),
		"History": (6, 2, (9, 0), (), "History", None),
	}
	_prop_map_put_ = {
	}
	# Default method for this class is 'Item'
	def __call__(self, ItemKey=defaultNamedNotOptArg):
		"""Gets an object managed by the factory by its key."""
		ret = self._oleobj_.InvokeTypes(0, LCID, 2, (9, 0), ((12, 0),),ItemKey
			)
		if ret is not None:
			ret = Dispatch(ret, '__call__', None)
		return ret

	# str(ob) and int(ob) will use __call__
	def __unicode__(self, *args):
		try:
			return unicode(self.__call__(*args))
		except pythoncom.com_error:
			return repr(self)
	def __str__(self, *args):
		return str(self.__unicode__(*args))
	def __int__(self, *args):
		return int(self.__call__(*args))

class IBaseField(DispatchBaseClass):
	"""Represents a basic data field or entity, such as an attachment. Use the properties and methods in IBaseField to perform  field-related actions, such as setting and refreshing field values."""
	CLSID = IID('{E2F29752-72F0-42DB-995C-3DB385F4CCE5}')
	coclass_clsid = None

	# The method Field is actually a property, but must be used as a method to correctly pass the arguments
	def Field(self, FieldName=defaultNamedNotOptArg):
		"""The value of the specified field."""
		return self._ApplyTypes_(0, 2, (12, 0), ((8, 0),), u'Field', None,FieldName
			)

	def LockObject(self):
		"""Locks the object. Returns True if lock successful."""
		return self._oleobj_.InvokeTypes(2, LCID, 1, (11, 0), (),)

	def Post(self):
		"""Posts all changed values into database."""
		return self._oleobj_.InvokeTypes(6, LCID, 1, (24, 0), (),)

	def Refresh(self):
		"""Reads saved values, overwriting values in memory."""
		return self._oleobj_.InvokeTypes(7, LCID, 1, (24, 0), (),)

	# The method SetField is actually a property, but must be used as a method to correctly pass the arguments
	def SetField(self, FieldName=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The value of the specified field."""
		return self._oleobj_.InvokeTypes(0, LCID, 4, (24, 0), ((8, 0), (12, 1)),FieldName
			, arg1)

	def UnLockObject(self):
		"""Unlocks the object."""
		return self._oleobj_.InvokeTypes(3, LCID, 1, (24, 0), (),)

	def Undo(self):
		"""Undoes changes to field values that have not been posted."""
		return self._oleobj_.InvokeTypes(8, LCID, 1, (24, 0), (),)

	_prop_map_get_ = {
		"AutoPost": (5, 2, (11, 0), (), "AutoPost", None),
		"ID": (4, 2, (12, 0), (), "ID", None),
		"IsLocked": (1, 2, (11, 0), (), "IsLocked", None),
		"Modified": (9, 2, (11, 0), (), "Modified", None),
		"Virtual": (10, 2, (11, 0), (), "Virtual", None),
	}
	_prop_map_put_ = {
		"AutoPost": ((5, LCID, 4, 0),()),
	}
	# Default method for this class is 'Field'
	def __call__(self, FieldName=defaultNamedNotOptArg):
		"""The value of the specified field."""
		return self._ApplyTypes_(0, 2, (12, 0), ((8, 0),), '__call__', None,FieldName
			)

	# str(ob) and int(ob) will use __call__
	def __unicode__(self, *args):
		try:
			return unicode(self.__call__(*args))
		except pythoncom.com_error:
			return repr(self)
	def __str__(self, *args):
		return str(self.__unicode__(*args))
	def __int__(self, *args):
		return int(self.__call__(*args))

class IBaseField2(DispatchBaseClass):
	"""Represents a basic data field or entity, such as an attachment."""
	CLSID = IID('{BE73DDAA-AD0F-4A89-8936-0EDA17599273}')
	coclass_clsid = IID('{64162251-9AE0-45BE-892D-535D8C5DD062}')

	# Result is of type IMultiValue
	# The method FieldMultiValue is actually a property, but must be used as a method to correctly pass the arguments
	def FieldMultiValue(self, FieldName=defaultNamedNotOptArg):
		"""The MultiValue object of the specified field."""
		ret = self._oleobj_.InvokeTypes(2, LCID, 2, (9, 0), ((8, 0),),FieldName
			)
		if ret is not None:
			ret = Dispatch(ret, u'FieldMultiValue', '{EB180CC0-6FDE-4A1E-A68E-F106EEED5E15}')
		return ret

	# The method SetFieldMultiValue is actually a property, but must be used as a method to correctly pass the arguments
	def SetFieldMultiValue(self, FieldName=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The MultiValue object of the specified field."""
		return self._oleobj_.InvokeTypes(2, LCID, 4, (24, 0), ((8, 0), (9, 1)),FieldName
			, arg1)

	_prop_map_get_ = {
		"AutoUnlock": (1, 2, (11, 0), (), "AutoUnlock", None),
	}
	_prop_map_put_ = {
		"AutoUnlock": ((1, LCID, 4, 0),()),
	}

class IBaseFieldEx(DispatchBaseClass):
	"""Represents a basic data field or entity, such as an attachment. Use the properties and methods to perform  field-related actions, such as setting and refreshing field values."""
	CLSID = IID('{8937B744-DE14-4E34-9A56-4E9E308B1863}')
	coclass_clsid = IID('{4FFF066E-4D6B-444B-94B1-73209211C62B}')

	# The method Field is actually a property, but must be used as a method to correctly pass the arguments
	def Field(self, FieldName=defaultNamedNotOptArg):
		"""The value of the specified field."""
		return self._ApplyTypes_(0, 2, (12, 0), ((8, 0),), u'Field', None,FieldName
			)

	def LockObject(self):
		"""Locks the object. Returns True if lock successful."""
		return self._oleobj_.InvokeTypes(2, LCID, 1, (11, 0), (),)

	def Post(self):
		"""Posts all changed values into database."""
		return self._oleobj_.InvokeTypes(6, LCID, 1, (24, 0), (),)

	def Refresh(self):
		"""Reads saved values, overwriting values in memory."""
		return self._oleobj_.InvokeTypes(7, LCID, 1, (24, 0), (),)

	# The method SetField is actually a property, but must be used as a method to correctly pass the arguments
	def SetField(self, FieldName=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The value of the specified field."""
		return self._oleobj_.InvokeTypes(0, LCID, 4, (24, 0), ((8, 0), (12, 1)),FieldName
			, arg1)

	def UnLockObject(self):
		"""Unlocks the object."""
		return self._oleobj_.InvokeTypes(3, LCID, 1, (24, 0), (),)

	def Undo(self):
		"""Undoes changes to field values that have not been posted."""
		return self._oleobj_.InvokeTypes(8, LCID, 1, (24, 0), (),)

	_prop_map_get_ = {
		"Attachments": (12, 2, (9, 0), (), "Attachments", None),
		"AutoPost": (5, 2, (11, 0), (), "AutoPost", None),
		"HasAttachment": (13, 2, (11, 0), (), "HasAttachment", None),
		"History": (11, 2, (9, 0), (), "History", None),
		"ID": (4, 2, (12, 0), (), "ID", None),
		"IsLocked": (1, 2, (11, 0), (), "IsLocked", None),
		"Modified": (9, 2, (11, 0), (), "Modified", None),
		"Virtual": (10, 2, (11, 0), (), "Virtual", None),
	}
	_prop_map_put_ = {
		"AutoPost": ((5, LCID, 4, 0),()),
	}
	# Default method for this class is 'Field'
	def __call__(self, FieldName=defaultNamedNotOptArg):
		"""The value of the specified field."""
		return self._ApplyTypes_(0, 2, (12, 0), ((8, 0),), '__call__', None,FieldName
			)

	# str(ob) and int(ob) will use __call__
	def __unicode__(self, *args):
		try:
			return unicode(self.__call__(*args))
		except pythoncom.com_error:
			return repr(self)
	def __str__(self, *args):
		return str(self.__unicode__(*args))
	def __int__(self, *args):
		return int(self.__call__(*args))

class IBaseFieldExMail(DispatchBaseClass):
	"""Supports mailing factory items."""
	CLSID = IID('{BF873A99-2FB9-43DB-9559-F9F0872F7534}')
	coclass_clsid = None

	# The method Field is actually a property, but must be used as a method to correctly pass the arguments
	def Field(self, FieldName=defaultNamedNotOptArg):
		"""The value of the specified field."""
		return self._ApplyTypes_(0, 2, (12, 0), ((8, 0),), u'Field', None,FieldName
			)

	def LockObject(self):
		"""Locks the object. Returns True if lock successful."""
		return self._oleobj_.InvokeTypes(2, LCID, 1, (11, 0), (),)

	def Mail(self, SendTo=defaultNamedNotOptArg, SendCc=u'', Option=0, Subject=u''
			, Comment=u''):
		"""Mails the IBaseFieldExMail field item."""
		return self._ApplyTypes_(14, 1, (24, 32), ((8, 1), (8, 49), (3, 49), (8, 49), (8, 49)), u'Mail', None,SendTo
			, SendCc, Option, Subject, Comment)

	def Post(self):
		"""Posts all changed values into database."""
		return self._oleobj_.InvokeTypes(6, LCID, 1, (24, 0), (),)

	def Refresh(self):
		"""Reads saved values, overwriting values in memory."""
		return self._oleobj_.InvokeTypes(7, LCID, 1, (24, 0), (),)

	# The method SetField is actually a property, but must be used as a method to correctly pass the arguments
	def SetField(self, FieldName=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The value of the specified field."""
		return self._oleobj_.InvokeTypes(0, LCID, 4, (24, 0), ((8, 0), (12, 1)),FieldName
			, arg1)

	def UnLockObject(self):
		"""Unlocks the object."""
		return self._oleobj_.InvokeTypes(3, LCID, 1, (24, 0), (),)

	def Undo(self):
		"""Undoes changes to field values that have not been posted."""
		return self._oleobj_.InvokeTypes(8, LCID, 1, (24, 0), (),)

	_prop_map_get_ = {
		"Attachments": (12, 2, (9, 0), (), "Attachments", None),
		"AutoPost": (5, 2, (11, 0), (), "AutoPost", None),
		"HasAttachment": (13, 2, (11, 0), (), "HasAttachment", None),
		"History": (11, 2, (9, 0), (), "History", None),
		"ID": (4, 2, (12, 0), (), "ID", None),
		"IsLocked": (1, 2, (11, 0), (), "IsLocked", None),
		"Modified": (9, 2, (11, 0), (), "Modified", None),
		"Virtual": (10, 2, (11, 0), (), "Virtual", None),
	}
	_prop_map_put_ = {
		"AutoPost": ((5, LCID, 4, 0),()),
	}
	# Default method for this class is 'Field'
	def __call__(self, FieldName=defaultNamedNotOptArg):
		"""The value of the specified field."""
		return self._ApplyTypes_(0, 2, (12, 0), ((8, 0),), '__call__', None,FieldName
			)

	# str(ob) and int(ob) will use __call__
	def __unicode__(self, *args):
		try:
			return unicode(self.__call__(*args))
		except pythoncom.com_error:
			return repr(self)
	def __str__(self, *args):
		return str(self.__unicode__(*args))
	def __int__(self, *args):
		return int(self.__call__(*args))

class IBaseParam(DispatchBaseClass):
	"""A Business Process Component parameter."""
	CLSID = IID('{DC091A33-E1E8-4A17-8C55-529C09B0670B}')
	coclass_clsid = None

	_prop_map_get_ = {
		"Desc": (16, 2, (3, 0), ((16392, 10),), "Desc", None),
		"Name": (14, 2, (3, 0), ((16392, 10),), "Name", None),
		"Order": (18, 2, (3, 0), ((16387, 10),), "Order", None),
		"Value": (15, 2, (3, 0), ((16392, 10),), "Value", None),
		"ValueType": (17, 2, (3, 0), ((16392, 10),), "ValueType", None),
	}
	_prop_map_put_ = {
		"Desc": ((16, LCID, 4, 0),()),
		"Name": ((14, LCID, 4, 0),()),
		"Order": ((18, LCID, 4, 0),()),
		"Value": ((15, LCID, 4, 0),()),
		"ValueType": ((17, LCID, 4, 0),()),
	}
	# Default property for this class is 'Value'
	def __call__(self):
		return self._ApplyTypes_(*(15, 2, (3, 0), ((16392, 10),), "Value", None))
	# str(ob) and int(ob) will use __call__
	def __unicode__(self, *args):
		try:
			return unicode(self.__call__(*args))
		except pythoncom.com_error:
			return repr(self)
	def __str__(self, *args):
		return str(self.__unicode__(*args))
	def __int__(self, *args):
		return int(self.__call__(*args))

class IBug(DispatchBaseClass):
	"""Represents a defect."""
	CLSID = IID('{2AF970F7-6CCC-4DFB-AA78-08F689481F94}')
	coclass_clsid = IID('{AF9180F9-8C16-4824-9EA1-A9010B072B2C}')

	# The method Field is actually a property, but must be used as a method to correctly pass the arguments
	def Field(self, FieldName=defaultNamedNotOptArg):
		"""The value of the specified field."""
		return self._ApplyTypes_(0, 2, (12, 0), ((8, 0),), u'Field', None,FieldName
			)

	# Result is of type IList
	def FindSimilarBugs(self, SimilarityRatio=10):
		"""Searches the defect summaries for similarites to this defect."""
		ret = self._oleobj_.InvokeTypes(21, LCID, 1, (9, 0), ((3, 49),),SimilarityRatio
			)
		if ret is not None:
			ret = Dispatch(ret, u'FindSimilarBugs', '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')
		return ret

	def LockObject(self):
		"""Locks the object. Returns True if lock successful."""
		return self._oleobj_.InvokeTypes(2, LCID, 1, (11, 0), (),)

	def Mail(self, SendTo=defaultNamedNotOptArg, SendCc=u'', Option=0, Subject=u''
			, Comment=u''):
		"""Mails the IBaseFieldExMail field item."""
		return self._ApplyTypes_(14, 1, (24, 32), ((8, 1), (8, 49), (3, 49), (8, 49), (8, 49)), u'Mail', None,SendTo
			, SendCc, Option, Subject, Comment)

	def Post(self):
		"""Posts all changed values into database."""
		return self._oleobj_.InvokeTypes(6, LCID, 1, (24, 0), (),)

	def Refresh(self):
		"""Reads saved values, overwriting values in memory."""
		return self._oleobj_.InvokeTypes(7, LCID, 1, (24, 0), (),)

	# The method SetField is actually a property, but must be used as a method to correctly pass the arguments
	def SetField(self, FieldName=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The value of the specified field."""
		return self._oleobj_.InvokeTypes(0, LCID, 4, (24, 0), ((8, 0), (12, 1)),FieldName
			, arg1)

	def UnLockObject(self):
		"""Unlocks the object."""
		return self._oleobj_.InvokeTypes(3, LCID, 1, (24, 0), (),)

	def Undo(self):
		"""Undoes changes to field values that have not been posted."""
		return self._oleobj_.InvokeTypes(8, LCID, 1, (24, 0), (),)

	_prop_map_get_ = {
		"AssignedTo": (20, 2, (8, 0), (), "AssignedTo", None),
		"Attachments": (12, 2, (9, 0), (), "Attachments", None),
		"AutoPost": (5, 2, (11, 0), (), "AutoPost", None),
		# Method 'ChangeLinks' returns object of type 'IList'
		"ChangeLinks": (23, 2, (9, 0), (), "ChangeLinks", '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}'),
		"DetectedBy": (19, 2, (8, 0), (), "DetectedBy", None),
		"HasAttachment": (13, 2, (11, 0), (), "HasAttachment", None),
		"HasChange": (22, 2, (11, 0), (), "HasChange", None),
		"History": (11, 2, (9, 0), (), "History", None),
		"ID": (4, 2, (12, 0), (), "ID", None),
		"IsLocked": (1, 2, (11, 0), (), "IsLocked", None),
		"Modified": (9, 2, (11, 0), (), "Modified", None),
		"Priority": (18, 2, (8, 0), (), "Priority", None),
		"Project": (16, 2, (8, 0), (), "Project", None),
		"Status": (15, 2, (8, 0), (), "Status", None),
		"Summary": (17, 2, (8, 0), (), "Summary", None),
		"Virtual": (10, 2, (11, 0), (), "Virtual", None),
	}
	_prop_map_put_ = {
		"AssignedTo": ((20, LCID, 4, 0),()),
		"AutoPost": ((5, LCID, 4, 0),()),
		"DetectedBy": ((19, LCID, 4, 0),()),
		"Priority": ((18, LCID, 4, 0),()),
		"Project": ((16, LCID, 4, 0),()),
		"Status": ((15, LCID, 4, 0),()),
		"Summary": ((17, LCID, 4, 0),()),
	}
	# Default method for this class is 'Field'
	def __call__(self, FieldName=defaultNamedNotOptArg):
		"""The value of the specified field."""
		return self._ApplyTypes_(0, 2, (12, 0), ((8, 0),), '__call__', None,FieldName
			)

	# str(ob) and int(ob) will use __call__
	def __unicode__(self, *args):
		try:
			return unicode(self.__call__(*args))
		except pythoncom.com_error:
			return repr(self)
	def __str__(self, *args):
		return str(self.__unicode__(*args))
	def __int__(self, *args):
		return int(self.__call__(*args))

class IBugFactory(DispatchBaseClass):
	"""Services to manage defect records."""
	CLSID = IID('{E46670F8-B7CE-4DA6-AC5F-AE1FB9181337}')
	coclass_clsid = IID('{5DB9FED8-D26F-4F27-911F-99216ED75986}')

	def AddItem(self, ItemData=defaultNamedNotOptArg):
		"""Creates a new item object."""
		ret = self._oleobj_.InvokeTypes(3, LCID, 1, (9, 0), ((12, 0),),ItemData
			)
		if ret is not None:
			ret = Dispatch(ret, u'AddItem', None)
		return ret

	def BuildAgeGraph(self, GroupByField=u'', SumOfField=u'', MaxAge=0, MaxCols=0
			, Filter=defaultNamedNotOptArg, ForceRefresh=False, ShowFullPath=False):
		"""Creates a graph that shows the lifetime of defects."""
		return self._ApplyTypes_(10, 1, (9, 32), ((8, 49), (8, 49), (3, 49), (3, 49), (12, 17), (11, 49), (11, 49)), u'BuildAgeGraph', None,GroupByField
			, SumOfField, MaxAge, MaxCols, Filter, ForceRefresh
			, ShowFullPath)

	def BuildPerfGraph(self, GroupByField=u'', SumOfField=u'', MaxCols=0, Filter=defaultNamedNotOptArg
			, FRDate=defaultNamedNotOptArg, ForceRefresh=False, ShowFullPath=False):
		"""Creates Performance Graph."""
		return self._ApplyTypes_(14, 1, (9, 32), ((8, 49), (8, 49), (3, 49), (12, 17), (12, 17), (11, 49), (11, 49)), u'BuildPerfGraph', None,GroupByField
			, SumOfField, MaxCols, Filter, FRDate, ForceRefresh
			, ShowFullPath)

	def BuildProgressGraph(self, GroupByField=u'', SumOfField=u'', ByHistory=True, MajorSkip=0
			, MinorSkip=1, MaxCols=0, Filter=defaultNamedNotOptArg, FRDate=defaultNamedNotOptArg, ForceRefresh=False
			, ShowFullPath=False):
		"""Creates a graph showing status at specific points: either defect accumulation or  estimated/actual time to fix."""
		return self._ApplyTypes_(11, 1, (9, 32), ((8, 49), (8, 49), (11, 49), (3, 49), (3, 49), (3, 49), (12, 17), (12, 17), (11, 49), (11, 49)), u'BuildProgressGraph', None,GroupByField
			, SumOfField, ByHistory, MajorSkip, MinorSkip, MaxCols
			, Filter, FRDate, ForceRefresh, ShowFullPath)

	def BuildSummaryGraph(self, XAxisField=u'', GroupByField=u'', SumOfField=u'', MaxCols=0
			, Filter=defaultNamedNotOptArg, ForceRefresh=False, ShowFullPath=False):
		"""Creates a summary graph either of the number of defects or the estimated/actual amount of time to fix."""
		return self._ApplyTypes_(9, 1, (9, 32), ((8, 49), (8, 49), (8, 49), (3, 49), (12, 17), (11, 49), (11, 49)), u'BuildSummaryGraph', None,XAxisField
			, GroupByField, SumOfField, MaxCols, Filter, ForceRefresh
			, ShowFullPath)

	def BuildTrendGraph(self, GroupByField=u'', SumOfField=u'', MaxCols=0, Filter=defaultNamedNotOptArg
			, FRDate=defaultNamedNotOptArg, ForceRefresh=False, ShowFullPath=False):
		"""Creates a graph that shows the history of changes to specific Defect fields for each time interval displayed."""
		return self._ApplyTypes_(13, 1, (9, 32), ((8, 49), (8, 49), (3, 49), (12, 17), (12, 17), (11, 49), (11, 49)), u'BuildTrendGraph', None,GroupByField
			, SumOfField, MaxCols, Filter, FRDate, ForceRefresh
			, ShowFullPath)

	# The method FetchLevel is actually a property, but must be used as a method to correctly pass the arguments
	def FetchLevel(self, FieldName=defaultNamedNotOptArg):
		"""The Fetch level for a field."""
		return self._oleobj_.InvokeTypes(7, LCID, 2, (2, 0), ((8, 1),),FieldName
			)

	# Result is of type IList
	def FindSimilarBugs(self, Pattern=defaultNamedNotOptArg, SimilarityRatio=10):
		"""Searches defect summaries and descriptions for matches to Pattern."""
		ret = self._oleobj_.InvokeTypes(12, LCID, 1, (9, 0), ((8, 0), (3, 49)),Pattern
			, SimilarityRatio)
		if ret is not None:
			ret = Dispatch(ret, u'FindSimilarBugs', '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')
		return ret

	# The method Item is actually a property, but must be used as a method to correctly pass the arguments
	def Item(self, ItemKey=defaultNamedNotOptArg):
		"""Gets an object managed by the factory by its key."""
		ret = self._oleobj_.InvokeTypes(0, LCID, 2, (9, 0), ((12, 0),),ItemKey
			)
		if ret is not None:
			ret = Dispatch(ret, u'Item', None)
		return ret

	def Mail(self, Items=defaultNamedNotOptArg, SendTo=defaultNamedNotOptArg, SendCc=u'', Option=0
			, Subject=u'', Comment=u''):
		"""Mails the list of IBase Factory Items. 'Items' is a list of ID numbers."""
		return self._ApplyTypes_(8, 1, (24, 32), ((12, 1), (8, 1), (8, 49), (3, 49), (8, 49), (8, 49)), u'Mail', None,Items
			, SendTo, SendCc, Option, Subject, Comment
			)

	# Result is of type IList
	def NewList(self, Filter=defaultNamedNotOptArg):
		"""Creates a list of objects according to the specified filter."""
		ret = self._oleobj_.InvokeTypes(1, LCID, 1, (9, 0), ((8, 0),),Filter
			)
		if ret is not None:
			ret = Dispatch(ret, u'NewList', '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')
		return ret

	def RemoveItem(self, ItemKey=defaultNamedNotOptArg):
		"""Removes item from the database. Removal takes place immediately, without a Post."""
		return self._oleobj_.InvokeTypes(4, LCID, 1, (24, 0), ((12, 0),),ItemKey
			)

	# The method SetFetchLevel is actually a property, but must be used as a method to correctly pass the arguments
	def SetFetchLevel(self, FieldName=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The Fetch level for a field."""
		return self._oleobj_.InvokeTypes(7, LCID, 4, (24, 0), ((8, 1), (2, 1)),FieldName
			, arg1)

	_prop_map_get_ = {
		# Method 'Fields' returns object of type 'IList'
		"Fields": (2, 2, (9, 0), (), "Fields", '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}'),
		"Filter": (5, 2, (9, 0), (), "Filter", None),
		"History": (6, 2, (9, 0), (), "History", None),
	}
	_prop_map_put_ = {
	}
	# Default method for this class is 'Item'
	def __call__(self, ItemKey=defaultNamedNotOptArg):
		"""Gets an object managed by the factory by its key."""
		ret = self._oleobj_.InvokeTypes(0, LCID, 2, (9, 0), ((12, 0),),ItemKey
			)
		if ret is not None:
			ret = Dispatch(ret, '__call__', None)
		return ret

	# str(ob) and int(ob) will use __call__
	def __unicode__(self, *args):
		try:
			return unicode(self.__call__(*args))
		except pythoncom.com_error:
			return repr(self)
	def __str__(self, *args):
		return str(self.__unicode__(*args))
	def __int__(self, *args):
		return int(self.__call__(*args))

class IBusinessProcess(DispatchBaseClass):
	"""IBusinessProcess Interface"""
	CLSID = IID('{590E515E-D527-4436-B04C-40942DBFFB5F}')
	coclass_clsid = IID('{5BE353A8-508B-48B5-848B-53DF49791E52}')

	def AddBPComponent(self, pComponent=defaultNamedNotOptArg):
		"""method AddBPComponent"""
		ret = self._oleobj_.InvokeTypes(106, LCID, 1, (9, 0), ((9, 1),),pComponent
			)
		if ret is not None:
			ret = Dispatch(ret, u'AddBPComponent', None)
		return ret

	def AddGroup(self):
		"""method AddGroup"""
		ret = self._oleobj_.InvokeTypes(108, LCID, 1, (9, 0), (),)
		if ret is not None:
			ret = Dispatch(ret, u'AddGroup', None)
		return ret

	def AddRTParam(self):
		"""method AddRTParam"""
		ret = self._oleobj_.InvokeTypes(103, LCID, 1, (9, 0), (),)
		if ret is not None:
			ret = Dispatch(ret, u'AddRTParam', None)
		return ret

	# The method BPComponentByID is actually a property, but must be used as a method to correctly pass the arguments
	def BPComponentByID(self, nCompID=defaultNamedNotOptArg):
		"""property BPComponentByID"""
		ret = self._oleobj_.InvokeTypes(116, LCID, 2, (9, 0), ((3, 1),),nCompID
			)
		if ret is not None:
			ret = Dispatch(ret, u'BPComponentByID', None)
		return ret

	def CancelDownloadPictures(self):
		"""method CancelDownloadPictures"""
		return self._oleobj_.InvokeTypes(113, LCID, 1, (24, 0), (),)

	def CoverRequirement(self, Req=defaultNamedNotOptArg, Order=defaultNamedNotOptArg, Recursive=defaultNamedNotOptArg):
		"""Checks if this test covers the specified requirement."""
		return self._oleobj_.InvokeTypes(20, LCID, 1, (3, 0), ((12, 1), (3, 1), (11, 1)),Req
			, Order, Recursive)

	def DeleteBPComponent(self, pBPComponent=defaultNamedNotOptArg):
		"""method DeleteBPComponent"""
		return self._oleobj_.InvokeTypes(107, LCID, 1, (24, 0), ((9, 1),),pBPComponent
			)

	def DeleteGroup(self, pBPGroup=defaultNamedNotOptArg):
		"""method DeleteGroup"""
		return self._oleobj_.InvokeTypes(109, LCID, 1, (24, 0), ((9, 1),),pBPGroup
			)

	def DeleteRTParam(self, pRTParam=defaultNamedNotOptArg):
		"""method DeleteRTParam"""
		return self._oleobj_.InvokeTypes(104, LCID, 1, (24, 0), ((9, 1),),pRTParam
			)

	def DownloadPictures(self):
		"""method DownloadPictures"""
		return self._oleobj_.InvokeTypes(111, LCID, 1, (24, 0), (),)

	def DownloadPicturesProgress(self, Total=pythoncom.Missing, Current=pythoncom.Missing):
		"""method DownloadPicturesProgress"""
		return self._ApplyTypes_(112, 1, (24, 0), ((16387, 2), (16387, 2)), u'DownloadPicturesProgress', None,Total
			, Current)

	# The method Field is actually a property, but must be used as a method to correctly pass the arguments
	def Field(self, FieldName=defaultNamedNotOptArg):
		"""The value of the specified field."""
		return self._ApplyTypes_(0, 2, (12, 0), ((8, 0),), u'Field', None,FieldName
			)

	# The method FullPathEx is actually a property, but must be used as a method to correctly pass the arguments
	def FullPathEx(self, isOrgFullPath=defaultNamedNotOptArg):
		"""For future use."""
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(34, LCID, 2, (8, 0), ((3, 1),),isOrgFullPath
			)

	# Result is of type IList
	def GetCoverList(self):
		"""Gets the list of all requirements covered by this test."""
		ret = self._oleobj_.InvokeTypes(22, LCID, 1, (9, 0), (),)
		if ret is not None:
			ret = Dispatch(ret, u'GetCoverList', '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')
		return ret

	# The method GroupByID is actually a property, but must be used as a method to correctly pass the arguments
	def GroupByID(self, nGroupID=defaultNamedNotOptArg):
		"""property Group"""
		ret = self._oleobj_.InvokeTypes(110, LCID, 2, (9, 0), ((3, 1),),nGroupID
			)
		if ret is not None:
			ret = Dispatch(ret, u'GroupByID', None)
		return ret

	def Load(self):
		"""method Load"""
		return self._oleobj_.InvokeTypes(100, LCID, 1, (24, 0), (),)

	def LockObject(self):
		"""Locks the object. Returns True if lock successful."""
		return self._oleobj_.InvokeTypes(2, LCID, 1, (11, 0), (),)

	def Mail(self, SendTo=defaultNamedNotOptArg, SendCc=u'', Option=0, Subject=u''
			, Comment=u''):
		"""Mails the IBaseFieldExMail field item."""
		return self._ApplyTypes_(14, 1, (24, 32), ((8, 1), (8, 49), (3, 49), (8, 49), (8, 49)), u'Mail', None,SendTo
			, SendCc, Option, Subject, Comment)

	def Post(self):
		"""Posts all changed values into database."""
		return self._oleobj_.InvokeTypes(6, LCID, 1, (24, 0), (),)

	def Refresh(self):
		"""Reads saved values, overwriting values in memory."""
		return self._oleobj_.InvokeTypes(7, LCID, 1, (24, 0), (),)

	def RemoveCoverage(self, Req=defaultNamedNotOptArg):
		"""Removes a requirement from the list of requirements this test covers."""
		return self._oleobj_.InvokeTypes(21, LCID, 1, (24, 0), ((12, 1),),Req
			)

	def Save(self):
		"""method Save"""
		return self._oleobj_.InvokeTypes(101, LCID, 1, (24, 0), (),)

	# The method SetField is actually a property, but must be used as a method to correctly pass the arguments
	def SetField(self, FieldName=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The value of the specified field."""
		return self._oleobj_.InvokeTypes(0, LCID, 4, (24, 0), ((8, 0), (12, 1)),FieldName
			, arg1)

	def UnLockObject(self):
		"""Unlocks the object."""
		return self._oleobj_.InvokeTypes(3, LCID, 1, (24, 0), (),)

	def Undo(self):
		"""Undoes changes to field values that have not been posted."""
		return self._oleobj_.InvokeTypes(8, LCID, 1, (24, 0), (),)

	_prop_map_get_ = {
		"Attachments": (12, 2, (9, 0), (), "Attachments", None),
		"AutoPost": (5, 2, (11, 0), (), "AutoPost", None),
		# Method 'BPComponents' returns object of type 'IList'
		"BPComponents": (105, 2, (9, 0), (), "BPComponents", '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}'),
		"BPTHistory": (115, 2, (9, 0), (), "BPTHistory", None),
		"CheckoutPathIfExist": (32, 2, (8, 0), (), "CheckoutPathIfExist", None),
		"DesStepsNum": (19, 2, (3, 0), (), "DesStepsNum", None),
		"DesignStepFactory": (18, 2, (9, 0), (), "DesignStepFactory", None),
		"ExecDate": (27, 2, (7, 0), (), "ExecDate", None),
		"ExecStatus": (24, 2, (8, 0), (), "ExecStatus", None),
		"ExtendedStorage": (23, 2, (9, 0), (), "ExtendedStorage", None),
		"FullPath": (15, 2, (8, 0), (), "FullPath", None),
		"HasAttachment": (13, 2, (11, 0), (), "HasAttachment", None),
		"HasCoverage": (25, 2, (11, 0), (), "HasCoverage", None),
		"HasParam": (30, 2, (11, 0), (), "HasParam", None),
		"History": (11, 2, (9, 0), (), "History", None),
		"HtmlScript": (114, 2, (24, 0), ((16392, 2),), "HtmlScript", None),
		"ID": (4, 2, (12, 0), (), "ID", None),
		"IgnoreDataHiding": (33, 2, (11, 0), (), "IgnoreDataHiding", None),
		"IsLocked": (1, 2, (11, 0), (), "IsLocked", None),
		"LastRun": (26, 2, (9, 0), (), "LastRun", None),
		"Modified": (9, 2, (11, 0), (), "Modified", None),
		"Name": (17, 2, (8, 0), (), "Name", None),
		"Params": (29, 2, (9, 0), (), "Params", None),
		# Method 'RTParameters' returns object of type 'IList'
		"RTParameters": (102, 2, (9, 0), (), "RTParameters", '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}'),
		"TemplateTest": (28, 2, (11, 0), (), "TemplateTest", None),
		"Type": (16, 2, (8, 0), (), "Type", None),
		"VCS": (31, 2, (9, 0), (), "VCS", None),
		"Virtual": (10, 2, (11, 0), (), "Virtual", None),
	}
	_prop_map_put_ = {
		"AutoPost": ((5, LCID, 4, 0),()),
		"IgnoreDataHiding": ((33, LCID, 4, 0),()),
		"Name": ((17, LCID, 4, 0),()),
		"TemplateTest": ((28, LCID, 4, 0),()),
		"Type": ((16, LCID, 4, 0),()),
	}
	# Default method for this class is 'Field'
	def __call__(self, FieldName=defaultNamedNotOptArg):
		"""The value of the specified field."""
		return self._ApplyTypes_(0, 2, (12, 0), ((8, 0),), '__call__', None,FieldName
			)

	# str(ob) and int(ob) will use __call__
	def __unicode__(self, *args):
		try:
			return unicode(self.__call__(*args))
		except pythoncom.com_error:
			return repr(self)
	def __str__(self, *args):
		return str(self.__unicode__(*args))
	def __int__(self, *args):
		return int(self.__call__(*args))

class ICacheMgr(DispatchBaseClass):
	"""Manages the system cache in conjunction with the ExtendedStorage objects."""
	CLSID = IID('{30300941-F52E-4FFD-8314-3EA232206EB0}')
	coclass_clsid = IID('{21DB8AA3-957F-4ACE-9CA7-AECFF8085BB4}')

	def Run(self):
		"""Starts the CacheMgr thread and returns immediately."""
		return self._oleobj_.InvokeTypes(1, LCID, 1, (24, 0), (),)

	def SetCurrentTest(self, bsTestPath=defaultNamedNotOptArg):
		"""Method SetCurrentTest."""
		return self._oleobj_.InvokeTypes(5, LCID, 1, (24, 0), ((8, 1),),bsTestPath
			)

	def SetFileTime(self, bsFilePath=defaultNamedNotOptArg):
		"""Use the date of download from the server instead of the access date when determining older files for deletion."""
		return self._oleobj_.InvokeTypes(3, LCID, 1, (24, 0), ((8, 1),),bsFilePath
			)

	def SetUpdateRegistry(self, bAllowUpdate=defaultNamedNotOptArg):
		"""Method SetUpdateRegistry."""
		return self._oleobj_.InvokeTypes(4, LCID, 1, (24, 0), ((3, 1),),bAllowUpdate
			)

	_prop_map_get_ = {
		"IsRunning": (2, 2, (3, 0), (), "IsRunning", None),
	}
	_prop_map_put_ = {
	}

class IChange(DispatchBaseClass):
	"""IChange Interface"""
	CLSID = IID('{74B94507-8A42-4D11-B5BD-943B76C9A982}')
	coclass_clsid = IID('{DBCF02A8-E281-4C0B-92B8-4F8AF677DD6C}')

	def AddBugLink(self, newVal=defaultNamedNotOptArg):
		"""Inserts bug id."""
		return self._oleobj_.InvokeTypes(21, LCID, 1, (24, 0), ((3, 1),),newVal
			)

	# The method Field is actually a property, but must be used as a method to correctly pass the arguments
	def Field(self, FieldName=defaultNamedNotOptArg):
		"""The value of the specified field."""
		return self._ApplyTypes_(0, 2, (12, 0), ((8, 0),), u'Field', None,FieldName
			)

	def GetBugLinks(self, pVal=pythoncom.Missing):
		return self._ApplyTypes_(23, 1, (24, 0), ((16393, 2),), u'GetBugLinks', None,pVal
			)

	def LockObject(self):
		"""Locks the object. Returns True if lock successful."""
		return self._oleobj_.InvokeTypes(2, LCID, 1, (11, 0), (),)

	def Post(self):
		"""Posts all changed values into database."""
		return self._oleobj_.InvokeTypes(6, LCID, 1, (24, 0), (),)

	def Refresh(self):
		"""Reads saved values, overwriting values in memory."""
		return self._oleobj_.InvokeTypes(7, LCID, 1, (24, 0), (),)

	def RemoveBugLink(self, newVal=defaultNamedNotOptArg):
		return self._oleobj_.InvokeTypes(22, LCID, 1, (24, 0), ((3, 1),),newVal
			)

	# The method SetField is actually a property, but must be used as a method to correctly pass the arguments
	def SetField(self, FieldName=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The value of the specified field."""
		return self._oleobj_.InvokeTypes(0, LCID, 4, (24, 0), ((8, 0), (12, 1)),FieldName
			, arg1)

	def UnLockObject(self):
		"""Unlocks the object."""
		return self._oleobj_.InvokeTypes(3, LCID, 1, (24, 0), (),)

	def Undo(self):
		"""Undoes changes to field values that have not been posted."""
		return self._oleobj_.InvokeTypes(8, LCID, 1, (24, 0), (),)

	_prop_map_get_ = {
		"Attachments": (12, 2, (9, 0), (), "Attachments", None),
		"AutoGetLinks": (24, 2, (11, 0), (), "AutoGetLinks", None),
		"AutoPost": (5, 2, (11, 0), (), "AutoPost", None),
		"ChangeEntryFactory": (20, 2, (9, 0), (), "ChangeEntryFactory", None),
		"ClosingDate": (18, 2, (7, 0), (), "ClosingDate", None),
		"Comments": (15, 2, (8, 0), (), "Comments", None),
		"CreatedBy": (16, 2, (8, 0), (), "CreatedBy", None),
		"CreationDate": (17, 2, (7, 0), (), "CreationDate", None),
		"Description": (14, 2, (8, 0), (), "Description", None),
		"HasAttachment": (13, 2, (11, 0), (), "HasAttachment", None),
		"History": (11, 2, (9, 0), (), "History", None),
		"ID": (4, 2, (12, 0), (), "ID", None),
		"IsLocked": (1, 2, (11, 0), (), "IsLocked", None),
		"Links": (25, 2, (8, 0), (), "Links", None),
		"Modified": (9, 2, (11, 0), (), "Modified", None),
		"VOB_Labels": (19, 2, (8, 0), (), "VOB_Labels", None),
		"Virtual": (10, 2, (11, 0), (), "Virtual", None),
	}
	_prop_map_put_ = {
		"AutoGetLinks": ((24, LCID, 4, 0),()),
		"AutoPost": ((5, LCID, 4, 0),()),
		"ClosingDate": ((18, LCID, 4, 0),()),
		"Comments": ((15, LCID, 4, 0),()),
		"Description": ((14, LCID, 4, 0),()),
		"VOB_Labels": ((19, LCID, 4, 0),()),
	}
	# Default method for this class is 'Field'
	def __call__(self, FieldName=defaultNamedNotOptArg):
		"""The value of the specified field."""
		return self._ApplyTypes_(0, 2, (12, 0), ((8, 0),), '__call__', None,FieldName
			)

	# str(ob) and int(ob) will use __call__
	def __unicode__(self, *args):
		try:
			return unicode(self.__call__(*args))
		except pythoncom.com_error:
			return repr(self)
	def __str__(self, *args):
		return str(self.__unicode__(*args))
	def __int__(self, *args):
		return int(self.__call__(*args))

class IChangeEntry(DispatchBaseClass):
	"""IChangeEntry Interface"""
	CLSID = IID('{0977F04D-0A7C-403D-A07B-1C2780362C46}')
	coclass_clsid = IID('{34881384-E4DD-40D2-AF7B-F7A14D226F2D}')

	# The method Field is actually a property, but must be used as a method to correctly pass the arguments
	def Field(self, FieldName=defaultNamedNotOptArg):
		"""The value of the specified field."""
		return self._ApplyTypes_(0, 2, (12, 0), ((8, 0),), u'Field', None,FieldName
			)

	def LockObject(self):
		"""Locks the object. Returns True if lock successful."""
		return self._oleobj_.InvokeTypes(2, LCID, 1, (11, 0), (),)

	def Post(self):
		"""Posts all changed values into database."""
		return self._oleobj_.InvokeTypes(6, LCID, 1, (24, 0), (),)

	def Refresh(self):
		"""Reads saved values, overwriting values in memory."""
		return self._oleobj_.InvokeTypes(7, LCID, 1, (24, 0), (),)

	# The method SetField is actually a property, but must be used as a method to correctly pass the arguments
	def SetField(self, FieldName=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The value of the specified field."""
		return self._oleobj_.InvokeTypes(0, LCID, 4, (24, 0), ((8, 0), (12, 1)),FieldName
			, arg1)

	def UnLockObject(self):
		"""Unlocks the object."""
		return self._oleobj_.InvokeTypes(3, LCID, 1, (24, 0), (),)

	def Undo(self):
		"""Undoes changes to field values that have not been posted."""
		return self._oleobj_.InvokeTypes(8, LCID, 1, (24, 0), (),)

	_prop_map_get_ = {
		"Attachments": (12, 2, (9, 0), (), "Attachments", None),
		"AutoPost": (5, 2, (11, 0), (), "AutoPost", None),
		"CreatedBy": (19, 2, (8, 0), (), "CreatedBy", None),
		"CreationDate": (20, 2, (7, 0), (), "CreationDate", None),
		"EndVersion": (18, 2, (8, 0), (), "EndVersion", None),
		"FileName": (14, 2, (8, 0), (), "FileName", None),
		"FilePath": (15, 2, (8, 0), (), "FilePath", None),
		"HasAttachment": (13, 2, (11, 0), (), "HasAttachment", None),
		"History": (11, 2, (9, 0), (), "History", None),
		"ID": (4, 2, (12, 0), (), "ID", None),
		"IsLocked": (1, 2, (11, 0), (), "IsLocked", None),
		"Modified": (9, 2, (11, 0), (), "Modified", None),
		"StartVersion": (16, 2, (8, 0), (), "StartVersion", None),
		"Virtual": (10, 2, (11, 0), (), "Virtual", None),
		"WorkingVersion": (17, 2, (8, 0), (), "WorkingVersion", None),
	}
	_prop_map_put_ = {
		"AutoPost": ((5, LCID, 4, 0),()),
		"EndVersion": ((18, LCID, 4, 0),()),
		"WorkingVersion": ((17, LCID, 4, 0),()),
	}
	# Default method for this class is 'Field'
	def __call__(self, FieldName=defaultNamedNotOptArg):
		"""The value of the specified field."""
		return self._ApplyTypes_(0, 2, (12, 0), ((8, 0),), '__call__', None,FieldName
			)

	# str(ob) and int(ob) will use __call__
	def __unicode__(self, *args):
		try:
			return unicode(self.__call__(*args))
		except pythoncom.com_error:
			return repr(self)
	def __str__(self, *args):
		return str(self.__unicode__(*args))
	def __int__(self, *args):
		return int(self.__call__(*args))

class IColumnInfo(DispatchBaseClass):
	"""Represents column information for a Recordset."""
	CLSID = IID('{0993EF6D-FAF3-42FF-BCF9-85BBB83753F6}')
	coclass_clsid = None

	# The method ColIndex is actually a property, but must be used as a method to correctly pass the arguments
	def ColIndex(self, Name=defaultNamedNotOptArg):
		"""Gets the index (zero-based) of a Recordset column specified by column name."""
		return self._oleobj_.InvokeTypes(5, LCID, 2, (3, 0), ((8, 0),),Name
			)

	# The method ColName is actually a property, but must be used as a method to correctly pass the arguments
	def ColName(self, Index=defaultNamedNotOptArg):
		"""The name of the Recordset column specified by Index (zero-based)."""
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(4, LCID, 2, (8, 0), ((3, 0),),Index
			)

	# The method ColSize is actually a property, but must be used as a method to correctly pass the arguments
	def ColSize(self, Index=defaultNamedNotOptArg):
		"""The physical size as defined in the database of the field specified by Index (zero-based)."""
		return self._oleobj_.InvokeTypes(2, LCID, 2, (3, 0), ((3, 0),),Index
			)

	# The method ColType is actually a property, but must be used as a method to correctly pass the arguments
	def ColType(self, Index=defaultNamedNotOptArg):
		"""The data type of column."""
		return self._oleobj_.InvokeTypes(3, LCID, 2, (3, 0), ((3, 0),),Index
			)

	_prop_map_get_ = {
		"ColCount": (1, 2, (3, 0), (), "ColCount", None),
	}
	_prop_map_put_ = {
	}

class IComFrec(DispatchBaseClass):
	"""IComFrec Interface."""
	CLSID = IID('{D2FA8791-3A81-4D03-95CA-74EF6E059C4F}')
	coclass_clsid = IID('{B2F590F7-BD30-45DD-90B7-F243D7E8B210}')

	def AddItem(self, AttrName=defaultNamedNotOptArg, Value=defaultNamedNotOptArg):
		"""Method AddItem."""
		return self._oleobj_.InvokeTypes(7, LCID, 1, (24, 0), ((8, 0), (8, 0)),AttrName
			, Value)

	# The method AttributeName is actually a property, but must be used as a method to correctly pass the arguments
	def AttributeName(self, Position=defaultNamedNotOptArg):
		"""Property AttributeName."""
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(11, LCID, 2, (8, 0), ((3, 0),),Position
			)

	# The method AttributePosition is actually a property, but must be used as a method to correctly pass the arguments
	def AttributePosition(self, AttributeName=defaultNamedNotOptArg):
		"""Retruns te position of a specified attribute."""
		return self._oleobj_.InvokeTypes(12, LCID, 2, (3, 0), ((8, 0),),AttributeName
			)

	def ClearItemList(self):
		"""Removes all items from the list."""
		return self._oleobj_.InvokeTypes(15, LCID, 1, (24, 0), (),)

	# The method IsAttribute is actually a property, but must be used as a method to correctly pass the arguments
	def IsAttribute(self, AttributeName=defaultNamedNotOptArg):
		"""Property IsAttribute."""
		return self._oleobj_.InvokeTypes(9, LCID, 2, (11, 0), ((8, 0),),AttributeName
			)

	def Open(self, NewData=defaultNamedNotOptArg):
		"""Method Open."""
		return self._oleobj_.InvokeTypes(1, LCID, 1, (24, 0), ((8, 0),),NewData
			)

	def RemoveItem(self, AttributeName=defaultNamedNotOptArg):
		"""Method RemoveItem - be aware Remove Item changes the positions of the items -  the index start with 0."""
		return self._oleobj_.InvokeTypes(13, LCID, 1, (24, 0), ((8, 0),),AttributeName
			)

	def RemoveItemPos(self, ItemPos=defaultNamedNotOptArg):
		"""Method RemoveItemPos."""
		return self._oleobj_.InvokeTypes(14, LCID, 1, (24, 0), ((3, 0),),ItemPos
			)

	# The method SetAttributeName is actually a property, but must be used as a method to correctly pass the arguments
	def SetAttributeName(self, Position=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""Property AttributeName."""
		return self._oleobj_.InvokeTypes(11, LCID, 4, (24, 0), ((3, 0), (8, 1)),Position
			, arg1)

	# The method SetValue is actually a property, but must be used as a method to correctly pass the arguments
	def SetValue(self, AtributeName=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""Property Value."""
		return self._oleobj_.InvokeTypes(6, LCID, 4, (24, 0), ((8, 0), (8, 1)),AtributeName
			, arg1)

	# The method SetValuePos is actually a property, but must be used as a method to correctly pass the arguments
	def SetValuePos(self, Pos=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""Property ValuePos , get set the value at specifid pos in the list."""
		return self._oleobj_.InvokeTypes(8, LCID, 4, (24, 0), ((3, 0), (8, 1)),Pos
			, arg1)

	# The method Value is actually a property, but must be used as a method to correctly pass the arguments
	def Value(self, AtributeName=defaultNamedNotOptArg):
		"""Property Value."""
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(6, LCID, 2, (8, 0), ((8, 0),),AtributeName
			)

	# The method ValuePos is actually a property, but must be used as a method to correctly pass the arguments
	def ValuePos(self, Pos=defaultNamedNotOptArg):
		"""Property ValuePos , get set the value at specifid pos in the list."""
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(8, LCID, 2, (8, 0), ((3, 0),),Pos
			)

	def WriteRecord(self):
		"""Writes the Record. The Attribute list is ready  for new input."""
		return self._oleobj_.InvokeTypes(2, LCID, 1, (24, 0), (),)

	_prop_map_get_ = {
		"Buffer": (5, 2, (8, 0), (), "Buffer", None),
		"NumberOfAttributes": (10, 2, (3, 0), (), "NumberOfAttributes", None),
		"ReadRecord": (3, 2, (11, 0), (), "ReadRecord", None),
		"RecordString": (4, 2, (8, 0), (), "RecordString", None),
	}
	_prop_map_put_ = {
	}
	# Default method for this class is 'Value'
	def __call__(self, AtributeName=defaultNamedNotOptArg):
		"""Property Value."""
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(6, LCID, 2, (8, 0), ((8, 0),),AtributeName
			)

	# str(ob) and int(ob) will use __call__
	def __unicode__(self, *args):
		try:
			return unicode(self.__call__(*args))
		except pythoncom.com_error:
			return repr(self)
	def __str__(self, *args):
		return str(self.__unicode__(*args))
	def __int__(self, *args):
		return int(self.__call__(*args))

class ICommand(DispatchBaseClass):
	"""Represents a database command."""
	CLSID = IID('{35DE061D-B934-4DEB-9F53-6A376EB034DF}')
	coclass_clsid = IID('{56FD2617-AEC0-46C8-805A-E69481480B68}')

	def AddParam(self, Name=defaultNamedNotOptArg, InitialValue=defaultNamedNotOptArg):
		"""Adds new parameter to Command object."""
		return self._oleobj_.InvokeTypes(5, LCID, 1, (24, 0), ((8, 0), (12, 0)),Name
			, InitialValue)

	def DeleteParam(self, Key=defaultNamedNotOptArg):
		"""Deletes the specified parameter from Command object."""
		return self._oleobj_.InvokeTypes(6, LCID, 1, (24, 0), ((12, 0),),Key
			)

	def DeleteParams(self):
		"""Deletes all parameters from Command object."""
		return self._oleobj_.InvokeTypes(7, LCID, 1, (24, 0), (),)

	def Execute(self):
		"""Executes the command in the CommandText property."""
		ret = self._oleobj_.InvokeTypes(9, LCID, 1, (9, 0), (),)
		if ret is not None:
			ret = Dispatch(ret, u'Execute', None)
		return ret

	# The method ParamIndex is actually a property, but must be used as a method to correctly pass the arguments
	def ParamIndex(self, Name=defaultNamedNotOptArg):
		"""The parameter index by parameter name."""
		return self._oleobj_.InvokeTypes(4, LCID, 2, (3, 0), ((8, 0),),Name
			)

	# The method ParamName is actually a property, but must be used as a method to correctly pass the arguments
	def ParamName(self, Index=defaultNamedNotOptArg):
		"""The parameter name by parameter index. The index is 0-based."""
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(3, LCID, 2, (8, 0), ((3, 0),),Index
			)

	# The method ParamType is actually a property, but must be used as a method to correctly pass the arguments
	def ParamType(self, Index=defaultNamedNotOptArg):
		"""The data type of the specifed parameter.  The index is 0-based."""
		return self._oleobj_.InvokeTypes(8, LCID, 2, (3, 0), ((3, 0),),Index
			)

	# The method ParamValue is actually a property, but must be used as a method to correctly pass the arguments
	def ParamValue(self, Key=defaultNamedNotOptArg):
		"""The parameter value."""
		return self._ApplyTypes_(2, 2, (12, 0), ((12, 0),), u'ParamValue', None,Key
			)

	# The method SetParamValue is actually a property, but must be used as a method to correctly pass the arguments
	def SetParamValue(self, Key=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The parameter value."""
		return self._oleobj_.InvokeTypes(2, LCID, 4, (24, 0), ((12, 0), (12, 1)),Key
			, arg1)

	_prop_map_get_ = {
		"AffectedRows": (11, 2, (3, 0), (), "AffectedRows", None),
		"CommandText": (0, 2, (8, 0), (), "CommandText", None),
		"Count": (1, 2, (3, 0), (), "Count", None),
		"IndexFields": (10, 2, (8, 0), (), "IndexFields", None),
	}
	_prop_map_put_ = {
		"CommandText": ((0, LCID, 4, 0),()),
		"IndexFields": ((10, LCID, 4, 0),()),
	}
	# Default property for this class is 'CommandText'
	def __call__(self):
		return self._ApplyTypes_(*(0, 2, (8, 0), (), "CommandText", None))
	# str(ob) and int(ob) will use __call__
	def __unicode__(self, *args):
		try:
			return unicode(self.__call__(*args))
		except pythoncom.com_error:
			return repr(self)
	def __str__(self, *args):
		return str(self.__unicode__(*args))
	def __int__(self, *args):
		return int(self.__call__(*args))
	#This class has Count() property - allow len(ob) to provide this
	def __len__(self):
		return self._ApplyTypes_(*(1, 2, (3, 0), (), "Count", None))
	#This class has a __len__ - this is needed so 'if object:' always returns TRUE.
	def __nonzero__(self):
		return True

class IComponent(DispatchBaseClass):
	"""Represents a Business Process Component."""
	CLSID = IID('{E8BFF66F-A020-4909-B9E3-1591182D27D7}')
	coclass_clsid = IID('{71CFAB12-C911-4E7A-B4CE-3F65E45344A1}')

	# Result is of type IList
	def BusinessProcesses(self):
		"""The list of associated BusinessProcess objects."""
		ret = self._oleobj_.InvokeTypes(19, LCID, 1, (9, 0), (),)
		if ret is not None:
			ret = Dispatch(ret, u'BusinessProcesses', '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')
		return ret

	def DeletePicture(self, bsData=defaultNamedNotOptArg):
		"""Deletes the snapshot."""
		return self._oleobj_.InvokeTypes(27, LCID, 1, (24, 0), ((8, 1),),bsData
			)

	# The method ExtendedStorage is actually a property, but must be used as a method to correctly pass the arguments
	def ExtendedStorage(self, nEntityType=defaultNamedNotOptArg):
		"""The ExtendedStorage object for this component."""
		ret = self._oleobj_.InvokeTypes(25, LCID, 2, (9, 0), ((3, 1),),nEntityType
			)
		if ret is not None:
			ret = Dispatch(ret, u'ExtendedStorage', None)
		return ret

	# The method Field is actually a property, but must be used as a method to correctly pass the arguments
	def Field(self, FieldName=defaultNamedNotOptArg):
		"""The value of the specified field."""
		return self._ApplyTypes_(0, 2, (12, 0), ((8, 0),), u'Field', None,FieldName
			)

	def LockObject(self):
		"""Locks the object. Returns True if lock successful."""
		return self._oleobj_.InvokeTypes(2, LCID, 1, (11, 0), (),)

	def Post(self):
		"""Posts all changed values into database."""
		return self._oleobj_.InvokeTypes(6, LCID, 1, (24, 0), (),)

	def Refresh(self):
		"""Reads saved values, overwriting values in memory."""
		return self._oleobj_.InvokeTypes(7, LCID, 1, (24, 0), (),)

	# The method SetField is actually a property, but must be used as a method to correctly pass the arguments
	def SetField(self, FieldName=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The value of the specified field."""
		return self._oleobj_.InvokeTypes(0, LCID, 4, (24, 0), ((8, 0), (12, 1)),FieldName
			, arg1)

	def SetStepsData(self):
		"""For backward compatiblity with versions prior to 9.0. Wrap component's step in HTML format"""
		return self._oleobj_.InvokeTypes(29, LCID, 1, (24, 0), (),)

	def UnLockObject(self):
		"""Unlocks the object."""
		return self._oleobj_.InvokeTypes(3, LCID, 1, (24, 0), (),)

	def Undo(self):
		"""Undoes changes to field values that have not been posted."""
		return self._oleobj_.InvokeTypes(8, LCID, 1, (24, 0), (),)

	_prop_map_get_ = {
		"ApplicationAreaID": (30, 2, (3, 0), (), "ApplicationAreaID", None),
		"Attachments": (12, 2, (9, 0), (), "Attachments", None),
		"AutoPost": (5, 2, (11, 0), (), "AutoPost", None),
		"ComponentParamFactory": (14, 2, (9, 0), (), "ComponentParamFactory", None),
		"ComponentStepFactory": (28, 2, (9, 0), (), "ComponentStepFactory", None),
		"Folder": (22, 2, (9, 0), (), "Folder", None),
		"HasAttachment": (13, 2, (11, 0), (), "HasAttachment", None),
		"HasPicture": (24, 2, (11, 0), (), "HasPicture", None),
		"HasScript": (23, 2, (11, 0), (), "HasScript", None),
		"History": (11, 2, (9, 0), (), "History", None),
		"ID": (4, 2, (12, 0), (), "ID", None),
		"IsIteratable": (20, 2, (11, 0), (), "IsIteratable", None),
		"IsLocked": (1, 2, (11, 0), (), "IsLocked", None),
		"IsObsolete": (21, 2, (11, 0), (), "IsObsolete", None),
		"Modified": (9, 2, (11, 0), (), "Modified", None),
		"Name": (18, 2, (8, 0), (), "Name", None),
		"ScriptType": (26, 2, (8, 0), (), "ScriptType", None),
		"Virtual": (10, 2, (11, 0), (), "Virtual", None),
	}
	_prop_map_put_ = {
		"ApplicationAreaID": ((30, LCID, 4, 0),()),
		"AutoPost": ((5, LCID, 4, 0),()),
		"HasPicture": ((24, LCID, 4, 0),()),
		"IsIteratable": ((20, LCID, 4, 0),()),
		"Name": ((18, LCID, 4, 0),()),
		"ScriptType": ((26, LCID, 4, 0),()),
	}
	# Default method for this class is 'Field'
	def __call__(self, FieldName=defaultNamedNotOptArg):
		"""The value of the specified field."""
		return self._ApplyTypes_(0, 2, (12, 0), ((8, 0),), '__call__', None,FieldName
			)

	# str(ob) and int(ob) will use __call__
	def __unicode__(self, *args):
		try:
			return unicode(self.__call__(*args))
		except pythoncom.com_error:
			return repr(self)
	def __str__(self, *args):
		return str(self.__unicode__(*args))
	def __int__(self, *args):
		return int(self.__call__(*args))

class IComponentFactory(DispatchBaseClass):
	"""Services for managing Business Process Components."""
	CLSID = IID('{A4CEDAE3-5E32-43FC-9D39-FDA737799A1E}')
	coclass_clsid = IID('{0B280A46-1606-4FD3-90E4-EF5149725200}')

	def AddItem(self, ItemData=defaultNamedNotOptArg):
		"""Creates a new item object."""
		ret = self._oleobj_.InvokeTypes(3, LCID, 1, (9, 0), ((12, 0),),ItemData
			)
		if ret is not None:
			ret = Dispatch(ret, u'AddItem', None)
		return ret

	# The method FetchLevel is actually a property, but must be used as a method to correctly pass the arguments
	def FetchLevel(self, FieldName=defaultNamedNotOptArg):
		"""The Fetch level for a field."""
		return self._oleobj_.InvokeTypes(7, LCID, 2, (2, 0), ((8, 1),),FieldName
			)

	def IsComponentNameValid(self, bsName=defaultNamedNotOptArg, pbsErrorString=pythoncom.Missing):
		"""Checks if the specified name is a valid name for a Business Process Component."""
		return self._ApplyTypes_(21, 1, (11, 0), ((8, 1), (16392, 2)), u'IsComponentNameValid', None,bsName
			, pbsErrorString)

	# The method Item is actually a property, but must be used as a method to correctly pass the arguments
	def Item(self, ItemKey=defaultNamedNotOptArg):
		"""Gets an object managed by the factory by its key."""
		ret = self._oleobj_.InvokeTypes(0, LCID, 2, (9, 0), ((12, 0),),ItemKey
			)
		if ret is not None:
			ret = Dispatch(ret, u'Item', None)
		return ret

	# Result is of type IList
	def NewList(self, Filter=defaultNamedNotOptArg):
		"""Creates a list of objects according to the specified filter."""
		ret = self._oleobj_.InvokeTypes(1, LCID, 1, (9, 0), ((8, 0),),Filter
			)
		if ret is not None:
			ret = Dispatch(ret, u'NewList', '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')
		return ret

	def RemoveItem(self, ItemKey=defaultNamedNotOptArg):
		"""Removes item from the database. Removal takes place immediately, without a Post."""
		return self._oleobj_.InvokeTypes(4, LCID, 1, (24, 0), ((12, 0),),ItemKey
			)

	# The method SetFetchLevel is actually a property, but must be used as a method to correctly pass the arguments
	def SetFetchLevel(self, FieldName=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The Fetch level for a field."""
		return self._oleobj_.InvokeTypes(7, LCID, 4, (24, 0), ((8, 1), (2, 1)),FieldName
			, arg1)

	def get_ItemFromServer(self, ItemKey=defaultNamedNotOptArg, pItem=defaultNamedNotOptArg):
		"""method get_ItemFromServer"""
		return self._oleobj_.InvokeTypes(20, LCID, 1, (24, 0), ((12, 0), (16393, 0)),ItemKey
			, pItem)

	_prop_map_get_ = {
		# Method 'Fields' returns object of type 'IList'
		"Fields": (2, 2, (9, 0), (), "Fields", '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}'),
		"Filter": (5, 2, (9, 0), (), "Filter", None),
		"History": (6, 2, (9, 0), (), "History", None),
	}
	_prop_map_put_ = {
	}
	# Default method for this class is 'Item'
	def __call__(self, ItemKey=defaultNamedNotOptArg):
		"""Gets an object managed by the factory by its key."""
		ret = self._oleobj_.InvokeTypes(0, LCID, 2, (9, 0), ((12, 0),),ItemKey
			)
		if ret is not None:
			ret = Dispatch(ret, '__call__', None)
		return ret

	# str(ob) and int(ob) will use __call__
	def __unicode__(self, *args):
		try:
			return unicode(self.__call__(*args))
		except pythoncom.com_error:
			return repr(self)
	def __str__(self, *args):
		return str(self.__unicode__(*args))
	def __int__(self, *args):
		return int(self.__call__(*args))

class IComponentFolder(DispatchBaseClass):
	"""Represents a Business Process Component folder."""
	CLSID = IID('{7A6F279C-7729-4394-B5BB-BC3E8902DA56}')
	coclass_clsid = IID('{E5A4F777-79B4-4999-B9CF-4733946D6381}')

	# The method Field is actually a property, but must be used as a method to correctly pass the arguments
	def Field(self, FieldName=defaultNamedNotOptArg):
		"""The value of the specified field."""
		return self._ApplyTypes_(0, 2, (12, 0), ((8, 0),), u'Field', None,FieldName
			)

	def LockObject(self):
		"""Locks the object. Returns True if lock successful."""
		return self._oleobj_.InvokeTypes(2, LCID, 1, (11, 0), (),)

	def Post(self):
		"""Posts all changed values into database."""
		return self._oleobj_.InvokeTypes(6, LCID, 1, (24, 0), (),)

	def Refresh(self):
		"""Reads saved values, overwriting values in memory."""
		return self._oleobj_.InvokeTypes(7, LCID, 1, (24, 0), (),)

	# The method SetField is actually a property, but must be used as a method to correctly pass the arguments
	def SetField(self, FieldName=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The value of the specified field."""
		return self._oleobj_.InvokeTypes(0, LCID, 4, (24, 0), ((8, 0), (12, 1)),FieldName
			, arg1)

	def UnLockObject(self):
		"""Unlocks the object."""
		return self._oleobj_.InvokeTypes(3, LCID, 1, (24, 0), (),)

	def Undo(self):
		"""Undoes changes to field values that have not been posted."""
		return self._oleobj_.InvokeTypes(8, LCID, 1, (24, 0), (),)

	_prop_map_get_ = {
		"Attachments": (12, 2, (9, 0), (), "Attachments", None),
		"AutoPost": (5, 2, (11, 0), (), "AutoPost", None),
		"ComponentFactory": (15, 2, (9, 0), (), "ComponentFactory", None),
		"ComponentFolderFactory": (16, 2, (9, 0), (), "ComponentFolderFactory", None),
		"HasAttachment": (13, 2, (11, 0), (), "HasAttachment", None),
		"History": (11, 2, (9, 0), (), "History", None),
		"ID": (4, 2, (12, 0), (), "ID", None),
		"IsLocked": (1, 2, (11, 0), (), "IsLocked", None),
		"Modified": (9, 2, (11, 0), (), "Modified", None),
		"Name": (14, 2, (8, 0), (), "Name", None),
		"Path": (17, 2, (8, 0), (), "Path", None),
		"Virtual": (10, 2, (11, 0), (), "Virtual", None),
	}
	_prop_map_put_ = {
		"AutoPost": ((5, LCID, 4, 0),()),
		"Name": ((14, LCID, 4, 0),()),
	}
	# Default method for this class is 'Field'
	def __call__(self, FieldName=defaultNamedNotOptArg):
		"""The value of the specified field."""
		return self._ApplyTypes_(0, 2, (12, 0), ((8, 0),), '__call__', None,FieldName
			)

	# str(ob) and int(ob) will use __call__
	def __unicode__(self, *args):
		try:
			return unicode(self.__call__(*args))
		except pythoncom.com_error:
			return repr(self)
	def __str__(self, *args):
		return str(self.__unicode__(*args))
	def __int__(self, *args):
		return int(self.__call__(*args))

class IComponentFolderFactory(DispatchBaseClass):
	"""Services for managing the organization of Business Process Components."""
	CLSID = IID('{D6F05DAE-7F99-4FE0-9432-D48CAC4BA16E}')
	coclass_clsid = IID('{36F5E5F1-C949-40AF-91E8-A4BC647B2A92}')

	def AddItem(self, ItemData=defaultNamedNotOptArg):
		"""Creates a new item object."""
		ret = self._oleobj_.InvokeTypes(3, LCID, 1, (9, 0), ((12, 0),),ItemData
			)
		if ret is not None:
			ret = Dispatch(ret, u'AddItem', None)
		return ret

	# The method FetchLevel is actually a property, but must be used as a method to correctly pass the arguments
	def FetchLevel(self, FieldName=defaultNamedNotOptArg):
		"""The Fetch level for a field."""
		return self._oleobj_.InvokeTypes(7, LCID, 2, (2, 0), ((8, 1),),FieldName
			)

	def FolderByPath(self, Path=defaultNamedNotOptArg):
		"""method FolderByPath"""
		ret = self._oleobj_.InvokeTypes(10, LCID, 1, (9, 0), ((8, 1),),Path
			)
		if ret is not None:
			ret = Dispatch(ret, u'FolderByPath', None)
		return ret

	def FolderPath(self, folderId=defaultNamedNotOptArg):
		"""Returns the path of the specified folder."""
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(9, LCID, 1, (8, 0), ((3, 1),),folderId
			)

	# The method Item is actually a property, but must be used as a method to correctly pass the arguments
	def Item(self, ItemKey=defaultNamedNotOptArg):
		"""Gets an object managed by the factory by its key."""
		ret = self._oleobj_.InvokeTypes(0, LCID, 2, (9, 0), ((12, 0),),ItemKey
			)
		if ret is not None:
			ret = Dispatch(ret, u'Item', None)
		return ret

	# Result is of type IList
	def NewList(self, Filter=defaultNamedNotOptArg):
		"""Creates a list of objects according to the specified filter."""
		ret = self._oleobj_.InvokeTypes(1, LCID, 1, (9, 0), ((8, 0),),Filter
			)
		if ret is not None:
			ret = Dispatch(ret, u'NewList', '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')
		return ret

	def Obsolete(self):
		"""method Obsolete"""
		ret = self._oleobj_.InvokeTypes(13, LCID, 1, (9, 0), (),)
		if ret is not None:
			ret = Dispatch(ret, u'Obsolete', None)
		return ret

	def RemoveItem(self, ItemKey=defaultNamedNotOptArg):
		"""Removes item from the database. Removal takes place immediately, without a Post."""
		return self._oleobj_.InvokeTypes(4, LCID, 1, (24, 0), ((12, 0),),ItemKey
			)

	def Root(self):
		"""The root folder for Business Process Components."""
		ret = self._oleobj_.InvokeTypes(8, LCID, 1, (9, 0), (),)
		if ret is not None:
			ret = Dispatch(ret, u'Root', None)
		return ret

	# The method SetFetchLevel is actually a property, but must be used as a method to correctly pass the arguments
	def SetFetchLevel(self, FieldName=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The Fetch level for a field."""
		return self._oleobj_.InvokeTypes(7, LCID, 4, (24, 0), ((8, 1), (2, 1)),FieldName
			, arg1)

	def Templates(self):
		"""method Templates"""
		ret = self._oleobj_.InvokeTypes(12, LCID, 1, (9, 0), (),)
		if ret is not None:
			ret = Dispatch(ret, u'Templates', None)
		return ret

	# Result is of type IList
	def UnattachedComponents(self):
		"""method UnattachedComponents"""
		ret = self._oleobj_.InvokeTypes(11, LCID, 1, (9, 0), (),)
		if ret is not None:
			ret = Dispatch(ret, u'UnattachedComponents', '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')
		return ret

	_prop_map_get_ = {
		# Method 'Fields' returns object of type 'IList'
		"Fields": (2, 2, (9, 0), (), "Fields", '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}'),
		"Filter": (5, 2, (9, 0), (), "Filter", None),
		"History": (6, 2, (9, 0), (), "History", None),
	}
	_prop_map_put_ = {
	}
	# Default method for this class is 'Item'
	def __call__(self, ItemKey=defaultNamedNotOptArg):
		"""Gets an object managed by the factory by its key."""
		ret = self._oleobj_.InvokeTypes(0, LCID, 2, (9, 0), ((12, 0),),ItemKey
			)
		if ret is not None:
			ret = Dispatch(ret, '__call__', None)
		return ret

	# str(ob) and int(ob) will use __call__
	def __unicode__(self, *args):
		try:
			return unicode(self.__call__(*args))
		except pythoncom.com_error:
			return repr(self)
	def __str__(self, *args):
		return str(self.__unicode__(*args))
	def __int__(self, *args):
		return int(self.__call__(*args))

class IComponentParam(DispatchBaseClass):
	"""Services for managing Business Process Test parameters."""
	CLSID = IID('{F65AD9CD-D5A3-4EC5-8904-017E4E9D2351}')
	coclass_clsid = IID('{4FFF066E-4D6B-444B-94B1-73209211C62B}')

	# The method Field is actually a property, but must be used as a method to correctly pass the arguments
	def Field(self, FieldName=defaultNamedNotOptArg):
		"""The value of the specified field."""
		return self._ApplyTypes_(0, 2, (12, 0), ((8, 0),), u'Field', None,FieldName
			)

	def LockObject(self):
		"""Locks the object. Returns True if lock successful."""
		return self._oleobj_.InvokeTypes(2, LCID, 1, (11, 0), (),)

	def Post(self):
		"""Posts all changed values into database."""
		return self._oleobj_.InvokeTypes(6, LCID, 1, (24, 0), (),)

	def Refresh(self):
		"""Reads saved values, overwriting values in memory."""
		return self._oleobj_.InvokeTypes(7, LCID, 1, (24, 0), (),)

	# The method SetField is actually a property, but must be used as a method to correctly pass the arguments
	def SetField(self, FieldName=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The value of the specified field."""
		return self._oleobj_.InvokeTypes(0, LCID, 4, (24, 0), ((8, 0), (12, 1)),FieldName
			, arg1)

	def UnLockObject(self):
		"""Unlocks the object."""
		return self._oleobj_.InvokeTypes(3, LCID, 1, (24, 0), (),)

	def Undo(self):
		"""Undoes changes to field values that have not been posted."""
		return self._oleobj_.InvokeTypes(8, LCID, 1, (24, 0), (),)

	_prop_map_get_ = {
		"AutoPost": (5, 2, (11, 0), (), "AutoPost", None),
		"Component": (20, 2, (9, 0), (), "Component", None),
		"Desc": (16, 2, (8, 0), (), "Desc", None),
		"ID": (4, 2, (12, 0), (), "ID", None),
		"IsLocked": (1, 2, (11, 0), (), "IsLocked", None),
		"IsOut": (21, 2, (3, 0), (), "IsOut", None),
		"Modified": (9, 2, (11, 0), (), "Modified", None),
		"Name": (14, 2, (8, 0), (), "Name", None),
		"Order": (18, 2, (3, 0), (), "Order", None),
		"Value": (15, 2, (8, 0), (), "Value", None),
		"ValueType": (17, 2, (8, 0), (), "ValueType", None),
		"Virtual": (10, 2, (11, 0), (), "Virtual", None),
	}
	_prop_map_put_ = {
		"AutoPost": ((5, LCID, 4, 0),()),
		"Desc": ((16, LCID, 4, 0),()),
		"IsOut": ((21, LCID, 4, 0),()),
		"Name": ((14, LCID, 4, 0),()),
		"Order": ((18, LCID, 4, 0),()),
		"Value": ((15, LCID, 4, 0),()),
		"ValueType": ((17, LCID, 4, 0),()),
	}
	# Default method for this class is 'Field'
	def __call__(self, FieldName=defaultNamedNotOptArg):
		"""The value of the specified field."""
		return self._ApplyTypes_(0, 2, (12, 0), ((8, 0),), '__call__', None,FieldName
			)

	# str(ob) and int(ob) will use __call__
	def __unicode__(self, *args):
		try:
			return unicode(self.__call__(*args))
		except pythoncom.com_error:
			return repr(self)
	def __str__(self, *args):
		return str(self.__unicode__(*args))
	def __int__(self, *args):
		return int(self.__call__(*args))

class IComponentStep(DispatchBaseClass):
	"""Represents a step in a Business Process Component."""
	CLSID = IID('{6687454E-8D1A-462E-BFDE-E342BFF132D3}')
	coclass_clsid = IID('{D600F6C4-1A12-4973-80A9-A976300DF1D7}')

	# The method Field is actually a property, but must be used as a method to correctly pass the arguments
	def Field(self, FieldName=defaultNamedNotOptArg):
		"""The value of the specified field."""
		return self._ApplyTypes_(0, 2, (12, 0), ((8, 0),), u'Field', None,FieldName
			)

	def LockObject(self):
		"""Locks the object. Returns True if lock successful."""
		return self._oleobj_.InvokeTypes(2, LCID, 1, (11, 0), (),)

	def Post(self):
		"""Posts all changed values into database."""
		return self._oleobj_.InvokeTypes(6, LCID, 1, (24, 0), (),)

	def Refresh(self):
		"""Reads saved values, overwriting values in memory."""
		return self._oleobj_.InvokeTypes(7, LCID, 1, (24, 0), (),)

	# The method SetField is actually a property, but must be used as a method to correctly pass the arguments
	def SetField(self, FieldName=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The value of the specified field."""
		return self._oleobj_.InvokeTypes(0, LCID, 4, (24, 0), ((8, 0), (12, 1)),FieldName
			, arg1)

	def UnLockObject(self):
		"""Unlocks the object."""
		return self._oleobj_.InvokeTypes(3, LCID, 1, (24, 0), (),)

	def Undo(self):
		"""Undoes changes to field values that have not been posted."""
		return self._oleobj_.InvokeTypes(8, LCID, 1, (24, 0), (),)

	def Validate(self):
		"""Validate the step."""
		return self._oleobj_.InvokeTypes(16, LCID, 1, (24, 0), (),)

	_prop_map_get_ = {
		"AutoPost": (5, 2, (11, 0), (), "AutoPost", None),
		"ID": (4, 2, (12, 0), (), "ID", None),
		"IsLocked": (1, 2, (11, 0), (), "IsLocked", None),
		"Modified": (9, 2, (11, 0), (), "Modified", None),
		"Order": (12, 2, (3, 0), (), "Order", None),
		"Parent": (13, 2, (9, 0), (), "Parent", None),
		"StepDescription": (11, 2, (8, 0), (), "StepDescription", None),
		"StepExpectedResult": (15, 2, (8, 0), (), "StepExpectedResult", None),
		"StepName": (14, 2, (8, 0), (), "StepName", None),
		"Virtual": (10, 2, (11, 0), (), "Virtual", None),
	}
	_prop_map_put_ = {
		"AutoPost": ((5, LCID, 4, 0),()),
		"Order": ((12, LCID, 4, 0),()),
		"StepDescription": ((11, LCID, 4, 0),()),
		"StepExpectedResult": ((15, LCID, 4, 0),()),
		"StepName": ((14, LCID, 4, 0),()),
	}
	# Default method for this class is 'Field'
	def __call__(self, FieldName=defaultNamedNotOptArg):
		"""The value of the specified field."""
		return self._ApplyTypes_(0, 2, (12, 0), ((8, 0),), '__call__', None,FieldName
			)

	# str(ob) and int(ob) will use __call__
	def __unicode__(self, *args):
		try:
			return unicode(self.__call__(*args))
		except pythoncom.com_error:
			return repr(self)
	def __str__(self, *args):
		return str(self.__unicode__(*args))
	def __int__(self, *args):
		return int(self.__call__(*args))

class ICondition(DispatchBaseClass):
	"""Represents a condition for a test to be executed."""
	CLSID = IID('{8A01DC68-2D34-4CB2-81D7-968F37C50715}')
	coclass_clsid = IID('{453FE104-67B0-470D-8D1E-70E7A8CF7774}')

	_prop_map_get_ = {
		"Description": (4, 2, (8, 0), (), "Description", None),
		"ID": (5, 2, (12, 0), (), "ID", None),
		"Source": (2, 2, (12, 0), (), "Source", None),
		"SourceInstance": (6, 2, (12, 0), (), "SourceInstance", None),
		"SourceTestId": (8, 2, (12, 0), (), "SourceTestId", None),
		"Target": (3, 2, (12, 0), (), "Target", None),
		"TargetInstance": (7, 2, (12, 0), (), "TargetInstance", None),
		"TargetTestId": (9, 2, (12, 0), (), "TargetTestId", None),
		"Type": (1, 2, (2, 0), (), "Type", None),
		"Value": (0, 2, (12, 0), (), "Value", None),
	}
	_prop_map_put_ = {
		"Description": ((4, LCID, 4, 0),()),
		"Value": ((0, LCID, 4, 0),()),
	}
	# Default property for this class is 'Value'
	def __call__(self):
		return self._ApplyTypes_(*(0, 2, (12, 0), (), "Value", None))
	# str(ob) and int(ob) will use __call__
	def __unicode__(self, *args):
		try:
			return unicode(self.__call__(*args))
		except pythoncom.com_error:
			return repr(self)
	def __str__(self, *args):
		return str(self.__unicode__(*args))
	def __int__(self, *args):
		return int(self.__call__(*args))

class IConditionFactory(DispatchBaseClass):
	"""Services for managing conditions for executing test instances in an test set."""
	CLSID = IID('{8C63633A-6537-41B5-93A3-982823DE8689}')
	coclass_clsid = IID('{EEBD59D7-740A-48EE-82F2-14BC33B54F14}')

	def AddItem(self, ItemData=defaultNamedNotOptArg):
		"""Creates a new item object."""
		ret = self._oleobj_.InvokeTypes(3, LCID, 1, (9, 0), ((12, 0),),ItemData
			)
		if ret is not None:
			ret = Dispatch(ret, u'AddItem', None)
		return ret

	# The method FetchLevel is actually a property, but must be used as a method to correctly pass the arguments
	def FetchLevel(self, FieldName=defaultNamedNotOptArg):
		"""The Fetch level for a field."""
		return self._oleobj_.InvokeTypes(7, LCID, 2, (2, 0), ((8, 1),),FieldName
			)

	# The method Item is actually a property, but must be used as a method to correctly pass the arguments
	def Item(self, ItemKey=defaultNamedNotOptArg):
		"""Gets an object managed by the factory by its key."""
		ret = self._oleobj_.InvokeTypes(0, LCID, 2, (9, 0), ((12, 0),),ItemKey
			)
		if ret is not None:
			ret = Dispatch(ret, u'Item', None)
		return ret

	# Result is of type IList
	def NewList(self, Filter=defaultNamedNotOptArg):
		"""Creates a list of objects according to the specified filter."""
		ret = self._oleobj_.InvokeTypes(1, LCID, 1, (9, 0), ((8, 0),),Filter
			)
		if ret is not None:
			ret = Dispatch(ret, u'NewList', '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')
		return ret

	def RemoveItem(self, ItemKey=defaultNamedNotOptArg):
		"""Removes item from the database. Removal takes place immediately, without a Post."""
		return self._oleobj_.InvokeTypes(4, LCID, 1, (24, 0), ((12, 0),),ItemKey
			)

	def Save(self):
		"""Saves all conditions."""
		return self._oleobj_.InvokeTypes(8, LCID, 1, (24, 0), (),)

	# The method SetFetchLevel is actually a property, but must be used as a method to correctly pass the arguments
	def SetFetchLevel(self, FieldName=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The Fetch level for a field."""
		return self._oleobj_.InvokeTypes(7, LCID, 4, (24, 0), ((8, 1), (2, 1)),FieldName
			, arg1)

	_prop_map_get_ = {
		# Method 'Fields' returns object of type 'IList'
		"Fields": (2, 2, (9, 0), (), "Fields", '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}'),
		"Filter": (5, 2, (9, 0), (), "Filter", None),
		"History": (6, 2, (9, 0), (), "History", None),
	}
	_prop_map_put_ = {
	}
	# Default method for this class is 'Item'
	def __call__(self, ItemKey=defaultNamedNotOptArg):
		"""Gets an object managed by the factory by its key."""
		ret = self._oleobj_.InvokeTypes(0, LCID, 2, (9, 0), ((12, 0),),ItemKey
			)
		if ret is not None:
			ret = Dispatch(ret, '__call__', None)
		return ret

	# str(ob) and int(ob) will use __call__
	def __unicode__(self, *args):
		try:
			return unicode(self.__call__(*args))
		except pythoncom.com_error:
			return repr(self)
	def __str__(self, *args):
		return str(self.__unicode__(*args))
	def __int__(self, *args):
		return int(self.__call__(*args))

class ICoverable(DispatchBaseClass):
	"""Represents an entity that covers a requirement."""
	CLSID = IID('{C5DF6450-F927-425A-A293-3A5CF0EBE56D}')
	coclass_clsid = IID('{488D6BE8-A962-4958-8E11-6E2EE9A20EFF}')

	def CoverRequirementEx(self, Req=defaultNamedNotOptArg, RequirementFilter=defaultNamedNotOptArg, Recursive=defaultNamedNotOptArg):
		"""Adds the specified requirement to the list of requirements covered by this entity. Optionally, also adds child requirements."""
		return self._oleobj_.InvokeTypes(2, LCID, 1, (24, 0), ((12, 1), (8, 1), (11, 1)),Req
			, RequirementFilter, Recursive)

	_prop_map_get_ = {
		"CoverageFactory": (1, 2, (9, 0), (), "CoverageFactory", None),
	}
	_prop_map_put_ = {
	}

class ICoverableReq(DispatchBaseClass):
	"""Represents a requirement that is covered by another entity."""
	CLSID = IID('{F061ABB7-A65F-4146-8621-5A8B04C87B8D}')
	coclass_clsid = IID('{A94555C2-77AD-4F2D-B061-A9ED11AE9FE6}')

	def AddSubjectToCoverage(self, SubjectID=defaultNamedNotOptArg, TestFilter=defaultNamedNotOptArg):
		"""Adds the tests  from the specified subject that match the input filter to the list of tests that cover the current requirement."""
		return self._oleobj_.InvokeTypes(5, LCID, 1, (24, 0), ((3, 1), (8, 1)),SubjectID
			, TestFilter)

	def AddTestInstanceToCoverage(self, TestInstanceID=defaultNamedNotOptArg):
		"""Adds the test instance to the list of test instances that cover the current requirement."""
		return self._oleobj_.InvokeTypes(2, LCID, 1, (24, 0), ((3, 1),),TestInstanceID
			)

	def AddTestToCoverage(self, TestId=defaultNamedNotOptArg):
		"""Adds the test to the list of tests that cover the current requirement."""
		return self._oleobj_.InvokeTypes(3, LCID, 1, (24, 0), ((3, 1),),TestId
			)

	def AddTestsFromTestSetToCoverage(self, TestSetID=defaultNamedNotOptArg, TestInstanceFilter=defaultNamedNotOptArg):
		"""Adds the test instances from the specified test set that match the input filter to the list of test instances that cover the current requirement."""
		return self._oleobj_.InvokeTypes(4, LCID, 1, (24, 0), ((3, 1), (8, 1)),TestSetID
			, TestInstanceFilter)

	_prop_map_get_ = {
		"RequirementCoverageFactory": (1, 2, (9, 0), (), "RequirementCoverageFactory", None),
	}
	_prop_map_put_ = {
	}

class ICoverageEntity(DispatchBaseClass):
	"""Represents the association between a requirement and another entity that covers it."""
	CLSID = IID('{BCF7E6D7-4281-40FA-923B-32DFA90715D7}')
	coclass_clsid = IID('{F87FF5C2-4631-4E5E-BF46-38490501A12B}')

	# The method Field is actually a property, but must be used as a method to correctly pass the arguments
	def Field(self, FieldName=defaultNamedNotOptArg):
		"""The value of the specified field."""
		return self._ApplyTypes_(0, 2, (12, 0), ((8, 0),), u'Field', None,FieldName
			)

	def LockObject(self):
		"""Locks the object. Returns True if lock successful."""
		return self._oleobj_.InvokeTypes(2, LCID, 1, (11, 0), (),)

	def Post(self):
		"""Posts all changed values into database."""
		return self._oleobj_.InvokeTypes(6, LCID, 1, (24, 0), (),)

	def Refresh(self):
		"""Reads saved values, overwriting values in memory."""
		return self._oleobj_.InvokeTypes(7, LCID, 1, (24, 0), (),)

	# The method SetField is actually a property, but must be used as a method to correctly pass the arguments
	def SetField(self, FieldName=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The value of the specified field."""
		return self._oleobj_.InvokeTypes(0, LCID, 4, (24, 0), ((8, 0), (12, 1)),FieldName
			, arg1)

	def UnLockObject(self):
		"""Unlocks the object."""
		return self._oleobj_.InvokeTypes(3, LCID, 1, (24, 0), (),)

	def Undo(self):
		"""Undoes changes to field values that have not been posted."""
		return self._oleobj_.InvokeTypes(8, LCID, 1, (24, 0), (),)

	_prop_map_get_ = {
		"AutoPost": (5, 2, (11, 0), (), "AutoPost", None),
		"CoverageType": (13, 2, (8, 0), (), "CoverageType", None),
		"ID": (4, 2, (12, 0), (), "ID", None),
		"IsLocked": (1, 2, (11, 0), (), "IsLocked", None),
		"Modified": (9, 2, (11, 0), (), "Modified", None),
		"Name": (15, 2, (8, 0), (), "Name", None),
		"RequirementEntity": (11, 2, (9, 0), (), "RequirementEntity", None),
		"Status": (14, 2, (8, 0), (), "Status", None),
		"TargetEntity": (12, 2, (9, 0), (), "TargetEntity", None),
		"Virtual": (10, 2, (11, 0), (), "Virtual", None),
	}
	_prop_map_put_ = {
		"AutoPost": ((5, LCID, 4, 0),()),
	}
	# Default method for this class is 'Field'
	def __call__(self, FieldName=defaultNamedNotOptArg):
		"""The value of the specified field."""
		return self._ApplyTypes_(0, 2, (12, 0), ((8, 0),), '__call__', None,FieldName
			)

	# str(ob) and int(ob) will use __call__
	def __unicode__(self, *args):
		try:
			return unicode(self.__call__(*args))
		except pythoncom.com_error:
			return repr(self)
	def __str__(self, *args):
		return str(self.__unicode__(*args))
	def __int__(self, *args):
		return int(self.__call__(*args))

class ICoverageFactory(DispatchBaseClass):
	"""Services for managing CoverageEntity objects."""
	CLSID = IID('{FD167DFE-8DEE-4930-AFAA-A83C2B1480EB}')
	coclass_clsid = IID('{D68C44C2-DF91-4095-B145-7FF8E430FBA7}')

	def AddItem(self, ItemData=defaultNamedNotOptArg):
		"""Creates a new item object."""
		ret = self._oleobj_.InvokeTypes(3, LCID, 1, (9, 0), ((12, 0),),ItemData
			)
		if ret is not None:
			ret = Dispatch(ret, u'AddItem', None)
		return ret

	# The method FetchLevel is actually a property, but must be used as a method to correctly pass the arguments
	def FetchLevel(self, FieldName=defaultNamedNotOptArg):
		"""The Fetch level for a field."""
		return self._oleobj_.InvokeTypes(7, LCID, 2, (2, 0), ((8, 1),),FieldName
			)

	# The method Item is actually a property, but must be used as a method to correctly pass the arguments
	def Item(self, ItemKey=defaultNamedNotOptArg):
		"""Gets an object managed by the factory by its key."""
		ret = self._oleobj_.InvokeTypes(0, LCID, 2, (9, 0), ((12, 0),),ItemKey
			)
		if ret is not None:
			ret = Dispatch(ret, u'Item', None)
		return ret

	# Result is of type IList
	def NewList(self, Filter=defaultNamedNotOptArg):
		"""Creates a list of objects according to the specified filter."""
		ret = self._oleobj_.InvokeTypes(1, LCID, 1, (9, 0), ((8, 0),),Filter
			)
		if ret is not None:
			ret = Dispatch(ret, u'NewList', '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')
		return ret

	def RemoveItem(self, ItemKey=defaultNamedNotOptArg):
		"""Removes item from the database. Removal takes place immediately, without a Post."""
		return self._oleobj_.InvokeTypes(4, LCID, 1, (24, 0), ((12, 0),),ItemKey
			)

	# The method SetFetchLevel is actually a property, but must be used as a method to correctly pass the arguments
	def SetFetchLevel(self, FieldName=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The Fetch level for a field."""
		return self._oleobj_.InvokeTypes(7, LCID, 4, (24, 0), ((8, 1), (2, 1)),FieldName
			, arg1)

	_prop_map_get_ = {
		# Method 'Fields' returns object of type 'IList'
		"Fields": (2, 2, (9, 0), (), "Fields", '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}'),
		"Filter": (5, 2, (9, 0), (), "Filter", None),
		"History": (6, 2, (9, 0), (), "History", None),
	}
	_prop_map_put_ = {
	}
	# Default method for this class is 'Item'
	def __call__(self, ItemKey=defaultNamedNotOptArg):
		"""Gets an object managed by the factory by its key."""
		ret = self._oleobj_.InvokeTypes(0, LCID, 2, (9, 0), ((12, 0),),ItemKey
			)
		if ret is not None:
			ret = Dispatch(ret, '__call__', None)
		return ret

	# str(ob) and int(ob) will use __call__
	def __unicode__(self, *args):
		try:
			return unicode(self.__call__(*args))
		except pythoncom.com_error:
			return repr(self)
	def __str__(self, *args):
		return str(self.__unicode__(*args))
	def __int__(self, *args):
		return int(self.__call__(*args))

class ICustomization(DispatchBaseClass):
	"""Services to perform customization tasks, such as adding users to user groups, defining user-defined fields, and defining user access privileges."""
	CLSID = IID('{9D4F53EF-41A2-4059-8AB3-13BBCA8333E8}')
	coclass_clsid = None

	def Commit(self):
		"""Posts all customization data changes to the project database."""
		return self._oleobj_.InvokeTypes(5, LCID, 1, (24, 0), (),)

	def Load(self):
		"""Loads all customization data from the server into the client cache. This includes actions, fields, lists, modules, permissions, users, and user groups."""
		return self._oleobj_.InvokeTypes(4, LCID, 1, (24, 0), (),)

	def LockObject(self):
		"""Locks the object. Returns True if lock successful."""
		return self._oleobj_.InvokeTypes(2, LCID, 1, (11, 0), (),)

	def UnLockObject(self):
		"""Unlocks the object."""
		return self._oleobj_.InvokeTypes(3, LCID, 1, (24, 0), (),)

	_prop_map_get_ = {
		"Actions": (11, 2, (9, 0), (), "Actions", None),
		"ExtendedUDFSupport": (14, 2, (3, 0), (), "ExtendedUDFSupport", None),
		"Fields": (6, 2, (9, 0), (), "Fields", None),
		"IsLocked": (1, 2, (11, 0), (), "IsLocked", None),
		"Lists": (7, 2, (9, 0), (), "Lists", None),
		"MailConditions": (13, 2, (9, 0), (), "MailConditions", None),
		"Modules": (12, 2, (9, 0), (), "Modules", None),
		"Permissions": (8, 2, (9, 0), (), "Permissions", None),
		"Users": (9, 2, (9, 0), (), "Users", None),
		"UsersGroups": (10, 2, (9, 0), (), "UsersGroups", None),
	}
	_prop_map_put_ = {
	}

class ICustomization2(DispatchBaseClass):
	"""Services to perform customization tasks, such as adding users to user groups, defining user-defined fields, and defining user access privileges."""
	CLSID = IID('{01A21FB9-74F1-4934-81F0-8C473F7A6E50}')
	coclass_clsid = IID('{E6F08C7F-A34B-43DA-A632-9410170F3D00}')

	def Commit(self):
		"""Posts all customization data changes to the project database."""
		return self._oleobj_.InvokeTypes(5, LCID, 1, (24, 0), (),)

	def Load(self):
		"""Loads all customization data from the server into the client cache. This includes actions, fields, lists, modules, permissions, users, and user groups."""
		return self._oleobj_.InvokeTypes(4, LCID, 1, (24, 0), (),)

	def LockObject(self):
		"""Locks the object. Returns True if lock successful."""
		return self._oleobj_.InvokeTypes(2, LCID, 1, (11, 0), (),)

	def Rollback(self):
		"""Discards unsaved changes to properties and child objects."""
		return self._oleobj_.InvokeTypes(16, LCID, 1, (24, 0), (),)

	def UnLockObject(self):
		"""Unlocks the object."""
		return self._oleobj_.InvokeTypes(3, LCID, 1, (24, 0), (),)

	_prop_map_get_ = {
		"Actions": (11, 2, (9, 0), (), "Actions", None),
		"ExtendedUDFSupport": (14, 2, (3, 0), (), "ExtendedUDFSupport", None),
		"Fields": (6, 2, (9, 0), (), "Fields", None),
		"IsChanged": (15, 2, (11, 0), (), "IsChanged", None),
		"IsLocked": (1, 2, (11, 0), (), "IsLocked", None),
		"Lists": (7, 2, (9, 0), (), "Lists", None),
		"MailConditions": (13, 2, (9, 0), (), "MailConditions", None),
		"Modules": (12, 2, (9, 0), (), "Modules", None),
		"Permissions": (8, 2, (9, 0), (), "Permissions", None),
		"Users": (9, 2, (9, 0), (), "Users", None),
		"UsersGroups": (10, 2, (9, 0), (), "UsersGroups", None),
	}
	_prop_map_put_ = {
	}

class ICustomizationAction(DispatchBaseClass):
	"""Represents a type of user action such as adding or deleting a bug."""
	CLSID = IID('{217D92D5-CD8A-4ADA-8ECC-9D13C224DCA8}')
	coclass_clsid = IID('{2BE06C1F-06AF-4EA7-B3D2-B5CAFD13AE88}')

	def AddGroup(self, Group=defaultNamedNotOptArg):
		"""Adds a user group to the list of user groups for which the current action is allowed."""
		return self._oleobj_.InvokeTypes(3, LCID, 1, (24, 0), ((12, 1),),Group
			)

	# The method IsGroupPermited is actually a property, but must be used as a method to correctly pass the arguments
	def IsGroupPermited(self, Group=defaultNamedNotOptArg):
		"""Checks if the group specified by the Group parameter has permission to perform the current action."""
		return self._oleobj_.InvokeTypes(7, LCID, 2, (11, 0), ((12, 1),),Group
			)

	# The method IsOwnerGroup is actually a property, but must be used as a method to correctly pass the arguments
	def IsOwnerGroup(self, Group=defaultNamedNotOptArg):
		"""Checks if the user group specified by the Group parameter is the owner of the current action."""
		return self._oleobj_.InvokeTypes(8, LCID, 2, (11, 0), ((12, 1),),Group
			)

	# The method IsUserPermited is actually a property, but must be used as a method to correctly pass the arguments
	def IsUserPermited(self, User=defaultNamedNotOptArg):
		"""Checks if the user specified by the User parameter is permitted to perform the current action."""
		return self._oleobj_.InvokeTypes(9, LCID, 2, (11, 0), ((12, 1),),User
			)

	def RemoveGroup(self, Group=defaultNamedNotOptArg):
		"""Removes the specified user group from the list of groups for which the current action is allowed."""
		return self._oleobj_.InvokeTypes(4, LCID, 1, (24, 0), ((12, 1),),Group
			)

	_prop_map_get_ = {
		"AuditAction": (10, 2, (11, 0), (), "AuditAction", None),
		# Method 'Groups' returns object of type 'IList'
		"Groups": (2, 2, (9, 0), (), "Groups", '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}'),
		"Name": (1, 2, (8, 0), (), "Name", None),
		"OwnerGroup": (5, 2, (9, 0), (), "OwnerGroup", None),
		"Updated": (6, 2, (11, 0), (), "Updated", None),
	}
	_prop_map_put_ = {
		"AuditAction": ((10, LCID, 4, 0),()),
		"OwnerGroup": ((5, LCID, 4, 0),()),
		"Updated": ((6, LCID, 4, 0),()),
	}

class ICustomizationActions(DispatchBaseClass):
	"""The collection of all CustomizationAction objects."""
	CLSID = IID('{E746670E-69C7-4DC1-93BC-9B4662B6015D}')
	coclass_clsid = IID('{A74CE8AE-2AA0-47F4-A85E-A6E96DA8EA1E}')

	# The method Action is actually a property, but must be used as a method to correctly pass the arguments
	def Action(self, Name=defaultNamedNotOptArg):
		"""The CustomizationAction object representing the action specified by Name."""
		ret = self._oleobj_.InvokeTypes(2, LCID, 2, (9, 0), ((8, 1),),Name
			)
		if ret is not None:
			ret = Dispatch(ret, u'Action', None)
		return ret

	_prop_map_get_ = {
		# Method 'Actions' returns object of type 'IList'
		"Actions": (1, 2, (9, 0), (), "Actions", '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}'),
	}
	_prop_map_put_ = {
	}

class ICustomizationField(DispatchBaseClass):
	"""Represents a user-defined field."""
	CLSID = IID('{E34F74EC-DC52-42D8-A7E4-B4F06A56CF40}')
	coclass_clsid = None

	_prop_map_get_ = {
		"ColumnName": (19, 2, (8, 0), (), "ColumnName", None),
		"ColumnType": (20, 2, (8, 0), (), "ColumnType", None),
		"DefaultValue": (30, 2, (8, 0), (), "DefaultValue", None),
		"EditMask": (21, 2, (8, 0), (), "EditMask", None),
		"EditStyle": (3, 2, (8, 0), (), "EditStyle", None),
		"FieldSize": (18, 2, (3, 0), (), "FieldSize", None),
		"GrantModifyMask": (26, 2, (3, 0), (), "GrantModifyMask", None),
		"IsActive": (8, 2, (11, 0), (), "IsActive", None),
		"IsByCode": (13, 2, (11, 0), (), "IsByCode", None),
		"IsCanFilter": (5, 2, (11, 0), (), "IsCanFilter", None),
		"IsCustomizable": (17, 2, (11, 0), (), "IsCustomizable", None),
		"IsEdit": (9, 2, (11, 0), (), "IsEdit", None),
		"IsHistory": (10, 2, (11, 0), (), "IsHistory", None),
		"IsKeepValue": (16, 2, (11, 0), (), "IsKeepValue", None),
		"IsKey": (6, 2, (11, 0), (), "IsKey", None),
		"IsMail": (11, 2, (11, 0), (), "IsMail", None),
		"IsRequired": (14, 2, (11, 0), (), "IsRequired", None),
		"IsSystem": (4, 2, (11, 0), (), "IsSystem", None),
		"IsToSum": (31, 2, (11, 0), (), "IsToSum", None),
		"IsTransitionLogic": (29, 2, (11, 0), (), "IsTransitionLogic", None),
		"IsVerify": (12, 2, (11, 0), (), "IsVerify", None),
		"IsVisibleInNewBug": (28, 2, (3, 0), (), "IsVisibleInNewBug", None),
		"KeyOrder": (7, 2, (3, 0), (), "KeyOrder", None),
		"List": (22, 2, (9, 0), (), "List", None),
		"NewCreated": (34, 2, (11, 0), (), "NewCreated", None),
		"OwnerSensibleMask": (27, 2, (3, 0), (), "OwnerSensibleMask", None),
		"RootId": (23, 2, (12, 0), (), "RootId", None),
		"TableName": (2, 2, (8, 0), (), "TableName", None),
		"Type": (24, 2, (3, 0), (), "Type", None),
		"Updated": (25, 2, (11, 0), (), "Updated", None),
		"UserColumnType": (15, 2, (8, 0), (), "UserColumnType", None),
		"UserLabel": (1, 2, (8, 0), (), "UserLabel", None),
		"VersionControl": (33, 2, (11, 0), (), "VersionControl", None),
		"VisibleForGroups": (32, 2, (3, 0), (), "VisibleForGroups", None),
	}
	_prop_map_put_ = {
		"ColumnType": ((20, LCID, 4, 0),()),
		"DefaultValue": ((30, LCID, 4, 0),()),
		"EditMask": ((21, LCID, 4, 0),()),
		"EditStyle": ((3, LCID, 4, 0),()),
		"FieldSize": ((18, LCID, 4, 0),()),
		"GrantModifyMask": ((26, LCID, 4, 0),()),
		"IsActive": ((8, LCID, 4, 0),()),
		"IsByCode": ((13, LCID, 4, 0),()),
		"IsCanFilter": ((5, LCID, 4, 0),()),
		"IsCustomizable": ((17, LCID, 4, 0),()),
		"IsEdit": ((9, LCID, 4, 0),()),
		"IsHistory": ((10, LCID, 4, 0),()),
		"IsKeepValue": ((16, LCID, 4, 0),()),
		"IsKey": ((6, LCID, 4, 0),()),
		"IsMail": ((11, LCID, 4, 0),()),
		"IsRequired": ((14, LCID, 4, 0),()),
		"IsSystem": ((4, LCID, 4, 0),()),
		"IsToSum": ((31, LCID, 4, 0),()),
		"IsTransitionLogic": ((29, LCID, 4, 0),()),
		"IsVerify": ((12, LCID, 4, 0),()),
		"IsVisibleInNewBug": ((28, LCID, 4, 0),()),
		"KeyOrder": ((7, LCID, 4, 0),()),
		"List": ((22, LCID, 4, 0),()),
		"NewCreated": ((34, LCID, 4, 0),()),
		"OwnerSensibleMask": ((27, LCID, 4, 0),()),
		"RootId": ((23, LCID, 4, 0),()),
		"Type": ((24, LCID, 4, 0),()),
		"Updated": ((25, LCID, 4, 0),()),
		"UserColumnType": ((15, LCID, 4, 0),()),
		"UserLabel": ((1, LCID, 4, 0),()),
		"VersionControl": ((33, LCID, 4, 0),()),
		"VisibleForGroups": ((32, LCID, 4, 0),()),
	}

class ICustomizationField2(DispatchBaseClass):
	"""Represents a user-defined field."""
	CLSID = IID('{2FA7E440-704F-4EFE-B500-EB93E7AFD294}')
	coclass_clsid = IID('{48BB9422-E786-463A-8617-25ADA366FAA0}')

	# The method GrantModifyForGroup is actually a property, but must be used as a method to correctly pass the arguments
	def GrantModifyForGroup(self, Group=defaultNamedNotOptArg):
		"""Indicates whether members of the group can change the value of this field."""
		return self._oleobj_.InvokeTypes(35, LCID, 2, (11, 0), ((12, 1),),Group
			)

	# The method OwnerSensibleForGroup is actually a property, but must be used as a method to correctly pass the arguments
	def OwnerSensibleForGroup(self, Group=defaultNamedNotOptArg):
		"""Indicates whether the field can be modified by the owner if the owner belongs to the specified group."""
		return self._oleobj_.InvokeTypes(37, LCID, 2, (11, 0), ((12, 1),),Group
			)

	# The method SetGrantModifyForGroup is actually a property, but must be used as a method to correctly pass the arguments
	def SetGrantModifyForGroup(self, Group=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""Indicates whether members of the group can change the value of this field."""
		return self._oleobj_.InvokeTypes(35, LCID, 4, (24, 0), ((12, 1), (11, 1)),Group
			, arg1)

	# The method SetOwnerSensibleForGroup is actually a property, but must be used as a method to correctly pass the arguments
	def SetOwnerSensibleForGroup(self, Group=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""Indicates whether the field can be modified by the owner if the owner belongs to the specified group."""
		return self._oleobj_.InvokeTypes(37, LCID, 4, (24, 0), ((12, 1), (11, 1)),Group
			, arg1)

	# The method SetVisibleForGroup is actually a property, but must be used as a method to correctly pass the arguments
	def SetVisibleForGroup(self, Group=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""Indicates whether the field is visible in the New Defect dialog to the specified group."""
		return self._oleobj_.InvokeTypes(41, LCID, 4, (24, 0), ((12, 1), (11, 1)),Group
			, arg1)

	# The method SetVisibleInNewBugForGroup is actually a property, but must be used as a method to correctly pass the arguments
	def SetVisibleInNewBugForGroup(self, Group=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""Indicates whether the field is visible to the specified group in the new defect dialog."""
		return self._oleobj_.InvokeTypes(39, LCID, 4, (24, 0), ((12, 1), (11, 1)),Group
			, arg1)

	# The method VisibleForGroup is actually a property, but must be used as a method to correctly pass the arguments
	def VisibleForGroup(self, Group=defaultNamedNotOptArg):
		"""Indicates whether the field is visible in the New Defect dialog to the specified group."""
		return self._oleobj_.InvokeTypes(41, LCID, 2, (11, 0), ((12, 1),),Group
			)

	# The method VisibleInNewBugForGroup is actually a property, but must be used as a method to correctly pass the arguments
	def VisibleInNewBugForGroup(self, Group=defaultNamedNotOptArg):
		"""Indicates whether the field is visible to the specified group in the new defect dialog."""
		return self._oleobj_.InvokeTypes(39, LCID, 2, (11, 0), ((12, 1),),Group
			)

	_prop_map_get_ = {
		"AuditUpdate": (44, 2, (11, 0), (), "AuditUpdate", None),
		# Method 'AuthorizedModifyForGroups' returns object of type 'IList'
		"AuthorizedModifyForGroups": (36, 2, (9, 0), (), "AuthorizedModifyForGroups", '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}'),
		# Method 'AuthorizedOwnerSensibleForGroups' returns object of type 'IList'
		"AuthorizedOwnerSensibleForGroups": (38, 2, (9, 0), (), "AuthorizedOwnerSensibleForGroups", '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}'),
		# Method 'AuthorizedVisibleForGroups' returns object of type 'IList'
		"AuthorizedVisibleForGroups": (42, 2, (9, 0), (), "AuthorizedVisibleForGroups", '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}'),
		# Method 'AuthorizedVisibleInNewBugForGroups' returns object of type 'IList'
		"AuthorizedVisibleInNewBugForGroups": (40, 2, (9, 0), (), "AuthorizedVisibleInNewBugForGroups", '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}'),
		"CanMakeMultiValue": (49, 2, (11, 0), (), "CanMakeMultiValue", None),
		"CanMakeSearchable": (46, 2, (11, 0), (), "CanMakeSearchable", None),
		"ColumnName": (19, 2, (8, 0), (), "ColumnName", None),
		"ColumnType": (20, 2, (8, 0), (), "ColumnType", None),
		"DefaultValue": (30, 2, (8, 0), (), "DefaultValue", None),
		"EditMask": (21, 2, (8, 0), (), "EditMask", None),
		"EditStyle": (3, 2, (8, 0), (), "EditStyle", None),
		"FieldSize": (18, 2, (3, 0), (), "FieldSize", None),
		"GrantModifyMask": (26, 2, (3, 0), (), "GrantModifyMask", None),
		"IsActive": (8, 2, (11, 0), (), "IsActive", None),
		"IsByCode": (13, 2, (11, 0), (), "IsByCode", None),
		"IsCanFilter": (5, 2, (11, 0), (), "IsCanFilter", None),
		"IsCanGroup": (43, 2, (11, 0), (), "IsCanGroup", None),
		"IsCustomizable": (17, 2, (11, 0), (), "IsCustomizable", None),
		"IsEdit": (9, 2, (11, 0), (), "IsEdit", None),
		"IsHistory": (10, 2, (11, 0), (), "IsHistory", None),
		"IsKeepValue": (16, 2, (11, 0), (), "IsKeepValue", None),
		"IsKey": (6, 2, (11, 0), (), "IsKey", None),
		"IsMail": (11, 2, (11, 0), (), "IsMail", None),
		"IsMultiValue": (48, 2, (11, 0), (), "IsMultiValue", None),
		"IsRequired": (14, 2, (11, 0), (), "IsRequired", None),
		"IsSearchable": (45, 2, (11, 0), (), "IsSearchable", None),
		"IsSystem": (4, 2, (11, 0), (), "IsSystem", None),
		"IsToSum": (31, 2, (11, 0), (), "IsToSum", None),
		"IsTransitionLogic": (29, 2, (11, 0), (), "IsTransitionLogic", None),
		"IsVerify": (12, 2, (11, 0), (), "IsVerify", None),
		"IsVirtual": (47, 2, (11, 0), (), "IsVirtual", None),
		"IsVisibleInNewBug": (28, 2, (3, 0), (), "IsVisibleInNewBug", None),
		"KeyOrder": (7, 2, (3, 0), (), "KeyOrder", None),
		"List": (22, 2, (9, 0), (), "List", None),
		"NewCreated": (34, 2, (11, 0), (), "NewCreated", None),
		"OwnerSensibleMask": (27, 2, (3, 0), (), "OwnerSensibleMask", None),
		"RootId": (23, 2, (12, 0), (), "RootId", None),
		"TableName": (2, 2, (8, 0), (), "TableName", None),
		"Type": (24, 2, (3, 0), (), "Type", None),
		"Updated": (25, 2, (11, 0), (), "Updated", None),
		"UserColumnType": (15, 2, (8, 0), (), "UserColumnType", None),
		"UserLabel": (1, 2, (8, 0), (), "UserLabel", None),
		"VersionControl": (33, 2, (11, 0), (), "VersionControl", None),
		"VisibleForGroups": (32, 2, (3, 0), (), "VisibleForGroups", None),
	}
	_prop_map_put_ = {
		"AuditUpdate": ((44, LCID, 4, 0),()),
		"ColumnType": ((20, LCID, 4, 0),()),
		"DefaultValue": ((30, LCID, 4, 0),()),
		"EditMask": ((21, LCID, 4, 0),()),
		"EditStyle": ((3, LCID, 4, 0),()),
		"FieldSize": ((18, LCID, 4, 0),()),
		"GrantModifyMask": ((26, LCID, 4, 0),()),
		"IsActive": ((8, LCID, 4, 0),()),
		"IsByCode": ((13, LCID, 4, 0),()),
		"IsCanFilter": ((5, LCID, 4, 0),()),
		"IsCanGroup": ((43, LCID, 4, 0),()),
		"IsCustomizable": ((17, LCID, 4, 0),()),
		"IsEdit": ((9, LCID, 4, 0),()),
		"IsHistory": ((10, LCID, 4, 0),()),
		"IsKeepValue": ((16, LCID, 4, 0),()),
		"IsKey": ((6, LCID, 4, 0),()),
		"IsMail": ((11, LCID, 4, 0),()),
		"IsMultiValue": ((48, LCID, 4, 0),()),
		"IsRequired": ((14, LCID, 4, 0),()),
		"IsSearchable": ((45, LCID, 4, 0),()),
		"IsSystem": ((4, LCID, 4, 0),()),
		"IsToSum": ((31, LCID, 4, 0),()),
		"IsTransitionLogic": ((29, LCID, 4, 0),()),
		"IsVerify": ((12, LCID, 4, 0),()),
		"IsVisibleInNewBug": ((28, LCID, 4, 0),()),
		"KeyOrder": ((7, LCID, 4, 0),()),
		"List": ((22, LCID, 4, 0),()),
		"NewCreated": ((34, LCID, 4, 0),()),
		"OwnerSensibleMask": ((27, LCID, 4, 0),()),
		"RootId": ((23, LCID, 4, 0),()),
		"Type": ((24, LCID, 4, 0),()),
		"Updated": ((25, LCID, 4, 0),()),
		"UserColumnType": ((15, LCID, 4, 0),()),
		"UserLabel": ((1, LCID, 4, 0),()),
		"VersionControl": ((33, LCID, 4, 0),()),
		"VisibleForGroups": ((32, LCID, 4, 0),()),
	}

class ICustomizationFields(DispatchBaseClass):
	"""Services for managing the collection of all CustomizationField objects in the project."""
	CLSID = IID('{6388B1FB-C735-4D24-B1E4-97221B302461}')
	coclass_clsid = IID('{FAE217FE-0F81-471F-B4A8-73B045C1710C}')

	def AddActiveField(self, TableName=defaultNamedNotOptArg):
		"""Finds the first free, inactive field in the specified table, signs the field as active, and returns the CustomizationFields object representing the field."""
		ret = self._oleobj_.InvokeTypes(3, LCID, 1, (9, 0), ((8, 1),),TableName
			)
		if ret is not None:
			ret = Dispatch(ret, u'AddActiveField', None)
		return ret

	def AddActiveMemoField(self, TableName=defaultNamedNotOptArg):
		"""Creates a new memo field in a table."""
		ret = self._oleobj_.InvokeTypes(5, LCID, 1, (9, 0), ((8, 1),),TableName
			)
		if ret is not None:
			ret = Dispatch(ret, u'AddActiveMemoField', None)
		return ret

	# The method Field is actually a property, but must be used as a method to correctly pass the arguments
	def Field(self, TableName=defaultNamedNotOptArg, FieldName=defaultNamedNotOptArg):
		"""The customization field object for the specified database table and field."""
		ret = self._oleobj_.InvokeTypes(1, LCID, 2, (9, 0), ((8, 1), (8, 1)),TableName
			, FieldName)
		if ret is not None:
			ret = Dispatch(ret, u'Field', None)
		return ret

	# The method FieldExists is actually a property, but must be used as a method to correctly pass the arguments
	def FieldExists(self, TableName=defaultNamedNotOptArg, FieldName=defaultNamedNotOptArg):
		"""Checks if the specified database field exists."""
		return self._oleobj_.InvokeTypes(4, LCID, 2, (11, 0), ((8, 1), (8, 1)),TableName
			, FieldName)

	# Result is of type IList
	# The method GetFields is actually a property, but must be used as a method to correctly pass the arguments
	def GetFields(self, TableName=u''):
		"""A list of all fields in the specified table."""
		return self._ApplyTypes_(2, 2, (9, 32), ((8, 49),), u'GetFields', '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}',TableName
			)

	_prop_map_get_ = {
		# Method 'Fields' returns object of type 'IList'
		"Fields": (2, 2, (9, 32), ((8, 49),), "Fields", '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}'),
	}
	_prop_map_put_ = {
	}

class ICustomizationList(DispatchBaseClass):
	"""Represents a user-defined list associated with a field."""
	CLSID = IID('{1E1C07CA-7339-4741-A41D-DEC5A065B1F1}')
	coclass_clsid = IID('{1E3AB54D-4D98-4B9F-9A0B-E6427054FED6}')

	def Find(self, Val=defaultNamedNotOptArg):
		"""Finds a CustomizationListNode of the current list by the node name or ID."""
		ret = self._oleobj_.InvokeTypes(3, LCID, 1, (9, 0), ((12, 1),),Val
			)
		if ret is not None:
			ret = Dispatch(ret, u'Find', None)
		return ret

	_prop_map_get_ = {
		"Deleted": (4, 2, (11, 0), (), "Deleted", None),
		"Name": (2, 2, (8, 0), (), "Name", None),
		"RootNode": (1, 2, (9, 0), (), "RootNode", None),
	}
	_prop_map_put_ = {
		"Deleted": ((4, LCID, 4, 0),()),
	}

class ICustomizationListNode(DispatchBaseClass):
	"""Represents a node in a list."""
	CLSID = IID('{18C04A97-2141-4852-8E02-0E79D3CAEAD3}')
	coclass_clsid = IID('{80A02CF4-53FB-499C-9549-2EE19815179A}')

	def AddChild(self, Node=defaultNamedNotOptArg):
		"""Adds a new sub-node to the current node."""
		ret = self._oleobj_.InvokeTypes(1, LCID, 1, (9, 0), ((12, 1),),Node
			)
		if ret is not None:
			ret = Dispatch(ret, u'AddChild', None)
		return ret

	# The method Child is actually a property, but must be used as a method to correctly pass the arguments
	def Child(self, NodeName=defaultNamedNotOptArg):
		"""Gets the specified sub-node."""
		ret = self._oleobj_.InvokeTypes(3, LCID, 2, (9, 0), ((8, 1),),NodeName
			)
		if ret is not None:
			ret = Dispatch(ret, u'Child', None)
		return ret

	def RemoveChild(self, Node=defaultNamedNotOptArg):
		"""Removes the specified sub-node from the current node."""
		return self._oleobj_.InvokeTypes(2, LCID, 1, (24, 0), ((12, 1),),Node
			)

	_prop_map_get_ = {
		"CanAddChild": (8, 2, (11, 0), (), "CanAddChild", None),
		# Method 'Children' returns object of type 'IList'
		"Children": (4, 2, (9, 0), (), "Children", '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}'),
		"ChildrenCount": (11, 2, (3, 0), (), "ChildrenCount", None),
		"Deleted": (13, 2, (11, 0), (), "Deleted", None),
		"Father": (7, 2, (9, 0), (), "Father", None),
		"ID": (6, 2, (3, 0), (), "ID", None),
		"List": (10, 2, (9, 0), (), "List", None),
		"Name": (5, 2, (8, 0), (), "Name", None),
		"Order": (14, 2, (3, 0), (), "Order", None),
		"ReadOnly": (9, 2, (11, 0), (), "ReadOnly", None),
		"Updated": (12, 2, (11, 0), (), "Updated", None),
	}
	_prop_map_put_ = {
		"Deleted": ((13, LCID, 4, 0),()),
		"Father": ((7, LCID, 4, 0),()),
		"ID": ((6, LCID, 4, 0),()),
		"Name": ((5, LCID, 4, 0),()),
		"Order": ((14, LCID, 4, 0),()),
		"Updated": ((12, LCID, 4, 0),()),
	}

class ICustomizationLists(DispatchBaseClass):
	"""A collection of all CustomizationList objects in the project."""
	CLSID = IID('{4EA26B82-65DC-4315-AD7F-D963A1C4EB2F}')
	coclass_clsid = IID('{6B8183C1-5676-4315-B202-2B78A15E1D6C}')

	def AddList(self, Name=defaultNamedNotOptArg):
		"""Adds a new CustomizationList object."""
		ret = self._oleobj_.InvokeTypes(1, LCID, 1, (9, 0), ((8, 1),),Name
			)
		if ret is not None:
			ret = Dispatch(ret, u'AddList', None)
		return ret

	# The method IsListExist is actually a property, but must be used as a method to correctly pass the arguments
	def IsListExist(self, ListName=defaultNamedNotOptArg):
		"""Checks if the specified customization list exists."""
		return self._oleobj_.InvokeTypes(6, LCID, 2, (11, 0), ((8, 1),),ListName
			)

	# The method List is actually a property, but must be used as a method to correctly pass the arguments
	def List(self, Param=defaultNamedNotOptArg):
		"""Gets the CustomizationList specified by name or ID."""
		ret = self._oleobj_.InvokeTypes(3, LCID, 2, (9, 0), ((12, 1),),Param
			)
		if ret is not None:
			ret = Dispatch(ret, u'List', None)
		return ret

	# The method ListByCount is actually a property, but must be used as a method to correctly pass the arguments
	def ListByCount(self, Count=defaultNamedNotOptArg):
		"""Gets the CustomizationList specified by its index number."""
		ret = self._oleobj_.InvokeTypes(5, LCID, 2, (9, 0), ((3, 1),),Count
			)
		if ret is not None:
			ret = Dispatch(ret, u'ListByCount', None)
		return ret

	def RemoveList(self, Name=defaultNamedNotOptArg):
		"""Removes a CustomizationList object."""
		return self._oleobj_.InvokeTypes(2, LCID, 1, (24, 0), ((8, 1),),Name
			)

	_prop_map_get_ = {
		"Count": (4, 2, (3, 0), (), "Count", None),
	}
	_prop_map_put_ = {
	}
	#This class has Count() property - allow len(ob) to provide this
	def __len__(self):
		return self._ApplyTypes_(*(4, 2, (3, 0), (), "Count", None))
	#This class has a __len__ - this is needed so 'if object:' always returns TRUE.
	def __nonzero__(self):
		return True

class ICustomizationMailCondition(DispatchBaseClass):
	"""Configuration of custom automatic mail notifications."""
	CLSID = IID('{509C8491-47BF-454B-B899-852A55A17F46}')
	coclass_clsid = IID('{78D71CB3-1DC2-49A6-BF0C-6D36AFD9E93E}')

	_prop_map_get_ = {
		"ConditionText": (2, 2, (8, 0), (), "ConditionText", None),
		"ConditionType": (5, 2, (3, 0), (), "ConditionType", None),
		"Deleted": (4, 2, (11, 0), (), "Deleted", None),
		"Name": (1, 2, (8, 0), (), "Name", None),
		"Updated": (3, 2, (11, 0), (), "Updated", None),
	}
	_prop_map_put_ = {
		"ConditionText": ((2, LCID, 4, 0),()),
		"Deleted": ((4, LCID, 4, 0),()),
		"Updated": ((3, LCID, 4, 0),()),
	}

class ICustomizationMailConditions(DispatchBaseClass):
	"""The collection of configurations for custom automatic mail notifications."""
	CLSID = IID('{2D05E2CD-60E9-4C56-A05D-D25872A00335}')
	coclass_clsid = IID('{93FB7AD9-0302-4EA3-8BE5-3437F4F52906}')

	def AddCondition(self, Name=defaultNamedNotOptArg, ConditionType=defaultNamedNotOptArg, ConditionText=defaultNamedNotOptArg):
		"""Creates and returns a new CustomizationMailCondition."""
		ret = self._oleobj_.InvokeTypes(3, LCID, 1, (9, 0), ((8, 1), (3, 1), (8, 1)),Name
			, ConditionType, ConditionText)
		if ret is not None:
			ret = Dispatch(ret, u'AddCondition', None)
		return ret

	# The method Condition is actually a property, but must be used as a method to correctly pass the arguments
	def Condition(self, Name=defaultNamedNotOptArg, ConditionType=defaultNamedNotOptArg):
		"""Gets the CustomizationMailCondition specified by the name and type."""
		ret = self._oleobj_.InvokeTypes(1, LCID, 2, (9, 0), ((8, 1), (3, 1)),Name
			, ConditionType)
		if ret is not None:
			ret = Dispatch(ret, u'Condition', None)
		return ret

	# The method ConditionExist is actually a property, but must be used as a method to correctly pass the arguments
	def ConditionExist(self, Name=defaultNamedNotOptArg, ConditionType=defaultNamedNotOptArg):
		"""Checks if the specified mail customization condition exists."""
		return self._oleobj_.InvokeTypes(5, LCID, 2, (11, 0), ((8, 1), (3, 1)),Name
			, ConditionType)

	def RemoveCondition(self, Name=defaultNamedNotOptArg, ConditionType=defaultNamedNotOptArg):
		"""Deletes the specified CustomizationMailCondition."""
		return self._oleobj_.InvokeTypes(4, LCID, 1, (24, 0), ((8, 1), (3, 1)),Name
			, ConditionType)

	_prop_map_get_ = {
		# Method 'Conditions' returns object of type 'IList'
		"Conditions": (2, 2, (9, 0), (), "Conditions", '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}'),
	}
	_prop_map_put_ = {
	}

class ICustomizationModules(DispatchBaseClass):
	"""Services for managing the customization modules."""
	CLSID = IID('{2EEC167C-FBBF-4A2F-8DE6-DE8A05EDDB50}')
	coclass_clsid = None

	# The method Description is actually a property, but must be used as a method to correctly pass the arguments
	def Description(self, ModuleID=defaultNamedNotOptArg):
		"""The Module Description"""
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(3, LCID, 2, (8, 0), ((3, 1),),ModuleID
			)

	# The method GUID is actually a property, but must be used as a method to correctly pass the arguments
	def GUID(self, ModuleID=defaultNamedNotOptArg):
		"""The Module's globally unique ID."""
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(2, LCID, 2, (8, 0), ((3, 1),),ModuleID
			)

	# The method Name is actually a property, but must be used as a method to correctly pass the arguments
	def Name(self, ModuleID=defaultNamedNotOptArg):
		"""The Module Name."""
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(1, LCID, 2, (8, 0), ((3, 1),),ModuleID
			)

	# The method SetDescription is actually a property, but must be used as a method to correctly pass the arguments
	def SetDescription(self, ModuleID=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The Module Description"""
		return self._oleobj_.InvokeTypes(3, LCID, 4, (24, 0), ((3, 1), (8, 1)),ModuleID
			, arg1)

	# The method SetGUID is actually a property, but must be used as a method to correctly pass the arguments
	def SetGUID(self, ModuleID=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The Module's globally unique ID."""
		return self._oleobj_.InvokeTypes(2, LCID, 4, (24, 0), ((3, 1), (8, 1)),ModuleID
			, arg1)

	# The method SetName is actually a property, but must be used as a method to correctly pass the arguments
	def SetName(self, ModuleID=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The Module Name."""
		return self._oleobj_.InvokeTypes(1, LCID, 4, (24, 0), ((3, 1), (8, 1)),ModuleID
			, arg1)

	# The method SetVisible is actually a property, but must be used as a method to correctly pass the arguments
	def SetVisible(self, ModuleID=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""A bit-mask that indicates for which groups the module is visible in the user interface."""
		return self._oleobj_.InvokeTypes(4, LCID, 4, (24, 0), ((3, 1), (3, 1)),ModuleID
			, arg1)

	# The method Visible is actually a property, but must be used as a method to correctly pass the arguments
	def Visible(self, ModuleID=defaultNamedNotOptArg):
		"""A bit-mask that indicates for which groups the module is visible in the user interface."""
		return self._oleobj_.InvokeTypes(4, LCID, 2, (3, 0), ((3, 1),),ModuleID
			)

	_prop_map_get_ = {
	}
	_prop_map_put_ = {
	}

class ICustomizationModules2(DispatchBaseClass):
	"""ICustomizationModules2 Interface"""
	CLSID = IID('{3A2F36D9-0CEF-4864-A067-7246D6615D9E}')
	coclass_clsid = IID('{78B7C423-5E9B-4225-811A-95ABE879B461}')

	# The method Description is actually a property, but must be used as a method to correctly pass the arguments
	def Description(self, ModuleID=defaultNamedNotOptArg):
		"""The Module Description"""
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(3, LCID, 2, (8, 0), ((3, 1),),ModuleID
			)

	# The method GUID is actually a property, but must be used as a method to correctly pass the arguments
	def GUID(self, ModuleID=defaultNamedNotOptArg):
		"""The Module's globally unique ID."""
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(2, LCID, 2, (8, 0), ((3, 1),),ModuleID
			)

	# The method IsVisibleForGroup is actually a property, but must be used as a method to correctly pass the arguments
	def IsVisibleForGroup(self, ModuleID=defaultNamedNotOptArg, Group=defaultNamedNotOptArg):
		"""Indicates if the specified module is visible in the UI to the specified user group."""
		return self._oleobj_.InvokeTypes(5, LCID, 2, (11, 0), ((3, 1), (12, 1)),ModuleID
			, Group)

	# The method Name is actually a property, but must be used as a method to correctly pass the arguments
	def Name(self, ModuleID=defaultNamedNotOptArg):
		"""The Module Name."""
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(1, LCID, 2, (8, 0), ((3, 1),),ModuleID
			)

	# The method SetDescription is actually a property, but must be used as a method to correctly pass the arguments
	def SetDescription(self, ModuleID=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The Module Description"""
		return self._oleobj_.InvokeTypes(3, LCID, 4, (24, 0), ((3, 1), (8, 1)),ModuleID
			, arg1)

	# The method SetGUID is actually a property, but must be used as a method to correctly pass the arguments
	def SetGUID(self, ModuleID=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The Module's globally unique ID."""
		return self._oleobj_.InvokeTypes(2, LCID, 4, (24, 0), ((3, 1), (8, 1)),ModuleID
			, arg1)

	# The method SetIsVisibleForGroup is actually a property, but must be used as a method to correctly pass the arguments
	def SetIsVisibleForGroup(self, ModuleID=defaultNamedNotOptArg, Group=defaultNamedNotOptArg, arg2=defaultUnnamedArg):
		"""Indicates if the specified module is visible in the UI to the specified user group."""
		return self._oleobj_.InvokeTypes(5, LCID, 4, (24, 0), ((3, 1), (12, 1), (11, 1)),ModuleID
			, Group, arg2)

	# The method SetName is actually a property, but must be used as a method to correctly pass the arguments
	def SetName(self, ModuleID=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The Module Name."""
		return self._oleobj_.InvokeTypes(1, LCID, 4, (24, 0), ((3, 1), (8, 1)),ModuleID
			, arg1)

	# The method SetVisible is actually a property, but must be used as a method to correctly pass the arguments
	def SetVisible(self, ModuleID=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""A bit-mask that indicates for which groups the module is visible in the user interface."""
		return self._oleobj_.InvokeTypes(4, LCID, 4, (24, 0), ((3, 1), (3, 1)),ModuleID
			, arg1)

	# The method Visible is actually a property, but must be used as a method to correctly pass the arguments
	def Visible(self, ModuleID=defaultNamedNotOptArg):
		"""A bit-mask that indicates for which groups the module is visible in the user interface."""
		return self._oleobj_.InvokeTypes(4, LCID, 2, (3, 0), ((3, 1),),ModuleID
			)

	# Result is of type IList
	# The method VisibleForGroups is actually a property, but must be used as a method to correctly pass the arguments
	def VisibleForGroups(self, ModuleID=defaultNamedNotOptArg):
		"""The list of the user groups to which the specified module is visible in the UI."""
		ret = self._oleobj_.InvokeTypes(6, LCID, 2, (9, 0), ((3, 1),),ModuleID
			)
		if ret is not None:
			ret = Dispatch(ret, u'VisibleForGroups', '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')
		return ret

	_prop_map_get_ = {
	}
	_prop_map_put_ = {
	}

class ICustomizationPermissions(DispatchBaseClass):
	"""Properties that define the ability of user groups to add, remove, and modify entities."""
	CLSID = IID('{7EE7D348-AED6-4B3C-8FC7-5D9D8EA8E4C9}')
	coclass_clsid = IID('{01C802C0-3021-4FE3-91AB-8C2AD5116BB4}')

	# The method AuditAddItem is actually a property, but must be used as a method to correctly pass the arguments
	def AuditAddItem(self, EntityName=defaultNamedNotOptArg):
		"""property AuditAddItem"""
		return self._oleobj_.InvokeTypes(10, LCID, 2, (11, 0), ((8, 1),),EntityName
			)

	# The method AuditRemoveItem is actually a property, but must be used as a method to correctly pass the arguments
	def AuditRemoveItem(self, EntityName=defaultNamedNotOptArg):
		"""property AuditRemoveItem"""
		return self._oleobj_.InvokeTypes(11, LCID, 2, (11, 0), ((8, 1),),EntityName
			)

	# The method CanAddItem is actually a property, but must be used as a method to correctly pass the arguments
	def CanAddItem(self, EntityName=defaultNamedNotOptArg, Group=defaultNamedNotOptArg):
		"""Indicates if members of the specified group can add the specified type of entity."""
		return self._oleobj_.InvokeTypes(1, LCID, 2, (11, 0), ((8, 1), (12, 1)),EntityName
			, Group)

	# The method CanAllowAttachment is actually a property, but must be used as a method to correctly pass the arguments
	def CanAllowAttachment(self, EntityName=defaultNamedNotOptArg, Group=defaultNamedNotOptArg):
		"""Indicates if members of the specified group can handle the specified type of entity attachments."""
		return self._oleobj_.InvokeTypes(5, LCID, 2, (3, 0), ((8, 1), (12, 1)),EntityName
			, Group)

	# The method CanModifyField is actually a property, but must be used as a method to correctly pass the arguments
	def CanModifyField(self, EntityName=defaultNamedNotOptArg, Field=defaultNamedNotOptArg, Group=defaultNamedNotOptArg):
		"""Indicates if members of the specified group can modify the specified field."""
		return self._oleobj_.InvokeTypes(3, LCID, 2, (3, 0), ((8, 1), (12, 1), (12, 1)),EntityName
			, Field, Group)

	# The method CanModifyItem is actually a property, but must be used as a method to correctly pass the arguments
	def CanModifyItem(self, EntityName=defaultNamedNotOptArg, Group=defaultNamedNotOptArg):
		"""Indicates if members of the specified group can modify the specified entity."""
		return self._oleobj_.InvokeTypes(9, LCID, 2, (11, 0), ((8, 1), (12, 1)),EntityName
			, Group)

	# The method CanRemoveItem is actually a property, but must be used as a method to correctly pass the arguments
	def CanRemoveItem(self, EntityName=defaultNamedNotOptArg, Group=defaultNamedNotOptArg):
		"""Indicates if members of the specified group can remove the specified type of entity."""
		return self._oleobj_.InvokeTypes(2, LCID, 2, (3, 0), ((8, 1), (12, 1)),EntityName
			, Group)

	# The method CanUseOwnerSensible is actually a property, but must be used as a method to correctly pass the arguments
	def CanUseOwnerSensible(self, EntityName=defaultNamedNotOptArg):
		"""Checks if the fields of the specified entity can be restricted for owner modifications only."""
		return self._oleobj_.InvokeTypes(6, LCID, 2, (11, 0), ((8, 1),),EntityName
			)

	# The method HasAttachmentField is actually a property, but must be used as a method to correctly pass the arguments
	def HasAttachmentField(self, EntityName=defaultNamedNotOptArg):
		"""Checks if the entity specified by the EntityName parameter can have attachments."""
		return self._oleobj_.InvokeTypes(7, LCID, 2, (11, 0), ((8, 1),),EntityName
			)

	# The method IsVisibleInNewBug is actually a property, but must be used as a method to correctly pass the arguments
	def IsVisibleInNewBug(self, EntityName=defaultNamedNotOptArg, Field=defaultNamedNotOptArg, Group=defaultNamedNotOptArg):
		"""Indicates if the specified Bug field is visible for the specified group in a new defect form."""
		return self._oleobj_.InvokeTypes(8, LCID, 2, (11, 0), ((8, 1), (12, 1), (12, 1)),EntityName
			, Field, Group)

	# The method SetAuditAddItem is actually a property, but must be used as a method to correctly pass the arguments
	def SetAuditAddItem(self, EntityName=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""property AuditAddItem"""
		return self._oleobj_.InvokeTypes(10, LCID, 4, (24, 0), ((8, 1), (11, 1)),EntityName
			, arg1)

	# The method SetAuditRemoveItem is actually a property, but must be used as a method to correctly pass the arguments
	def SetAuditRemoveItem(self, EntityName=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""property AuditRemoveItem"""
		return self._oleobj_.InvokeTypes(11, LCID, 4, (24, 0), ((8, 1), (11, 1)),EntityName
			, arg1)

	# The method SetCanAddItem is actually a property, but must be used as a method to correctly pass the arguments
	def SetCanAddItem(self, EntityName=defaultNamedNotOptArg, Group=defaultNamedNotOptArg, arg2=defaultUnnamedArg):
		"""Indicates if members of the specified group can add the specified type of entity."""
		return self._oleobj_.InvokeTypes(1, LCID, 4, (24, 0), ((8, 1), (12, 1), (11, 1)),EntityName
			, Group, arg2)

	# The method SetCanAllowAttachment is actually a property, but must be used as a method to correctly pass the arguments
	def SetCanAllowAttachment(self, EntityName=defaultNamedNotOptArg, Group=defaultNamedNotOptArg, arg2=defaultUnnamedArg):
		"""Indicates if members of the specified group can handle the specified type of entity attachments."""
		return self._oleobj_.InvokeTypes(5, LCID, 4, (24, 0), ((8, 1), (12, 1), (3, 1)),EntityName
			, Group, arg2)

	# The method SetCanModifyField is actually a property, but must be used as a method to correctly pass the arguments
	def SetCanModifyField(self, EntityName=defaultNamedNotOptArg, Field=defaultNamedNotOptArg, Group=defaultNamedNotOptArg, arg3=defaultUnnamedArg):
		"""Indicates if members of the specified group can modify the specified field."""
		return self._oleobj_.InvokeTypes(3, LCID, 4, (24, 0), ((8, 1), (12, 1), (12, 1), (3, 1)),EntityName
			, Field, Group, arg3)

	# The method SetCanModifyItem is actually a property, but must be used as a method to correctly pass the arguments
	def SetCanModifyItem(self, EntityName=defaultNamedNotOptArg, Group=defaultNamedNotOptArg, arg2=defaultUnnamedArg):
		"""Indicates if members of the specified group can modify the specified entity."""
		return self._oleobj_.InvokeTypes(9, LCID, 4, (24, 0), ((8, 1), (12, 1), (11, 1)),EntityName
			, Group, arg2)

	# The method SetCanRemoveItem is actually a property, but must be used as a method to correctly pass the arguments
	def SetCanRemoveItem(self, EntityName=defaultNamedNotOptArg, Group=defaultNamedNotOptArg, arg2=defaultUnnamedArg):
		"""Indicates if members of the specified group can remove the specified type of entity."""
		return self._oleobj_.InvokeTypes(2, LCID, 4, (24, 0), ((8, 1), (12, 1), (3, 1)),EntityName
			, Group, arg2)

	# The method SetIsVisibleInNewBug is actually a property, but must be used as a method to correctly pass the arguments
	def SetIsVisibleInNewBug(self, EntityName=defaultNamedNotOptArg, Field=defaultNamedNotOptArg, Group=defaultNamedNotOptArg, arg3=defaultUnnamedArg):
		"""Indicates if the specified Bug field is visible for the specified group in a new defect form."""
		return self._oleobj_.InvokeTypes(8, LCID, 4, (24, 0), ((8, 1), (12, 1), (12, 1), (11, 1)),EntityName
			, Field, Group, arg3)

	# The method TransitionRules is actually a property, but must be used as a method to correctly pass the arguments
	def TransitionRules(self, EntityName=defaultNamedNotOptArg, Field=defaultNamedNotOptArg, Group=defaultNamedNotOptArg):
		"""The transition rules for the specified field and group."""
		ret = self._oleobj_.InvokeTypes(4, LCID, 2, (9, 0), ((8, 1), (12, 1), (12, 1)),EntityName
			, Field, Group)
		if ret is not None:
			ret = Dispatch(ret, u'TransitionRules', None)
		return ret

	_prop_map_get_ = {
	}
	_prop_map_put_ = {
	}

class ICustomizationTransitionRule(DispatchBaseClass):
	"""Represents a single transition rule. A transition rule allows a change in value from a given source value to a given destination value."""
	CLSID = IID('{93FF8BA2-20E4-40C0-BB37-638B5BFD8DAC}')
	coclass_clsid = IID('{F4332BCF-1D24-4F24-AD5E-FEDEC7FAFBB6}')

	_prop_map_get_ = {
		"DestinationValue": (2, 2, (8, 0), (), "DestinationValue", None),
		"IsAllowed": (3, 2, (11, 0), (), "IsAllowed", None),
		"SourceValue": (1, 2, (8, 0), (), "SourceValue", None),
		"Updated": (4, 2, (11, 0), (), "Updated", None),
	}
	_prop_map_put_ = {
		"DestinationValue": ((2, LCID, 4, 0),()),
		"IsAllowed": ((3, LCID, 4, 0),()),
		"SourceValue": ((1, LCID, 4, 0),()),
		"Updated": ((4, LCID, 4, 0),()),
	}

class ICustomizationTransitionRules(DispatchBaseClass):
	"""A collection of CustomizationTransitionRule objects applied to a specific field and user group."""
	CLSID = IID('{365FA56C-AE1E-46EB-A776-6EDDB82BF290}')
	coclass_clsid = IID('{1570FD42-5F4B-49B2-839C-712098B3543E}')

	def AddTransitionRule(self):
		"""Adds a new CustomizationTransitionRule to the current object."""
		ret = self._oleobj_.InvokeTypes(3, LCID, 1, (9, 0), (),)
		if ret is not None:
			ret = Dispatch(ret, u'AddTransitionRule', None)
		return ret

	def RemoveTransitionRule(self, Rule=defaultNamedNotOptArg):
		"""Removes the specified transition rule from the current object."""
		return self._oleobj_.InvokeTypes(4, LCID, 1, (24, 0), ((12, 1),),Rule
			)

	# The method TransitionRule is actually a property, but must be used as a method to correctly pass the arguments
	def TransitionRule(self, Position=defaultNamedNotOptArg):
		"""The CustomizationTransitionRule specified by its position in the list of transition rules."""
		ret = self._oleobj_.InvokeTypes(2, LCID, 2, (9, 0), ((3, 1),),Position
			)
		if ret is not None:
			ret = Dispatch(ret, u'TransitionRule', None)
		return ret

	_prop_map_get_ = {
		"Count": (1, 2, (3, 0), (), "Count", None),
		"EntityName": (8, 2, (8, 0), (), "EntityName", None),
		"Field": (6, 2, (9, 0), (), "Field", None),
		"Group": (7, 2, (9, 0), (), "Group", None),
		"Updated": (5, 2, (11, 0), (), "Updated", None),
	}
	_prop_map_put_ = {
		"Updated": ((5, LCID, 4, 0),()),
	}
	#This class has Count() property - allow len(ob) to provide this
	def __len__(self):
		return self._ApplyTypes_(*(1, 2, (3, 0), (), "Count", None))
	#This class has a __len__ - this is needed so 'if object:' always returns TRUE.
	def __nonzero__(self):
		return True

class ICustomizationUser(DispatchBaseClass):
	"""Represents a user for purposes of adding and removing the user to and from user groups."""
	CLSID = IID('{C10389DD-70AC-48F5-BCF0-9A1CBA5FCAD9}')
	coclass_clsid = IID('{A870D9D0-B890-45E7-B3B3-53B5FA0C4F03}')

	def AddToGroup(self, Group=defaultNamedNotOptArg):
		"""Adds this user to the specified user group."""
		return self._oleobj_.InvokeTypes(8, LCID, 1, (24, 0), ((12, 1),),Group
			)

	# Result is of type IList
	def GroupsList(self):
		"""The list of all the user groups to which this user belongs."""
		ret = self._oleobj_.InvokeTypes(11, LCID, 1, (9, 0), (),)
		if ret is not None:
			ret = Dispatch(ret, u'GroupsList', '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')
		return ret

	# The method InGroup is actually a property, but must be used as a method to correctly pass the arguments
	def InGroup(self, GroupName=defaultNamedNotOptArg):
		"""Indicates if this user is a member of the specified group."""
		return self._oleobj_.InvokeTypes(13, LCID, 2, (11, 0), ((12, 1),),GroupName
			)

	# The method In_Group is actually a property, but must be used as a method to correctly pass the arguments
	def In_Group(self, GroupName=defaultNamedNotOptArg):
		"""Same as InGroup. It is preferable to use InGroup since it uses better error handling."""
		return self._oleobj_.InvokeTypes(9, LCID, 2, (11, 0), ((12, 1),),GroupName
			)

	def RemoveFromGroup(self, Group=defaultNamedNotOptArg):
		"""Removes this user from the specified user group."""
		return self._oleobj_.InvokeTypes(7, LCID, 1, (24, 0), ((12, 1),),Group
			)

	# The method SetInGroup is actually a property, but must be used as a method to correctly pass the arguments
	def SetInGroup(self, GroupName=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""Indicates if this user is a member of the specified group."""
		return self._oleobj_.InvokeTypes(13, LCID, 4, (24, 0), ((12, 1), (11, 1)),GroupName
			, arg1)

	# The method SetIn_Group is actually a property, but must be used as a method to correctly pass the arguments
	def SetIn_Group(self, GroupName=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""Same as InGroup. It is preferable to use InGroup since it uses better error handling."""
		return self._oleobj_.InvokeTypes(9, LCID, 4, (24, 0), ((12, 1), (11, 1)),GroupName
			, arg1)

	_prop_map_get_ = {
		"Address": (1, 2, (8, 0), (), "Address", None),
		"Deleted": (10, 2, (11, 0), (), "Deleted", None),
		"DomainAuthentication": (14, 2, (8, 0), (), "DomainAuthentication", None),
		"Email": (2, 2, (8, 0), (), "Email", None),
		"FullName": (3, 2, (8, 0), (), "FullName", None),
		"Name": (5, 2, (8, 0), (), "Name", None),
		"Phone": (4, 2, (8, 0), (), "Phone", None),
		"Updated": (6, 2, (11, 0), (), "Updated", None),
	}
	_prop_map_put_ = {
		"Address": ((1, LCID, 4, 0),()),
		"Deleted": ((10, LCID, 4, 0),()),
		"DomainAuthentication": ((14, LCID, 4, 0),()),
		"Email": ((2, LCID, 4, 0),()),
		"FullName": ((3, LCID, 4, 0),()),
		"Password": ((12, LCID, 4, 0),()),
		"Phone": ((4, LCID, 4, 0),()),
		"Updated": ((6, LCID, 4, 0),()),
	}

class ICustomizationUsers(DispatchBaseClass):
	"""Services for managing the collection of all CustomizationUser objects."""
	CLSID = IID('{DCFC6A77-C0F7-4F36-82E2-5164749254AF}')
	coclass_clsid = IID('{8FE1F909-64F2-49B3-908F-4F15C64D9F08}')

	def AddSiteAuthenticatedUser(self, UserName=defaultNamedNotOptArg, FullName=defaultNamedNotOptArg, Email=defaultNamedNotOptArg, Description=defaultNamedNotOptArg
			, Phone=defaultNamedNotOptArg, DomainAuthentication=defaultNamedNotOptArg, Group=defaultNamedNotOptArg):
		"""Adds site user with credentials for LDAP authentication."""
		return self._oleobj_.InvokeTypes(8, LCID, 1, (24, 0), ((8, 1), (8, 1), (8, 1), (8, 1), (8, 1), (8, 1), (12, 1)),UserName
			, FullName, Email, Description, Phone, DomainAuthentication
			, Group)

	def AddSiteUser(self, UserName=defaultNamedNotOptArg, FullName=defaultNamedNotOptArg, Email=defaultNamedNotOptArg, Description=defaultNamedNotOptArg
			, Phone=defaultNamedNotOptArg, Group=defaultNamedNotOptArg):
		"""Creates a new site user and adds it to the current project."""
		return self._oleobj_.InvokeTypes(6, LCID, 1, (24, 0), ((8, 1), (8, 1), (8, 1), (8, 1), (8, 1), (12, 1)),UserName
			, FullName, Email, Description, Phone, Group
			)

	def AddUser(self, Name=defaultNamedNotOptArg):
		"""Adds an existing site user to the current project."""
		ret = self._oleobj_.InvokeTypes(2, LCID, 1, (9, 0), ((8, 1),),Name
			)
		if ret is not None:
			ret = Dispatch(ret, u'AddUser', None)
		return ret

	def RemoveUser(self, Name=defaultNamedNotOptArg):
		"""Removes a user from the collection."""
		return self._oleobj_.InvokeTypes(4, LCID, 1, (24, 0), ((8, 0),),Name
			)

	# The method User is actually a property, but must be used as a method to correctly pass the arguments
	def User(self, Name=defaultNamedNotOptArg):
		"""The CustomizationUser object representing the specified user."""
		ret = self._oleobj_.InvokeTypes(1, LCID, 2, (9, 0), ((8, 1),),Name
			)
		if ret is not None:
			ret = Dispatch(ret, u'User', None)
		return ret

	_prop_map_get_ = {
		"DomainUsers": (5, 2, (8, 0), (), "DomainUsers", None),
		"DomainUsersWithoutProjectUsers": (7, 2, (8, 0), (), "DomainUsersWithoutProjectUsers", None),
		# Method 'Users' returns object of type 'IList'
		"Users": (3, 2, (9, 0), (), "Users", '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}'),
	}
	_prop_map_put_ = {
	}

class ICustomizationUsersGroup(DispatchBaseClass):
	"""Represents a user group for purposes of adding and removing users."""
	CLSID = IID('{7E12617E-3B01-42BD-A16F-13118038D0B7}')
	coclass_clsid = IID('{7545BFC6-913F-4DF9-9F70-C815E13E4CBA}')

	def AddUser(self, User=defaultNamedNotOptArg):
		"""Adds a new user to the user group."""
		return self._oleobj_.InvokeTypes(4, LCID, 1, (24, 0), ((12, 1),),User
			)

	# The method HideFilter is actually a property, but must be used as a method to correctly pass the arguments
	def HideFilter(self, FilterType=defaultNamedNotOptArg):
		"""The data hiding filter for the entity specified by the FilterType parameter."""
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(10, LCID, 2, (8, 0), ((8, 1),),FilterType
			)

	def RemoveUser(self, User=defaultNamedNotOptArg):
		"""Removes a user from the user group."""
		return self._oleobj_.InvokeTypes(5, LCID, 1, (24, 0), ((12, 1),),User
			)

	# The method SetHideFilter is actually a property, but must be used as a method to correctly pass the arguments
	def SetHideFilter(self, FilterType=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The data hiding filter for the entity specified by the FilterType parameter."""
		return self._oleobj_.InvokeTypes(10, LCID, 4, (24, 0), ((8, 1), (8, 1)),FilterType
			, arg1)

	# Result is of type IList
	def UsersList(self):
		"""A list of all members of the user group."""
		ret = self._oleobj_.InvokeTypes(6, LCID, 1, (9, 0), (),)
		if ret is not None:
			ret = Dispatch(ret, u'UsersList', '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')
		return ret

	_prop_map_get_ = {
		"Deleted": (8, 2, (11, 0), (), "Deleted", None),
		"ID": (1, 2, (3, 0), (), "ID", None),
		"IsSystem": (9, 2, (11, 0), (), "IsSystem", None),
		"Is_System": (3, 2, (11, 0), (), "Is_System", None),
		"Name": (2, 2, (8, 0), (), "Name", None),
		"Updated": (7, 2, (11, 0), (), "Updated", None),
	}
	_prop_map_put_ = {
		"Deleted": ((8, LCID, 4, 0),()),
		"Name": ((2, LCID, 4, 0),()),
		"Updated": ((7, LCID, 4, 0),()),
	}

class ICustomizationUsersGroups(DispatchBaseClass):
	"""Services for managing the collection of all CustomizationUsersGroup objects."""
	CLSID = IID('{1CD20510-7700-42B6-83AB-4AFC0419318D}')
	coclass_clsid = IID('{AB6C55FF-1D41-4AE7-A8FD-6C7BA8BE2620}')

	def AddGroup(self, Name=defaultNamedNotOptArg):
		"""Adds a new CustomizationUsersGroup to the collection."""
		ret = self._oleobj_.InvokeTypes(1, LCID, 1, (9, 0), ((8, 1),),Name
			)
		if ret is not None:
			ret = Dispatch(ret, u'AddGroup', None)
		return ret

	# The method Group is actually a property, but must be used as a method to correctly pass the arguments
	def Group(self, Name=defaultNamedNotOptArg):
		"""Gets the specified user group."""
		ret = self._oleobj_.InvokeTypes(2, LCID, 2, (9, 0), ((8, 1),),Name
			)
		if ret is not None:
			ret = Dispatch(ret, u'Group', None)
		return ret

	def RemoveGroup(self, Name=defaultNamedNotOptArg):
		"""Removes a CustomizationUsersGroup object."""
		return self._oleobj_.InvokeTypes(3, LCID, 1, (24, 0), ((8, 1),),Name
			)

	_prop_map_get_ = {
		# Method 'Groups' returns object of type 'IList'
		"Groups": (4, 2, (9, 0), (), "Groups", '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}'),
	}
	_prop_map_put_ = {
	}

class IDBManager(DispatchBaseClass):
	"""IDBManager Interface"""
	CLSID = IID('{437A2A3C-00EB-4B53-89C8-BBC5DED2D52C}')
	coclass_clsid = IID('{F8BBE233-A298-4C0D-8D86-3D442BB8ED08}')

	def CreateDatabase(self, DBType=defaultNamedNotOptArg, DBName=defaultNamedNotOptArg, ConnectionString=defaultNamedNotOptArg):
		"""Creates the database on the specified domain."""
		return self._oleobj_.InvokeTypes(1, LCID, 1, (24, 0), ((3, 1), (8, 1), (8, 1)),DBType
			, DBName, ConnectionString)

	def RemoveDatabase(self, DBName=defaultNamedNotOptArg):
		"""Removes the database from the domain."""
		return self._oleobj_.InvokeTypes(2, LCID, 1, (24, 0), ((8, 1),),DBName
			)

	_prop_map_get_ = {
		"AdminUserName": (3, 2, (8, 0), (), "AdminUserName", None),
		"AdminUserPassword": (5, 2, (8, 0), (), "AdminUserPassword", None),
		"Domain": (4, 2, (8, 0), (), "Domain", None),
	}
	_prop_map_put_ = {
		"AdminUserName": ((3, LCID, 4, 0),()),
		"AdminUserPassword": ((5, LCID, 4, 0),()),
		"Domain": ((4, LCID, 4, 0),()),
	}

class IDesignStep(DispatchBaseClass):
	"""Represents a single design step in a test."""
	CLSID = IID('{E04B8E16-1118-4D05-90CE-3DCC39633158}')
	coclass_clsid = IID('{19AD0931-4FF6-4B1C-83AB-464A9D200345}')

	# The method Field is actually a property, but must be used as a method to correctly pass the arguments
	def Field(self, FieldName=defaultNamedNotOptArg):
		"""The value of the specified field."""
		return self._ApplyTypes_(0, 2, (12, 0), ((8, 0),), u'Field', None,FieldName
			)

	def LockObject(self):
		"""Locks the object. Returns True if lock successful."""
		return self._oleobj_.InvokeTypes(2, LCID, 1, (11, 0), (),)

	def Post(self):
		"""Posts all changed values into database."""
		return self._oleobj_.InvokeTypes(6, LCID, 1, (24, 0), (),)

	def Refresh(self):
		"""Reads saved values, overwriting values in memory."""
		return self._oleobj_.InvokeTypes(7, LCID, 1, (24, 0), (),)

	# The method SetField is actually a property, but must be used as a method to correctly pass the arguments
	def SetField(self, FieldName=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The value of the specified field."""
		return self._oleobj_.InvokeTypes(0, LCID, 4, (24, 0), ((8, 0), (12, 1)),FieldName
			, arg1)

	def UnLockObject(self):
		"""Unlocks the object."""
		return self._oleobj_.InvokeTypes(3, LCID, 1, (24, 0), (),)

	def Undo(self):
		"""Undoes changes to field values that have not been posted."""
		return self._oleobj_.InvokeTypes(8, LCID, 1, (24, 0), (),)

	_prop_map_get_ = {
		"Attachments": (12, 2, (9, 0), (), "Attachments", None),
		"AutoPost": (5, 2, (11, 0), (), "AutoPost", None),
		"EvaluatedStepDescription": (21, 2, (8, 0), (), "EvaluatedStepDescription", None),
		"EvaluatedStepExpectedResult": (22, 2, (8, 0), (), "EvaluatedStepExpectedResult", None),
		"HasAttachment": (13, 2, (11, 0), (), "HasAttachment", None),
		"History": (11, 2, (9, 0), (), "History", None),
		"ID": (4, 2, (12, 0), (), "ID", None),
		"IsLocked": (1, 2, (11, 0), (), "IsLocked", None),
		"LinkTest": (18, 2, (12, 0), (), "LinkTest", None),
		"LinkTestID": (20, 2, (3, 0), (), "LinkTestID", None),
		"LinkedParams": (19, 2, (9, 0), (), "LinkedParams", None),
		"Modified": (9, 2, (11, 0), (), "Modified", None),
		"Order": (17, 2, (3, 0), (), "Order", None),
		"ParentTest": (23, 2, (9, 0), (), "ParentTest", None),
		"StepDescription": (15, 2, (8, 0), (), "StepDescription", None),
		"StepExpectedResult": (16, 2, (8, 0), (), "StepExpectedResult", None),
		"StepName": (14, 2, (8, 0), (), "StepName", None),
		"Virtual": (10, 2, (11, 0), (), "Virtual", None),
	}
	_prop_map_put_ = {
		"AutoPost": ((5, LCID, 4, 0),()),
		"LinkTest": ((18, LCID, 4, 0),()),
		"Order": ((17, LCID, 4, 0),()),
		"StepDescription": ((15, LCID, 4, 0),()),
		"StepExpectedResult": ((16, LCID, 4, 0),()),
		"StepName": ((14, LCID, 4, 0),()),
	}
	# Default method for this class is 'Field'
	def __call__(self, FieldName=defaultNamedNotOptArg):
		"""The value of the specified field."""
		return self._ApplyTypes_(0, 2, (12, 0), ((8, 0),), '__call__', None,FieldName
			)

	# str(ob) and int(ob) will use __call__
	def __unicode__(self, *args):
		try:
			return unicode(self.__call__(*args))
		except pythoncom.com_error:
			return repr(self)
	def __str__(self, *args):
		return str(self.__unicode__(*args))
	def __int__(self, *args):
		return int(self.__call__(*args))

class IExecEventInfo(DispatchBaseClass):
	"""The execution information of the scheduler. This object handles the actual, not the planned, information."""
	CLSID = IID('{5377347E-3F8D-4B54-B962-18527B652EDD}')
	coclass_clsid = IID('{0409CEAA-2CAA-4596-A9EF-B9F0644D17EA}')

	# The method EventParam is actually a property, but must be used as a method to correctly pass the arguments
	def EventParam(self, ParamName=defaultNamedNotOptArg):
		"""Gets the specified Execution Event Parameter."""
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(4, LCID, 2, (8, 0), ((8, 1),),ParamName
			)

	_prop_map_get_ = {
		"EventDate": (3, 2, (8, 0), (), "EventDate", None),
		"EventTime": (2, 2, (8, 0), (), "EventTime", None),
		"EventType": (1, 2, (3, 0), (), "EventType", None),
	}
	_prop_map_put_ = {
	}

class IExecEventNotifyByMailSettings(DispatchBaseClass):
	"""Represents the notification to be sent by email after a test has completed its run."""
	CLSID = IID('{615FBF36-3E96-4C0F-9827-12FA20D13C58}')
	coclass_clsid = IID('{D1D6B200-B24D-4C8D-8622-77FDBF49AC99}')

	# The method Enabled is actually a property, but must be used as a method to correctly pass the arguments
	def Enabled(self, EventType=defaultNamedNotOptArg):
		"""Indicates if mail is enabled for the specifed event type."""
		return self._oleobj_.InvokeTypes(3, LCID, 2, (11, 0), ((3, 1),),EventType
			)

	def Save(self, AutoPost=True):
		"""Uploads the notification settings to the server."""
		return self._oleobj_.InvokeTypes(4, LCID, 1, (24, 0), ((11, 49),),AutoPost
			)

	# The method SetEnabled is actually a property, but must be used as a method to correctly pass the arguments
	def SetEnabled(self, EventType=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""Indicates if mail is enabled for the specifed event type."""
		return self._oleobj_.InvokeTypes(3, LCID, 4, (24, 0), ((3, 1), (11, 1)),EventType
			, arg1)

	_prop_map_get_ = {
		"EMailTo": (1, 2, (8, 0), (), "EMailTo", None),
		"UserMessage": (2, 2, (8, 0), (), "UserMessage", None),
	}
	_prop_map_put_ = {
		"EMailTo": ((1, LCID, 4, 0),()),
		"UserMessage": ((2, LCID, 4, 0),()),
	}

class IExecutionSettings(DispatchBaseClass):
	"""Information on the execution of a test set."""
	CLSID = IID('{41120F91-BBBE-4913-975D-5346234765A6}')
	coclass_clsid = IID('{85F1A554-CF12-42A1-BC3F-BBF8BBE83DF1}')

	# The method OnExecEventSchedulerActionParams is actually a property, but must be used as a method to correctly pass the arguments
	def OnExecEventSchedulerActionParams(self, EventType=defaultNamedNotOptArg):
		"""The restart action parameters."""
		ret = self._oleobj_.InvokeTypes(5, LCID, 2, (9, 0), ((3, 1),),EventType
			)
		if ret is not None:
			ret = Dispatch(ret, u'OnExecEventSchedulerActionParams', None)
		return ret

	# The method OnExecEventSchedulerActionType is actually a property, but must be used as a method to correctly pass the arguments
	def OnExecEventSchedulerActionType(self, EventType=defaultNamedNotOptArg):
		"""The action type of the execution event."""
		return self._oleobj_.InvokeTypes(4, LCID, 2, (3, 0), ((3, 1),),EventType
			)

	# The method SetOnExecEventSchedulerActionType is actually a property, but must be used as a method to correctly pass the arguments
	def SetOnExecEventSchedulerActionType(self, EventType=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The action type of the execution event."""
		return self._oleobj_.InvokeTypes(4, LCID, 4, (24, 0), ((3, 1), (3, 1)),EventType
			, arg1)

	_prop_map_get_ = {
		"PlannedExecutionDate": (2, 2, (12, 0), (), "PlannedExecutionDate", None),
		"PlannedExecutionTime": (1, 2, (12, 0), (), "PlannedExecutionTime", None),
		"PlannedRunDuration": (3, 2, (12, 0), (), "PlannedRunDuration", None),
	}
	_prop_map_put_ = {
		"PlannedExecutionDate": ((2, LCID, 4, 0),()),
		"PlannedExecutionTime": ((1, LCID, 4, 0),()),
		"PlannedRunDuration": ((3, LCID, 4, 0),()),
	}

class IExecutionStatus(DispatchBaseClass):
	"""Represents the execution status of the scheduler. The user can scan through each test in the scheduler."""
	CLSID = IID('{D367F107-1BCE-4AAB-8E6B-BF6399BD64FC}')
	coclass_clsid = IID('{E2E80F73-808A-495E-89F6-430721FE9623}')

	# Result is of type IList
	def EventsList(self):
		"""Gets the list of the execution events."""
		ret = self._oleobj_.InvokeTypes(4, LCID, 1, (9, 0), (),)
		if ret is not None:
			ret = Dispatch(ret, u'EventsList', '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')
		return ret

	# The method Item is actually a property, but must be used as a method to correctly pass the arguments
	def Item(self, Index=defaultNamedNotOptArg):
		"""Gets a TestExecStatus object by index (1-based)."""
		return self._ApplyTypes_(0, 2, (12, 0), ((3, 0),), u'Item', None,Index
			)

	def RefreshExecStatusInfo(self, TestData=defaultNamedNotOptArg, Force=defaultNamedNotOptArg):
		"""Refreshes the execution status from the execution controller."""
		return self._oleobj_.InvokeTypes(2, LCID, 1, (24, 0), ((12, 1), (11, 1)),TestData
			, Force)

	_prop_map_get_ = {
		"Count": (1, 2, (3, 0), (), "Count", None),
		"Finished": (3, 2, (11, 0), (), "Finished", None),
	}
	_prop_map_put_ = {
	}
	# Default method for this class is 'Item'
	def __call__(self, Index=defaultNamedNotOptArg):
		"""Gets a TestExecStatus object by index (1-based)."""
		return self._ApplyTypes_(0, 2, (12, 0), ((3, 0),), '__call__', None,Index
			)

	# str(ob) and int(ob) will use __call__
	def __unicode__(self, *args):
		try:
			return unicode(self.__call__(*args))
		except pythoncom.com_error:
			return repr(self)
	def __str__(self, *args):
		return str(self.__unicode__(*args))
	def __int__(self, *args):
		return int(self.__call__(*args))
	def __iter__(self):
		"Return a Python iterator for this object"
		ob = self._oleobj_.InvokeTypes(-4,LCID,2,(13, 10),())
		return win32com.client.util.Iterator(ob, None)
	def _NewEnum(self):
		"Create an enumerator from this object"
		return win32com.client.util.WrapEnum(self._oleobj_.InvokeTypes(-4,LCID,2,(13, 10),()),None)
	def __getitem__(self, index):
		"Allow this class to be accessed as a collection"
		if '_enum_' not in self.__dict__:
			self.__dict__['_enum_'] = self._NewEnum()
		return self._enum_.__getitem__(index)
	#This class has Count() property - allow len(ob) to provide this
	def __len__(self):
		return self._ApplyTypes_(*(1, 2, (3, 0), (), "Count", None))
	#This class has a __len__ - this is needed so 'if object:' always returns TRUE.
	def __nonzero__(self):
		return True

class IExport(DispatchBaseClass):
	"""IExport Interface"""
	CLSID = IID('{08E8895B-686D-4F75-9F16-F9E130D99470}')
	coclass_clsid = IID('{DCB4C421-E9F4-4A89-9190-B49411B17167}')

	def ExportData(self, XMLData=defaultNamedNotOptArg, ResourceID=defaultNamedNotOptArg):
		"""method ExportData"""
		return self._oleobj_.InvokeTypes(1, LCID, 1, (24, 0), ((8, 0), (8, 0)),XMLData
			, ResourceID)

	_prop_map_get_ = {
	}
	_prop_map_put_ = {
	}

class IExtendedStorage(DispatchBaseClass):
	"""Represents a storage structure used to transfer files between the server and client file system and delete files."""
	CLSID = IID('{BF9B38B0-935B-4112-92EC-49FED46AC64D}')
	coclass_clsid = IID('{41BAF7D8-718C-45B6-9115-C91850F27074}')

	def Cancel(self):
		"""Cancels the load or save action."""
		return self._oleobj_.InvokeTypes(6, LCID, 1, (24, 0), (),)

	def Delete(self, FSysFilter=defaultNamedNotOptArg, nDeleteType=defaultNamedNotOptArg):
		"""Deletes files locally or from the server."""
		return self._oleobj_.InvokeTypes(3, LCID, 1, (24, 0), ((8, 1), (3, 1)),FSysFilter
			, nDeleteType)

	def GetLastError(self):
		"""Gets the last error that occurred during asynchronous load and save operations."""
		return self._oleobj_.InvokeTypes(9, LCID, 1, (24, 0), (),)

	def Load(self, FSysFilter=u'*.*', synchronize=False):
		"""Downloads the storage structure to the client file system."""
		return self._ApplyTypes_(1, 1, (8, 32), ((8, 49), (11, 49)), u'Load', None,FSysFilter
			, synchronize)

	def LoadEx(self, FSysFilter=defaultNamedNotOptArg, synchronize=defaultNamedNotOptArg, pVal=pythoncom.Missing, pNonFatalErrorOccured=pythoncom.Missing):
		"""Downloads the storage structure to the client file system."""
		return self._ApplyTypes_(11, 1, (8, 0), ((8, 1), (11, 1), (16393, 2), (16395, 2)), u'LoadEx', None,FSysFilter
			, synchronize, pVal, pNonFatalErrorOccured)

	def Progress(self, Total=pythoncom.Missing, Current=pythoncom.Missing):
		"""Polls the progress of the last action (Load or Save)."""
		return self._ApplyTypes_(8, 1, (8, 0), ((16387, 2), (16387, 2)), u'Progress', None,Total
			, Current)

	def Save(self, FSysFilter=u'*.*', synchronize=False):
		"""Uploads the storage structure to the server."""
		return self._ApplyTypes_(2, 1, (24, 32), ((8, 49), (11, 49)), u'Save', None,FSysFilter
			, synchronize)

	def SaveEx(self, FSysFilter=defaultNamedNotOptArg, synchronize=defaultNamedNotOptArg, pVal=pythoncom.Missing):
		"""Uploads the storage structure to the server."""
		return self._ApplyTypes_(10, 1, (11, 0), ((8, 1), (11, 1), (16393, 2)), u'SaveEx', None,FSysFilter
			, synchronize, pVal)

	_prop_map_get_ = {
		"ActionFinished": (7, 2, (3, 0), (), "ActionFinished", None),
		"ClientPath": (5, 2, (8, 0), (), "ClientPath", None),
		"Root": (0, 2, (9, 0), (), "Root", None),
		"ServerPath": (4, 2, (8, 0), (), "ServerPath", None),
	}
	_prop_map_put_ = {
		"ClientPath": ((5, LCID, 4, 0),()),
		"ServerPath": ((4, LCID, 4, 0),()),
	}
	# Default property for this class is 'Root'
	def __call__(self):
		return self._ApplyTypes_(*(0, 2, (9, 0), (), "Root", None))
	# str(ob) and int(ob) will use __call__
	def __unicode__(self, *args):
		try:
			return unicode(self.__call__(*args))
		except pythoncom.com_error:
			return repr(self)
	def __str__(self, *args):
		return str(self.__unicode__(*args))
	def __int__(self, *args):
		return int(self.__call__(*args))

class IFactoryList(DispatchBaseClass):
	"""Services to create and maintains lists within entity factories. Use any factory object to create any number of list instances for objects in the factory."""
	CLSID = IID('{68FE31A3-6242-4AA4-9BBE-D0715F810DB3}')
	coclass_clsid = IID('{3F767372-C6D1-4F1B-8F27-1EABB614551C}')

	def Add(self, vNew=defaultNamedNotOptArg):
		"""Adds a new item to the current list object."""
		return self._oleobj_.InvokeTypes(2, LCID, 1, (24, 0), ((12, 0),),vNew
			)

	def IndexOfItem(self, Item=defaultNamedNotOptArg):
		"""Gets the index of the passed object."""
		return self._oleobj_.InvokeTypes(11, LCID, 1, (3, 0), ((12, 0),),Item
			)

	def Insert(self, Pos=defaultNamedNotOptArg, vNew=defaultNamedNotOptArg):
		"""Inserts a new item at the specified position."""
		return self._oleobj_.InvokeTypes(5, LCID, 1, (24, 0), ((3, 0), (12, 0)),Pos
			, vNew)

	# The method Item is actually a property, but must be used as a method to correctly pass the arguments
	def Item(self, Index=defaultNamedNotOptArg):
		"""Gets an item by index. The index is one-based."""
		return self._ApplyTypes_(0, 2, (12, 0), ((3, 0),), u'Item', None,Index
			)

	def Post(self):
		"""Posts changed data for all items in list."""
		return self._oleobj_.InvokeTypes(9, LCID, 1, (24, 0), (),)

	# The method Ratio is actually a property, but must be used as a method to correctly pass the arguments
	def Ratio(self, Index=defaultNamedNotOptArg):
		"""The similarity ratio for the specified item."""
		return self._oleobj_.InvokeTypes(10, LCID, 2, (3, 0), ((3, 1),),Index
			)

	def Refresh(self):
		"""Reads saved list filter and data for all listed objects, overwriting values in memory."""
		return self._oleobj_.InvokeTypes(8, LCID, 1, (24, 0), (),)

	def Remove(self, Index=defaultNamedNotOptArg):
		"""Removes the specified item from the current list object."""
		return self._oleobj_.InvokeTypes(3, LCID, 1, (24, 0), ((3, 0),),Index
			)

	def Swap(self, Pos1=defaultNamedNotOptArg, Pos2=defaultNamedNotOptArg):
		"""Swaps the two list items specified by their positions."""
		return self._oleobj_.InvokeTypes(6, LCID, 1, (24, 0), ((3, 0), (3, 0)),Pos1
			, Pos2)

	_prop_map_get_ = {
		"Count": (1, 2, (3, 0), (), "Count", None),
		# Method 'Fields' returns object of type 'IList'
		"Fields": (7, 2, (9, 0), (), "Fields", '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}'),
	}
	_prop_map_put_ = {
	}
	# Default method for this class is 'Item'
	def __call__(self, Index=defaultNamedNotOptArg):
		"""Gets an item by index. The index is one-based."""
		return self._ApplyTypes_(0, 2, (12, 0), ((3, 0),), '__call__', None,Index
			)

	# str(ob) and int(ob) will use __call__
	def __unicode__(self, *args):
		try:
			return unicode(self.__call__(*args))
		except pythoncom.com_error:
			return repr(self)
	def __str__(self, *args):
		return str(self.__unicode__(*args))
	def __int__(self, *args):
		return int(self.__call__(*args))
	def __iter__(self):
		"Return a Python iterator for this object"
		ob = self._oleobj_.InvokeTypes(-4,LCID,2,(13, 10),())
		return win32com.client.util.Iterator(ob, None)
	def _NewEnum(self):
		"Create an enumerator from this object"
		return win32com.client.util.WrapEnum(self._oleobj_.InvokeTypes(-4,LCID,2,(13, 10),()),None)
	def __getitem__(self, index):
		"Allow this class to be accessed as a collection"
		if '_enum_' not in self.__dict__:
			self.__dict__['_enum_'] = self._NewEnum()
		return self._enum_.__getitem__(index)
	#This class has Count() property - allow len(ob) to provide this
	def __len__(self):
		return self._ApplyTypes_(*(1, 2, (3, 0), (), "Count", None))
	#This class has a __len__ - this is needed so 'if object:' always returns TRUE.
	def __nonzero__(self):
		return True

class IFieldProperty(DispatchBaseClass):
	"""Properties for a field."""
	CLSID = IID('{C2E1AF68-28C8-4DA3-8C65-5E1D230B1FF6}')
	coclass_clsid = None

	_prop_map_get_ = {
		"DBColumnName": (2, 2, (8, 0), (), "DBColumnName", None),
		"DBColumnType": (3, 2, (8, 0), (), "DBColumnType", None),
		"DBTableName": (1, 2, (8, 0), (), "DBTableName", None),
		"EditMask": (5, 2, (8, 0), (), "EditMask", None),
		"EditStyle": (4, 2, (8, 0), (), "EditStyle", None),
		"FieldSize": (21, 2, (3, 0), (), "FieldSize", None),
		"IsActive": (11, 2, (11, 0), (), "IsActive", None),
		"IsByCode": (16, 2, (11, 0), (), "IsByCode", None),
		"IsCanFilter": (7, 2, (11, 0), (), "IsCanFilter", None),
		"IsCustomizable": (20, 2, (11, 0), (), "IsCustomizable", None),
		"IsEdit": (10, 2, (11, 0), (), "IsEdit", None),
		"IsHistory": (12, 2, (11, 0), (), "IsHistory", None),
		"IsKeepValue": (19, 2, (11, 0), (), "IsKeepValue", None),
		"IsKey": (8, 2, (11, 0), (), "IsKey", None),
		"IsMail": (13, 2, (11, 0), (), "IsMail", None),
		"IsModify": (25, 2, (11, 0), (), "IsModify", None),
		"IsRequired": (17, 2, (11, 0), (), "IsRequired", None),
		"IsSystem": (6, 2, (11, 0), (), "IsSystem", None),
		"IsToSum": (24, 2, (11, 0), (), "IsToSum", None),
		"IsVerify": (14, 2, (11, 0), (), "IsVerify", None),
		"IsVersionControl": (26, 2, (11, 0), (), "IsVersionControl", None),
		"IsVisible": (27, 2, (11, 0), (), "IsVisible", None),
		"IsVisibleInNewBug": (22, 2, (11, 0), (), "IsVisibleInNewBug", None),
		"KeyOrder": (9, 2, (3, 0), (), "KeyOrder", None),
		"ReadOnly": (23, 2, (11, 0), (), "ReadOnly", None),
		"Root": (15, 2, (9, 0), (), "Root", None),
		"UserColumnType": (18, 2, (8, 0), (), "UserColumnType", None),
		"UserLabel": (0, 2, (8, 0), (), "UserLabel", None),
	}
	_prop_map_put_ = {
	}
	# Default property for this class is 'UserLabel'
	def __call__(self):
		return self._ApplyTypes_(*(0, 2, (8, 0), (), "UserLabel", None))
	# str(ob) and int(ob) will use __call__
	def __unicode__(self, *args):
		try:
			return unicode(self.__call__(*args))
		except pythoncom.com_error:
			return repr(self)
	def __str__(self, *args):
		return str(self.__unicode__(*args))
	def __int__(self, *args):
		return int(self.__call__(*args))

class IFieldProperty2(DispatchBaseClass):
	"""IFieldProperty2 Interface"""
	CLSID = IID('{F5DEB618-2CF4-44A1-81A1-00922B9C4F5B}')
	coclass_clsid = IID('{B7E70913-D656-4DB9-8763-3812FC75BB46}')

	_prop_map_get_ = {
		"DBColumnName": (2, 2, (8, 0), (), "DBColumnName", None),
		"DBColumnType": (3, 2, (8, 0), (), "DBColumnType", None),
		"DBTableName": (1, 2, (8, 0), (), "DBTableName", None),
		"EditMask": (5, 2, (8, 0), (), "EditMask", None),
		"EditStyle": (4, 2, (8, 0), (), "EditStyle", None),
		"FieldSize": (21, 2, (3, 0), (), "FieldSize", None),
		"IsActive": (11, 2, (11, 0), (), "IsActive", None),
		"IsByCode": (16, 2, (11, 0), (), "IsByCode", None),
		"IsCanFilter": (7, 2, (11, 0), (), "IsCanFilter", None),
		"IsCanGroup": (28, 2, (11, 0), (), "IsCanGroup", None),
		"IsCustomizable": (20, 2, (11, 0), (), "IsCustomizable", None),
		"IsEdit": (10, 2, (11, 0), (), "IsEdit", None),
		"IsHistory": (12, 2, (11, 0), (), "IsHistory", None),
		"IsKeepValue": (19, 2, (11, 0), (), "IsKeepValue", None),
		"IsKey": (8, 2, (11, 0), (), "IsKey", None),
		"IsMail": (13, 2, (11, 0), (), "IsMail", None),
		"IsModify": (25, 2, (11, 0), (), "IsModify", None),
		"IsMultiValue": (30, 2, (11, 0), (), "IsMultiValue", None),
		"IsRequired": (17, 2, (11, 0), (), "IsRequired", None),
		"IsSearchable": (29, 2, (11, 0), (), "IsSearchable", None),
		"IsSystem": (6, 2, (11, 0), (), "IsSystem", None),
		"IsToSum": (24, 2, (11, 0), (), "IsToSum", None),
		"IsVerify": (14, 2, (11, 0), (), "IsVerify", None),
		"IsVersionControl": (26, 2, (11, 0), (), "IsVersionControl", None),
		"IsVisible": (27, 2, (11, 0), (), "IsVisible", None),
		"IsVisibleInNewBug": (22, 2, (11, 0), (), "IsVisibleInNewBug", None),
		"KeyOrder": (9, 2, (3, 0), (), "KeyOrder", None),
		"ReadOnly": (23, 2, (11, 0), (), "ReadOnly", None),
		"Root": (15, 2, (9, 0), (), "Root", None),
		"UserColumnType": (18, 2, (8, 0), (), "UserColumnType", None),
		"UserLabel": (0, 2, (8, 0), (), "UserLabel", None),
	}
	_prop_map_put_ = {
	}
	# Default property for this class is 'UserLabel'
	def __call__(self):
		return self._ApplyTypes_(*(0, 2, (8, 0), (), "UserLabel", None))
	# str(ob) and int(ob) will use __call__
	def __unicode__(self, *args):
		try:
			return unicode(self.__call__(*args))
		except pythoncom.com_error:
			return repr(self)
	def __str__(self, *args):
		return str(self.__unicode__(*args))
	def __int__(self, *args):
		return int(self.__call__(*args))

class IFileData(DispatchBaseClass):
	"""Represents folder or file information."""
	CLSID = IID('{3A2C3A43-4D96-4682-91CC-9E091B64C1FE}')
	coclass_clsid = IID('{37B25CC5-C319-4DAD-A57D-05A8D1201EE0}')

	# Result is of type IList
	def Items(self):
		"""The folder's child items."""
		ret = self._oleobj_.InvokeTypes(4, LCID, 1, (9, 0), (),)
		if ret is not None:
			ret = Dispatch(ret, u'Items', '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')
		return ret

	def ModifyDate(self):
		"""The file's modify date."""
		return self._oleobj_.InvokeTypes(3, LCID, 1, (7, 0), (),)

	def Size(self):
		"""The file size in kbytes."""
		return self._oleobj_.InvokeTypes(2, LCID, 1, (3, 0), (),)

	def Type(self):
		"""The type of object for which data is stored."""
		return self._oleobj_.InvokeTypes(1, LCID, 1, (2, 0), (),)

	_prop_map_get_ = {
		"Name": (0, 2, (8, 0), (), "Name", None),
	}
	_prop_map_put_ = {
	}
	# Default property for this class is 'Name'
	def __call__(self):
		return self._ApplyTypes_(*(0, 2, (8, 0), (), "Name", None))
	# str(ob) and int(ob) will use __call__
	def __unicode__(self, *args):
		try:
			return unicode(self.__call__(*args))
		except pythoncom.com_error:
			return repr(self)
	def __str__(self, *args):
		return str(self.__unicode__(*args))
	def __int__(self, *args):
		return int(self.__call__(*args))

class IFollowUpManager(DispatchBaseClass):
	"""Manages the user-defined follow-ups."""
	CLSID = IID('{6EE10992-2569-4838-86DA-DAE1E1240E79}')
	coclass_clsid = IID('{488D6BE8-A962-4958-8E11-6E2EE9A20EFF}')

	def CancelFollowUp(self):
		"""Removes the current entity's follow-up."""
		return self._oleobj_.InvokeTypes(1610678274, LCID, 1, (24, 0), (),)

	def GetFollowUp(self, FollowUpDate=pythoncom.Missing, Description=pythoncom.Missing):
		"""Gets the date and description for the follow-up associated with the current object."""
		return self._ApplyTypes_(1610678272, 1, (24, 0), ((16391, 2), (16392, 2)), u'GetFollowUp', None,FollowUpDate
			, Description)

	def HasFollowUp(self):
		"""This object has at least one follow-up associated with it."""
		return self._oleobj_.InvokeTypes(1610678275, LCID, 1, (11, 0), (),)

	def IsFollowUpOverdue(self):
		"""Checks if the date of the unsent follow-up is earlier than the current database server date."""
		return self._oleobj_.InvokeTypes(1610678276, LCID, 1, (11, 0), (),)

	def SetFollowUp(self, FollowUpDate=defaultNamedNotOptArg, Description=defaultNamedNotOptArg):
		"""Sets an alert for follow-up for the current object."""
		return self._oleobj_.InvokeTypes(1610678273, LCID, 1, (24, 0), ((7, 1), (8, 1)),FollowUpDate
			, Description)

	_prop_map_get_ = {
	}
	_prop_map_put_ = {
	}

class IGraph(DispatchBaseClass):
	"""Represents a graph built through a method."""
	CLSID = IID('{ECCB1143-8914-497A-ACA0-8789CA64C2D6}')
	coclass_clsid = IID('{8CEB0A26-08A1-4110-A851-6BB50278BF94}')

	# The method ColName is actually a property, but must be used as a method to correctly pass the arguments
	def ColName(self, Col=defaultNamedNotOptArg):
		"""Gets the name of the specified column. Column numbers are zero-based."""
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(1, LCID, 2, (8, 0), ((3, 1),),Col
			)

	# The method ColTotal is actually a property, but must be used as a method to correctly pass the arguments
	def ColTotal(self, Col=defaultNamedNotOptArg):
		"""The sum of the values in the specified graph data column. The column number is zero-based."""
		return self._oleobj_.InvokeTypes(11, LCID, 2, (3, 0), ((3, 1),),Col
			)

	# The method ColType is actually a property, but must be used as a method to correctly pass the arguments
	def ColType(self, Col=defaultNamedNotOptArg):
		"""The column type."""
		return self._oleobj_.InvokeTypes(3, LCID, 2, (2, 0), ((3, 1),),Col
			)

	# The method Data is actually a property, but must be used as a method to correctly pass the arguments
	def Data(self, Col=defaultNamedNotOptArg, Row=defaultNamedNotOptArg):
		"""The data in the specified cell. Column and Row numbers are zero-based."""
		return self._oleobj_.InvokeTypes(4, LCID, 2, (3, 0), ((3, 1), (3, 1)),Col
			, Row)

	# Result is of type IList
	def DrillDown(self, pAreas=defaultNamedNotOptArg, mAreas=defaultNamedNotOptArg):
		"""Drills down to graph data."""
		ret = self._oleobj_.InvokeTypes(7, LCID, 1, (9, 0), ((12, 1), (12, 1)),pAreas
			, mAreas)
		if ret is not None:
			ret = Dispatch(ret, u'DrillDown', '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')
		return ret

	# Result is of type IList
	def MultiDrillDown(self, Areas=defaultNamedNotOptArg):
		"""Gets detailed information about a graph area consisting of four sets of coordinates."""
		ret = self._oleobj_.InvokeTypes(12, LCID, 1, (9, 0), ((12, 1),),Areas
			)
		if ret is not None:
			ret = Dispatch(ret, u'MultiDrillDown', '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')
		return ret

	# The method RowName is actually a property, but must be used as a method to correctly pass the arguments
	def RowName(self, Row=defaultNamedNotOptArg):
		"""Gets the row name if the specified row. Row numbers are zero-based."""
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(2, LCID, 2, (8, 0), ((3, 1),),Row
			)

	# The method RowTotal is actually a property, but must be used as a method to correctly pass the arguments
	def RowTotal(self, Row=defaultNamedNotOptArg):
		"""The sum of the values in the specified graph data row. The row number is zero-based."""
		return self._oleobj_.InvokeTypes(10, LCID, 2, (3, 0), ((3, 1),),Row
			)

	_prop_map_get_ = {
		"ColCount": (5, 2, (3, 0), (), "ColCount", None),
		"GraphTotal": (9, 2, (3, 0), (), "GraphTotal", None),
		"MaxValue": (8, 2, (3, 0), (), "MaxValue", None),
		"RowCount": (6, 2, (3, 0), (), "RowCount", None),
		"StartDate": (13, 2, (7, 0), (), "StartDate", None),
	}
	_prop_map_put_ = {
	}

class IGraphBuilder(DispatchBaseClass):
	"""Services for creating graphs."""
	CLSID = IID('{8F96D0C2-FFFE-4C6F-984E-CE022A50EA0E}')
	coclass_clsid = IID('{771F8212-D6CE-48A1-9F3A-87EF3E6E4CD5}')

	def BuildGraph(self, pGraphDef=defaultNamedNotOptArg):
		"""Build the graph."""
		ret = self._oleobj_.InvokeTypes(2, LCID, 1, (9, 0), ((9, 1),),pGraphDef
			)
		if ret is not None:
			ret = Dispatch(ret, u'BuildGraph', None)
		return ret

	# Result is of type IList
	def BuildMultipleGraphs(self, GraphDefs=defaultNamedNotOptArg):
		"""Build several graphs."""
		ret = self._oleobj_.InvokeTypes(3, LCID, 1, (9, 0), ((12, 1),),GraphDefs
			)
		if ret is not None:
			ret = Dispatch(ret, u'BuildMultipleGraphs', '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')
		return ret

	def CreateGraphDefinition(self, Module=defaultNamedNotOptArg, GraphType=defaultNamedNotOptArg):
		"""Create an IGraphDefinition object."""
		ret = self._oleobj_.InvokeTypes(1, LCID, 1, (9, 0), ((3, 1), (3, 1)),Module
			, GraphType)
		if ret is not None:
			ret = Dispatch(ret, u'CreateGraphDefinition', None)
		return ret

	_prop_map_get_ = {
	}
	_prop_map_put_ = {
	}

class IGraphDefinition(DispatchBaseClass):
	"""IGraphDefinition Interface"""
	CLSID = IID('{96615E7A-A7C4-4B6F-A00A-418B92070F83}')
	coclass_clsid = IID('{FDF08CE0-896F-498D-928D-F85512B52F4F}')

	# The method Property is actually a property, but must be used as a method to correctly pass the arguments
	def Property(self, Prop=defaultNamedNotOptArg):
		"""Get property. Use tagGRAPH_PROPERTIES enumeration"""
		return self._ApplyTypes_(3, 2, (12, 0), ((3, 1),), u'Property', None,Prop
			)

	# The method SetProperty is actually a property, but must be used as a method to correctly pass the arguments
	def SetProperty(self, Prop=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""Get property. Use tagGRAPH_PROPERTIES enumeration"""
		return self._oleobj_.InvokeTypes(3, LCID, 4, (24, 0), ((3, 1), (12, 1)),Prop
			, arg1)

	_prop_map_get_ = {
		"Filter": (4, 2, (9, 0), (), "Filter", None),
		"Module": (1, 2, (3, 0), (), "Module", None),
		"Text": (5, 2, (8, 0), (), "Text", None),
		"Type": (2, 2, (3, 0), (), "Type", None),
	}
	_prop_map_put_ = {
		"Filter": ((4, LCID, 4, 0),()),
	}

class IGroupingItem(DispatchBaseClass):
	"""A set of objects having the same value in a specific field."""
	CLSID = IID('{31FDA98D-C50C-416D-BB4F-9D5294896175}')
	coclass_clsid = IID('{904CED76-CF4A-4C85-BB23-2B4A9DCB1D6A}')

	_prop_map_get_ = {
		"FieldName": (1, 2, (8, 0), (), "FieldName", None),
		"FieldValue": (2, 2, (8, 0), (), "FieldValue", None),
		# Method 'GroupList' returns object of type 'IList'
		"GroupList": (5, 2, (9, 0), (), "GroupList", '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}'),
		"ItemCount": (3, 2, (3, 0), (), "ItemCount", None),
		# Method 'ItemList' returns object of type 'IList'
		"ItemList": (4, 2, (9, 0), (), "ItemList", '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}'),
	}
	_prop_map_put_ = {
	}

class IGroupingManager(DispatchBaseClass):
	"""Provides services for grouping."""
	CLSID = IID('{33CCFC63-CE02-47AB-9A11-BB2E0C324723}')
	coclass_clsid = IID('{F801F7A2-04DF-4DD3-8A5E-C0CC66E0595E}')

	def Clear(self):
		"""Deletes the data from the GroupItem list in memory."""
		return self._oleobj_.InvokeTypes(6, LCID, 1, (24, 0), (),)

	# The method Group is actually a property, but must be used as a method to correctly pass the arguments
	def Group(self, Name=defaultNamedNotOptArg):
		"""The grouping criteria."""
		return self._oleobj_.InvokeTypes(2, LCID, 2, (3, 0), ((8, 1),),Name
			)

	# Result is of type IList
	def GroupList(self):
		"""The list of top-level GroupItem objects."""
		ret = self._oleobj_.InvokeTypes(3, LCID, 1, (9, 0), (),)
		if ret is not None:
			ret = Dispatch(ret, u'GroupList', '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')
		return ret

	def IsClear(self):
		"""Checks if the GroupingManager contains any data. True if there is no data."""
		return self._oleobj_.InvokeTypes(7, LCID, 1, (11, 0), (),)

	# Result is of type IList
	def ItemList(self):
		"""The list of references to the members of the group."""
		ret = self._oleobj_.InvokeTypes(4, LCID, 1, (9, 0), (),)
		if ret is not None:
			ret = Dispatch(ret, u'ItemList', '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')
		return ret

	def Refresh(self):
		"""Fetches the GroupList according to the Filter and Group values, overwriting the list in memory."""
		return self._oleobj_.InvokeTypes(5, LCID, 1, (24, 0), (),)

	# The method SetGroup is actually a property, but must be used as a method to correctly pass the arguments
	def SetGroup(self, Name=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The grouping criteria."""
		return self._oleobj_.InvokeTypes(2, LCID, 4, (24, 0), ((8, 1), (3, 1)),Name
			, arg1)

	_prop_map_get_ = {
		"Filter": (1, 2, (9, 0), (), "Filter", None),
		"Text": (8, 2, (8, 0), (), "Text", None),
	}
	_prop_map_put_ = {
		"Filter": ((1, LCID, 4, 0),()),
		"Text": ((8, LCID, 4, 0),()),
	}

class IHierarchyFilter(DispatchBaseClass):
	"""IHierarchyFilter Interface"""
	CLSID = IID('{BE140C38-7D92-4C50-8C1C-A4E43C0FC329}')
	coclass_clsid = IID('{DCA4F956-F4B7-43BB-B544-58E56D0DEDC8}')

	# The method CaseSensitive is actually a property, but must be used as a method to correctly pass the arguments
	def CaseSensitive(self, FieldName=defaultNamedNotOptArg):
		"""Indicates if the filter for the specified field is case sensitive. The default is False."""
		return self._oleobj_.InvokeTypes(7, LCID, 2, (11, 0), ((8, 1),),FieldName
			)

	def Clear(self):
		"""Clears the current filter."""
		return self._oleobj_.InvokeTypes(3, LCID, 1, (24, 0), (),)

	# The method Filter is actually a property, but must be used as a method to correctly pass the arguments
	def Filter(self, FieldName=defaultNamedNotOptArg):
		"""The filter condition for the field specified by the FieldName."""
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(0, LCID, 2, (8, 0), ((8, 1),),FieldName
			)

	def GetXFilter(self, JoinEntities=defaultNamedNotOptArg, Inclusive=pythoncom.Missing):
		"""Gets the intersection filter specified by JoinEntities."""
		return self._ApplyTypes_(14, 1, (8, 0), ((8, 1), (16395, 2)), u'GetXFilter', None,JoinEntities
			, Inclusive)

	def IsClear(self):
		"""Checks whether the filter is clear, including cross-filter settings"""
		return self._oleobj_.InvokeTypes(15, LCID, 1, (11, 0), (),)

	# Result is of type IList
	def NewList(self):
		"""Creates a new list from current filter."""
		ret = self._oleobj_.InvokeTypes(4, LCID, 1, (9, 0), (),)
		if ret is not None:
			ret = Dispatch(ret, u'NewList', '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')
		return ret

	# The method Order is actually a property, but must be used as a method to correctly pass the arguments
	def Order(self, FieldName=defaultNamedNotOptArg):
		"""The position of the specified field in the list of the sort fields. (zero-based)"""
		return self._oleobj_.InvokeTypes(1, LCID, 2, (3, 0), ((8, 1),),FieldName
			)

	# The method OrderDirection is actually a property, but must be used as a method to correctly pass the arguments
	def OrderDirection(self, FieldName=defaultNamedNotOptArg):
		"""The sort order: TDOLE_ASCENDING or TDOLE_DESCENDING"""
		return self._oleobj_.InvokeTypes(2, LCID, 2, (2, 0), ((8, 1),),FieldName
			)

	def Refresh(self):
		"""Reads saved data and refreshes the fetched list according to the current filter, overwriting values in memory."""
		return self._oleobj_.InvokeTypes(6, LCID, 1, (24, 0), (),)

	# The method SetCaseSensitive is actually a property, but must be used as a method to correctly pass the arguments
	def SetCaseSensitive(self, FieldName=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""Indicates if the filter for the specified field is case sensitive. The default is False."""
		return self._oleobj_.InvokeTypes(7, LCID, 4, (24, 0), ((8, 1), (11, 1)),FieldName
			, arg1)

	# The method SetFilter is actually a property, but must be used as a method to correctly pass the arguments
	def SetFilter(self, FieldName=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The filter condition for the field specified by the FieldName."""
		return self._oleobj_.InvokeTypes(0, LCID, 4, (24, 0), ((8, 1), (8, 1)),FieldName
			, arg1)

	# The method SetOrder is actually a property, but must be used as a method to correctly pass the arguments
	def SetOrder(self, FieldName=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The position of the specified field in the list of the sort fields. (zero-based)"""
		return self._oleobj_.InvokeTypes(1, LCID, 4, (24, 0), ((8, 1), (3, 1)),FieldName
			, arg1)

	# The method SetOrderDirection is actually a property, but must be used as a method to correctly pass the arguments
	def SetOrderDirection(self, FieldName=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The sort order: TDOLE_ASCENDING or TDOLE_DESCENDING"""
		return self._oleobj_.InvokeTypes(2, LCID, 4, (24, 0), ((8, 1), (2, 1)),FieldName
			, arg1)

	# The method SetUFilter is actually a property, but must be used as a method to correctly pass the arguments
	def SetUFilter(self, EntityType=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The Union Filter according to the entity type."""
		return self._oleobj_.InvokeTypes(10, LCID, 4, (24, 0), ((8, 1), (8, 1)),EntityType
			, arg1)

	# The method SetXFilter is actually a property, but must be used as a method to correctly pass the arguments
	def SetXFilter(self, EntityType=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The Intersection Filter according to the entity type."""
		return self._oleobj_.InvokeTypes(9, LCID, 4, (24, 0), ((8, 1), (8, 1)),EntityType
			, arg1)

	# The method UFilter is actually a property, but must be used as a method to correctly pass the arguments
	def UFilter(self, EntityType=defaultNamedNotOptArg):
		"""The Union Filter according to the entity type."""
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(10, LCID, 2, (8, 0), ((8, 1),),EntityType
			)

	# The method XFilter is actually a property, but must be used as a method to correctly pass the arguments
	def XFilter(self, EntityType=defaultNamedNotOptArg):
		"""The Intersection Filter according to the entity type."""
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(9, LCID, 2, (8, 0), ((8, 1),),EntityType
			)

	_prop_map_get_ = {
		"DataType": (11, 2, (8, 0), (), "DataType", None),
		# Method 'Fields' returns object of type 'IList'
		"Fields": (5, 2, (9, 0), (), "Fields", '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}'),
		"KeepHierarchical": (16, 2, (11, 0), (), "KeepHierarchical", None),
		"Text": (8, 2, (8, 0), (), "Text", None),
		"_Text": (12, 2, (8, 0), (), "_Text", None),
	}
	_prop_map_put_ = {
		"KeepHierarchical": ((16, LCID, 4, 0),()),
		"Text": ((8, LCID, 4, 0),()),
		"_Text": ((12, LCID, 4, 0),()),
	}
	# Default method for this class is 'Filter'
	def __call__(self, FieldName=defaultNamedNotOptArg):
		"""The filter condition for the field specified by the FieldName."""
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(0, LCID, 2, (8, 0), ((8, 1),),FieldName
			)

	# str(ob) and int(ob) will use __call__
	def __unicode__(self, *args):
		try:
			return unicode(self.__call__(*args))
		except pythoncom.com_error:
			return repr(self)
	def __str__(self, *args):
		return str(self.__unicode__(*args))
	def __int__(self, *args):
		return int(self.__call__(*args))

class IHierarchySupportList(DispatchBaseClass):
	"""IHierarchySupportList Interface"""
	CLSID = IID('{DDE517F0-AF73-4327-A1BD-403E6A047B0A}')
	coclass_clsid = IID('{E214FA66-87AE-4088-8F25-75DBDCB21B7D}')

	def Add(self, vNew=defaultNamedNotOptArg):
		"""Adds a new item to the current list object."""
		return self._oleobj_.InvokeTypes(2, LCID, 1, (24, 0), ((12, 0),),vNew
			)

	def IndexOfItem(self, Item=defaultNamedNotOptArg):
		"""Gets the index of the passed object."""
		return self._oleobj_.InvokeTypes(11, LCID, 1, (3, 0), ((12, 0),),Item
			)

	def Insert(self, Pos=defaultNamedNotOptArg, vNew=defaultNamedNotOptArg):
		"""Inserts a new item at the specified position."""
		return self._oleobj_.InvokeTypes(5, LCID, 1, (24, 0), ((3, 0), (12, 0)),Pos
			, vNew)

	# The method IsInFilter is actually a property, but must be used as a method to correctly pass the arguments
	def IsInFilter(self, Index=defaultNamedNotOptArg):
		"""property Group"""
		return self._oleobj_.InvokeTypes(12, LCID, 2, (11, 0), ((3, 1),),Index
			)

	# The method Item is actually a property, but must be used as a method to correctly pass the arguments
	def Item(self, Index=defaultNamedNotOptArg):
		"""Gets an item by index. The index is one-based."""
		return self._ApplyTypes_(0, 2, (12, 0), ((3, 0),), u'Item', None,Index
			)

	def Post(self):
		"""Posts changed data for all items in list."""
		return self._oleobj_.InvokeTypes(9, LCID, 1, (24, 0), (),)

	# The method Ratio is actually a property, but must be used as a method to correctly pass the arguments
	def Ratio(self, Index=defaultNamedNotOptArg):
		"""The similarity ratio for the specified item."""
		return self._oleobj_.InvokeTypes(10, LCID, 2, (3, 0), ((3, 1),),Index
			)

	def Refresh(self):
		"""Reads saved list filter and data for all listed objects, overwriting values in memory."""
		return self._oleobj_.InvokeTypes(8, LCID, 1, (24, 0), (),)

	def Remove(self, Index=defaultNamedNotOptArg):
		"""Removes the specified item from the current list object."""
		return self._oleobj_.InvokeTypes(3, LCID, 1, (24, 0), ((3, 0),),Index
			)

	def Swap(self, Pos1=defaultNamedNotOptArg, Pos2=defaultNamedNotOptArg):
		"""Swaps the two list items specified by their positions."""
		return self._oleobj_.InvokeTypes(6, LCID, 1, (24, 0), ((3, 0), (3, 0)),Pos1
			, Pos2)

	_prop_map_get_ = {
		"Count": (1, 2, (3, 0), (), "Count", None),
		# Method 'Fields' returns object of type 'IList'
		"Fields": (7, 2, (9, 0), (), "Fields", '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}'),
	}
	_prop_map_put_ = {
	}
	# Default method for this class is 'Item'
	def __call__(self, Index=defaultNamedNotOptArg):
		"""Gets an item by index. The index is one-based."""
		return self._ApplyTypes_(0, 2, (12, 0), ((3, 0),), '__call__', None,Index
			)

	# str(ob) and int(ob) will use __call__
	def __unicode__(self, *args):
		try:
			return unicode(self.__call__(*args))
		except pythoncom.com_error:
			return repr(self)
	def __str__(self, *args):
		return str(self.__unicode__(*args))
	def __int__(self, *args):
		return int(self.__call__(*args))
	def __iter__(self):
		"Return a Python iterator for this object"
		ob = self._oleobj_.InvokeTypes(-4,LCID,2,(13, 10),())
		return win32com.client.util.Iterator(ob, None)
	def _NewEnum(self):
		"Create an enumerator from this object"
		return win32com.client.util.WrapEnum(self._oleobj_.InvokeTypes(-4,LCID,2,(13, 10),()),None)
	def __getitem__(self, index):
		"Allow this class to be accessed as a collection"
		if '_enum_' not in self.__dict__:
			self.__dict__['_enum_'] = self._NewEnum()
		return self._enum_.__getitem__(index)
	#This class has Count() property - allow len(ob) to provide this
	def __len__(self):
		return self._ApplyTypes_(*(1, 2, (3, 0), (), "Count", None))
	#This class has a __len__ - this is needed so 'if object:' always returns TRUE.
	def __nonzero__(self):
		return True

class IHistory(DispatchBaseClass):
	"""Supports retrieval of a list of history records for objects such as Test, Bug, Step, and others."""
	CLSID = IID('{15FBB8D4-7034-413E-A8F0-1E03B7FA4F0B}')
	coclass_clsid = IID('{1422200A-A929-430A-8683-6D8FDCA8BA89}')

	def ClearHistory(self, Filter=u''):
		"""Clears the history records selected by the filter."""
		return self._ApplyTypes_(3, 1, (24, 32), ((8, 49),), u'ClearHistory', None,Filter
			)

	# Result is of type IList
	def NewList(self, Filter=defaultNamedNotOptArg):
		"""Gets filtered list of history data records."""
		ret = self._oleobj_.InvokeTypes(1, LCID, 1, (9, 0), ((8, 0),),Filter
			)
		if ret is not None:
			ret = Dispatch(ret, u'NewList', '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')
		return ret

	_prop_map_get_ = {
		"Filter": (2, 2, (9, 0), (), "Filter", None),
	}
	_prop_map_put_ = {
	}

class IHistoryRecord(DispatchBaseClass):
	"""Represents a single history change."""
	CLSID = IID('{BCAE2958-39FA-4742-93B4-16D204CF67AB}')
	coclass_clsid = None

	_prop_map_get_ = {
		"ChangeDate": (2, 2, (7, 0), (), "ChangeDate", None),
		"Changer": (3, 2, (8, 0), (), "Changer", None),
		"FieldName": (1, 2, (8, 0), (), "FieldName", None),
		"ItemKey": (5, 2, (12, 0), (), "ItemKey", None),
		"NewValue": (4, 2, (12, 0), (), "NewValue", None),
	}
	_prop_map_put_ = {
	}

class IHistoryRecord2(DispatchBaseClass):
	"""IHistoryRecord2 Interface"""
	CLSID = IID('{E672B813-30DA-4429-97A7-A1616F0B7D2D}')
	coclass_clsid = IID('{89E2DE95-A1CA-47D1-ABCE-21D48A645E98}')

	_prop_map_get_ = {
		"ChangeDate": (2, 2, (7, 0), (), "ChangeDate", None),
		"Changer": (3, 2, (8, 0), (), "Changer", None),
		"FieldName": (1, 2, (8, 0), (), "FieldName", None),
		"ItemKey": (5, 2, (12, 0), (), "ItemKey", None),
		"NewValue": (4, 2, (12, 0), (), "NewValue", None),
		"OldValue": (6, 2, (12, 0), (), "OldValue", None),
	}
	_prop_map_put_ = {
	}

class IHost(DispatchBaseClass):
	"""Represents a single host server. The ID of a host is its name."""
	CLSID = IID('{F90E8FE5-EC4E-4EBA-8A81-32BCCEE3AB94}')
	coclass_clsid = IID('{6AF3BFA2-C914-4FF8-9531-A047745F2597}')

	# The method Field is actually a property, but must be used as a method to correctly pass the arguments
	def Field(self, FieldName=defaultNamedNotOptArg):
		"""The value of the specified field."""
		return self._ApplyTypes_(0, 2, (12, 0), ((8, 0),), u'Field', None,FieldName
			)

	def LockObject(self):
		"""Locks the object. Returns True if lock successful."""
		return self._oleobj_.InvokeTypes(2, LCID, 1, (11, 0), (),)

	def Post(self):
		"""Posts all changed values into database."""
		return self._oleobj_.InvokeTypes(6, LCID, 1, (24, 0), (),)

	def Refresh(self):
		"""Reads saved values, overwriting values in memory."""
		return self._oleobj_.InvokeTypes(7, LCID, 1, (24, 0), (),)

	# The method SetField is actually a property, but must be used as a method to correctly pass the arguments
	def SetField(self, FieldName=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The value of the specified field."""
		return self._oleobj_.InvokeTypes(0, LCID, 4, (24, 0), ((8, 0), (12, 1)),FieldName
			, arg1)

	def UnLockObject(self):
		"""Unlocks the object."""
		return self._oleobj_.InvokeTypes(3, LCID, 1, (24, 0), (),)

	def Undo(self):
		"""Undoes changes to field values that have not been posted."""
		return self._oleobj_.InvokeTypes(8, LCID, 1, (24, 0), (),)

	_prop_map_get_ = {
		"AutoPost": (5, 2, (11, 0), (), "AutoPost", None),
		"Description": (11, 2, (8, 0), (), "Description", None),
		"ID": (4, 2, (12, 0), (), "ID", None),
		"IsLocked": (1, 2, (11, 0), (), "IsLocked", None),
		"Modified": (9, 2, (11, 0), (), "Modified", None),
		"Name": (13, 2, (8, 0), (), "Name", None),
		"RexServer": (12, 2, (8, 0), (), "RexServer", None),
		"Virtual": (10, 2, (11, 0), (), "Virtual", None),
	}
	_prop_map_put_ = {
		"AutoPost": ((5, LCID, 4, 0),()),
		"Description": ((11, LCID, 4, 0),()),
	}
	# Default method for this class is 'Field'
	def __call__(self, FieldName=defaultNamedNotOptArg):
		"""The value of the specified field."""
		return self._ApplyTypes_(0, 2, (12, 0), ((8, 0),), '__call__', None,FieldName
			)

	# str(ob) and int(ob) will use __call__
	def __unicode__(self, *args):
		try:
			return unicode(self.__call__(*args))
		except pythoncom.com_error:
			return repr(self)
	def __str__(self, *args):
		return str(self.__unicode__(*args))
	def __int__(self, *args):
		return int(self.__call__(*args))

class IHostGroup(DispatchBaseClass):
	"""Services for managing a group of host servers."""
	CLSID = IID('{B3468A97-FD4C-4A05-9E49-1FD0ED7FD9E2}')
	coclass_clsid = IID('{78E51781-1777-4A05-A13F-1BB9FAFE3E48}')

	def AddHost(self, Val=defaultNamedNotOptArg):
		"""Adds a host to the group."""
		return self._oleobj_.InvokeTypes(15, LCID, 1, (24, 0), ((12, 1),),Val
			)

	# The method Field is actually a property, but must be used as a method to correctly pass the arguments
	def Field(self, FieldName=defaultNamedNotOptArg):
		"""The value of the specified field."""
		return self._ApplyTypes_(0, 2, (12, 0), ((8, 0),), u'Field', None,FieldName
			)

	def LockObject(self):
		"""Locks the object. Returns True if lock successful."""
		return self._oleobj_.InvokeTypes(2, LCID, 1, (11, 0), (),)

	# Result is of type IList
	def NewList(self):
		"""Gets a list of all hosts in the group."""
		ret = self._oleobj_.InvokeTypes(17, LCID, 1, (9, 0), (),)
		if ret is not None:
			ret = Dispatch(ret, u'NewList', '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')
		return ret

	def Post(self):
		"""Posts all changed values into database."""
		return self._oleobj_.InvokeTypes(6, LCID, 1, (24, 0), (),)

	def Refresh(self):
		"""Reads saved values, overwriting values in memory."""
		return self._oleobj_.InvokeTypes(7, LCID, 1, (24, 0), (),)

	def RemoveHost(self, Val=defaultNamedNotOptArg):
		"""Removes the specified host from the group."""
		return self._oleobj_.InvokeTypes(16, LCID, 1, (24, 0), ((12, 1),),Val
			)

	# The method SetField is actually a property, but must be used as a method to correctly pass the arguments
	def SetField(self, FieldName=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The value of the specified field."""
		return self._oleobj_.InvokeTypes(0, LCID, 4, (24, 0), ((8, 0), (12, 1)),FieldName
			, arg1)

	def UnLockObject(self):
		"""Unlocks the object."""
		return self._oleobj_.InvokeTypes(3, LCID, 1, (24, 0), (),)

	def Undo(self):
		"""Undoes changes to field values that have not been posted."""
		return self._oleobj_.InvokeTypes(8, LCID, 1, (24, 0), (),)

	_prop_map_get_ = {
		"Attachments": (12, 2, (9, 0), (), "Attachments", None),
		"AutoPost": (5, 2, (11, 0), (), "AutoPost", None),
		"HasAttachment": (13, 2, (11, 0), (), "HasAttachment", None),
		"History": (11, 2, (9, 0), (), "History", None),
		"ID": (4, 2, (12, 0), (), "ID", None),
		"IsLocked": (1, 2, (11, 0), (), "IsLocked", None),
		"Modified": (9, 2, (11, 0), (), "Modified", None),
		"Name": (14, 2, (8, 0), (), "Name", None),
		"Virtual": (10, 2, (11, 0), (), "Virtual", None),
	}
	_prop_map_put_ = {
		"AutoPost": ((5, LCID, 4, 0),()),
	}
	# Default method for this class is 'Field'
	def __call__(self, FieldName=defaultNamedNotOptArg):
		"""The value of the specified field."""
		return self._ApplyTypes_(0, 2, (12, 0), ((8, 0),), '__call__', None,FieldName
			)

	# str(ob) and int(ob) will use __call__
	def __unicode__(self, *args):
		try:
			return unicode(self.__call__(*args))
		except pythoncom.com_error:
			return repr(self)
	def __str__(self, *args):
		return str(self.__unicode__(*args))
	def __int__(self, *args):
		return int(self.__call__(*args))

class IHostGroupFactory(DispatchBaseClass):
	"""Services for managing definitions of groups of host servers."""
	CLSID = IID('{FA8C3437-3B5B-4246-B775-523DEEB2734A}')
	coclass_clsid = IID('{29823A4B-9739-4E55-972D-6091096E9090}')

	def AddItem(self, ItemData=defaultNamedNotOptArg):
		"""Creates a new item object."""
		ret = self._oleobj_.InvokeTypes(3, LCID, 1, (9, 0), ((12, 0),),ItemData
			)
		if ret is not None:
			ret = Dispatch(ret, u'AddItem', None)
		return ret

	# The method FetchLevel is actually a property, but must be used as a method to correctly pass the arguments
	def FetchLevel(self, FieldName=defaultNamedNotOptArg):
		"""The Fetch level for a field."""
		return self._oleobj_.InvokeTypes(7, LCID, 2, (2, 0), ((8, 1),),FieldName
			)

	# The method Item is actually a property, but must be used as a method to correctly pass the arguments
	def Item(self, ItemKey=defaultNamedNotOptArg):
		"""Gets an object managed by the factory by its key."""
		ret = self._oleobj_.InvokeTypes(0, LCID, 2, (9, 0), ((12, 0),),ItemKey
			)
		if ret is not None:
			ret = Dispatch(ret, u'Item', None)
		return ret

	# Result is of type IList
	def NewList(self, Filter=defaultNamedNotOptArg):
		"""Creates a list of objects according to the specified filter."""
		ret = self._oleobj_.InvokeTypes(1, LCID, 1, (9, 0), ((8, 0),),Filter
			)
		if ret is not None:
			ret = Dispatch(ret, u'NewList', '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')
		return ret

	def RemoveHost(self, Host=defaultNamedNotOptArg):
		"""Removes host from all groups."""
		return self._oleobj_.InvokeTypes(8, LCID, 1, (24, 0), ((12, 1),),Host
			)

	def RemoveItem(self, ItemKey=defaultNamedNotOptArg):
		"""Removes item from the database. Removal takes place immediately, without a Post."""
		return self._oleobj_.InvokeTypes(4, LCID, 1, (24, 0), ((12, 0),),ItemKey
			)

	# The method SetFetchLevel is actually a property, but must be used as a method to correctly pass the arguments
	def SetFetchLevel(self, FieldName=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The Fetch level for a field."""
		return self._oleobj_.InvokeTypes(7, LCID, 4, (24, 0), ((8, 1), (2, 1)),FieldName
			, arg1)

	_prop_map_get_ = {
		# Method 'Fields' returns object of type 'IList'
		"Fields": (2, 2, (9, 0), (), "Fields", '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}'),
		"Filter": (5, 2, (9, 0), (), "Filter", None),
		"History": (6, 2, (9, 0), (), "History", None),
	}
	_prop_map_put_ = {
	}
	# Default method for this class is 'Item'
	def __call__(self, ItemKey=defaultNamedNotOptArg):
		"""Gets an object managed by the factory by its key."""
		ret = self._oleobj_.InvokeTypes(0, LCID, 2, (9, 0), ((12, 0),),ItemKey
			)
		if ret is not None:
			ret = Dispatch(ret, '__call__', None)
		return ret

	# str(ob) and int(ob) will use __call__
	def __unicode__(self, *args):
		try:
			return unicode(self.__call__(*args))
		except pythoncom.com_error:
			return repr(self)
	def __str__(self, *args):
		return str(self.__unicode__(*args))
	def __int__(self, *args):
		return int(self.__call__(*args))

class IImport(DispatchBaseClass):
	"""IImport Interface"""
	CLSID = IID('{3A6DFEA9-5E15-4E8F-B480-2B025730F7BD}')
	coclass_clsid = IID('{CD3E5686-4B11-462F-9619-D2FA447DCE96}')

	def GetColumnNames(self, bstrpColumns=pythoncom.Missing, bstrResourceeName=defaultNamedNotOptArg):
		"""method GetColumnNames"""
		return self._ApplyTypes_(1, 1, (24, 0), ((16392, 2), (8, 1)), u'GetColumnNames', None,bstrpColumns
			, bstrResourceeName)

	def ImportData(self, bstrColumnNames=defaultNamedNotOptArg, bstrpData=defaultNamedNotOptArg):
		"""method ImportData"""
		return self._oleobj_.InvokeTypes(2, LCID, 1, (24, 0), ((8, 0), (16392, 0)),bstrColumnNames
			, bstrpData)

	_prop_map_get_ = {
	}
	_prop_map_put_ = {
	}

class ILink(DispatchBaseClass):
	"""Represents an association between a Bug and another entity."""
	CLSID = IID('{79CFC0C1-7BB6-48CE-BC47-8074DDBCA542}')
	coclass_clsid = IID('{75180DF9-EF06-4FB4-8EDB-5697713AB54C}')

	# The method Field is actually a property, but must be used as a method to correctly pass the arguments
	def Field(self, FieldName=defaultNamedNotOptArg):
		"""The value of the specified field."""
		return self._ApplyTypes_(0, 2, (12, 0), ((8, 0),), u'Field', None,FieldName
			)

	def LockObject(self):
		"""Locks the object. Returns True if lock successful."""
		return self._oleobj_.InvokeTypes(2, LCID, 1, (11, 0), (),)

	def Post(self):
		"""Posts all changed values into database."""
		return self._oleobj_.InvokeTypes(6, LCID, 1, (24, 0), (),)

	def Refresh(self):
		"""Reads saved values, overwriting values in memory."""
		return self._oleobj_.InvokeTypes(7, LCID, 1, (24, 0), (),)

	# The method SetField is actually a property, but must be used as a method to correctly pass the arguments
	def SetField(self, FieldName=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The value of the specified field."""
		return self._oleobj_.InvokeTypes(0, LCID, 4, (24, 0), ((8, 0), (12, 1)),FieldName
			, arg1)

	def UnLockObject(self):
		"""Unlocks the object."""
		return self._oleobj_.InvokeTypes(3, LCID, 1, (24, 0), (),)

	def Undo(self):
		"""Undoes changes to field values that have not been posted."""
		return self._oleobj_.InvokeTypes(8, LCID, 1, (24, 0), (),)

	_prop_map_get_ = {
		"AutoPost": (5, 2, (11, 0), (), "AutoPost", None),
		"Comment": (17, 2, (8, 0), (), "Comment", None),
		"CreatedBy": (14, 2, (8, 0), (), "CreatedBy", None),
		"CreationDate": (15, 2, (7, 0), (), "CreationDate", None),
		"ID": (4, 2, (12, 0), (), "ID", None),
		"IsLocked": (1, 2, (11, 0), (), "IsLocked", None),
		"LinkType": (16, 2, (8, 0), (), "LinkType", None),
		"LinkedByEntity": (13, 2, (9, 0), (), "LinkedByEntity", None),
		"Modified": (9, 2, (11, 0), (), "Modified", None),
		"SourceEntity": (11, 2, (9, 0), (), "SourceEntity", None),
		"TargetEntity": (12, 2, (9, 0), (), "TargetEntity", None),
		"Virtual": (10, 2, (11, 0), (), "Virtual", None),
	}
	_prop_map_put_ = {
		"AutoPost": ((5, LCID, 4, 0),()),
		"Comment": ((17, LCID, 4, 0),()),
		"CreatedBy": ((14, LCID, 4, 0),()),
		"CreationDate": ((15, LCID, 4, 0),()),
		"LinkType": ((16, LCID, 4, 0),()),
		"TargetEntity": ((12, LCID, 4, 0),()),
	}
	# Default method for this class is 'Field'
	def __call__(self, FieldName=defaultNamedNotOptArg):
		"""The value of the specified field."""
		return self._ApplyTypes_(0, 2, (12, 0), ((8, 0),), '__call__', None,FieldName
			)

	# str(ob) and int(ob) will use __call__
	def __unicode__(self, *args):
		try:
			return unicode(self.__call__(*args))
		except pythoncom.com_error:
			return repr(self)
	def __str__(self, *args):
		return str(self.__unicode__(*args))
	def __int__(self, *args):
		return int(self.__call__(*args))

class ILinkFactory(DispatchBaseClass):
	"""Services to manage Link objects."""
	CLSID = IID('{49B715FA-458E-46EA-A171-0E0BFB38B3AF}')
	coclass_clsid = IID('{2D52E7C5-B6DE-4D8D-B885-7C1F1DC509B8}')

	def AddItem(self, ItemData=defaultNamedNotOptArg):
		"""Creates a new item object."""
		ret = self._oleobj_.InvokeTypes(3, LCID, 1, (9, 0), ((12, 0),),ItemData
			)
		if ret is not None:
			ret = Dispatch(ret, u'AddItem', None)
		return ret

	# The method FetchLevel is actually a property, but must be used as a method to correctly pass the arguments
	def FetchLevel(self, FieldName=defaultNamedNotOptArg):
		"""The Fetch level for a field."""
		return self._oleobj_.InvokeTypes(7, LCID, 2, (2, 0), ((8, 1),),FieldName
			)

	# The method Item is actually a property, but must be used as a method to correctly pass the arguments
	def Item(self, ItemKey=defaultNamedNotOptArg):
		"""Gets an object managed by the factory by its key."""
		ret = self._oleobj_.InvokeTypes(0, LCID, 2, (9, 0), ((12, 0),),ItemKey
			)
		if ret is not None:
			ret = Dispatch(ret, u'Item', None)
		return ret

	# Result is of type IList
	def NewList(self, Filter=defaultNamedNotOptArg):
		"""Creates a list of objects according to the specified filter."""
		ret = self._oleobj_.InvokeTypes(1, LCID, 1, (9, 0), ((8, 0),),Filter
			)
		if ret is not None:
			ret = Dispatch(ret, u'NewList', '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')
		return ret

	def RemoveItem(self, ItemKey=defaultNamedNotOptArg):
		"""Removes item from the database. Removal takes place immediately, without a Post."""
		return self._oleobj_.InvokeTypes(4, LCID, 1, (24, 0), ((12, 0),),ItemKey
			)

	# The method SetFetchLevel is actually a property, but must be used as a method to correctly pass the arguments
	def SetFetchLevel(self, FieldName=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The Fetch level for a field."""
		return self._oleobj_.InvokeTypes(7, LCID, 4, (24, 0), ((8, 1), (2, 1)),FieldName
			, arg1)

	_prop_map_get_ = {
		# Method 'Fields' returns object of type 'IList'
		"Fields": (2, 2, (9, 0), (), "Fields", '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}'),
		"Filter": (5, 2, (9, 0), (), "Filter", None),
		"FullLinkage": (9, 2, (11, 0), (), "FullLinkage", None),
		"History": (6, 2, (9, 0), (), "History", None),
		"Owner": (8, 2, (9, 0), (), "Owner", None),
	}
	_prop_map_put_ = {
		"FullLinkage": ((9, LCID, 4, 0),()),
	}
	# Default method for this class is 'Item'
	def __call__(self, ItemKey=defaultNamedNotOptArg):
		"""Gets an object managed by the factory by its key."""
		ret = self._oleobj_.InvokeTypes(0, LCID, 2, (9, 0), ((12, 0),),ItemKey
			)
		if ret is not None:
			ret = Dispatch(ret, '__call__', None)
		return ret

	# str(ob) and int(ob) will use __call__
	def __unicode__(self, *args):
		try:
			return unicode(self.__call__(*args))
		except pythoncom.com_error:
			return repr(self)
	def __str__(self, *args):
		return str(self.__unicode__(*args))
	def __int__(self, *args):
		return int(self.__call__(*args))

class ILinkable(DispatchBaseClass):
	"""Services for managing links."""
	CLSID = IID('{9B1CCF47-8DC6-4B9D-AC24-35DD3361A175}')
	coclass_clsid = IID('{A94555C2-77AD-4F2D-B061-A9ED11AE9FE6}')

	_prop_map_get_ = {
		"BugLinkFactory": (1, 2, (9, 0), (), "BugLinkFactory", None),
		"HasLinkage": (3, 2, (11, 0), (), "HasLinkage", None),
		"HasOthersLinkage": (4, 2, (11, 0), (), "HasOthersLinkage", None),
		"LinkFactory": (2, 2, (9, 0), (), "LinkFactory", None),
	}
	_prop_map_put_ = {
	}

class IList(DispatchBaseClass):
	"""Services to create and maintain lists. Use any factory object to create any number of list instances for objects in the factory."""
	CLSID = IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')
	coclass_clsid = IID('{9007A7F1-AC71-4563-A943-CFF4051E7E3D}')

	def Add(self, vNew=defaultNamedNotOptArg):
		"""Adds a new item to the current list object."""
		return self._oleobj_.InvokeTypes(2, LCID, 1, (24, 0), ((12, 0),),vNew
			)

	def Insert(self, Pos=defaultNamedNotOptArg, vNew=defaultNamedNotOptArg):
		"""Inserts a new item at the specified position."""
		return self._oleobj_.InvokeTypes(5, LCID, 1, (24, 0), ((3, 0), (12, 0)),Pos
			, vNew)

	# The method Item is actually a property, but must be used as a method to correctly pass the arguments
	def Item(self, Index=defaultNamedNotOptArg):
		"""Gets an item by index. The index is one-based."""
		return self._ApplyTypes_(0, 2, (12, 0), ((3, 0),), u'Item', None,Index
			)

	def Remove(self, Index=defaultNamedNotOptArg):
		"""Removes the specified item from the current list object."""
		return self._oleobj_.InvokeTypes(3, LCID, 1, (24, 0), ((3, 0),),Index
			)

	def Swap(self, Pos1=defaultNamedNotOptArg, Pos2=defaultNamedNotOptArg):
		"""Swaps the two list items specified by their positions."""
		return self._oleobj_.InvokeTypes(6, LCID, 1, (24, 0), ((3, 0), (3, 0)),Pos1
			, Pos2)

	_prop_map_get_ = {
		"Count": (1, 2, (3, 0), (), "Count", None),
	}
	_prop_map_put_ = {
	}
	# Default method for this class is 'Item'
	def __call__(self, Index=defaultNamedNotOptArg):
		"""Gets an item by index. The index is one-based."""
		return self._ApplyTypes_(0, 2, (12, 0), ((3, 0),), '__call__', None,Index
			)

	# str(ob) and int(ob) will use __call__
	def __unicode__(self, *args):
		try:
			return unicode(self.__call__(*args))
		except pythoncom.com_error:
			return repr(self)
	def __str__(self, *args):
		return str(self.__unicode__(*args))
	def __int__(self, *args):
		return int(self.__call__(*args))
	def __iter__(self):
		"Return a Python iterator for this object"
		ob = self._oleobj_.InvokeTypes(-4,LCID,2,(13, 10),())
		return win32com.client.util.Iterator(ob, None)
	def _NewEnum(self):
		"Create an enumerator from this object"
		return win32com.client.util.WrapEnum(self._oleobj_.InvokeTypes(-4,LCID,2,(13, 10),()),None)
	def __getitem__(self, index):
		"Allow this class to be accessed as a collection"
		if '_enum_' not in self.__dict__:
			self.__dict__['_enum_'] = self._NewEnum()
		return self._enum_.__getitem__(index)
	#This class has Count() property - allow len(ob) to provide this
	def __len__(self):
		return self._ApplyTypes_(*(1, 2, (3, 0), (), "Count", None))
	#This class has a __len__ - this is needed so 'if object:' always returns TRUE.
	def __nonzero__(self):
		return True

class IMultiValue(DispatchBaseClass):
	"""Represents several values stored in a single field."""
	CLSID = IID('{EB180CC0-6FDE-4A1E-A68E-F106EEED5E15}')
	coclass_clsid = IID('{D2990AC2-106D-4889-B299-D6D4223649E6}')

	_prop_map_get_ = {
		# Method 'List' returns object of type 'IList'
		"List": (2, 2, (9, 0), (), "List", '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}'),
		"Text": (1, 2, (8, 0), (), "Text", None),
	}
	_prop_map_put_ = {
		"List": ((2, LCID, 4, 0),()),
		"Text": ((1, LCID, 4, 0),()),
	}

class IObjectLockingSupport(DispatchBaseClass):
	"""Services to support locking objects."""
	CLSID = IID('{260EDCFF-BF72-4146-BC41-5B2280652ED0}')
	coclass_clsid = IID('{606FDBBB-55A7-48D6-BF70-06A7CE08049C}')

	def LockObject(self):
		"""Locks the object. Returns True if lock successful."""
		return self._oleobj_.InvokeTypes(2, LCID, 1, (11, 0), (),)

	def UnLockObject(self):
		"""Unlocks the object."""
		return self._oleobj_.InvokeTypes(3, LCID, 1, (24, 0), (),)

	_prop_map_get_ = {
		"IsLocked": (1, 2, (11, 0), (), "IsLocked", None),
	}
	_prop_map_put_ = {
	}

class IOnExecEventSchedulerActionParams(DispatchBaseClass):
	"""Handles the action parameter list."""
	CLSID = IID('{CBD80CD0-1961-4191-A318-ABC50AB2ACD9}')
	coclass_clsid = IID('{F64C065B-D9BF-41CB-91AD-C1672A8220DF}')

	# The method Parameter is actually a property, but must be used as a method to correctly pass the arguments
	def Parameter(self, Index=defaultNamedNotOptArg):
		"""Gets or set the value of the specified action parameter."""
		return self._ApplyTypes_(2, 2, (12, 0), ((3, 1),), u'Parameter', None,Index
			)

	# The method SetParameter is actually a property, but must be used as a method to correctly pass the arguments
	def SetParameter(self, Index=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""Gets or set the value of the specified action parameter."""
		return self._oleobj_.InvokeTypes(2, LCID, 4, (24, 0), ((3, 1), (12, 1)),Index
			, arg1)

	_prop_map_get_ = {
		"OnExecEventSchedulerAction": (1, 2, (3, 0), (), "OnExecEventSchedulerAction", None),
	}
	_prop_map_put_ = {
	}

class IOnExecEventSchedulerRestartParams(DispatchBaseClass):
	"""Defines behavior after completion of a test set."""
	CLSID = IID('{3120287D-98B1-4D49-9BC5-3324555D8D04}')
	coclass_clsid = IID('{A57D9723-040E-44C9-829C-64439CC526C1}')

	# The method Parameter is actually a property, but must be used as a method to correctly pass the arguments
	def Parameter(self, Index=defaultNamedNotOptArg):
		"""Gets or set the value of the specified action parameter."""
		return self._ApplyTypes_(2, 2, (12, 0), ((3, 1),), u'Parameter', None,Index
			)

	# The method SetParameter is actually a property, but must be used as a method to correctly pass the arguments
	def SetParameter(self, Index=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""Gets or set the value of the specified action parameter."""
		return self._oleobj_.InvokeTypes(2, LCID, 4, (24, 0), ((3, 1), (12, 1)),Index
			, arg1)

	_prop_map_get_ = {
		"CleanupTest": (4, 2, (12, 0), (), "CleanupTest", None),
		"NumberOfRetries": (3, 2, (3, 0), (), "NumberOfRetries", None),
		"OnExecEventSchedulerAction": (1, 2, (3, 0), (), "OnExecEventSchedulerAction", None),
	}
	_prop_map_put_ = {
		"CleanupTest": ((4, LCID, 4, 0),()),
		"NumberOfRetries": ((3, LCID, 4, 0),()),
	}

class IParam(DispatchBaseClass):
	"""Represents parameters for a Command object."""
	CLSID = IID('{AE1410CC-D940-4356-A926-6DF6C1F45AED}')
	coclass_clsid = None

	def AddParam(self, Name=defaultNamedNotOptArg, InitialValue=defaultNamedNotOptArg):
		"""Adds new parameter to Command object."""
		return self._oleobj_.InvokeTypes(5, LCID, 1, (24, 0), ((8, 0), (12, 0)),Name
			, InitialValue)

	def DeleteParam(self, Key=defaultNamedNotOptArg):
		"""Deletes the specified parameter from Command object."""
		return self._oleobj_.InvokeTypes(6, LCID, 1, (24, 0), ((12, 0),),Key
			)

	def DeleteParams(self):
		"""Deletes all parameters from Command object."""
		return self._oleobj_.InvokeTypes(7, LCID, 1, (24, 0), (),)

	# The method ParamIndex is actually a property, but must be used as a method to correctly pass the arguments
	def ParamIndex(self, Name=defaultNamedNotOptArg):
		"""The parameter index by parameter name."""
		return self._oleobj_.InvokeTypes(4, LCID, 2, (3, 0), ((8, 0),),Name
			)

	# The method ParamName is actually a property, but must be used as a method to correctly pass the arguments
	def ParamName(self, Index=defaultNamedNotOptArg):
		"""The parameter name by parameter index. The index is 0-based."""
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(3, LCID, 2, (8, 0), ((3, 0),),Index
			)

	# The method ParamType is actually a property, but must be used as a method to correctly pass the arguments
	def ParamType(self, Index=defaultNamedNotOptArg):
		"""The data type of the specifed parameter.  The index is 0-based."""
		return self._oleobj_.InvokeTypes(8, LCID, 2, (3, 0), ((3, 0),),Index
			)

	# The method ParamValue is actually a property, but must be used as a method to correctly pass the arguments
	def ParamValue(self, Key=defaultNamedNotOptArg):
		"""The parameter value."""
		return self._ApplyTypes_(2, 2, (12, 0), ((12, 0),), u'ParamValue', None,Key
			)

	# The method SetParamValue is actually a property, but must be used as a method to correctly pass the arguments
	def SetParamValue(self, Key=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The parameter value."""
		return self._oleobj_.InvokeTypes(2, LCID, 4, (24, 0), ((12, 0), (12, 1)),Key
			, arg1)

	_prop_map_get_ = {
		"Count": (1, 2, (3, 0), (), "Count", None),
	}
	_prop_map_put_ = {
	}
	#This class has Count() property - allow len(ob) to provide this
	def __len__(self):
		return self._ApplyTypes_(*(1, 2, (3, 0), (), "Count", None))
	#This class has a __len__ - this is needed so 'if object:' always returns TRUE.
	def __nonzero__(self):
		return True

class IProductInfo(DispatchBaseClass):
	"""Information about the current version."""
	CLSID = IID('{2C8D6469-AE61-4F01-801E-6B2087216786}')
	coclass_clsid = IID('{73D2B96A-0A3F-41CA-A8F2-E371220B4C63}')

	_prop_map_get_ = {
		"BPTVersion": (2, 2, (3, 0), (), "BPTVersion", None),
		"QCVersion": (1, 2, (3, 0), ((16387, 2), (16387, 2)), "QCVersion", None),
	}
	_prop_map_put_ = {
	}

class IProjectProperties(DispatchBaseClass):
	"""Global project parameters and settings."""
	CLSID = IID('{59A79946-0678-4E59-B4B0-9967E4314CCA}')
	coclass_clsid = IID('{31905CFC-D317-4B8D-85DC-F63D8D3E6E28}')

	# The method IsParam is actually a property, but must be used as a method to correctly pass the arguments
	def IsParam(self, ParamName=defaultNamedNotOptArg):
		"""Checks if a parameter of this name exists."""
		return self._oleobj_.InvokeTypes(4, LCID, 2, (11, 0), ((8, 1),),ParamName
			)

	# The method ParamName is actually a property, but must be used as a method to correctly pass the arguments
	def ParamName(self, ParamIndex=defaultNamedNotOptArg):
		"""Gets the parameter name."""
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(3, LCID, 2, (8, 0), ((3, 1),),ParamIndex
			)

	# The method ParamValue is actually a property, but must be used as a method to correctly pass the arguments
	def ParamValue(self, vParam=defaultNamedNotOptArg):
		"""The value of the parameter."""
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(2, LCID, 2, (8, 0), ((12, 1),),vParam
			)

	_prop_map_get_ = {
		"Count": (1, 2, (3, 0), (), "Count", None),
	}
	_prop_map_put_ = {
	}
	#This class has Count() property - allow len(ob) to provide this
	def __len__(self):
		return self._ApplyTypes_(*(1, 2, (3, 0), (), "Count", None))
	#This class has a __len__ - this is needed so 'if object:' always returns TRUE.
	def __nonzero__(self):
		return True

class IRTParam(DispatchBaseClass):
	"""IRTParam Interface"""
	CLSID = IID('{E0C8D290-50AF-4811-A5FB-3ED5B78B99F3}')
	coclass_clsid = IID('{5666ABB3-0FC1-4C22-A4B0-8D43CD1E54A3}')

	# The method Field is actually a property, but must be used as a method to correctly pass the arguments
	def Field(self, FieldName=defaultNamedNotOptArg):
		"""The value of the specified field."""
		return self._ApplyTypes_(0, 2, (12, 0), ((8, 0),), u'Field', None,FieldName
			)

	def LockObject(self):
		"""Locks the object. Returns True if lock successful."""
		return self._oleobj_.InvokeTypes(2, LCID, 1, (11, 0), (),)

	def Post(self):
		"""Posts all changed values into database."""
		return self._oleobj_.InvokeTypes(6, LCID, 1, (24, 0), (),)

	def Refresh(self):
		"""Reads saved values, overwriting values in memory."""
		return self._oleobj_.InvokeTypes(7, LCID, 1, (24, 0), (),)

	# The method SetField is actually a property, but must be used as a method to correctly pass the arguments
	def SetField(self, FieldName=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The value of the specified field."""
		return self._oleobj_.InvokeTypes(0, LCID, 4, (24, 0), ((8, 0), (12, 1)),FieldName
			, arg1)

	def UnLockObject(self):
		"""Unlocks the object."""
		return self._oleobj_.InvokeTypes(3, LCID, 1, (24, 0), (),)

	def Undo(self):
		"""Undoes changes to field values that have not been posted."""
		return self._oleobj_.InvokeTypes(8, LCID, 1, (24, 0), (),)

	_prop_map_get_ = {
		"AutoPost": (5, 2, (11, 0), (), "AutoPost", None),
		"Desc": (16, 2, (8, 0), (), "Desc", None),
		"ID": (4, 2, (12, 0), (), "ID", None),
		"IsLocked": (1, 2, (11, 0), (), "IsLocked", None),
		"Modified": (9, 2, (11, 0), (), "Modified", None),
		"Name": (14, 2, (8, 0), (), "Name", None),
		"Order": (18, 2, (3, 0), (), "Order", None),
		"Value": (15, 2, (8, 0), (), "Value", None),
		"ValueType": (17, 2, (8, 0), (), "ValueType", None),
		"Virtual": (10, 2, (11, 0), (), "Virtual", None),
	}
	_prop_map_put_ = {
		"AutoPost": ((5, LCID, 4, 0),()),
		"Desc": ((16, LCID, 4, 0),()),
		"Name": ((14, LCID, 4, 0),()),
		"Order": ((18, LCID, 4, 0),()),
		"Value": ((15, LCID, 4, 0),()),
		"ValueType": ((17, LCID, 4, 0),()),
	}
	# Default method for this class is 'Field'
	def __call__(self, FieldName=defaultNamedNotOptArg):
		"""The value of the specified field."""
		return self._ApplyTypes_(0, 2, (12, 0), ((8, 0),), '__call__', None,FieldName
			)

	# str(ob) and int(ob) will use __call__
	def __unicode__(self, *args):
		try:
			return unicode(self.__call__(*args))
		except pythoncom.com_error:
			return repr(self)
	def __str__(self, *args):
		return str(self.__unicode__(*args))
	def __int__(self, *args):
		return int(self.__call__(*args))

class IRecordset(DispatchBaseClass):
	"""Represents the entire set of records resulting from an executed command. At any given time, the Recordset object refers to a single record within the record set as the current record."""
	CLSID = IID('{025854DA-9D81-40E8-853D-F4EA33073A77}')
	coclass_clsid = IID('{4FEDB030-AE1C-407C-8732-0A9E042E9B27}')

	def Clone(self):
		"""Creates a duplicate of this Recordset object."""
		ret = self._oleobj_.InvokeTypes(15, LCID, 1, (9, 0), (),)
		if ret is not None:
			ret = Dispatch(ret, u'Clone', None)
		return ret

	# The method ColIndex is actually a property, but must be used as a method to correctly pass the arguments
	def ColIndex(self, Name=defaultNamedNotOptArg):
		"""Gets the index (zero-based) of a Recordset column specified by column name."""
		return self._oleobj_.InvokeTypes(5, LCID, 2, (3, 0), ((8, 0),),Name
			)

	# The method ColName is actually a property, but must be used as a method to correctly pass the arguments
	def ColName(self, Index=defaultNamedNotOptArg):
		"""The name of the Recordset column specified by Index (zero-based)."""
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(4, LCID, 2, (8, 0), ((3, 0),),Index
			)

	# The method ColSize is actually a property, but must be used as a method to correctly pass the arguments
	def ColSize(self, Index=defaultNamedNotOptArg):
		"""The physical size as defined in the database of the field specified by Index (zero-based)."""
		return self._oleobj_.InvokeTypes(2, LCID, 2, (3, 0), ((3, 0),),Index
			)

	# The method ColType is actually a property, but must be used as a method to correctly pass the arguments
	def ColType(self, Index=defaultNamedNotOptArg):
		"""The data type of column."""
		return self._oleobj_.InvokeTypes(3, LCID, 2, (3, 0), ((3, 0),),Index
			)

	# The method FieldValue is actually a property, but must be used as a method to correctly pass the arguments
	def FieldValue(self, FieldKey=defaultNamedNotOptArg):
		"""The value for the specified field. FieldKey is either the column index (0-based) or column name."""
		return self._ApplyTypes_(0, 2, (12, 0), ((12, 0),), u'FieldValue', None,FieldKey
			)

	def First(self):
		"""Moves to the first record and makes it the current record."""
		return self._oleobj_.InvokeTypes(11, LCID, 1, (24, 0), (),)

	def Last(self):
		"""Moves to the last record and makes it the current record."""
		return self._oleobj_.InvokeTypes(14, LCID, 1, (24, 0), (),)

	def Next(self):
		"""Moves to the next record and makes it the current record."""
		return self._oleobj_.InvokeTypes(12, LCID, 1, (24, 0), (),)

	def Prev(self):
		"""Moves to the previous record and makes it the current record."""
		return self._oleobj_.InvokeTypes(13, LCID, 1, (24, 0), (),)

	def Refresh(self, Range=0, Low=defaultNamedNotOptArg, High=defaultNamedNotOptArg):
		"""Refreshes the Recordset, overwriting values in memory."""
		return self._oleobj_.InvokeTypes(16, LCID, 1, (24, 0), ((3, 49), (3, 17), (3, 17)),Range
			, Low, High)

	# The method SetFieldValue is actually a property, but must be used as a method to correctly pass the arguments
	def SetFieldValue(self, FieldKey=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The value for the specified field. FieldKey is either the column index (0-based) or column name."""
		return self._oleobj_.InvokeTypes(0, LCID, 4, (24, 0), ((12, 0), (12, 1)),FieldKey
			, arg1)

	_prop_map_get_ = {
		"BOR": (7, 2, (11, 0), (), "BOR", None),
		"CacheSize": (9, 2, (3, 0), (), "CacheSize", None),
		"ColCount": (1, 2, (3, 0), (), "ColCount", None),
		"EOR": (8, 2, (11, 0), (), "EOR", None),
		"Position": (10, 2, (3, 0), (), "Position", None),
		"RecordCount": (6, 2, (3, 0), (), "RecordCount", None),
	}
	_prop_map_put_ = {
		"CacheSize": ((9, LCID, 4, 0),()),
		"Position": ((10, LCID, 4, 0),()),
	}
	# Default method for this class is 'FieldValue'
	def __call__(self, FieldKey=defaultNamedNotOptArg):
		"""The value for the specified field. FieldKey is either the column index (0-based) or column name."""
		return self._ApplyTypes_(0, 2, (12, 0), ((12, 0),), '__call__', None,FieldKey
			)

	# str(ob) and int(ob) will use __call__
	def __unicode__(self, *args):
		try:
			return unicode(self.__call__(*args))
		except pythoncom.com_error:
			return repr(self)
	def __str__(self, *args):
		return str(self.__unicode__(*args))
	def __int__(self, *args):
		return int(self.__call__(*args))

class IReq(DispatchBaseClass):
	"""Represents a requirement for which testing must be performed."""
	CLSID = IID('{4592C936-2524-4D05-9978-95901EDE0A54}')
	coclass_clsid = IID('{A94555C2-77AD-4F2D-B061-A9ED11AE9FE6}')

	def AddCoverage(self, TestId=defaultNamedNotOptArg, Order=defaultNamedNotOptArg):
		"""Assigns a test to cover this requirement."""
		return self._oleobj_.InvokeTypes(24, LCID, 1, (3, 0), ((3, 1), (3, 1)),TestId
			, Order)

	def AddCoverageByFilter(self, SubjectID=defaultNamedNotOptArg, Order=defaultNamedNotOptArg, TestFilter=defaultNamedNotOptArg):
		"""Adds the tests from the specified subject that match the input filter to the list of tests that cover the current requirement."""
		return self._oleobj_.InvokeTypes(35, LCID, 1, (3, 0), ((3, 1), (3, 1), (8, 1)),SubjectID
			, Order, TestFilter)

	def AddCoverageEx(self, SubjectID=defaultNamedNotOptArg, Order=defaultNamedNotOptArg):
		"""Assigns all the tests in the specified subject folder to cover this current requirement."""
		return self._oleobj_.InvokeTypes(25, LCID, 1, (3, 0), ((3, 1), (3, 1)),SubjectID
			, Order)

	# The method Field is actually a property, but must be used as a method to correctly pass the arguments
	def Field(self, FieldName=defaultNamedNotOptArg):
		"""The value of the specified field."""
		return self._ApplyTypes_(0, 2, (12, 0), ((8, 0),), u'Field', None,FieldName
			)

	# Result is of type IList
	def GetCoverList(self, Recursive=False):
		"""Gets a list of the tests that cover this requirement."""
		ret = self._oleobj_.InvokeTypes(27, LCID, 1, (9, 0), ((11, 49),),Recursive
			)
		if ret is not None:
			ret = Dispatch(ret, u'GetCoverList', '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')
		return ret

	# Result is of type IList
	def GetCoverListByFilter(self, TestFilter=defaultNamedNotOptArg, Recursive=False):
		"""Gets an list of all tests covering the current requirement that match the filter."""
		ret = self._oleobj_.InvokeTypes(36, LCID, 1, (9, 0), ((8, 1), (11, 49)),TestFilter
			, Recursive)
		if ret is not None:
			ret = Dispatch(ret, u'GetCoverListByFilter', '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')
		return ret

	def LockObject(self):
		"""Locks the object. Returns True if lock successful."""
		return self._oleobj_.InvokeTypes(2, LCID, 1, (11, 0), (),)

	def Mail(self, SendTo=defaultNamedNotOptArg, SendCc=u'', Option=0, Subject=u''
			, Comment=u''):
		"""Mails the IBaseFieldExMail field item."""
		return self._ApplyTypes_(14, 1, (24, 32), ((8, 1), (8, 49), (3, 49), (8, 49), (8, 49)), u'Mail', None,SendTo
			, SendCc, Option, Subject, Comment)

	def Move(self, NewFatherId=defaultNamedNotOptArg, NewOrder=defaultNamedNotOptArg):
		"""Moves a requirement to being a child of a specified father in the requirements tree. """
		return self._oleobj_.InvokeTypes(31, LCID, 1, (24, 0), ((3, 0), (3, 0)),NewFatherId
			, NewOrder)

	def Post(self):
		"""Posts all changed values into database."""
		return self._oleobj_.InvokeTypes(6, LCID, 1, (24, 0), (),)

	def Refresh(self):
		"""Reads saved values, overwriting values in memory."""
		return self._oleobj_.InvokeTypes(7, LCID, 1, (24, 0), (),)

	def RemoveCoverage(self, vTest=defaultNamedNotOptArg, Recursive=False):
		"""Removes a test from the list of tests that cover this requirement."""
		return self._oleobj_.InvokeTypes(26, LCID, 1, (24, 0), ((12, 1), (11, 49)),vTest
			, Recursive)

	# The method SetField is actually a property, but must be used as a method to correctly pass the arguments
	def SetField(self, FieldName=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The value of the specified field."""
		return self._oleobj_.InvokeTypes(0, LCID, 4, (24, 0), ((8, 0), (12, 1)),FieldName
			, arg1)

	def UnLockObject(self):
		"""Unlocks the object."""
		return self._oleobj_.InvokeTypes(3, LCID, 1, (24, 0), (),)

	def Undo(self):
		"""Undoes changes to field values that have not been posted."""
		return self._oleobj_.InvokeTypes(8, LCID, 1, (24, 0), (),)

	_prop_map_get_ = {
		"Attachments": (12, 2, (9, 0), (), "Attachments", None),
		"Author": (19, 2, (8, 0), (), "Author", None),
		"AutoPost": (5, 2, (11, 0), (), "AutoPost", None),
		"Comment": (16, 2, (8, 0), (), "Comment", None),
		"Count": (20, 2, (3, 0), (), "Count", None),
		"HasAttachment": (13, 2, (11, 0), (), "HasAttachment", None),
		"HasCoverage": (30, 2, (11, 0), (), "HasCoverage", None),
		"History": (11, 2, (9, 0), (), "History", None),
		"ID": (4, 2, (12, 0), (), "ID", None),
		"IsFolder": (32, 2, (11, 0), (), "IsFolder", None),
		"IsLocked": (1, 2, (11, 0), (), "IsLocked", None),
		"Modified": (9, 2, (11, 0), (), "Modified", None),
		"Name": (15, 2, (8, 0), (), "Name", None),
		"Paragraph": (28, 2, (8, 0), (), "Paragraph", None),
		"Path": (29, 2, (8, 0), (), "Path", None),
		"Priority": (21, 2, (8, 0), (), "Priority", None),
		"Product": (17, 2, (8, 0), (), "Product", None),
		"ReqCoverageStatus": (34, 2, (9, 0), (), "ReqCoverageStatus", None),
		"ReqSummaryStatus": (33, 2, (9, 0), (), "ReqSummaryStatus", None),
		"Reviewed": (23, 2, (8, 0), (), "Reviewed", None),
		"Status": (22, 2, (8, 0), (), "Status", None),
		"Type": (18, 2, (8, 0), (), "Type", None),
		"Virtual": (10, 2, (11, 0), (), "Virtual", None),
	}
	_prop_map_put_ = {
		"Author": ((19, LCID, 4, 0),()),
		"AutoPost": ((5, LCID, 4, 0),()),
		"Comment": ((16, LCID, 4, 0),()),
		"IsFolder": ((32, LCID, 4, 0),()),
		"Name": ((15, LCID, 4, 0),()),
		"Priority": ((21, LCID, 4, 0),()),
		"Product": ((17, LCID, 4, 0),()),
		"Reviewed": ((23, LCID, 4, 0),()),
		"Type": ((18, LCID, 4, 0),()),
	}
	# Default method for this class is 'Field'
	def __call__(self, FieldName=defaultNamedNotOptArg):
		"""The value of the specified field."""
		return self._ApplyTypes_(0, 2, (12, 0), ((8, 0),), '__call__', None,FieldName
			)

	# str(ob) and int(ob) will use __call__
	def __unicode__(self, *args):
		try:
			return unicode(self.__call__(*args))
		except pythoncom.com_error:
			return repr(self)
	def __str__(self, *args):
		return str(self.__unicode__(*args))
	def __int__(self, *args):
		return int(self.__call__(*args))
	#This class has Count() property - allow len(ob) to provide this
	def __len__(self):
		return self._ApplyTypes_(*(20, 2, (3, 0), (), "Count", None))
	#This class has a __len__ - this is needed so 'if object:' always returns TRUE.
	def __nonzero__(self):
		return True

class IReqCoverageFactory(DispatchBaseClass):
	"""Services for managing requirement coverage."""
	CLSID = IID('{3F004630-2A1E-4E7A-9133-6A0EA826C48F}')
	coclass_clsid = IID('{AAFCDC5B-5E1A-4E41-829A-225C14BB0C88}')

	def AddItem(self, ItemData=defaultNamedNotOptArg):
		"""Creates a new item object."""
		ret = self._oleobj_.InvokeTypes(3, LCID, 1, (9, 0), ((12, 0),),ItemData
			)
		if ret is not None:
			ret = Dispatch(ret, u'AddItem', None)
		return ret

	# The method FetchLevel is actually a property, but must be used as a method to correctly pass the arguments
	def FetchLevel(self, FieldName=defaultNamedNotOptArg):
		"""The Fetch level for a field."""
		return self._oleobj_.InvokeTypes(7, LCID, 2, (2, 0), ((8, 1),),FieldName
			)

	# The method Item is actually a property, but must be used as a method to correctly pass the arguments
	def Item(self, ItemKey=defaultNamedNotOptArg):
		"""Gets an object managed by the factory by its key."""
		ret = self._oleobj_.InvokeTypes(0, LCID, 2, (9, 0), ((12, 0),),ItemKey
			)
		if ret is not None:
			ret = Dispatch(ret, u'Item', None)
		return ret

	# Result is of type IList
	def NewList(self, Filter=defaultNamedNotOptArg):
		"""Creates a list of objects according to the specified filter."""
		ret = self._oleobj_.InvokeTypes(1, LCID, 1, (9, 0), ((8, 0),),Filter
			)
		if ret is not None:
			ret = Dispatch(ret, u'NewList', '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')
		return ret

	def RemoveItem(self, ItemKey=defaultNamedNotOptArg):
		"""Removes item from the database. Removal takes place immediately, without a Post."""
		return self._oleobj_.InvokeTypes(4, LCID, 1, (24, 0), ((12, 0),),ItemKey
			)

	# The method SetFetchLevel is actually a property, but must be used as a method to correctly pass the arguments
	def SetFetchLevel(self, FieldName=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The Fetch level for a field."""
		return self._oleobj_.InvokeTypes(7, LCID, 4, (24, 0), ((8, 1), (2, 1)),FieldName
			, arg1)

	_prop_map_get_ = {
		# Method 'Fields' returns object of type 'IList'
		"Fields": (2, 2, (9, 0), (), "Fields", '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}'),
		"Filter": (5, 2, (9, 0), (), "Filter", None),
		"FullCoverage": (8, 2, (11, 0), (), "FullCoverage", None),
		"History": (6, 2, (9, 0), (), "History", None),
	}
	_prop_map_put_ = {
		"FullCoverage": ((8, LCID, 4, 0),()),
	}
	# Default method for this class is 'Item'
	def __call__(self, ItemKey=defaultNamedNotOptArg):
		"""Gets an object managed by the factory by its key."""
		ret = self._oleobj_.InvokeTypes(0, LCID, 2, (9, 0), ((12, 0),),ItemKey
			)
		if ret is not None:
			ret = Dispatch(ret, '__call__', None)
		return ret

	# str(ob) and int(ob) will use __call__
	def __unicode__(self, *args):
		try:
			return unicode(self.__call__(*args))
		except pythoncom.com_error:
			return repr(self)
	def __str__(self, *args):
		return str(self.__unicode__(*args))
	def __int__(self, *args):
		return int(self.__call__(*args))

class IReqCoverageStatus(DispatchBaseClass):
	"""Requirement Coverage Status"""
	CLSID = IID('{650BCE02-3C9A-4C22-ABCF-C2872EED73EB}')
	coclass_clsid = IID('{54770024-A5FB-4710-8A9E-64A9A0DCBCFA}')

	# The method SummaryStatus is actually a property, but must be used as a method to correctly pass the arguments
	def SummaryStatus(self, Status=defaultNamedNotOptArg):
		"""The number of child nodes with the specified status."""
		return self._oleobj_.InvokeTypes(2, LCID, 2, (3, 0), ((8, 1),),Status
			)

	_prop_map_get_ = {
		# Method 'PossibleStatuses' returns object of type 'IList'
		"PossibleStatuses": (1, 2, (9, 0), (), "PossibleStatuses", '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}'),
		"TotalSummaryNodes": (3, 2, (3, 0), (), "TotalSummaryNodes", None),
	}
	_prop_map_put_ = {
	}

class IReqFactory(DispatchBaseClass):
	"""Services for managing requirements."""
	CLSID = IID('{833093EE-C983-46F7-88BF-DE5D7E2FCBBD}')
	coclass_clsid = None

	def AddItem(self, ItemData=defaultNamedNotOptArg):
		"""Creates a new item object."""
		ret = self._oleobj_.InvokeTypes(3, LCID, 1, (9, 0), ((12, 0),),ItemData
			)
		if ret is not None:
			ret = Dispatch(ret, u'AddItem', None)
		return ret

	def BuildPerfGraph(self, GroupByField=u'', SumOfField=u'', MaxCols=0, Filter=defaultNamedNotOptArg
			, FRDate=defaultNamedNotOptArg, ForceRefresh=False, ShowFullPath=False):
		"""Builds Performance Graph."""
		return self._ApplyTypes_(14, 1, (9, 32), ((8, 49), (8, 49), (3, 49), (12, 17), (12, 17), (11, 49), (11, 49)), u'BuildPerfGraph', None,GroupByField
			, SumOfField, MaxCols, Filter, FRDate, ForceRefresh
			, ShowFullPath)

	def BuildPerfGraphEx(self, GroupByField=u'', SumOfField=u'', MaxCols=0, Filter=defaultNamedNotOptArg
			, FRDate=defaultNamedNotOptArg, ForceRefresh=False, ShowFullPath=False, ShowNullParents=False):
		"""Builds Performance Graph."""
		return self._ApplyTypes_(18, 1, (9, 32), ((8, 49), (8, 49), (3, 49), (12, 17), (12, 17), (11, 49), (11, 49), (11, 49)), u'BuildPerfGraphEx', None,GroupByField
			, SumOfField, MaxCols, Filter, FRDate, ForceRefresh
			, ShowFullPath, ShowNullParents)

	def BuildProgressGraph(self, GroupByField=u'', SumOfField=u'', ByHistory=True, MajorSkip=0
			, MinorSkip=1, MaxCols=0, Filter=defaultNamedNotOptArg, FRDate=defaultNamedNotOptArg, ForceRefresh=False
			, ShowFullPath=False):
		"""Builds Req progress graph."""
		return self._ApplyTypes_(12, 1, (9, 32), ((8, 49), (8, 49), (11, 49), (3, 49), (3, 49), (3, 49), (12, 17), (12, 17), (11, 49), (11, 49)), u'BuildProgressGraph', None,GroupByField
			, SumOfField, ByHistory, MajorSkip, MinorSkip, MaxCols
			, Filter, FRDate, ForceRefresh, ShowFullPath)

	def BuildProgressGraphEx(self, GroupByField=u'', SumOfField=u'', ByHistory=True, MajorSkip=0
			, MinorSkip=1, MaxCols=0, Filter=defaultNamedNotOptArg, FRDate=defaultNamedNotOptArg, ForceRefresh=False
			, ShowFullPath=False, ShowNullParents=False):
		"""Builds Requirement progress graph. Extends BuildProgressGraph with ability to display Null parent folders."""
		return self._ApplyTypes_(16, 1, (9, 32), ((8, 49), (8, 49), (11, 49), (3, 49), (3, 49), (3, 49), (12, 17), (12, 17), (11, 49), (11, 49), (11, 49)), u'BuildProgressGraphEx', None,GroupByField
			, SumOfField, ByHistory, MajorSkip, MinorSkip, MaxCols
			, Filter, FRDate, ForceRefresh, ShowFullPath, ShowNullParents
			)

	def BuildSummaryGraph(self, XAxisField=u'', GroupByField=u'', SumOfField=u'', MaxCols=0
			, Filter=defaultNamedNotOptArg, ForceRefresh=False, ShowFullPath=False):
		"""Builds Req summary graph."""
		return self._ApplyTypes_(11, 1, (9, 32), ((8, 49), (8, 49), (8, 49), (3, 49), (12, 17), (11, 49), (11, 49)), u'BuildSummaryGraph', None,XAxisField
			, GroupByField, SumOfField, MaxCols, Filter, ForceRefresh
			, ShowFullPath)

	def BuildSummaryGraphEx(self, XAxisField=u'', GroupByField=u'', SumOfField=u'', MaxCols=0
			, Filter=defaultNamedNotOptArg, ForceRefresh=False, ShowFullPath=False, ShowNullParents=False):
		"""Builds Req summary graph. Extends BuildSummaryGraph with ability to display Null parent folders."""
		return self._ApplyTypes_(15, 1, (9, 32), ((8, 49), (8, 49), (8, 49), (3, 49), (12, 17), (11, 49), (11, 49), (11, 49)), u'BuildSummaryGraphEx', None,XAxisField
			, GroupByField, SumOfField, MaxCols, Filter, ForceRefresh
			, ShowFullPath, ShowNullParents)

	def BuildTrendGraph(self, GroupByField=u'', SumOfField=u'', MaxCols=0, Filter=defaultNamedNotOptArg
			, FRDate=defaultNamedNotOptArg, ForceRefresh=False, ShowFullPath=False):
		"""Creates graph that shows the number of defect status changes over time."""
		return self._ApplyTypes_(13, 1, (9, 32), ((8, 49), (8, 49), (3, 49), (12, 17), (12, 17), (11, 49), (11, 49)), u'BuildTrendGraph', None,GroupByField
			, SumOfField, MaxCols, Filter, FRDate, ForceRefresh
			, ShowFullPath)

	def BuildTrendGraphEx(self, GroupByField=u'', SumOfField=u'', MaxCols=0, Filter=defaultNamedNotOptArg
			, FRDate=defaultNamedNotOptArg, ForceRefresh=False, ShowFullPath=False, ShowNullParents=False):
		"""Creates graph that shows the number of defect status changes over time."""
		return self._ApplyTypes_(17, 1, (9, 32), ((8, 49), (8, 49), (3, 49), (12, 17), (12, 17), (11, 49), (11, 49), (11, 49)), u'BuildTrendGraphEx', None,GroupByField
			, SumOfField, MaxCols, Filter, FRDate, ForceRefresh
			, ShowFullPath, ShowNullParents)

	# The method FetchLevel is actually a property, but must be used as a method to correctly pass the arguments
	def FetchLevel(self, FieldName=defaultNamedNotOptArg):
		"""The Fetch level for a field."""
		return self._oleobj_.InvokeTypes(7, LCID, 2, (2, 0), ((8, 1),),FieldName
			)

	# Result is of type IList
	def Find(self, StartRootID=defaultNamedNotOptArg, FieldName=defaultNamedNotOptArg, Pattern=defaultNamedNotOptArg, Mode=0
			, Limit=100):
		"""Gets a list of requirements containing the Pattern value in specified field."""
		ret = self._oleobj_.InvokeTypes(10, LCID, 1, (9, 0), ((3, 1), (8, 1), (8, 1), (3, 49), (3, 49)),StartRootID
			, FieldName, Pattern, Mode, Limit)
		if ret is not None:
			ret = Dispatch(ret, u'Find', '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')
		return ret

	# Result is of type IList
	def GetChildrenList(self, FatherID=defaultNamedNotOptArg):
		"""Gets list of child requirements."""
		ret = self._oleobj_.InvokeTypes(9, LCID, 1, (9, 0), ((3, 1),),FatherID
			)
		if ret is not None:
			ret = Dispatch(ret, u'GetChildrenList', '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')
		return ret

	# The method Item is actually a property, but must be used as a method to correctly pass the arguments
	def Item(self, ItemKey=defaultNamedNotOptArg):
		"""Gets an object managed by the factory by its key."""
		ret = self._oleobj_.InvokeTypes(0, LCID, 2, (9, 0), ((12, 0),),ItemKey
			)
		if ret is not None:
			ret = Dispatch(ret, u'Item', None)
		return ret

	def Mail(self, Items=defaultNamedNotOptArg, SendTo=defaultNamedNotOptArg, SendCc=u'', Option=0
			, Subject=u'', Comment=u''):
		"""Mails the list of IBase Factory Items. 'Items' is a list of ID numbers."""
		return self._ApplyTypes_(8, 1, (24, 32), ((12, 1), (8, 1), (8, 49), (3, 49), (8, 49), (8, 49)), u'Mail', None,Items
			, SendTo, SendCc, Option, Subject, Comment
			)

	# Result is of type IList
	def NewList(self, Filter=defaultNamedNotOptArg):
		"""Creates a list of objects according to the specified filter."""
		ret = self._oleobj_.InvokeTypes(1, LCID, 1, (9, 0), ((8, 0),),Filter
			)
		if ret is not None:
			ret = Dispatch(ret, u'NewList', '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')
		return ret

	def RemoveItem(self, ItemKey=defaultNamedNotOptArg):
		"""Removes item from the database. Removal takes place immediately, without a Post."""
		return self._oleobj_.InvokeTypes(4, LCID, 1, (24, 0), ((12, 0),),ItemKey
			)

	# The method SetFetchLevel is actually a property, but must be used as a method to correctly pass the arguments
	def SetFetchLevel(self, FieldName=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The Fetch level for a field."""
		return self._oleobj_.InvokeTypes(7, LCID, 4, (24, 0), ((8, 1), (2, 1)),FieldName
			, arg1)

	_prop_map_get_ = {
		# Method 'Fields' returns object of type 'IList'
		"Fields": (2, 2, (9, 0), (), "Fields", '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}'),
		"Filter": (5, 2, (9, 0), (), "Filter", None),
		"History": (6, 2, (9, 0), (), "History", None),
	}
	_prop_map_put_ = {
	}
	# Default method for this class is 'Item'
	def __call__(self, ItemKey=defaultNamedNotOptArg):
		"""Gets an object managed by the factory by its key."""
		ret = self._oleobj_.InvokeTypes(0, LCID, 2, (9, 0), ((12, 0),),ItemKey
			)
		if ret is not None:
			ret = Dispatch(ret, '__call__', None)
		return ret

	# str(ob) and int(ob) will use __call__
	def __unicode__(self, *args):
		try:
			return unicode(self.__call__(*args))
		except pythoncom.com_error:
			return repr(self)
	def __str__(self, *args):
		return str(self.__unicode__(*args))
	def __int__(self, *args):
		return int(self.__call__(*args))

class IReqFactory2(DispatchBaseClass):
	"""Services for managing requirements."""
	CLSID = IID('{1F73BDFF-AC15-47FF-8264-88ADE733AAB7}')
	coclass_clsid = IID('{F4E856D4-FCD7-11D4-9D8A-0001029DEAF5}')

	def AddItem(self, ItemData=defaultNamedNotOptArg):
		"""Creates a new item object."""
		ret = self._oleobj_.InvokeTypes(3, LCID, 1, (9, 0), ((12, 0),),ItemData
			)
		if ret is not None:
			ret = Dispatch(ret, u'AddItem', None)
		return ret

	def BuildPerfGraph(self, GroupByField=u'', SumOfField=u'', MaxCols=0, Filter=defaultNamedNotOptArg
			, FRDate=defaultNamedNotOptArg, ForceRefresh=False, ShowFullPath=False):
		"""Builds Performance Graph."""
		return self._ApplyTypes_(14, 1, (9, 32), ((8, 49), (8, 49), (3, 49), (12, 17), (12, 17), (11, 49), (11, 49)), u'BuildPerfGraph', None,GroupByField
			, SumOfField, MaxCols, Filter, FRDate, ForceRefresh
			, ShowFullPath)

	def BuildPerfGraphEx(self, GroupByField=u'', SumOfField=u'', MaxCols=0, Filter=defaultNamedNotOptArg
			, FRDate=defaultNamedNotOptArg, ForceRefresh=False, ShowFullPath=False, ShowNullParents=False):
		"""Builds Performance Graph."""
		return self._ApplyTypes_(18, 1, (9, 32), ((8, 49), (8, 49), (3, 49), (12, 17), (12, 17), (11, 49), (11, 49), (11, 49)), u'BuildPerfGraphEx', None,GroupByField
			, SumOfField, MaxCols, Filter, FRDate, ForceRefresh
			, ShowFullPath, ShowNullParents)

	def BuildProgressGraph(self, GroupByField=u'', SumOfField=u'', ByHistory=True, MajorSkip=0
			, MinorSkip=1, MaxCols=0, Filter=defaultNamedNotOptArg, FRDate=defaultNamedNotOptArg, ForceRefresh=False
			, ShowFullPath=False):
		"""Builds Req progress graph."""
		return self._ApplyTypes_(12, 1, (9, 32), ((8, 49), (8, 49), (11, 49), (3, 49), (3, 49), (3, 49), (12, 17), (12, 17), (11, 49), (11, 49)), u'BuildProgressGraph', None,GroupByField
			, SumOfField, ByHistory, MajorSkip, MinorSkip, MaxCols
			, Filter, FRDate, ForceRefresh, ShowFullPath)

	def BuildProgressGraphEx(self, GroupByField=u'', SumOfField=u'', ByHistory=True, MajorSkip=0
			, MinorSkip=1, MaxCols=0, Filter=defaultNamedNotOptArg, FRDate=defaultNamedNotOptArg, ForceRefresh=False
			, ShowFullPath=False, ShowNullParents=False):
		"""Builds Requirement progress graph. Extends BuildProgressGraph with ability to display Null parent folders."""
		return self._ApplyTypes_(16, 1, (9, 32), ((8, 49), (8, 49), (11, 49), (3, 49), (3, 49), (3, 49), (12, 17), (12, 17), (11, 49), (11, 49), (11, 49)), u'BuildProgressGraphEx', None,GroupByField
			, SumOfField, ByHistory, MajorSkip, MinorSkip, MaxCols
			, Filter, FRDate, ForceRefresh, ShowFullPath, ShowNullParents
			)

	def BuildSummaryGraph(self, XAxisField=u'', GroupByField=u'', SumOfField=u'', MaxCols=0
			, Filter=defaultNamedNotOptArg, ForceRefresh=False, ShowFullPath=False):
		"""Builds Req summary graph."""
		return self._ApplyTypes_(11, 1, (9, 32), ((8, 49), (8, 49), (8, 49), (3, 49), (12, 17), (11, 49), (11, 49)), u'BuildSummaryGraph', None,XAxisField
			, GroupByField, SumOfField, MaxCols, Filter, ForceRefresh
			, ShowFullPath)

	def BuildSummaryGraphEx(self, XAxisField=u'', GroupByField=u'', SumOfField=u'', MaxCols=0
			, Filter=defaultNamedNotOptArg, ForceRefresh=False, ShowFullPath=False, ShowNullParents=False):
		"""Builds Req summary graph. Extends BuildSummaryGraph with ability to display Null parent folders."""
		return self._ApplyTypes_(15, 1, (9, 32), ((8, 49), (8, 49), (8, 49), (3, 49), (12, 17), (11, 49), (11, 49), (11, 49)), u'BuildSummaryGraphEx', None,XAxisField
			, GroupByField, SumOfField, MaxCols, Filter, ForceRefresh
			, ShowFullPath, ShowNullParents)

	def BuildTrendGraph(self, GroupByField=u'', SumOfField=u'', MaxCols=0, Filter=defaultNamedNotOptArg
			, FRDate=defaultNamedNotOptArg, ForceRefresh=False, ShowFullPath=False):
		"""Creates graph that shows the number of defect status changes over time."""
		return self._ApplyTypes_(13, 1, (9, 32), ((8, 49), (8, 49), (3, 49), (12, 17), (12, 17), (11, 49), (11, 49)), u'BuildTrendGraph', None,GroupByField
			, SumOfField, MaxCols, Filter, FRDate, ForceRefresh
			, ShowFullPath)

	def BuildTrendGraphEx(self, GroupByField=u'', SumOfField=u'', MaxCols=0, Filter=defaultNamedNotOptArg
			, FRDate=defaultNamedNotOptArg, ForceRefresh=False, ShowFullPath=False, ShowNullParents=False):
		"""Creates graph that shows the number of defect status changes over time."""
		return self._ApplyTypes_(17, 1, (9, 32), ((8, 49), (8, 49), (3, 49), (12, 17), (12, 17), (11, 49), (11, 49), (11, 49)), u'BuildTrendGraphEx', None,GroupByField
			, SumOfField, MaxCols, Filter, FRDate, ForceRefresh
			, ShowFullPath, ShowNullParents)

	# The method FetchLevel is actually a property, but must be used as a method to correctly pass the arguments
	def FetchLevel(self, FieldName=defaultNamedNotOptArg):
		"""The Fetch level for a field."""
		return self._oleobj_.InvokeTypes(7, LCID, 2, (2, 0), ((8, 1),),FieldName
			)

	# Result is of type IList
	def Find(self, StartRootID=defaultNamedNotOptArg, FieldName=defaultNamedNotOptArg, Pattern=defaultNamedNotOptArg, Mode=0
			, Limit=100):
		"""Gets a list of requirements containing the Pattern value in specified field."""
		ret = self._oleobj_.InvokeTypes(10, LCID, 1, (9, 0), ((3, 1), (8, 1), (8, 1), (3, 49), (3, 49)),StartRootID
			, FieldName, Pattern, Mode, Limit)
		if ret is not None:
			ret = Dispatch(ret, u'Find', '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')
		return ret

	# Result is of type IList
	def GetChildrenList(self, FatherID=defaultNamedNotOptArg):
		"""Gets list of child requirements."""
		ret = self._oleobj_.InvokeTypes(9, LCID, 1, (9, 0), ((3, 1),),FatherID
			)
		if ret is not None:
			ret = Dispatch(ret, u'GetChildrenList', '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')
		return ret

	# Result is of type IList
	def GetFilteredChildrenList(self, FatherID=defaultNamedNotOptArg, Filter=0):
		"""Gets filtered list of child requirements."""
		ret = self._oleobj_.InvokeTypes(19, LCID, 1, (9, 0), ((3, 1), (9, 49)),FatherID
			, Filter)
		if ret is not None:
			ret = Dispatch(ret, u'GetFilteredChildrenList', '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')
		return ret

	# The method Item is actually a property, but must be used as a method to correctly pass the arguments
	def Item(self, ItemKey=defaultNamedNotOptArg):
		"""Gets an object managed by the factory by its key."""
		ret = self._oleobj_.InvokeTypes(0, LCID, 2, (9, 0), ((12, 0),),ItemKey
			)
		if ret is not None:
			ret = Dispatch(ret, u'Item', None)
		return ret

	def Mail(self, Items=defaultNamedNotOptArg, SendTo=defaultNamedNotOptArg, SendCc=u'', Option=0
			, Subject=u'', Comment=u''):
		"""Mails the list of IBase Factory Items. 'Items' is a list of ID numbers."""
		return self._ApplyTypes_(8, 1, (24, 32), ((12, 1), (8, 1), (8, 49), (3, 49), (8, 49), (8, 49)), u'Mail', None,Items
			, SendTo, SendCc, Option, Subject, Comment
			)

	# Result is of type IList
	def NewList(self, Filter=defaultNamedNotOptArg):
		"""Creates a list of objects according to the specified filter."""
		ret = self._oleobj_.InvokeTypes(1, LCID, 1, (9, 0), ((8, 0),),Filter
			)
		if ret is not None:
			ret = Dispatch(ret, u'NewList', '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')
		return ret

	def RemoveItem(self, ItemKey=defaultNamedNotOptArg):
		"""Removes item from the database. Removal takes place immediately, without a Post."""
		return self._oleobj_.InvokeTypes(4, LCID, 1, (24, 0), ((12, 0),),ItemKey
			)

	# The method SetFetchLevel is actually a property, but must be used as a method to correctly pass the arguments
	def SetFetchLevel(self, FieldName=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The Fetch level for a field."""
		return self._oleobj_.InvokeTypes(7, LCID, 4, (24, 0), ((8, 1), (2, 1)),FieldName
			, arg1)

	_prop_map_get_ = {
		# Method 'Fields' returns object of type 'IList'
		"Fields": (2, 2, (9, 0), (), "Fields", '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}'),
		"Filter": (5, 2, (9, 0), (), "Filter", None),
		"History": (6, 2, (9, 0), (), "History", None),
	}
	_prop_map_put_ = {
	}
	# Default method for this class is 'Item'
	def __call__(self, ItemKey=defaultNamedNotOptArg):
		"""Gets an object managed by the factory by its key."""
		ret = self._oleobj_.InvokeTypes(0, LCID, 2, (9, 0), ((12, 0),),ItemKey
			)
		if ret is not None:
			ret = Dispatch(ret, '__call__', None)
		return ret

	# str(ob) and int(ob) will use __call__
	def __unicode__(self, *args):
		try:
			return unicode(self.__call__(*args))
		except pythoncom.com_error:
			return repr(self)
	def __str__(self, *args):
		return str(self.__unicode__(*args))
	def __int__(self, *args):
		return int(self.__call__(*args))

class IReqSummaryStatus(DispatchBaseClass):
	"""IReqSummaryStatus Interface"""
	CLSID = IID('{CCD9F62A-3899-4233-9C2C-A25BFF1AF041}')
	coclass_clsid = IID('{6C760530-757D-4D83-B07A-DD5F29559457}')

	# The method SummaryStatus is actually a property, but must be used as a method to correctly pass the arguments
	def SummaryStatus(self, StatusName=defaultNamedNotOptArg):
		"""The number of child nodes with the specified status."""
		return self._oleobj_.InvokeTypes(2, LCID, 2, (3, 0), ((8, 1),),StatusName
			)

	_prop_map_get_ = {
		# Method 'PossibleStatuses' returns object of type 'IList'
		"PossibleStatuses": (1, 2, (9, 0), (), "PossibleStatuses", '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}'),
		"TotalSummaryNodes": (3, 2, (3, 0), (), "TotalSummaryNodes", None),
	}
	_prop_map_put_ = {
	}

class IRule(DispatchBaseClass):
	"""Represents a rule for generating an alert."""
	CLSID = IID('{5186B0F4-DAC2-4ABD-B248-8ED6E852C40D}')
	coclass_clsid = IID('{B2FDDFE1-6019-4444-9EE6-479FA0E554A2}')

	def Post(self):
		"""Saves the changes to the Rules object to the server."""
		return self._oleobj_.InvokeTypes(5, LCID, 1, (24, 0), (),)

	_prop_map_get_ = {
		"Description": (4, 2, (8, 0), (), "Description", None),
		"ID": (1, 2, (3, 0), (), "ID", None),
		"IsActive": (2, 2, (11, 0), (), "IsActive", None),
		"ToMail": (3, 2, (11, 0), (), "ToMail", None),
	}
	_prop_map_put_ = {
		"IsActive": ((2, LCID, 4, 0),()),
		"ToMail": ((3, LCID, 4, 0),()),
	}

class IRuleManager(DispatchBaseClass):
	"""Services for managing the notification rules."""
	CLSID = IID('{FCBDBAD5-380A-43F5-B4D4-4CD988B0C924}')
	coclass_clsid = IID('{2C6320EE-DE3C-46E3-9BCB-7204F3ADEC19}')

	def GetRule(self, ID=defaultNamedNotOptArg):
		"""Gets a traceablity notification rule by its ID."""
		ret = self._oleobj_.InvokeTypes(1, LCID, 1, (9, 0), ((3, 1),),ID
			)
		if ret is not None:
			ret = Dispatch(ret, u'GetRule', None)
		return ret

	# Result is of type IList
	# The method GetRules is actually a property, but must be used as a method to correctly pass the arguments
	def GetRules(self, NeedRefresh=False):
		"""A list of all the Rule objects."""
		ret = self._oleobj_.InvokeTypes(2, LCID, 2, (9, 0), ((11, 49),),NeedRefresh
			)
		if ret is not None:
			ret = Dispatch(ret, u'GetRules', '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')
		return ret

	_prop_map_get_ = {
		# Method 'Rules' returns object of type 'IList'
		"Rules": (2, 2, (9, 0), ((11, 49),), "Rules", '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}'),
	}
	_prop_map_put_ = {
	}

class IRun(DispatchBaseClass):
	"""Represents a test run."""
	CLSID = IID('{34023178-4154-4B16-80A4-6C610096648A}')
	coclass_clsid = None

	def CancelRun(self):
		"""Cancels the run."""
		return self._oleobj_.InvokeTypes(26, LCID, 1, (24, 0), (),)

	def CopyDesignSteps(self):
		"""Copies design steps into the test run of an executed test."""
		return self._oleobj_.InvokeTypes(20, LCID, 1, (24, 0), (),)

	def CopyStepsToTest(self):
		"""Copies all run execution steps, including new added steps, into the design steps of the corresponding planning test."""
		return self._oleobj_.InvokeTypes(21, LCID, 1, (24, 0), (),)

	# The method Field is actually a property, but must be used as a method to correctly pass the arguments
	def Field(self, FieldName=defaultNamedNotOptArg):
		"""The value of the specified field."""
		return self._ApplyTypes_(0, 2, (12, 0), ((8, 0),), u'Field', None,FieldName
			)

	def LockObject(self):
		"""Locks the object. Returns True if lock successful."""
		return self._oleobj_.InvokeTypes(2, LCID, 1, (11, 0), (),)

	# The method Params is actually a property, but must be used as a method to correctly pass the arguments
	def Params(self, SourceMode=defaultNamedNotOptArg):
		"""The step parameters of this run."""
		ret = self._oleobj_.InvokeTypes(23, LCID, 2, (9, 0), ((3, 1),),SourceMode
			)
		if ret is not None:
			ret = Dispatch(ret, u'Params', None)
		return ret

	def Post(self):
		"""Posts all changed values into database."""
		return self._oleobj_.InvokeTypes(6, LCID, 1, (24, 0), (),)

	def Refresh(self):
		"""Reads saved values, overwriting values in memory."""
		return self._oleobj_.InvokeTypes(7, LCID, 1, (24, 0), (),)

	def ResolveStepsParameters(self, UpdateLocalCache=True):
		"""Updates the texts of the run's steps by resolving the parameter values at run time."""
		return self._oleobj_.InvokeTypes(24, LCID, 1, (24, 0), ((11, 49),),UpdateLocalCache
			)

	# The method SetField is actually a property, but must be used as a method to correctly pass the arguments
	def SetField(self, FieldName=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The value of the specified field."""
		return self._oleobj_.InvokeTypes(0, LCID, 4, (24, 0), ((8, 0), (12, 1)),FieldName
			, arg1)

	def UnLockObject(self):
		"""Unlocks the object."""
		return self._oleobj_.InvokeTypes(3, LCID, 1, (24, 0), (),)

	def Undo(self):
		"""Undoes changes to field values that have not been posted."""
		return self._oleobj_.InvokeTypes(8, LCID, 1, (24, 0), (),)

	_prop_map_get_ = {
		"Attachments": (12, 2, (9, 0), (), "Attachments", None),
		"AutoPost": (5, 2, (11, 0), (), "AutoPost", None),
		"ExtendedStorage": (19, 2, (9, 0), (), "ExtendedStorage", None),
		"HasAttachment": (13, 2, (11, 0), (), "HasAttachment", None),
		"History": (11, 2, (9, 0), (), "History", None),
		"ID": (4, 2, (12, 0), (), "ID", None),
		"IsLocked": (1, 2, (11, 0), (), "IsLocked", None),
		"Modified": (9, 2, (11, 0), (), "Modified", None),
		"Name": (15, 2, (8, 0), (), "Name", None),
		"ResultLocation": (14, 2, (8, 0), (), "ResultLocation", None),
		"Status": (16, 2, (8, 0), (), "Status", None),
		"StepFactory": (17, 2, (9, 0), (), "StepFactory", None),
		"TestId": (18, 2, (3, 0), (), "TestId", None),
		"TestInstance": (25, 2, (3, 0), (), "TestInstance", None),
		"TestSetID": (22, 2, (3, 0), (), "TestSetID", None),
		"Virtual": (10, 2, (11, 0), (), "Virtual", None),
	}
	_prop_map_put_ = {
		"AutoPost": ((5, LCID, 4, 0),()),
		"Name": ((15, LCID, 4, 0),()),
		"Status": ((16, LCID, 4, 0),()),
	}
	# Default method for this class is 'Field'
	def __call__(self, FieldName=defaultNamedNotOptArg):
		"""The value of the specified field."""
		return self._ApplyTypes_(0, 2, (12, 0), ((8, 0),), '__call__', None,FieldName
			)

	# str(ob) and int(ob) will use __call__
	def __unicode__(self, *args):
		try:
			return unicode(self.__call__(*args))
		except pythoncom.com_error:
			return repr(self)
	def __str__(self, *args):
		return str(self.__unicode__(*args))
	def __int__(self, *args):
		return int(self.__call__(*args))

class IRun2(DispatchBaseClass):
	"""IRun2 Interface"""
	CLSID = IID('{16EF2BF4-8509-475E-B34E-3BF99C221221}')
	coclass_clsid = IID('{5AB19406-71C9-40F0-AFD3-4EF9A77FE648}')

	def CancelRun(self):
		"""Cancels the run."""
		return self._oleobj_.InvokeTypes(26, LCID, 1, (24, 0), (),)

	def CopyDesignSteps(self):
		"""Copies design steps into the test run of an executed test."""
		return self._oleobj_.InvokeTypes(20, LCID, 1, (24, 0), (),)

	def CopyStepsToTest(self):
		"""Copies all run execution steps, including new added steps, into the design steps of the corresponding planning test."""
		return self._oleobj_.InvokeTypes(21, LCID, 1, (24, 0), (),)

	# The method Field is actually a property, but must be used as a method to correctly pass the arguments
	def Field(self, FieldName=defaultNamedNotOptArg):
		"""The value of the specified field."""
		return self._ApplyTypes_(0, 2, (12, 0), ((8, 0),), u'Field', None,FieldName
			)

	def LockObject(self):
		"""Locks the object. Returns True if lock successful."""
		return self._oleobj_.InvokeTypes(2, LCID, 1, (11, 0), (),)

	# The method Params is actually a property, but must be used as a method to correctly pass the arguments
	def Params(self, SourceMode=defaultNamedNotOptArg):
		"""The step parameters of this run."""
		ret = self._oleobj_.InvokeTypes(23, LCID, 2, (9, 0), ((3, 1),),SourceMode
			)
		if ret is not None:
			ret = Dispatch(ret, u'Params', None)
		return ret

	def Post(self):
		"""Posts all changed values into database."""
		return self._oleobj_.InvokeTypes(6, LCID, 1, (24, 0), (),)

	def Refresh(self):
		"""Reads saved values, overwriting values in memory."""
		return self._oleobj_.InvokeTypes(7, LCID, 1, (24, 0), (),)

	def ResolveStepsParameters(self, UpdateLocalCache=True):
		"""Updates the texts of the run's steps by resolving the parameter values at run time."""
		return self._oleobj_.InvokeTypes(24, LCID, 1, (24, 0), ((11, 49),),UpdateLocalCache
			)

	# The method SetField is actually a property, but must be used as a method to correctly pass the arguments
	def SetField(self, FieldName=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The value of the specified field."""
		return self._oleobj_.InvokeTypes(0, LCID, 4, (24, 0), ((8, 0), (12, 1)),FieldName
			, arg1)

	def UnLockObject(self):
		"""Unlocks the object."""
		return self._oleobj_.InvokeTypes(3, LCID, 1, (24, 0), (),)

	def Undo(self):
		"""Undoes changes to field values that have not been posted."""
		return self._oleobj_.InvokeTypes(8, LCID, 1, (24, 0), (),)

	_prop_map_get_ = {
		"Attachments": (12, 2, (9, 0), (), "Attachments", None),
		"AutoPost": (5, 2, (11, 0), (), "AutoPost", None),
		"BPStepParamFactory": (27, 2, (9, 0), (), "BPStepParamFactory", None),
		"ExtendedStorage": (19, 2, (9, 0), (), "ExtendedStorage", None),
		"HasAttachment": (13, 2, (11, 0), (), "HasAttachment", None),
		"History": (11, 2, (9, 0), (), "History", None),
		"ID": (4, 2, (12, 0), (), "ID", None),
		"IsLocked": (1, 2, (11, 0), (), "IsLocked", None),
		"Modified": (9, 2, (11, 0), (), "Modified", None),
		"Name": (15, 2, (8, 0), (), "Name", None),
		"ResultLocation": (14, 2, (8, 0), (), "ResultLocation", None),
		"Status": (16, 2, (8, 0), (), "Status", None),
		"StepFactory": (17, 2, (9, 0), (), "StepFactory", None),
		"TestId": (18, 2, (3, 0), (), "TestId", None),
		"TestInstance": (25, 2, (3, 0), (), "TestInstance", None),
		"TestInstanceID": (28, 2, (3, 0), (), "TestInstanceID", None),
		"TestSetID": (22, 2, (3, 0), (), "TestSetID", None),
		"Virtual": (10, 2, (11, 0), (), "Virtual", None),
	}
	_prop_map_put_ = {
		"AutoPost": ((5, LCID, 4, 0),()),
		"Name": ((15, LCID, 4, 0),()),
		"Status": ((16, LCID, 4, 0),()),
	}
	# Default method for this class is 'Field'
	def __call__(self, FieldName=defaultNamedNotOptArg):
		"""The value of the specified field."""
		return self._ApplyTypes_(0, 2, (12, 0), ((8, 0),), '__call__', None,FieldName
			)

	# str(ob) and int(ob) will use __call__
	def __unicode__(self, *args):
		try:
			return unicode(self.__call__(*args))
		except pythoncom.com_error:
			return repr(self)
	def __str__(self, *args):
		return str(self.__unicode__(*args))
	def __int__(self, *args):
		return int(self.__call__(*args))

class IRunFactory(DispatchBaseClass):
	"""Services for managing test runs."""
	CLSID = IID('{682F76CF-D479-4A34-AD8F-108F6B6C23DB}')
	coclass_clsid = IID('{3A24FCD2-D5E3-4B5E-82BE-2202DD566FAD}')

	def AddItem(self, ItemData=defaultNamedNotOptArg):
		"""Creates a new item object."""
		ret = self._oleobj_.InvokeTypes(3, LCID, 1, (9, 0), ((12, 0),),ItemData
			)
		if ret is not None:
			ret = Dispatch(ret, u'AddItem', None)
		return ret

	def DeleteDuplicateRuns(self, RunName=defaultNamedNotOptArg):
		"""Removes duplicate runs for the current test set. Removal is immediate without Post."""
		return self._oleobj_.InvokeTypes(9, LCID, 1, (24, 0), ((8, 0),),RunName
			)

	# The method FetchLevel is actually a property, but must be used as a method to correctly pass the arguments
	def FetchLevel(self, FieldName=defaultNamedNotOptArg):
		"""The Fetch level for a field."""
		return self._oleobj_.InvokeTypes(7, LCID, 2, (2, 0), ((8, 1),),FieldName
			)

	# The method Item is actually a property, but must be used as a method to correctly pass the arguments
	def Item(self, ItemKey=defaultNamedNotOptArg):
		"""Gets an object managed by the factory by its key."""
		ret = self._oleobj_.InvokeTypes(0, LCID, 2, (9, 0), ((12, 0),),ItemKey
			)
		if ret is not None:
			ret = Dispatch(ret, u'Item', None)
		return ret

	# Result is of type IList
	def NewList(self, Filter=defaultNamedNotOptArg):
		"""Creates a list of objects according to the specified filter."""
		ret = self._oleobj_.InvokeTypes(1, LCID, 1, (9, 0), ((8, 0),),Filter
			)
		if ret is not None:
			ret = Dispatch(ret, u'NewList', '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')
		return ret

	def RemoveItem(self, ItemKey=defaultNamedNotOptArg):
		"""Removes item from the database. Removal takes place immediately, without a Post."""
		return self._oleobj_.InvokeTypes(4, LCID, 1, (24, 0), ((12, 0),),ItemKey
			)

	# The method SetFetchLevel is actually a property, but must be used as a method to correctly pass the arguments
	def SetFetchLevel(self, FieldName=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The Fetch level for a field."""
		return self._oleobj_.InvokeTypes(7, LCID, 4, (24, 0), ((8, 1), (2, 1)),FieldName
			, arg1)

	_prop_map_get_ = {
		# Method 'Fields' returns object of type 'IList'
		"Fields": (2, 2, (9, 0), (), "Fields", '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}'),
		"Filter": (5, 2, (9, 0), (), "Filter", None),
		"History": (6, 2, (9, 0), (), "History", None),
		"UniqueRunName": (8, 2, (8, 0), (), "UniqueRunName", None),
	}
	_prop_map_put_ = {
	}
	# Default method for this class is 'Item'
	def __call__(self, ItemKey=defaultNamedNotOptArg):
		"""Gets an object managed by the factory by its key."""
		ret = self._oleobj_.InvokeTypes(0, LCID, 2, (9, 0), ((12, 0),),ItemKey
			)
		if ret is not None:
			ret = Dispatch(ret, '__call__', None)
		return ret

	# str(ob) and int(ob) will use __call__
	def __unicode__(self, *args):
		try:
			return unicode(self.__call__(*args))
		except pythoncom.com_error:
			return repr(self)
	def __str__(self, *args):
		return str(self.__unicode__(*args))
	def __int__(self, *args):
		return int(self.__call__(*args))

class ISearchOptions(DispatchBaseClass):
	"""Options for controlling searches."""
	CLSID = IID('{2444C43F-5371-4B4A-B6B6-34D205644C35}')
	coclass_clsid = IID('{EEFE895F-49F8-4E82-9E78-4A6596B5E6F2}')

	# The method Property is actually a property, but must be used as a method to correctly pass the arguments
	def Property(self, Prop=defaultNamedNotOptArg):
		"""The search configuration flag."""
		return self._ApplyTypes_(1, 2, (12, 0), ((3, 1),), u'Property', None,Prop
			)

	# The method SetProperty is actually a property, but must be used as a method to correctly pass the arguments
	def SetProperty(self, Prop=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The search configuration flag."""
		return self._oleobj_.InvokeTypes(1, LCID, 4, (24, 0), ((3, 1), (12, 1)),Prop
			, arg1)

	_prop_map_get_ = {
		"Filter": (2, 2, (9, 0), (), "Filter", None),
		"Text": (3, 2, (8, 0), (), "Text", None),
	}
	_prop_map_put_ = {
		"Filter": ((2, LCID, 4, 0),()),
	}

class ISearchableFactory(DispatchBaseClass):
	"""ISearchableFactory Interface"""
	CLSID = IID('{03986C41-712B-4A6F-8C65-B50CBF7FC640}')
	coclass_clsid = IID('{0B280A46-1606-4FD3-90E4-EF5149725200}')

	def CreateSearchOptions(self):
		"""Returns a new SearchOptions object."""
		ret = self._oleobj_.InvokeTypes(1001, LCID, 1, (9, 0), (),)
		if ret is not None:
			ret = Dispatch(ret, u'CreateSearchOptions', None)
		return ret

	def IsSearchable(self):
		"""Checks if the Search method can be called by this factory."""
		return self._oleobj_.InvokeTypes(1003, LCID, 1, (11, 0), (),)

	# Result is of type IList
	def Search(self, Query=defaultNamedNotOptArg, pSearchOptions=defaultNamedNotOptArg):
		"""Finds entities matching the search query"""
		ret = self._oleobj_.InvokeTypes(1002, LCID, 1, (9, 0), ((8, 1), (9, 17)),Query
			, pSearchOptions)
		if ret is not None:
			ret = Dispatch(ret, u'Search', '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')
		return ret

	_prop_map_get_ = {
	}
	_prop_map_put_ = {
	}

class ISettings(DispatchBaseClass):
	"""Represents users' settings in various user-defined categories."""
	CLSID = IID('{55110A8D-110B-460C-9D28-F8B4BCF3DFF0}')
	coclass_clsid = None

	def Close(self):
		"""Closes and updates the category."""
		return self._oleobj_.InvokeTypes(3, LCID, 1, (24, 0), (),)

	def DeleteCategory(self, Category=defaultNamedNotOptArg):
		"""Deletes the current settings folder."""
		return self._oleobj_.InvokeTypes(4, LCID, 1, (24, 0), ((8, 0),),Category
			)

	def DeleteValue(self, Name=defaultNamedNotOptArg):
		"""Deletes the specified item."""
		return self._oleobj_.InvokeTypes(5, LCID, 1, (24, 0), ((8, 0),),Name
			)

	def Open(self, Category=defaultNamedNotOptArg):
		"""Sets the category to be used in subsequent actions by the client."""
		return self._oleobj_.InvokeTypes(1, LCID, 1, (24, 0), ((8, 0),),Category
			)

	def Post(self):
		"""Posts the category to the server."""
		return self._oleobj_.InvokeTypes(7, LCID, 1, (24, 0), (),)

	def Refresh(self, Category=defaultNamedNotOptArg):
		"""Reads saved values, overwriting values in memory."""
		return self._oleobj_.InvokeTypes(8, LCID, 1, (24, 0), ((8, 0),),Category
			)

	# The method SetValue is actually a property, but must be used as a method to correctly pass the arguments
	def SetValue(self, Name=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The value of the specified item in the active category."""
		return self._oleobj_.InvokeTypes(2, LCID, 4, (24, 0), ((8, 0), (8, 1)),Name
			, arg1)

	# The method Value is actually a property, but must be used as a method to correctly pass the arguments
	def Value(self, Name=defaultNamedNotOptArg):
		"""The value of the specified item in the active category."""
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(2, LCID, 2, (8, 0), ((8, 0),),Name
			)

	_prop_map_get_ = {
		# Method 'EnumItems' returns object of type 'IList'
		"EnumItems": (6, 2, (9, 0), (), "EnumItems", '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}'),
	}
	_prop_map_put_ = {
	}
	# Default method for this class is 'Value'
	def __call__(self, Name=defaultNamedNotOptArg):
		"""The value of the specified item in the active category."""
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(2, LCID, 2, (8, 0), ((8, 0),),Name
			)

	# str(ob) and int(ob) will use __call__
	def __unicode__(self, *args):
		try:
			return unicode(self.__call__(*args))
		except pythoncom.com_error:
			return repr(self)
	def __str__(self, *args):
		return str(self.__unicode__(*args))
	def __int__(self, *args):
		return int(self.__call__(*args))

class ISettings2(DispatchBaseClass):
	"""Represents users' settings in various user-defined categories."""
	CLSID = IID('{B761098B-AC59-4C3D-A427-D09231A402B8}')
	coclass_clsid = IID('{BD969260-2F3A-40D0-BE71-7ACE587FB343}')

	def Close(self):
		"""Closes and updates the category."""
		return self._oleobj_.InvokeTypes(3, LCID, 1, (24, 0), (),)

	def DeleteCategory(self, Category=defaultNamedNotOptArg):
		"""Deletes the current settings folder."""
		return self._oleobj_.InvokeTypes(4, LCID, 1, (24, 0), ((8, 0),),Category
			)

	def DeleteValue(self, Name=defaultNamedNotOptArg):
		"""Deletes the specified item."""
		return self._oleobj_.InvokeTypes(5, LCID, 1, (24, 0), ((8, 0),),Name
			)

	# The method IsSystem is actually a property, but must be used as a method to correctly pass the arguments
	def IsSystem(self, Name=defaultNamedNotOptArg):
		"""Checks if the specified setting is built-in and read-only."""
		return self._oleobj_.InvokeTypes(9, LCID, 2, (11, 0), ((8, 0),),Name
			)

	def Open(self, Category=defaultNamedNotOptArg):
		"""Sets the category to be used in subsequent actions by the client."""
		return self._oleobj_.InvokeTypes(1, LCID, 1, (24, 0), ((8, 0),),Category
			)

	def Post(self):
		"""Posts the category to the server."""
		return self._oleobj_.InvokeTypes(7, LCID, 1, (24, 0), (),)

	def Refresh(self, Category=defaultNamedNotOptArg):
		"""Reads saved values, overwriting values in memory."""
		return self._oleobj_.InvokeTypes(8, LCID, 1, (24, 0), ((8, 0),),Category
			)

	# The method SetValue is actually a property, but must be used as a method to correctly pass the arguments
	def SetValue(self, Name=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The value of the specified item in the active category."""
		return self._oleobj_.InvokeTypes(2, LCID, 4, (24, 0), ((8, 0), (8, 1)),Name
			, arg1)

	# The method Value is actually a property, but must be used as a method to correctly pass the arguments
	def Value(self, Name=defaultNamedNotOptArg):
		"""The value of the specified item in the active category."""
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(2, LCID, 2, (8, 0), ((8, 0),),Name
			)

	_prop_map_get_ = {
		# Method 'EnumItems' returns object of type 'IList'
		"EnumItems": (6, 2, (9, 0), (), "EnumItems", '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}'),
	}
	_prop_map_put_ = {
	}
	# Default method for this class is 'Value'
	def __call__(self, Name=defaultNamedNotOptArg):
		"""The value of the specified item in the active category."""
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(2, LCID, 2, (8, 0), ((8, 0),),Name
			)

	# str(ob) and int(ob) will use __call__
	def __unicode__(self, *args):
		try:
			return unicode(self.__call__(*args))
		except pythoncom.com_error:
			return repr(self)
	def __str__(self, *args):
		return str(self.__unicode__(*args))
	def __int__(self, *args):
		return int(self.__call__(*args))

class IStep(DispatchBaseClass):
	"""Represents a test step in a test run."""
	CLSID = IID('{992BABA3-6360-4BD1-A337-B75F67BDB417}')
	coclass_clsid = None

	# The method Field is actually a property, but must be used as a method to correctly pass the arguments
	def Field(self, FieldName=defaultNamedNotOptArg):
		"""The value of the specified field."""
		return self._ApplyTypes_(0, 2, (12, 0), ((8, 0),), u'Field', None,FieldName
			)

	def LockObject(self):
		"""Locks the object. Returns True if lock successful."""
		return self._oleobj_.InvokeTypes(2, LCID, 1, (11, 0), (),)

	def Post(self):
		"""Posts all changed values into database."""
		return self._oleobj_.InvokeTypes(6, LCID, 1, (24, 0), (),)

	def Refresh(self):
		"""Reads saved values, overwriting values in memory."""
		return self._oleobj_.InvokeTypes(7, LCID, 1, (24, 0), (),)

	# The method SetField is actually a property, but must be used as a method to correctly pass the arguments
	def SetField(self, FieldName=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The value of the specified field."""
		return self._oleobj_.InvokeTypes(0, LCID, 4, (24, 0), ((8, 0), (12, 1)),FieldName
			, arg1)

	def UnLockObject(self):
		"""Unlocks the object."""
		return self._oleobj_.InvokeTypes(3, LCID, 1, (24, 0), (),)

	def Undo(self):
		"""Undoes changes to field values that have not been posted."""
		return self._oleobj_.InvokeTypes(8, LCID, 1, (24, 0), (),)

	_prop_map_get_ = {
		"Attachments": (12, 2, (9, 0), (), "Attachments", None),
		"AutoPost": (5, 2, (11, 0), (), "AutoPost", None),
		"DesignStepSource": (18, 2, (3, 0), (), "DesignStepSource", None),
		"HasAttachment": (13, 2, (11, 0), (), "HasAttachment", None),
		"History": (11, 2, (9, 0), (), "History", None),
		"ID": (4, 2, (12, 0), (), "ID", None),
		"IsLocked": (1, 2, (11, 0), (), "IsLocked", None),
		"Modified": (9, 2, (11, 0), (), "Modified", None),
		"Name": (14, 2, (8, 0), (), "Name", None),
		"Status": (15, 2, (8, 0), (), "Status", None),
		"TestSource": (17, 2, (3, 0), (), "TestSource", None),
		"Virtual": (10, 2, (11, 0), (), "Virtual", None),
	}
	_prop_map_put_ = {
		"AutoPost": ((5, LCID, 4, 0),()),
		"CreationMode": ((16, LCID, 4, 0),()),
		"Name": ((14, LCID, 4, 0),()),
		"Status": ((15, LCID, 4, 0),()),
	}
	# Default method for this class is 'Field'
	def __call__(self, FieldName=defaultNamedNotOptArg):
		"""The value of the specified field."""
		return self._ApplyTypes_(0, 2, (12, 0), ((8, 0),), '__call__', None,FieldName
			)

	# str(ob) and int(ob) will use __call__
	def __unicode__(self, *args):
		try:
			return unicode(self.__call__(*args))
		except pythoncom.com_error:
			return repr(self)
	def __str__(self, *args):
		return str(self.__unicode__(*args))
	def __int__(self, *args):
		return int(self.__call__(*args))

class IStep2(DispatchBaseClass):
	"""A test step in a test run. Extends IStep."""
	CLSID = IID('{5FF530DD-245E-4F97-A59F-3DE69FFCC55E}')
	coclass_clsid = IID('{C29AB208-56D4-493C-80FB-EA952235BAFB}')

	# The method Field is actually a property, but must be used as a method to correctly pass the arguments
	def Field(self, FieldName=defaultNamedNotOptArg):
		"""The value of the specified field."""
		return self._ApplyTypes_(0, 2, (12, 0), ((8, 0),), u'Field', None,FieldName
			)

	def LockObject(self):
		"""Locks the object. Returns True if lock successful."""
		return self._oleobj_.InvokeTypes(2, LCID, 1, (11, 0), (),)

	def Post(self):
		"""Posts all changed values into database."""
		return self._oleobj_.InvokeTypes(6, LCID, 1, (24, 0), (),)

	def Refresh(self):
		"""Reads saved values, overwriting values in memory."""
		return self._oleobj_.InvokeTypes(7, LCID, 1, (24, 0), (),)

	# The method SetField is actually a property, but must be used as a method to correctly pass the arguments
	def SetField(self, FieldName=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The value of the specified field."""
		return self._oleobj_.InvokeTypes(0, LCID, 4, (24, 0), ((8, 0), (12, 1)),FieldName
			, arg1)

	def UnLockObject(self):
		"""Unlocks the object."""
		return self._oleobj_.InvokeTypes(3, LCID, 1, (24, 0), (),)

	def Undo(self):
		"""Undoes changes to field values that have not been posted."""
		return self._oleobj_.InvokeTypes(8, LCID, 1, (24, 0), (),)

	_prop_map_get_ = {
		"Attachments": (12, 2, (9, 0), (), "Attachments", None),
		"AutoPost": (5, 2, (11, 0), (), "AutoPost", None),
		"BPStepParamFactory": (19, 2, (9, 0), (), "BPStepParamFactory", None),
		"DesignStepSource": (18, 2, (3, 0), (), "DesignStepSource", None),
		"HasAttachment": (13, 2, (11, 0), (), "HasAttachment", None),
		"History": (11, 2, (9, 0), (), "History", None),
		"ID": (4, 2, (12, 0), (), "ID", None),
		"IsLocked": (1, 2, (11, 0), (), "IsLocked", None),
		"Modified": (9, 2, (11, 0), (), "Modified", None),
		"Name": (14, 2, (8, 0), (), "Name", None),
		"Run": (20, 2, (9, 0), (), "Run", None),
		"Status": (15, 2, (8, 0), (), "Status", None),
		"TestSource": (17, 2, (3, 0), (), "TestSource", None),
		"Virtual": (10, 2, (11, 0), (), "Virtual", None),
	}
	_prop_map_put_ = {
		"AutoPost": ((5, LCID, 4, 0),()),
		"CreationMode": ((16, LCID, 4, 0),()),
		"Name": ((14, LCID, 4, 0),()),
		"Status": ((15, LCID, 4, 0),()),
	}
	# Default method for this class is 'Field'
	def __call__(self, FieldName=defaultNamedNotOptArg):
		"""The value of the specified field."""
		return self._ApplyTypes_(0, 2, (12, 0), ((8, 0),), '__call__', None,FieldName
			)

	# str(ob) and int(ob) will use __call__
	def __unicode__(self, *args):
		try:
			return unicode(self.__call__(*args))
		except pythoncom.com_error:
			return repr(self)
	def __str__(self, *args):
		return str(self.__unicode__(*args))
	def __int__(self, *args):
		return int(self.__call__(*args))

class IStepParams(DispatchBaseClass):
	"""A collection of test parameters."""
	CLSID = IID('{B4776982-5666-4075-99A3-0574EDA12EF2}')
	coclass_clsid = IID('{86BDCEC4-35BD-4DA8-8373-474C4EBDFA9F}')

	def AddParam(self, ParamName=defaultNamedNotOptArg, ParamType=defaultNamedNotOptArg):
		"""Adds a new parameter to the object."""
		return self._oleobj_.InvokeTypes(11, LCID, 1, (24, 0), ((8, 1), (8, 1)),ParamName
			, ParamType)

	# The method BaseValue is actually a property, but must be used as a method to correctly pass the arguments
	def BaseValue(self, vParam=defaultNamedNotOptArg, HasBaseValue=pythoncom.Missing):
		"""Gets a more basic value of the param - some default."""
		return self._ApplyTypes_(6, 2, (8, 0), ((12, 1), (16395, 2)), u'BaseValue', None,vParam
			, HasBaseValue)

	def ClearParam(self, vParam=defaultNamedNotOptArg):
		"""Clears the parameter value by setting it to null."""
		return self._oleobj_.InvokeTypes(3, LCID, 1, (24, 0), ((12, 1),),vParam
			)

	def DeleteParam(self, ParamName=defaultNamedNotOptArg):
		"""Deletes the specified parameter."""
		return self._oleobj_.InvokeTypes(12, LCID, 1, (24, 0), ((8, 1),),ParamName
			)

	# The method ParamExist is actually a property, but must be used as a method to correctly pass the arguments
	def ParamExist(self, ParamName=defaultNamedNotOptArg):
		"""Checks if a parameter of this name exists."""
		return self._oleobj_.InvokeTypes(5, LCID, 2, (11, 0), ((8, 1),),ParamName
			)

	# The method ParamName is actually a property, but must be used as a method to correctly pass the arguments
	def ParamName(self, nPosition=defaultNamedNotOptArg):
		"""The name of the parameter in the specified position in the StepParams collection (zero-based)."""
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(7, LCID, 2, (8, 0), ((3, 1),),nPosition
			)

	# The method ParamType is actually a property, but must be used as a method to correctly pass the arguments
	def ParamType(self, vParam=defaultNamedNotOptArg):
		"""Gets Parameter Type - such as defined by user."""
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(4, LCID, 2, (8, 0), ((12, 1),),vParam
			)

	# The method ParamValue is actually a property, but must be used as a method to correctly pass the arguments
	def ParamValue(self, vParam=defaultNamedNotOptArg):
		"""The value of the specified parameter."""
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(2, LCID, 2, (8, 0), ((12, 1),),vParam
			)

	def Refresh(self):
		"""Refreshes the data from the database, discarding changes."""
		return self._oleobj_.InvokeTypes(10, LCID, 1, (24, 0), (),)

	def Save(self):
		"""Uploads the parameter definitions to the database."""
		return self._oleobj_.InvokeTypes(9, LCID, 1, (24, 0), (),)

	# The method SetParamType is actually a property, but must be used as a method to correctly pass the arguments
	def SetParamType(self, vParam=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""Gets Parameter Type - such as defined by user."""
		return self._oleobj_.InvokeTypes(4, LCID, 4, (24, 0), ((12, 1), (8, 1)),vParam
			, arg1)

	# The method SetParamValue is actually a property, but must be used as a method to correctly pass the arguments
	def SetParamValue(self, vParam=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The value of the specified parameter."""
		return self._oleobj_.InvokeTypes(2, LCID, 4, (24, 0), ((12, 1), (8, 1)),vParam
			, arg1)

	# The method Type is actually a property, but must be used as a method to correctly pass the arguments
	def Type(self, vParam=defaultNamedNotOptArg):
		"""Gets the parameter type: predefined, null, or regular."""
		return self._oleobj_.InvokeTypes(8, LCID, 2, (3, 0), ((12, 1),),vParam
			)

	_prop_map_get_ = {
		"Count": (1, 2, (3, 0), (), "Count", None),
	}
	_prop_map_put_ = {
	}
	#This class has Count() property - allow len(ob) to provide this
	def __len__(self):
		return self._ApplyTypes_(*(1, 2, (3, 0), (), "Count", None))
	#This class has a __len__ - this is needed so 'if object:' always returns TRUE.
	def __nonzero__(self):
		return True

class ISubjectNode(DispatchBaseClass):
	"""Represents a subject folder in a Quality Center subject tree."""
	CLSID = IID('{0F500A37-F2F1-4079-9BE7-48C1DA715E27}')
	coclass_clsid = IID('{7300228B-0F1B-44D3-BF3A-790253622527}')

	# Result is of type ISysTreeNode
	def AddNode(self, NodeName=defaultNamedNotOptArg):
		"""Adds a new child node."""
		ret = self._oleobj_.InvokeTypes(1, LCID, 1, (9, 0), ((8, 0),),NodeName
			)
		if ret is not None:
			ret = Dispatch(ret, u'AddNode', '{8538CCBA-0B3E-481F-BB2B-14F0553CA146}')
		return ret

	# Result is of type ISysTreeNode
	# The method Child is actually a property, but must be used as a method to correctly pass the arguments
	def Child(self, Index=defaultNamedNotOptArg):
		"""Gets a sub-folder of the current folder."""
		ret = self._oleobj_.InvokeTypes(8, LCID, 2, (9, 0), ((3, 0),),Index
			)
		if ret is not None:
			ret = Dispatch(ret, u'Child', '{8538CCBA-0B3E-481F-BB2B-14F0553CA146}')
		return ret

	# Result is of type ISysTreeNode
	def FindChildNode(self, ChildName=defaultNamedNotOptArg):
		"""Finds a child node by node name."""
		ret = self._oleobj_.InvokeTypes(5, LCID, 1, (9, 0), ((8, 0),),ChildName
			)
		if ret is not None:
			ret = Dispatch(ret, u'FindChildNode', '{8538CCBA-0B3E-481F-BB2B-14F0553CA146}')
		return ret

	# Result is of type IList
	def FindChildren(self, Pattern=defaultNamedNotOptArg, MatchCase=False, Filter=u''):
		"""Finds node children according to specified search pattern."""
		return self._ApplyTypes_(11, 1, (9, 32), ((8, 1), (11, 49), (8, 49)), u'FindChildren', '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}',Pattern
			, MatchCase, Filter)

	# Result is of type IList
	def FindTests(self, Pattern=defaultNamedNotOptArg, MatchCase=False, Filter=u''):
		"""Finds tests within the current subject folder whose name matches the specified text pattern."""
		return self._ApplyTypes_(19, 1, (9, 32), ((8, 1), (11, 49), (8, 49)), u'FindTests', '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}',Pattern
			, MatchCase, Filter)

	def Move(self, Parent=defaultNamedNotOptArg):
		"""Moves the current tree mode to be under the specified parent."""
		return self._oleobj_.InvokeTypes(22, LCID, 1, (24, 0), ((12, 1),),Parent
			)

	# Result is of type IList
	def NewList(self):
		"""Gets a list of the node's children."""
		ret = self._oleobj_.InvokeTypes(2, LCID, 1, (9, 0), (),)
		if ret is not None:
			ret = Dispatch(ret, u'NewList', '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')
		return ret

	def Post(self):
		"""Posts all changed values to the database."""
		return self._oleobj_.InvokeTypes(13, LCID, 1, (24, 0), (),)

	def Refresh(self):
		"""Reads the saved node data, overwriting values in memory."""
		return self._oleobj_.InvokeTypes(14, LCID, 1, (24, 0), (),)

	def RemoveNode(self, Node=defaultNamedNotOptArg):
		"""Deletes the specified node."""
		return self._oleobj_.InvokeTypes(6, LCID, 1, (24, 0), ((12, 0),),Node
			)

	def RemoveSubjectNode(self, Node=defaultNamedNotOptArg, DeleteTests=False):
		"""Removes the specified node from the subject tree."""
		return self._oleobj_.InvokeTypes(16, LCID, 1, (24, 0), ((12, 0), (11, 49)),Node
			, DeleteTests)

	_prop_map_get_ = {
		"Attachments": (18, 2, (9, 0), (), "Attachments", None),
		"Attribute": (4, 2, (3, 0), (), "Attribute", None),
		"Count": (9, 2, (3, 0), (), "Count", None),
		"DepthType": (10, 2, (2, 0), (), "DepthType", None),
		"Description": (17, 2, (8, 0), (), "Description", None),
		# Method 'Father' returns object of type 'ISysTreeNode'
		"Father": (7, 2, (9, 0), (), "Father", '{8538CCBA-0B3E-481F-BB2B-14F0553CA146}'),
		"HasAttachment": (21, 2, (11, 0), (), "HasAttachment", None),
		"Name": (0, 2, (8, 0), (), "Name", None),
		"NodeID": (3, 2, (3, 0), (), "NodeID", None),
		"Path": (12, 2, (8, 0), (), "Path", None),
		"TestFactory": (15, 2, (9, 0), (), "TestFactory", None),
		"ViewOrder": (20, 2, (3, 0), (), "ViewOrder", None),
	}
	_prop_map_put_ = {
		"Description": ((17, LCID, 4, 0),()),
		"Name": ((0, LCID, 4, 0),()),
		"ViewOrder": ((20, LCID, 4, 0),()),
	}
	# Default property for this class is 'Name'
	def __call__(self):
		return self._ApplyTypes_(*(0, 2, (8, 0), (), "Name", None))
	# str(ob) and int(ob) will use __call__
	def __unicode__(self, *args):
		try:
			return unicode(self.__call__(*args))
		except pythoncom.com_error:
			return repr(self)
	def __str__(self, *args):
		return str(self.__unicode__(*args))
	def __int__(self, *args):
		return int(self.__call__(*args))
	#This class has Count() property - allow len(ob) to provide this
	def __len__(self):
		return self._ApplyTypes_(*(9, 2, (3, 0), (), "Count", None))
	#This class has a __len__ - this is needed so 'if object:' always returns TRUE.
	def __nonzero__(self):
		return True

class ISysTreeNode(DispatchBaseClass):
	"""Represents a system folder, which is a tree node in the TreeManager object."""
	CLSID = IID('{8538CCBA-0B3E-481F-BB2B-14F0553CA146}')
	coclass_clsid = IID('{BCE38540-36CC-4C3A-B474-11C8E8EEEDE0}')

	# Result is of type ISysTreeNode
	def AddNode(self, NodeName=defaultNamedNotOptArg):
		"""Adds a new child node."""
		ret = self._oleobj_.InvokeTypes(1, LCID, 1, (9, 0), ((8, 0),),NodeName
			)
		if ret is not None:
			ret = Dispatch(ret, u'AddNode', '{8538CCBA-0B3E-481F-BB2B-14F0553CA146}')
		return ret

	# Result is of type ISysTreeNode
	# The method Child is actually a property, but must be used as a method to correctly pass the arguments
	def Child(self, Index=defaultNamedNotOptArg):
		"""Gets a sub-folder of the current folder."""
		ret = self._oleobj_.InvokeTypes(8, LCID, 2, (9, 0), ((3, 0),),Index
			)
		if ret is not None:
			ret = Dispatch(ret, u'Child', '{8538CCBA-0B3E-481F-BB2B-14F0553CA146}')
		return ret

	# Result is of type ISysTreeNode
	def FindChildNode(self, ChildName=defaultNamedNotOptArg):
		"""Finds a child node by node name."""
		ret = self._oleobj_.InvokeTypes(5, LCID, 1, (9, 0), ((8, 0),),ChildName
			)
		if ret is not None:
			ret = Dispatch(ret, u'FindChildNode', '{8538CCBA-0B3E-481F-BB2B-14F0553CA146}')
		return ret

	# Result is of type IList
	def FindChildren(self, Pattern=defaultNamedNotOptArg, MatchCase=False, Filter=u''):
		"""Finds node children according to specified search pattern."""
		return self._ApplyTypes_(11, 1, (9, 32), ((8, 1), (11, 49), (8, 49)), u'FindChildren', '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}',Pattern
			, MatchCase, Filter)

	# Result is of type IList
	def NewList(self):
		"""Gets a list of the node's children."""
		ret = self._oleobj_.InvokeTypes(2, LCID, 1, (9, 0), (),)
		if ret is not None:
			ret = Dispatch(ret, u'NewList', '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')
		return ret

	def Post(self):
		"""Posts all changed values to the database."""
		return self._oleobj_.InvokeTypes(13, LCID, 1, (24, 0), (),)

	def Refresh(self):
		"""Reads the saved node data, overwriting values in memory."""
		return self._oleobj_.InvokeTypes(14, LCID, 1, (24, 0), (),)

	def RemoveNode(self, Node=defaultNamedNotOptArg):
		"""Deletes the specified node."""
		return self._oleobj_.InvokeTypes(6, LCID, 1, (24, 0), ((12, 0),),Node
			)

	_prop_map_get_ = {
		"Attribute": (4, 2, (3, 0), (), "Attribute", None),
		"Count": (9, 2, (3, 0), (), "Count", None),
		"DepthType": (10, 2, (2, 0), (), "DepthType", None),
		# Method 'Father' returns object of type 'ISysTreeNode'
		"Father": (7, 2, (9, 0), (), "Father", '{8538CCBA-0B3E-481F-BB2B-14F0553CA146}'),
		"Name": (0, 2, (8, 0), (), "Name", None),
		"NodeID": (3, 2, (3, 0), (), "NodeID", None),
		"Path": (12, 2, (8, 0), (), "Path", None),
	}
	_prop_map_put_ = {
		"Name": ((0, LCID, 4, 0),()),
	}
	# Default property for this class is 'Name'
	def __call__(self):
		return self._ApplyTypes_(*(0, 2, (8, 0), (), "Name", None))
	# str(ob) and int(ob) will use __call__
	def __unicode__(self, *args):
		try:
			return unicode(self.__call__(*args))
		except pythoncom.com_error:
			return repr(self)
	def __str__(self, *args):
		return str(self.__unicode__(*args))
	def __int__(self, *args):
		return int(self.__call__(*args))
	#This class has Count() property - allow len(ob) to provide this
	def __len__(self):
		return self._ApplyTypes_(*(9, 2, (3, 0), (), "Count", None))
	#This class has a __len__ - this is needed so 'if object:' always returns TRUE.
	def __nonzero__(self):
		return True

class ITDChat(DispatchBaseClass):
	"""ITDChat Interface"""
	CLSID = IID('{D323F3D1-837E-4C0F-9ACB-7CBCDDA557DC}')
	coclass_clsid = IID('{C7CE06D8-F6D2-47B1-AB82-EE9A762171CD}')

	def ChangeChat(self, NewChatRoom=defaultNamedNotOptArg):
		"""Changes chat room."""
		return self._oleobj_.InvokeTypes(5, LCID, 1, (24, 0), ((8, 1),),NewChatRoom
			)

	# The method ChatData is actually a property, but must be used as a method to correctly pass the arguments
	def ChatData(self, GetAllMesseges=defaultNamedNotOptArg):
		"""Receives new chat messages."""
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(4, LCID, 2, (8, 0), ((11, 1),),GetAllMesseges
			)

	def Connect(self, ChatRoom=defaultNamedNotOptArg, NewData=pythoncom.Missing):
		"""Connects to chat."""
		return self._ApplyTypes_(1, 1, (24, 0), ((8, 1), (16392, 2)), u'Connect', None,ChatRoom
			, NewData)

	def Disconnect(self):
		"""Disconnects from chat."""
		return self._oleobj_.InvokeTypes(2, LCID, 1, (24, 0), (),)

	def PutMessage(self, ChatMessage=defaultNamedNotOptArg, NewData=pythoncom.Missing):
		"""Sends message to chat and recieve new messages."""
		return self._ApplyTypes_(3, 1, (24, 0), ((8, 1), (16392, 2)), u'PutMessage', None,ChatMessage
			, NewData)

	_prop_map_get_ = {
	}
	_prop_map_put_ = {
	}

class ITDConnection(DispatchBaseClass):
	"""Represents a single server connection."""
	CLSID = IID('{DB67E08D-7FA7-4DAC-87BB-2A55CBFFE008}')
	coclass_clsid = None

	def ChangePassword(self, OldPassword=defaultNamedNotOptArg, NewPassword=defaultNamedNotOptArg):
		"""Changes the password for the currently logged in user."""
		return self._oleobj_.InvokeTypes(31, LCID, 1, (24, 0), ((8, 1), (8, 1)),OldPassword
			, NewPassword)

	def ConnectProject(self, ProjectName=defaultNamedNotOptArg, UserName=defaultNamedNotOptArg, Password=u''):
		"""Connects to the specified project."""
		return self._ApplyTypes_(15, 1, (24, 32), ((8, 0), (8, 0), (8, 49)), u'ConnectProject', None,ProjectName
			, UserName, Password)

	def ConnectProjectEx(self, DomainName=defaultNamedNotOptArg, ProjectName=defaultNamedNotOptArg, UserName=defaultNamedNotOptArg, Password=u''):
		"""Connects to the specified project in the specified domain."""
		return self._ApplyTypes_(54, 1, (24, 32), ((8, 1), (8, 1), (8, 1), (8, 49)), u'ConnectProjectEx', None,DomainName
			, ProjectName, UserName, Password)

	def ConnectToVCSAs(self, UserName=defaultNamedNotOptArg, Password=defaultNamedNotOptArg, CopyDesStep=defaultNamedNotOptArg):
		"""obsolete Method ConnectToVCSAs."""
		return self._oleobj_.InvokeTypes(56, LCID, 1, (24, 0), ((8, 0), (8, 0), (8, 0)),UserName
			, Password, CopyDesStep)

	# The method DirectoryPath is actually a property, but must be used as a method to correctly pass the arguments
	def DirectoryPath(self, nType=defaultNamedNotOptArg):
		"""The path of the server side respository directory for the database repository type."""
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(35, LCID, 2, (8, 0), ((3, 0),),nType
			)

	def DisconnectProject(self):
		"""Disconnects from the project."""
		return self._oleobj_.InvokeTypes(16, LCID, 1, (24, 0), (),)

	# Result is of type IList
	# The method Fields is actually a property, but must be used as a method to correctly pass the arguments
	def Fields(self, DataType=defaultNamedNotOptArg):
		"""A list of fields for the table specified in the DataType argument."""
		ret = self._oleobj_.InvokeTypes(27, LCID, 2, (9, 0), ((8, 1),),DataType
			)
		if ret is not None:
			ret = Dispatch(ret, u'Fields', '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')
		return ret

	def GetLicense(self, LicenseType=defaultNamedNotOptArg):
		"""Allocates a license type according to the LicenseType parameter"""
		return self._oleobj_.InvokeTypes(22, LCID, 1, (24, 0), ((3, 0),),LicenseType
			)

	def GetLicenseStatus(self, ClientType=defaultNamedNotOptArg, InUse=pythoncom.Missing, Max=pythoncom.Missing):
		"""The license status for the specified client type."""
		return self._ApplyTypes_(42, 1, (24, 0), ((3, 1), (16387, 2), (16387, 2)), u'GetLicenseStatus', None,ClientType
			, InUse, Max)

	def GetLicenses(self, LicensesType=defaultNamedNotOptArg, pVal=defaultNamedNotOptArg):
		"""Allocates multiple licenses types according to the LicensesType parameter."""
		return self._oleobj_.InvokeTypes(57, LCID, 1, (24, 0), ((3, 0), (16392, 0)),LicensesType
			, pVal)

	def GetTDVersion(self, pbsMajorVersion=pythoncom.Missing, pbsBuildNum=pythoncom.Missing):
		"""The major and minor versions of the OTA API."""
		return self._ApplyTypes_(72, 1, (24, 0), ((16392, 2), (16392, 2)), u'GetTDVersion', None,pbsMajorVersion
			, pbsBuildNum)

	def InitConnection(self, ServerName=defaultNamedNotOptArg, DomainName=u'', DomainPswd=u''):
		"""Initializes the Open Test Architecture API server connection for specified domain."""
		return self._ApplyTypes_(13, 1, (24, 32), ((8, 0), (8, 49), (8, 49)), u'InitConnection', None,ServerName
			, DomainName, DomainPswd)

	def InitConnectionEx(self, ServerName=defaultNamedNotOptArg):
		"""Initializes the  connection."""
		return self._oleobj_.InvokeTypes(52, LCID, 1, (24, 0), ((8, 0),),ServerName
			)

	# The method ModuleVisible is actually a property, but must be used as a method to correctly pass the arguments
	def ModuleVisible(self, ModuleID=defaultNamedNotOptArg):
		"""The specified module is visible to the connected user. This property is used for Quality Center UI programming and has no function in third-party applications."""
		return self._oleobj_.InvokeTypes(51, LCID, 2, (11, 0), ((3, 1),),ModuleID
			)

	# Result is of type IList
	# The method ProjectsListEx is actually a property, but must be used as a method to correctly pass the arguments
	def ProjectsListEx(self, DomainName=defaultNamedNotOptArg):
		"""The projects available within the specified domain."""
		ret = self._oleobj_.InvokeTypes(53, LCID, 2, (9, 0), ((8, 1),),DomainName
			)
		if ret is not None:
			ret = Dispatch(ret, u'ProjectsListEx', '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')
		return ret

	def PurgeRuns(self, TestSetFilter=defaultNamedNotOptArg, KeepLast=defaultNamedNotOptArg, DateUnit=defaultNamedNotOptArg, UnitCount=defaultNamedNotOptArg
			, StepsOnly=defaultNamedNotOptArg):
		"""Deletes the runs in this connection that match the input arguments."""
		return self._oleobj_.InvokeTypes(40, LCID, 1, (24, 0), ((8, 1), (3, 1), (12, 1), (3, 1), (11, 1)),TestSetFilter
			, KeepLast, DateUnit, UnitCount, StepsOnly)

	def ReleaseConnection(self):
		"""ReleaseConnection must be called for each call of an initialization method after finishing work with the Open Test Architecture API."""
		return self._oleobj_.InvokeTypes(14, LCID, 1, (24, 0), (),)

	def SendMail(self, SendTo=defaultNamedNotOptArg, SendFrom=u'', Subject=u'', Message=u''
			, attachArray=defaultNamedNotOptArg, bsFormat=u''):
		"""Sends Mail."""
		return self._ApplyTypes_(23, 1, (24, 32), ((8, 1), (8, 49), (8, 49), (8, 49), (12, 17), (8, 49)), u'SendMail', None,SendTo
			, SendFrom, Subject, Message, attachArray, bsFormat
			)

	# The method SetClientType is actually a property, but must be used as a method to correctly pass the arguments
	def SetClientType(self, newVal=defaultNamedNotOptArg, arg1=u''):
		"""Sets the Client type. For internal use."""
		return self._ApplyTypes_(69, 4, (24, 32), ((8, 1), (8, 49)), u'SetClientType', None,newVal
			, arg1)

	def SynchronizeFollowUps(self, Password=defaultNamedNotOptArg):
		"""Obsolete: Sends all previously unsent followups."""
		return self._oleobj_.InvokeTypes(65, LCID, 1, (24, 0), ((8, 1),),Password
			)

	# The method TDParams is actually a property, but must be used as a method to correctly pass the arguments
	def TDParams(self, Request=defaultNamedNotOptArg):
		"""Returns the value of the parameter whose name is specified by Request."""
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(45, LCID, 2, (8, 0), ((8, 1),),Request
			)

	_prop_map_get_ = {
		"ActionPermission": (21, 2, (9, 0), (), "ActionPermission", None),
		"AlertManager": (63, 2, (9, 0), (), "AlertManager", None),
		"AllowReconnect": (64, 2, (11, 0), (), "AllowReconnect", None),
		"Analysis": (58, 2, (9, 0), (), "Analysis", None),
		"BugFactory": (8, 2, (9, 0), (), "BugFactory", None),
		"ChangeFactory": (36, 2, (9, 0), (), "ChangeFactory", None),
		"CheckoutRepository": (47, 2, (8, 0), (), "CheckoutRepository", None),
		"Command": (18, 2, (9, 0), (), "Command", None),
		"CommonSettings": (28, 2, (9, 0), (), "CommonSettings", None),
		"ComponentFactory": (70, 2, (9, 0), (), "ComponentFactory", None),
		"ComponentFolderFactory": (71, 2, (9, 0), (), "ComponentFolderFactory", None),
		"Connected": (1, 2, (11, 0), (), "Connected", None),
		"Customization": (26, 2, (9, 0), (), "Customization", None),
		"DBManager": (25, 2, (9, 0), (), "DBManager", None),
		"DBName": (60, 2, (8, 0), (), "DBName", None),
		"DBType": (24, 2, (8, 0), (), "DBType", None),
		"DomainName": (43, 2, (8, 0), (), "DomainName", None),
		# Method 'DomainsList' returns object of type 'IList'
		"DomainsList": (55, 2, (9, 0), (), "DomainsList", '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}'),
		"ExtendedStorage": (34, 2, (9, 0), (), "ExtendedStorage", None),
		"HostFactory": (11, 2, (9, 0), (), "HostFactory", None),
		"HostGroupFactory": (30, 2, (9, 0), (), "HostGroupFactory", None),
		"IgnoreHtmlFormat": (67, 2, (11, 0), (), "IgnoreHtmlFormat", None),
		"KeepConnection": (66, 2, (11, 0), (), "KeepConnection", None),
		"MailConditions": (37, 2, (9, 0), (), "MailConditions", None),
		"Password": (33, 2, (8, 0), (), "Password", None),
		"ProjectConnected": (2, 2, (11, 0), (), "ProjectConnected", None),
		"ProjectName": (4, 2, (8, 0), (), "ProjectName", None),
		"ProjectProperties": (41, 2, (9, 0), (), "ProjectProperties", None),
		# Method 'ProjectsList' returns object of type 'IList'
		"ProjectsList": (17, 2, (9, 0), (), "ProjectsList", '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}'),
		"ReportRole": (68, 2, (8, 0), (), "ReportRole", None),
		"ReqFactory": (20, 2, (9, 0), (), "ReqFactory", None),
		"Rules": (61, 2, (9, 0), (), "Rules", None),
		"RunFactory": (50, 2, (9, 0), (), "RunFactory", None),
		"ServerName": (3, 2, (8, 0), (), "ServerName", None),
		"ServerTime": (38, 2, (7, 0), (), "ServerTime", None),
		"ServerURL": (73, 2, (8, 0), (), "ServerURL", None),
		"TDSettings": (39, 2, (9, 0), (), "TDSettings", None),
		"TestFactory": (7, 2, (9, 0), (), "TestFactory", None),
		"TestRepository": (5, 2, (8, 0), (), "TestRepository", None),
		"TestSetFactory": (9, 2, (9, 0), (), "TestSetFactory", None),
		"TestSetTreeManager": (62, 2, (9, 0), (), "TestSetTreeManager", None),
		"TextParam": (44, 2, (9, 0), (), "TextParam", None),
		"TreeManager": (19, 2, (9, 0), (), "TreeManager", None),
		# Method 'UserGroupsList' returns object of type 'IList'
		"UserGroupsList": (10, 2, (9, 0), (), "UserGroupsList", '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}'),
		"UserName": (6, 2, (8, 0), (), "UserName", None),
		"UserSettings": (29, 2, (9, 0), (), "UserSettings", None),
		# Method 'UsersList' returns object of type 'IList'
		"UsersList": (32, 2, (9, 0), (), "UsersList", '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}'),
		"UsingProgress": (46, 2, (11, 0), (), "UsingProgress", None),
		"VCS": (12, 2, (9, 0), (), "VCS", None),
		"VMRepository": (59, 2, (8, 0), (), "VMRepository", None),
		"VcsDbRepository": (49, 2, (8, 0), (), "VcsDbRepository", None),
		"ViewsRepository": (48, 2, (8, 0), (), "ViewsRepository", None),
	}
	_prop_map_put_ = {
		"AllowReconnect": ((64, LCID, 4, 0),()),
		"ClientType": ((69, LCID, 4, 0),("u''",)),
		"IgnoreHtmlFormat": ((67, LCID, 4, 0),()),
		"KeepConnection": ((66, LCID, 4, 0),()),
		"UsingProgress": ((46, LCID, 4, 0),()),
	}

class ITDConnection2(DispatchBaseClass):
	"""ITDConnection2 Interface"""
	CLSID = IID('{4E3DA2DC-070F-4E73-BE64-2E91A8CA7DF8}')
	coclass_clsid = IID('{C5CBD7B2-490C-45F5-8C40-B8C3D108E6D7}')

	def ChangePassword(self, OldPassword=defaultNamedNotOptArg, NewPassword=defaultNamedNotOptArg):
		"""Changes the password for the currently logged in user."""
		return self._oleobj_.InvokeTypes(31, LCID, 1, (24, 0), ((8, 1), (8, 1)),OldPassword
			, NewPassword)

	def Connect(self, DomainName=defaultNamedNotOptArg, ProjectName=defaultNamedNotOptArg):
		"""Connects the logged-in user to the specified project in the  domain."""
		return self._oleobj_.InvokeTypes(82, LCID, 1, (24, 0), ((8, 1), (8, 1)),DomainName
			, ProjectName)

	def ConnectProject(self, ProjectName=defaultNamedNotOptArg, UserName=defaultNamedNotOptArg, Password=u''):
		"""Connects to the specified project."""
		return self._ApplyTypes_(15, 1, (24, 32), ((8, 0), (8, 0), (8, 49)), u'ConnectProject', None,ProjectName
			, UserName, Password)

	def ConnectProjectEx(self, DomainName=defaultNamedNotOptArg, ProjectName=defaultNamedNotOptArg, UserName=defaultNamedNotOptArg, Password=u''):
		"""Connects to the specified project in the specified domain."""
		return self._ApplyTypes_(54, 1, (24, 32), ((8, 1), (8, 1), (8, 1), (8, 49)), u'ConnectProjectEx', None,DomainName
			, ProjectName, UserName, Password)

	def ConnectToVCSAs(self, UserName=defaultNamedNotOptArg, Password=defaultNamedNotOptArg, CopyDesStep=defaultNamedNotOptArg):
		"""obsolete Method ConnectToVCSAs."""
		return self._oleobj_.InvokeTypes(56, LCID, 1, (24, 0), ((8, 0), (8, 0), (8, 0)),UserName
			, Password, CopyDesStep)

	# The method DirectoryPath is actually a property, but must be used as a method to correctly pass the arguments
	def DirectoryPath(self, nType=defaultNamedNotOptArg):
		"""The path of the server side respository directory for the database repository type."""
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(35, LCID, 2, (8, 0), ((3, 0),),nType
			)

	def Disconnect(self):
		"""Disconnects the user from the currently connected project."""
		return self._oleobj_.InvokeTypes(83, LCID, 1, (24, 0), (),)

	def DisconnectProject(self):
		"""Disconnects from the project."""
		return self._oleobj_.InvokeTypes(16, LCID, 1, (24, 0), (),)

	# Result is of type IList
	# The method Fields is actually a property, but must be used as a method to correctly pass the arguments
	def Fields(self, DataType=defaultNamedNotOptArg):
		"""A list of fields for the table specified in the DataType argument."""
		ret = self._oleobj_.InvokeTypes(27, LCID, 2, (9, 0), ((8, 1),),DataType
			)
		if ret is not None:
			ret = Dispatch(ret, u'Fields', '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')
		return ret

	def GetLicense(self, LicenseType=defaultNamedNotOptArg):
		"""Allocates a license type according to the LicenseType parameter"""
		return self._oleobj_.InvokeTypes(22, LCID, 1, (24, 0), ((3, 0),),LicenseType
			)

	def GetLicenseStatus(self, ClientType=defaultNamedNotOptArg, InUse=pythoncom.Missing, Max=pythoncom.Missing):
		"""The license status for the specified client type."""
		return self._ApplyTypes_(42, 1, (24, 0), ((3, 1), (16387, 2), (16387, 2)), u'GetLicenseStatus', None,ClientType
			, InUse, Max)

	def GetLicenses(self, LicensesType=defaultNamedNotOptArg, pVal=defaultNamedNotOptArg):
		"""Allocates multiple licenses types according to the LicensesType parameter."""
		return self._oleobj_.InvokeTypes(57, LCID, 1, (24, 0), ((3, 0), (16392, 0)),LicensesType
			, pVal)

	def GetTDVersion(self, pbsMajorVersion=pythoncom.Missing, pbsBuildNum=pythoncom.Missing):
		"""The major and minor versions of the OTA API."""
		return self._ApplyTypes_(72, 1, (24, 0), ((16392, 2), (16392, 2)), u'GetTDVersion', None,pbsMajorVersion
			, pbsBuildNum)

	def InitConnection(self, ServerName=defaultNamedNotOptArg, DomainName=u'', DomainPswd=u''):
		"""Initializes the Open Test Architecture API server connection for specified domain."""
		return self._ApplyTypes_(13, 1, (24, 32), ((8, 0), (8, 49), (8, 49)), u'InitConnection', None,ServerName
			, DomainName, DomainPswd)

	def InitConnectionEx(self, ServerName=defaultNamedNotOptArg):
		"""Initializes the  connection."""
		return self._oleobj_.InvokeTypes(52, LCID, 1, (24, 0), ((8, 0),),ServerName
			)

	def Login(self, UserName=defaultNamedNotOptArg, Password=defaultNamedNotOptArg):
		"""Confirms that the user is authorized to use Quality Center. If authorized, the user is logged in and can connect to projects."""
		return self._oleobj_.InvokeTypes(81, LCID, 1, (24, 0), ((8, 1), (8, 1)),UserName
			, Password)

	def Logout(self):
		"""Terminates the user's session on this TDConnection."""
		return self._oleobj_.InvokeTypes(84, LCID, 1, (24, 0), (),)

	# The method ModuleVisible is actually a property, but must be used as a method to correctly pass the arguments
	def ModuleVisible(self, ModuleID=defaultNamedNotOptArg):
		"""The specified module is visible to the connected user. This property is used for Quality Center UI programming and has no function in third-party applications."""
		return self._oleobj_.InvokeTypes(51, LCID, 2, (11, 0), ((3, 1),),ModuleID
			)

	# Result is of type IList
	# The method ProjectsListEx is actually a property, but must be used as a method to correctly pass the arguments
	def ProjectsListEx(self, DomainName=defaultNamedNotOptArg):
		"""The projects available within the specified domain."""
		ret = self._oleobj_.InvokeTypes(53, LCID, 2, (9, 0), ((8, 1),),DomainName
			)
		if ret is not None:
			ret = Dispatch(ret, u'ProjectsListEx', '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')
		return ret

	def PurgeRuns(self, TestSetFilter=defaultNamedNotOptArg, KeepLast=defaultNamedNotOptArg, DateUnit=defaultNamedNotOptArg, UnitCount=defaultNamedNotOptArg
			, StepsOnly=defaultNamedNotOptArg):
		"""Deletes the runs in this connection that match the input arguments."""
		return self._oleobj_.InvokeTypes(40, LCID, 1, (24, 0), ((8, 1), (3, 1), (12, 1), (3, 1), (11, 1)),TestSetFilter
			, KeepLast, DateUnit, UnitCount, StepsOnly)

	def ReleaseConnection(self):
		"""ReleaseConnection must be called for each call of an initialization method after finishing work with the Open Test Architecture API."""
		return self._oleobj_.InvokeTypes(14, LCID, 1, (24, 0), (),)

	def SendMail(self, SendTo=defaultNamedNotOptArg, SendFrom=u'', Subject=u'', Message=u''
			, attachArray=defaultNamedNotOptArg, bsFormat=u''):
		"""Sends Mail."""
		return self._ApplyTypes_(23, 1, (24, 32), ((8, 1), (8, 49), (8, 49), (8, 49), (12, 17), (8, 49)), u'SendMail', None,SendTo
			, SendFrom, Subject, Message, attachArray, bsFormat
			)

	# The method SetClientType is actually a property, but must be used as a method to correctly pass the arguments
	def SetClientType(self, newVal=defaultNamedNotOptArg, arg1=u''):
		"""Sets the Client type. For internal use."""
		return self._ApplyTypes_(69, 4, (24, 32), ((8, 1), (8, 49)), u'SetClientType', None,newVal
			, arg1)

	def SynchronizeFollowUps(self, Password=defaultNamedNotOptArg):
		"""Obsolete: Sends all previously unsent followups."""
		return self._oleobj_.InvokeTypes(65, LCID, 1, (24, 0), ((8, 1),),Password
			)

	# The method TDParams is actually a property, but must be used as a method to correctly pass the arguments
	def TDParams(self, Request=defaultNamedNotOptArg):
		"""Returns the value of the parameter whose name is specified by Request."""
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(45, LCID, 2, (8, 0), ((8, 1),),Request
			)

	# Result is of type IList
	# The method VisibleProjects is actually a property, but must be used as a method to correctly pass the arguments
	def VisibleProjects(self, DomainName=defaultNamedNotOptArg):
		"""The list of projects in the specified domain that the current logged in user is allowed to connect to."""
		ret = self._oleobj_.InvokeTypes(87, LCID, 2, (9, 0), ((8, 1),),DomainName
			)
		if ret is not None:
			ret = Dispatch(ret, u'VisibleProjects', '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')
		return ret

	_prop_map_get_ = {
		"ActionPermission": (21, 2, (9, 0), (), "ActionPermission", None),
		"AlertManager": (63, 2, (9, 0), (), "AlertManager", None),
		"AllowReconnect": (64, 2, (11, 0), (), "AllowReconnect", None),
		"Analysis": (58, 2, (9, 0), (), "Analysis", None),
		"AuditPropertyFactory": (78, 2, (9, 0), (), "AuditPropertyFactory", None),
		"AuditRecordFactory": (77, 2, (9, 0), (), "AuditRecordFactory", None),
		"BugFactory": (8, 2, (9, 0), (), "BugFactory", None),
		"ChangeFactory": (36, 2, (9, 0), (), "ChangeFactory", None),
		"CheckoutRepository": (47, 2, (8, 0), (), "CheckoutRepository", None),
		"Command": (18, 2, (9, 0), (), "Command", None),
		"CommonSettings": (28, 2, (9, 0), (), "CommonSettings", None),
		"ComponentFactory": (70, 2, (9, 0), (), "ComponentFactory", None),
		"ComponentFolderFactory": (71, 2, (9, 0), (), "ComponentFolderFactory", None),
		"Connected": (1, 2, (11, 0), (), "Connected", None),
		"Customization": (26, 2, (9, 0), (), "Customization", None),
		"DBManager": (25, 2, (9, 0), (), "DBManager", None),
		"DBName": (60, 2, (8, 0), (), "DBName", None),
		"DBType": (24, 2, (8, 0), (), "DBType", None),
		"DomainName": (43, 2, (8, 0), (), "DomainName", None),
		# Method 'DomainsList' returns object of type 'IList'
		"DomainsList": (55, 2, (9, 0), (), "DomainsList", '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}'),
		"ExtendedStorage": (34, 2, (9, 0), (), "ExtendedStorage", None),
		"GraphBuilder": (76, 2, (9, 0), (), "GraphBuilder", None),
		"HostFactory": (11, 2, (9, 0), (), "HostFactory", None),
		"HostGroupFactory": (30, 2, (9, 0), (), "HostGroupFactory", None),
		"IgnoreHtmlFormat": (67, 2, (11, 0), (), "IgnoreHtmlFormat", None),
		"IsSearchSupported": (80, 2, (11, 0), (), "IsSearchSupported", None),
		"KeepConnection": (66, 2, (11, 0), (), "KeepConnection", None),
		"LoggedIn": (85, 2, (11, 0), (), "LoggedIn", None),
		"MailConditions": (37, 2, (9, 0), (), "MailConditions", None),
		"Password": (33, 2, (8, 0), (), "Password", None),
		"ProductInfo": (75, 2, (9, 0), (), "ProductInfo", None),
		"ProjectConnected": (2, 2, (11, 0), (), "ProjectConnected", None),
		"ProjectName": (4, 2, (8, 0), (), "ProjectName", None),
		"ProjectProperties": (41, 2, (9, 0), (), "ProjectProperties", None),
		# Method 'ProjectsList' returns object of type 'IList'
		"ProjectsList": (17, 2, (9, 0), (), "ProjectsList", '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}'),
		"ReportRole": (68, 2, (8, 0), (), "ReportRole", None),
		"ReqFactory": (20, 2, (9, 0), (), "ReqFactory", None),
		"Rules": (61, 2, (9, 0), (), "Rules", None),
		"RunFactory": (50, 2, (9, 0), (), "RunFactory", None),
		"ServerName": (3, 2, (8, 0), (), "ServerName", None),
		"ServerTime": (38, 2, (7, 0), (), "ServerTime", None),
		"ServerURL": (73, 2, (8, 0), (), "ServerURL", None),
		"TDSettings": (39, 2, (9, 0), (), "TDSettings", None),
		"TSTestFactory": (79, 2, (9, 0), (), "TSTestFactory", None),
		"TestFactory": (7, 2, (9, 0), (), "TestFactory", None),
		"TestRepository": (5, 2, (8, 0), (), "TestRepository", None),
		"TestSetFactory": (9, 2, (9, 0), (), "TestSetFactory", None),
		"TestSetTreeManager": (62, 2, (9, 0), (), "TestSetTreeManager", None),
		"TextParam": (44, 2, (9, 0), (), "TextParam", None),
		"TreeManager": (19, 2, (9, 0), (), "TreeManager", None),
		# Method 'UserGroupsList' returns object of type 'IList'
		"UserGroupsList": (10, 2, (9, 0), (), "UserGroupsList", '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}'),
		"UserName": (6, 2, (8, 0), (), "UserName", None),
		"UserSettings": (29, 2, (9, 0), (), "UserSettings", None),
		# Method 'UsersList' returns object of type 'IList'
		"UsersList": (32, 2, (9, 0), (), "UsersList", '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}'),
		"UsingProgress": (46, 2, (11, 0), (), "UsingProgress", None),
		"VCS": (12, 2, (9, 0), (), "VCS", None),
		"VMRepository": (59, 2, (8, 0), (), "VMRepository", None),
		"VcsDbRepository": (49, 2, (8, 0), (), "VcsDbRepository", None),
		"ViewsRepository": (48, 2, (8, 0), (), "ViewsRepository", None),
		# Method 'VisibleDomains' returns object of type 'IList'
		"VisibleDomains": (86, 2, (9, 0), (), "VisibleDomains", '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}'),
	}
	_prop_map_put_ = {
		"AllowReconnect": ((64, LCID, 4, 0),()),
		"ClientType": ((69, LCID, 4, 0),("u''",)),
		"IgnoreHtmlFormat": ((67, LCID, 4, 0),()),
		"KeepConnection": ((66, LCID, 4, 0),()),
		"UsingProgress": ((46, LCID, 4, 0),()),
	}

class ITDErrorInfo(DispatchBaseClass):
	"""ITDErrorInfo Interface"""
	CLSID = IID('{DEBC6003-2ABE-4C93-A769-646F300A7986}')
	coclass_clsid = IID('{5BC906B1-56F3-4DC0-A6BB-78440F908823}')

	def GetDescription(self, pBstrDescription=pythoncom.Missing):
		return self._ApplyTypes_(1610678274, 1, (24, 0), ((16392, 2),), u'GetDescription', None,pBstrDescription
			)

	def GetGUID(self, pGUID=pythoncom.Missing):
		return self._ApplyTypes_(1610678272, 1, (24, 0), ((36, 2),), u'GetGUID', None,pGUID
			)

	def GetHelpContext(self, pdwHelpContext=pythoncom.Missing):
		return self._ApplyTypes_(1610678276, 1, (24, 0), ((16403, 2),), u'GetHelpContext', None,pdwHelpContext
			)

	def GetHelpFile(self, pBstrHelpFile=pythoncom.Missing):
		return self._ApplyTypes_(1610678275, 1, (24, 0), ((16392, 2),), u'GetHelpFile', None,pBstrHelpFile
			)

	def GetSource(self, pBstrSource=pythoncom.Missing):
		return self._ApplyTypes_(1610678273, 1, (24, 0), ((16392, 2),), u'GetSource', None,pBstrSource
			)

	_prop_map_get_ = {
		"Details": (1, 2, (8, 0), (), "Details", None),
	}
	_prop_map_put_ = {
	}

class ITDField(DispatchBaseClass):
	"""Represents a field with field system properties and other properties."""
	CLSID = IID('{3D416474-2373-446E-9090-DBC083B6C382}')
	coclass_clsid = IID('{20F3E82A-9A7D-44FA-94C6-03E84C49521D}')

	def IsValidValue(self, Value=defaultNamedNotOptArg, pObject=defaultNamedNotOptArg):
		"""Checks if the input value is valid for the field."""
		return self._oleobj_.InvokeTypes(3, LCID, 1, (24, 0), ((12, 1), (9, 1)),Value
			, pObject)

	_prop_map_get_ = {
		"Name": (0, 2, (8, 0), (), "Name", None),
		"Property": (2, 2, (9, 0), (), "Property", None),
		"Type": (1, 2, (3, 0), (), "Type", None),
	}
	_prop_map_put_ = {
	}
	# Default property for this class is 'Name'
	def __call__(self):
		return self._ApplyTypes_(*(0, 2, (8, 0), (), "Name", None))
	# str(ob) and int(ob) will use __call__
	def __unicode__(self, *args):
		try:
			return unicode(self.__call__(*args))
		except pythoncom.com_error:
			return repr(self)
	def __str__(self, *args):
		return str(self.__unicode__(*args))
	def __int__(self, *args):
		return int(self.__call__(*args))

class ITDFilter(DispatchBaseClass):
	"""Services for creation of a filtered list of field objects without SQL."""
	CLSID = IID('{452897AD-D9F8-4EAE-80DB-B9C11807507F}')
	coclass_clsid = None

	# The method CaseSensitive is actually a property, but must be used as a method to correctly pass the arguments
	def CaseSensitive(self, FieldName=defaultNamedNotOptArg):
		"""Indicates if the filter for the specified field is case sensitive. The default is False."""
		return self._oleobj_.InvokeTypes(7, LCID, 2, (11, 0), ((8, 1),),FieldName
			)

	def Clear(self):
		"""Clears the current filter."""
		return self._oleobj_.InvokeTypes(3, LCID, 1, (24, 0), (),)

	# The method Filter is actually a property, but must be used as a method to correctly pass the arguments
	def Filter(self, FieldName=defaultNamedNotOptArg):
		"""The filter condition for the field specified by the FieldName."""
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(0, LCID, 2, (8, 0), ((8, 1),),FieldName
			)

	# Result is of type IList
	def NewList(self):
		"""Creates a new list from current filter."""
		ret = self._oleobj_.InvokeTypes(4, LCID, 1, (9, 0), (),)
		if ret is not None:
			ret = Dispatch(ret, u'NewList', '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')
		return ret

	# The method Order is actually a property, but must be used as a method to correctly pass the arguments
	def Order(self, FieldName=defaultNamedNotOptArg):
		"""The position of the specified field in the list of the sort fields. (zero-based)"""
		return self._oleobj_.InvokeTypes(1, LCID, 2, (3, 0), ((8, 1),),FieldName
			)

	# The method OrderDirection is actually a property, but must be used as a method to correctly pass the arguments
	def OrderDirection(self, FieldName=defaultNamedNotOptArg):
		"""The sort order: TDOLE_ASCENDING or TDOLE_DESCENDING"""
		return self._oleobj_.InvokeTypes(2, LCID, 2, (2, 0), ((8, 1),),FieldName
			)

	def Refresh(self):
		"""Reads saved data and refreshes the fetched list according to the current filter, overwriting values in memory."""
		return self._oleobj_.InvokeTypes(6, LCID, 1, (24, 0), (),)

	# The method SetCaseSensitive is actually a property, but must be used as a method to correctly pass the arguments
	def SetCaseSensitive(self, FieldName=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""Indicates if the filter for the specified field is case sensitive. The default is False."""
		return self._oleobj_.InvokeTypes(7, LCID, 4, (24, 0), ((8, 1), (11, 1)),FieldName
			, arg1)

	# The method SetFilter is actually a property, but must be used as a method to correctly pass the arguments
	def SetFilter(self, FieldName=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The filter condition for the field specified by the FieldName."""
		return self._oleobj_.InvokeTypes(0, LCID, 4, (24, 0), ((8, 1), (8, 1)),FieldName
			, arg1)

	# The method SetOrder is actually a property, but must be used as a method to correctly pass the arguments
	def SetOrder(self, FieldName=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The position of the specified field in the list of the sort fields. (zero-based)"""
		return self._oleobj_.InvokeTypes(1, LCID, 4, (24, 0), ((8, 1), (3, 1)),FieldName
			, arg1)

	# The method SetOrderDirection is actually a property, but must be used as a method to correctly pass the arguments
	def SetOrderDirection(self, FieldName=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The sort order: TDOLE_ASCENDING or TDOLE_DESCENDING"""
		return self._oleobj_.InvokeTypes(2, LCID, 4, (24, 0), ((8, 1), (2, 1)),FieldName
			, arg1)

	# The method SetUFilter is actually a property, but must be used as a method to correctly pass the arguments
	def SetUFilter(self, EntityType=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The Union Filter according to the entity type."""
		return self._oleobj_.InvokeTypes(10, LCID, 4, (24, 0), ((8, 1), (8, 1)),EntityType
			, arg1)

	# The method SetXFilter is actually a property, but must be used as a method to correctly pass the arguments
	def SetXFilter(self, EntityType=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The Intersection Filter according to the entity type."""
		return self._oleobj_.InvokeTypes(9, LCID, 4, (24, 0), ((8, 1), (8, 1)),EntityType
			, arg1)

	# The method UFilter is actually a property, but must be used as a method to correctly pass the arguments
	def UFilter(self, EntityType=defaultNamedNotOptArg):
		"""The Union Filter according to the entity type."""
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(10, LCID, 2, (8, 0), ((8, 1),),EntityType
			)

	# The method XFilter is actually a property, but must be used as a method to correctly pass the arguments
	def XFilter(self, EntityType=defaultNamedNotOptArg):
		"""The Intersection Filter according to the entity type."""
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(9, LCID, 2, (8, 0), ((8, 1),),EntityType
			)

	_prop_map_get_ = {
		"DataType": (11, 2, (8, 0), (), "DataType", None),
		# Method 'Fields' returns object of type 'IList'
		"Fields": (5, 2, (9, 0), (), "Fields", '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}'),
		"Text": (8, 2, (8, 0), (), "Text", None),
		"_Text": (12, 2, (8, 0), (), "_Text", None),
	}
	_prop_map_put_ = {
		"Text": ((8, LCID, 4, 0),()),
		"_Text": ((12, LCID, 4, 0),()),
	}
	# Default method for this class is 'Filter'
	def __call__(self, FieldName=defaultNamedNotOptArg):
		"""The filter condition for the field specified by the FieldName."""
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(0, LCID, 2, (8, 0), ((8, 1),),FieldName
			)

	# str(ob) and int(ob) will use __call__
	def __unicode__(self, *args):
		try:
			return unicode(self.__call__(*args))
		except pythoncom.com_error:
			return repr(self)
	def __str__(self, *args):
		return str(self.__unicode__(*args))
	def __int__(self, *args):
		return int(self.__call__(*args))

class ITDFilter2(DispatchBaseClass):
	"""Services for creation of a filtered list of field objects without SQL."""
	CLSID = IID('{B14CE979-E8D8-426B-AFCF-B8AA9AFE267B}')
	coclass_clsid = IID('{6F671E95-2720-4583-BCD8-7CC3E5D3C4AF}')

	# The method CaseSensitive is actually a property, but must be used as a method to correctly pass the arguments
	def CaseSensitive(self, FieldName=defaultNamedNotOptArg):
		"""Indicates if the filter for the specified field is case sensitive. The default is False."""
		return self._oleobj_.InvokeTypes(7, LCID, 2, (11, 0), ((8, 1),),FieldName
			)

	def Clear(self):
		"""Clears the current filter."""
		return self._oleobj_.InvokeTypes(3, LCID, 1, (24, 0), (),)

	# The method Filter is actually a property, but must be used as a method to correctly pass the arguments
	def Filter(self, FieldName=defaultNamedNotOptArg):
		"""The filter condition for the field specified by the FieldName."""
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(0, LCID, 2, (8, 0), ((8, 1),),FieldName
			)

	def GetXFilter(self, JoinEntities=defaultNamedNotOptArg, Inclusive=pythoncom.Missing):
		"""Gets the intersection filter specified by JoinEntities."""
		return self._ApplyTypes_(14, 1, (8, 0), ((8, 1), (16395, 2)), u'GetXFilter', None,JoinEntities
			, Inclusive)

	def IsClear(self):
		"""Checks whether the filter is clear, including cross-filter settings"""
		return self._oleobj_.InvokeTypes(15, LCID, 1, (11, 0), (),)

	# Result is of type IList
	def NewList(self):
		"""Creates a new list from current filter."""
		ret = self._oleobj_.InvokeTypes(4, LCID, 1, (9, 0), (),)
		if ret is not None:
			ret = Dispatch(ret, u'NewList', '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')
		return ret

	# The method Order is actually a property, but must be used as a method to correctly pass the arguments
	def Order(self, FieldName=defaultNamedNotOptArg):
		"""The position of the specified field in the list of the sort fields. (zero-based)"""
		return self._oleobj_.InvokeTypes(1, LCID, 2, (3, 0), ((8, 1),),FieldName
			)

	# The method OrderDirection is actually a property, but must be used as a method to correctly pass the arguments
	def OrderDirection(self, FieldName=defaultNamedNotOptArg):
		"""The sort order: TDOLE_ASCENDING or TDOLE_DESCENDING"""
		return self._oleobj_.InvokeTypes(2, LCID, 2, (2, 0), ((8, 1),),FieldName
			)

	def Refresh(self):
		"""Reads saved data and refreshes the fetched list according to the current filter, overwriting values in memory."""
		return self._oleobj_.InvokeTypes(6, LCID, 1, (24, 0), (),)

	# The method SetCaseSensitive is actually a property, but must be used as a method to correctly pass the arguments
	def SetCaseSensitive(self, FieldName=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""Indicates if the filter for the specified field is case sensitive. The default is False."""
		return self._oleobj_.InvokeTypes(7, LCID, 4, (24, 0), ((8, 1), (11, 1)),FieldName
			, arg1)

	# The method SetFilter is actually a property, but must be used as a method to correctly pass the arguments
	def SetFilter(self, FieldName=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The filter condition for the field specified by the FieldName."""
		return self._oleobj_.InvokeTypes(0, LCID, 4, (24, 0), ((8, 1), (8, 1)),FieldName
			, arg1)

	# The method SetOrder is actually a property, but must be used as a method to correctly pass the arguments
	def SetOrder(self, FieldName=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The position of the specified field in the list of the sort fields. (zero-based)"""
		return self._oleobj_.InvokeTypes(1, LCID, 4, (24, 0), ((8, 1), (3, 1)),FieldName
			, arg1)

	# The method SetOrderDirection is actually a property, but must be used as a method to correctly pass the arguments
	def SetOrderDirection(self, FieldName=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The sort order: TDOLE_ASCENDING or TDOLE_DESCENDING"""
		return self._oleobj_.InvokeTypes(2, LCID, 4, (24, 0), ((8, 1), (2, 1)),FieldName
			, arg1)

	# The method SetUFilter is actually a property, but must be used as a method to correctly pass the arguments
	def SetUFilter(self, EntityType=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The Union Filter according to the entity type."""
		return self._oleobj_.InvokeTypes(10, LCID, 4, (24, 0), ((8, 1), (8, 1)),EntityType
			, arg1)

	# The method SetXFilter is actually a property, but must be used as a method to correctly pass the arguments
	def SetXFilter(self, EntityType=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The Intersection Filter according to the entity type."""
		return self._oleobj_.InvokeTypes(9, LCID, 4, (24, 0), ((8, 1), (8, 1)),EntityType
			, arg1)

	# The method UFilter is actually a property, but must be used as a method to correctly pass the arguments
	def UFilter(self, EntityType=defaultNamedNotOptArg):
		"""The Union Filter according to the entity type."""
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(10, LCID, 2, (8, 0), ((8, 1),),EntityType
			)

	# The method XFilter is actually a property, but must be used as a method to correctly pass the arguments
	def XFilter(self, EntityType=defaultNamedNotOptArg):
		"""The Intersection Filter according to the entity type."""
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(9, LCID, 2, (8, 0), ((8, 1),),EntityType
			)

	_prop_map_get_ = {
		"DataType": (11, 2, (8, 0), (), "DataType", None),
		# Method 'Fields' returns object of type 'IList'
		"Fields": (5, 2, (9, 0), (), "Fields", '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}'),
		"Text": (8, 2, (8, 0), (), "Text", None),
		"_Text": (12, 2, (8, 0), (), "_Text", None),
	}
	_prop_map_put_ = {
		"Text": ((8, LCID, 4, 0),()),
		"_Text": ((12, LCID, 4, 0),()),
	}
	# Default method for this class is 'Filter'
	def __call__(self, FieldName=defaultNamedNotOptArg):
		"""The filter condition for the field specified by the FieldName."""
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(0, LCID, 2, (8, 0), ((8, 1),),FieldName
			)

	# str(ob) and int(ob) will use __call__
	def __unicode__(self, *args):
		try:
			return unicode(self.__call__(*args))
		except pythoncom.com_error:
			return repr(self)
	def __str__(self, *args):
		return str(self.__unicode__(*args))
	def __int__(self, *args):
		return int(self.__call__(*args))

class ITDMailConditions(DispatchBaseClass):
	"""Responsible for configuration of automatic mail notifications."""
	CLSID = IID('{FCA0B016-AC98-4E63-A7EE-34759BF8038C}')
	coclass_clsid = IID('{90A0FD6D-D4F5-4951-AA4D-3954864997C6}')

	def Close(self, NeedToSave=defaultNamedNotOptArg):
		"""Closes and updates Mail Condition."""
		return self._oleobj_.InvokeTypes(7, LCID, 1, (24, 0), ((11, 0),),NeedToSave
			)

	# The method Condition is actually a property, but must be used as a method to correctly pass the arguments
	def Condition(self, Name=defaultNamedNotOptArg, IsUserCondition=defaultNamedNotOptArg):
		"""The defect-matching rule."""
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(5, LCID, 2, (8, 0), ((8, 0), (11, 0)),Name
			, IsUserCondition)

	def DeleteCondition(self, Name=defaultNamedNotOptArg, IsUserCondition=defaultNamedNotOptArg):
		"""Deletes the Mail Condition."""
		return self._oleobj_.InvokeTypes(6, LCID, 1, (24, 0), ((8, 0), (11, 0)),Name
			, IsUserCondition)

	# Result is of type IList
	# The method GetItemList is actually a property, but must be used as a method to correctly pass the arguments
	def GetItemList(self, GetRealNames=False):
		"""Gets the list of Mail Condition Users."""
		ret = self._oleobj_.InvokeTypes(8, LCID, 2, (9, 0), ((11, 49),),GetRealNames
			)
		if ret is not None:
			ret = Dispatch(ret, u'GetItemList', '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')
		return ret

	def Load(self):
		"""Loads Mail Conditions."""
		return self._oleobj_.InvokeTypes(4, LCID, 1, (24, 0), (),)

	def LockObject(self):
		"""Locks the object. Returns True if lock successful."""
		return self._oleobj_.InvokeTypes(2, LCID, 1, (11, 0), (),)

	# The method SetCondition is actually a property, but must be used as a method to correctly pass the arguments
	def SetCondition(self, Name=defaultNamedNotOptArg, IsUserCondition=defaultNamedNotOptArg, arg2=defaultUnnamedArg):
		"""The defect-matching rule."""
		return self._oleobj_.InvokeTypes(5, LCID, 4, (24, 0), ((8, 0), (11, 0), (8, 1)),Name
			, IsUserCondition, arg2)

	def UnLockObject(self):
		"""Unlocks the object."""
		return self._oleobj_.InvokeTypes(3, LCID, 1, (24, 0), (),)

	_prop_map_get_ = {
		"IsLocked": (1, 2, (11, 0), (), "IsLocked", None),
		# Method 'ItemList' returns object of type 'IList'
		"ItemList": (8, 2, (9, 0), ((11, 49),), "ItemList", '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}'),
	}
	_prop_map_put_ = {
	}

class ITDUtils(DispatchBaseClass):
	"""ITDUtils Interface"""
	CLSID = IID('{BB603787-CBD0-48CD-AA8A-B4532DC714D1}')
	coclass_clsid = IID('{977FEB6A-82DF-4F53-ADA2-F722F7E07D23}')

	def Encrypt(self, strToEncrypt=defaultNamedNotOptArg):
		"""method Encrypt"""
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(1, LCID, 1, (8, 0), ((8, 0),),strToEncrypt
			)

	_prop_map_get_ = {
		"CurrentTimeStamp": (3, 2, (8, 0), (), "CurrentTimeStamp", None),
	}
	_prop_map_put_ = {
	}

class ITSScheduler(DispatchBaseClass):
	"""Responsible for executing selected automated tests."""
	CLSID = IID('{E99AC0C8-46AB-42C3-9CB2-9C9AEC35A861}')
	coclass_clsid = IID('{C121682C-6B6E-4A3F-A1CE-9E78FE897009}')

	def Run(self, TestData=defaultNamedOptArg):
		"""Starts execution the test set or of the specified tests."""
		return self._oleobj_.InvokeTypes(1, LCID, 1, (24, 0), ((12, 17),),TestData
			)

	# The method RunOnHost is actually a property, but must be used as a method to correctly pass the arguments
	def RunOnHost(self, TSTestId=defaultNamedNotOptArg):
		"""The host on which to execute the test instance."""
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(9, LCID, 2, (8, 0), ((12, 1),),TSTestId
			)

	# The method SetRunOnHost is actually a property, but must be used as a method to correctly pass the arguments
	def SetRunOnHost(self, TSTestId=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The host on which to execute the test instance."""
		return self._oleobj_.InvokeTypes(9, LCID, 4, (24, 0), ((12, 1), (8, 1)),TSTestId
			, arg1)

	# The method SetVM_Config is actually a property, but must be used as a method to correctly pass the arguments
	def SetVM_Config(self, TSTestId=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""Obsolete. Gets VM Ware Configuration."""
		return self._oleobj_.InvokeTypes(10, LCID, 4, (24, 0), ((12, 1), (8, 1)),TSTestId
			, arg1)

	def Stop(self, TestData=defaultNamedOptArg):
		"""Stops execution of the test set or the specified tests."""
		return self._oleobj_.InvokeTypes(2, LCID, 1, (24, 0), ((12, 17),),TestData
			)

	# The method VM_Config is actually a property, but must be used as a method to correctly pass the arguments
	def VM_Config(self, TSTestId=defaultNamedNotOptArg):
		"""Obsolete. Gets VM Ware Configuration."""
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(10, LCID, 2, (8, 0), ((12, 1),),TSTestId
			)

	_prop_map_get_ = {
		"ExecutionLog": (8, 2, (8, 0), (), "ExecutionLog", None),
		"ExecutionStatus": (7, 2, (9, 0), (), "ExecutionStatus", None),
		"HostTimeOut": (5, 2, (3, 0), (), "HostTimeOut", None),
		"LogEnabled": (3, 2, (11, 0), (), "LogEnabled", None),
		"RunAllLocally": (6, 2, (11, 0), (), "RunAllLocally", None),
		"TdHostName": (4, 2, (8, 0), (), "TdHostName", None),
	}
	_prop_map_put_ = {
		"HostTimeOut": ((5, LCID, 4, 0),()),
		"LogEnabled": ((3, LCID, 4, 0),()),
		"RunAllLocally": ((6, LCID, 4, 0),()),
		"TdHostName": ((4, LCID, 4, 0),()),
	}

class ITSTest(DispatchBaseClass):
	"""Represents a test instance, or execution test, in a test set. A test instance represents a single use of a planning Test in a TestSet. It can be associated with one or more runs."""
	CLSID = IID('{FFE8FF09-7522-448E-933E-724B6A149887}')
	coclass_clsid = None

	# The method Field is actually a property, but must be used as a method to correctly pass the arguments
	def Field(self, FieldName=defaultNamedNotOptArg):
		"""The value of the specified field."""
		return self._ApplyTypes_(0, 2, (12, 0), ((8, 0),), u'Field', None,FieldName
			)

	def LockObject(self):
		"""Locks the object. Returns True if lock successful."""
		return self._oleobj_.InvokeTypes(2, LCID, 1, (11, 0), (),)

	def Post(self):
		"""Posts all changed values into database."""
		return self._oleobj_.InvokeTypes(6, LCID, 1, (24, 0), (),)

	def Refresh(self):
		"""Reads saved values, overwriting values in memory."""
		return self._oleobj_.InvokeTypes(7, LCID, 1, (24, 0), (),)

	# The method SetField is actually a property, but must be used as a method to correctly pass the arguments
	def SetField(self, FieldName=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The value of the specified field."""
		return self._oleobj_.InvokeTypes(0, LCID, 4, (24, 0), ((8, 0), (12, 1)),FieldName
			, arg1)

	def UnLockObject(self):
		"""Unlocks the object."""
		return self._oleobj_.InvokeTypes(3, LCID, 1, (24, 0), (),)

	def Undo(self):
		"""Undoes changes to field values that have not been posted."""
		return self._oleobj_.InvokeTypes(8, LCID, 1, (24, 0), (),)

	_prop_map_get_ = {
		"Attachments": (12, 2, (9, 0), (), "Attachments", None),
		"AutoPost": (5, 2, (11, 0), (), "AutoPost", None),
		"ExecutionParams": (21, 2, (8, 0), (), "ExecutionParams", None),
		"ExecutionSettings": (24, 2, (9, 0), (), "ExecutionSettings", None),
		"HasAttachment": (13, 2, (11, 0), (), "HasAttachment", None),
		"HasSteps": (20, 2, (11, 0), (), "HasSteps", None),
		"History": (11, 2, (9, 0), (), "History", None),
		"HostName": (17, 2, (8, 0), (), "HostName", None),
		"ID": (4, 2, (12, 0), (), "ID", None),
		"Instance": (26, 2, (3, 0), (), "Instance", None),
		"IsLocked": (1, 2, (11, 0), (), "IsLocked", None),
		"LastRun": (22, 2, (9, 0), (), "LastRun", None),
		"Modified": (9, 2, (11, 0), (), "Modified", None),
		"Name": (18, 2, (8, 0), (), "Name", None),
		"Params": (23, 2, (9, 0), (), "Params", None),
		"RunFactory": (15, 2, (9, 0), (), "RunFactory", None),
		"Status": (14, 2, (8, 0), (), "Status", None),
		"Test": (16, 2, (9, 0), (), "Test", None),
		"TestId": (25, 2, (12, 0), (), "TestId", None),
		"TestName": (27, 2, (8, 0), (), "TestName", None),
		"TestSet": (28, 2, (9, 0), (), "TestSet", None),
		"Type": (19, 2, (8, 0), (), "Type", None),
		"Virtual": (10, 2, (11, 0), (), "Virtual", None),
	}
	_prop_map_put_ = {
		"AutoPost": ((5, LCID, 4, 0),()),
		"ExecutionParams": ((21, LCID, 4, 0),()),
		"HostName": ((17, LCID, 4, 0),()),
		"Status": ((14, LCID, 4, 0),()),
	}
	# Default method for this class is 'Field'
	def __call__(self, FieldName=defaultNamedNotOptArg):
		"""The value of the specified field."""
		return self._ApplyTypes_(0, 2, (12, 0), ((8, 0),), '__call__', None,FieldName
			)

	# str(ob) and int(ob) will use __call__
	def __unicode__(self, *args):
		try:
			return unicode(self.__call__(*args))
		except pythoncom.com_error:
			return repr(self)
	def __str__(self, *args):
		return str(self.__unicode__(*args))
	def __int__(self, *args):
		return int(self.__call__(*args))

class ITSTest2(DispatchBaseClass):
	"""Represents a test instance, or execution test, in a test set. A test instance represents a single use of a planning Test in a TestSet. It can be associated with one or more runs."""
	CLSID = IID('{133B9314-9A98-4659-866D-4BA8DCD4A643}')
	coclass_clsid = IID('{488D6BE8-A962-4958-8E11-6E2EE9A20EFF}')

	# The method Field is actually a property, but must be used as a method to correctly pass the arguments
	def Field(self, FieldName=defaultNamedNotOptArg):
		"""The value of the specified field."""
		return self._ApplyTypes_(0, 2, (12, 0), ((8, 0),), u'Field', None,FieldName
			)

	def LockObject(self):
		"""Locks the object. Returns True if lock successful."""
		return self._oleobj_.InvokeTypes(2, LCID, 1, (11, 0), (),)

	def Post(self):
		"""Posts all changed values into database."""
		return self._oleobj_.InvokeTypes(6, LCID, 1, (24, 0), (),)

	def Refresh(self):
		"""Reads saved values, overwriting values in memory."""
		return self._oleobj_.InvokeTypes(7, LCID, 1, (24, 0), (),)

	# The method SetField is actually a property, but must be used as a method to correctly pass the arguments
	def SetField(self, FieldName=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The value of the specified field."""
		return self._oleobj_.InvokeTypes(0, LCID, 4, (24, 0), ((8, 0), (12, 1)),FieldName
			, arg1)

	def UnLockObject(self):
		"""Unlocks the object."""
		return self._oleobj_.InvokeTypes(3, LCID, 1, (24, 0), (),)

	def Undo(self):
		"""Undoes changes to field values that have not been posted."""
		return self._oleobj_.InvokeTypes(8, LCID, 1, (24, 0), (),)

	_prop_map_get_ = {
		"Attachments": (12, 2, (9, 0), (), "Attachments", None),
		"AutoPost": (5, 2, (11, 0), (), "AutoPost", None),
		"ExecutionParams": (21, 2, (8, 0), (), "ExecutionParams", None),
		"ExecutionSettings": (24, 2, (9, 0), (), "ExecutionSettings", None),
		"HasAttachment": (13, 2, (11, 0), (), "HasAttachment", None),
		"HasCoverage": (29, 2, (11, 0), (), "HasCoverage", None),
		"HasSteps": (20, 2, (11, 0), (), "HasSteps", None),
		"History": (11, 2, (9, 0), (), "History", None),
		"HostName": (17, 2, (8, 0), (), "HostName", None),
		"ID": (4, 2, (12, 0), (), "ID", None),
		"Instance": (26, 2, (3, 0), (), "Instance", None),
		"IsLocked": (1, 2, (11, 0), (), "IsLocked", None),
		"LastRun": (22, 2, (9, 0), (), "LastRun", None),
		"Modified": (9, 2, (11, 0), (), "Modified", None),
		"Name": (18, 2, (8, 0), (), "Name", None),
		"Params": (23, 2, (9, 0), (), "Params", None),
		"RunFactory": (15, 2, (9, 0), (), "RunFactory", None),
		"Status": (14, 2, (8, 0), (), "Status", None),
		"Test": (16, 2, (9, 0), (), "Test", None),
		"TestId": (25, 2, (12, 0), (), "TestId", None),
		"TestName": (27, 2, (8, 0), (), "TestName", None),
		"TestSet": (28, 2, (9, 0), (), "TestSet", None),
		"Type": (19, 2, (8, 0), (), "Type", None),
		"Virtual": (10, 2, (11, 0), (), "Virtual", None),
	}
	_prop_map_put_ = {
		"AutoPost": ((5, LCID, 4, 0),()),
		"ExecutionParams": ((21, LCID, 4, 0),()),
		"HostName": ((17, LCID, 4, 0),()),
		"Status": ((14, LCID, 4, 0),()),
	}
	# Default method for this class is 'Field'
	def __call__(self, FieldName=defaultNamedNotOptArg):
		"""The value of the specified field."""
		return self._ApplyTypes_(0, 2, (12, 0), ((8, 0),), '__call__', None,FieldName
			)

	# str(ob) and int(ob) will use __call__
	def __unicode__(self, *args):
		try:
			return unicode(self.__call__(*args))
		except pythoncom.com_error:
			return repr(self)
	def __str__(self, *args):
		return str(self.__unicode__(*args))
	def __int__(self, *args):
		return int(self.__call__(*args))

class ITest(DispatchBaseClass):
	"""Represents a planning test."""
	CLSID = IID('{4777BC00-DDBD-4DBD-ACE6-BEAE379E2051}')
	coclass_clsid = IID('{FBED7041-8CD1-471D-A1D8-C15A7C1D4CE9}')

	def CoverRequirement(self, Req=defaultNamedNotOptArg, Order=defaultNamedNotOptArg, Recursive=defaultNamedNotOptArg):
		"""Checks if this test covers the specified requirement."""
		return self._oleobj_.InvokeTypes(20, LCID, 1, (3, 0), ((12, 1), (3, 1), (11, 1)),Req
			, Order, Recursive)

	# The method Field is actually a property, but must be used as a method to correctly pass the arguments
	def Field(self, FieldName=defaultNamedNotOptArg):
		"""The value of the specified field."""
		return self._ApplyTypes_(0, 2, (12, 0), ((8, 0),), u'Field', None,FieldName
			)

	# The method FullPathEx is actually a property, but must be used as a method to correctly pass the arguments
	def FullPathEx(self, isOrgFullPath=defaultNamedNotOptArg):
		"""For future use."""
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(34, LCID, 2, (8, 0), ((3, 1),),isOrgFullPath
			)

	# Result is of type IList
	def GetCoverList(self):
		"""Gets the list of all requirements covered by this test."""
		ret = self._oleobj_.InvokeTypes(22, LCID, 1, (9, 0), (),)
		if ret is not None:
			ret = Dispatch(ret, u'GetCoverList', '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')
		return ret

	def LockObject(self):
		"""Locks the object. Returns True if lock successful."""
		return self._oleobj_.InvokeTypes(2, LCID, 1, (11, 0), (),)

	def Mail(self, SendTo=defaultNamedNotOptArg, SendCc=u'', Option=0, Subject=u''
			, Comment=u''):
		"""Mails the IBaseFieldExMail field item."""
		return self._ApplyTypes_(14, 1, (24, 32), ((8, 1), (8, 49), (3, 49), (8, 49), (8, 49)), u'Mail', None,SendTo
			, SendCc, Option, Subject, Comment)

	def Post(self):
		"""Posts all changed values into database."""
		return self._oleobj_.InvokeTypes(6, LCID, 1, (24, 0), (),)

	def Refresh(self):
		"""Reads saved values, overwriting values in memory."""
		return self._oleobj_.InvokeTypes(7, LCID, 1, (24, 0), (),)

	def RemoveCoverage(self, Req=defaultNamedNotOptArg):
		"""Removes a requirement from the list of requirements this test covers."""
		return self._oleobj_.InvokeTypes(21, LCID, 1, (24, 0), ((12, 1),),Req
			)

	# The method SetField is actually a property, but must be used as a method to correctly pass the arguments
	def SetField(self, FieldName=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The value of the specified field."""
		return self._oleobj_.InvokeTypes(0, LCID, 4, (24, 0), ((8, 0), (12, 1)),FieldName
			, arg1)

	def UnLockObject(self):
		"""Unlocks the object."""
		return self._oleobj_.InvokeTypes(3, LCID, 1, (24, 0), (),)

	def Undo(self):
		"""Undoes changes to field values that have not been posted."""
		return self._oleobj_.InvokeTypes(8, LCID, 1, (24, 0), (),)

	_prop_map_get_ = {
		"Attachments": (12, 2, (9, 0), (), "Attachments", None),
		"AutoPost": (5, 2, (11, 0), (), "AutoPost", None),
		"CheckoutPathIfExist": (32, 2, (8, 0), (), "CheckoutPathIfExist", None),
		"DesStepsNum": (19, 2, (3, 0), (), "DesStepsNum", None),
		"DesignStepFactory": (18, 2, (9, 0), (), "DesignStepFactory", None),
		"ExecDate": (27, 2, (7, 0), (), "ExecDate", None),
		"ExecStatus": (24, 2, (8, 0), (), "ExecStatus", None),
		"ExtendedStorage": (23, 2, (9, 0), (), "ExtendedStorage", None),
		"FullPath": (15, 2, (8, 0), (), "FullPath", None),
		"HasAttachment": (13, 2, (11, 0), (), "HasAttachment", None),
		"HasCoverage": (25, 2, (11, 0), (), "HasCoverage", None),
		"HasParam": (30, 2, (11, 0), (), "HasParam", None),
		"History": (11, 2, (9, 0), (), "History", None),
		"ID": (4, 2, (12, 0), (), "ID", None),
		"IgnoreDataHiding": (33, 2, (11, 0), (), "IgnoreDataHiding", None),
		"IsLocked": (1, 2, (11, 0), (), "IsLocked", None),
		"LastRun": (26, 2, (9, 0), (), "LastRun", None),
		"Modified": (9, 2, (11, 0), (), "Modified", None),
		"Name": (17, 2, (8, 0), (), "Name", None),
		"Params": (29, 2, (9, 0), (), "Params", None),
		"TemplateTest": (28, 2, (11, 0), (), "TemplateTest", None),
		"Type": (16, 2, (8, 0), (), "Type", None),
		"VCS": (31, 2, (9, 0), (), "VCS", None),
		"Virtual": (10, 2, (11, 0), (), "Virtual", None),
	}
	_prop_map_put_ = {
		"AutoPost": ((5, LCID, 4, 0),()),
		"IgnoreDataHiding": ((33, LCID, 4, 0),()),
		"Name": ((17, LCID, 4, 0),()),
		"TemplateTest": ((28, LCID, 4, 0),()),
		"Type": ((16, LCID, 4, 0),()),
	}
	# Default method for this class is 'Field'
	def __call__(self, FieldName=defaultNamedNotOptArg):
		"""The value of the specified field."""
		return self._ApplyTypes_(0, 2, (12, 0), ((8, 0),), '__call__', None,FieldName
			)

	# str(ob) and int(ob) will use __call__
	def __unicode__(self, *args):
		try:
			return unicode(self.__call__(*args))
		except pythoncom.com_error:
			return repr(self)
	def __str__(self, *args):
		return str(self.__unicode__(*args))
	def __int__(self, *args):
		return int(self.__call__(*args))

class ITestExecStatus(DispatchBaseClass):
	"""Represents the execution status of the test currently running."""
	CLSID = IID('{C1ED849E-CB41-45A9-A256-9A0D3CFDB350}')
	coclass_clsid = IID('{170FFBC3-AE31-4AE8-9C65-0F0FC5AD5CCD}')

	_prop_map_get_ = {
		"Message": (2, 2, (8, 0), (), "Message", None),
		"Status": (3, 2, (8, 0), (), "Status", None),
		"TSTestId": (4, 2, (12, 0), (), "TSTestId", None),
		"TestId": (1, 2, (3, 0), (), "TestId", None),
		"TestInstance": (5, 2, (3, 0), (), "TestInstance", None),
	}
	_prop_map_put_ = {
	}

class ITestFactory(DispatchBaseClass):
	"""Services for managing tests."""
	CLSID = IID('{5BB6D957-669D-47B2-8324-981492EDC42E}')
	coclass_clsid = IID('{D398384F-9D08-4E70-8400-910D8750BDAB}')

	def AddItem(self, ItemData=defaultNamedNotOptArg):
		"""Creates a new item object."""
		ret = self._oleobj_.InvokeTypes(3, LCID, 1, (9, 0), ((12, 0),),ItemData
			)
		if ret is not None:
			ret = Dispatch(ret, u'AddItem', None)
		return ret

	def BuildPerfGraph(self, GroupByField=u'', SumOfField=u'', MaxCols=0, Filter=defaultNamedNotOptArg
			, FRDate=defaultNamedNotOptArg, ForceRefresh=False, ShowFullPath=False):
		"""Creates Performance Graph."""
		return self._ApplyTypes_(13, 1, (9, 32), ((8, 49), (8, 49), (3, 49), (12, 17), (12, 17), (11, 49), (11, 49)), u'BuildPerfGraph', None,GroupByField
			, SumOfField, MaxCols, Filter, FRDate, ForceRefresh
			, ShowFullPath)

	def BuildProgressGraph(self, GroupByField=u'', SumOfField=u'', MajorSkip=0, MinorSkip=1
			, MaxCols=0, Filter=defaultNamedNotOptArg, FRDate=defaultNamedNotOptArg, ForceRefresh=False, ShowFullPath=False):
		"""Creates graph showing number of tests in a test set executed over a period of time."""
		return self._ApplyTypes_(10, 1, (9, 32), ((8, 49), (8, 49), (3, 49), (3, 49), (3, 49), (12, 17), (12, 17), (11, 49), (11, 49)), u'BuildProgressGraph', None,GroupByField
			, SumOfField, MajorSkip, MinorSkip, MaxCols, Filter
			, FRDate, ForceRefresh, ShowFullPath)

	def BuildProgressGraphEx(self, GroupByField=u'', SumOfField=u'', ByHistory=True, MajorSkip=0
			, MinorSkip=1, MaxCols=0, Filter=defaultNamedNotOptArg, FRDate=defaultNamedNotOptArg, ForceRefresh=False
			, ShowFullPath=False):
		"""Creates graph showing number of tests in a test set executed over a period of time."""
		return self._ApplyTypes_(15, 1, (9, 32), ((8, 49), (8, 49), (11, 49), (3, 49), (3, 49), (3, 49), (12, 17), (12, 17), (11, 49), (11, 49)), u'BuildProgressGraphEx', None,GroupByField
			, SumOfField, ByHistory, MajorSkip, MinorSkip, MaxCols
			, Filter, FRDate, ForceRefresh, ShowFullPath)

	def BuildSummaryGraph(self, XAxisField=u'', GroupByField=u'', SumOfField=u'', MaxCols=0
			, Filter=defaultNamedNotOptArg, ForceRefresh=False, ShowFullPath=False):
		"""Creates summary graph showing number of tests executed in a test set according to test and test run."""
		return self._ApplyTypes_(9, 1, (9, 32), ((8, 49), (8, 49), (8, 49), (3, 49), (12, 17), (11, 49), (11, 49)), u'BuildSummaryGraph', None,XAxisField
			, GroupByField, SumOfField, MaxCols, Filter, ForceRefresh
			, ShowFullPath)

	def BuildTrendGraph(self, GroupByField=u'', SumOfField=u'', MaxCols=0, Filter=defaultNamedNotOptArg
			, FRDate=defaultNamedNotOptArg, ForceRefresh=False, ShowFullPath=False):
		"""Creates graph that shows the number of defect status changes over time."""
		return self._ApplyTypes_(12, 1, (9, 32), ((8, 49), (8, 49), (3, 49), (12, 17), (12, 17), (11, 49), (11, 49)), u'BuildTrendGraph', None,GroupByField
			, SumOfField, MaxCols, Filter, FRDate, ForceRefresh
			, ShowFullPath)

	# The method FetchLevel is actually a property, but must be used as a method to correctly pass the arguments
	def FetchLevel(self, FieldName=defaultNamedNotOptArg):
		"""The Fetch level for a field."""
		return self._oleobj_.InvokeTypes(7, LCID, 2, (2, 0), ((8, 1),),FieldName
			)

	# The method Item is actually a property, but must be used as a method to correctly pass the arguments
	def Item(self, ItemKey=defaultNamedNotOptArg):
		"""Gets an object managed by the factory by its key."""
		ret = self._oleobj_.InvokeTypes(0, LCID, 2, (9, 0), ((12, 0),),ItemKey
			)
		if ret is not None:
			ret = Dispatch(ret, u'Item', None)
		return ret

	def Mail(self, Items=defaultNamedNotOptArg, SendTo=defaultNamedNotOptArg, SendCc=u'', Option=0
			, Subject=u'', Comment=u''):
		"""Mails the list of IBase Factory Items. 'Items' is a list of ID numbers."""
		return self._ApplyTypes_(8, 1, (24, 32), ((12, 1), (8, 1), (8, 49), (3, 49), (8, 49), (8, 49)), u'Mail', None,Items
			, SendTo, SendCc, Option, Subject, Comment
			)

	# Result is of type IList
	def NewList(self, Filter=defaultNamedNotOptArg):
		"""Creates a list of objects according to the specified filter."""
		ret = self._oleobj_.InvokeTypes(1, LCID, 1, (9, 0), ((8, 0),),Filter
			)
		if ret is not None:
			ret = Dispatch(ret, u'NewList', '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')
		return ret

	def RemoveItem(self, ItemKey=defaultNamedNotOptArg):
		"""Removes item from the database. Removal takes place immediately, without a Post."""
		return self._oleobj_.InvokeTypes(4, LCID, 1, (24, 0), ((12, 0),),ItemKey
			)

	# The method SetFetchLevel is actually a property, but must be used as a method to correctly pass the arguments
	def SetFetchLevel(self, FieldName=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The Fetch level for a field."""
		return self._oleobj_.InvokeTypes(7, LCID, 4, (24, 0), ((8, 1), (2, 1)),FieldName
			, arg1)

	_prop_map_get_ = {
		# Method 'Fields' returns object of type 'IList'
		"Fields": (2, 2, (9, 0), (), "Fields", '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}'),
		"Filter": (5, 2, (9, 0), (), "Filter", None),
		"History": (6, 2, (9, 0), (), "History", None),
		"IgnoreDataHiding": (14, 2, (11, 0), (), "IgnoreDataHiding", None),
		"RepositoryStorage": (11, 2, (9, 0), (), "RepositoryStorage", None),
	}
	_prop_map_put_ = {
		"IgnoreDataHiding": ((14, LCID, 4, 0),()),
	}
	# Default method for this class is 'Item'
	def __call__(self, ItemKey=defaultNamedNotOptArg):
		"""Gets an object managed by the factory by its key."""
		ret = self._oleobj_.InvokeTypes(0, LCID, 2, (9, 0), ((12, 0),),ItemKey
			)
		if ret is not None:
			ret = Dispatch(ret, '__call__', None)
		return ret

	# str(ob) and int(ob) will use __call__
	def __unicode__(self, *args):
		try:
			return unicode(self.__call__(*args))
		except pythoncom.com_error:
			return repr(self)
	def __str__(self, *args):
		return str(self.__unicode__(*args))
	def __int__(self, *args):
		return int(self.__call__(*args))

class ITestSet(DispatchBaseClass):
	"""Represents a series of tests to be run as a group to meet a specific testing goal."""
	CLSID = IID('{5BBB5891-F832-497C-BE92-76A242809E67}')
	coclass_clsid = IID('{729074D5-C699-4B07-8E8E-25E827C71ED2}')

	def BuildPerfGraph(self, GroupByField=u'', SumOfField=u'', MaxCols=0, Filter=defaultNamedNotOptArg
			, FRDate=defaultNamedNotOptArg, ForceRefresh=False, ShowFullPath=False):
		"""Creates Perf Graph."""
		return self._ApplyTypes_(26, 1, (9, 32), ((8, 49), (8, 49), (3, 49), (12, 17), (12, 17), (11, 49), (11, 49)), u'BuildPerfGraph', None,GroupByField
			, SumOfField, MaxCols, Filter, FRDate, ForceRefresh
			, ShowFullPath)

	def BuildProgressGraph(self, GroupByField=u'', SumOfField=u'', MajorSkip=0, MinorSkip=1
			, MaxCols=0, Filter=defaultNamedNotOptArg, FRDate=defaultNamedNotOptArg, ForceRefresh=False, ShowFullPath=False):
		"""Builds test set progress graph."""
		return self._ApplyTypes_(19, 1, (9, 32), ((8, 49), (8, 49), (3, 49), (3, 49), (3, 49), (12, 17), (12, 17), (11, 49), (11, 49)), u'BuildProgressGraph', None,GroupByField
			, SumOfField, MajorSkip, MinorSkip, MaxCols, Filter
			, FRDate, ForceRefresh, ShowFullPath)

	def BuildProgressGraphEx(self, GroupByField=u'', SumOfField=u'', ByHistory=True, MajorSkip=0
			, MinorSkip=1, MaxCols=0, Filter=defaultNamedNotOptArg, FRDate=defaultNamedNotOptArg, ForceRefresh=False
			, ShowFullPath=False):
		"""Builds test set progress graph."""
		return self._ApplyTypes_(29, 1, (9, 32), ((8, 49), (8, 49), (11, 49), (3, 49), (3, 49), (3, 49), (12, 17), (12, 17), (11, 49), (11, 49)), u'BuildProgressGraphEx', None,GroupByField
			, SumOfField, ByHistory, MajorSkip, MinorSkip, MaxCols
			, Filter, FRDate, ForceRefresh, ShowFullPath)

	def BuildSummaryGraph(self, XAxisField=u'', GroupByField=u'', SumOfField=u'', MaxCols=0
			, Filter=defaultNamedNotOptArg, ForceRefresh=False, ShowFullPath=False):
		"""Builds test set summary graph."""
		return self._ApplyTypes_(18, 1, (9, 32), ((8, 49), (8, 49), (8, 49), (3, 49), (12, 17), (11, 49), (11, 49)), u'BuildSummaryGraph', None,XAxisField
			, GroupByField, SumOfField, MaxCols, Filter, ForceRefresh
			, ShowFullPath)

	def BuildTrendGraph(self, GroupByField=u'', SumOfField=u'', MaxCols=0, Filter=defaultNamedNotOptArg
			, FRDate=defaultNamedNotOptArg, ForceRefresh=False, ShowFullPath=False):
		"""Creates graph that shows the test set changes over time."""
		return self._ApplyTypes_(25, 1, (9, 32), ((8, 49), (8, 49), (3, 49), (12, 17), (12, 17), (11, 49), (11, 49)), u'BuildTrendGraph', None,GroupByField
			, SumOfField, MaxCols, Filter, FRDate, ForceRefresh
			, ShowFullPath)

	def CheckTestInstances(self, TestIDs=defaultNamedNotOptArg):
		"""Gets the count of how many instances of the specified test are in this test set."""
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(28, LCID, 1, (8, 0), ((8, 1),),TestIDs
			)

	# The method Field is actually a property, but must be used as a method to correctly pass the arguments
	def Field(self, FieldName=defaultNamedNotOptArg):
		"""The value of the specified field."""
		return self._ApplyTypes_(0, 2, (12, 0), ((8, 0),), u'Field', None,FieldName
			)

	def LockObject(self):
		"""Locks the object. Returns True if lock successful."""
		return self._oleobj_.InvokeTypes(2, LCID, 1, (11, 0), (),)

	def Post(self):
		"""Posts all changed values into database."""
		return self._oleobj_.InvokeTypes(6, LCID, 1, (24, 0), (),)

	def PurgeExecutions(self):
		"""Purges all the running executions within the test set."""
		return self._oleobj_.InvokeTypes(21, LCID, 1, (24, 0), (),)

	def Refresh(self):
		"""Reads saved values, overwriting values in memory."""
		return self._oleobj_.InvokeTypes(7, LCID, 1, (24, 0), (),)

	def ResetTestSet(self, DeleteRuns=defaultNamedNotOptArg):
		"""Sets the status of each test in the test set to 'No Run'."""
		return self._oleobj_.InvokeTypes(27, LCID, 1, (24, 0), ((11, 1),),DeleteRuns
			)

	# The method SetField is actually a property, but must be used as a method to correctly pass the arguments
	def SetField(self, FieldName=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The value of the specified field."""
		return self._oleobj_.InvokeTypes(0, LCID, 4, (24, 0), ((8, 0), (12, 1)),FieldName
			, arg1)

	def StartExecution(self, ServerName=defaultNamedNotOptArg):
		"""Starts the execution controller."""
		ret = self._oleobj_.InvokeTypes(20, LCID, 1, (9, 0), ((8, 1),),ServerName
			)
		if ret is not None:
			ret = Dispatch(ret, u'StartExecution', None)
		return ret

	def UnLockObject(self):
		"""Unlocks the object."""
		return self._oleobj_.InvokeTypes(3, LCID, 1, (24, 0), (),)

	def Undo(self):
		"""Undoes changes to field values that have not been posted."""
		return self._oleobj_.InvokeTypes(8, LCID, 1, (24, 0), (),)

	_prop_map_get_ = {
		"Attachments": (12, 2, (9, 0), (), "Attachments", None),
		"AutoPost": (5, 2, (11, 0), (), "AutoPost", None),
		"ConditionFactory": (17, 2, (9, 0), (), "ConditionFactory", None),
		"ExecEventNotifyByMailSettings": (22, 2, (9, 0), (), "ExecEventNotifyByMailSettings", None),
		"ExecutionSettings": (23, 2, (9, 0), (), "ExecutionSettings", None),
		"HasAttachment": (13, 2, (11, 0), (), "HasAttachment", None),
		"History": (11, 2, (9, 0), (), "History", None),
		"ID": (4, 2, (12, 0), (), "ID", None),
		"IsLocked": (1, 2, (11, 0), (), "IsLocked", None),
		"Modified": (9, 2, (11, 0), (), "Modified", None),
		"Name": (14, 2, (8, 0), (), "Name", None),
		"Status": (15, 2, (8, 0), (), "Status", None),
		"TSTestFactory": (16, 2, (9, 0), (), "TSTestFactory", None),
		"TestDefaultExecutionSettings": (24, 2, (9, 0), (), "TestDefaultExecutionSettings", None),
		"TestSetFolder": (30, 2, (9, 0), (), "TestSetFolder", None),
		"Virtual": (10, 2, (11, 0), (), "Virtual", None),
	}
	_prop_map_put_ = {
		"AutoPost": ((5, LCID, 4, 0),()),
		"Name": ((14, LCID, 4, 0),()),
		"Status": ((15, LCID, 4, 0),()),
		"TestSetFolder": ((30, LCID, 4, 0),()),
	}
	# Default method for this class is 'Field'
	def __call__(self, FieldName=defaultNamedNotOptArg):
		"""The value of the specified field."""
		return self._ApplyTypes_(0, 2, (12, 0), ((8, 0),), '__call__', None,FieldName
			)

	# str(ob) and int(ob) will use __call__
	def __unicode__(self, *args):
		try:
			return unicode(self.__call__(*args))
		except pythoncom.com_error:
			return repr(self)
	def __str__(self, *args):
		return str(self.__unicode__(*args))
	def __int__(self, *args):
		return int(self.__call__(*args))

class ITestSetFactory(DispatchBaseClass):
	"""Services for managing test sets."""
	CLSID = IID('{6199FE11-C44D-4712-99DF-4F5EF3F80A29}')
	coclass_clsid = IID('{50A81398-8F2F-4F38-B88D-F734C727706C}')

	def AddItem(self, ItemData=defaultNamedNotOptArg):
		"""Creates a new item object."""
		ret = self._oleobj_.InvokeTypes(3, LCID, 1, (9, 0), ((12, 0),),ItemData
			)
		if ret is not None:
			ret = Dispatch(ret, u'AddItem', None)
		return ret

	def BuildPerfGraph(self, TestSetID=defaultNamedNotOptArg, GroupByField=u'', SumOfField=u'', MaxCols=0
			, Filter=defaultNamedNotOptArg, FRDate=defaultNamedNotOptArg, ForceRefresh=False, ShowFullPath=False):
		"""Creates Performance Graph."""
		return self._ApplyTypes_(11, 1, (9, 32), ((3, 1), (8, 49), (8, 49), (3, 49), (12, 17), (12, 17), (11, 49), (11, 49)), u'BuildPerfGraph', None,TestSetID
			, GroupByField, SumOfField, MaxCols, Filter, FRDate
			, ForceRefresh, ShowFullPath)

	def BuildProgressGraph(self, TestSetID=defaultNamedNotOptArg, GroupByField=u'', SumOfField=u'', MajorSkip=0
			, MinorSkip=1, MaxCols=0, Filter=defaultNamedNotOptArg, FRDate=defaultNamedNotOptArg, ForceRefresh=False
			, ShowFullPath=False):
		"""Creates graph showing number of tests in all test sets executed over a period of time."""
		return self._ApplyTypes_(9, 1, (9, 32), ((3, 1), (8, 49), (8, 49), (3, 49), (3, 49), (3, 49), (12, 17), (12, 17), (11, 49), (11, 49)), u'BuildProgressGraph', None,TestSetID
			, GroupByField, SumOfField, MajorSkip, MinorSkip, MaxCols
			, Filter, FRDate, ForceRefresh, ShowFullPath)

	def BuildProgressGraphEx(self, TestSetID=defaultNamedNotOptArg, GroupByField=u'', SumOfField=u'', ByHistory=True
			, MajorSkip=0, MinorSkip=1, MaxCols=0, Filter=defaultNamedNotOptArg, FRDate=defaultNamedNotOptArg
			, ForceRefresh=False, ShowFullPath=False):
		"""Creates progress graph for specified test set."""
		return self._ApplyTypes_(12, 1, (9, 32), ((3, 1), (8, 49), (8, 49), (11, 49), (3, 49), (3, 49), (3, 49), (12, 17), (12, 17), (11, 49), (11, 49)), u'BuildProgressGraphEx', None,TestSetID
			, GroupByField, SumOfField, ByHistory, MajorSkip, MinorSkip
			, MaxCols, Filter, FRDate, ForceRefresh, ShowFullPath
			)

	def BuildSummaryGraph(self, TestSetID=defaultNamedNotOptArg, XAxisField=u'', GroupByField=u'', SumOfField=u''
			, MaxCols=0, Filter=defaultNamedNotOptArg, ForceRefresh=False, ShowFullPath=False):
		"""Creates graph showing the number of requirements reported according to the defect tracking information specified."""
		return self._ApplyTypes_(8, 1, (9, 32), ((3, 1), (8, 49), (8, 49), (8, 49), (3, 49), (12, 17), (11, 49), (11, 49)), u'BuildSummaryGraph', None,TestSetID
			, XAxisField, GroupByField, SumOfField, MaxCols, Filter
			, ForceRefresh, ShowFullPath)

	def BuildTrendGraph(self, TestSetID=defaultNamedNotOptArg, GroupByField=u'', SumOfField=u'', MaxCols=0
			, Filter=defaultNamedNotOptArg, FRDate=defaultNamedNotOptArg, ForceRefresh=False, ShowFullPath=False):
		"""Creates graph showing the number of status changes over a time period."""
		return self._ApplyTypes_(10, 1, (9, 32), ((3, 1), (8, 49), (8, 49), (3, 49), (12, 17), (12, 17), (11, 49), (11, 49)), u'BuildTrendGraph', None,TestSetID
			, GroupByField, SumOfField, MaxCols, Filter, FRDate
			, ForceRefresh, ShowFullPath)

	# The method FetchLevel is actually a property, but must be used as a method to correctly pass the arguments
	def FetchLevel(self, FieldName=defaultNamedNotOptArg):
		"""The Fetch level for a field."""
		return self._oleobj_.InvokeTypes(7, LCID, 2, (2, 0), ((8, 1),),FieldName
			)

	# The method Item is actually a property, but must be used as a method to correctly pass the arguments
	def Item(self, ItemKey=defaultNamedNotOptArg):
		"""Gets an object managed by the factory by its key."""
		ret = self._oleobj_.InvokeTypes(0, LCID, 2, (9, 0), ((12, 0),),ItemKey
			)
		if ret is not None:
			ret = Dispatch(ret, u'Item', None)
		return ret

	# Result is of type IList
	def NewList(self, Filter=defaultNamedNotOptArg):
		"""Creates a list of objects according to the specified filter."""
		ret = self._oleobj_.InvokeTypes(1, LCID, 1, (9, 0), ((8, 0),),Filter
			)
		if ret is not None:
			ret = Dispatch(ret, u'NewList', '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')
		return ret

	def RemoveItem(self, ItemKey=defaultNamedNotOptArg):
		"""Removes item from the database. Removal takes place immediately, without a Post."""
		return self._oleobj_.InvokeTypes(4, LCID, 1, (24, 0), ((12, 0),),ItemKey
			)

	# The method SetFetchLevel is actually a property, but must be used as a method to correctly pass the arguments
	def SetFetchLevel(self, FieldName=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The Fetch level for a field."""
		return self._oleobj_.InvokeTypes(7, LCID, 4, (24, 0), ((8, 1), (2, 1)),FieldName
			, arg1)

	_prop_map_get_ = {
		# Method 'Fields' returns object of type 'IList'
		"Fields": (2, 2, (9, 0), (), "Fields", '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}'),
		"Filter": (5, 2, (9, 0), (), "Filter", None),
		"History": (6, 2, (9, 0), (), "History", None),
	}
	_prop_map_put_ = {
	}
	# Default method for this class is 'Item'
	def __call__(self, ItemKey=defaultNamedNotOptArg):
		"""Gets an object managed by the factory by its key."""
		ret = self._oleobj_.InvokeTypes(0, LCID, 2, (9, 0), ((12, 0),),ItemKey
			)
		if ret is not None:
			ret = Dispatch(ret, '__call__', None)
		return ret

	# str(ob) and int(ob) will use __call__
	def __unicode__(self, *args):
		try:
			return unicode(self.__call__(*args))
		except pythoncom.com_error:
			return repr(self)
	def __str__(self, *args):
		return str(self.__unicode__(*args))
	def __int__(self, *args):
		return int(self.__call__(*args))

class ITestSetFolder(DispatchBaseClass):
	"""Manages the tests and test sets included in one test set folder."""
	CLSID = IID('{E9E2BCFE-CAAC-492D-A210-C2E49A68C78F}')
	coclass_clsid = None

	# Result is of type ISysTreeNode
	def AddNode(self, NodeName=defaultNamedNotOptArg):
		"""Adds a new child node."""
		ret = self._oleobj_.InvokeTypes(1, LCID, 1, (9, 0), ((8, 0),),NodeName
			)
		if ret is not None:
			ret = Dispatch(ret, u'AddNode', '{8538CCBA-0B3E-481F-BB2B-14F0553CA146}')
		return ret

	def AddNodeDisp(self, NodeName=defaultNamedNotOptArg):
		"""Adds a new child node."""
		ret = self._oleobj_.InvokeTypes(20, LCID, 1, (9, 0), ((8, 1),),NodeName
			)
		if ret is not None:
			ret = Dispatch(ret, u'AddNodeDisp', None)
		return ret

	# Result is of type ISysTreeNode
	# The method Child is actually a property, but must be used as a method to correctly pass the arguments
	def Child(self, Index=defaultNamedNotOptArg):
		"""Gets a sub-folder of the current folder."""
		ret = self._oleobj_.InvokeTypes(8, LCID, 2, (9, 0), ((3, 0),),Index
			)
		if ret is not None:
			ret = Dispatch(ret, u'Child', '{8538CCBA-0B3E-481F-BB2B-14F0553CA146}')
		return ret

	# Result is of type ISysTreeNode
	def FindChildNode(self, ChildName=defaultNamedNotOptArg):
		"""Finds a child node by node name."""
		ret = self._oleobj_.InvokeTypes(5, LCID, 1, (9, 0), ((8, 0),),ChildName
			)
		if ret is not None:
			ret = Dispatch(ret, u'FindChildNode', '{8538CCBA-0B3E-481F-BB2B-14F0553CA146}')
		return ret

	# Result is of type IList
	def FindChildren(self, Pattern=defaultNamedNotOptArg, MatchCase=False, Filter=u''):
		"""Finds node children according to specified search pattern."""
		return self._ApplyTypes_(11, 1, (9, 32), ((8, 1), (11, 49), (8, 49)), u'FindChildren', '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}',Pattern
			, MatchCase, Filter)

	# Result is of type IList
	def FindTestSets(self, Pattern=defaultNamedNotOptArg, MatchCase=False, Filter=u''):
		"""Gets a list of contained testsets according to the specified pattern."""
		return self._ApplyTypes_(26, 1, (9, 32), ((8, 1), (11, 49), (8, 49)), u'FindTestSets', '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}',Pattern
			, MatchCase, Filter)

	def Move(self, Father=defaultNamedNotOptArg):
		"""Moves folder node under new father."""
		return self._oleobj_.InvokeTypes(22, LCID, 1, (24, 0), ((12, 1),),Father
			)

	# Result is of type IList
	def NewList(self):
		"""Gets a list of the node's children."""
		ret = self._oleobj_.InvokeTypes(2, LCID, 1, (9, 0), (),)
		if ret is not None:
			ret = Dispatch(ret, u'NewList', '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')
		return ret

	def Post(self):
		"""Posts all changed values to the database."""
		return self._oleobj_.InvokeTypes(13, LCID, 1, (24, 0), (),)

	def Refresh(self):
		"""Reads the saved node data, overwriting values in memory."""
		return self._oleobj_.InvokeTypes(14, LCID, 1, (24, 0), (),)

	def RemoveNode(self, Node=defaultNamedNotOptArg):
		"""Deletes the specified node."""
		return self._oleobj_.InvokeTypes(6, LCID, 1, (24, 0), ((12, 0),),Node
			)

	def RemoveNodeEx(self, Node=defaultNamedNotOptArg, DeleteTestSets=False):
		"""Deletes a test set folder node."""
		return self._oleobj_.InvokeTypes(21, LCID, 1, (24, 0), ((12, 1), (11, 49)),Node
			, DeleteTestSets)

	_prop_map_get_ = {
		"Attachments": (25, 2, (9, 0), (), "Attachments", None),
		"Attribute": (4, 2, (3, 0), (), "Attribute", None),
		"Count": (9, 2, (3, 0), (), "Count", None),
		"DepthType": (10, 2, (2, 0), (), "DepthType", None),
		"Description": (15, 2, (8, 0), (), "Description", None),
		# Method 'Father' returns object of type 'ISysTreeNode'
		"Father": (7, 2, (9, 0), (), "Father", '{8538CCBA-0B3E-481F-BB2B-14F0553CA146}'),
		"FatherDisp": (17, 2, (9, 0), (), "FatherDisp", None),
		"FatherID": (16, 2, (3, 0), (), "FatherID", None),
		"HasAttachment": (24, 2, (11, 0), (), "HasAttachment", None),
		"Name": (0, 2, (8, 0), (), "Name", None),
		"NodeID": (3, 2, (3, 0), (), "NodeID", None),
		"Path": (12, 2, (8, 0), (), "Path", None),
		# Method 'SubNodes' returns object of type 'IList'
		"SubNodes": (19, 2, (9, 0), (), "SubNodes", '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}'),
		"TestSetFactory": (23, 2, (9, 0), (), "TestSetFactory", None),
		"ViewOrder": (18, 2, (3, 0), (), "ViewOrder", None),
	}
	_prop_map_put_ = {
		"Description": ((15, LCID, 4, 0),()),
		"Name": ((0, LCID, 4, 0),()),
		"ViewOrder": ((18, LCID, 4, 0),()),
	}
	# Default property for this class is 'Name'
	def __call__(self):
		return self._ApplyTypes_(*(0, 2, (8, 0), (), "Name", None))
	# str(ob) and int(ob) will use __call__
	def __unicode__(self, *args):
		try:
			return unicode(self.__call__(*args))
		except pythoncom.com_error:
			return repr(self)
	def __str__(self, *args):
		return str(self.__unicode__(*args))
	def __int__(self, *args):
		return int(self.__call__(*args))
	#This class has Count() property - allow len(ob) to provide this
	def __len__(self):
		return self._ApplyTypes_(*(9, 2, (3, 0), (), "Count", None))
	#This class has a __len__ - this is needed so 'if object:' always returns TRUE.
	def __nonzero__(self):
		return True

class ITestSetFolder2(DispatchBaseClass):
	"""Manages the tests and test sets included in one test set folder."""
	CLSID = IID('{18B58E14-B956-40A3-A37F-B8EF1136F238}')
	coclass_clsid = IID('{606FDBBB-55A7-48D6-BF70-06A7CE08049C}')

	# Result is of type ISysTreeNode
	def AddNode(self, NodeName=defaultNamedNotOptArg):
		"""Adds a new child node."""
		ret = self._oleobj_.InvokeTypes(1, LCID, 1, (9, 0), ((8, 0),),NodeName
			)
		if ret is not None:
			ret = Dispatch(ret, u'AddNode', '{8538CCBA-0B3E-481F-BB2B-14F0553CA146}')
		return ret

	def AddNodeDisp(self, NodeName=defaultNamedNotOptArg):
		"""Adds a new child node."""
		ret = self._oleobj_.InvokeTypes(20, LCID, 1, (9, 0), ((8, 1),),NodeName
			)
		if ret is not None:
			ret = Dispatch(ret, u'AddNodeDisp', None)
		return ret

	# Result is of type ISysTreeNode
	# The method Child is actually a property, but must be used as a method to correctly pass the arguments
	def Child(self, Index=defaultNamedNotOptArg):
		"""Gets a sub-folder of the current folder."""
		ret = self._oleobj_.InvokeTypes(8, LCID, 2, (9, 0), ((3, 0),),Index
			)
		if ret is not None:
			ret = Dispatch(ret, u'Child', '{8538CCBA-0B3E-481F-BB2B-14F0553CA146}')
		return ret

	# Result is of type ISysTreeNode
	def FindChildNode(self, ChildName=defaultNamedNotOptArg):
		"""Finds a child node by node name."""
		ret = self._oleobj_.InvokeTypes(5, LCID, 1, (9, 0), ((8, 0),),ChildName
			)
		if ret is not None:
			ret = Dispatch(ret, u'FindChildNode', '{8538CCBA-0B3E-481F-BB2B-14F0553CA146}')
		return ret

	# Result is of type IList
	def FindChildren(self, Pattern=defaultNamedNotOptArg, MatchCase=False, Filter=u''):
		"""Finds node children according to specified search pattern."""
		return self._ApplyTypes_(11, 1, (9, 32), ((8, 1), (11, 49), (8, 49)), u'FindChildren', '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}',Pattern
			, MatchCase, Filter)

	# Result is of type IList
	def FindTestInstances(self, Pattern=defaultNamedNotOptArg, MatchCase=False, Filter=u''):
		"""The list of test instances in all test sets in given folder and its subfolders that match the specified pattern."""
		return self._ApplyTypes_(27, 1, (9, 32), ((8, 1), (11, 49), (8, 49)), u'FindTestInstances', '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}',Pattern
			, MatchCase, Filter)

	# Result is of type IList
	def FindTestSets(self, Pattern=defaultNamedNotOptArg, MatchCase=False, Filter=u''):
		"""Gets a list of contained testsets according to the specified pattern."""
		return self._ApplyTypes_(26, 1, (9, 32), ((8, 1), (11, 49), (8, 49)), u'FindTestSets', '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}',Pattern
			, MatchCase, Filter)

	def Move(self, Father=defaultNamedNotOptArg):
		"""Moves folder node under new father."""
		return self._oleobj_.InvokeTypes(22, LCID, 1, (24, 0), ((12, 1),),Father
			)

	# Result is of type IList
	def NewList(self):
		"""Gets a list of the node's children."""
		ret = self._oleobj_.InvokeTypes(2, LCID, 1, (9, 0), (),)
		if ret is not None:
			ret = Dispatch(ret, u'NewList', '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')
		return ret

	def Post(self):
		"""Posts all changed values to the database."""
		return self._oleobj_.InvokeTypes(13, LCID, 1, (24, 0), (),)

	def Refresh(self):
		"""Reads the saved node data, overwriting values in memory."""
		return self._oleobj_.InvokeTypes(14, LCID, 1, (24, 0), (),)

	def RemoveNode(self, Node=defaultNamedNotOptArg):
		"""Deletes the specified node."""
		return self._oleobj_.InvokeTypes(6, LCID, 1, (24, 0), ((12, 0),),Node
			)

	def RemoveNodeEx(self, Node=defaultNamedNotOptArg, DeleteTestSets=False):
		"""Deletes a test set folder node."""
		return self._oleobj_.InvokeTypes(21, LCID, 1, (24, 0), ((12, 1), (11, 49)),Node
			, DeleteTestSets)

	_prop_map_get_ = {
		"Attachments": (25, 2, (9, 0), (), "Attachments", None),
		"Attribute": (4, 2, (3, 0), (), "Attribute", None),
		"Count": (9, 2, (3, 0), (), "Count", None),
		"DepthType": (10, 2, (2, 0), (), "DepthType", None),
		"Description": (15, 2, (8, 0), (), "Description", None),
		# Method 'Father' returns object of type 'ISysTreeNode'
		"Father": (7, 2, (9, 0), (), "Father", '{8538CCBA-0B3E-481F-BB2B-14F0553CA146}'),
		"FatherDisp": (17, 2, (9, 0), (), "FatherDisp", None),
		"FatherID": (16, 2, (3, 0), (), "FatherID", None),
		"HasAttachment": (24, 2, (11, 0), (), "HasAttachment", None),
		"Name": (0, 2, (8, 0), (), "Name", None),
		"NodeID": (3, 2, (3, 0), (), "NodeID", None),
		"Path": (12, 2, (8, 0), (), "Path", None),
		# Method 'SubNodes' returns object of type 'IList'
		"SubNodes": (19, 2, (9, 0), (), "SubNodes", '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}'),
		"TestSetFactory": (23, 2, (9, 0), (), "TestSetFactory", None),
		"ViewOrder": (18, 2, (3, 0), (), "ViewOrder", None),
	}
	_prop_map_put_ = {
		"Description": ((15, LCID, 4, 0),()),
		"Name": ((0, LCID, 4, 0),()),
		"ViewOrder": ((18, LCID, 4, 0),()),
	}
	# Default property for this class is 'Name'
	def __call__(self):
		return self._ApplyTypes_(*(0, 2, (8, 0), (), "Name", None))
	# str(ob) and int(ob) will use __call__
	def __unicode__(self, *args):
		try:
			return unicode(self.__call__(*args))
		except pythoncom.com_error:
			return repr(self)
	def __str__(self, *args):
		return str(self.__unicode__(*args))
	def __int__(self, *args):
		return int(self.__call__(*args))
	#This class has Count() property - allow len(ob) to provide this
	def __len__(self):
		return self._ApplyTypes_(*(9, 2, (3, 0), (), "Count", None))
	#This class has a __len__ - this is needed so 'if object:' always returns TRUE.
	def __nonzero__(self):
		return True

class ITestSetTreeManager(DispatchBaseClass):
	"""Manages the test set tree and its related test set folders."""
	CLSID = IID('{FD53A549-A095-469F-AD8E-95F9F034D2DF}')
	coclass_clsid = IID('{911B9D10-736A-49A2-9A19-34C3CFE6B2E9}')

	# The method NodeById is actually a property, but must be used as a method to correctly pass the arguments
	def NodeById(self, NodeID=defaultNamedNotOptArg):
		"""Gets the testset tree node object cooresponding to the specified node Id."""
		ret = self._oleobj_.InvokeTypes(2, LCID, 2, (9, 0), ((3, 0),),NodeID
			)
		if ret is not None:
			ret = Dispatch(ret, u'NodeById', None)
		return ret

	# The method NodeByPath is actually a property, but must be used as a method to correctly pass the arguments
	def self, Path=defaultNamedNotOptArg):
		"""Gets the test set tree node from the specified tree path."""
		ret = self._oleobj_.InvokeTypes(1, LCID, 2, (9, 0), ((8, 0),),Path
			)
		if ret is not None:
			ret = Dispatch(ret, u'NodeByPath', None)
		return ret

	_prop_map_get_ = {
		"Root": (0, 2, (9, 0), (), "Root", None),
		"Unattached": (3, 2, (9, 0), (), "Unattached", None),
	}
	_prop_map_put_ = {
	}
	# Default property for this class is 'Root'
	def __call__(self):
		return self._ApplyTypes_(*(0, 2, (9, 0), (), "Root", None))
	# str(ob) and int(ob) will use __call__
	def __unicode__(self, *args):
		try:
			return unicode(self.__call__(*args))
		except pythoncom.com_error:
			return repr(self)
	def __str__(self, *args):
		return str(self.__unicode__(*args))
	def __int__(self, *args):
		return int(self.__call__(*args))

class ITextParser(DispatchBaseClass):
	"""ITextParser Interface."""
	CLSID = IID('{328737D2-7777-4E5E-BB9A-488277C730C5}')
	coclass_clsid = IID('{AAF726A2-9187-4DF5-9528-A3E66CDD28E2}')

	def ClearParam(self, vParam=defaultNamedNotOptArg):
		"""Clear the parameter - Set its value to null."""
		return self._oleobj_.InvokeTypes(3, LCID, 1, (24, 0), ((12, 1),),vParam
			)

	def EvaluateText(self):
		"""Converts the parameters to their values."""
		return self._oleobj_.InvokeTypes(10, LCID, 1, (24, 0), (),)

	def Initialize(self, StartClose=u'<%', EndClose=u'%>', TypeClose=u'?', MaxLen=-1
			, DefaultType=u'string'):
		"""Initilizes the parser."""
		return self._ApplyTypes_(8, 1, (24, 32), ((8, 49), (8, 49), (8, 49), (3, 49), (8, 49)), u'Initialize', None,StartClose
			, EndClose, TypeClose, MaxLen, DefaultType)

	# The method ParamExist is actually a property, but must be used as a method to correctly pass the arguments
	def ParamExist(self, ParamName=defaultNamedNotOptArg):
		"""Checks if a parameter with the specified name exists."""
		return self._oleobj_.InvokeTypes(6, LCID, 2, (11, 0), ((8, 1),),ParamName
			)

	# The method ParamName is actually a property, but must be used as a method to correctly pass the arguments
	def ParamName(self, nPosition=defaultNamedNotOptArg):
		"""Gets the parameter name."""
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(7, LCID, 2, (8, 0), ((3, 1),),nPosition
			)

	# The method ParamType is actually a property, but must be used as a method to correctly pass the arguments
	def ParamType(self, vParam=defaultNamedNotOptArg):
		"""Gets the user-defined Parameter Type."""
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(5, LCID, 2, (8, 0), ((12, 1),),vParam
			)

	# The method ParamValue is actually a property, but must be used as a method to correctly pass the arguments
	def ParamValue(self, vParam=defaultNamedNotOptArg):
		"""The parameter value."""
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(2, LCID, 2, (8, 0), ((12, 1),),vParam
			)

	# The method SetParamValue is actually a property, but must be used as a method to correctly pass the arguments
	def SetParamValue(self, vParam=defaultNamedNotOptArg, arg1=defaultUnnamedArg):
		"""The parameter value."""
		return self._oleobj_.InvokeTypes(2, LCID, 4, (24, 0), ((12, 1), (8, 1)),vParam
			, arg1)

	# The method Type is actually a property, but must be used as a method to correctly pass the arguments
	def Type(self, vParam=defaultNamedNotOptArg):
		"""Gets the Parameter Type - predefined, null, or regular."""
		return self._oleobj_.InvokeTypes(4, LCID, 2, (3, 0), ((12, 1),),vParam
			)

	_prop_map_get_ = {
		"Count": (1, 2, (3, 0), (), "Count", None),
		"Text": (9, 2, (8, 0), (), "Text", None),
	}
	_prop_map_put_ = {
		"Text": ((9, LCID, 4, 0),()),
	}
	#This class has Count() property - allow len(ob) to provide this
	def __len__(self):
		return self._ApplyTypes_(*(1, 2, (3, 0), (), "Count", None))
	#This class has a __len__ - this is needed so 'if object:' always returns TRUE.
	def __nonzero__(self):
		return True

class ITreeManager(DispatchBaseClass):
	"""Represents the system tree, containing a subject tree and all hierarchical field trees."""
	CLSID = IID('{E60B4439-691C-433D-B6D9-793044915A01}')
	coclass_clsid = IID('{77D80D34-C6FF-43A5-9E67-C7E0A81CF3F2}')

	# Result is of type IList
	# The method GetRootList is actually a property, but must be used as a method to correctly pass the arguments
	def GetRootList(self, Val=0):
		"""Gets list of available trees in the specified range."""
		ret = self._oleobj_.InvokeTypes(4, LCID, 2, (9, 0), ((2, 49),),Val
			)
		if ret is not None:
			ret = Dispatch(ret, u'GetRootList', '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')
		return ret

	# The method NodeById is actually a property, but must be used as a method to correctly pass the arguments
	def NodeById(self, NodeID=defaultNamedNotOptArg):
		"""Gets the object having the specified node Id."""
		ret = self._oleobj_.InvokeTypes(3, LCID, 2, (9, 0), ((3, 0),),NodeID
			)
		if ret is not None:
			ret = Dispatch(ret, u'NodeById', None)
		return ret

	# The method NodeByPath is actually a property, but must be used as a method to correctly pass the arguments
	def NodeByPath(self, Path=defaultNamedNotOptArg):
		"""Gets the node at the specified tree path."""
		ret = self._oleobj_.InvokeTypes(2, LCID, 2, (9, 0), ((8, 0),),Path
			)
		if ret is not None:
			ret = Dispatch(ret, u'NodeByPath', None)
		return ret

	# The method NodePath is actually a property, but must be used as a method to correctly pass the arguments
	def NodePath(self, NodeID=defaultNamedNotOptArg):
		"""Gets the node path, slash separated."""
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(1, LCID, 2, (8, 0), ((3, 0),),NodeID
			)

	# The method TreeRoot is actually a property, but must be used as a method to correctly pass the arguments
	def TreeRoot(self, RootName=defaultNamedNotOptArg):
		"""The root node specified by the node name."""
		ret = self._oleobj_.InvokeTypes(0, LCID, 2, (9, 0), ((8, 0),),RootName
			)
		if ret is not None:
			ret = Dispatch(ret, u'TreeRoot', None)
		return ret

	_prop_map_get_ = {
		# Method 'RootList' returns object of type 'IList'
		"RootList": (4, 2, (9, 0), ((2, 49),), "RootList", '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}'),
	}
	_prop_map_put_ = {
	}
	# Default method for this class is 'TreeRoot'
	def __call__(self, RootName=defaultNamedNotOptArg):
		"""The root node specified by the node name."""
		ret = self._oleobj_.InvokeTypes(0, LCID, 2, (9, 0), ((8, 0),),RootName
			)
		if ret is not None:
			ret = Dispatch(ret, '__call__', None)
		return ret

	# str(ob) and int(ob) will use __call__
	def __unicode__(self, *args):
		try:
			return unicode(self.__call__(*args))
		except pythoncom.com_error:
			return repr(self)
	def __str__(self, *args):
		return str(self.__unicode__(*args))
	def __int__(self, *args):
		return int(self.__call__(*args))

class IVCS(DispatchBaseClass):
	"""Represents a Version Control System connection."""
	CLSID = IID('{6C66F3F1-52E8-4B39-840B-FB1C9800C676}')
	coclass_clsid = IID('{2D1E1D34-561E-40BE-845B-E1F872209A32}')

	def AddObjectToVcs(self, Version=u'1.1.1', Comments=u'Object Created in VCS'):
		"""Adds an object to the Version control database. """
		return self._ApplyTypes_(4, 1, (24, 32), ((8, 49), (8, 49)), u'AddObjectToVcs', None,Version
			, Comments)

	def CheckIn(self, Version=defaultNamedNotOptArg, Comments=defaultNamedNotOptArg, Remove=True, SetCurrent=True):
		"""Checks in an object."""
		return self._oleobj_.InvokeTypes(7, LCID, 1, (24, 0), ((8, 1), (8, 1), (11, 49), (11, 49)),Version
			, Comments, Remove, SetCurrent)

	def CheckInEx(self, Version=defaultNamedNotOptArg, Comments=defaultNamedNotOptArg, Remove=True, SetCurrent=True
			, ForceCheckin=False, Reserved=0):
		"""Checks in an object."""
		return self._oleobj_.InvokeTypes(20, LCID, 1, (24, 0), ((8, 1), (8, 1), (11, 49), (11, 49), (11, 49), (19, 49)),Version
			, Comments, Remove, SetCurrent, ForceCheckin, Reserved
			)

	def CheckOut(self, Version=defaultNamedNotOptArg, Comment=defaultNamedNotOptArg, Lock=defaultNamedNotOptArg, ReadOnly=False
			, Sync=True):
		"""Checks out an object"""
		return self._oleobj_.InvokeTypes(6, LCID, 1, (24, 0), ((8, 1), (8, 1), (11, 1), (11, 49), (11, 49)),Version
			, Comment, Lock, ReadOnly, Sync)

	def ClearView(self, bstrVersion=u''):
		"""For future use."""
		return self._ApplyTypes_(17, 1, (24, 32), ((8, 49),), u'ClearView', None,bstrVersion
			)

	def DeleteObjectFromVCS(self):
		"""Deletes the object from the Version Control database."""
		return self._oleobj_.InvokeTypes(5, LCID, 1, (24, 0), (),)

	def LockVcsObject(self):
		"""Changes VCS object status from GET to CHECK-OUT. The object must be in a GET status to use this method."""
		return self._oleobj_.InvokeTypes(9, LCID, 1, (24, 0), (),)

	def Refresh(self):
		"""Refreshes the VCS object from the server to the client, overwriting values in memory."""
		return self._oleobj_.InvokeTypes(19, LCID, 1, (24, 0), (),)

	def SetCurrentVersion(self, Version=defaultNamedNotOptArg):
		"""This method is potentially destructive. Use only if necessary and with care. Sets the VCS object current version."""
		return self._oleobj_.InvokeTypes(8, LCID, 1, (24, 0), ((8, 1),),Version
			)

	def UndoCheckout(self, Remove=defaultNamedNotOptArg):
		"""Undoes the check out operation, falling back to the current version on the server."""
		return self._oleobj_.InvokeTypes(10, LCID, 1, (24, 0), ((11, 1),),Remove
			)

	# The method VersionInfo is actually a property, but must be used as a method to correctly pass the arguments
	def VersionInfo(self, Version=defaultNamedNotOptArg):
		"""A reference to a VersionItem object containing information about a specific version, specified by the Version argument."""
		ret = self._oleobj_.InvokeTypes(15, LCID, 2, (9, 0), ((8, 0),),Version
			)
		if ret is not None:
			ret = Dispatch(ret, u'VersionInfo', None)
		return ret

	def ViewVersion(self, bstrVersion=defaultNamedNotOptArg, bstrViewPath=defaultNamedNotOptArg):
		"""Method ViewVersion."""
		return self._ApplyTypes_(16, 1, (24, 0), ((8, 1), (16392, 3)), u'ViewVersion', None,bstrVersion
			, bstrViewPath)

	_prop_map_get_ = {
		"CheckoutInfo": (12, 2, (9, 0), (), "CheckoutInfo", None),
		"CurrentVersion": (11, 2, (8, 0), (), "CurrentVersion", None),
		"IsCheckedOut": (2, 2, (11, 0), (), "IsCheckedOut", None),
		"IsLocked": (3, 2, (11, 0), (), "IsLocked", None),
		"LockedBy": (14, 2, (8, 0), (), "LockedBy", None),
		"Version": (1, 2, (8, 0), (), "Version", None),
		# Method 'Versions' returns object of type 'IList'
		"Versions": (13, 2, (9, 0), (), "Versions", '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}'),
		# Method 'VersionsEx' returns object of type 'IList'
		"VersionsEx": (18, 2, (9, 0), (), "VersionsEx", '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}'),
	}
	_prop_map_put_ = {
	}

class IVcsAdmin(DispatchBaseClass):
	"""IVcsAdmin Interface"""
	CLSID = IID('{4FB0D662-F589-4C2D-BC4E-1A6E75845472}')
	coclass_clsid = IID('{0FC5B9FD-629D-4E3F-8A36-41347ADA3107}')

	_prop_map_get_ = {
	}
	_prop_map_put_ = {
	}

class IVersionItem(DispatchBaseClass):
	"""Represents specific version information."""
	CLSID = IID('{38D402FA-9823-4D25-979E-62C377AC2E77}')
	coclass_clsid = IID('{0DCB2995-2738-4BE5-873D-ADAEB29262FA}')

	_prop_map_get_ = {
		"Comments": (1, 2, (8, 0), (), "Comments", None),
		"Date": (4, 2, (8, 0), (), "Date", None),
		"IsLocked": (3, 2, (11, 0), (), "IsLocked", None),
		"Time": (6, 2, (8, 0), (), "Time", None),
		"User": (5, 2, (8, 0), (), "User", None),
		"Version": (2, 2, (8, 0), (), "Version", None),
	}
	_prop_map_put_ = {
	}

class _DIOtaEvents:
	"""_DIOtaEvents."""
	CLSID = CLSID_Sink = IID('{C79EC040-A067-11D5-9D6E-000102E0AC0C}')
	coclass_clsid = IID('{C5CBD7B2-490C-45F5-8C40-B8C3D108E6D7}')
	_public_methods_ = [] # For COM Server support
	_dispid_to_func_ = {
		        7 : "OnDisconnectingProject",
		        2 : "OnConnectServer",
		        1 : "OnConnectingServer",
		        3 : "OnConnectingProject",
		       17 : "OnUpdatingItem",
		        8 : "OnDisconnectProject",
		       21 : "OnServerErrRecieved",
		       20 : "OnFetchNewList",
		       10 : "OnAddItem",
		       14 : "OnSetItemValue",
		        6 : "OnDisconnectServer",
		        5 : "OnDisconnectingServer",
		       13 : "OnSettingItemValue",
		        9 : "OnAddingItem",
		        4 : "OnConnectProject",
		       12 : "OnDeleteItem",
		       11 : "OnDeletingItem",
		       16 : "OnGetItemValue",
		       18 : "OnUpdateItem",
		       19 : "OnFetchingNewList",
		       15 : "OnGettingItemValue",
		}

	def __init__(self, oobj = None):
		if oobj is None:
			self._olecp = None
		else:
			import win32com.server.util
			from win32com.server.policy import EventHandlerPolicy
			cpc=oobj._oleobj_.QueryInterface(pythoncom.IID_IConnectionPointContainer)
			cp=cpc.FindConnectionPoint(self.CLSID_Sink)
			cookie=cp.Advise(win32com.server.util.wrap(self, usePolicy=EventHandlerPolicy))
			self._olecp,self._olecp_cookie = cp,cookie
	def __del__(self):
		try:
			self.close()
		except pythoncom.com_error:
			pass
	def close(self):
		if self._olecp is not None:
			cp,cookie,self._olecp,self._olecp_cookie = self._olecp,self._olecp_cookie,None,None
			cp.Unadvise(cookie)
	def _query_interface_(self, iid):
		import win32com.server.util
		if iid==self.CLSID_Sink: return win32com.server.util.wrap(self)

	# Event Handlers
	# If you create handlers, they should have the following prototypes:
#	def OnDisconnectingProject(self):
#	def OnConnectServer(self):
#	def OnConnectingServer(self, Domain=defaultNamedNotOptArg, Server=defaultNamedNotOptArg):
#	def OnConnectingProject(self, Project=defaultNamedNotOptArg, User=defaultNamedNotOptArg, Password=defaultNamedNotOptArg):
#	def OnUpdatingItem(self, ObjType=defaultNamedNotOptArg, Object_=defaultNamedNotOptArg):
#	def OnDisconnectProject(self):
#	def OnServerErrRecieved(self, ErrorCode=defaultNamedNotOptArg):
#	def OnFetchNewList(self, List=defaultNamedNotOptArg):
#	def OnAddItem(self, ObjType=defaultNamedNotOptArg, Object_=defaultNamedNotOptArg):
#	def OnSetItemValue(self, ObjType=defaultNamedNotOptArg, Object_=defaultNamedNotOptArg, FieldName=defaultNamedNotOptArg, FieldValue=defaultNamedNotOptArg):
#	def OnDisconnectServer(self):
#	def OnDisconnectingServer(self):
#	def OnSettingItemValue(self, ObjType=defaultNamedNotOptArg, Object_=defaultNamedNotOptArg, FieldName=defaultNamedNotOptArg, FieldValue=defaultNamedNotOptArg):
#	def OnAddingItem(self, ObjType=defaultNamedNotOptArg, Object_=defaultNamedNotOptArg):
#	def OnConnectProject(self):
#	def OnDeleteItem(self, ObjType=defaultNamedNotOptArg):
#	def OnDeletingItem(self, ObjType=defaultNamedNotOptArg, Object_=defaultNamedNotOptArg):
#	def OnGetItemValue(self, ObjType=defaultNamedNotOptArg, Object_=defaultNamedNotOptArg, FieldName=defaultNamedNotOptArg, FieldValue=defaultNamedNotOptArg):
#	def OnUpdateItem(self, ObjType=defaultNamedNotOptArg, Object_=defaultNamedNotOptArg):
#	def OnFetchingNewList(self, Filter=defaultNamedNotOptArg):
#	def OnGettingItemValue(self, ObjType=defaultNamedNotOptArg, Object_=defaultNamedNotOptArg, FieldName=defaultNamedNotOptArg):


class _DIProgressEvents:
	"""_DIProgressEvents."""
	CLSID = CLSID_Sink = IID('{F4E85688-FCD7-11D4-9D8A-0001029DEAF5}')
	coclass_clsid = IID('{C5CBD7B2-490C-45F5-8C40-B8C3D108E6D7}')
	_public_methods_ = [] # For COM Server support
	_dispid_to_func_ = {
		        3 : "OnServerProgress",
		        2 : "OnDataAvailable",
		        1 : "OnProgress",
		        4 : "OnMessage",
		}

	def __init__(self, oobj = None):
		if oobj is None:
			self._olecp = None
		else:
			import win32com.server.util
			from win32com.server.policy import EventHandlerPolicy
			cpc=oobj._oleobj_.QueryInterface(pythoncom.IID_IConnectionPointContainer)
			cp=cpc.FindConnectionPoint(self.CLSID_Sink)
			cookie=cp.Advise(win32com.server.util.wrap(self, usePolicy=EventHandlerPolicy))
			self._olecp,self._olecp_cookie = cp,cookie
	def __del__(self):
		try:
			self.close()
		except pythoncom.com_error:
			pass
	def close(self):
		if self._olecp is not None:
			cp,cookie,self._olecp,self._olecp_cookie = self._olecp,self._olecp_cookie,None,None
			cp.Unadvise(cookie)
	def _query_interface_(self, iid):
		import win32com.server.util
		if iid==self.CLSID_Sink: return win32com.server.util.wrap(self)

	# Event Handlers
	# If you create handlers, they should have the following prototypes:
#	def OnServerProgress(self, Time=defaultNamedNotOptArg, Message=defaultNamedNotOptArg):
#	def OnDataAvailable(self, ErrorCode=defaultNamedNotOptArg):
#	def OnProgress(self, Current=defaultNamedNotOptArg, Total=defaultNamedNotOptArg, Message=defaultNamedNotOptArg):
#	def OnMessage(self, Message=defaultNamedNotOptArg):


class _ITestSetFolder(DispatchBaseClass):
	"""_ITestSetFolder Interface"""
	CLSID = IID('{5FCA1AE7-AE4A-4709-BD98-13240D68BBD4}')
	coclass_clsid = IID('{606FDBBB-55A7-48D6-BF70-06A7CE08049C}')

	def _FindChildNode_(self, ChildName=defaultNamedNotOptArg, Recursive=defaultNamedNotOptArg, pNode=pythoncom.Missing, pIndex=pythoncom.Missing):
		return self._ApplyTypes_(1610678272, 1, (24, 0), ((8, 1), (11, 1), (16393, 2), (16387, 2)), u'_FindChildNode_', None,ChildName
			, Recursive, pNode, pIndex)

	def _IsInitialized_(self):
		return self._oleobj_.InvokeTypes(1610678273, LCID, 1, (11, 0), (),)

	def _MoveChild_(self, Node=defaultNamedNotOptArg, Order=defaultNamedNotOptArg):
		return self._oleobj_.InvokeTypes(1610678275, LCID, 1, (24, 0), ((9, 1), (11, 1)),Node
			, Order)

	def _SetNodeData_(self, NodeData=defaultNamedNotOptArg):
		return self._oleobj_.InvokeTypes(1610678274, LCID, 1, (24, 0), ((8, 1),),NodeData
			)

	_prop_map_get_ = {
	}
	_prop_map_put_ = {
	}

class _ITreeNode(DispatchBaseClass):
	"""_ITreeNode Interface."""
	CLSID = IID('{81113FC0-ECB4-43E5-AEF6-BFEEC9560CB4}')
	coclass_clsid = IID('{BCE38540-36CC-4C3A-B474-11C8E8EEEDE0}')

	def _FindChildNode_(self, ChildName=defaultNamedNotOptArg, Recusive=defaultNamedNotOptArg, pNode=pythoncom.Missing):
		return self._ApplyTypes_(1610678273, 1, (24, 0), ((8, 1), (11, 1), (16393, 2)), u'_FindChildNode_', None,ChildName
			, Recusive, pNode)

	def _GetFatherID_(self, pVal=pythoncom.Missing):
		return self._ApplyTypes_(1610678272, 1, (24, 0), ((16387, 2),), u'_GetFatherID_', None,pVal
			)

	def _IsInitialized_(self):
		return self._oleobj_.InvokeTypes(1610678274, LCID, 1, (11, 0), (),)

	def _MoveChild(self, Node=defaultNamedNotOptArg, Order=defaultNamedNotOptArg):
		return self._oleobj_.InvokeTypes(1610678276, LCID, 1, (24, 0), ((9, 1), (11, 1)),Node
			, Order)

	def _SetNodeData_(self, NodeData=defaultNamedNotOptArg):
		return self._oleobj_.InvokeTypes(1610678275, LCID, 1, (24, 0), ((8, 1),),NodeData
			)

	_prop_map_get_ = {
	}
	_prop_map_put_ = {
	}

from win32com.client import CoClassBaseClass
class ActionPermission(CoClassBaseClass): # A CoClass
	# Used to determine whether a specified action can be performed by users.
	CLSID = IID('{8D7BCC69-04CB-422A-8F85-2654E3539340}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IActionPermission,
	]
	default_interface = IActionPermission

class Alert(CoClassBaseClass): # A CoClass
	# Notifications sent to the user.
	CLSID = IID('{0A29F8B6-0FFE-4783-B3B0-71FB3FB8D6D9}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IAlert,
	]
	default_interface = IAlert

class AlertManager(CoClassBaseClass): # A CoClass
	# Services for managing alerts.
	CLSID = IID('{7F04BEF8-6460-4BF2-825A-F758E8657F51}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IAlertManager,
	]
	default_interface = IAlertManager

# This CoClass is known by the name 'TDClient80.AmarillusHash.1'
class AmarillusHash(CoClassBaseClass): # A CoClass
	# Support for hash generation.
	CLSID = IID('{61C395DB-BDD5-4431-995D-E5F38E8FAC70}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IAmarillusHash,
	]
	default_interface = IAmarillusHash

class Analysis(CoClassBaseClass): # A CoClass
	# Analysis Class
	CLSID = IID('{E8D39BCF-F7AF-4DE0-82B2-8B51AE2C8427}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IAnalysis,
	]
	default_interface = IAnalysis

class Attachment(CoClassBaseClass): # A CoClass
	# Represents a single file or Internet address attached to a field object.
	CLSID = IID('{46A97504-127F-4A93-BCD0-889AF362754E}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IBaseField2,
		IAttachment,
	]
	default_interface = IAttachment

class AttachmentFactory(CoClassBaseClass): # A CoClass
	# Services to manage attachments of the current field object.
	CLSID = IID('{9D02F17C-BECD-4845-AA04-5326832104AA}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IAttachmentFactory,
		IBaseFactory2,
	]
	default_interface = IAttachmentFactory

class AttachmentVcs(CoClassBaseClass): # A CoClass
	# Represents a Version Control System attachment.
	CLSID = IID('{6C25C267-173B-45CB-8150-7BD1F9584981}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IAttachmentVcs,
	]
	default_interface = IAttachmentVcs

class AuditProperty(CoClassBaseClass): # A CoClass
	# A property associated with an AuditRecord.
	CLSID = IID('{64162251-9AE0-45BE-892D-535D8C5DD062}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IAuditProperty,
		IBaseField2,
	]
	default_interface = IAuditProperty

class AuditPropertyFactory(CoClassBaseClass): # A CoClass
	# Services for managing AuditProperty objects.
	CLSID = IID('{012F18E6-986E-4C65-A5D8-5F1696BC220E}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IAuditPropertyFactory,
	]
	default_interface = IAuditPropertyFactory

class AuditRecord(CoClassBaseClass): # A CoClass
	# A data change that is tracked.
	CLSID = IID('{9BD93246-17D5-4A2C-9318-41B5FDBF0B51}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IBaseField2,
		IAuditRecord,
	]
	default_interface = IAuditRecord

class AuditRecordFactory(CoClassBaseClass): # A CoClass
	# Services for managing AuditRecord objects.
	CLSID = IID('{43A652C9-63BA-44DE-BDF4-642D0596EDD2}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IAuditRecordFactory,
	]
	default_interface = IAuditRecordFactory

class Auditable(CoClassBaseClass): # A CoClass
	# Services to allow changes to an entity to be tracked.
	CLSID = IID('{D70D7E57-CD6F-4FCA-954F-D73C30B9FD90}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IAuditable,
	]
	default_interface = IAuditable

class BPComponent(CoClassBaseClass): # A CoClass
	# A Business Process Component.
	CLSID = IID('{C64F7478-FF48-49A7-8260-251EAB8D5B7C}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IBPComponent,
	]
	default_interface = IBPComponent

class BPGroup(CoClassBaseClass): # A CoClass
	# Support for grouping business process components so that they iterate as a unit.
	CLSID = IID('{0940D6D1-1A04-4274-8568-7F1C82BFCAFA}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IBPGroup,
	]
	default_interface = IBPGroup

class BPHistoryRecord(CoClassBaseClass): # A CoClass
	# BPHistoryRecord Class.
	CLSID = IID('{27F5B75D-9DB6-44A3-AAA7-0713836A15A6}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IBPHistoryRecord,
	]
	default_interface = IBPHistoryRecord

class BPIteration(CoClassBaseClass): # A CoClass
	# The design-time definition of a Business Process Component iteration.
	CLSID = IID('{6E67A18A-81DD-4546-9D64-10701F5E9558}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IBPIteration,
	]
	default_interface = IBPIteration

class BPIterationParam(CoClassBaseClass): # A CoClass
	# An instance of a Business Process Component parameter that belongs to a single interation of the component.
	CLSID = IID('{F0D306D2-2BE8-43E8-B229-9B8EA279B8D5}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IBPIterationParam,
	]
	default_interface = IBPIterationParam

class BPParameter(CoClassBaseClass): # A CoClass
	# A Business Process Component parameter.
	CLSID = IID('{67CDC9EF-6EEB-4E19-80E1-77855970409A}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IBPParam,
	]
	default_interface = IBPParam

class BPStepFactory(CoClassBaseClass): # A CoClass
	# Creates BPT test steps for a Run object.
	CLSID = IID('{C8511847-C115-4680-9512-16D4371F9D58}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IBaseFactory,
		IBaseFactory2,
	]
	default_interface = IBaseFactory

class BPStepParam(CoClassBaseClass): # A CoClass
	# A Business Process Component step parameter during a manual run of the component.
	CLSID = IID('{2824DF5E-75A6-475C-AE4D-3DC4E62EA4F5}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IBPStepParam,
	]
	default_interface = IBPStepParam

class BPStepParamFactory(CoClassBaseClass): # A CoClass
	# Services for managing BPStepParam objects.
	CLSID = IID('{396C2A71-28E7-4410-8BD9-380953D4B606}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IBaseFactory,
		IBaseFactory2,
	]
	default_interface = IBaseFactory

class Bug(CoClassBaseClass): # A CoClass
	# A defect.
	CLSID = IID('{AF9180F9-8C16-4824-9EA1-A9010B072B2C}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IBaseField2,
		IFollowUpManager,
		ILinkable,
		IAlertable,
		IBug,
	]
	default_interface = IBug

class BugFactory(CoClassBaseClass): # A CoClass
	# Services for managing defects.
	CLSID = IID('{5DB9FED8-D26F-4F27-911F-99216ED75986}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IBaseFactory2,
		IAlertableEntityFactory,
		IBugFactory,
		ISearchableFactory,
	]
	default_interface = IBugFactory

class BusinessProcess(CoClassBaseClass): # A CoClass
	# BusinessProcess Class.
	CLSID = IID('{5BE353A8-508B-48B5-848B-53DF49791E52}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IBaseField2,
		IBusinessProcess,
	]
	default_interface = IBusinessProcess

# This CoClass is known by the name 'TDApiOle80.CacheMgr.1'
class CacheMgr(CoClassBaseClass): # A CoClass
	# Manages the system cache in conjunction with the ExtendedStorage objects.
	CLSID = IID('{21DB8AA3-957F-4ACE-9CA7-AECFF8085BB4}')
	coclass_sources = [
	]
	coclass_interfaces = [
		ICacheMgr,
	]
	default_interface = ICacheMgr

class Change(CoClassBaseClass): # A CoClass
	# Change Class.
	CLSID = IID('{DBCF02A8-E281-4C0B-92B8-4F8AF677DD6C}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IChange,
	]
	default_interface = IChange

class ChangeEntry(CoClassBaseClass): # A CoClass
	# ChangeEntry Class.
	CLSID = IID('{34881384-E4DD-40D2-AF7B-F7A14D226F2D}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IChangeEntry,
	]
	default_interface = IChangeEntry

class ChangeEntryFactory(CoClassBaseClass): # A CoClass
	# ChangeEntryFactory Class.
	CLSID = IID('{D7B3D7EE-FC85-460D-AB27-21E86176599F}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IBaseFactory,
		IBaseFactory2,
	]
	default_interface = IBaseFactory

class ChangeFactory(CoClassBaseClass): # A CoClass
	# ChangeFactory Class.
	CLSID = IID('{2291E05E-F2FE-4BDC-BA75-706788C70F62}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IBaseFactory,
		IBaseFactory2,
	]
	default_interface = IBaseFactory

# This CoClass is known by the name 'TDClient80.ComFrec.1'
class ComFrec(CoClassBaseClass): # A CoClass
	# ComFrec Class.
	CLSID = IID('{B2F590F7-BD30-45DD-90B7-F243D7E8B210}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IComFrec,
	]
	default_interface = IComFrec

class Command(CoClassBaseClass): # A CoClass
	# Represents a database command.
	CLSID = IID('{56FD2617-AEC0-46C8-805A-E69481480B68}')
	coclass_sources = [
	]
	coclass_interfaces = [
		ICommand,
	]
	default_interface = ICommand

class Component(CoClassBaseClass): # A CoClass
	# Represents a Business Process Component.
	CLSID = IID('{71CFAB12-C911-4E7A-B4CE-3F65E45344A1}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IComponent,
		IBaseField2,
	]
	default_interface = IComponent

class ComponentFactory(CoClassBaseClass): # A CoClass
	# Services for managing Business Process Components.
	CLSID = IID('{0B280A46-1606-4FD3-90E4-EF5149725200}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IComponentFactory,
		IBaseFactory2,
		ISearchableFactory,
	]
	default_interface = IComponentFactory

class ComponentFolder(CoClassBaseClass): # A CoClass
	# Represents a Business Process Component folder.
	CLSID = IID('{E5A4F777-79B4-4999-B9CF-4733946D6381}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IBaseField2,
		IComponentFolder,
	]
	default_interface = IComponentFolder

class ComponentFolderFactory(CoClassBaseClass): # A CoClass
	# ComponentFolderFactory Class.
	CLSID = IID('{36F5E5F1-C949-40AF-91E8-A4BC647B2A92}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IBaseFactory2,
		IComponentFolderFactory,
	]
	default_interface = IComponentFolderFactory

class ComponentParam(CoClassBaseClass): # A CoClass
	# Services for managing Business Process Test parameters.
	CLSID = IID('{4FFF066E-4D6B-444B-94B1-73209211C62B}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IBaseField2,
		IBaseFieldEx,
		IComponentParam,
	]
	default_interface = IComponentParam

class ComponentParamFactory(CoClassBaseClass): # A CoClass
	# ComponentParamFactory Class.
	CLSID = IID('{05A59D3B-C4D9-4E73-BD19-D7D0233D3E1C}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IBaseFactory,
		IBaseFactory2,
	]
	default_interface = IBaseFactory

class ComponentStep(CoClassBaseClass): # A CoClass
	# Represents a step in a Business Process Component.
	CLSID = IID('{D600F6C4-1A12-4973-80A9-A976300DF1D7}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IBaseField2,
		IComponentStep,
	]
	default_interface = IComponentStep

class ComponentStepFactory(CoClassBaseClass): # A CoClass
	# Services to manage ComponentStep objects.
	CLSID = IID('{46C05B73-AAE3-4420-9B38-6F221471632D}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IBaseFactory,
		IBaseFactory2,
	]
	default_interface = IBaseFactory

class Condition(CoClassBaseClass): # A CoClass
	# Represents a condition for a test to be executed.
	CLSID = IID('{453FE104-67B0-470D-8D1E-70E7A8CF7774}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IBaseField2,
		ICondition,
	]
	default_interface = ICondition

class ConditionFactory(CoClassBaseClass): # A CoClass
	# Services for managing conditions for executing test instances in an test set.
	CLSID = IID('{EEBD59D7-740A-48EE-82F2-14BC33B54F14}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IBaseFactory2,
		IConditionFactory,
	]
	default_interface = IConditionFactory

class CoverageEntity(CoClassBaseClass): # A CoClass
	# Represents the association between a requirement and another entity that covers it.
	CLSID = IID('{F87FF5C2-4631-4E5E-BF46-38490501A12B}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IBaseField2,
		ICoverageEntity,
	]
	default_interface = ICoverageEntity

class CoverageFactory(CoClassBaseClass): # A CoClass
	# Services for managing CoverageEntity objects.
	CLSID = IID('{D68C44C2-DF91-4095-B145-7FF8E430FBA7}')
	coclass_sources = [
	]
	coclass_interfaces = [
		ICoverageFactory,
		IBaseFactory2,
	]
	default_interface = ICoverageFactory

class Customization(CoClassBaseClass): # A CoClass
	# Services to perform customization tasks, such as adding users to user groups, defining user-defined fields, and defining user access privileges.
	CLSID = IID('{E6F08C7F-A34B-43DA-A632-9410170F3D00}')
	coclass_sources = [
	]
	coclass_interfaces = [
		ICustomization2,
	]
	default_interface = ICustomization2

class CustomizationAction(CoClassBaseClass): # A CoClass
	# Represents a type of user action such as adding or deleting a bug. Actions are listed in the AC_ACTION_NAME field of the Actions table.
	CLSID = IID('{2BE06C1F-06AF-4EA7-B3D2-B5CAFD13AE88}')
	coclass_sources = [
	]
	coclass_interfaces = [
		ICustomizationAction,
	]
	default_interface = ICustomizationAction

class CustomizationActions(CoClassBaseClass): # A CoClass
	# The collection of all CustomizationAction objects.
	CLSID = IID('{A74CE8AE-2AA0-47F4-A85E-A6E96DA8EA1E}')
	coclass_sources = [
	]
	coclass_interfaces = [
		ICustomizationActions,
	]
	default_interface = ICustomizationActions

class CustomizationField(CoClassBaseClass): # A CoClass
	# Represents a user-defined field.
	CLSID = IID('{48BB9422-E786-463A-8617-25ADA366FAA0}')
	coclass_sources = [
	]
	coclass_interfaces = [
		ICustomizationField2,
	]
	default_interface = ICustomizationField2

class CustomizationFields(CoClassBaseClass): # A CoClass
	# The collection of all CustomizationField objects.
	CLSID = IID('{FAE217FE-0F81-471F-B4A8-73B045C1710C}')
	coclass_sources = [
	]
	coclass_interfaces = [
		ICustomizationFields,
	]
	default_interface = ICustomizationFields

class CustomizationList(CoClassBaseClass): # A CoClass
	# Represents a user-defined list associated with a field.
	CLSID = IID('{1E3AB54D-4D98-4B9F-9A0B-E6427054FED6}')
	coclass_sources = [
	]
	coclass_interfaces = [
		ICustomizationList,
	]
	default_interface = ICustomizationList

class CustomizationListNode(CoClassBaseClass): # A CoClass
	# Represents a node in a list.
	CLSID = IID('{80A02CF4-53FB-499C-9549-2EE19815179A}')
	coclass_sources = [
	]
	coclass_interfaces = [
		ICustomizationListNode,
	]
	default_interface = ICustomizationListNode

class CustomizationLists(CoClassBaseClass): # A CoClass
	# The collection of all CustomizationList objects.
	CLSID = IID('{6B8183C1-5676-4315-B202-2B78A15E1D6C}')
	coclass_sources = [
	]
	coclass_interfaces = [
		ICustomizationLists,
	]
	default_interface = ICustomizationLists

class CustomizationMailCondition(CoClassBaseClass): # A CoClass
	# Configuration of custom automatic mail notifications.
	CLSID = IID('{78D71CB3-1DC2-49A6-BF0C-6D36AFD9E93E}')
	coclass_sources = [
	]
	coclass_interfaces = [
		ICustomizationMailCondition,
	]
	default_interface = ICustomizationMailCondition

class CustomizationMailConditions(CoClassBaseClass): # A CoClass
	# The collection of CustomizationMailCondition configurations for custom automatic mail notifications.
	CLSID = IID('{93FB7AD9-0302-4EA3-8BE5-3437F4F52906}')
	coclass_sources = [
	]
	coclass_interfaces = [
		ICustomizationMailConditions,
	]
	default_interface = ICustomizationMailConditions

class CustomizationModules(CoClassBaseClass): # A CoClass
	# Services for managing the customization modules.
	CLSID = IID('{78B7C423-5E9B-4225-811A-95ABE879B461}')
	coclass_sources = [
	]
	coclass_interfaces = [
		ICustomizationModules2,
	]
	default_interface = ICustomizationModules2

class CustomizationPermissions(CoClassBaseClass): # A CoClass
	# Properties that define the ability of user groups to add, remove, and modify entities.
	CLSID = IID('{01C802C0-3021-4FE3-91AB-8C2AD5116BB4}')
	coclass_sources = [
	]
	coclass_interfaces = [
		ICustomizationPermissions,
	]
	default_interface = ICustomizationPermissions

class CustomizationTransitionRule(CoClassBaseClass): # A CoClass
	# Represents a single transition rule.
	CLSID = IID('{F4332BCF-1D24-4F24-AD5E-FEDEC7FAFBB6}')
	coclass_sources = [
	]
	coclass_interfaces = [
		ICustomizationTransitionRule,
	]
	default_interface = ICustomizationTransitionRule

class CustomizationTransitionRules(CoClassBaseClass): # A CoClass
	# A collection of CustomizationTransitionRule objects applied to a specific field and user group.
	CLSID = IID('{1570FD42-5F4B-49B2-839C-712098B3543E}')
	coclass_sources = [
	]
	coclass_interfaces = [
		ICustomizationTransitionRules,
	]
	default_interface = ICustomizationTransitionRules

class CustomizationUser(CoClassBaseClass): # A CoClass
	# Represents a user for purposes of adding and removing the user to and from user groups.
	CLSID = IID('{A870D9D0-B890-45E7-B3B3-53B5FA0C4F03}')
	coclass_sources = [
	]
	coclass_interfaces = [
		ICustomizationUser,
	]
	default_interface = ICustomizationUser

class CustomizationUsers(CoClassBaseClass): # A CoClass
	# The collection of all CustomizationUser objects.
	CLSID = IID('{8FE1F909-64F2-49B3-908F-4F15C64D9F08}')
	coclass_sources = [
	]
	coclass_interfaces = [
		ICustomizationUsers,
	]
	default_interface = ICustomizationUsers

class CustomizationUsersGroup(CoClassBaseClass): # A CoClass
	# Represents a user group for purposes of adding and removing users.
	CLSID = IID('{7545BFC6-913F-4DF9-9F70-C815E13E4CBA}')
	coclass_sources = [
	]
	coclass_interfaces = [
		ICustomizationUsersGroup,
	]
	default_interface = ICustomizationUsersGroup

class CustomizationUsersGroups(CoClassBaseClass): # A CoClass
	# The collection of all CustomizationUsersGroup objects.
	CLSID = IID('{AB6C55FF-1D41-4AE7-A8FD-6C7BA8BE2620}')
	coclass_sources = [
	]
	coclass_interfaces = [
		ICustomizationUsersGroups,
	]
	default_interface = ICustomizationUsersGroups

class DBManager(CoClassBaseClass): # A CoClass
	# DBManager Class.
	CLSID = IID('{F8BBE233-A298-4C0D-8D86-3D442BB8ED08}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IDBManager,
	]
	default_interface = IDBManager

class DesignStep(CoClassBaseClass): # A CoClass
	# Represents a single design step in a test.
	CLSID = IID('{19AD0931-4FF6-4B1C-83AB-464A9D200345}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IBaseField2,
		IDesignStep,
	]
	default_interface = IDesignStep

class DesignStepFactory(CoClassBaseClass): # A CoClass
	# Adds and removes design steps in a Test object.
	CLSID = IID('{D5941686-5E86-431D-87E3-A06D67B3AC44}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IBaseFactory,
		IBaseFactory2,
	]
	default_interface = IBaseFactory

class ExecEventActionParams(CoClassBaseClass): # A CoClass
	# Actions to be taken after the completion of a test set run.
	CLSID = IID('{F64C065B-D9BF-41CB-91AD-C1672A8220DF}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IOnExecEventSchedulerActionParams,
	]
	default_interface = IOnExecEventSchedulerActionParams

class ExecEventInfo(CoClassBaseClass): # A CoClass
	# The execution information of the scheduler. This object handles the actual, not the planned, information.
	CLSID = IID('{0409CEAA-2CAA-4596-A9EF-B9F0644D17EA}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IExecEventInfo,
	]
	default_interface = IExecEventInfo

class ExecEventNotifyByMailSettings(CoClassBaseClass): # A CoClass
	# Represents the notification to be sent by email after a test has completed its run.
	CLSID = IID('{D1D6B200-B24D-4C8D-8622-77FDBF49AC99}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IExecEventNotifyByMailSettings,
	]
	default_interface = IExecEventNotifyByMailSettings

class ExecEventRestartActionParams(CoClassBaseClass): # A CoClass
	# Information on actions to be taken during restart after completion of a test set.
	CLSID = IID('{A57D9723-040E-44C9-829C-64439CC526C1}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IOnExecEventSchedulerRestartParams,
	]
	default_interface = IOnExecEventSchedulerRestartParams

class ExecSettings(CoClassBaseClass): # A CoClass
	# The information on the execution of a test set.
	CLSID = IID('{85F1A554-CF12-42A1-BC3F-BBF8BBE83DF1}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IExecutionSettings,
	]
	default_interface = IExecutionSettings

class ExecutionStatus(CoClassBaseClass): # A CoClass
	# Represents the execution status of the scheduler.
	CLSID = IID('{E2E80F73-808A-495E-89F6-430721FE9623}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IExecutionStatus,
	]
	default_interface = IExecutionStatus

# This CoClass is known by the name 'TDClient.Export.1'
class Export(CoClassBaseClass): # A CoClass
	# Export Class.
	CLSID = IID('{DCB4C421-E9F4-4A89-9190-B49411B17167}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IExport,
	]
	default_interface = IExport

class ExtendedStorage(CoClassBaseClass): # A CoClass
	# Represents a storage structure used to transfer files between the server and client file system and delete files.
	CLSID = IID('{C79B8E7E-8AAF-47B5-94A3-C824BF8453E6}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IExtendedStorage,
	]
	default_interface = IExtendedStorage

class ExtendedStorageProxy(CoClassBaseClass): # A CoClass
	# Represents the project repository. The implementation is either in the file system or database depending on the project configuration.
	CLSID = IID('{41BAF7D8-718C-45B6-9115-C91850F27074}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IExtendedStorage,
	]
	default_interface = IExtendedStorage

class FactoryList(CoClassBaseClass): # A CoClass
	# Create and maintain lists within entity factories. Use any factory object to create any number of list instances for objects in the factory.
	CLSID = IID('{3F767372-C6D1-4F1B-8F27-1EABB614551C}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IFactoryList,
	]
	default_interface = IFactoryList

class FieldProperty(CoClassBaseClass): # A CoClass
	# Properties for object fields.
	CLSID = IID('{B7E70913-D656-4DB9-8763-3812FC75BB46}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IFieldProperty2,
	]
	default_interface = IFieldProperty2

class FileData(CoClassBaseClass): # A CoClass
	# Represents folder or file information.
	CLSID = IID('{37B25CC5-C319-4DAD-A57D-05A8D1201EE0}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IFileData,
	]
	default_interface = IFileData

class Graph(CoClassBaseClass): # A CoClass
	# Represents a graph built through a method.
	CLSID = IID('{8CEB0A26-08A1-4110-A851-6BB50278BF94}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IGraph,
	]
	default_interface = IGraph

class GraphBuilder(CoClassBaseClass): # A CoClass
	# Services for creating graphs.
	CLSID = IID('{771F8212-D6CE-48A1-9F3A-87EF3E6E4CD5}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IGraphBuilder,
	]
	default_interface = IGraphBuilder

class GraphDefinition(CoClassBaseClass): # A CoClass
	# GraphDefinitions Class
	CLSID = IID('{FDF08CE0-896F-498D-928D-F85512B52F4F}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IGraphDefinition,
	]
	default_interface = IGraphDefinition

class GroupingItem(CoClassBaseClass): # A CoClass
	# A set of entities having the same value in a specific field.
	CLSID = IID('{904CED76-CF4A-4C85-BB23-2B4A9DCB1D6A}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IGroupingItem,
	]
	default_interface = IGroupingItem

class GroupingManager(CoClassBaseClass): # A CoClass
	# Provides services for grouping.
	CLSID = IID('{F801F7A2-04DF-4DD3-8A5E-C0CC66E0595E}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IGroupingManager,
	]
	default_interface = IGroupingManager

class HierarchyFilter(CoClassBaseClass): # A CoClass
	# HierarchyFilter Class
	CLSID = IID('{DCA4F956-F4B7-43BB-B544-58E56D0DEDC8}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IHierarchyFilter,
	]
	default_interface = IHierarchyFilter

class HierarchySupportList(CoClassBaseClass): # A CoClass
	# HierarchySupportList Class
	CLSID = IID('{E214FA66-87AE-4088-8F25-75DBDCB21B7D}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IHierarchySupportList,
	]
	default_interface = IHierarchySupportList

class History(CoClassBaseClass): # A CoClass
	# Enables retrieval of a list of history records.
	CLSID = IID('{1422200A-A929-430A-8683-6D8FDCA8BA89}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IHistory,
	]
	default_interface = IHistory

class HistoryRecord(CoClassBaseClass): # A CoClass
	# Represents a single change.
	CLSID = IID('{89E2DE95-A1CA-47D1-ABCE-21D48A645E98}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IHistoryRecord2,
	]
	default_interface = IHistoryRecord2

class Host(CoClassBaseClass): # A CoClass
	# Represents a single host server.
	CLSID = IID('{6AF3BFA2-C914-4FF8-9531-A047745F2597}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IBaseField2,
		IHost,
	]
	default_interface = IHost

class HostFactory(CoClassBaseClass): # A CoClass
	# Services for managing host servers definitions.
	CLSID = IID('{19F58A3A-0E31-441A-A95B-70F416B7AC53}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IBaseFactory,
		IBaseFactory2,
	]
	default_interface = IBaseFactory

class HostGroup(CoClassBaseClass): # A CoClass
	# Services for managing a group of host servers.
	CLSID = IID('{78E51781-1777-4A05-A13F-1BB9FAFE3E48}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IHostGroup,
	]
	default_interface = IHostGroup

class HostGroupFactory(CoClassBaseClass): # A CoClass
	# Creates and removes groups of host servers.
	CLSID = IID('{29823A4B-9739-4E55-972D-6091096E9090}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IBaseFactory2,
		IHostGroupFactory,
	]
	default_interface = IHostGroupFactory

# This CoClass is known by the name 'TDClient.Import.1'
class Import(CoClassBaseClass): # A CoClass
	# Import Class.
	CLSID = IID('{CD3E5686-4B11-462F-9619-D2FA447DCE96}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IImport,
	]
	default_interface = IImport

class Link(CoClassBaseClass): # A CoClass
	# Represents an association between a Bug and another entity.
	CLSID = IID('{75180DF9-EF06-4FB4-8EDB-5697713AB54C}')
	coclass_sources = [
	]
	coclass_interfaces = [
		ILink,
		IBaseField2,
	]
	default_interface = ILink

class LinkFactory(CoClassBaseClass): # A CoClass
	# Services to manage Link objects.
	CLSID = IID('{2D52E7C5-B6DE-4D8D-B885-7C1F1DC509B8}')
	coclass_sources = [
	]
	coclass_interfaces = [
		ILinkFactory,
		IBaseFactory2,
	]
	default_interface = ILinkFactory

# This CoClass is known by the name 'TDApiOle80.List.1'
class List(CoClassBaseClass): # A CoClass
	# Create and maintain lists. Use any factory object to create any number of list instances for objects in the factory.
	CLSID = IID('{9007A7F1-AC71-4563-A943-CFF4051E7E3D}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IList,
	]
	default_interface = IList

class MultiValue(CoClassBaseClass): # A CoClass
	# Represents several values stored in a single field.
	CLSID = IID('{D2990AC2-106D-4889-B299-D6D4223649E6}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IMultiValue,
	]
	default_interface = IMultiValue

class ProductInfo(CoClassBaseClass): # A CoClass
	# Information about the current version.
	CLSID = IID('{73D2B96A-0A3F-41CA-A8F2-E371220B4C63}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IProductInfo,
	]
	default_interface = IProductInfo

class ProjectProperties(CoClassBaseClass): # A CoClass
	# Global project parameters and settings.
	CLSID = IID('{31905CFC-D317-4B8D-85DC-F63D8D3E6E28}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IProjectProperties,
	]
	default_interface = IProjectProperties

class ProjectRepository(CoClassBaseClass): # A CoClass
	# Represents the project repository when stored in the database. Not used if the repository is on the file system.
	CLSID = IID('{49444E30-82A0-4786-9566-E7DA73044631}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IExtendedStorage,
	]
	default_interface = IExtendedStorage

class RTParam(CoClassBaseClass): # A CoClass
	# RTParameter Class
	CLSID = IID('{5666ABB3-0FC1-4C22-A4B0-8D43CD1E54A3}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IRTParam,
	]
	default_interface = IRTParam

class Recordset(CoClassBaseClass): # A CoClass
	# Represents the entire set of records resulting from an executed command. At any given time, a Recordset object refers to a single record within the record set as the current record.
	CLSID = IID('{4FEDB030-AE1C-407C-8732-0A9E042E9B27}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IRecordset,
	]
	default_interface = IRecordset

class Req(CoClassBaseClass): # A CoClass
	# Represents a requirement for which testing must be performed.
	CLSID = IID('{A94555C2-77AD-4F2D-B061-A9ED11AE9FE6}')
	coclass_sources = [
	]
	coclass_interfaces = [
		ICoverableReq,
		IReq,
		ILinkable,
		IBaseField2,
	]
	default_interface = IReq

class ReqCoverageFactory(CoClassBaseClass): # A CoClass
	# ReqCoverageFactory Class
	CLSID = IID('{AAFCDC5B-5E1A-4E41-829A-225C14BB0C88}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IBaseFactory2,
		IReqCoverageFactory,
	]
	default_interface = IReqCoverageFactory

class ReqCoverageStatus(CoClassBaseClass): # A CoClass
	# ReqCoverageStatus Class.
	CLSID = IID('{54770024-A5FB-4710-8A9E-64A9A0DCBCFA}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IReqCoverageStatus,
	]
	default_interface = IReqCoverageStatus

class ReqFactory(CoClassBaseClass): # A CoClass
	# Services for managing requirements.
	CLSID = IID('{F4E856D4-FCD7-11D4-9D8A-0001029DEAF5}')
	coclass_sources = [
	]
	coclass_interfaces = [
		ISearchableFactory,
		IReqFactory2,
		IBaseFactory2,
	]
	default_interface = IReqFactory2

class ReqSummaryStatus(CoClassBaseClass): # A CoClass
	# ReqSummaryStatus Class.
	CLSID = IID('{6C760530-757D-4D83-B07A-DD5F29559457}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IReqSummaryStatus,
	]
	default_interface = IReqSummaryStatus

class Rule(CoClassBaseClass): # A CoClass
	# Represents a rule for generating an alert.
	CLSID = IID('{B2FDDFE1-6019-4444-9EE6-479FA0E554A2}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IRule,
	]
	default_interface = IRule

class RuleManager(CoClassBaseClass): # A CoClass
	# Services for managing the notification rules.
	CLSID = IID('{2C6320EE-DE3C-46E3-9BCB-7204F3ADEC19}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IRuleManager,
	]
	default_interface = IRuleManager

class Run(CoClassBaseClass): # A CoClass
	# Represents a test run.
	CLSID = IID('{5AB19406-71C9-40F0-AFD3-4EF9A77FE648}')
	coclass_sources = [
	]
	coclass_interfaces = [
		ILinkable,
		IRun2,
		IBaseField2,
	]
	default_interface = IRun2

class RunFactory(CoClassBaseClass): # A CoClass
	# Creates test runs.
	CLSID = IID('{3A24FCD2-D5E3-4B5E-82BE-2202DD566FAD}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IRunFactory,
		IBaseFactory2,
	]
	default_interface = IRunFactory

class SearchOptions(CoClassBaseClass): # A CoClass
	# Options for controlling searches.
	CLSID = IID('{EEFE895F-49F8-4E82-9E78-4A6596B5E6F2}')
	coclass_sources = [
	]
	coclass_interfaces = [
		ISearchOptions,
	]
	default_interface = ISearchOptions

class Settings(CoClassBaseClass): # A CoClass
	# Represents users' settings in various user-defined categories.
	CLSID = IID('{BD969260-2F3A-40D0-BE71-7ACE587FB343}')
	coclass_sources = [
	]
	coclass_interfaces = [
		ISettings2,
	]
	default_interface = ISettings2

class Step(CoClassBaseClass): # A CoClass
	# Represents a test step in a test run.
	CLSID = IID('{C29AB208-56D4-493C-80FB-EA952235BAFB}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IBaseField2,
		ILinkable,
		IStep2,
	]
	default_interface = IStep2

class StepFactory(CoClassBaseClass): # A CoClass
	# Creates test steps for a Run object.
	CLSID = IID('{12BEFCBF-0269-4BC3-9745-8B6BF5895027}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IBaseFactory,
		IBaseFactory2,
	]
	default_interface = IBaseFactory

class StepParams(CoClassBaseClass): # A CoClass
	# A collection of test parameters.
	CLSID = IID('{86BDCEC4-35BD-4DA8-8373-474C4EBDFA9F}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IStepParams,
	]
	default_interface = IStepParams

class SubjectNode(CoClassBaseClass): # A CoClass
	# Represents a subject folder in a Quality Center subject tree.
	CLSID = IID('{7300228B-0F1B-44D3-BF3A-790253622527}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IBaseField2,
		_ITreeNode,
		ISubjectNode,
		IObjectLockingSupport,
	]
	default_interface = ISubjectNode

class SysTreeNode(CoClassBaseClass): # A CoClass
	# Represents a system folder, that is, a tree node in the TreeManager object.
	CLSID = IID('{BCE38540-36CC-4C3A-B474-11C8E8EEEDE0}')
	coclass_sources = [
	]
	coclass_interfaces = [
		ISysTreeNode,
		IBaseField2,
		_ITreeNode,
	]
	default_interface = ISysTreeNode

class TDChat(CoClassBaseClass): # A CoClass
	# TDChat Class.
	CLSID = IID('{C7CE06D8-F6D2-47B1-AB82-EE9A762171CD}')
	coclass_sources = [
	]
	coclass_interfaces = [
		ITDChat,
	]
	default_interface = ITDChat

# This CoClass is known by the name 'TDApiOle80.TDConnection.1'
class TDConnection(CoClassBaseClass): # A CoClass
	# Represents a single server connection.
	CLSID = IID('{C5CBD7B2-490C-45F5-8C40-B8C3D108E6D7}')
	coclass_sources = [
		_DIProgressEvents,
		_DIOtaEvents,
	]
	default_source = _DIOtaEvents
	coclass_interfaces = [
		ITDConnection2,
	]
	default_interface = ITDConnection2

class TDErrorInfo(CoClassBaseClass): # A CoClass
	# TDErrorInfo Class.
	CLSID = IID('{5BC906B1-56F3-4DC0-A6BB-78440F908823}')
	coclass_sources = [
	]
	coclass_interfaces = [
		ITDErrorInfo,
	]
	default_interface = ITDErrorInfo

class TDField(CoClassBaseClass): # A CoClass
	# Properties for a field.
	CLSID = IID('{20F3E82A-9A7D-44FA-94C6-03E84C49521D}')
	coclass_sources = [
	]
	coclass_interfaces = [
		ITDField,
	]
	default_interface = ITDField

class TDFilter(CoClassBaseClass): # A CoClass
	# Services for creation of a filtered list of field objects without SQL.
	CLSID = IID('{6F671E95-2720-4583-BCD8-7CC3E5D3C4AF}')
	coclass_sources = [
	]
	coclass_interfaces = [
		ITDFilter2,
	]
	default_interface = ITDFilter2

class TDMailConditions(CoClassBaseClass): # A CoClass
	# Responsible for the configuration of automatic mail notifications.
	CLSID = IID('{90A0FD6D-D4F5-4951-AA4D-3954864997C6}')
	coclass_sources = [
	]
	coclass_interfaces = [
		ITDMailConditions,
	]
	default_interface = ITDMailConditions

# This CoClass is known by the name 'TDClient.TDUtils.1'
class TDUtils(CoClassBaseClass): # A CoClass
	# Utility routines and properties.
	CLSID = IID('{977FEB6A-82DF-4F53-ADA2-F722F7E07D23}')
	coclass_sources = [
	]
	coclass_interfaces = [
		ITDUtils,
	]
	default_interface = ITDUtils

class TSScheduler(CoClassBaseClass): # A CoClass
	# Responsible for executing selected automated tests.
	CLSID = IID('{C121682C-6B6E-4A3F-A1CE-9E78FE897009}')
	coclass_sources = [
	]
	coclass_interfaces = [
		ITSScheduler,
	]
	default_interface = ITSScheduler

class TSTest(CoClassBaseClass): # A CoClass
	# Represents a test instance, or execution test, in a test set. A test instance represents a single use of a planning Test in a TestSet. It can be associated with one or more runs.
	CLSID = IID('{488D6BE8-A962-4958-8E11-6E2EE9A20EFF}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IFollowUpManager,
		ICoverable,
		ILinkable,
		IAlertable,
		ITSTest2,
		IBaseField2,
	]
	default_interface = ITSTest2

class TSTestFactory(CoClassBaseClass): # A CoClass
	# Manages execution tests (TSTest objects) in a test set.
	CLSID = IID('{1586DCBE-08C3-430F-98F1-D3F078A01E34}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IAlertableEntityFactory,
		IBaseFactory,
		IBaseFactory2,
	]
	default_interface = IBaseFactory

class Test(CoClassBaseClass): # A CoClass
	# Represents a planning test.
	CLSID = IID('{FBED7041-8CD1-471D-A1D8-C15A7C1D4CE9}')
	coclass_sources = [
	]
	coclass_interfaces = [
		ITest,
		IFollowUpManager,
		ICoverable,
		ILinkable,
		IAlertable,
		IBaseField2,
	]
	default_interface = ITest

class TestExecStatus(CoClassBaseClass): # A CoClass
	# Represents the execution status of the test currently running.
	CLSID = IID('{170FFBC3-AE31-4AE8-9C65-0F0FC5AD5CCD}')
	coclass_sources = [
	]
	coclass_interfaces = [
		ITestExecStatus,
	]
	default_interface = ITestExecStatus

class TestFactory(CoClassBaseClass): # A CoClass
	# Services for managing tests.
	CLSID = IID('{D398384F-9D08-4E70-8400-910D8750BDAB}')
	coclass_sources = [
	]
	coclass_interfaces = [
		ITestFactory,
		IBaseFactory2,
		IAlertableEntityFactory,
		ISearchableFactory,
	]
	default_interface = ITestFactory

class TestSet(CoClassBaseClass): # A CoClass
	# A group of tests designed to meet a specific testing goal.
	CLSID = IID('{729074D5-C699-4B07-8E8E-25E827C71ED2}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IBaseField2,
		ITestSet,
		ILinkable,
	]
	default_interface = ITestSet

class TestSetFactory(CoClassBaseClass): # A CoClass
	# Services for managing test sets.
	CLSID = IID('{50A81398-8F2F-4F38-B88D-F734C727706C}')
	coclass_sources = [
	]
	coclass_interfaces = [
		ITestSetFactory,
		IBaseFactory2,
	]
	default_interface = ITestSetFactory

class TestSetFolder(CoClassBaseClass): # A CoClass
	# Manages the tests and test sets included in a particular test set folder.
	CLSID = IID('{606FDBBB-55A7-48D6-BF70-06A7CE08049C}')
	coclass_sources = [
	]
	coclass_interfaces = [
		_ITestSetFolder,
		IObjectLockingSupport,
		ITestSetFolder2,
	]
	default_interface = ITestSetFolder2

class TestSetTreeManager(CoClassBaseClass): # A CoClass
	# Manages the test set tree and its related test set folders.
	CLSID = IID('{911B9D10-736A-49A2-9A19-34C3CFE6B2E9}')
	coclass_sources = [
	]
	coclass_interfaces = [
		ITestSetTreeManager,
	]
	default_interface = ITestSetTreeManager

class TextParser(CoClassBaseClass): # A CoClass
	# TextParser Class.
	CLSID = IID('{AAF726A2-9187-4DF5-9528-A3E66CDD28E2}')
	coclass_sources = [
	]
	coclass_interfaces = [
		ITextParser,
	]
	default_interface = ITextParser

class TreeManager(CoClassBaseClass): # A CoClass
	# Represents the system tree, containing a subject tree and all hierarchical field trees.
	CLSID = IID('{77D80D34-C6FF-43A5-9E67-C7E0A81CF3F2}')
	coclass_sources = [
	]
	coclass_interfaces = [
		ITreeManager,
	]
	default_interface = ITreeManager

class VCS(CoClassBaseClass): # A CoClass
	# Represents a Version Control System connection.
	CLSID = IID('{2D1E1D34-561E-40BE-845B-E1F872209A32}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IVCS,
	]
	default_interface = IVCS

class VcsAdmin(CoClassBaseClass): # A CoClass
	# VcsAdmin Class.
	CLSID = IID('{0FC5B9FD-629D-4E3F-8A36-41347ADA3107}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IVcsAdmin,
	]
	default_interface = IVcsAdmin

class VersionItem(CoClassBaseClass): # A CoClass
	# Represents specific version information.
	CLSID = IID('{0DCB2995-2738-4BE5-873D-ADAEB29262FA}')
	coclass_sources = [
	]
	coclass_interfaces = [
		IVersionItem,
	]
	default_interface = IVersionItem

IActionPermission_vtables_dispatch_ = 1
IActionPermission_vtables_ = [
	(( u'ActionEnabled' , u'ActionIdentity' , u'ActionTarget' , u'pVal' , ), 1, (1, (), [ 
			(12, 1, None, None) , (12, 17, None, None) , (16395, 10, None, None) , ], 1 , 2 , 4 , 1 , 28 , (3, 0, None, None) , 0 , )),
]

IAlert_vtables_dispatch_ = 1
IAlert_vtables_ = [
	(( u'Description' , u'pVal' , ), 1, (1, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
	(( u'AlertDate' , u'pVal' , ), 2, (2, (), [ (16391, 10, None, None) , ], 1 , 2 , 4 , 0 , 32 , (3, 0, None, None) , 0 , )),
	(( u'ID' , u'pVal' , ), 3, (3, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 36 , (3, 0, None, None) , 0 , )),
	(( u'AlertType' , u'pVal' , ), 4, (4, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 40 , (3, 0, None, None) , 0 , )),
	(( u'Unread' , u'pVal' , ), 5, (5, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 44 , (3, 0, None, None) , 0 , )),
	(( u'Unread' , u'pVal' , ), 5, (5, (), [ (11, 1, None, None) , ], 1 , 4 , 4 , 0 , 48 , (3, 0, None, None) , 0 , )),
	(( u'Subject' , u'pVal' , ), 6, (6, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 52 , (3, 0, None, None) , 0 , )),
	(( u'ParentEntityURL' , u'pVal' , ), 7, (7, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 56 , (3, 0, None, None) , 64 , )),
]

IAlertManager_vtables_dispatch_ = 1
IAlertManager_vtables_ = [
	(( u'DeleteAlert' , u'IDs' , ), 1, (1, (), [ (12, 1, None, None) , ], 1 , 1 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
	(( u'CleanAllAlerts' , ), 2, (2, (), [ ], 1 , 1 , 4 , 0 , 32 , (3, 0, None, None) , 0 , )),
	(( u'Alert' , u'ID' , u'pVal' , ), 3, (3, (), [ (3, 1, None, None) , 
			(16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 36 , (3, 0, None, None) , 0 , )),
	(( u'AlertList' , u'EntityType' , u'NeedRefresh' , u'pList' , ), 4, (4, (), [ 
			(8, 1, None, None) , (11, 49, 'False', None) , (16393, 10, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , ], 1 , 2 , 4 , 0 , 40 , (3, 0, None, None) , 0 , )),
]

IAlertable_vtables_dispatch_ = 0
IAlertable_vtables_ = [
	(( u'DeleteAlert' , u'IDs' , ), 1610678272, (1610678272, (), [ (12, 1, None, None) , ], 1 , 1 , 4 , 0 , 12 , (3, 0, None, None) , 0 , )),
	(( u'CleanAllAlerts' , ), 1610678273, (1610678273, (), [ ], 1 , 1 , 4 , 0 , 16 , (3, 0, None, None) , 0 , )),
	(( u'GetAlert' , u'ID' , u'pVal' , ), 1610678274, (1610678274, (), [ (3, 1, None, None) , 
			(16393, 10, None, None) , ], 1 , 1 , 4 , 0 , 20 , (3, 0, None, None) , 0 , )),
	(( u'GetAlertList' , u'NeedRefresh' , u'pList' , ), 1610678275, (1610678275, (), [ (11, 49, 'False', None) , 
			(16393, 10, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , ], 1 , 1 , 4 , 0 , 24 , (3, 0, None, None) , 0 , )),
	(( u'HasAlerts' , u'pVal' , ), 1610678276, (1610678276, (), [ (16395, 10, None, None) , ], 1 , 1 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
	(( u'HasNewAlerts' , u'pVal' , ), 1610678277, (1610678277, (), [ (16395, 10, None, None) , ], 1 , 1 , 4 , 0 , 32 , (3, 0, None, None) , 0 , )),
]

IAlertableEntityFactory_vtables_dispatch_ = 0
IAlertableEntityFactory_vtables_ = [
]

IAmarillusHash_vtables_dispatch_ = 1
IAmarillusHash_vtables_ = [
	(( u'GenerateHash' , u'Value' , u'pVal' , ), 1, (1, (), [ (8, 1, None, None) , 
			(16392, 10, None, None) , ], 1 , 1 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
]

IAnalysis_vtables_dispatch_ = 1
IAnalysis_vtables_ = [
	(( u'FilterText' , u'pVal' , ), 1, (1, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
	(( u'FilterText' , u'pVal' , ), 1, (1, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 32 , (3, 0, None, None) , 0 , )),
	(( u'Type' , u'pVal' , ), 2, (2, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 36 , (3, 0, None, None) , 0 , )),
	(( u'Type' , u'pVal' , ), 2, (2, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 40 , (3, 0, None, None) , 0 , )),
	(( u'GetDistinctValues' , u'Fields' , u'pVal' , ), 3, (3, (), [ (12, 1, None, None) , 
			(16396, 10, None, None) , ], 1 , 1 , 4 , 0 , 44 , (3, 0, None, None) , 0 , )),
	(( u'GetSummaryData' , u'Fields' , u'DataFields' , u'Data' , ), 4, (4, (), [ 
			(12, 1, None, None) , (12, 1, None, None) , (16396, 10, None, None) , ], 1 , 1 , 4 , 0 , 48 , (3, 0, None, None) , 0 , )),
	(( u'JoinCondition' , u'pVal' , ), 5, (5, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 52 , (3, 0, None, None) , 0 , )),
	(( u'JoinCondition' , u'pVal' , ), 5, (5, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 56 , (3, 0, None, None) , 0 , )),
]

IAttachment_vtables_dispatch_ = 1
IAttachment_vtables_ = [
	(( u'Type' , u'pVal' , ), 11, (11, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 80 , (3, 0, None, None) , 0 , )),
	(( u'Type' , u'pVal' , ), 11, (11, (), [ (3, 1, None, None) , ], 1 , 4 , 4 , 0 , 84 , (3, 0, None, None) , 0 , )),
	(( u'Name' , u'ViewFormat' , u'pVal' , ), 12, (12, (), [ (3, 49, '0', None) , 
			(16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 88 , (3, 0, None, None) , 0 , )),
	(( u'Name' , u'ViewFormat' , u'pVal' , ), 12, (12, (), [ (3, 49, '0', None) , 
			(16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 88 , (3, 0, None, None) , 0 , )),
	(( u'Description' , u'pVal' , ), 13, (13, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 92 , (3, 0, None, None) , 0 , )),
	(( u'Description' , u'pVal' , ), 13, (13, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 96 , (3, 0, None, None) , 0 , )),
	(( u'Data' , u'pVal' , ), 14, (14, (), [ (16396, 10, None, None) , ], 1 , 2 , 4 , 0 , 100 , (3, 0, None, None) , 0 , )),
	(( u'FileName' , u'pVal' , ), 15, (15, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 104 , (3, 0, None, None) , 0 , )),
	(( u'FileName' , u'pVal' , ), 15, (15, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 108 , (3, 0, None, None) , 0 , )),
	(( u'Load' , u'synchronize' , u'RootPath' , ), 16, (16, (), [ (11, 0, None, None) , 
			(16392, 0, None, None) , ], 1 , 1 , 4 , 0 , 112 , (3, 0, None, None) , 0 , )),
	(( u'Save' , u'synchronize' , ), 17, (17, (), [ (11, 0, None, None) , ], 1 , 1 , 4 , 0 , 116 , (3, 0, None, None) , 0 , )),
	(( u'AttachmentStorage' , u'pVal' , ), 18, (18, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 120 , (3, 0, None, None) , 0 , )),
	(( u'DirectLink' , u'pVal' , ), 19, (19, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 124 , (3, 0, None, None) , 0 , )),
	(( u'ServerFileName' , u'pVal' , ), 20, (20, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 128 , (3, 0, None, None) , 0 , )),
	(( u'FileSize' , u'pVal' , ), 21, (21, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 132 , (3, 0, None, None) , 0 , )),
	(( u'LastModified' , u'pVal' , ), 22, (22, (), [ (16391, 10, None, None) , ], 1 , 2 , 4 , 0 , 136 , (3, 0, None, None) , 0 , )),
	(( u'Rename' , u'NewName' , ), 23, (23, (), [ (8, 1, None, None) , ], 1 , 1 , 4 , 0 , 140 , (3, 0, None, None) , 0 , )),
]

IAttachmentFactory_vtables_dispatch_ = 1
IAttachmentFactory_vtables_ = [
	(( u'AttachmentStorage' , u'pVal' , ), 8, (8, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 64 , (3, 0, None, None) , 0 , )),
	(( u'FactoryProperties' , u'OwnerType' , u'OwnerKey' , ), 9, (9, (), [ (16392, 2, None, None) , 
			(16396, 2, None, None) , ], 1 , 1 , 4 , 0 , 68 , (3, 0, None, None) , 0 , )),
]

IAttachmentVcs_vtables_dispatch_ = 1
IAttachmentVcs_vtables_ = [
]

IAuditProperty_vtables_dispatch_ = 1
IAuditProperty_vtables_ = [
	(( u'TableName' , u'pVal' , ), 11, (11, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 80 , (3, 0, None, None) , 0 , )),
	(( u'FieldName' , u'pVal' , ), 12, (12, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 84 , (3, 0, None, None) , 0 , )),
	(( u'PropertyName' , u'pVal' , ), 13, (13, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 88 , (3, 0, None, None) , 0 , )),
	(( u'OldValue' , u'pVal' , ), 14, (14, (), [ (16396, 10, None, None) , ], 1 , 2 , 4 , 0 , 92 , (3, 0, None, None) , 0 , )),
	(( u'NewValue' , u'pVal' , ), 15, (15, (), [ (16396, 10, None, None) , ], 1 , 2 , 4 , 0 , 96 , (3, 0, None, None) , 0 , )),
	(( u'ActionID' , u'pVal' , ), 16, (16, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 100 , (3, 0, None, None) , 0 , )),
	(( u'Action' , u'pVal' , ), 17, (17, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 104 , (3, 0, None, None) , 0 , )),
	(( u'UserName' , u'pVal' , ), 18, (18, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 108 , (3, 0, None, None) , 0 , )),
	(( u'SessionID' , u'pVal' , ), 19, (19, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 112 , (3, 0, None, None) , 0 , )),
	(( u'Time' , u'pVal' , ), 20, (20, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 116 , (3, 0, None, None) , 0 , )),
	(( u'EntityType' , u'pVal' , ), 21, (21, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 120 , (3, 0, None, None) , 0 , )),
	(( u'EntityID' , u'pVal' , ), 22, (22, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 124 , (3, 0, None, None) , 0 , )),
]

IAuditPropertyFactory_vtables_dispatch_ = 1
IAuditPropertyFactory_vtables_ = [
]

IAuditRecord_vtables_dispatch_ = 1
IAuditRecord_vtables_ = [
	(( u'AuditPropertyFactory' , u'pVal' , ), 11, (11, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 80 , (3, 0, None, None) , 0 , )),
	(( u'Action' , u'pVal' , ), 12, (12, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 84 , (3, 0, None, None) , 0 , )),
	(( u'EntityType' , u'pVal' , ), 13, (13, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 88 , (3, 0, None, None) , 0 , )),
	(( u'EntityID' , u'pVal' , ), 14, (14, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 92 , (3, 0, None, None) , 0 , )),
	(( u'UserName' , u'pVal' , ), 15, (15, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 96 , (3, 0, None, None) , 0 , )),
	(( u'SessionID' , u'pVal' , ), 16, (16, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 100 , (3, 0, None, None) , 0 , )),
	(( u'Time' , u'pVal' , ), 17, (17, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 104 , (3, 0, None, None) , 0 , )),
	(( u'ContextID' , u'pVal' , ), 18, (18, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 108 , (3, 0, None, None) , 64 , )),
	(( u'Description' , u'pVal' , ), 19, (19, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 112 , (3, 0, None, None) , 0 , )),
	(( u'ActionID' , u'pVal' , ), 20, (20, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 116 , (3, 0, None, None) , 0 , )),
]

IAuditRecordFactory_vtables_dispatch_ = 1
IAuditRecordFactory_vtables_ = [
]

IAuditable_vtables_dispatch_ = 1
IAuditable_vtables_ = [
	(( u'AuditRecordFactory' , u'pVal' , ), 1, (1, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
	(( u'AuditPropertyFactory' , u'pVal' , ), 2, (2, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 32 , (3, 0, None, None) , 0 , )),
]

IBPComponent_vtables_dispatch_ = 1
IBPComponent_vtables_ = [
	(( u'Component' , u'pVal' , ), 14, (14, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 92 , (3, 0, None, None) , 0 , )),
	(( u'Order' , u'plVal' , ), 15, (15, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 96 , (3, 0, None, None) , 0 , )),
	(( u'Order' , u'plVal' , ), 15, (15, (), [ (3, 1, None, None) , ], 1 , 4 , 4 , 0 , 100 , (3, 0, None, None) , 0 , )),
	(( u'Test' , u'pVal' , ), 16, (16, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 104 , (3, 0, None, None) , 0 , )),
	(( u'FailureCondition' , u'pVal' , ), 17, (17, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 108 , (3, 0, None, None) , 0 , )),
	(( u'FailureCondition' , u'pVal' , ), 17, (17, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 112 , (3, 0, None, None) , 0 , )),
	(( u'Name' , u'pVal' , ), 18, (18, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 116 , (3, 0, None, None) , 0 , )),
	(( u'Group' , u'pVal' , ), 19, (19, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 120 , (3, 0, None, None) , 0 , )),
	(( u'GroupID' , ), 20, (20, (), [ (3, 0, None, None) , ], 1 , 4 , 4 , 0 , 124 , (3, 0, None, None) , 65 , )),
	(( u'BPParams' , u'pVal' , ), 21, (21, (), [ (16393, 10, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , ], 1 , 2 , 4 , 0 , 128 , (3, 0, None, None) , 0 , )),
	(( u'Iterations' , u'pVal' , ), 22, (22, (), [ (16393, 10, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , ], 1 , 2 , 4 , 0 , 132 , (3, 0, None, None) , 0 , )),
	(( u'AddIteration' , u'pVal' , ), 23, (23, (), [ (16393, 10, None, None) , ], 1 , 1 , 4 , 0 , 136 , (3, 0, None, None) , 0 , )),
	(( u'DeleteIteration' , u'pVal' , ), 24, (24, (), [ (9, 1, None, None) , ], 1 , 1 , 4 , 0 , 140 , (3, 0, None, None) , 0 , )),
	(( u'_AddBPParameter' , u'pComponentParam' , u'ppBPParam' , ), 25, (25, (), [ (9, 1, None, None) , 
			(16393, 10, None, None) , ], 1 , 1 , 4 , 0 , 144 , (3, 0, None, None) , 65 , )),
]

IBPGroup_vtables_dispatch_ = 1
IBPGroup_vtables_ = [
	(( u'BPComponents' , u'pBPComps' , ), 1, (1, (), [ (16393, 10, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , ], 1 , 1 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
	(( u'AddBPComponent' , u'pBPComponent' , ), 2, (2, (), [ (9, 1, None, None) , ], 1 , 1 , 4 , 0 , 32 , (3, 0, None, None) , 0 , )),
	(( u'DeleteBPComponent' , u'pBPComponent' , ), 3, (3, (), [ (9, 1, None, None) , ], 1 , 1 , 4 , 0 , 36 , (3, 0, None, None) , 0 , )),
	(( u'ID' , u'pVal' , ), 4, (4, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 40 , (3, 0, None, None) , 0 , )),
]

IBPHistoryRecord_vtables_dispatch_ = 1
IBPHistoryRecord_vtables_ = [
	(( u'Description' , u'pVal' , ), 1, (1, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
	(( u'Time' , u'pVal' , ), 2, (2, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 32 , (3, 0, None, None) , 0 , )),
	(( u'Changer' , u'pVal' , ), 3, (3, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 36 , (3, 0, None, None) , 0 , )),
	(( u'ToXmlString' , u'xmlString' , ), 4, (4, (), [ (16392, 10, None, None) , ], 1 , 1 , 4 , 0 , 40 , (3, 0, None, None) , 65 , )),
]

IBPIteration_vtables_dispatch_ = 1
IBPIteration_vtables_ = [
	(( u'BPComponent' , u'pVal' , ), 14, (14, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 80 , (3, 0, None, None) , 0 , )),
	(( u'IterationParams' , u'pVal' , ), 15, (15, (), [ (16393, 10, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , ], 1 , 2 , 4 , 0 , 84 , (3, 0, None, None) , 0 , )),
	(( u'AddParam' , u'pVal' , u'ppVal' , ), 16, (16, (), [ (9, 1, None, None) , 
			(16393, 10, None, None) , ], 1 , 1 , 4 , 0 , 88 , (3, 0, None, None) , 0 , )),
	(( u'DeleteParam' , u'pVal' , ), 17, (17, (), [ (9, 1, None, None) , ], 1 , 1 , 4 , 0 , 92 , (3, 0, None, None) , 0 , )),
	(( u'DeleteIterationParams' , ), 18, (18, (), [ ], 1 , 1 , 4 , 0 , 96 , (3, 0, None, None) , 0 , )),
	(( u'Order' , u'pVal' , ), 19, (19, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 100 , (3, 0, None, None) , 0 , )),
	(( u'Order' , u'pVal' , ), 19, (19, (), [ (3, 1, None, None) , ], 1 , 4 , 4 , 0 , 104 , (3, 0, None, None) , 0 , )),
]

IBPIterationParam_vtables_dispatch_ = 1
IBPIterationParam_vtables_ = [
	(( u'BPParameter' , u'pVal' , ), 20, (20, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 80 , (3, 0, None, None) , 0 , )),
	(( u'Value' , u'pVal' , ), 21, (21, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 84 , (3, 0, None, None) , 0 , )),
	(( u'Value' , u'pVal' , ), 21, (21, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 88 , (3, 0, None, None) , 0 , )),
]

IBPParam_vtables_dispatch_ = 1
IBPParam_vtables_ = [
	(( u'Reference' , u'pVal' , ), 20, (20, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 80 , (3, 0, None, None) , 0 , )),
	(( u'Reference' , u'pVal' , ), 20, (20, (), [ (9, 1, None, None) , ], 1 , 4 , 4 , 0 , 84 , (3, 0, None, None) , 0 , )),
	(( u'Type' , u'pnType' , ), 21, (21, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 88 , (3, 0, None, None) , 0 , )),
	(( u'ComponentParam' , u'pVal' , ), 22, (22, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 92 , (3, 0, None, None) , 0 , )),
	(( u'ComponentParamName' , u'pVal' , ), 23, (23, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 96 , (3, 0, None, None) , 0 , )),
	(( u'ComponentParamIsOut' , u'pVal' , ), 26, (26, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 100 , (3, 0, None, None) , 0 , )),
	(( u'BPComponent' , u'pVal' , ), 24, (24, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 104 , (3, 0, None, None) , 0 , )),
	(( u'ComponentParamOrder' , u'pVal' , ), 25, (25, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 108 , (3, 0, None, None) , 0 , )),
]

IBPStepParam_vtables_dispatch_ = 1
IBPStepParam_vtables_ = [
	(( u'Name' , u'pVal' , ), 14, (14, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 80 , (3, 0, None, None) , 0 , )),
	(( u'Name' , u'pVal' , ), 14, (14, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 84 , (3, 0, None, None) , 0 , )),
	(( u'Value' , u'pVal' , ), 15, (15, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 88 , (3, 0, None, None) , 0 , )),
	(( u'Value' , u'pVal' , ), 15, (15, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 92 , (3, 0, None, None) , 0 , )),
	(( u'Type' , u'pVal' , ), 16, (16, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 96 , (3, 0, None, None) , 0 , )),
	(( u'Type' , u'pVal' , ), 16, (16, (), [ (3, 1, None, None) , ], 1 , 4 , 4 , 0 , 100 , (3, 0, None, None) , 0 , )),
	(( u'ReferencedParamID' , u'pVal' , ), 17, (17, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 104 , (3, 0, None, None) , 0 , )),
	(( u'ReferencedParamID' , u'pVal' , ), 17, (17, (), [ (3, 1, None, None) , ], 1 , 4 , 4 , 0 , 108 , (3, 0, None, None) , 0 , )),
	(( u'StepID' , u'pVal' , ), 18, (18, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 112 , (3, 0, None, None) , 0 , )),
]

IBaseFactory_vtables_dispatch_ = 1
IBaseFactory_vtables_ = [
	(( u'Item' , u'ItemKey' , u'pItem' , ), 0, (0, (), [ (12, 0, None, None) , 
			(16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
	(( u'NewList' , u'Filter' , u'pList' , ), 1, (1, (), [ (8, 0, None, None) , 
			(16393, 10, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , ], 1 , 1 , 4 , 0 , 32 , (3, 0, None, None) , 0 , )),
	(( u'Fields' , u'pFields' , ), 2, (2, (), [ (16393, 10, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , ], 1 , 2 , 4 , 0 , 36 , (3, 0, None, None) , 0 , )),
	(( u'AddItem' , u'ItemData' , u'pItem' , ), 3, (3, (), [ (12, 0, None, None) , 
			(16393, 10, None, None) , ], 1 , 1 , 4 , 0 , 40 , (3, 0, None, None) , 0 , )),
	(( u'RemoveItem' , u'ItemKey' , ), 4, (4, (), [ (12, 0, None, None) , ], 1 , 1 , 4 , 0 , 44 , (3, 0, None, None) , 0 , )),
	(( u'Filter' , u'pFilter' , ), 5, (5, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 48 , (3, 0, None, None) , 0 , )),
	(( u'History' , u'pHistory' , ), 6, (6, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 52 , (3, 0, None, None) , 0 , )),
	(( u'FetchLevel' , u'FieldName' , u'pVal' , ), 7, (7, (), [ (8, 1, None, None) , 
			(16386, 10, None, None) , ], 1 , 2 , 4 , 0 , 56 , (3, 0, None, None) , 0 , )),
	(( u'FetchLevel' , u'FieldName' , u'pVal' , ), 7, (7, (), [ (8, 1, None, None) , 
			(2, 1, None, None) , ], 1 , 4 , 4 , 0 , 60 , (3, 0, None, None) , 0 , )),
]

IBaseFactory2_vtables_dispatch_ = 1
IBaseFactory2_vtables_ = [
	(( u'GroupingManager' , u'pGroupingManager' , ), 1, (1, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
	(( u'GroupingSupported' , u'pbResult' , ), 2, (2, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 32 , (3, 0, None, None) , 0 , )),
]

IBaseFactoryEx_vtables_dispatch_ = 1
IBaseFactoryEx_vtables_ = [
	(( u'Mail' , u'Items' , u'SendTo' , u'SendCc' , u'Option' , 
			u'Subject' , u'Comment' , ), 8, (8, (), [ (12, 1, None, None) , (8, 1, None, None) , 
			(8, 49, "u''", None) , (3, 49, '0', None) , (8, 49, "u''", None) , (8, 49, "u''", None) , ], 1 , 1 , 4 , 0 , 64 , (3, 32, None, None) , 0 , )),
]

IBaseField_vtables_dispatch_ = 1
IBaseField_vtables_ = [
	(( u'Field' , u'FieldName' , u'pVal' , ), 0, (0, (), [ (8, 0, None, None) , 
			(16396, 10, None, None) , ], 1 , 2 , 4 , 0 , 40 , (3, 0, None, None) , 0 , )),
	(( u'Field' , u'FieldName' , u'pVal' , ), 0, (0, (), [ (8, 0, None, None) , 
			(12, 1, None, None) , ], 1 , 4 , 4 , 0 , 44 , (3, 0, None, None) , 0 , )),
	(( u'ID' , u'pVal' , ), 4, (4, (), [ (16396, 10, None, None) , ], 1 , 2 , 4 , 0 , 48 , (3, 0, None, None) , 0 , )),
	(( u'AutoPost' , u'pVal' , ), 5, (5, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 52 , (3, 0, None, None) , 0 , )),
	(( u'AutoPost' , u'pVal' , ), 5, (5, (), [ (11, 1, None, None) , ], 1 , 4 , 4 , 0 , 56 , (3, 0, None, None) , 0 , )),
	(( u'Post' , ), 6, (6, (), [ ], 1 , 1 , 4 , 0 , 60 , (3, 0, None, None) , 0 , )),
	(( u'Refresh' , ), 7, (7, (), [ ], 1 , 1 , 4 , 0 , 64 , (3, 0, None, None) , 0 , )),
	(( u'Undo' , ), 8, (8, (), [ ], 1 , 1 , 4 , 0 , 68 , (3, 0, None, None) , 0 , )),
	(( u'Modified' , u'pVal' , ), 9, (9, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 72 , (3, 0, None, None) , 0 , )),
	(( u'Virtual' , u'pVal' , ), 10, (10, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 76 , (3, 0, None, None) , 0 , )),
]

IBaseField2_vtables_dispatch_ = 1
IBaseField2_vtables_ = [
	(( u'AutoUnlock' , u'pVal' , ), 1, (1, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
	(( u'AutoUnlock' , u'pVal' , ), 1, (1, (), [ (11, 1, None, None) , ], 1 , 4 , 4 , 0 , 32 , (3, 0, None, None) , 0 , )),
	(( u'FieldMultiValue' , u'FieldName' , u'pMultiValue' , ), 2, (2, (), [ (8, 0, None, None) , 
			(16393, 10, None, "IID('{EB180CC0-6FDE-4A1E-A68E-F106EEED5E15}')") , ], 1 , 2 , 4 , 0 , 36 , (3, 0, None, None) , 0 , )),
	(( u'FieldMultiValue' , u'FieldName' , u'pMultiValue' , ), 2, (2, (), [ (8, 0, None, None) , 
			(9, 1, None, "IID('{EB180CC0-6FDE-4A1E-A68E-F106EEED5E15}')") , ], 1 , 4 , 4 , 0 , 40 , (3, 0, None, None) , 0 , )),
]

IBaseFieldEx_vtables_dispatch_ = 1
IBaseFieldEx_vtables_ = [
	(( u'History' , u'pHistory' , ), 11, (11, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 80 , (3, 0, None, None) , 0 , )),
	(( u'Attachments' , u'pVal' , ), 12, (12, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 84 , (3, 0, None, None) , 0 , )),
	(( u'HasAttachment' , u'pVal' , ), 13, (13, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 88 , (3, 0, None, None) , 0 , )),
]

IBaseFieldExMail_vtables_dispatch_ = 1
IBaseFieldExMail_vtables_ = [
	(( u'Mail' , u'SendTo' , u'SendCc' , u'Option' , u'Subject' , 
			u'Comment' , ), 14, (14, (), [ (8, 1, None, None) , (8, 49, "u''", None) , (3, 49, '0', None) , 
			(8, 49, "u''", None) , (8, 49, "u''", None) , ], 1 , 1 , 4 , 0 , 92 , (3, 32, None, None) , 0 , )),
]

IBug_vtables_dispatch_ = 1
IBug_vtables_ = [
	(( u'Status' , u'pVal' , ), 15, (15, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 96 , (3, 0, None, None) , 0 , )),
	(( u'Status' , u'pVal' , ), 15, (15, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 100 , (3, 0, None, None) , 0 , )),
	(( u'Project' , u'pVal' , ), 16, (16, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 104 , (3, 0, None, None) , 0 , )),
	(( u'Project' , u'pVal' , ), 16, (16, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 108 , (3, 0, None, None) , 0 , )),
	(( u'Summary' , u'pVal' , ), 17, (17, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 112 , (3, 0, None, None) , 0 , )),
	(( u'Summary' , u'pVal' , ), 17, (17, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 116 , (3, 0, None, None) , 0 , )),
	(( u'Priority' , u'pVal' , ), 18, (18, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 120 , (3, 0, None, None) , 0 , )),
	(( u'Priority' , u'pVal' , ), 18, (18, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 124 , (3, 0, None, None) , 0 , )),
	(( u'DetectedBy' , u'pVal' , ), 19, (19, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 128 , (3, 0, None, None) , 0 , )),
	(( u'DetectedBy' , u'pVal' , ), 19, (19, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 132 , (3, 0, None, None) , 0 , )),
	(( u'AssignedTo' , u'pVal' , ), 20, (20, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 136 , (3, 0, None, None) , 0 , )),
	(( u'AssignedTo' , u'pVal' , ), 20, (20, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 140 , (3, 0, None, None) , 0 , )),
	(( u'FindSimilarBugs' , u'SimilarityRatio' , u'pList' , ), 21, (21, (), [ (3, 49, '10', None) , 
			(16393, 10, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , ], 1 , 1 , 4 , 0 , 144 , (3, 0, None, None) , 0 , )),
	(( u'HasChange' , u'pVal' , ), 22, (22, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 148 , (3, 0, None, None) , 64 , )),
	(( u'ChangeLinks' , u'pVal' , ), 23, (23, (), [ (16393, 10, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , ], 1 , 2 , 4 , 0 , 152 , (3, 0, None, None) , 64 , )),
]

IBugFactory_vtables_dispatch_ = 1
IBugFactory_vtables_ = [
	(( u'BuildSummaryGraph' , u'XAxisField' , u'GroupByField' , u'SumOfField' , u'MaxCols' , 
			u'Filter' , u'ForceRefresh' , u'ShowFullPath' , u'pGraph' , ), 9, (9, (), [ 
			(8, 49, "u''", None) , (8, 49, "u''", None) , (8, 49, "u''", None) , (3, 49, '0', None) , (12, 17, None, None) , 
			(11, 49, 'False', None) , (11, 49, 'False', None) , (16393, 10, None, None) , ], 1 , 1 , 4 , 0 , 68 , (3, 32, None, None) , 0 , )),
	(( u'BuildAgeGraph' , u'GroupByField' , u'SumOfField' , u'MaxAge' , u'MaxCols' , 
			u'Filter' , u'ForceRefresh' , u'ShowFullPath' , u'pGraph' , ), 10, (10, (), [ 
			(8, 49, "u''", None) , (8, 49, "u''", None) , (3, 49, '0', None) , (3, 49, '0', None) , (12, 17, None, None) , 
			(11, 49, 'False', None) , (11, 49, 'False', None) , (16393, 10, None, None) , ], 1 , 1 , 4 , 0 , 72 , (3, 32, None, None) , 0 , )),
	(( u'BuildProgressGraph' , u'GroupByField' , u'SumOfField' , u'ByHistory' , u'MajorSkip' , 
			u'MinorSkip' , u'MaxCols' , u'Filter' , u'FRDate' , u'ForceRefresh' , 
			u'ShowFullPath' , u'pGraph' , ), 11, (11, (), [ (8, 49, "u''", None) , (8, 49, "u''", None) , 
			(11, 49, 'True', None) , (3, 49, '0', None) , (3, 49, '1', None) , (3, 49, '0', None) , (12, 17, None, None) , 
			(12, 17, None, None) , (11, 49, 'False', None) , (11, 49, 'False', None) , (16393, 10, None, None) , ], 1 , 1 , 4 , 0 , 76 , (3, 32, None, None) , 0 , )),
	(( u'FindSimilarBugs' , u'Pattern' , u'SimilarityRatio' , u'pList' , ), 12, (12, (), [ 
			(8, 0, None, None) , (3, 49, '10', None) , (16393, 10, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , ], 1 , 1 , 4 , 0 , 80 , (3, 0, None, None) , 0 , )),
	(( u'BuildTrendGraph' , u'GroupByField' , u'SumOfField' , u'MaxCols' , u'Filter' , 
			u'FRDate' , u'ForceRefresh' , u'ShowFullPath' , u'pGraph' , ), 13, (13, (), [ 
			(8, 49, "u''", None) , (8, 49, "u''", None) , (3, 49, '0', None) , (12, 17, None, None) , (12, 17, None, None) , 
			(11, 49, 'False', None) , (11, 49, 'False', None) , (16393, 10, None, None) , ], 1 , 1 , 4 , 0 , 84 , (3, 32, None, None) , 0 , )),
	(( u'BuildPerfGraph' , u'GroupByField' , u'SumOfField' , u'MaxCols' , u'Filter' , 
			u'FRDate' , u'ForceRefresh' , u'ShowFullPath' , u'pGraph' , ), 14, (14, (), [ 
			(8, 49, "u''", None) , (8, 49, "u''", None) , (3, 49, '0', None) , (12, 17, None, None) , (12, 17, None, None) , 
			(11, 49, 'False', None) , (11, 49, 'False', None) , (16393, 10, None, None) , ], 1 , 1 , 4 , 0 , 88 , (3, 32, None, None) , 64 , )),
]

IBusinessProcess_vtables_dispatch_ = 1
IBusinessProcess_vtables_ = [
	(( u'Load' , ), 100, (100, (), [ ], 1 , 1 , 4 , 0 , 192 , (3, 0, None, None) , 0 , )),
	(( u'Save' , ), 101, (101, (), [ ], 1 , 1 , 4 , 0 , 196 , (3, 0, None, None) , 0 , )),
	(( u'RTParameters' , u'pList' , ), 102, (102, (), [ (16393, 10, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , ], 1 , 2 , 4 , 0 , 200 , (3, 0, None, None) , 0 , )),
	(( u'AddRTParam' , u'pRTParam' , ), 103, (103, (), [ (16393, 10, None, None) , ], 1 , 1 , 4 , 0 , 204 , (3, 0, None, None) , 0 , )),
	(( u'DeleteRTParam' , u'pRTParam' , ), 104, (104, (), [ (9, 1, None, None) , ], 1 , 1 , 4 , 0 , 208 , (3, 0, None, None) , 0 , )),
	(( u'BPComponents' , u'pList' , ), 105, (105, (), [ (16393, 10, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , ], 1 , 2 , 4 , 0 , 212 , (3, 0, None, None) , 0 , )),
	(( u'AddBPComponent' , u'pComponent' , u'pBPComponent' , ), 106, (106, (), [ (9, 1, None, None) , 
			(16393, 10, None, None) , ], 1 , 1 , 4 , 0 , 216 , (3, 0, None, None) , 0 , )),
	(( u'DeleteBPComponent' , u'pBPComponent' , ), 107, (107, (), [ (9, 1, None, None) , ], 1 , 1 , 4 , 0 , 220 , (3, 0, None, None) , 0 , )),
	(( u'AddGroup' , u'ppBPGroup' , ), 108, (108, (), [ (16393, 10, None, None) , ], 1 , 1 , 4 , 0 , 224 , (3, 0, None, None) , 0 , )),
	(( u'DeleteGroup' , u'pBPGroup' , ), 109, (109, (), [ (9, 1, None, None) , ], 1 , 1 , 4 , 0 , 228 , (3, 0, None, None) , 0 , )),
	(( u'GroupByID' , u'nGroupID' , u'Val' , ), 110, (110, (), [ (3, 1, None, None) , 
			(16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 232 , (3, 0, None, None) , 0 , )),
	(( u'DownloadPictures' , ), 111, (111, (), [ ], 1 , 1 , 4 , 0 , 236 , (3, 0, None, None) , 0 , )),
	(( u'DownloadPicturesProgress' , u'Total' , u'Current' , ), 112, (112, (), [ (16387, 2, None, None) , 
			(16387, 2, None, None) , ], 1 , 1 , 4 , 0 , 240 , (3, 0, None, None) , 0 , )),
	(( u'CancelDownloadPictures' , ), 113, (113, (), [ ], 1 , 1 , 4 , 0 , 244 , (3, 0, None, None) , 0 , )),
	(( u'HtmlScript' , u'pVal' , ), 114, (114, (), [ (16392, 2, None, None) , ], 1 , 2 , 4 , 0 , 248 , (3, 0, None, None) , 0 , )),
	(( u'BPTHistory' , u'pVal' , ), 115, (115, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 252 , (3, 0, None, None) , 0 , )),
	(( u'BPComponentByID' , u'nCompID' , u'pVal' , ), 116, (116, (), [ (3, 1, None, None) , 
			(16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 256 , (3, 0, None, None) , 0 , )),
]

ICacheMgr_vtables_dispatch_ = 1
ICacheMgr_vtables_ = [
	(( u'Run' , ), 1, (1, (), [ ], 1 , 1 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
	(( u'IsRunning' , u'pVal' , ), 2, (2, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 32 , (3, 0, None, None) , 0 , )),
	(( u'SetFileTime' , u'bsFilePath' , ), 3, (3, (), [ (8, 1, None, None) , ], 1 , 1 , 4 , 0 , 36 , (3, 0, None, None) , 0 , )),
	(( u'SetUpdateRegistry' , u'bAllowUpdate' , ), 4, (4, (), [ (3, 1, None, None) , ], 1 , 1 , 4 , 0 , 40 , (3, 0, None, None) , 0 , )),
	(( u'SetCurrentTest' , u'bsTestPath' , ), 5, (5, (), [ (8, 1, None, None) , ], 1 , 1 , 4 , 0 , 44 , (3, 0, None, None) , 0 , )),
]

IChange_vtables_dispatch_ = 1
IChange_vtables_ = [
	(( u'Description' , u'pVal' , ), 14, (14, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 92 , (3, 0, None, None) , 0 , )),
	(( u'Description' , u'pVal' , ), 14, (14, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 96 , (3, 0, None, None) , 0 , )),
	(( u'Comments' , u'pVal' , ), 15, (15, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 100 , (3, 0, None, None) , 0 , )),
	(( u'Comments' , u'pVal' , ), 15, (15, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 104 , (3, 0, None, None) , 0 , )),
	(( u'CreatedBy' , u'pVal' , ), 16, (16, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 108 , (3, 0, None, None) , 0 , )),
	(( u'CreationDate' , u'pVal' , ), 17, (17, (), [ (16391, 10, None, None) , ], 1 , 2 , 4 , 0 , 112 , (3, 0, None, None) , 0 , )),
	(( u'ClosingDate' , u'pVal' , ), 18, (18, (), [ (16391, 10, None, None) , ], 1 , 2 , 4 , 0 , 116 , (3, 0, None, None) , 0 , )),
	(( u'ClosingDate' , u'pVal' , ), 18, (18, (), [ (7, 1, None, None) , ], 1 , 4 , 4 , 0 , 120 , (3, 0, None, None) , 0 , )),
	(( u'VOB_Labels' , u'pVal' , ), 19, (19, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 124 , (3, 0, None, None) , 0 , )),
	(( u'VOB_Labels' , u'pVal' , ), 19, (19, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 128 , (3, 0, None, None) , 0 , )),
	(( u'ChangeEntryFactory' , u'pVal' , ), 20, (20, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 132 , (3, 0, None, None) , 0 , )),
	(( u'AddBugLink' , u'newVal' , ), 21, (21, (), [ (3, 1, None, None) , ], 1 , 1 , 4 , 0 , 136 , (3, 0, None, None) , 0 , )),
	(( u'RemoveBugLink' , u'newVal' , ), 22, (22, (), [ (3, 1, None, None) , ], 1 , 1 , 4 , 0 , 140 , (3, 0, None, None) , 0 , )),
	(( u'GetBugLinks' , u'pVal' , ), 23, (23, (), [ (16393, 2, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , ], 1 , 1 , 4 , 0 , 144 , (3, 0, None, None) , 0 , )),
	(( u'AutoGetLinks' , u'pVal' , ), 24, (24, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 148 , (3, 0, None, None) , 0 , )),
	(( u'AutoGetLinks' , u'pVal' , ), 24, (24, (), [ (11, 1, None, None) , ], 1 , 4 , 4 , 0 , 152 , (3, 0, None, None) , 0 , )),
	(( u'Links' , u'pVal' , ), 25, (25, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 156 , (3, 0, None, None) , 0 , )),
]

IChangeEntry_vtables_dispatch_ = 1
IChangeEntry_vtables_ = [
	(( u'FileName' , u'pVal' , ), 14, (14, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 92 , (3, 0, None, None) , 0 , )),
	(( u'FilePath' , u'pVal' , ), 15, (15, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 96 , (3, 0, None, None) , 0 , )),
	(( u'StartVersion' , u'pVal' , ), 16, (16, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 100 , (3, 0, None, None) , 0 , )),
	(( u'WorkingVersion' , u'pVal' , ), 17, (17, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 104 , (3, 0, None, None) , 0 , )),
	(( u'WorkingVersion' , u'pVal' , ), 17, (17, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 108 , (3, 0, None, None) , 0 , )),
	(( u'EndVersion' , u'pVal' , ), 18, (18, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 112 , (3, 0, None, None) , 0 , )),
	(( u'EndVersion' , u'pVal' , ), 18, (18, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 116 , (3, 0, None, None) , 0 , )),
	(( u'CreatedBy' , u'pVal' , ), 19, (19, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 120 , (3, 0, None, None) , 0 , )),
	(( u'CreationDate' , u'pVal' , ), 20, (20, (), [ (16391, 10, None, None) , ], 1 , 2 , 4 , 0 , 124 , (3, 0, None, None) , 0 , )),
]

IColumnInfo_vtables_dispatch_ = 1
IColumnInfo_vtables_ = [
	(( u'ColCount' , u'pVal' , ), 1, (1, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
	(( u'ColSize' , u'Index' , u'pVal' , ), 2, (2, (), [ (3, 0, None, None) , 
			(16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 32 , (3, 0, None, None) , 0 , )),
	(( u'ColType' , u'Index' , u'pVal' , ), 3, (3, (), [ (3, 0, None, None) , 
			(16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 36 , (3, 0, None, None) , 0 , )),
	(( u'ColName' , u'Index' , u'pVal' , ), 4, (4, (), [ (3, 0, None, None) , 
			(16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 40 , (3, 0, None, None) , 0 , )),
	(( u'ColIndex' , u'Name' , u'pVal' , ), 5, (5, (), [ (8, 0, None, None) , 
			(16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 44 , (3, 0, None, None) , 0 , )),
]

IComFrec_vtables_dispatch_ = 1
IComFrec_vtables_ = [
	(( u'Open' , u'NewData' , ), 1, (1, (), [ (8, 0, None, None) , ], 1 , 1 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
	(( u'WriteRecord' , ), 2, (2, (), [ ], 1 , 1 , 4 , 0 , 32 , (3, 0, None, None) , 0 , )),
	(( u'ReadRecord' , u'pVal' , ), 3, (3, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 36 , (3, 0, None, None) , 0 , )),
	(( u'RecordString' , u'pVal' , ), 4, (4, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 40 , (3, 0, None, None) , 0 , )),
	(( u'Buffer' , u'pVal' , ), 5, (5, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 44 , (3, 0, None, None) , 0 , )),
	(( u'Value' , u'AtributeName' , u'pVal' , ), 6, (6, (), [ (8, 0, None, None) , 
			(16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 48 , (3, 0, None, None) , 0 , )),
	(( u'Value' , u'AtributeName' , u'pVal' , ), 6, (6, (), [ (8, 0, None, None) , 
			(8, 1, None, None) , ], 1 , 4 , 4 , 0 , 52 , (3, 0, None, None) , 0 , )),
	(( u'AddItem' , u'AttrName' , u'Value' , ), 7, (7, (), [ (8, 0, None, None) , 
			(8, 0, None, None) , ], 1 , 1 , 4 , 0 , 56 , (3, 0, None, None) , 0 , )),
	(( u'ValuePos' , u'Pos' , u'pVal' , ), 8, (8, (), [ (3, 0, None, None) , 
			(16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 60 , (3, 0, None, None) , 0 , )),
	(( u'ValuePos' , u'Pos' , u'pVal' , ), 8, (8, (), [ (3, 0, None, None) , 
			(8, 1, None, None) , ], 1 , 4 , 4 , 0 , 64 , (3, 0, None, None) , 0 , )),
	(( u'IsAttribute' , u'AttributeName' , u'pVal' , ), 9, (9, (), [ (8, 0, None, None) , 
			(16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 68 , (3, 0, None, None) , 0 , )),
	(( u'NumberOfAttributes' , u'pVal' , ), 10, (10, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 72 , (3, 0, None, None) , 0 , )),
	(( u'AttributeName' , u'Position' , u'pVal' , ), 11, (11, (), [ (3, 0, None, None) , 
			(16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 76 , (3, 0, None, None) , 0 , )),
	(( u'AttributeName' , u'Position' , u'pVal' , ), 11, (11, (), [ (3, 0, None, None) , 
			(8, 1, None, None) , ], 1 , 4 , 4 , 0 , 80 , (3, 0, None, None) , 0 , )),
	(( u'AttributePosition' , u'AttributeName' , u'pVal' , ), 12, (12, (), [ (8, 0, None, None) , 
			(16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 84 , (3, 0, None, None) , 0 , )),
	(( u'RemoveItem' , u'AttributeName' , ), 13, (13, (), [ (8, 0, None, None) , ], 1 , 1 , 4 , 0 , 88 , (3, 0, None, None) , 0 , )),
	(( u'RemoveItemPos' , u'ItemPos' , ), 14, (14, (), [ (3, 0, None, None) , ], 1 , 1 , 4 , 0 , 92 , (3, 0, None, None) , 0 , )),
	(( u'ClearItemList' , ), 15, (15, (), [ ], 1 , 1 , 4 , 0 , 96 , (3, 0, None, None) , 0 , )),
]

ICommand_vtables_dispatch_ = 1
ICommand_vtables_ = [
	(( u'CommandText' , u'pVal' , ), 0, (0, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 64 , (3, 0, None, None) , 0 , )),
	(( u'CommandText' , u'pVal' , ), 0, (0, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 68 , (3, 0, None, None) , 0 , )),
	(( u'Execute' , u'pRecordset' , ), 9, (9, (), [ (16393, 10, None, None) , ], 1 , 1 , 4 , 0 , 72 , (3, 0, None, None) , 0 , )),
	(( u'IndexFields' , u'pVal' , ), 10, (10, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 76 , (3, 0, None, None) , 0 , )),
	(( u'IndexFields' , u'pVal' , ), 10, (10, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 80 , (3, 0, None, None) , 0 , )),
	(( u'AffectedRows' , u'pVal' , ), 11, (11, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 84 , (3, 0, None, None) , 0 , )),
]

IComponent_vtables_dispatch_ = 1
IComponent_vtables_ = [
	(( u'ComponentParamFactory' , u'pVal' , ), 14, (14, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 92 , (3, 0, None, None) , 0 , )),
	(( u'Name' , u'pVal' , ), 18, (18, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 96 , (3, 0, None, None) , 0 , )),
	(( u'Name' , u'pVal' , ), 18, (18, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 100 , (3, 0, None, None) , 0 , )),
	(( u'BusinessProcesses' , u'BPList' , ), 19, (19, (), [ (16393, 10, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , ], 1 , 1 , 4 , 0 , 104 , (3, 0, None, None) , 0 , )),
	(( u'IsIteratable' , u'pVal' , ), 20, (20, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 108 , (3, 0, None, None) , 0 , )),
	(( u'IsIteratable' , u'pVal' , ), 20, (20, (), [ (11, 1, None, None) , ], 1 , 4 , 4 , 0 , 112 , (3, 0, None, None) , 0 , )),
	(( u'IsObsolete' , u'pIsObsolete' , ), 21, (21, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 116 , (3, 0, None, None) , 0 , )),
	(( u'Folder' , u'ppFolder' , ), 22, (22, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 120 , (3, 0, None, None) , 0 , )),
	(( u'HasScript' , u'pvbVal' , ), 23, (23, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 124 , (3, 0, None, None) , 0 , )),
	(( u'HasPicture' , u'pvbVal' , ), 24, (24, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 128 , (3, 0, None, None) , 0 , )),
	(( u'HasPicture' , u'pvbVal' , ), 24, (24, (), [ (11, 1, None, None) , ], 1 , 4 , 4 , 0 , 132 , (3, 0, None, None) , 0 , )),
	(( u'ExtendedStorage' , u'nEntityType' , u'ppVal' , ), 25, (25, (), [ (3, 1, None, None) , 
			(16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 136 , (3, 0, None, None) , 0 , )),
	(( u'ScriptType' , u'pbsVal' , ), 26, (26, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 140 , (3, 0, None, None) , 0 , )),
	(( u'ScriptType' , u'pbsVal' , ), 26, (26, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 144 , (3, 0, None, None) , 0 , )),
	(( u'DeletePicture' , u'bsData' , ), 27, (27, (), [ (8, 1, None, None) , ], 1 , 1 , 4 , 0 , 148 , (3, 0, None, None) , 0 , )),
	(( u'ComponentStepFactory' , u'pVal' , ), 28, (28, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 152 , (3, 0, None, None) , 0 , )),
	(( u'SetStepsData' , ), 29, (29, (), [ ], 1 , 1 , 4 , 0 , 156 , (3, 0, None, None) , 0 , )),
	(( u'ApplicationAreaID' , u'pVal' , ), 30, (30, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 160 , (3, 0, None, None) , 0 , )),
	(( u'ApplicationAreaID' , u'pVal' , ), 30, (30, (), [ (3, 1, None, None) , ], 1 , 4 , 4 , 0 , 164 , (3, 0, None, None) , 0 , )),
]

IComponentFactory_vtables_dispatch_ = 1
IComponentFactory_vtables_ = [
	(( u'get_ItemFromServer' , u'ItemKey' , u'pItem' , ), 20, (20, (), [ (12, 0, None, None) , 
			(16393, 0, None, None) , ], 1 , 1 , 4 , 0 , 64 , (3, 0, None, None) , 64 , )),
	(( u'IsComponentNameValid' , u'bsName' , u'pbsErrorString' , u'pvbValid' , ), 21, (21, (), [ 
			(8, 1, None, None) , (16392, 2, None, None) , (16395, 10, None, None) , ], 1 , 1 , 4 , 0 , 68 , (3, 0, None, None) , 0 , )),
]

IComponentFolder_vtables_dispatch_ = 1
IComponentFolder_vtables_ = [
	(( u'Name' , u'pVal' , ), 14, (14, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 92 , (3, 0, None, None) , 0 , )),
	(( u'Name' , u'pVal' , ), 14, (14, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 96 , (3, 0, None, None) , 0 , )),
	(( u'ComponentFactory' , u'ppVal' , ), 15, (15, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 100 , (3, 0, None, None) , 0 , )),
	(( u'ComponentFolderFactory' , u'ppVal' , ), 16, (16, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 104 , (3, 0, None, None) , 0 , )),
	(( u'Path' , u'pbsPath' , ), 17, (17, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 108 , (3, 0, None, None) , 0 , )),
]

IComponentFolderFactory_vtables_dispatch_ = 1
IComponentFolderFactory_vtables_ = [
	(( u'Root' , u'pVal' , ), 8, (8, (), [ (16393, 10, None, None) , ], 1 , 1 , 4 , 0 , 64 , (3, 0, None, None) , 0 , )),
	(( u'FolderPath' , u'folderId' , u'pVal' , ), 9, (9, (), [ (3, 1, None, None) , 
			(16392, 10, None, None) , ], 1 , 1 , 4 , 0 , 68 , (3, 0, None, None) , 0 , )),
	(( u'FolderByPath' , u'Path' , u'pVal' , ), 10, (10, (), [ (8, 1, None, None) , 
			(16393, 10, None, None) , ], 1 , 1 , 4 , 0 , 72 , (3, 0, None, None) , 0 , )),
	(( u'UnattachedComponents' , u'pVal' , ), 11, (11, (), [ (16393, 10, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , ], 1 , 1 , 4 , 0 , 76 , (3, 0, None, None) , 0 , )),
	(( u'Templates' , u'pVal' , ), 12, (12, (), [ (16393, 10, None, None) , ], 1 , 1 , 4 , 0 , 80 , (3, 0, None, None) , 0 , )),
	(( u'Obsolete' , u'pVal' , ), 13, (13, (), [ (16393, 10, None, None) , ], 1 , 1 , 4 , 0 , 84 , (3, 0, None, None) , 0 , )),
]

IComponentParam_vtables_dispatch_ = 1
IComponentParam_vtables_ = [
	(( u'Component' , u'pVal' , ), 20, (20, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 120 , (3, 0, None, None) , 0 , )),
	(( u'IsOut' , u'pVal' , ), 21, (21, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 124 , (3, 0, None, None) , 0 , )),
	(( u'IsOut' , u'pVal' , ), 21, (21, (), [ (3, 1, None, None) , ], 1 , 4 , 4 , 0 , 128 , (3, 0, None, None) , 0 , )),
]

IComponentStep_vtables_dispatch_ = 1
IComponentStep_vtables_ = [
	(( u'StepDescription' , u'pVal' , ), 11, (11, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 80 , (3, 0, None, None) , 0 , )),
	(( u'StepDescription' , u'pVal' , ), 11, (11, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 84 , (3, 0, None, None) , 0 , )),
	(( u'Order' , u'pVal' , ), 12, (12, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 88 , (3, 0, None, None) , 0 , )),
	(( u'Order' , u'pVal' , ), 12, (12, (), [ (3, 1, None, None) , ], 1 , 4 , 4 , 0 , 92 , (3, 0, None, None) , 0 , )),
	(( u'Parent' , u'pVal' , ), 13, (13, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 96 , (3, 0, None, None) , 0 , )),
	(( u'StepName' , u'pVal' , ), 14, (14, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 100 , (3, 0, None, None) , 0 , )),
	(( u'StepName' , u'pVal' , ), 14, (14, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 104 , (3, 0, None, None) , 0 , )),
	(( u'StepExpectedResult' , u'pVal' , ), 15, (15, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 108 , (3, 0, None, None) , 0 , )),
	(( u'StepExpectedResult' , u'pVal' , ), 15, (15, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 112 , (3, 0, None, None) , 0 , )),
	(( u'Validate' , ), 16, (16, (), [ ], 1 , 1 , 4 , 0 , 116 , (3, 0, None, None) , 0 , )),
]

ICondition_vtables_dispatch_ = 1
ICondition_vtables_ = [
	(( u'Value' , u'pVal' , ), 0, (0, (), [ (16396, 10, None, None) , ], 1 , 2 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
	(( u'Value' , u'pVal' , ), 0, (0, (), [ (12, 1, None, None) , ], 1 , 4 , 4 , 0 , 32 , (3, 0, None, None) , 0 , )),
	(( u'Type' , u'pVal' , ), 1, (1, (), [ (16386, 10, None, None) , ], 1 , 2 , 4 , 0 , 36 , (3, 0, None, None) , 0 , )),
	(( u'Source' , u'pVal' , ), 2, (2, (), [ (16396, 10, None, None) , ], 1 , 2 , 4 , 0 , 40 , (3, 0, None, None) , 0 , )),
	(( u'Target' , u'pVal' , ), 3, (3, (), [ (16396, 10, None, None) , ], 1 , 2 , 4 , 0 , 44 , (3, 0, None, None) , 0 , )),
	(( u'Description' , u'pVal' , ), 4, (4, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 48 , (3, 0, None, None) , 0 , )),
	(( u'Description' , u'pVal' , ), 4, (4, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 52 , (3, 0, None, None) , 0 , )),
	(( u'ID' , u'pVal' , ), 5, (5, (), [ (16396, 10, None, None) , ], 1 , 2 , 4 , 0 , 56 , (3, 0, None, None) , 0 , )),
	(( u'SourceInstance' , u'pVal' , ), 6, (6, (), [ (16396, 10, None, None) , ], 1 , 2 , 4 , 0 , 60 , (3, 0, None, None) , 0 , )),
	(( u'TargetInstance' , u'pVal' , ), 7, (7, (), [ (16396, 10, None, None) , ], 1 , 2 , 4 , 0 , 64 , (3, 0, None, None) , 0 , )),
	(( u'SourceTestId' , u'pVal' , ), 8, (8, (), [ (16396, 10, None, None) , ], 1 , 2 , 4 , 0 , 68 , (3, 0, None, None) , 0 , )),
	(( u'TargetTestId' , u'pVal' , ), 9, (9, (), [ (16396, 10, None, None) , ], 1 , 2 , 4 , 0 , 72 , (3, 0, None, None) , 0 , )),
]

IConditionFactory_vtables_dispatch_ = 1
IConditionFactory_vtables_ = [
	(( u'Save' , ), 8, (8, (), [ ], 1 , 1 , 4 , 0 , 64 , (3, 0, None, None) , 0 , )),
]

ICoverable_vtables_dispatch_ = 1
ICoverable_vtables_ = [
	(( u'CoverageFactory' , u'pVal' , ), 1, (1, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
	(( u'CoverRequirementEx' , u'Req' , u'RequirementFilter' , u'Recursive' , ), 2, (2, (), [ 
			(12, 1, None, None) , (8, 1, None, None) , (11, 1, None, None) , ], 1 , 1 , 4 , 0 , 32 , (3, 0, None, None) , 0 , )),
]

ICoverableReq_vtables_dispatch_ = 1
ICoverableReq_vtables_ = [
	(( u'RequirementCoverageFactory' , u'pVal' , ), 1, (1, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
	(( u'AddTestInstanceToCoverage' , u'TestInstanceID' , ), 2, (2, (), [ (3, 1, None, None) , ], 1 , 1 , 4 , 0 , 32 , (3, 0, None, None) , 0 , )),
	(( u'AddTestToCoverage' , u'TestId' , ), 3, (3, (), [ (3, 1, None, None) , ], 1 , 1 , 4 , 0 , 36 , (3, 0, None, None) , 0 , )),
	(( u'AddTestsFromTestSetToCoverage' , u'TestSetID' , u'TestInstanceFilter' , ), 4, (4, (), [ (3, 1, None, None) , 
			(8, 1, None, None) , ], 1 , 1 , 4 , 0 , 40 , (3, 0, None, None) , 0 , )),
	(( u'AddSubjectToCoverage' , u'SubjectID' , u'TestFilter' , ), 5, (5, (), [ (3, 1, None, None) , 
			(8, 1, None, None) , ], 1 , 1 , 4 , 0 , 44 , (3, 0, None, None) , 0 , )),
]

ICoverageEntity_vtables_dispatch_ = 1
ICoverageEntity_vtables_ = [
	(( u'RequirementEntity' , u'pVal' , ), 11, (11, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 80 , (3, 0, None, None) , 0 , )),
	(( u'TargetEntity' , u'pVal' , ), 12, (12, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 84 , (3, 0, None, None) , 0 , )),
	(( u'CoverageType' , u'pVal' , ), 13, (13, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 88 , (3, 0, None, None) , 0 , )),
	(( u'Status' , u'pVal' , ), 14, (14, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 92 , (3, 0, None, None) , 0 , )),
	(( u'Name' , u'pVal' , ), 15, (15, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 96 , (3, 0, None, None) , 0 , )),
]

ICoverageFactory_vtables_dispatch_ = 1
ICoverageFactory_vtables_ = [
]

ICustomization_vtables_dispatch_ = 1
ICustomization_vtables_ = [
	(( u'Load' , ), 4, (4, (), [ ], 1 , 1 , 4 , 0 , 40 , (3, 0, None, None) , 0 , )),
	(( u'Commit' , ), 5, (5, (), [ ], 1 , 1 , 4 , 0 , 44 , (3, 0, None, None) , 0 , )),
	(( u'Fields' , u'pVal' , ), 6, (6, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 48 , (3, 0, None, None) , 0 , )),
	(( u'Lists' , u'pVal' , ), 7, (7, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 52 , (3, 0, None, None) , 0 , )),
	(( u'Permissions' , u'pVal' , ), 8, (8, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 56 , (3, 0, None, None) , 0 , )),
	(( u'Users' , u'pVal' , ), 9, (9, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 60 , (3, 0, None, None) , 0 , )),
	(( u'UsersGroups' , u'pVal' , ), 10, (10, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 64 , (3, 0, None, None) , 0 , )),
	(( u'Actions' , u'pVal' , ), 11, (11, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 68 , (3, 0, None, None) , 0 , )),
	(( u'Modules' , u'pVal' , ), 12, (12, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 72 , (3, 0, None, None) , 0 , )),
	(( u'MailConditions' , u'pVal' , ), 13, (13, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 76 , (3, 0, None, None) , 0 , )),
	(( u'ExtendedUDFSupport' , u'pVal' , ), 14, (14, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 80 , (3, 0, None, None) , 0 , )),
]

ICustomization2_vtables_dispatch_ = 1
ICustomization2_vtables_ = [
	(( u'IsChanged' , u'IsChanged' , ), 15, (15, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 84 , (3, 0, None, None) , 0 , )),
	(( u'Rollback' , ), 16, (16, (), [ ], 1 , 1 , 4 , 0 , 88 , (3, 0, None, None) , 0 , )),
]

ICustomizationAction_vtables_dispatch_ = 1
ICustomizationAction_vtables_ = [
	(( u'Name' , u'pVal' , ), 1, (1, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
	(( u'Groups' , u'pVal' , ), 2, (2, (), [ (16393, 10, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , ], 1 , 2 , 4 , 0 , 32 , (3, 0, None, None) , 0 , )),
	(( u'AddGroup' , u'Group' , ), 3, (3, (), [ (12, 1, None, None) , ], 1 , 1 , 4 , 0 , 36 , (3, 0, None, None) , 0 , )),
	(( u'RemoveGroup' , u'Group' , ), 4, (4, (), [ (12, 1, None, None) , ], 1 , 1 , 4 , 0 , 40 , (3, 0, None, None) , 0 , )),
	(( u'OwnerGroup' , u'pVal' , ), 5, (5, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 44 , (3, 0, None, None) , 0 , )),
	(( u'OwnerGroup' , u'pVal' , ), 5, (5, (), [ (9, 1, None, None) , ], 1 , 4 , 4 , 0 , 48 , (3, 0, None, None) , 0 , )),
	(( u'Updated' , u'pVal' , ), 6, (6, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 52 , (3, 0, None, None) , 0 , )),
	(( u'Updated' , u'pVal' , ), 6, (6, (), [ (11, 1, None, None) , ], 1 , 4 , 4 , 0 , 56 , (3, 0, None, None) , 0 , )),
	(( u'IsGroupPermited' , u'Group' , u'pVal' , ), 7, (7, (), [ (12, 1, None, None) , 
			(16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 60 , (3, 0, None, None) , 0 , )),
	(( u'IsOwnerGroup' , u'Group' , u'pVal' , ), 8, (8, (), [ (12, 1, None, None) , 
			(16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 64 , (3, 0, None, None) , 0 , )),
	(( u'IsUserPermited' , u'User' , u'pVal' , ), 9, (9, (), [ (12, 1, None, None) , 
			(16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 68 , (3, 0, None, None) , 0 , )),
	(( u'AuditAction' , u'pVal' , ), 10, (10, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 72 , (3, 0, None, None) , 64 , )),
	(( u'AuditAction' , u'pVal' , ), 10, (10, (), [ (11, 1, None, None) , ], 1 , 4 , 4 , 0 , 76 , (3, 0, None, None) , 64 , )),
]

ICustomizationActions_vtables_dispatch_ = 1
ICustomizationActions_vtables_ = [
	(( u'Actions' , u'pVal' , ), 1, (1, (), [ (16393, 10, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , ], 1 , 2 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
	(( u'Action' , u'Name' , u'pVal' , ), 2, (2, (), [ (8, 1, None, None) , 
			(16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 32 , (3, 0, None, None) , 0 , )),
]

ICustomizationField_vtables_dispatch_ = 1
ICustomizationField_vtables_ = [
	(( u'UserLabel' , u'pVal' , ), 1, (1, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
	(( u'UserLabel' , u'pVal' , ), 1, (1, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 32 , (3, 0, None, None) , 0 , )),
	(( u'TableName' , u'pVal' , ), 2, (2, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 36 , (3, 0, None, None) , 0 , )),
	(( u'EditStyle' , u'pVal' , ), 3, (3, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 40 , (3, 0, None, None) , 64 , )),
	(( u'EditStyle' , u'pVal' , ), 3, (3, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 44 , (3, 0, None, None) , 64 , )),
	(( u'IsSystem' , u'pVal' , ), 4, (4, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 48 , (3, 0, None, None) , 0 , )),
	(( u'IsSystem' , u'pVal' , ), 4, (4, (), [ (11, 1, None, None) , ], 1 , 4 , 4 , 0 , 52 , (3, 0, None, None) , 0 , )),
	(( u'IsCanFilter' , u'pVal' , ), 5, (5, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 56 , (3, 0, None, None) , 0 , )),
	(( u'IsCanFilter' , u'pVal' , ), 5, (5, (), [ (11, 1, None, None) , ], 1 , 4 , 4 , 0 , 60 , (3, 0, None, None) , 0 , )),
	(( u'IsKey' , u'pVal' , ), 6, (6, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 64 , (3, 0, None, None) , 0 , )),
	(( u'IsKey' , u'pVal' , ), 6, (6, (), [ (11, 1, None, None) , ], 1 , 4 , 4 , 0 , 68 , (3, 0, None, None) , 64 , )),
	(( u'KeyOrder' , u'pVal' , ), 7, (7, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 72 , (3, 0, None, None) , 0 , )),
	(( u'KeyOrder' , u'pVal' , ), 7, (7, (), [ (3, 1, None, None) , ], 1 , 4 , 4 , 0 , 76 , (3, 0, None, None) , 64 , )),
	(( u'IsActive' , u'pVal' , ), 8, (8, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 80 , (3, 0, None, None) , 0 , )),
	(( u'IsActive' , u'pVal' , ), 8, (8, (), [ (11, 1, None, None) , ], 1 , 4 , 4 , 0 , 84 , (3, 0, None, None) , 0 , )),
	(( u'IsEdit' , u'pVal' , ), 9, (9, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 88 , (3, 0, None, None) , 0 , )),
	(( u'IsEdit' , u'pVal' , ), 9, (9, (), [ (11, 1, None, None) , ], 1 , 4 , 4 , 0 , 92 , (3, 0, None, None) , 0 , )),
	(( u'IsHistory' , u'pVal' , ), 10, (10, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 96 , (3, 0, None, None) , 0 , )),
	(( u'IsHistory' , u'pVal' , ), 10, (10, (), [ (11, 1, None, None) , ], 1 , 4 , 4 , 0 , 100 , (3, 0, None, None) , 0 , )),
	(( u'IsMail' , u'pVal' , ), 11, (11, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 104 , (3, 0, None, None) , 0 , )),
	(( u'IsMail' , u'pVal' , ), 11, (11, (), [ (11, 1, None, None) , ], 1 , 4 , 4 , 0 , 108 , (3, 0, None, None) , 0 , )),
	(( u'IsVerify' , u'pVal' , ), 12, (12, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 112 , (3, 0, None, None) , 0 , )),
	(( u'IsVerify' , u'pVal' , ), 12, (12, (), [ (11, 1, None, None) , ], 1 , 4 , 4 , 0 , 116 , (3, 0, None, None) , 0 , )),
	(( u'IsByCode' , u'pVal' , ), 13, (13, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 120 , (3, 0, None, None) , 0 , )),
	(( u'IsByCode' , u'pVal' , ), 13, (13, (), [ (11, 1, None, None) , ], 1 , 4 , 4 , 0 , 124 , (3, 0, None, None) , 0 , )),
	(( u'IsRequired' , u'pVal' , ), 14, (14, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 128 , (3, 0, None, None) , 0 , )),
	(( u'IsRequired' , u'pVal' , ), 14, (14, (), [ (11, 1, None, None) , ], 1 , 4 , 4 , 0 , 132 , (3, 0, None, None) , 0 , )),
	(( u'UserColumnType' , u'pVal' , ), 15, (15, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 136 , (3, 0, None, None) , 0 , )),
	(( u'UserColumnType' , u'pVal' , ), 15, (15, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 140 , (3, 0, None, None) , 0 , )),
	(( u'IsKeepValue' , u'pVal' , ), 16, (16, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 144 , (3, 0, None, None) , 0 , )),
	(( u'IsKeepValue' , u'pVal' , ), 16, (16, (), [ (11, 1, None, None) , ], 1 , 4 , 4 , 0 , 148 , (3, 0, None, None) , 0 , )),
	(( u'IsCustomizable' , u'pVal' , ), 17, (17, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 152 , (3, 0, None, None) , 0 , )),
	(( u'IsCustomizable' , u'pVal' , ), 17, (17, (), [ (11, 1, None, None) , ], 1 , 4 , 4 , 0 , 156 , (3, 0, None, None) , 0 , )),
	(( u'FieldSize' , u'pVal' , ), 18, (18, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 160 , (3, 0, None, None) , 0 , )),
	(( u'FieldSize' , u'pVal' , ), 18, (18, (), [ (3, 1, None, None) , ], 1 , 4 , 4 , 0 , 164 , (3, 0, None, None) , 0 , )),
	(( u'ColumnName' , u'pVal' , ), 19, (19, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 168 , (3, 0, None, None) , 0 , )),
	(( u'ColumnType' , u'pVal' , ), 20, (20, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 172 , (3, 0, None, None) , 0 , )),
	(( u'ColumnType' , u'pVal' , ), 20, (20, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 176 , (3, 0, None, None) , 0 , )),
	(( u'EditMask' , u'pVal' , ), 21, (21, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 180 , (3, 0, None, None) , 0 , )),
	(( u'EditMask' , u'pVal' , ), 21, (21, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 184 , (3, 0, None, None) , 0 , )),
	(( u'List' , u'pVal' , ), 22, (22, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 188 , (3, 0, None, None) , 0 , )),
	(( u'List' , u'pVal' , ), 22, (22, (), [ (9, 1, None, None) , ], 1 , 4 , 4 , 0 , 192 , (3, 0, None, None) , 0 , )),
	(( u'RootId' , u'pVal' , ), 23, (23, (), [ (16396, 10, None, None) , ], 1 , 2 , 4 , 0 , 196 , (3, 0, None, None) , 0 , )),
	(( u'RootId' , u'pVal' , ), 23, (23, (), [ (12, 1, None, None) , ], 1 , 4 , 4 , 0 , 200 , (3, 0, None, None) , 0 , )),
	(( u'Type' , u'pVal' , ), 24, (24, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 204 , (3, 0, None, None) , 0 , )),
	(( u'Type' , u'pVal' , ), 24, (24, (), [ (3, 1, None, None) , ], 1 , 4 , 4 , 0 , 208 , (3, 0, None, None) , 0 , )),
	(( u'Updated' , u'pVal' , ), 25, (25, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 212 , (3, 0, None, None) , 0 , )),
	(( u'Updated' , u'pVal' , ), 25, (25, (), [ (11, 1, None, None) , ], 1 , 4 , 4 , 0 , 216 , (3, 0, None, None) , 0 , )),
	(( u'GrantModifyMask' , u'pVal' , ), 26, (26, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 220 , (3, 0, None, None) , 0 , )),
	(( u'GrantModifyMask' , u'pVal' , ), 26, (26, (), [ (3, 1, None, None) , ], 1 , 4 , 4 , 0 , 224 , (3, 0, None, None) , 0 , )),
	(( u'OwnerSensibleMask' , u'pVal' , ), 27, (27, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 228 , (3, 0, None, None) , 0 , )),
	(( u'OwnerSensibleMask' , u'pVal' , ), 27, (27, (), [ (3, 1, None, None) , ], 1 , 4 , 4 , 0 , 232 , (3, 0, None, None) , 0 , )),
	(( u'IsVisibleInNewBug' , u'pVal' , ), 28, (28, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 236 , (3, 0, None, None) , 0 , )),
	(( u'IsVisibleInNewBug' , u'pVal' , ), 28, (28, (), [ (3, 1, None, None) , ], 1 , 4 , 4 , 0 , 240 , (3, 0, None, None) , 0 , )),
	(( u'IsTransitionLogic' , u'pVal' , ), 29, (29, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 244 , (3, 0, None, None) , 0 , )),
	(( u'IsTransitionLogic' , u'pVal' , ), 29, (29, (), [ (11, 1, None, None) , ], 1 , 4 , 4 , 0 , 248 , (3, 0, None, None) , 0 , )),
	(( u'DefaultValue' , u'pVal' , ), 30, (30, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 252 , (3, 0, None, None) , 0 , )),
	(( u'DefaultValue' , u'pVal' , ), 30, (30, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 256 , (3, 0, None, None) , 0 , )),
	(( u'IsToSum' , u'pVal' , ), 31, (31, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 260 , (3, 0, None, None) , 0 , )),
	(( u'IsToSum' , u'pVal' , ), 31, (31, (), [ (11, 1, None, None) , ], 1 , 4 , 4 , 0 , 264 , (3, 0, None, None) , 0 , )),
	(( u'VisibleForGroups' , u'pVal' , ), 32, (32, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 268 , (3, 0, None, None) , 0 , )),
	(( u'VisibleForGroups' , u'pVal' , ), 32, (32, (), [ (3, 1, None, None) , ], 1 , 4 , 4 , 0 , 272 , (3, 0, None, None) , 0 , )),
	(( u'VersionControl' , u'pVal' , ), 33, (33, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 276 , (3, 0, None, None) , 0 , )),
	(( u'VersionControl' , u'pVal' , ), 33, (33, (), [ (11, 1, None, None) , ], 1 , 4 , 4 , 0 , 280 , (3, 0, None, None) , 0 , )),
	(( u'NewCreated' , u'pVal' , ), 34, (34, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 284 , (3, 0, None, None) , 0 , )),
	(( u'NewCreated' , u'pVal' , ), 34, (34, (), [ (11, 1, None, None) , ], 1 , 4 , 4 , 0 , 288 , (3, 0, None, None) , 0 , )),
]

ICustomizationField2_vtables_dispatch_ = 1
ICustomizationField2_vtables_ = [
	(( u'GrantModifyForGroup' , u'Group' , u'pGrant' , ), 35, (35, (), [ (12, 1, None, None) , 
			(16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 292 , (3, 0, None, None) , 0 , )),
	(( u'GrantModifyForGroup' , u'Group' , u'pGrant' , ), 35, (35, (), [ (12, 1, None, None) , 
			(11, 1, None, None) , ], 1 , 4 , 4 , 0 , 296 , (3, 0, None, None) , 0 , )),
	(( u'AuthorizedModifyForGroups' , u'pVal' , ), 36, (36, (), [ (16393, 10, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , ], 1 , 2 , 4 , 0 , 300 , (3, 0, None, None) , 0 , )),
	(( u'OwnerSensibleForGroup' , u'Group' , u'pGrant' , ), 37, (37, (), [ (12, 1, None, None) , 
			(16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 304 , (3, 0, None, None) , 0 , )),
	(( u'OwnerSensibleForGroup' , u'Group' , u'pGrant' , ), 37, (37, (), [ (12, 1, None, None) , 
			(11, 1, None, None) , ], 1 , 4 , 4 , 0 , 308 , (3, 0, None, None) , 0 , )),
	(( u'AuthorizedOwnerSensibleForGroups' , u'pVal' , ), 38, (38, (), [ (16393, 10, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , ], 1 , 2 , 4 , 0 , 312 , (3, 0, None, None) , 0 , )),
	(( u'VisibleInNewBugForGroup' , u'Group' , u'pGrant' , ), 39, (39, (), [ (12, 1, None, None) , 
			(16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 316 , (3, 0, None, None) , 0 , )),
	(( u'VisibleInNewBugForGroup' , u'Group' , u'pGrant' , ), 39, (39, (), [ (12, 1, None, None) , 
			(11, 1, None, None) , ], 1 , 4 , 4 , 0 , 320 , (3, 0, None, None) , 0 , )),
	(( u'AuthorizedVisibleInNewBugForGroups' , u'pVal' , ), 40, (40, (), [ (16393, 10, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , ], 1 , 2 , 4 , 0 , 324 , (3, 0, None, None) , 0 , )),
	(( u'VisibleForGroup' , u'Group' , u'pGrant' , ), 41, (41, (), [ (12, 1, None, None) , 
			(16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 328 , (3, 0, None, None) , 0 , )),
	(( u'VisibleForGroup' , u'Group' , u'pGrant' , ), 41, (41, (), [ (12, 1, None, None) , 
			(11, 1, None, None) , ], 1 , 4 , 4 , 0 , 332 , (3, 0, None, None) , 0 , )),
	(( u'AuthorizedVisibleForGroups' , u'pVal' , ), 42, (42, (), [ (16393, 10, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , ], 1 , 2 , 4 , 0 , 336 , (3, 0, None, None) , 0 , )),
	(( u'IsCanGroup' , u'pVal' , ), 43, (43, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 340 , (3, 0, None, None) , 0 , )),
	(( u'IsCanGroup' , u'pVal' , ), 43, (43, (), [ (11, 1, None, None) , ], 1 , 4 , 4 , 0 , 344 , (3, 0, None, None) , 0 , )),
	(( u'AuditUpdate' , u'pVal' , ), 44, (44, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 348 , (3, 0, None, None) , 0 , )),
	(( u'AuditUpdate' , u'pVal' , ), 44, (44, (), [ (11, 1, None, None) , ], 1 , 4 , 4 , 0 , 352 , (3, 0, None, None) , 0 , )),
	(( u'IsSearchable' , u'pVal' , ), 45, (45, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 356 , (3, 0, None, None) , 0 , )),
	(( u'IsSearchable' , u'pVal' , ), 45, (45, (), [ (11, 1, None, None) , ], 1 , 4 , 4 , 0 , 360 , (3, 0, None, None) , 0 , )),
	(( u'CanMakeSearchable' , u'pVal' , ), 46, (46, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 364 , (3, 0, None, None) , 0 , )),
	(( u'IsVirtual' , u'pVal' , ), 47, (47, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 368 , (3, 0, None, None) , 0 , )),
	(( u'IsMultiValue' , u'pVal' , ), 48, (48, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 372 , (3, 0, None, None) , 0 , )),
	(( u'IsMultiValue' , u'pVal' , ), 48, (48, (), [ (11, 1, None, None) , ], 1 , 4 , 4 , 0 , 376 , (3, 0, None, None) , 0 , )),
	(( u'CanMakeMultiValue' , u'pVal' , ), 49, (49, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 380 , (3, 0, None, None) , 0 , )),
]

ICustomizationFields_vtables_dispatch_ = 1
ICustomizationFields_vtables_ = [
	(( u'Field' , u'TableName' , u'FieldName' , u'pVal' , ), 1, (1, (), [ 
			(8, 1, None, None) , (8, 1, None, None) , (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
	(( u'Fields' , u'TableName' , u'pVal' , ), 2, (2, (), [ (8, 49, "u''", None) , 
			(16393, 10, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , ], 1 , 2 , 4 , 0 , 32 , (3, 32, None, None) , 0 , )),
	(( u'Fields' , u'TableName' , u'pVal' , ), 2, (2, (), [ (8, 49, "u''", None) , 
			(16393, 10, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , ], 1 , 2 , 4 , 0 , 32 , (3, 32, None, None) , 0 , )),
	(( u'AddActiveField' , u'TableName' , u'pField' , ), 3, (3, (), [ (8, 1, None, None) , 
			(16393, 10, None, None) , ], 1 , 1 , 4 , 0 , 36 , (3, 0, None, None) , 0 , )),
	(( u'FieldExists' , u'TableName' , u'FieldName' , u'pVal' , ), 4, (4, (), [ 
			(8, 1, None, None) , (8, 1, None, None) , (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 40 , (3, 0, None, None) , 0 , )),
	(( u'AddActiveMemoField' , u'TableName' , u'pField' , ), 5, (5, (), [ (8, 1, None, None) , 
			(16393, 10, None, None) , ], 1 , 1 , 4 , 0 , 44 , (3, 0, None, None) , 0 , )),
]

ICustomizationList_vtables_dispatch_ = 1
ICustomizationList_vtables_ = [
	(( u'RootNode' , u'pVal' , ), 1, (1, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
	(( u'Name' , u'pVal' , ), 2, (2, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 32 , (3, 0, None, None) , 0 , )),
	(( u'Find' , u'Val' , u'pNode' , ), 3, (3, (), [ (12, 1, None, None) , 
			(16393, 10, None, None) , ], 1 , 1 , 4 , 0 , 36 , (3, 0, None, None) , 0 , )),
	(( u'Deleted' , u'pVal' , ), 4, (4, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 40 , (3, 0, None, None) , 0 , )),
	(( u'Deleted' , u'pVal' , ), 4, (4, (), [ (11, 1, None, None) , ], 1 , 4 , 4 , 0 , 44 , (3, 0, None, None) , 0 , )),
]

ICustomizationListNode_vtables_dispatch_ = 1
ICustomizationListNode_vtables_ = [
	(( u'AddChild' , u'Node' , u'NewChildNode' , ), 1, (1, (), [ (12, 1, None, None) , 
			(16393, 10, None, None) , ], 1 , 1 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
	(( u'RemoveChild' , u'Node' , ), 2, (2, (), [ (12, 1, None, None) , ], 1 , 1 , 4 , 0 , 32 , (3, 0, None, None) , 0 , )),
	(( u'Child' , u'NodeName' , u'pVal' , ), 3, (3, (), [ (8, 1, None, None) , 
			(16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 36 , (3, 0, None, None) , 0 , )),
	(( u'Children' , u'pVal' , ), 4, (4, (), [ (16393, 10, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , ], 1 , 2 , 4 , 0 , 40 , (3, 0, None, None) , 0 , )),
	(( u'Name' , u'pVal' , ), 5, (5, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 44 , (3, 0, None, None) , 0 , )),
	(( u'Name' , u'pVal' , ), 5, (5, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 48 , (3, 0, None, None) , 0 , )),
	(( u'ID' , u'pVal' , ), 6, (6, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 52 , (3, 0, None, None) , 64 , )),
	(( u'ID' , u'pVal' , ), 6, (6, (), [ (3, 1, None, None) , ], 1 , 4 , 4 , 0 , 56 , (3, 0, None, None) , 0 , )),
	(( u'Father' , u'pVal' , ), 7, (7, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 60 , (3, 0, None, None) , 0 , )),
	(( u'Father' , u'pVal' , ), 7, (7, (), [ (9, 1, None, None) , ], 1 , 4 , 4 , 0 , 64 , (3, 0, None, None) , 0 , )),
	(( u'CanAddChild' , u'pVal' , ), 8, (8, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 68 , (3, 0, None, None) , 0 , )),
	(( u'ReadOnly' , u'pVal' , ), 9, (9, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 72 , (3, 0, None, None) , 0 , )),
	(( u'List' , u'pVal' , ), 10, (10, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 76 , (3, 0, None, None) , 0 , )),
	(( u'ChildrenCount' , u'pVal' , ), 11, (11, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 80 , (3, 0, None, None) , 0 , )),
	(( u'Updated' , u'pVal' , ), 12, (12, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 84 , (3, 0, None, None) , 0 , )),
	(( u'Updated' , u'pVal' , ), 12, (12, (), [ (11, 1, None, None) , ], 1 , 4 , 4 , 0 , 88 , (3, 0, None, None) , 0 , )),
	(( u'Deleted' , u'pVal' , ), 13, (13, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 92 , (3, 0, None, None) , 0 , )),
	(( u'Deleted' , u'pVal' , ), 13, (13, (), [ (11, 1, None, None) , ], 1 , 4 , 4 , 0 , 96 , (3, 0, None, None) , 0 , )),
	(( u'Order' , u'pVal' , ), 14, (14, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 100 , (3, 0, None, None) , 0 , )),
	(( u'Order' , u'pVal' , ), 14, (14, (), [ (3, 1, None, None) , ], 1 , 4 , 4 , 0 , 104 , (3, 0, None, None) , 0 , )),
]

ICustomizationLists_vtables_dispatch_ = 1
ICustomizationLists_vtables_ = [
	(( u'AddList' , u'Name' , u'NewList' , ), 1, (1, (), [ (8, 1, None, None) , 
			(16393, 10, None, None) , ], 1 , 1 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
	(( u'RemoveList' , u'Name' , ), 2, (2, (), [ (8, 1, None, None) , ], 1 , 1 , 4 , 0 , 32 , (3, 0, None, None) , 0 , )),
	(( u'List' , u'Param' , u'pVal' , ), 3, (3, (), [ (12, 1, None, None) , 
			(16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 36 , (3, 0, None, None) , 0 , )),
	(( u'Count' , u'pVal' , ), 4, (4, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 40 , (3, 0, None, None) , 0 , )),
	(( u'ListByCount' , u'Count' , u'pVal' , ), 5, (5, (), [ (3, 1, None, None) , 
			(16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 44 , (3, 0, None, None) , 0 , )),
	(( u'IsListExist' , u'ListName' , u'pVal' , ), 6, (6, (), [ (8, 1, None, None) , 
			(16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 48 , (3, 0, None, None) , 0 , )),
]

ICustomizationMailCondition_vtables_dispatch_ = 1
ICustomizationMailCondition_vtables_ = [
	(( u'Name' , u'pVal' , ), 1, (1, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
	(( u'ConditionText' , u'pVal' , ), 2, (2, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 32 , (3, 0, None, None) , 0 , )),
	(( u'ConditionText' , u'pVal' , ), 2, (2, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 36 , (3, 0, None, None) , 0 , )),
	(( u'Updated' , u'pVal' , ), 3, (3, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 40 , (3, 0, None, None) , 0 , )),
	(( u'Updated' , u'pVal' , ), 3, (3, (), [ (11, 1, None, None) , ], 1 , 4 , 4 , 0 , 44 , (3, 0, None, None) , 0 , )),
	(( u'Deleted' , u'pVal' , ), 4, (4, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 48 , (3, 0, None, None) , 0 , )),
	(( u'Deleted' , u'pVal' , ), 4, (4, (), [ (11, 1, None, None) , ], 1 , 4 , 4 , 0 , 52 , (3, 0, None, None) , 0 , )),
	(( u'ConditionType' , u'pVal' , ), 5, (5, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 56 , (3, 0, None, None) , 0 , )),
]

ICustomizationMailConditions_vtables_dispatch_ = 1
ICustomizationMailConditions_vtables_ = [
	(( u'Condition' , u'Name' , u'ConditionType' , u'pVal' , ), 1, (1, (), [ 
			(8, 1, None, None) , (3, 1, None, None) , (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
	(( u'Conditions' , u'pVal' , ), 2, (2, (), [ (16393, 10, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , ], 1 , 2 , 4 , 0 , 32 , (3, 0, None, None) , 0 , )),
	(( u'AddCondition' , u'Name' , u'ConditionType' , u'ConditionText' , u'pVal' , 
			), 3, (3, (), [ (8, 1, None, None) , (3, 1, None, None) , (8, 1, None, None) , (16393, 10, None, None) , ], 1 , 1 , 4 , 0 , 36 , (3, 0, None, None) , 0 , )),
	(( u'RemoveCondition' , u'Name' , u'ConditionType' , ), 4, (4, (), [ (8, 1, None, None) , 
			(3, 1, None, None) , ], 1 , 1 , 4 , 0 , 40 , (3, 0, None, None) , 0 , )),
	(( u'ConditionExist' , u'Name' , u'ConditionType' , u'pVal' , ), 5, (5, (), [ 
			(8, 1, None, None) , (3, 1, None, None) , (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 44 , (3, 0, None, None) , 0 , )),
]

ICustomizationModules_vtables_dispatch_ = 1
ICustomizationModules_vtables_ = [
	(( u'Name' , u'ModuleID' , u'pVal' , ), 1, (1, (), [ (3, 1, None, None) , 
			(16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
	(( u'Name' , u'ModuleID' , u'pVal' , ), 1, (1, (), [ (3, 1, None, None) , 
			(8, 1, None, None) , ], 1 , 4 , 4 , 0 , 32 , (3, 0, None, None) , 0 , )),
	(( u'GUID' , u'ModuleID' , u'pVal' , ), 2, (2, (), [ (3, 1, None, None) , 
			(16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 36 , (3, 0, None, None) , 0 , )),
	(( u'GUID' , u'ModuleID' , u'pVal' , ), 2, (2, (), [ (3, 1, None, None) , 
			(8, 1, None, None) , ], 1 , 4 , 4 , 0 , 40 , (3, 0, None, None) , 0 , )),
	(( u'Description' , u'ModuleID' , u'pVal' , ), 3, (3, (), [ (3, 1, None, None) , 
			(16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 44 , (3, 0, None, None) , 0 , )),
	(( u'Description' , u'ModuleID' , u'pVal' , ), 3, (3, (), [ (3, 1, None, None) , 
			(8, 1, None, None) , ], 1 , 4 , 4 , 0 , 48 , (3, 0, None, None) , 0 , )),
	(( u'Visible' , u'ModuleID' , u'pVal' , ), 4, (4, (), [ (3, 1, None, None) , 
			(16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 52 , (3, 0, None, None) , 0 , )),
	(( u'Visible' , u'ModuleID' , u'pVal' , ), 4, (4, (), [ (3, 1, None, None) , 
			(3, 1, None, None) , ], 1 , 4 , 4 , 0 , 56 , (3, 0, None, None) , 0 , )),
]

ICustomizationModules2_vtables_dispatch_ = 1
ICustomizationModules2_vtables_ = [
	(( u'IsVisibleForGroup' , u'ModuleID' , u'Group' , u'pVal' , ), 5, (5, (), [ 
			(3, 1, None, None) , (12, 1, None, None) , (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 60 , (3, 0, None, None) , 0 , )),
	(( u'IsVisibleForGroup' , u'ModuleID' , u'Group' , u'pVal' , ), 5, (5, (), [ 
			(3, 1, None, None) , (12, 1, None, None) , (11, 1, None, None) , ], 1 , 4 , 4 , 0 , 64 , (3, 0, None, None) , 0 , )),
	(( u'VisibleForGroups' , u'ModuleID' , u'pVal' , ), 6, (6, (), [ (3, 1, None, None) , 
			(16393, 10, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , ], 1 , 2 , 4 , 0 , 68 , (3, 0, None, None) , 0 , )),
]

ICustomizationPermissions_vtables_dispatch_ = 1
ICustomizationPermissions_vtables_ = [
	(( u'CanAddItem' , u'EntityName' , u'Group' , u'pVal' , ), 1, (1, (), [ 
			(8, 1, None, None) , (12, 1, None, None) , (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
	(( u'CanAddItem' , u'EntityName' , u'Group' , u'pVal' , ), 1, (1, (), [ 
			(8, 1, None, None) , (12, 1, None, None) , (11, 1, None, None) , ], 1 , 4 , 4 , 0 , 32 , (3, 0, None, None) , 0 , )),
	(( u'CanRemoveItem' , u'EntityName' , u'Group' , u'pVal' , ), 2, (2, (), [ 
			(8, 1, None, None) , (12, 1, None, None) , (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 36 , (3, 0, None, None) , 0 , )),
	(( u'CanRemoveItem' , u'EntityName' , u'Group' , u'pVal' , ), 2, (2, (), [ 
			(8, 1, None, None) , (12, 1, None, None) , (3, 1, None, None) , ], 1 , 4 , 4 , 0 , 40 , (3, 0, None, None) , 0 , )),
	(( u'CanModifyField' , u'EntityName' , u'Field' , u'Group' , u'pVal' , 
			), 3, (3, (), [ (8, 1, None, None) , (12, 1, None, None) , (12, 1, None, None) , (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 44 , (3, 0, None, None) , 0 , )),
	(( u'CanModifyField' , u'EntityName' , u'Field' , u'Group' , u'pVal' , 
			), 3, (3, (), [ (8, 1, None, None) , (12, 1, None, None) , (12, 1, None, None) , (3, 1, None, None) , ], 1 , 4 , 4 , 0 , 48 , (3, 0, None, None) , 0 , )),
	(( u'TransitionRules' , u'EntityName' , u'Field' , u'Group' , u'pVal' , 
			), 4, (4, (), [ (8, 1, None, None) , (12, 1, None, None) , (12, 1, None, None) , (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 52 , (3, 0, None, None) , 0 , )),
	(( u'CanAllowAttachment' , u'EntityName' , u'Group' , u'pVal' , ), 5, (5, (), [ 
			(8, 1, None, None) , (12, 1, None, None) , (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 56 , (3, 0, None, None) , 0 , )),
	(( u'CanAllowAttachment' , u'EntityName' , u'Group' , u'pVal' , ), 5, (5, (), [ 
			(8, 1, None, None) , (12, 1, None, None) , (3, 1, None, None) , ], 1 , 4 , 4 , 0 , 60 , (3, 0, None, None) , 0 , )),
	(( u'CanUseOwnerSensible' , u'EntityName' , u'pVal' , ), 6, (6, (), [ (8, 1, None, None) , 
			(16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 64 , (3, 0, None, None) , 0 , )),
	(( u'HasAttachmentField' , u'EntityName' , u'pVal' , ), 7, (7, (), [ (8, 1, None, None) , 
			(16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 68 , (3, 0, None, None) , 0 , )),
	(( u'IsVisibleInNewBug' , u'EntityName' , u'Field' , u'Group' , u'pVal' , 
			), 8, (8, (), [ (8, 1, None, None) , (12, 1, None, None) , (12, 1, None, None) , (11, 1, None, None) , ], 1 , 4 , 4 , 0 , 72 , (3, 0, None, None) , 0 , )),
	(( u'IsVisibleInNewBug' , u'EntityName' , u'Field' , u'Group' , u'pVal' , 
			), 8, (8, (), [ (8, 1, None, None) , (12, 1, None, None) , (12, 1, None, None) , (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 76 , (3, 0, None, None) , 0 , )),
	(( u'CanModifyItem' , u'EntityName' , u'Group' , u'pVal' , ), 9, (9, (), [ 
			(8, 1, None, None) , (12, 1, None, None) , (11, 1, None, None) , ], 1 , 4 , 4 , 0 , 80 , (3, 0, None, None) , 0 , )),
	(( u'CanModifyItem' , u'EntityName' , u'Group' , u'pVal' , ), 9, (9, (), [ 
			(8, 1, None, None) , (12, 1, None, None) , (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 84 , (3, 0, None, None) , 0 , )),
	(( u'AuditAddItem' , u'EntityName' , u'pVal' , ), 10, (10, (), [ (8, 1, None, None) , 
			(16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 88 , (3, 0, None, None) , 64 , )),
	(( u'AuditAddItem' , u'EntityName' , u'pVal' , ), 10, (10, (), [ (8, 1, None, None) , 
			(11, 1, None, None) , ], 1 , 4 , 4 , 0 , 92 , (3, 0, None, None) , 64 , )),
	(( u'AuditRemoveItem' , u'EntityName' , u'pVal' , ), 11, (11, (), [ (8, 1, None, None) , 
			(16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 96 , (3, 0, None, None) , 64 , )),
	(( u'AuditRemoveItem' , u'EntityName' , u'pVal' , ), 11, (11, (), [ (8, 1, None, None) , 
			(11, 1, None, None) , ], 1 , 4 , 4 , 0 , 100 , (3, 0, None, None) , 64 , )),
]

ICustomizationTransitionRule_vtables_dispatch_ = 1
ICustomizationTransitionRule_vtables_ = [
	(( u'SourceValue' , u'pVal' , ), 1, (1, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
	(( u'SourceValue' , u'pVal' , ), 1, (1, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 32 , (3, 0, None, None) , 0 , )),
	(( u'DestinationValue' , u'pVal' , ), 2, (2, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 36 , (3, 0, None, None) , 0 , )),
	(( u'DestinationValue' , u'pVal' , ), 2, (2, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 40 , (3, 0, None, None) , 0 , )),
	(( u'IsAllowed' , u'pVal' , ), 3, (3, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 44 , (3, 0, None, None) , 64 , )),
	(( u'IsAllowed' , u'pVal' , ), 3, (3, (), [ (11, 1, None, None) , ], 1 , 4 , 4 , 0 , 48 , (3, 0, None, None) , 64 , )),
	(( u'Updated' , u'pVal' , ), 4, (4, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 52 , (3, 0, None, None) , 0 , )),
	(( u'Updated' , u'pVal' , ), 4, (4, (), [ (11, 1, None, None) , ], 1 , 4 , 4 , 0 , 56 , (3, 0, None, None) , 0 , )),
]

ICustomizationTransitionRules_vtables_dispatch_ = 1
ICustomizationTransitionRules_vtables_ = [
	(( u'Count' , u'pVal' , ), 1, (1, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
	(( u'TransitionRule' , u'Position' , u'pVal' , ), 2, (2, (), [ (3, 1, None, None) , 
			(16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 32 , (3, 0, None, None) , 0 , )),
	(( u'AddTransitionRule' , u'pVal' , ), 3, (3, (), [ (16393, 10, None, None) , ], 1 , 1 , 4 , 0 , 36 , (3, 0, None, None) , 0 , )),
	(( u'RemoveTransitionRule' , u'Rule' , ), 4, (4, (), [ (12, 1, None, None) , ], 1 , 1 , 4 , 0 , 40 , (3, 0, None, None) , 0 , )),
	(( u'Updated' , u'pVal' , ), 5, (5, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 44 , (3, 0, None, None) , 0 , )),
	(( u'Updated' , u'pVal' , ), 5, (5, (), [ (11, 1, None, None) , ], 1 , 4 , 4 , 0 , 48 , (3, 0, None, None) , 0 , )),
	(( u'Field' , u'pVal' , ), 6, (6, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 52 , (3, 0, None, None) , 0 , )),
	(( u'Group' , u'pVal' , ), 7, (7, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 56 , (3, 0, None, None) , 0 , )),
	(( u'EntityName' , u'pVal' , ), 8, (8, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 60 , (3, 0, None, None) , 0 , )),
]

ICustomizationUser_vtables_dispatch_ = 1
ICustomizationUser_vtables_ = [
	(( u'Address' , u'pVal' , ), 1, (1, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
	(( u'Address' , u'pVal' , ), 1, (1, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 32 , (3, 0, None, None) , 0 , )),
	(( u'Email' , u'pVal' , ), 2, (2, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 36 , (3, 0, None, None) , 0 , )),
	(( u'Email' , u'pVal' , ), 2, (2, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 40 , (3, 0, None, None) , 0 , )),
	(( u'FullName' , u'pVal' , ), 3, (3, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 44 , (3, 0, None, None) , 0 , )),
	(( u'FullName' , u'pVal' , ), 3, (3, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 48 , (3, 0, None, None) , 0 , )),
	(( u'Phone' , u'pVal' , ), 4, (4, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 52 , (3, 0, None, None) , 0 , )),
	(( u'Phone' , u'pVal' , ), 4, (4, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 56 , (3, 0, None, None) , 0 , )),
	(( u'Name' , u'pVal' , ), 5, (5, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 60 , (3, 0, None, None) , 0 , )),
	(( u'Updated' , u'pVal' , ), 6, (6, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 64 , (3, 0, None, None) , 0 , )),
	(( u'Updated' , u'pVal' , ), 6, (6, (), [ (11, 1, None, None) , ], 1 , 4 , 4 , 0 , 68 , (3, 0, None, None) , 0 , )),
	(( u'RemoveFromGroup' , u'Group' , ), 7, (7, (), [ (12, 1, None, None) , ], 1 , 1 , 4 , 0 , 72 , (3, 0, None, None) , 0 , )),
	(( u'AddToGroup' , u'Group' , ), 8, (8, (), [ (12, 1, None, None) , ], 1 , 1 , 4 , 0 , 76 , (3, 0, None, None) , 0 , )),
	(( u'In_Group' , u'GroupName' , u'pVal' , ), 9, (9, (), [ (12, 1, None, None) , 
			(16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 80 , (3, 0, None, None) , 64 , )),
	(( u'In_Group' , u'GroupName' , u'pVal' , ), 9, (9, (), [ (12, 1, None, None) , 
			(11, 1, None, None) , ], 1 , 4 , 4 , 0 , 84 , (3, 0, None, None) , 64 , )),
	(( u'Deleted' , u'pVal' , ), 10, (10, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 88 , (3, 0, None, None) , 0 , )),
	(( u'Deleted' , u'pVal' , ), 10, (10, (), [ (11, 1, None, None) , ], 1 , 4 , 4 , 0 , 92 , (3, 0, None, None) , 0 , )),
	(( u'GroupsList' , u'pGroupsList' , ), 11, (11, (), [ (16393, 10, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , ], 1 , 1 , 4 , 0 , 96 , (3, 0, None, None) , 0 , )),
	(( u'Password' , ), 12, (12, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 100 , (3, 0, None, None) , 0 , )),
	(( u'InGroup' , u'GroupName' , u'pVal' , ), 13, (13, (), [ (12, 1, None, None) , 
			(16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 104 , (3, 0, None, None) , 0 , )),
	(( u'InGroup' , u'GroupName' , u'pVal' , ), 13, (13, (), [ (12, 1, None, None) , 
			(11, 1, None, None) , ], 1 , 4 , 4 , 0 , 108 , (3, 0, None, None) , 0 , )),
	(( u'DomainAuthentication' , u'pVal' , ), 14, (14, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 112 , (3, 0, None, None) , 0 , )),
	(( u'DomainAuthentication' , u'pVal' , ), 14, (14, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 116 , (3, 0, None, None) , 0 , )),
]

ICustomizationUsers_vtables_dispatch_ = 1
ICustomizationUsers_vtables_ = [
	(( u'User' , u'Name' , u'pVal' , ), 1, (1, (), [ (8, 1, None, None) , 
			(16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
	(( u'AddUser' , u'Name' , u'pVal' , ), 2, (2, (), [ (8, 1, None, None) , 
			(16393, 10, None, None) , ], 1 , 1 , 4 , 0 , 32 , (3, 0, None, None) , 0 , )),
	(( u'Users' , u'pVal' , ), 3, (3, (), [ (16393, 10, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , ], 1 , 2 , 4 , 0 , 36 , (3, 0, None, None) , 0 , )),
	(( u'RemoveUser' , u'Name' , ), 4, (4, (), [ (8, 0, None, None) , ], 1 , 1 , 4 , 0 , 40 , (3, 0, None, None) , 0 , )),
	(( u'DomainUsers' , u'Response' , ), 5, (5, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 44 , (3, 0, None, None) , 64 , )),
	(( u'AddSiteUser' , u'UserName' , u'FullName' , u'Email' , u'Description' , 
			u'Phone' , u'Group' , ), 6, (6, (), [ (8, 1, None, None) , (8, 1, None, None) , 
			(8, 1, None, None) , (8, 1, None, None) , (8, 1, None, None) , (12, 1, None, None) , ], 1 , 1 , 4 , 0 , 48 , (3, 0, None, None) , 0 , )),
	(( u'DomainUsersWithoutProjectUsers' , u'Response' , ), 7, (7, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 52 , (3, 0, None, None) , 64 , )),
	(( u'AddSiteAuthenticatedUser' , u'UserName' , u'FullName' , u'Email' , u'Description' , 
			u'Phone' , u'DomainAuthentication' , u'Group' , ), 8, (8, (), [ (8, 1, None, None) , 
			(8, 1, None, None) , (8, 1, None, None) , (8, 1, None, None) , (8, 1, None, None) , (8, 1, None, None) , 
			(12, 1, None, None) , ], 1 , 1 , 4 , 0 , 56 , (3, 0, None, None) , 0 , )),
]

ICustomizationUsersGroup_vtables_dispatch_ = 1
ICustomizationUsersGroup_vtables_ = [
	(( u'ID' , u'pVal' , ), 1, (1, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
	(( u'Name' , u'pVal' , ), 2, (2, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 32 , (3, 0, None, None) , 0 , )),
	(( u'Name' , u'pVal' , ), 2, (2, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 36 , (3, 0, None, None) , 0 , )),
	(( u'Is_System' , u'pVal' , ), 3, (3, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 40 , (3, 0, None, None) , 64 , )),
	(( u'AddUser' , u'User' , ), 4, (4, (), [ (12, 1, None, None) , ], 1 , 1 , 4 , 0 , 44 , (3, 0, None, None) , 0 , )),
	(( u'RemoveUser' , u'User' , ), 5, (5, (), [ (12, 1, None, None) , ], 1 , 1 , 4 , 0 , 48 , (3, 0, None, None) , 0 , )),
	(( u'UsersList' , u'List' , ), 6, (6, (), [ (16393, 10, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , ], 1 , 1 , 4 , 0 , 52 , (3, 0, None, None) , 0 , )),
	(( u'Updated' , u'pVal' , ), 7, (7, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 56 , (3, 0, None, None) , 0 , )),
	(( u'Updated' , u'pVal' , ), 7, (7, (), [ (11, 1, None, None) , ], 1 , 4 , 4 , 0 , 60 , (3, 0, None, None) , 0 , )),
	(( u'Deleted' , u'pVal' , ), 8, (8, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 64 , (3, 0, None, None) , 0 , )),
	(( u'Deleted' , u'pVal' , ), 8, (8, (), [ (11, 1, None, None) , ], 1 , 4 , 4 , 0 , 68 , (3, 0, None, None) , 0 , )),
	(( u'IsSystem' , u'pVal' , ), 9, (9, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 72 , (3, 0, None, None) , 0 , )),
	(( u'HideFilter' , u'FilterType' , u'pVal' , ), 10, (10, (), [ (8, 1, None, None) , 
			(16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 76 , (3, 0, None, None) , 0 , )),
	(( u'HideFilter' , u'FilterType' , u'pVal' , ), 10, (10, (), [ (8, 1, None, None) , 
			(8, 1, None, None) , ], 1 , 4 , 4 , 0 , 80 , (3, 0, None, None) , 0 , )),
]

ICustomizationUsersGroups_vtables_dispatch_ = 1
ICustomizationUsersGroups_vtables_ = [
	(( u'AddGroup' , u'Name' , u'NewGroup' , ), 1, (1, (), [ (8, 1, None, None) , 
			(16393, 10, None, None) , ], 1 , 1 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
	(( u'Group' , u'Name' , u'pVal' , ), 2, (2, (), [ (8, 1, None, None) , 
			(16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 32 , (3, 0, None, None) , 0 , )),
	(( u'RemoveGroup' , u'Name' , ), 3, (3, (), [ (8, 1, None, None) , ], 1 , 1 , 4 , 0 , 36 , (3, 0, None, None) , 0 , )),
	(( u'Groups' , u'pVal' , ), 4, (4, (), [ (16393, 10, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , ], 1 , 2 , 4 , 0 , 40 , (3, 0, None, None) , 0 , )),
]

IDBManager_vtables_dispatch_ = 1
IDBManager_vtables_ = [
	(( u'CreateDatabase' , u'DBType' , u'DBName' , u'ConnectionString' , ), 1, (1, (), [ 
			(3, 1, None, None) , (8, 1, None, None) , (8, 1, None, None) , ], 1 , 1 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
	(( u'RemoveDatabase' , u'DBName' , ), 2, (2, (), [ (8, 1, None, None) , ], 1 , 1 , 4 , 0 , 32 , (3, 0, None, None) , 0 , )),
	(( u'AdminUserName' , u'pVal' , ), 3, (3, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 36 , (3, 0, None, None) , 0 , )),
	(( u'AdminUserName' , u'pVal' , ), 3, (3, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 40 , (3, 0, None, None) , 0 , )),
	(( u'Domain' , u'pVal' , ), 4, (4, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 44 , (3, 0, None, None) , 0 , )),
	(( u'Domain' , u'pVal' , ), 4, (4, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 48 , (3, 0, None, None) , 0 , )),
	(( u'AdminUserPassword' , u'pVal' , ), 5, (5, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 52 , (3, 0, None, None) , 0 , )),
	(( u'AdminUserPassword' , u'pVal' , ), 5, (5, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 56 , (3, 0, None, None) , 0 , )),
]

IDesignStep_vtables_dispatch_ = 1
IDesignStep_vtables_ = [
	(( u'StepName' , u'pVal' , ), 14, (14, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 92 , (3, 0, None, None) , 0 , )),
	(( u'StepName' , u'pVal' , ), 14, (14, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 96 , (3, 0, None, None) , 0 , )),
	(( u'StepDescription' , u'pVal' , ), 15, (15, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 100 , (3, 0, None, None) , 0 , )),
	(( u'StepDescription' , u'pVal' , ), 15, (15, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 104 , (3, 0, None, None) , 0 , )),
	(( u'StepExpectedResult' , u'pVal' , ), 16, (16, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 108 , (3, 0, None, None) , 0 , )),
	(( u'StepExpectedResult' , u'pVal' , ), 16, (16, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 112 , (3, 0, None, None) , 0 , )),
	(( u'Order' , u'pVal' , ), 17, (17, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 116 , (3, 0, None, None) , 0 , )),
	(( u'Order' , u'pVal' , ), 17, (17, (), [ (3, 1, None, None) , ], 1 , 4 , 4 , 0 , 120 , (3, 0, None, None) , 0 , )),
	(( u'LinkTest' , u'pVal' , ), 18, (18, (), [ (16396, 10, None, None) , ], 1 , 2 , 4 , 0 , 124 , (3, 0, None, None) , 0 , )),
	(( u'LinkTest' , u'pVal' , ), 18, (18, (), [ (12, 1, None, None) , ], 1 , 4 , 4 , 0 , 128 , (3, 0, None, None) , 0 , )),
	(( u'LinkedParams' , u'pVal' , ), 19, (19, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 132 , (3, 0, None, None) , 0 , )),
	(( u'LinkTestID' , u'pVal' , ), 20, (20, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 136 , (3, 0, None, None) , 0 , )),
	(( u'EvaluatedStepDescription' , u'pVal' , ), 21, (21, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 140 , (3, 0, None, None) , 0 , )),
	(( u'EvaluatedStepExpectedResult' , u'pVal' , ), 22, (22, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 144 , (3, 0, None, None) , 0 , )),
	(( u'ParentTest' , u'pVal' , ), 23, (23, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 148 , (3, 0, None, None) , 0 , )),
]

IErrorInfo_vtables_dispatch_ = 0
IErrorInfo_vtables_ = [
	(( u'GetGUID' , u'pGUID' , ), 1610678272, (1610678272, (), [ (36, 2, None, None) , ], 1 , 1 , 4 , 0 , 12 , (3, 0, None, None) , 0 , )),
	(( u'GetSource' , u'pBstrSource' , ), 1610678273, (1610678273, (), [ (16392, 2, None, None) , ], 1 , 1 , 4 , 0 , 16 , (3, 0, None, None) , 0 , )),
	(( u'GetDescription' , u'pBstrDescription' , ), 1610678274, (1610678274, (), [ (16392, 2, None, None) , ], 1 , 1 , 4 , 0 , 20 , (3, 0, None, None) , 0 , )),
	(( u'GetHelpFile' , u'pBstrHelpFile' , ), 1610678275, (1610678275, (), [ (16392, 2, None, None) , ], 1 , 1 , 4 , 0 , 24 , (3, 0, None, None) , 0 , )),
	(( u'GetHelpContext' , u'pdwHelpContext' , ), 1610678276, (1610678276, (), [ (16403, 2, None, None) , ], 1 , 1 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
]

IExecEventInfo_vtables_dispatch_ = 1
IExecEventInfo_vtables_ = [
	(( u'EventType' , u'pVal' , ), 1, (1, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
	(( u'EventTime' , u'pVal' , ), 2, (2, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 32 , (3, 0, None, None) , 0 , )),
	(( u'EventDate' , u'pVal' , ), 3, (3, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 36 , (3, 0, None, None) , 0 , )),
	(( u'EventParam' , u'ParamName' , u'pVal' , ), 4, (4, (), [ (8, 1, None, None) , 
			(16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 40 , (3, 0, None, None) , 64 , )),
]

IExecEventNotifyByMailSettings_vtables_dispatch_ = 1
IExecEventNotifyByMailSettings_vtables_ = [
	(( u'EMailTo' , u'pVal' , ), 1, (1, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
	(( u'EMailTo' , u'pVal' , ), 1, (1, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 32 , (3, 0, None, None) , 0 , )),
	(( u'UserMessage' , u'pVal' , ), 2, (2, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 36 , (3, 0, None, None) , 0 , )),
	(( u'UserMessage' , u'pVal' , ), 2, (2, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 40 , (3, 0, None, None) , 0 , )),
	(( u'Enabled' , u'EventType' , u'pVal' , ), 3, (3, (), [ (3, 1, None, None) , 
			(16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 44 , (3, 0, None, None) , 0 , )),
	(( u'Enabled' , u'EventType' , u'pVal' , ), 3, (3, (), [ (3, 1, None, None) , 
			(11, 1, None, None) , ], 1 , 4 , 4 , 0 , 48 , (3, 0, None, None) , 0 , )),
	(( u'Save' , u'AutoPost' , ), 4, (4, (), [ (11, 49, 'True', None) , ], 1 , 1 , 4 , 0 , 52 , (3, 0, None, None) , 0 , )),
]

IExecutionSettings_vtables_dispatch_ = 1
IExecutionSettings_vtables_ = [
	(( u'PlannedExecutionTime' , u'vVal' , ), 1, (1, (), [ (12, 1, None, None) , ], 1 , 4 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
	(( u'PlannedExecutionTime' , u'vVal' , ), 1, (1, (), [ (16396, 10, None, None) , ], 1 , 2 , 4 , 0 , 32 , (3, 0, None, None) , 0 , )),
	(( u'PlannedExecutionDate' , u'vVal' , ), 2, (2, (), [ (12, 1, None, None) , ], 1 , 4 , 4 , 0 , 36 , (3, 0, None, None) , 0 , )),
	(( u'PlannedExecutionDate' , u'vVal' , ), 2, (2, (), [ (16396, 10, None, None) , ], 1 , 2 , 4 , 0 , 40 , (3, 0, None, None) , 0 , )),
	(( u'PlannedRunDuration' , u'vVal' , ), 3, (3, (), [ (12, 1, None, None) , ], 1 , 4 , 4 , 0 , 44 , (3, 0, None, None) , 0 , )),
	(( u'PlannedRunDuration' , u'vVal' , ), 3, (3, (), [ (16396, 10, None, None) , ], 1 , 2 , 4 , 0 , 48 , (3, 0, None, None) , 0 , )),
	(( u'OnExecEventSchedulerActionType' , u'EventType' , u'pVal' , ), 4, (4, (), [ (3, 1, None, None) , 
			(16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 52 , (3, 0, None, None) , 0 , )),
	(( u'OnExecEventSchedulerActionType' , u'EventType' , u'pVal' , ), 4, (4, (), [ (3, 1, None, None) , 
			(3, 1, None, None) , ], 1 , 4 , 4 , 0 , 56 , (3, 0, None, None) , 0 , )),
	(( u'OnExecEventSchedulerActionParams' , u'EventType' , u'ppVal' , ), 5, (5, (), [ (3, 1, None, None) , 
			(16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 60 , (3, 0, None, None) , 0 , )),
]

IExecutionStatus_vtables_dispatch_ = 1
IExecutionStatus_vtables_ = [
	(( u'Count' , u'pcount' , ), 1, (1, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
	(( u'RefreshExecStatusInfo' , u'TestData' , u'Force' , ), 2, (2, (), [ (12, 1, None, None) , 
			(11, 1, None, None) , ], 1 , 1 , 4 , 0 , 32 , (3, 0, None, None) , 0 , )),
	(( u'Finished' , u'pVal' , ), 3, (3, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 36 , (3, 0, None, None) , 0 , )),
	(( u'EventsList' , u'pVal' , ), 4, (4, (), [ (16393, 10, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , ], 1 , 1 , 4 , 0 , 40 , (3, 0, None, None) , 0 , )),
	(( u'Item' , u'Index' , u'pVal' , ), 0, (0, (), [ (3, 0, None, None) , 
			(16396, 10, None, None) , ], 1 , 2 , 4 , 0 , 44 , (3, 0, None, None) , 0 , )),
	(( u'_NewEnum' , u'pVal' , ), -4, (-4, (), [ (16397, 10, None, None) , ], 1 , 2 , 4 , 0 , 48 , (3, 0, None, None) , 0 , )),
]

IExport_vtables_dispatch_ = 1
IExport_vtables_ = [
	(( u'ExportData' , u'XMLData' , u'ResourceID' , ), 1, (1, (), [ (8, 0, None, None) , 
			(8, 0, None, None) , ], 1 , 1 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
]

IExtendedStorage_vtables_dispatch_ = 1
IExtendedStorage_vtables_ = [
	(( u'Root' , u'pVal' , ), 0, (0, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
	(( u'Load' , u'FSysFilter' , u'synchronize' , u'RootPath' , ), 1, (1, (), [ 
			(8, 49, "u'*.*'", None) , (11, 49, 'False', None) , (16392, 10, None, None) , ], 1 , 1 , 4 , 0 , 32 , (3, 32, None, None) , 0 , )),
	(( u'Save' , u'FSysFilter' , u'synchronize' , ), 2, (2, (), [ (8, 49, "u'*.*'", None) , 
			(11, 49, 'False', None) , ], 1 , 1 , 4 , 0 , 36 , (3, 32, None, None) , 0 , )),
	(( u'Delete' , u'FSysFilter' , u'nDeleteType' , ), 3, (3, (), [ (8, 1, None, None) , 
			(3, 1, None, None) , ], 1 , 1 , 4 , 0 , 40 , (3, 0, None, None) , 0 , )),
	(( u'ServerPath' , u'pVal' , ), 4, (4, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 44 , (3, 0, None, None) , 0 , )),
	(( u'ServerPath' , u'pVal' , ), 4, (4, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 48 , (3, 0, None, None) , 0 , )),
	(( u'ClientPath' , u'pVal' , ), 5, (5, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 52 , (3, 0, None, None) , 0 , )),
	(( u'ClientPath' , u'pVal' , ), 5, (5, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 56 , (3, 0, None, None) , 0 , )),
	(( u'Cancel' , ), 6, (6, (), [ ], 1 , 1 , 4 , 0 , 60 , (3, 0, None, None) , 0 , )),
	(( u'ActionFinished' , u'pVal' , ), 7, (7, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 64 , (3, 0, None, None) , 0 , )),
	(( u'Progress' , u'Total' , u'Current' , u'Files' , ), 8, (8, (), [ 
			(16387, 2, None, None) , (16387, 2, None, None) , (16392, 10, None, None) , ], 1 , 1 , 4 , 0 , 68 , (3, 0, None, None) , 0 , )),
	(( u'GetLastError' , ), 9, (9, (), [ ], 1 , 1 , 4 , 0 , 72 , (3, 0, None, None) , 0 , )),
	(( u'SaveEx' , u'FSysFilter' , u'synchronize' , u'pVal' , u'pNonFatalErrorOccured' , 
			), 10, (10, (), [ (8, 1, None, None) , (11, 1, None, None) , (16393, 2, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , (16395, 10, None, None) , ], 1 , 1 , 4 , 0 , 76 , (3, 0, None, None) , 0 , )),
	(( u'LoadEx' , u'FSysFilter' , u'synchronize' , u'pVal' , u'pNonFatalErrorOccured' , 
			u'RootPath' , ), 11, (11, (), [ (8, 1, None, None) , (11, 1, None, None) , (16393, 2, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , 
			(16395, 2, None, None) , (16392, 10, None, None) , ], 1 , 1 , 4 , 0 , 80 , (3, 0, None, None) , 0 , )),
]

IFactoryList_vtables_dispatch_ = 1
IFactoryList_vtables_ = [
	(( u'Fields' , u'pFields' , ), 7, (7, (), [ (16393, 10, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , ], 1 , 2 , 4 , 0 , 56 , (3, 0, None, None) , 0 , )),
	(( u'Refresh' , ), 8, (8, (), [ ], 1 , 1 , 4 , 0 , 60 , (3, 0, None, None) , 0 , )),
	(( u'Post' , ), 9, (9, (), [ ], 1 , 1 , 4 , 0 , 64 , (3, 0, None, None) , 0 , )),
	(( u'Ratio' , u'Index' , u'pVal' , ), 10, (10, (), [ (3, 1, None, None) , 
			(16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 68 , (3, 0, None, None) , 0 , )),
	(( u'IndexOfItem' , u'Item' , u'Index' , ), 11, (11, (), [ (12, 0, None, None) , 
			(16387, 10, None, None) , ], 1 , 1 , 4 , 0 , 72 , (3, 0, None, None) , 0 , )),
]

IFieldProperty_vtables_dispatch_ = 1
IFieldProperty_vtables_ = [
	(( u'UserLabel' , u'pVal' , ), 0, (0, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
	(( u'DBTableName' , u'pVal' , ), 1, (1, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 32 , (3, 0, None, None) , 0 , )),
	(( u'DBColumnName' , u'pVal' , ), 2, (2, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 36 , (3, 0, None, None) , 0 , )),
	(( u'DBColumnType' , u'pVal' , ), 3, (3, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 40 , (3, 0, None, None) , 0 , )),
	(( u'EditStyle' , u'pVal' , ), 4, (4, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 44 , (3, 0, None, None) , 0 , )),
	(( u'EditMask' , u'pVal' , ), 5, (5, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 48 , (3, 0, None, None) , 0 , )),
	(( u'IsSystem' , u'pVal' , ), 6, (6, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 52 , (3, 0, None, None) , 0 , )),
	(( u'IsCanFilter' , u'pVal' , ), 7, (7, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 56 , (3, 0, None, None) , 0 , )),
	(( u'IsKey' , u'pVal' , ), 8, (8, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 60 , (3, 0, None, None) , 0 , )),
	(( u'KeyOrder' , u'pVal' , ), 9, (9, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 64 , (3, 0, None, None) , 64 , )),
	(( u'IsEdit' , u'pVal' , ), 10, (10, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 68 , (3, 0, None, None) , 0 , )),
	(( u'IsActive' , u'pVal' , ), 11, (11, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 72 , (3, 0, None, None) , 0 , )),
	(( u'IsHistory' , u'pVal' , ), 12, (12, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 76 , (3, 0, None, None) , 0 , )),
	(( u'IsMail' , u'pVal' , ), 13, (13, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 80 , (3, 0, None, None) , 0 , )),
	(( u'IsVerify' , u'pVal' , ), 14, (14, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 84 , (3, 0, None, None) , 0 , )),
	(( u'Root' , u'pVal' , ), 15, (15, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 88 , (3, 0, None, None) , 0 , )),
	(( u'IsByCode' , u'pVal' , ), 16, (16, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 92 , (3, 0, None, None) , 0 , )),
	(( u'IsRequired' , u'pVal' , ), 17, (17, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 96 , (3, 0, None, None) , 0 , )),
	(( u'UserColumnType' , u'pVal' , ), 18, (18, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 100 , (3, 0, None, None) , 0 , )),
	(( u'IsKeepValue' , u'pVal' , ), 19, (19, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 104 , (3, 0, None, None) , 0 , )),
	(( u'IsCustomizable' , u'pVal' , ), 20, (20, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 108 , (3, 0, None, None) , 0 , )),
	(( u'FieldSize' , u'pVal' , ), 21, (21, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 112 , (3, 0, None, None) , 0 , )),
	(( u'IsVisibleInNewBug' , u'pVal' , ), 22, (22, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 116 , (3, 0, None, None) , 0 , )),
	(( u'ReadOnly' , u'pVal' , ), 23, (23, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 120 , (3, 0, None, None) , 0 , )),
	(( u'IsToSum' , u'pVal' , ), 24, (24, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 124 , (3, 0, None, None) , 0 , )),
	(( u'IsModify' , u'pVal' , ), 25, (25, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 128 , (3, 0, None, None) , 0 , )),
	(( u'IsVersionControl' , u'pVal' , ), 26, (26, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 132 , (3, 0, None, None) , 0 , )),
	(( u'IsVisible' , u'pVal' , ), 27, (27, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 136 , (3, 0, None, None) , 64 , )),
]

IFieldProperty2_vtables_dispatch_ = 1
IFieldProperty2_vtables_ = [
	(( u'IsCanGroup' , u'pVal' , ), 28, (28, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 140 , (3, 0, None, None) , 0 , )),
	(( u'IsSearchable' , u'pVal' , ), 29, (29, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 144 , (3, 0, None, None) , 0 , )),
	(( u'IsMultiValue' , u'pVal' , ), 30, (30, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 148 , (3, 0, None, None) , 0 , )),
]

IFileData_vtables_dispatch_ = 1
IFileData_vtables_ = [
	(( u'Name' , u'pVal' , ), 0, (0, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
	(( u'Type' , u'pVal' , ), 1, (1, (), [ (16386, 10, None, None) , ], 1 , 1 , 4 , 0 , 32 , (3, 0, None, None) , 0 , )),
	(( u'Size' , u'pVal' , ), 2, (2, (), [ (16387, 10, None, None) , ], 1 , 1 , 4 , 0 , 36 , (3, 0, None, None) , 0 , )),
	(( u'ModifyDate' , u'pVal' , ), 3, (3, (), [ (16391, 10, None, None) , ], 1 , 1 , 4 , 0 , 40 , (3, 0, None, None) , 0 , )),
	(( u'Items' , u'pVal' , ), 4, (4, (), [ (16393, 10, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , ], 1 , 1 , 4 , 0 , 44 , (3, 0, None, None) , 0 , )),
]

IFollowUpManager_vtables_dispatch_ = 0
IFollowUpManager_vtables_ = [
	(( u'GetFollowUp' , u'FollowUpDate' , u'Description' , ), 1610678272, (1610678272, (), [ (16391, 2, None, None) , 
			(16392, 2, None, None) , ], 1 , 1 , 4 , 0 , 12 , (3, 0, None, None) , 0 , )),
	(( u'SetFollowUp' , u'FollowUpDate' , u'Description' , ), 1610678273, (1610678273, (), [ (7, 1, None, None) , 
			(8, 1, None, None) , ], 1 , 1 , 4 , 0 , 16 , (3, 0, None, None) , 0 , )),
	(( u'CancelFollowUp' , ), 1610678274, (1610678274, (), [ ], 1 , 1 , 4 , 0 , 20 , (3, 0, None, None) , 0 , )),
	(( u'HasFollowUp' , u'pVal' , ), 1610678275, (1610678275, (), [ (16395, 10, None, None) , ], 1 , 1 , 4 , 0 , 24 , (3, 0, None, None) , 0 , )),
	(( u'IsFollowUpOverdue' , u'pVal' , ), 1610678276, (1610678276, (), [ (16395, 10, None, None) , ], 1 , 1 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
]

IGraph_vtables_dispatch_ = 1
IGraph_vtables_ = [
	(( u'ColName' , u'Col' , u'pVal' , ), 1, (1, (), [ (3, 1, None, None) , 
			(16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
	(( u'RowName' , u'Row' , u'pVal' , ), 2, (2, (), [ (3, 1, None, None) , 
			(16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 32 , (3, 0, None, None) , 0 , )),
	(( u'ColType' , u'Col' , u'pVal' , ), 3, (3, (), [ (3, 1, None, None) , 
			(16386, 10, None, None) , ], 1 , 2 , 4 , 0 , 36 , (3, 0, None, None) , 64 , )),
	(( u'Data' , u'Col' , u'Row' , u'pVal' , ), 4, (4, (), [ 
			(3, 1, None, None) , (3, 1, None, None) , (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 40 , (3, 0, None, None) , 0 , )),
	(( u'ColCount' , u'pVal' , ), 5, (5, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 44 , (3, 0, None, None) , 0 , )),
	(( u'RowCount' , u'pVal' , ), 6, (6, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 48 , (3, 0, None, None) , 0 , )),
	(( u'DrillDown' , u'pAreas' , u'mAreas' , u'pList' , ), 7, (7, (), [ 
			(12, 1, None, None) , (12, 1, None, None) , (16393, 10, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , ], 1 , 1 , 4 , 0 , 52 , (3, 0, None, None) , 0 , )),
	(( u'MaxValue' , u'pVal' , ), 8, (8, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 56 , (3, 0, None, None) , 0 , )),
	(( u'GraphTotal' , u'pVal' , ), 9, (9, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 60 , (3, 0, None, None) , 0 , )),
	(( u'RowTotal' , u'Row' , u'pVal' , ), 10, (10, (), [ (3, 1, None, None) , 
			(16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 64 , (3, 0, None, None) , 0 , )),
	(( u'ColTotal' , u'Col' , u'pVal' , ), 11, (11, (), [ (3, 1, None, None) , 
			(16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 68 , (3, 0, None, None) , 0 , )),
	(( u'MultiDrillDown' , u'Areas' , u'pList' , ), 12, (12, (), [ (12, 1, None, None) , 
			(16393, 10, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , ], 1 , 1 , 4 , 0 , 72 , (3, 0, None, None) , 0 , )),
	(( u'StartDate' , u'pVal' , ), 13, (13, (), [ (16391, 10, None, None) , ], 1 , 2 , 4 , 0 , 76 , (3, 0, None, None) , 0 , )),
]

IGraphBuilder_vtables_dispatch_ = 1
IGraphBuilder_vtables_ = [
	(( u'CreateGraphDefinition' , u'Module' , u'GraphType' , u'pGraphDef' , ), 1, (1, (), [ 
			(3, 1, None, None) , (3, 1, None, None) , (16393, 10, None, None) , ], 1 , 1 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
	(( u'BuildGraph' , u'pGraphDef' , u'pGraph' , ), 2, (2, (), [ (9, 1, None, None) , 
			(16393, 10, None, None) , ], 1 , 1 , 4 , 0 , 32 , (3, 0, None, None) , 0 , )),
	(( u'BuildMultipleGraphs' , u'GraphDefs' , u'pGraphs' , ), 3, (3, (), [ (12, 1, None, None) , 
			(16393, 10, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , ], 1 , 1 , 4 , 0 , 36 , (3, 0, None, None) , 0 , )),
]

IGraphDefinition_vtables_dispatch_ = 1
IGraphDefinition_vtables_ = [
	(( u'Module' , u'pModule' , ), 1, (1, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
	(( u'Type' , u'pType' , ), 2, (2, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 32 , (3, 0, None, None) , 0 , )),
	(( u'Property' , u'Prop' , u'pVal' , ), 3, (3, (), [ (3, 1, None, None) , 
			(16396, 10, None, None) , ], 1 , 2 , 4 , 0 , 36 , (3, 0, None, None) , 0 , )),
	(( u'Property' , u'Prop' , u'pVal' , ), 3, (3, (), [ (3, 1, None, None) , 
			(12, 1, None, None) , ], 1 , 4 , 4 , 0 , 40 , (3, 0, None, None) , 0 , )),
	(( u'Filter' , u'pFilter' , ), 4, (4, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 44 , (3, 0, None, None) , 0 , )),
	(( u'Filter' , u'pFilter' , ), 4, (4, (), [ (9, 1, None, None) , ], 1 , 4 , 4 , 0 , 48 , (3, 0, None, None) , 0 , )),
	(( u'Text' , u'pVal' , ), 5, (5, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 52 , (3, 0, None, None) , 0 , )),
]

IGroupingItem_vtables_dispatch_ = 1
IGroupingItem_vtables_ = [
	(( u'FieldName' , u'pVal' , ), 1, (1, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
	(( u'FieldValue' , u'pVal' , ), 2, (2, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 32 , (3, 0, None, None) , 0 , )),
	(( u'ItemCount' , u'pVal' , ), 3, (3, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 36 , (3, 0, None, None) , 0 , )),
	(( u'ItemList' , u'pVal' , ), 4, (4, (), [ (16393, 10, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , ], 1 , 2 , 4 , 0 , 40 , (3, 0, None, None) , 0 , )),
	(( u'GroupList' , u'pVal' , ), 5, (5, (), [ (16393, 10, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , ], 1 , 2 , 4 , 0 , 44 , (3, 0, None, None) , 0 , )),
]

IGroupingManager_vtables_dispatch_ = 1
IGroupingManager_vtables_ = [
	(( u'Filter' , u'pVal' , ), 1, (1, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
	(( u'Filter' , u'pVal' , ), 1, (1, (), [ (9, 1, None, None) , ], 1 , 4 , 4 , 0 , 32 , (3, 0, None, None) , 0 , )),
	(( u'Group' , u'Name' , u'pVal' , ), 2, (2, (), [ (8, 1, None, None) , 
			(16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 36 , (3, 0, None, None) , 0 , )),
	(( u'Group' , u'Name' , u'pVal' , ), 2, (2, (), [ (8, 1, None, None) , 
			(3, 1, None, None) , ], 1 , 4 , 4 , 0 , 40 , (3, 0, None, None) , 0 , )),
	(( u'GroupList' , u'pList' , ), 3, (3, (), [ (16393, 10, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , ], 1 , 1 , 4 , 0 , 44 , (3, 0, None, None) , 0 , )),
	(( u'ItemList' , u'pList' , ), 4, (4, (), [ (16393, 10, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , ], 1 , 1 , 4 , 0 , 48 , (3, 0, None, None) , 0 , )),
	(( u'Refresh' , ), 5, (5, (), [ ], 1 , 1 , 4 , 0 , 52 , (3, 0, None, None) , 0 , )),
	(( u'Clear' , ), 6, (6, (), [ ], 1 , 1 , 4 , 0 , 56 , (3, 0, None, None) , 0 , )),
	(( u'IsClear' , u'pbResult' , ), 7, (7, (), [ (16395, 10, None, None) , ], 1 , 1 , 4 , 0 , 60 , (3, 0, None, None) , 0 , )),
	(( u'Text' , u'pVal' , ), 8, (8, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 64 , (3, 0, None, None) , 0 , )),
	(( u'Text' , u'pVal' , ), 8, (8, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 68 , (3, 0, None, None) , 0 , )),
]

IHierarchyFilter_vtables_dispatch_ = 1
IHierarchyFilter_vtables_ = [
	(( u'KeepHierarchical' , u'pVal' , ), 16, (16, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 124 , (3, 0, None, None) , 0 , )),
	(( u'KeepHierarchical' , u'pVal' , ), 16, (16, (), [ (11, 1, None, None) , ], 1 , 4 , 4 , 0 , 128 , (3, 0, None, None) , 0 , )),
]

IHierarchySupportList_vtables_dispatch_ = 1
IHierarchySupportList_vtables_ = [
	(( u'IsInFilter' , u'Index' , u'pVal' , ), 12, (12, (), [ (3, 1, None, None) , 
			(16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 76 , (3, 0, None, None) , 0 , )),
]

IHistory_vtables_dispatch_ = 1
IHistory_vtables_ = [
	(( u'NewList' , u'Filter' , u'pList' , ), 1, (1, (), [ (8, 0, None, None) , 
			(16393, 10, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , ], 1 , 1 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
	(( u'Filter' , u'pVal' , ), 2, (2, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 32 , (3, 0, None, None) , 0 , )),
	(( u'ClearHistory' , u'Filter' , ), 3, (3, (), [ (8, 49, "u''", None) , ], 1 , 1 , 4 , 0 , 36 , (3, 32, None, None) , 0 , )),
]

IHistoryRecord_vtables_dispatch_ = 1
IHistoryRecord_vtables_ = [
	(( u'FieldName' , u'pVal' , ), 1, (1, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
	(( u'ChangeDate' , u'pVal' , ), 2, (2, (), [ (16391, 10, None, None) , ], 1 , 2 , 4 , 0 , 32 , (3, 0, None, None) , 0 , )),
	(( u'Changer' , u'pVal' , ), 3, (3, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 36 , (3, 0, None, None) , 0 , )),
	(( u'NewValue' , u'pVal' , ), 4, (4, (), [ (16396, 10, None, None) , ], 1 , 2 , 4 , 0 , 40 , (3, 0, None, None) , 0 , )),
	(( u'ItemKey' , u'pVal' , ), 5, (5, (), [ (16396, 10, None, None) , ], 1 , 2 , 4 , 0 , 44 , (3, 0, None, None) , 0 , )),
]

IHistoryRecord2_vtables_dispatch_ = 1
IHistoryRecord2_vtables_ = [
	(( u'OldValue' , u'pVal' , ), 6, (6, (), [ (16396, 10, None, None) , ], 1 , 2 , 4 , 0 , 48 , (3, 0, None, None) , 0 , )),
]

IHost_vtables_dispatch_ = 1
IHost_vtables_ = [
	(( u'Description' , u'pVal' , ), 11, (11, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 80 , (3, 0, None, None) , 0 , )),
	(( u'Description' , u'pVal' , ), 11, (11, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 84 , (3, 0, None, None) , 0 , )),
	(( u'RexServer' , u'pVal' , ), 12, (12, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 88 , (3, 0, None, None) , 0 , )),
	(( u'Name' , u'pVal' , ), 13, (13, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 92 , (3, 0, None, None) , 0 , )),
]

IHostGroup_vtables_dispatch_ = 1
IHostGroup_vtables_ = [
	(( u'Name' , u'pVal' , ), 14, (14, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 92 , (3, 0, None, None) , 0 , )),
	(( u'AddHost' , u'Val' , ), 15, (15, (), [ (12, 1, None, None) , ], 1 , 1 , 4 , 0 , 96 , (3, 0, None, None) , 0 , )),
	(( u'RemoveHost' , u'Val' , ), 16, (16, (), [ (12, 1, None, None) , ], 1 , 1 , 4 , 0 , 100 , (3, 0, None, None) , 0 , )),
	(( u'NewList' , u'pVal' , ), 17, (17, (), [ (16393, 10, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , ], 1 , 1 , 4 , 0 , 104 , (3, 0, None, None) , 0 , )),
]

IHostGroupFactory_vtables_dispatch_ = 1
IHostGroupFactory_vtables_ = [
	(( u'RemoveHost' , u'Host' , ), 8, (8, (), [ (12, 1, None, None) , ], 1 , 1 , 4 , 0 , 64 , (3, 0, None, None) , 0 , )),
]

IImport_vtables_dispatch_ = 1
IImport_vtables_ = [
	(( u'GetColumnNames' , u'bstrpColumns' , u'bstrResourceeName' , ), 1, (1, (), [ (16392, 2, None, None) , 
			(8, 1, None, None) , ], 1 , 1 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
	(( u'ImportData' , u'bstrColumnNames' , u'bstrpData' , ), 2, (2, (), [ (8, 0, None, None) , 
			(16392, 0, None, None) , ], 1 , 1 , 4 , 0 , 32 , (3, 0, None, None) , 0 , )),
]

ILink_vtables_dispatch_ = 1
ILink_vtables_ = [
	(( u'SourceEntity' , u'pVal' , ), 11, (11, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 80 , (3, 0, None, None) , 0 , )),
	(( u'TargetEntity' , u'pVal' , ), 12, (12, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 84 , (3, 0, None, None) , 0 , )),
	(( u'TargetEntity' , u'pVal' , ), 12, (12, (), [ (9, 1, None, None) , ], 1 , 4 , 4 , 0 , 88 , (3, 0, None, None) , 0 , )),
	(( u'LinkedByEntity' , u'pVal' , ), 13, (13, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 92 , (3, 0, None, None) , 0 , )),
	(( u'CreatedBy' , u'pVal' , ), 14, (14, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 96 , (3, 0, None, None) , 0 , )),
	(( u'CreatedBy' , u'pVal' , ), 14, (14, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 100 , (3, 0, None, None) , 0 , )),
	(( u'CreationDate' , u'pVal' , ), 15, (15, (), [ (16391, 10, None, None) , ], 1 , 2 , 4 , 0 , 104 , (3, 0, None, None) , 0 , )),
	(( u'CreationDate' , u'pVal' , ), 15, (15, (), [ (7, 1, None, None) , ], 1 , 4 , 4 , 0 , 108 , (3, 0, None, None) , 0 , )),
	(( u'LinkType' , u'pVal' , ), 16, (16, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 112 , (3, 0, None, None) , 0 , )),
	(( u'LinkType' , u'pVal' , ), 16, (16, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 116 , (3, 0, None, None) , 0 , )),
	(( u'Comment' , u'pVal' , ), 17, (17, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 120 , (3, 0, None, None) , 0 , )),
	(( u'Comment' , u'pVal' , ), 17, (17, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 124 , (3, 0, None, None) , 0 , )),
]

ILinkFactory_vtables_dispatch_ = 1
ILinkFactory_vtables_ = [
	(( u'Owner' , u'pVal' , ), 8, (8, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 64 , (3, 0, None, None) , 64 , )),
	(( u'FullLinkage' , u'pVal' , ), 9, (9, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 68 , (3, 0, None, None) , 0 , )),
	(( u'FullLinkage' , u'pVal' , ), 9, (9, (), [ (11, 1, None, None) , ], 1 , 4 , 4 , 0 , 72 , (3, 0, None, None) , 0 , )),
]

ILinkable_vtables_dispatch_ = 1
ILinkable_vtables_ = [
	(( u'BugLinkFactory' , u'pVal' , ), 1, (1, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
	(( u'LinkFactory' , u'pVal' , ), 2, (2, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 32 , (3, 0, None, None) , 0 , )),
	(( u'HasLinkage' , u'pVal' , ), 3, (3, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 36 , (3, 0, None, None) , 0 , )),
	(( u'HasOthersLinkage' , u'pVal' , ), 4, (4, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 40 , (3, 0, None, None) , 0 , )),
]

IList_vtables_dispatch_ = 1
IList_vtables_ = [
	(( u'Count' , u'pcount' , ), 1, (1, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
	(( u'Add' , u'vNew' , ), 2, (2, (), [ (12, 0, None, None) , ], 1 , 1 , 4 , 0 , 32 , (3, 0, None, None) , 0 , )),
	(( u'Remove' , u'Index' , ), 3, (3, (), [ (3, 0, None, None) , ], 1 , 1 , 4 , 0 , 36 , (3, 0, None, None) , 0 , )),
	(( u'Item' , u'Index' , u'pVal' , ), 0, (0, (), [ (3, 0, None, None) , 
			(16396, 10, None, None) , ], 1 , 2 , 4 , 0 , 40 , (3, 0, None, None) , 0 , )),
	(( u'_NewEnum' , u'pVal' , ), -4, (-4, (), [ (16397, 10, None, None) , ], 1 , 2 , 4 , 0 , 44 , (3, 0, None, None) , 0 , )),
	(( u'Insert' , u'Pos' , u'vNew' , ), 5, (5, (), [ (3, 0, None, None) , 
			(12, 0, None, None) , ], 1 , 1 , 4 , 0 , 48 , (3, 0, None, None) , 0 , )),
	(( u'Swap' , u'Pos1' , u'Pos2' , ), 6, (6, (), [ (3, 0, None, None) , 
			(3, 0, None, None) , ], 1 , 1 , 4 , 0 , 52 , (3, 0, None, None) , 0 , )),
]

IMultiValue_vtables_dispatch_ = 1
IMultiValue_vtables_ = [
	(( u'Text' , u'pVal' , ), 1, (1, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
	(( u'Text' , u'pVal' , ), 1, (1, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 32 , (3, 0, None, None) , 0 , )),
	(( u'List' , u'pVal' , ), 2, (2, (), [ (16393, 10, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , ], 1 , 2 , 4 , 0 , 36 , (3, 0, None, None) , 0 , )),
	(( u'List' , u'pVal' , ), 2, (2, (), [ (9, 1, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , ], 1 , 4 , 4 , 0 , 40 , (3, 0, None, None) , 0 , )),
]

IObjectLockingSupport_vtables_dispatch_ = 1
IObjectLockingSupport_vtables_ = [
	(( u'IsLocked' , u'pVal' , ), 1, (1, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
	(( u'LockObject' , u'pVal' , ), 2, (2, (), [ (16395, 10, None, None) , ], 1 , 1 , 4 , 0 , 32 , (3, 0, None, None) , 0 , )),
	(( u'UnLockObject' , ), 3, (3, (), [ ], 1 , 1 , 4 , 0 , 36 , (3, 0, None, None) , 0 , )),
]

IOnExecEventSchedulerActionParams_vtables_dispatch_ = 1
IOnExecEventSchedulerActionParams_vtables_ = [
	(( u'OnExecEventSchedulerAction' , u'pVal' , ), 1, (1, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
	(( u'Parameter' , u'Index' , u'vVal' , ), 2, (2, (), [ (3, 1, None, None) , 
			(12, 1, None, None) , ], 1 , 4 , 4 , 0 , 32 , (3, 0, None, None) , 64 , )),
	(( u'Parameter' , u'Index' , u'vVal' , ), 2, (2, (), [ (3, 1, None, None) , 
			(16396, 10, None, None) , ], 1 , 2 , 4 , 0 , 36 , (3, 0, None, None) , 64 , )),
]

IOnExecEventSchedulerRestartParams_vtables_dispatch_ = 1
IOnExecEventSchedulerRestartParams_vtables_ = [
	(( u'NumberOfRetries' , u'Val' , ), 3, (3, (), [ (3, 1, None, None) , ], 1 , 4 , 4 , 0 , 40 , (3, 0, None, None) , 0 , )),
	(( u'NumberOfRetries' , u'Val' , ), 3, (3, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 44 , (3, 0, None, None) , 0 , )),
	(( u'CleanupTest' , u'vVal' , ), 4, (4, (), [ (12, 1, None, None) , ], 1 , 4 , 4 , 0 , 48 , (3, 0, None, None) , 0 , )),
	(( u'CleanupTest' , u'vVal' , ), 4, (4, (), [ (16396, 10, None, None) , ], 1 , 2 , 4 , 0 , 52 , (3, 0, None, None) , 0 , )),
]

IParam_vtables_dispatch_ = 1
IParam_vtables_ = [
	(( u'Count' , u'pVal' , ), 1, (1, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
	(( u'ParamValue' , u'Key' , u'pVal' , ), 2, (2, (), [ (12, 0, None, None) , 
			(16396, 10, None, None) , ], 1 , 2 , 4 , 0 , 32 , (3, 0, None, None) , 0 , )),
	(( u'ParamValue' , u'Key' , u'pVal' , ), 2, (2, (), [ (12, 0, None, None) , 
			(12, 1, None, None) , ], 1 , 4 , 4 , 0 , 36 , (3, 0, None, None) , 0 , )),
	(( u'ParamName' , u'Index' , u'pVal' , ), 3, (3, (), [ (3, 0, None, None) , 
			(16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 40 , (3, 0, None, None) , 0 , )),
	(( u'ParamIndex' , u'Name' , u'pVal' , ), 4, (4, (), [ (8, 0, None, None) , 
			(16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 44 , (3, 0, None, None) , 0 , )),
	(( u'AddParam' , u'Name' , u'InitialValue' , ), 5, (5, (), [ (8, 0, None, None) , 
			(12, 0, None, None) , ], 1 , 1 , 4 , 0 , 48 , (3, 0, None, None) , 0 , )),
	(( u'DeleteParam' , u'Key' , ), 6, (6, (), [ (12, 0, None, None) , ], 1 , 1 , 4 , 0 , 52 , (3, 0, None, None) , 0 , )),
	(( u'DeleteParams' , ), 7, (7, (), [ ], 1 , 1 , 4 , 0 , 56 , (3, 0, None, None) , 0 , )),
	(( u'ParamType' , u'Index' , u'pVal' , ), 8, (8, (), [ (3, 0, None, None) , 
			(16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 60 , (3, 0, None, None) , 0 , )),
]

IProductInfo_vtables_dispatch_ = 1
IProductInfo_vtables_ = [
	(( u'QCVersion' , u'pnMajorVersion' , u'pnMinorVersion' , u'pnBuildNum' , ), 1, (1, (), [ 
			(16387, 2, None, None) , (16387, 2, None, None) , (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
	(( u'BPTVersion' , u'pnBPTVersion' , ), 2, (2, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 32 , (3, 0, None, None) , 0 , )),
]

IProjectProperties_vtables_dispatch_ = 1
IProjectProperties_vtables_ = [
	(( u'Count' , u'pVal' , ), 1, (1, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
	(( u'ParamValue' , u'vParam' , u'pVal' , ), 2, (2, (), [ (12, 1, None, None) , 
			(16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 32 , (3, 0, None, None) , 0 , )),
	(( u'ParamName' , u'ParamIndex' , u'pVal' , ), 3, (3, (), [ (3, 1, None, None) , 
			(16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 36 , (3, 0, None, None) , 0 , )),
	(( u'IsParam' , u'ParamName' , u'pVal' , ), 4, (4, (), [ (8, 1, None, None) , 
			(16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 40 , (3, 0, None, None) , 0 , )),
]

IRTParam_vtables_dispatch_ = 1
IRTParam_vtables_ = [
]

IRecordset_vtables_dispatch_ = 1
IRecordset_vtables_ = [
	(( u'FieldValue' , u'FieldKey' , u'pVal' , ), 0, (0, (), [ (12, 0, None, None) , 
			(16396, 10, None, None) , ], 1 , 2 , 4 , 0 , 48 , (3, 0, None, None) , 0 , )),
	(( u'FieldValue' , u'FieldKey' , u'pVal' , ), 0, (0, (), [ (12, 0, None, None) , 
			(12, 1, None, None) , ], 1 , 4 , 4 , 0 , 52 , (3, 0, None, None) , 0 , )),
	(( u'RecordCount' , u'pVal' , ), 6, (6, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 56 , (3, 0, None, None) , 0 , )),
	(( u'BOR' , u'pVal' , ), 7, (7, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 60 , (3, 0, None, None) , 0 , )),
	(( u'EOR' , u'pVal' , ), 8, (8, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 64 , (3, 0, None, None) , 0 , )),
	(( u'CacheSize' , u'pVal' , ), 9, (9, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 68 , (3, 0, None, None) , 0 , )),
	(( u'CacheSize' , u'pVal' , ), 9, (9, (), [ (3, 1, None, None) , ], 1 , 4 , 4 , 0 , 72 , (3, 0, None, None) , 0 , )),
	(( u'Position' , u'pVal' , ), 10, (10, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 76 , (3, 0, None, None) , 0 , )),
	(( u'Position' , u'pVal' , ), 10, (10, (), [ (3, 1, None, None) , ], 1 , 4 , 4 , 0 , 80 , (3, 0, None, None) , 0 , )),
	(( u'First' , ), 11, (11, (), [ ], 1 , 1 , 4 , 0 , 84 , (3, 0, None, None) , 0 , )),
	(( u'Next' , ), 12, (12, (), [ ], 1 , 1 , 4 , 0 , 88 , (3, 0, None, None) , 0 , )),
	(( u'Prev' , ), 13, (13, (), [ ], 1 , 1 , 4 , 0 , 92 , (3, 0, None, None) , 0 , )),
	(( u'Last' , ), 14, (14, (), [ ], 1 , 1 , 4 , 0 , 96 , (3, 0, None, None) , 0 , )),
	(( u'Clone' , u'pRecordset' , ), 15, (15, (), [ (16393, 10, None, None) , ], 1 , 1 , 4 , 0 , 100 , (3, 0, None, None) , 0 , )),
	(( u'Refresh' , u'Range' , u'Low' , u'High' , ), 16, (16, (), [ 
			(3, 49, '0', None) , (3, 17, None, None) , (3, 17, None, None) , ], 1 , 1 , 4 , 0 , 104 , (3, 0, None, None) , 0 , )),
]

IReq_vtables_dispatch_ = 1
IReq_vtables_ = [
	(( u'Name' , u'pVal' , ), 15, (15, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 96 , (3, 0, None, None) , 0 , )),
	(( u'Name' , u'pVal' , ), 15, (15, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 100 , (3, 0, None, None) , 0 , )),
	(( u'Comment' , u'pVal' , ), 16, (16, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 104 , (3, 0, None, None) , 0 , )),
	(( u'Comment' , u'pVal' , ), 16, (16, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 108 , (3, 0, None, None) , 0 , )),
	(( u'Product' , u'pVal' , ), 17, (17, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 112 , (3, 0, None, None) , 0 , )),
	(( u'Product' , u'pVal' , ), 17, (17, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 116 , (3, 0, None, None) , 0 , )),
	(( u'Type' , u'pVal' , ), 18, (18, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 120 , (3, 0, None, None) , 0 , )),
	(( u'Type' , u'pVal' , ), 18, (18, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 124 , (3, 0, None, None) , 0 , )),
	(( u'Author' , u'pVal' , ), 19, (19, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 128 , (3, 0, None, None) , 0 , )),
	(( u'Author' , u'pVal' , ), 19, (19, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 132 , (3, 0, None, None) , 0 , )),
	(( u'Count' , u'pVal' , ), 20, (20, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 136 , (3, 0, None, None) , 0 , )),
	(( u'Priority' , u'pVal' , ), 21, (21, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 140 , (3, 0, None, None) , 0 , )),
	(( u'Priority' , u'pVal' , ), 21, (21, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 144 , (3, 0, None, None) , 0 , )),
	(( u'Status' , u'pVal' , ), 22, (22, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 148 , (3, 0, None, None) , 0 , )),
	(( u'Reviewed' , u'pVal' , ), 23, (23, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 152 , (3, 0, None, None) , 0 , )),
	(( u'Reviewed' , u'pVal' , ), 23, (23, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 156 , (3, 0, None, None) , 0 , )),
	(( u'AddCoverage' , u'TestId' , u'Order' , u'RealOrder' , ), 24, (24, (), [ 
			(3, 1, None, None) , (3, 1, None, None) , (16387, 10, None, None) , ], 1 , 1 , 4 , 0 , 160 , (3, 0, None, None) , 0 , )),
	(( u'AddCoverageEx' , u'SubjectID' , u'Order' , u'RealOrder' , ), 25, (25, (), [ 
			(3, 1, None, None) , (3, 1, None, None) , (16387, 10, None, None) , ], 1 , 1 , 4 , 0 , 164 , (3, 0, None, None) , 0 , )),
	(( u'RemoveCoverage' , u'vTest' , u'Recursive' , ), 26, (26, (), [ (12, 1, None, None) , 
			(11, 49, 'False', None) , ], 1 , 1 , 4 , 0 , 168 , (3, 0, None, None) , 0 , )),
	(( u'GetCoverList' , u'Recursive' , u'pList' , ), 27, (27, (), [ (11, 49, 'False', None) , 
			(16393, 10, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , ], 1 , 1 , 4 , 0 , 172 , (3, 0, None, None) , 0 , )),
	(( u'Paragraph' , u'pVal' , ), 28, (28, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 176 , (3, 0, None, None) , 0 , )),
	(( u'Path' , u'pVal' , ), 29, (29, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 180 , (3, 0, None, None) , 0 , )),
	(( u'HasCoverage' , u'pVal' , ), 30, (30, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 184 , (3, 0, None, None) , 0 , )),
	(( u'Move' , u'NewFatherId' , u'NewOrder' , ), 31, (31, (), [ (3, 0, None, None) , 
			(3, 0, None, None) , ], 1 , 1 , 4 , 0 , 188 , (3, 0, None, None) , 0 , )),
	(( u'IsFolder' , u'pVal' , ), 32, (32, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 192 , (3, 0, None, None) , 64 , )),
	(( u'IsFolder' , u'pVal' , ), 32, (32, (), [ (11, 1, None, None) , ], 1 , 4 , 4 , 0 , 196 , (3, 0, None, None) , 64 , )),
	(( u'ReqSummaryStatus' , u'pVal' , ), 33, (33, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 200 , (3, 0, None, None) , 64 , )),
	(( u'ReqCoverageStatus' , u'pVal' , ), 34, (34, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 204 , (3, 0, None, None) , 64 , )),
	(( u'AddCoverageByFilter' , u'SubjectID' , u'Order' , u'TestFilter' , u'RealOrder' , 
			), 35, (35, (), [ (3, 1, None, None) , (3, 1, None, None) , (8, 1, None, None) , (16387, 10, None, None) , ], 1 , 1 , 4 , 0 , 208 , (3, 0, None, None) , 0 , )),
	(( u'GetCoverListByFilter' , u'TestFilter' , u'Recursive' , u'pList' , ), 36, (36, (), [ 
			(8, 1, None, None) , (11, 49, 'False', None) , (16393, 10, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , ], 1 , 1 , 4 , 0 , 212 , (3, 0, None, None) , 0 , )),
]

IReqCoverageFactory_vtables_dispatch_ = 1
IReqCoverageFactory_vtables_ = [
	(( u'FullCoverage' , u'pVal' , ), 8, (8, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 64 , (3, 0, None, None) , 0 , )),
	(( u'FullCoverage' , u'pVal' , ), 8, (8, (), [ (11, 1, None, None) , ], 1 , 4 , 4 , 0 , 68 , (3, 0, None, None) , 0 , )),
]

IReqCoverageStatus_vtables_dispatch_ = 1
IReqCoverageStatus_vtables_ = [
	(( u'PossibleStatuses' , u'pVal' , ), 1, (1, (), [ (16393, 10, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , ], 1 , 2 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
	(( u'SummaryStatus' , u'Status' , u'pVal' , ), 2, (2, (), [ (8, 1, None, None) , 
			(16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 32 , (3, 0, None, None) , 0 , )),
	(( u'TotalSummaryNodes' , u'pVal' , ), 3, (3, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 36 , (3, 0, None, None) , 0 , )),
]

IReqFactory_vtables_dispatch_ = 1
IReqFactory_vtables_ = [
	(( u'GetChildrenList' , u'FatherID' , u'pList' , ), 9, (9, (), [ (3, 1, None, None) , 
			(16393, 10, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , ], 1 , 1 , 4 , 0 , 68 , (3, 0, None, None) , 0 , )),
	(( u'Find' , u'StartRootID' , u'FieldName' , u'Pattern' , u'Mode' , 
			u'Limit' , u'pList' , ), 10, (10, (), [ (3, 1, None, None) , (8, 1, None, None) , 
			(8, 1, None, None) , (3, 49, '0', None) , (3, 49, '100', None) , (16393, 10, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , ], 1 , 1 , 4 , 0 , 72 , (3, 0, None, None) , 0 , )),
	(( u'BuildSummaryGraph' , u'XAxisField' , u'GroupByField' , u'SumOfField' , u'MaxCols' , 
			u'Filter' , u'ForceRefresh' , u'ShowFullPath' , u'pGraph' , ), 11, (11, (), [ 
			(8, 49, "u''", None) , (8, 49, "u''", None) , (8, 49, "u''", None) , (3, 49, '0', None) , (12, 17, None, None) , 
			(11, 49, 'False', None) , (11, 49, 'False', None) , (16393, 10, None, None) , ], 1 , 1 , 4 , 0 , 76 , (3, 32, None, None) , 0 , )),
	(( u'BuildProgressGraph' , u'GroupByField' , u'SumOfField' , u'ByHistory' , u'MajorSkip' , 
			u'MinorSkip' , u'MaxCols' , u'Filter' , u'FRDate' , u'ForceRefresh' , 
			u'ShowFullPath' , u'pGraph' , ), 12, (12, (), [ (8, 49, "u''", None) , (8, 49, "u''", None) , 
			(11, 49, 'True', None) , (3, 49, '0', None) , (3, 49, '1', None) , (3, 49, '0', None) , (12, 17, None, None) , 
			(12, 17, None, None) , (11, 49, 'False', None) , (11, 49, 'False', None) , (16393, 10, None, None) , ], 1 , 1 , 4 , 0 , 80 , (3, 32, None, None) , 0 , )),
	(( u'BuildTrendGraph' , u'GroupByField' , u'SumOfField' , u'MaxCols' , u'Filter' , 
			u'FRDate' , u'ForceRefresh' , u'ShowFullPath' , u'pGraph' , ), 13, (13, (), [ 
			(8, 49, "u''", None) , (8, 49, "u''", None) , (3, 49, '0', None) , (12, 17, None, None) , (12, 17, None, None) , 
			(11, 49, 'False', None) , (11, 49, 'False', None) , (16393, 10, None, None) , ], 1 , 1 , 4 , 0 , 84 , (3, 32, None, None) , 0 , )),
	(( u'BuildPerfGraph' , u'GroupByField' , u'SumOfField' , u'MaxCols' , u'Filter' , 
			u'FRDate' , u'ForceRefresh' , u'ShowFullPath' , u'pGraph' , ), 14, (14, (), [ 
			(8, 49, "u''", None) , (8, 49, "u''", None) , (3, 49, '0', None) , (12, 17, None, None) , (12, 17, None, None) , 
			(11, 49, 'False', None) , (11, 49, 'False', None) , (16393, 10, None, None) , ], 1 , 1 , 4 , 0 , 88 , (3, 32, None, None) , 0 , )),
	(( u'BuildSummaryGraphEx' , u'XAxisField' , u'GroupByField' , u'SumOfField' , u'MaxCols' , 
			u'Filter' , u'ForceRefresh' , u'ShowFullPath' , u'ShowNullParents' , u'pGraph' , 
			), 15, (15, (), [ (8, 49, "u''", None) , (8, 49, "u''", None) , (8, 49, "u''", None) , (3, 49, '0', None) , 
			(12, 17, None, None) , (11, 49, 'False', None) , (11, 49, 'False', None) , (11, 49, 'False', None) , (16393, 10, None, None) , ], 1 , 1 , 4 , 0 , 92 , (3, 32, None, None) , 0 , )),
	(( u'BuildProgressGraphEx' , u'GroupByField' , u'SumOfField' , u'ByHistory' , u'MajorSkip' , 
			u'MinorSkip' , u'MaxCols' , u'Filter' , u'FRDate' , u'ForceRefresh' , 
			u'ShowFullPath' , u'ShowNullParents' , u'pGraph' , ), 16, (16, (), [ (8, 49, "u''", None) , 
			(8, 49, "u''", None) , (11, 49, 'True', None) , (3, 49, '0', None) , (3, 49, '1', None) , (3, 49, '0', None) , 
			(12, 17, None, None) , (12, 17, None, None) , (11, 49, 'False', None) , (11, 49, 'False', None) , (11, 49, 'False', None) , 
			(16393, 10, None, None) , ], 1 , 1 , 4 , 0 , 96 , (3, 32, None, None) , 0 , )),
	(( u'BuildTrendGraphEx' , u'GroupByField' , u'SumOfField' , u'MaxCols' , u'Filter' , 
			u'FRDate' , u'ForceRefresh' , u'ShowFullPath' , u'ShowNullParents' , u'pGraph' , 
			), 17, (17, (), [ (8, 49, "u''", None) , (8, 49, "u''", None) , (3, 49, '0', None) , (12, 17, None, None) , 
			(12, 17, None, None) , (11, 49, 'False', None) , (11, 49, 'False', None) , (11, 49, 'False', None) , (16393, 10, None, None) , ], 1 , 1 , 4 , 0 , 100 , (3, 32, None, None) , 0 , )),
	(( u'BuildPerfGraphEx' , u'GroupByField' , u'SumOfField' , u'MaxCols' , u'Filter' , 
			u'FRDate' , u'ForceRefresh' , u'ShowFullPath' , u'ShowNullParents' , u'pGraph' , 
			), 18, (18, (), [ (8, 49, "u''", None) , (8, 49, "u''", None) , (3, 49, '0', None) , (12, 17, None, None) , 
			(12, 17, None, None) , (11, 49, 'False', None) , (11, 49, 'False', None) , (11, 49, 'False', None) , (16393, 10, None, None) , ], 1 , 1 , 4 , 0 , 104 , (3, 32, None, None) , 0 , )),
]

IReqFactory2_vtables_dispatch_ = 1
IReqFactory2_vtables_ = [
	(( u'GetFilteredChildrenList' , u'FatherID' , u'Filter' , u'pList' , ), 19, (19, (), [ 
			(3, 1, None, None) , (9, 49, '0', "IID('{452897AD-D9F8-4EAE-80DB-B9C11807507F}')") , (16393, 10, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , ], 1 , 1 , 4 , 0 , 108 , (3, 0, None, None) , 0 , )),
]

IReqSummaryStatus_vtables_dispatch_ = 1
IReqSummaryStatus_vtables_ = [
	(( u'PossibleStatuses' , u'pVal' , ), 1, (1, (), [ (16393, 10, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , ], 1 , 2 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
	(( u'SummaryStatus' , u'StatusName' , u'pVal' , ), 2, (2, (), [ (8, 1, None, None) , 
			(16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 32 , (3, 0, None, None) , 0 , )),
	(( u'TotalSummaryNodes' , u'pVal' , ), 3, (3, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 36 , (3, 0, None, None) , 0 , )),
]

IRule_vtables_dispatch_ = 1
IRule_vtables_ = [
	(( u'ID' , u'pVal' , ), 1, (1, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
	(( u'IsActive' , u'pVal' , ), 2, (2, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 32 , (3, 0, None, None) , 0 , )),
	(( u'IsActive' , u'pVal' , ), 2, (2, (), [ (11, 1, None, None) , ], 1 , 4 , 4 , 0 , 36 , (3, 0, None, None) , 0 , )),
	(( u'ToMail' , u'pVal' , ), 3, (3, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 40 , (3, 0, None, None) , 0 , )),
	(( u'ToMail' , u'pVal' , ), 3, (3, (), [ (11, 1, None, None) , ], 1 , 4 , 4 , 0 , 44 , (3, 0, None, None) , 0 , )),
	(( u'Description' , u'pVal' , ), 4, (4, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 48 , (3, 0, None, None) , 0 , )),
	(( u'Post' , ), 5, (5, (), [ ], 1 , 1 , 4 , 0 , 52 , (3, 0, None, None) , 0 , )),
]

IRuleManager_vtables_dispatch_ = 1
IRuleManager_vtables_ = [
	(( u'GetRule' , u'ID' , u'pVal' , ), 1, (1, (), [ (3, 1, None, None) , 
			(16393, 10, None, None) , ], 1 , 1 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
	(( u'Rules' , u'NeedRefresh' , u'pList' , ), 2, (2, (), [ (11, 49, 'False', None) , 
			(16393, 10, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , ], 1 , 2 , 4 , 0 , 32 , (3, 0, None, None) , 0 , )),
	(( u'Rules' , u'NeedRefresh' , u'pList' , ), 2, (2, (), [ (11, 49, 'False', None) , 
			(16393, 10, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , ], 1 , 2 , 4 , 0 , 32 , (3, 0, None, None) , 0 , )),
]

IRun_vtables_dispatch_ = 1
IRun_vtables_ = [
	(( u'ResultLocation' , u'pVal' , ), 14, (14, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 92 , (3, 0, None, None) , 0 , )),
	(( u'Name' , u'pVal' , ), 15, (15, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 96 , (3, 0, None, None) , 0 , )),
	(( u'Name' , u'pVal' , ), 15, (15, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 100 , (3, 0, None, None) , 0 , )),
	(( u'Status' , u'pVal' , ), 16, (16, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 104 , (3, 0, None, None) , 0 , )),
	(( u'Status' , u'pVal' , ), 16, (16, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 108 , (3, 0, None, None) , 0 , )),
	(( u'StepFactory' , u'pVal' , ), 17, (17, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 112 , (3, 0, None, None) , 0 , )),
	(( u'TestId' , u'pVal' , ), 18, (18, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 116 , (3, 0, None, None) , 0 , )),
	(( u'ExtendedStorage' , u'pVal' , ), 19, (19, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 120 , (3, 0, None, None) , 0 , )),
	(( u'CopyDesignSteps' , ), 20, (20, (), [ ], 1 , 1 , 4 , 0 , 124 , (3, 0, None, None) , 0 , )),
	(( u'CopyStepsToTest' , ), 21, (21, (), [ ], 1 , 1 , 4 , 0 , 128 , (3, 0, None, None) , 0 , )),
	(( u'TestSetID' , u'pVal' , ), 22, (22, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 132 , (3, 0, None, None) , 0 , )),
	(( u'Params' , u'SourceMode' , u'pVal' , ), 23, (23, (), [ (3, 1, None, None) , 
			(16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 136 , (3, 0, None, None) , 0 , )),
	(( u'ResolveStepsParameters' , u'UpdateLocalCache' , ), 24, (24, (), [ (11, 49, 'True', None) , ], 1 , 1 , 4 , 0 , 140 , (3, 0, None, None) , 0 , )),
	(( u'TestInstance' , u'pVal' , ), 25, (25, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 144 , (3, 0, None, None) , 0 , )),
	(( u'CancelRun' , ), 26, (26, (), [ ], 1 , 1 , 4 , 0 , 148 , (3, 0, None, None) , 0 , )),
]

IRun2_vtables_dispatch_ = 1
IRun2_vtables_ = [
	(( u'BPStepParamFactory' , u'pVal' , ), 27, (27, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 152 , (3, 0, None, None) , 0 , )),
	(( u'TestInstanceID' , u'pVal' , ), 28, (28, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 156 , (3, 0, None, None) , 0 , )),
]

IRunFactory_vtables_dispatch_ = 1
IRunFactory_vtables_ = [
	(( u'UniqueRunName' , u'pVal' , ), 8, (8, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 64 , (3, 0, None, None) , 0 , )),
	(( u'DeleteDuplicateRuns' , u'RunName' , ), 9, (9, (), [ (8, 0, None, None) , ], 1 , 1 , 4 , 0 , 68 , (3, 0, None, None) , 0 , )),
]

ISearchOptions_vtables_dispatch_ = 1
ISearchOptions_vtables_ = [
	(( u'Property' , u'Prop' , u'pVal' , ), 1, (1, (), [ (3, 1, None, None) , 
			(16396, 10, None, None) , ], 1 , 2 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
	(( u'Property' , u'Prop' , u'pVal' , ), 1, (1, (), [ (3, 1, None, None) , 
			(12, 1, None, None) , ], 1 , 4 , 4 , 0 , 32 , (3, 0, None, None) , 0 , )),
	(( u'Filter' , u'pFilter' , ), 2, (2, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 36 , (3, 0, None, None) , 0 , )),
	(( u'Filter' , u'pFilter' , ), 2, (2, (), [ (9, 1, None, None) , ], 1 , 4 , 4 , 0 , 40 , (3, 0, None, None) , 0 , )),
	(( u'Text' , u'pVal' , ), 3, (3, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 44 , (3, 0, None, None) , 64 , )),
]

ISearchableFactory_vtables_dispatch_ = 1
ISearchableFactory_vtables_ = [
	(( u'CreateSearchOptions' , u'pSearchOptions' , ), 1001, (1001, (), [ (16393, 10, None, None) , ], 1 , 1 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
	(( u'Search' , u'Query' , u'pSearchOptions' , u'pList' , ), 1002, (1002, (), [ 
			(8, 1, None, None) , (9, 17, None, None) , (16393, 10, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , ], 1 , 1 , 4 , 0 , 32 , (3, 0, None, None) , 0 , )),
	(( u'IsSearchable' , u'pResult' , ), 1003, (1003, (), [ (16395, 10, None, None) , ], 1 , 1 , 4 , 0 , 36 , (3, 0, None, None) , 0 , )),
]

ISettings_vtables_dispatch_ = 1
ISettings_vtables_ = [
	(( u'Open' , u'Category' , ), 1, (1, (), [ (8, 0, None, None) , ], 1 , 1 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
	(( u'Value' , u'Name' , u'pVal' , ), 2, (2, (), [ (8, 0, None, None) , 
			(16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 32 , (3, 0, None, None) , 0 , )),
	(( u'Value' , u'Name' , u'pVal' , ), 2, (2, (), [ (8, 0, None, None) , 
			(8, 1, None, None) , ], 1 , 4 , 4 , 0 , 36 , (3, 0, None, None) , 0 , )),
	(( u'Close' , ), 3, (3, (), [ ], 1 , 1 , 4 , 0 , 40 , (3, 0, None, None) , 0 , )),
	(( u'DeleteCategory' , u'Category' , ), 4, (4, (), [ (8, 0, None, None) , ], 1 , 1 , 4 , 0 , 44 , (3, 0, None, None) , 0 , )),
	(( u'DeleteValue' , u'Name' , ), 5, (5, (), [ (8, 0, None, None) , ], 1 , 1 , 4 , 0 , 48 , (3, 0, None, None) , 0 , )),
	(( u'EnumItems' , u'pList' , ), 6, (6, (), [ (16393, 10, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , ], 1 , 2 , 4 , 0 , 52 , (3, 0, None, None) , 0 , )),
	(( u'Post' , ), 7, (7, (), [ ], 1 , 1 , 4 , 0 , 56 , (3, 0, None, None) , 0 , )),
	(( u'Refresh' , u'Category' , ), 8, (8, (), [ (8, 0, None, None) , ], 1 , 1 , 4 , 0 , 60 , (3, 0, None, None) , 0 , )),
]

ISettings2_vtables_dispatch_ = 1
ISettings2_vtables_ = [
	(( u'IsSystem' , u'Name' , u'pVal' , ), 9, (9, (), [ (8, 0, None, None) , 
			(16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 64 , (3, 0, None, None) , 0 , )),
]

IStep_vtables_dispatch_ = 1
IStep_vtables_ = [
	(( u'Name' , u'pVal' , ), 14, (14, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 92 , (3, 0, None, None) , 0 , )),
	(( u'Name' , u'pVal' , ), 14, (14, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 96 , (3, 0, None, None) , 0 , )),
	(( u'Status' , u'pVal' , ), 15, (15, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 100 , (3, 0, None, None) , 0 , )),
	(( u'Status' , u'pVal' , ), 15, (15, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 104 , (3, 0, None, None) , 0 , )),
	(( u'CreationMode' , ), 16, (16, (), [ (2, 1, None, None) , ], 1 , 4 , 4 , 0 , 108 , (3, 0, None, None) , 64 , )),
	(( u'TestSource' , u'nTest' , ), 17, (17, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 112 , (3, 0, None, None) , 0 , )),
	(( u'DesignStepSource' , u'nStep' , ), 18, (18, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 116 , (3, 0, None, None) , 0 , )),
]

IStep2_vtables_dispatch_ = 1
IStep2_vtables_ = [
	(( u'BPStepParamFactory' , u'pVal' , ), 19, (19, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 120 , (3, 0, None, None) , 0 , )),
	(( u'Run' , u'pVal' , ), 20, (20, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 124 , (3, 0, None, None) , 0 , )),
]

IStepParams_vtables_dispatch_ = 1
IStepParams_vtables_ = [
	(( u'Count' , u'pVal' , ), 1, (1, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
	(( u'ParamValue' , u'vParam' , u'pVal' , ), 2, (2, (), [ (12, 1, None, None) , 
			(16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 32 , (3, 0, None, None) , 0 , )),
	(( u'ParamValue' , u'vParam' , u'pVal' , ), 2, (2, (), [ (12, 1, None, None) , 
			(8, 1, None, None) , ], 1 , 4 , 4 , 0 , 36 , (3, 0, None, None) , 0 , )),
	(( u'ClearParam' , u'vParam' , ), 3, (3, (), [ (12, 1, None, None) , ], 1 , 1 , 4 , 0 , 40 , (3, 0, None, None) , 0 , )),
	(( u'ParamType' , u'vParam' , u'pVal' , ), 4, (4, (), [ (12, 1, None, None) , 
			(16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 44 , (3, 0, None, None) , 64 , )),
	(( u'ParamType' , u'vParam' , u'pVal' , ), 4, (4, (), [ (12, 1, None, None) , 
			(8, 1, None, None) , ], 1 , 4 , 4 , 0 , 48 , (3, 0, None, None) , 64 , )),
	(( u'ParamExist' , u'ParamName' , u'pVal' , ), 5, (5, (), [ (8, 1, None, None) , 
			(16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 52 , (3, 0, None, None) , 0 , )),
	(( u'BaseValue' , u'vParam' , u'HasBaseValue' , u'pVal' , ), 6, (6, (), [ 
			(12, 1, None, None) , (16395, 2, None, None) , (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 56 , (3, 0, None, None) , 64 , )),
	(( u'ParamName' , u'nPosition' , u'pVal' , ), 7, (7, (), [ (3, 1, None, None) , 
			(16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 60 , (3, 0, None, None) , 0 , )),
	(( u'Type' , u'vParam' , u'pVal' , ), 8, (8, (), [ (12, 1, None, None) , 
			(16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 64 , (3, 0, None, None) , 0 , )),
	(( u'Save' , ), 9, (9, (), [ ], 1 , 1 , 4 , 0 , 68 , (3, 0, None, None) , 0 , )),
	(( u'Refresh' , ), 10, (10, (), [ ], 1 , 1 , 4 , 0 , 72 , (3, 0, None, None) , 0 , )),
	(( u'AddParam' , u'ParamName' , u'ParamType' , ), 11, (11, (), [ (8, 1, None, None) , 
			(8, 1, None, None) , ], 1 , 1 , 4 , 0 , 76 , (3, 0, None, None) , 0 , )),
	(( u'DeleteParam' , u'ParamName' , ), 12, (12, (), [ (8, 1, None, None) , ], 1 , 1 , 4 , 0 , 80 , (3, 0, None, None) , 0 , )),
]

ISubjectNode_vtables_dispatch_ = 1
ISubjectNode_vtables_ = [
	(( u'TestFactory' , u'pVal' , ), 15, (15, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 92 , (3, 0, None, None) , 0 , )),
	(( u'RemoveSubjectNode' , u'Node' , u'DeleteTests' , ), 16, (16, (), [ (12, 0, None, None) , 
			(11, 49, 'False', None) , ], 1 , 1 , 4 , 0 , 96 , (3, 0, None, None) , 0 , )),
	(( u'Description' , u'pVal' , ), 17, (17, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 100 , (3, 0, None, None) , 0 , )),
	(( u'Description' , u'pVal' , ), 17, (17, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 104 , (3, 0, None, None) , 0 , )),
	(( u'Attachments' , u'pVal' , ), 18, (18, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 108 , (3, 0, None, None) , 0 , )),
	(( u'FindTests' , u'Pattern' , u'MatchCase' , u'Filter' , u'pList' , 
			), 19, (19, (), [ (8, 1, None, None) , (11, 49, 'False', None) , (8, 49, "u''", None) , (16393, 10, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , ], 1 , 1 , 4 , 0 , 112 , (3, 32, None, None) , 0 , )),
	(( u'ViewOrder' , u'pVal' , ), 20, (20, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 116 , (3, 0, None, None) , 0 , )),
	(( u'ViewOrder' , u'pVal' , ), 20, (20, (), [ (3, 1, None, None) , ], 1 , 4 , 4 , 0 , 120 , (3, 0, None, None) , 0 , )),
	(( u'HasAttachment' , u'pVal' , ), 21, (21, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 124 , (3, 0, None, None) , 0 , )),
	(( u'Move' , u'Parent' , ), 22, (22, (), [ (12, 1, None, None) , ], 1 , 1 , 4 , 0 , 128 , (3, 0, None, None) , 0 , )),
]

ISupportCopyPaste_vtables_dispatch_ = 0
ISupportCopyPaste_vtables_ = [
	(( u'PasteFromClipBoard' , u'ClipboardData' , u'TargetID' , u'Mode' , u'OrderID' , 
			), 1610678272, (1610678272, (), [ (8, 1, None, None) , (8, 49, "u''", None) , (3, 49, '0', None) , (3, 49, '-1', None) , ], 1 , 1 , 4 , 0 , 12 , (3, 32, None, None) , 0 , )),
	(( u'CopyToClipBoard' , u'IDSFilter' , u'Mode' , u'Path' , u'pVal' , 
			), 1610678273, (1610678273, (), [ (8, 1, None, None) , (3, 49, '0', None) , (8, 49, "u''", None) , (16392, 10, None, None) , ], 1 , 1 , 4 , 0 , 16 , (3, 32, None, None) , 0 , )),
]

ISysTreeNode_vtables_dispatch_ = 1
ISysTreeNode_vtables_ = [
	(( u'Name' , u'pVal' , ), 0, (0, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
	(( u'Name' , u'pVal' , ), 0, (0, (), [ (8, 0, None, None) , ], 1 , 4 , 4 , 0 , 32 , (3, 0, None, None) , 0 , )),
	(( u'AddNode' , u'NodeName' , u'pNode' , ), 1, (1, (), [ (8, 0, None, None) , 
			(16393, 10, None, "IID('{8538CCBA-0B3E-481F-BB2B-14F0553CA146}')") , ], 1 , 1 , 4 , 0 , 36 , (3, 0, None, None) , 0 , )),
	(( u'NewList' , u'pList' , ), 2, (2, (), [ (16393, 10, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , ], 1 , 1 , 4 , 0 , 40 , (3, 0, None, None) , 0 , )),
	(( u'NodeID' , u'pVal' , ), 3, (3, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 44 , (3, 0, None, None) , 0 , )),
	(( u'Attribute' , u'pVal' , ), 4, (4, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 48 , (3, 0, None, None) , 0 , )),
	(( u'FindChildNode' , u'ChildName' , u'pNode' , ), 5, (5, (), [ (8, 0, None, None) , 
			(16393, 10, None, "IID('{8538CCBA-0B3E-481F-BB2B-14F0553CA146}')") , ], 1 , 1 , 4 , 0 , 52 , (3, 0, None, None) , 0 , )),
	(( u'RemoveNode' , u'Node' , ), 6, (6, (), [ (12, 0, None, None) , ], 1 , 1 , 4 , 0 , 56 , (3, 0, None, None) , 0 , )),
	(( u'Father' , u'pNode' , ), 7, (7, (), [ (16393, 10, None, "IID('{8538CCBA-0B3E-481F-BB2B-14F0553CA146}')") , ], 1 , 2 , 4 , 0 , 60 , (3, 0, None, None) , 0 , )),
	(( u'Child' , u'Index' , u'pNode' , ), 8, (8, (), [ (3, 0, None, None) , 
			(16393, 10, None, "IID('{8538CCBA-0B3E-481F-BB2B-14F0553CA146}')") , ], 1 , 2 , 4 , 0 , 64 , (3, 0, None, None) , 0 , )),
	(( u'Count' , u'pVal' , ), 9, (9, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 68 , (3, 0, None, None) , 0 , )),
	(( u'DepthType' , u'pVal' , ), 10, (10, (), [ (16386, 10, None, None) , ], 1 , 2 , 4 , 0 , 72 , (3, 0, None, None) , 0 , )),
	(( u'FindChildren' , u'Pattern' , u'MatchCase' , u'Filter' , u'pList' , 
			), 11, (11, (), [ (8, 1, None, None) , (11, 49, 'False', None) , (8, 49, "u''", None) , (16393, 10, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , ], 1 , 1 , 4 , 0 , 76 , (3, 32, None, None) , 0 , )),
	(( u'Path' , u'pVal' , ), 12, (12, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 80 , (3, 0, None, None) , 0 , )),
	(( u'Post' , ), 13, (13, (), [ ], 1 , 1 , 4 , 0 , 84 , (3, 0, None, None) , 0 , )),
	(( u'Refresh' , ), 14, (14, (), [ ], 1 , 1 , 4 , 0 , 88 , (3, 0, None, None) , 0 , )),
]

ITDChat_vtables_dispatch_ = 1
ITDChat_vtables_ = [
	(( u'Connect' , u'ChatRoom' , u'NewData' , ), 1, (1, (), [ (8, 1, None, None) , 
			(16392, 2, None, None) , ], 1 , 1 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
	(( u'Disconnect' , ), 2, (2, (), [ ], 1 , 1 , 4 , 0 , 32 , (3, 0, None, None) , 0 , )),
	(( u'PutMessage' , u'ChatMessage' , u'NewData' , ), 3, (3, (), [ (8, 1, None, None) , 
			(16392, 2, None, None) , ], 1 , 1 , 4 , 0 , 36 , (3, 0, None, None) , 0 , )),
	(( u'ChatData' , u'GetAllMesseges' , u'NewData' , ), 4, (4, (), [ (11, 1, None, None) , 
			(16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 40 , (3, 0, None, None) , 0 , )),
	(( u'ChangeChat' , u'NewChatRoom' , ), 5, (5, (), [ (8, 1, None, None) , ], 1 , 1 , 4 , 0 , 44 , (3, 0, None, None) , 0 , )),
]

ITDConnection_vtables_dispatch_ = 1
ITDConnection_vtables_ = [
	(( u'Connected' , u'pVal' , ), 1, (1, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
	(( u'ProjectConnected' , u'pVal' , ), 2, (2, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 32 , (3, 0, None, None) , 0 , )),
	(( u'ServerName' , u'pVal' , ), 3, (3, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 36 , (3, 0, None, None) , 0 , )),
	(( u'ProjectName' , u'pVal' , ), 4, (4, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 40 , (3, 0, None, None) , 0 , )),
	(( u'TestRepository' , u'pVal' , ), 5, (5, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 44 , (3, 0, None, None) , 0 , )),
	(( u'UserName' , u'pVal' , ), 6, (6, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 48 , (3, 0, None, None) , 0 , )),
	(( u'TestFactory' , u'pVal' , ), 7, (7, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 52 , (3, 0, None, None) , 0 , )),
	(( u'BugFactory' , u'pVal' , ), 8, (8, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 56 , (3, 0, None, None) , 0 , )),
	(( u'TestSetFactory' , u'pVal' , ), 9, (9, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 60 , (3, 0, None, None) , 0 , )),
	(( u'UserGroupsList' , u'pVal' , ), 10, (10, (), [ (16393, 10, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , ], 1 , 2 , 4 , 0 , 64 , (3, 0, None, None) , 0 , )),
	(( u'HostFactory' , u'pVal' , ), 11, (11, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 68 , (3, 0, None, None) , 0 , )),
	(( u'VCS' , u'pVal' , ), 12, (12, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 72 , (3, 0, None, None) , 0 , )),
	(( u'InitConnection' , u'ServerName' , u'DomainName' , u'DomainPswd' , ), 13, (13, (), [ 
			(8, 0, None, None) , (8, 49, "u''", None) , (8, 49, "u''", None) , ], 1 , 1 , 4 , 0 , 76 , (3, 32, None, None) , 64 , )),
	(( u'ReleaseConnection' , ), 14, (14, (), [ ], 1 , 1 , 4 , 0 , 80 , (3, 0, None, None) , 0 , )),
	(( u'ConnectProject' , u'ProjectName' , u'UserName' , u'Password' , ), 15, (15, (), [ 
			(8, 0, None, None) , (8, 0, None, None) , (8, 49, "u''", None) , ], 1 , 1 , 4 , 0 , 84 , (3, 32, None, None) , 0 , )),
	(( u'DisconnectProject' , ), 16, (16, (), [ ], 1 , 1 , 4 , 0 , 88 , (3, 0, None, None) , 0 , )),
	(( u'ProjectsList' , u'pList' , ), 17, (17, (), [ (16393, 10, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , ], 1 , 2 , 4 , 0 , 92 , (3, 0, None, None) , 0 , )),
	(( u'Command' , u'pVal' , ), 18, (18, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 96 , (3, 0, None, None) , 0 , )),
	(( u'TreeManager' , u'pVal' , ), 19, (19, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 100 , (3, 0, None, None) , 0 , )),
	(( u'ReqFactory' , u'pVal' , ), 20, (20, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 104 , (3, 0, None, None) , 0 , )),
	(( u'ActionPermission' , u'pVal' , ), 21, (21, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 108 , (3, 0, None, None) , 0 , )),
	(( u'GetLicense' , u'LicenseType' , ), 22, (22, (), [ (3, 0, None, None) , ], 1 , 1 , 4 , 0 , 112 , (3, 0, None, None) , 64 , )),
	(( u'SendMail' , u'SendTo' , u'SendFrom' , u'Subject' , u'Message' , 
			u'attachArray' , u'bsFormat' , ), 23, (23, (), [ (8, 1, None, None) , (8, 49, "u''", None) , 
			(8, 49, "u''", None) , (8, 49, "u''", None) , (12, 17, None, None) , (8, 49, "u''", None) , ], 1 , 1 , 4 , 0 , 116 , (3, 32, None, None) , 0 , )),
	(( u'DBType' , u'pVal' , ), 24, (24, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 120 , (3, 0, None, None) , 0 , )),
	(( u'DBManager' , u'pVal' , ), 25, (25, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 124 , (3, 0, None, None) , 64 , )),
	(( u'Customization' , u'pVal' , ), 26, (26, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 128 , (3, 0, None, None) , 0 , )),
	(( u'Fields' , u'DataType' , u'pVal' , ), 27, (27, (), [ (8, 1, None, None) , 
			(16393, 10, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , ], 1 , 2 , 4 , 0 , 132 , (3, 0, None, None) , 0 , )),
	(( u'CommonSettings' , u'pVal' , ), 28, (28, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 136 , (3, 0, None, None) , 0 , )),
	(( u'UserSettings' , u'pVal' , ), 29, (29, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 140 , (3, 0, None, None) , 0 , )),
	(( u'HostGroupFactory' , u'pVal' , ), 30, (30, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 144 , (3, 0, None, None) , 0 , )),
	(( u'ChangePassword' , u'OldPassword' , u'NewPassword' , ), 31, (31, (), [ (8, 1, None, None) , 
			(8, 1, None, None) , ], 1 , 1 , 4 , 0 , 148 , (3, 0, None, None) , 0 , )),
	(( u'UsersList' , u'pVal' , ), 32, (32, (), [ (16393, 10, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , ], 1 , 2 , 4 , 0 , 152 , (3, 0, None, None) , 0 , )),
	(( u'Password' , u'pVal' , ), 33, (33, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 156 , (3, 0, None, None) , 64 , )),
	(( u'ExtendedStorage' , u'pVal' , ), 34, (34, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 160 , (3, 0, None, None) , 0 , )),
	(( u'DirectoryPath' , u'nType' , u'pVal' , ), 35, (35, (), [ (3, 0, None, None) , 
			(16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 164 , (3, 0, None, None) , 0 , )),
	(( u'ChangeFactory' , u'pVal' , ), 36, (36, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 168 , (3, 0, None, None) , 64 , )),
	(( u'MailConditions' , u'pVal' , ), 37, (37, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 172 , (3, 0, None, None) , 0 , )),
	(( u'ServerTime' , u'pVal' , ), 38, (38, (), [ (16391, 10, None, None) , ], 1 , 2 , 4 , 0 , 176 , (3, 0, None, None) , 0 , )),
	(( u'TDSettings' , u'pVal' , ), 39, (39, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 180 , (3, 0, None, None) , 64 , )),
	(( u'PurgeRuns' , u'TestSetFilter' , u'KeepLast' , u'DateUnit' , u'UnitCount' , 
			u'StepsOnly' , ), 40, (40, (), [ (8, 1, None, None) , (3, 1, None, None) , (12, 1, None, None) , 
			(3, 1, None, None) , (11, 1, None, None) , ], 1 , 1 , 4 , 0 , 184 , (3, 0, None, None) , 0 , )),
	(( u'ProjectProperties' , u'pVal' , ), 41, (41, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 188 , (3, 0, None, None) , 0 , )),
	(( u'GetLicenseStatus' , u'ClientType' , u'InUse' , u'Max' , ), 42, (42, (), [ 
			(3, 1, None, None) , (16387, 2, None, None) , (16387, 2, None, None) , ], 1 , 1 , 4 , 0 , 192 , (3, 0, None, None) , 0 , )),
	(( u'DomainName' , u'pVal' , ), 43, (43, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 196 , (3, 0, None, None) , 0 , )),
	(( u'TextParam' , u'pVal' , ), 44, (44, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 200 , (3, 0, None, None) , 0 , )),
	(( u'TDParams' , u'Request' , u'OutData' , ), 45, (45, (), [ (8, 1, None, None) , 
			(16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 204 , (3, 0, None, None) , 0 , )),
	(( u'UsingProgress' , u'pVal' , ), 46, (46, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 208 , (3, 0, None, None) , 0 , )),
	(( u'UsingProgress' , u'pVal' , ), 46, (46, (), [ (11, 1, None, None) , ], 1 , 4 , 4 , 0 , 212 , (3, 0, None, None) , 0 , )),
	(( u'CheckoutRepository' , u'pVal' , ), 47, (47, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 216 , (3, 0, None, None) , 0 , )),
	(( u'ViewsRepository' , u'pVal' , ), 48, (48, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 220 , (3, 0, None, None) , 64 , )),
	(( u'VcsDbRepository' , u'pVal' , ), 49, (49, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 224 , (3, 0, None, None) , 0 , )),
	(( u'RunFactory' , u'pVal' , ), 50, (50, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 228 , (3, 0, None, None) , 0 , )),
	(( u'ModuleVisible' , u'ModuleID' , u'pVal' , ), 51, (51, (), [ (3, 1, None, None) , 
			(16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 232 , (3, 0, None, None) , 0 , )),
	(( u'InitConnectionEx' , u'ServerName' , ), 52, (52, (), [ (8, 0, None, None) , ], 1 , 1 , 4 , 0 , 236 , (3, 0, None, None) , 0 , )),
	(( u'ProjectsListEx' , u'DomainName' , u'pList' , ), 53, (53, (), [ (8, 1, None, None) , 
			(16393, 10, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , ], 1 , 2 , 4 , 0 , 240 , (3, 0, None, None) , 0 , )),
	(( u'ConnectProjectEx' , u'DomainName' , u'ProjectName' , u'UserName' , u'Password' , 
			), 54, (54, (), [ (8, 1, None, None) , (8, 1, None, None) , (8, 1, None, None) , (8, 49, "u''", None) , ], 1 , 1 , 4 , 0 , 244 , (3, 32, None, None) , 0 , )),
	(( u'DomainsList' , u'pVal' , ), 55, (55, (), [ (16393, 10, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , ], 1 , 2 , 4 , 0 , 248 , (3, 0, None, None) , 0 , )),
	(( u'ConnectToVCSAs' , u'UserName' , u'Password' , u'CopyDesStep' , ), 56, (56, (), [ 
			(8, 0, None, None) , (8, 0, None, None) , (8, 0, None, None) , ], 1 , 1 , 4 , 0 , 252 , (3, 0, None, None) , 64 , )),
	(( u'GetLicenses' , u'LicensesType' , u'pVal' , ), 57, (57, (), [ (3, 0, None, None) , 
			(16392, 0, None, None) , ], 1 , 1 , 4 , 0 , 256 , (3, 0, None, None) , 0 , )),
	(( u'Analysis' , u'pVal' , ), 58, (58, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 260 , (3, 0, None, None) , 64 , )),
	(( u'VMRepository' , u'pVal' , ), 59, (59, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 264 , (3, 0, None, None) , 64 , )),
	(( u'DBName' , u'pVal' , ), 60, (60, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 268 , (3, 0, None, None) , 0 , )),
	(( u'Rules' , u'pVal' , ), 61, (61, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 272 , (3, 0, None, None) , 0 , )),
	(( u'TestSetTreeManager' , u'pVal' , ), 62, (62, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 276 , (3, 0, None, None) , 0 , )),
	(( u'AlertManager' , u'pVal' , ), 63, (63, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 280 , (3, 0, None, None) , 0 , )),
	(( u'AllowReconnect' , u'pVal' , ), 64, (64, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 284 , (3, 0, None, None) , 64 , )),
	(( u'AllowReconnect' , u'pVal' , ), 64, (64, (), [ (11, 1, None, None) , ], 1 , 4 , 4 , 0 , 288 , (3, 0, None, None) , 64 , )),
	(( u'SynchronizeFollowUps' , u'Password' , ), 65, (65, (), [ (8, 1, None, None) , ], 1 , 1 , 4 , 0 , 292 , (3, 0, None, None) , 64 , )),
	(( u'KeepConnection' , u'pVal' , ), 66, (66, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 296 , (3, 0, None, None) , 64 , )),
	(( u'KeepConnection' , u'pVal' , ), 66, (66, (), [ (11, 1, None, None) , ], 1 , 4 , 4 , 0 , 300 , (3, 0, None, None) , 64 , )),
	(( u'IgnoreHtmlFormat' , u'pVal' , ), 67, (67, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 304 , (3, 0, None, None) , 0 , )),
	(( u'IgnoreHtmlFormat' , u'pVal' , ), 67, (67, (), [ (11, 1, None, None) , ], 1 , 4 , 4 , 0 , 308 , (3, 0, None, None) , 0 , )),
	(( u'ReportRole' , u'pVal' , ), 68, (68, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 312 , (3, 0, None, None) , 0 , )),
	(( u'ClientType' , u'newVal' , ), 69, (69, (), [ (8, 1, None, None) , (8, 49, "u''", None) , ], 1 , 4 , 4 , 0 , 316 , (3, 32, None, None) , 0 , )),
	(( u'ClientType' , u'newVal' , ), 69, (69, (), [ (8, 1, None, None) , (8, 49, "u''", None) , ], 1 , 4 , 4 , 0 , 316 , (3, 32, None, None) , 0 , )),
	(( u'ComponentFactory' , u'pVal' , ), 70, (70, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 320 , (3, 0, None, None) , 64 , )),
	(( u'ComponentFolderFactory' , u'pVal' , ), 71, (71, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 324 , (3, 0, None, None) , 64 , )),
	(( u'GetTDVersion' , u'pbsMajorVersion' , u'pbsBuildNum' , ), 72, (72, (), [ (16392, 2, None, None) , 
			(16392, 2, None, None) , ], 1 , 1 , 4 , 0 , 328 , (3, 0, None, None) , 0 , )),
	(( u'ServerURL' , u'pVal' , ), 73, (73, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 332 , (3, 0, None, None) , 0 , )),
]

ITDConnection2_vtables_dispatch_ = 1
ITDConnection2_vtables_ = [
	(( u'ProductInfo' , u'pProductInfo' , ), 75, (75, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 336 , (3, 0, None, None) , 0 , )),
	(( u'GraphBuilder' , u'pGraphBuilder' , ), 76, (76, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 340 , (3, 0, None, None) , 0 , )),
	(( u'AuditRecordFactory' , u'pVal' , ), 77, (77, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 344 , (3, 0, None, None) , 0 , )),
	(( u'AuditPropertyFactory' , u'pVal' , ), 78, (78, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 348 , (3, 0, None, None) , 0 , )),
	(( u'TSTestFactory' , u'pVal' , ), 79, (79, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 352 , (3, 0, None, None) , 0 , )),
	(( u'IsSearchSupported' , u'pVal' , ), 80, (80, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 356 , (3, 0, None, None) , 0 , )),
	(( u'Login' , u'UserName' , u'Password' , ), 81, (81, (), [ (8, 1, None, None) , 
			(8, 1, None, None) , ], 1 , 1 , 4 , 0 , 360 , (3, 0, None, None) , 0 , )),
	(( u'Connect' , u'DomainName' , u'ProjectName' , ), 82, (82, (), [ (8, 1, None, None) , 
			(8, 1, None, None) , ], 1 , 1 , 4 , 0 , 364 , (3, 0, None, None) , 0 , )),
	(( u'Disconnect' , ), 83, (83, (), [ ], 1 , 1 , 4 , 0 , 368 , (3, 0, None, None) , 0 , )),
	(( u'Logout' , ), 84, (84, (), [ ], 1 , 1 , 4 , 0 , 372 , (3, 0, None, None) , 0 , )),
	(( u'LoggedIn' , u'pLoggedIn' , ), 85, (85, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 376 , (3, 0, None, None) , 0 , )),
	(( u'VisibleDomains' , u'pDomainsList' , ), 86, (86, (), [ (16393, 10, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , ], 1 , 2 , 4 , 0 , 380 , (3, 0, None, None) , 0 , )),
	(( u'VisibleProjects' , u'DomainName' , u'pProjectsList' , ), 87, (87, (), [ (8, 1, None, None) , 
			(16393, 10, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , ], 1 , 2 , 4 , 0 , 384 , (3, 0, None, None) , 0 , )),
]

ITDErrorInfo_vtables_dispatch_ = 0
ITDErrorInfo_vtables_ = [
	(( u'Details' , u'pVal' , ), 1, (1, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 32 , (3, 0, None, None) , 0 , )),
]

ITDField_vtables_dispatch_ = 1
ITDField_vtables_ = [
	(( u'Name' , u'pVal' , ), 0, (0, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
	(( u'Type' , u'pVal' , ), 1, (1, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 32 , (3, 0, None, None) , 0 , )),
	(( u'Property' , u'pVal' , ), 2, (2, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 36 , (3, 0, None, None) , 0 , )),
	(( u'IsValidValue' , u'Value' , u'pObject' , ), 3, (3, (), [ (12, 1, None, None) , 
			(9, 1, None, None) , ], 1 , 1 , 4 , 0 , 40 , (3, 0, None, None) , 0 , )),
]

ITDFilter_vtables_dispatch_ = 1
ITDFilter_vtables_ = [
	(( u'_Text' , u'pVal' , ), 12, (12, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 28 , (3, 0, None, None) , 65 , )),
	(( u'_Text' , u'pVal' , ), 12, (12, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 32 , (3, 0, None, None) , 65 , )),
	(( u'Filter' , u'FieldName' , u'pVal' , ), 0, (0, (), [ (8, 1, None, None) , 
			(16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 36 , (3, 0, None, None) , 0 , )),
	(( u'Filter' , u'FieldName' , u'pVal' , ), 0, (0, (), [ (8, 1, None, None) , 
			(8, 1, None, None) , ], 1 , 4 , 4 , 0 , 40 , (3, 0, None, None) , 0 , )),
	(( u'Order' , u'FieldName' , u'pVal' , ), 1, (1, (), [ (8, 1, None, None) , 
			(16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 44 , (3, 0, None, None) , 0 , )),
	(( u'Order' , u'FieldName' , u'pVal' , ), 1, (1, (), [ (8, 1, None, None) , 
			(3, 1, None, None) , ], 1 , 4 , 4 , 0 , 48 , (3, 0, None, None) , 0 , )),
	(( u'OrderDirection' , u'FieldName' , u'pVal' , ), 2, (2, (), [ (8, 1, None, None) , 
			(16386, 10, None, None) , ], 1 , 2 , 4 , 0 , 52 , (3, 0, None, None) , 0 , )),
	(( u'OrderDirection' , u'FieldName' , u'pVal' , ), 2, (2, (), [ (8, 1, None, None) , 
			(2, 1, None, None) , ], 1 , 4 , 4 , 0 , 56 , (3, 0, None, None) , 0 , )),
	(( u'Clear' , ), 3, (3, (), [ ], 1 , 1 , 4 , 0 , 60 , (3, 0, None, None) , 0 , )),
	(( u'NewList' , u'pList' , ), 4, (4, (), [ (16393, 10, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , ], 1 , 1 , 4 , 0 , 64 , (3, 0, None, None) , 0 , )),
	(( u'Fields' , u'pFields' , ), 5, (5, (), [ (16393, 10, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , ], 1 , 2 , 4 , 0 , 68 , (3, 0, None, None) , 0 , )),
	(( u'Refresh' , ), 6, (6, (), [ ], 1 , 1 , 4 , 0 , 72 , (3, 0, None, None) , 0 , )),
	(( u'CaseSensitive' , u'FieldName' , u'pVal' , ), 7, (7, (), [ (8, 1, None, None) , 
			(16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 76 , (3, 0, None, None) , 0 , )),
	(( u'CaseSensitive' , u'FieldName' , u'pVal' , ), 7, (7, (), [ (8, 1, None, None) , 
			(11, 1, None, None) , ], 1 , 4 , 4 , 0 , 80 , (3, 0, None, None) , 0 , )),
	(( u'Text' , u'pVal' , ), 8, (8, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 84 , (3, 0, None, None) , 0 , )),
	(( u'Text' , u'pVal' , ), 8, (8, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 88 , (3, 0, None, None) , 0 , )),
	(( u'XFilter' , u'EntityType' , u'pVal' , ), 9, (9, (), [ (8, 1, None, None) , 
			(16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 92 , (3, 0, None, None) , 0 , )),
	(( u'XFilter' , u'EntityType' , u'pVal' , ), 9, (9, (), [ (8, 1, None, None) , 
			(8, 1, None, None) , ], 1 , 4 , 4 , 0 , 96 , (3, 0, None, None) , 0 , )),
	(( u'UFilter' , u'EntityType' , u'pVal' , ), 10, (10, (), [ (8, 1, None, None) , 
			(16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 100 , (3, 0, None, None) , 64 , )),
	(( u'UFilter' , u'EntityType' , u'pVal' , ), 10, (10, (), [ (8, 1, None, None) , 
			(8, 1, None, None) , ], 1 , 4 , 4 , 0 , 104 , (3, 0, None, None) , 64 , )),
	(( u'DataType' , u'pVal' , ), 11, (11, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 108 , (3, 0, None, None) , 0 , )),
]

ITDFilter2_vtables_dispatch_ = 1
ITDFilter2_vtables_ = [
	(( u'SetXFilter' , u'JoinEntities' , u'Inclusive' , u'FilterText' , ), 13, (13, (), [ 
			(8, 1, None, None) , (11, 1, None, None) , (8, 1, None, None) , ], 1 , 1 , 4 , 0 , 112 , (3, 0, None, None) , 0 , )),
	(( u'GetXFilter' , u'JoinEntities' , u'Inclusive' , u'pFilterText' , ), 14, (14, (), [ 
			(8, 1, None, None) , (16395, 2, None, None) , (16392, 10, None, None) , ], 1 , 1 , 4 , 0 , 116 , (3, 0, None, None) , 0 , )),
	(( u'IsClear' , u'pbResult' , ), 15, (15, (), [ (16395, 10, None, None) , ], 1 , 1 , 4 , 0 , 120 , (3, 0, None, None) , 0 , )),
]

ITDMailConditions_vtables_dispatch_ = 1
ITDMailConditions_vtables_ = [
	(( u'Load' , ), 4, (4, (), [ ], 1 , 1 , 4 , 0 , 40 , (3, 0, None, None) , 0 , )),
	(( u'Condition' , u'Name' , u'IsUserCondition' , u'pVal' , ), 5, (5, (), [ 
			(8, 0, None, None) , (11, 0, None, None) , (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 44 , (3, 0, None, None) , 0 , )),
	(( u'Condition' , u'Name' , u'IsUserCondition' , u'pVal' , ), 5, (5, (), [ 
			(8, 0, None, None) , (11, 0, None, None) , (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 48 , (3, 0, None, None) , 0 , )),
	(( u'DeleteCondition' , u'Name' , u'IsUserCondition' , ), 6, (6, (), [ (8, 0, None, None) , 
			(11, 0, None, None) , ], 1 , 1 , 4 , 0 , 52 , (3, 0, None, None) , 0 , )),
	(( u'Close' , u'NeedToSave' , ), 7, (7, (), [ (11, 0, None, None) , ], 1 , 1 , 4 , 0 , 56 , (3, 0, None, None) , 0 , )),
	(( u'ItemList' , u'GetRealNames' , u'pList' , ), 8, (8, (), [ (11, 49, 'False', None) , 
			(16393, 10, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , ], 1 , 2 , 4 , 0 , 60 , (3, 0, None, None) , 0 , )),
	(( u'ItemList' , u'GetRealNames' , u'pList' , ), 8, (8, (), [ (11, 49, 'False', None) , 
			(16393, 10, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , ], 1 , 2 , 4 , 0 , 60 , (3, 0, None, None) , 0 , )),
]

ITDProgressNotify_vtables_dispatch_ = 0
ITDProgressNotify_vtables_ = [
	(( u'OnProgress' , u'Current' , u'Total' , u'Message' , ), 1610678272, (1610678272, (), [ 
			(3, 1, None, None) , (3, 1, None, None) , (8, 1, None, None) , ], 1 , 1 , 4 , 0 , 12 , (3, 0, None, None) , 0 , )),
	(( u'OnDataAvailable' , u'ErrorCode' , ), 1610678273, (1610678273, (), [ (19, 1, None, None) , ], 1 , 1 , 4 , 0 , 16 , (3, 0, None, None) , 0 , )),
	(( u'OnServerProgress' , u'Time' , u'Message' , ), 1610678274, (1610678274, (), [ (3, 1, None, None) , 
			(8, 1, None, None) , ], 1 , 1 , 4 , 0 , 20 , (3, 0, None, None) , 0 , )),
	(( u'OnMessage' , u'Message' , ), 1610678275, (1610678275, (), [ (8, 1, None, None) , ], 1 , 1 , 4 , 0 , 24 , (3, 0, None, None) , 0 , )),
]

ITDUtils_vtables_dispatch_ = 1
ITDUtils_vtables_ = [
	(( u'Encrypt' , u'strToEncrypt' , u'encryptedStr' , ), 1, (1, (), [ (8, 0, None, None) , 
			(16392, 10, None, None) , ], 1 , 1 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
	(( u'CurrentTimeStamp' , u'pVal' , ), 3, (3, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 32 , (3, 0, None, None) , 0 , )),
]

ITSScheduler_vtables_dispatch_ = 1
ITSScheduler_vtables_ = [
	(( u'Run' , u'TestData' , ), 1, (1, (), [ (12, 17, None, None) , ], 1 , 1 , 4 , 1 , 28 , (3, 0, None, None) , 0 , )),
	(( u'Stop' , u'TestData' , ), 2, (2, (), [ (12, 17, None, None) , ], 1 , 1 , 4 , 1 , 32 , (3, 0, None, None) , 0 , )),
	(( u'LogEnabled' , u'pVal' , ), 3, (3, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 36 , (3, 0, None, None) , 0 , )),
	(( u'LogEnabled' , u'pVal' , ), 3, (3, (), [ (11, 1, None, None) , ], 1 , 4 , 4 , 0 , 40 , (3, 0, None, None) , 0 , )),
	(( u'TdHostName' , u'pVal' , ), 4, (4, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 44 , (3, 0, None, None) , 0 , )),
	(( u'TdHostName' , u'pVal' , ), 4, (4, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 48 , (3, 0, None, None) , 0 , )),
	(( u'HostTimeOut' , u'pVal' , ), 5, (5, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 52 , (3, 0, None, None) , 0 , )),
	(( u'HostTimeOut' , u'pVal' , ), 5, (5, (), [ (3, 1, None, None) , ], 1 , 4 , 4 , 0 , 56 , (3, 0, None, None) , 0 , )),
	(( u'RunAllLocally' , u'pVal' , ), 6, (6, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 60 , (3, 0, None, None) , 0 , )),
	(( u'RunAllLocally' , u'pVal' , ), 6, (6, (), [ (11, 1, None, None) , ], 1 , 4 , 4 , 0 , 64 , (3, 0, None, None) , 0 , )),
	(( u'ExecutionStatus' , u'pVal' , ), 7, (7, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 68 , (3, 0, None, None) , 0 , )),
	(( u'ExecutionLog' , u'pVal' , ), 8, (8, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 72 , (3, 0, None, None) , 0 , )),
	(( u'RunOnHost' , u'TSTestId' , u'pVal' , ), 9, (9, (), [ (12, 1, None, None) , 
			(16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 76 , (3, 0, None, None) , 0 , )),
	(( u'RunOnHost' , u'TSTestId' , u'pVal' , ), 9, (9, (), [ (12, 1, None, None) , 
			(8, 1, None, None) , ], 1 , 4 , 4 , 0 , 80 , (3, 0, None, None) , 0 , )),
	(( u'VM_Config' , u'TSTestId' , u'pVal' , ), 10, (10, (), [ (12, 1, None, None) , 
			(16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 84 , (3, 0, None, None) , 64 , )),
	(( u'VM_Config' , u'TSTestId' , u'pVal' , ), 10, (10, (), [ (12, 1, None, None) , 
			(8, 1, None, None) , ], 1 , 4 , 4 , 0 , 88 , (3, 0, None, None) , 64 , )),
]

ITSTest_vtables_dispatch_ = 1
ITSTest_vtables_ = [
	(( u'Status' , u'pVal' , ), 14, (14, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 92 , (3, 0, None, None) , 0 , )),
	(( u'Status' , u'pVal' , ), 14, (14, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 96 , (3, 0, None, None) , 0 , )),
	(( u'RunFactory' , u'pVal' , ), 15, (15, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 100 , (3, 0, None, None) , 0 , )),
	(( u'Test' , u'pVal' , ), 16, (16, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 104 , (3, 0, None, None) , 0 , )),
	(( u'HostName' , u'pVal' , ), 17, (17, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 108 , (3, 0, None, None) , 0 , )),
	(( u'HostName' , u'pVal' , ), 17, (17, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 112 , (3, 0, None, None) , 0 , )),
	(( u'Name' , u'pVal' , ), 18, (18, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 116 , (3, 0, None, None) , 0 , )),
	(( u'Type' , u'pVal' , ), 19, (19, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 120 , (3, 0, None, None) , 0 , )),
	(( u'HasSteps' , u'pVal' , ), 20, (20, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 124 , (3, 0, None, None) , 0 , )),
	(( u'ExecutionParams' , u'pVal' , ), 21, (21, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 128 , (3, 0, None, None) , 0 , )),
	(( u'ExecutionParams' , u'pVal' , ), 21, (21, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 132 , (3, 0, None, None) , 0 , )),
	(( u'LastRun' , u'pVal' , ), 22, (22, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 136 , (3, 0, None, None) , 0 , )),
	(( u'Params' , u'pVal' , ), 23, (23, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 140 , (3, 0, None, None) , 0 , )),
	(( u'ExecutionSettings' , u'pVal' , ), 24, (24, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 144 , (3, 0, None, None) , 0 , )),
	(( u'TestId' , u'pVal' , ), 25, (25, (), [ (16396, 10, None, None) , ], 1 , 2 , 4 , 0 , 148 , (3, 0, None, None) , 0 , )),
	(( u'Instance' , u'pVal' , ), 26, (26, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 152 , (3, 0, None, None) , 0 , )),
	(( u'TestName' , u'pVal' , ), 27, (27, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 156 , (3, 0, None, None) , 0 , )),
	(( u'TestSet' , u'pVal' , ), 28, (28, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 160 , (3, 0, None, None) , 0 , )),
]

ITSTest2_vtables_dispatch_ = 1
ITSTest2_vtables_ = [
	(( u'HasCoverage' , u'pVal' , ), 29, (29, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 164 , (3, 0, None, None) , 0 , )),
]

ITest_vtables_dispatch_ = 1
ITest_vtables_ = [
	(( u'FullPath' , u'pVal' , ), 15, (15, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 96 , (3, 0, None, None) , 0 , )),
	(( u'Type' , u'pVal' , ), 16, (16, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 100 , (3, 0, None, None) , 0 , )),
	(( u'Type' , u'pVal' , ), 16, (16, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 104 , (3, 0, None, None) , 0 , )),
	(( u'Name' , u'pVal' , ), 17, (17, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 108 , (3, 0, None, None) , 0 , )),
	(( u'Name' , u'pVal' , ), 17, (17, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 112 , (3, 0, None, None) , 0 , )),
	(( u'DesignStepFactory' , u'pVal' , ), 18, (18, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 116 , (3, 0, None, None) , 0 , )),
	(( u'DesStepsNum' , u'pVal' , ), 19, (19, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 120 , (3, 0, None, None) , 0 , )),
	(( u'CoverRequirement' , u'Req' , u'Order' , u'Recursive' , u'pOrder' , 
			), 20, (20, (), [ (12, 1, None, None) , (3, 1, None, None) , (11, 1, None, None) , (16387, 10, None, None) , ], 1 , 1 , 4 , 0 , 124 , (3, 0, None, None) , 0 , )),
	(( u'RemoveCoverage' , u'Req' , ), 21, (21, (), [ (12, 1, None, None) , ], 1 , 1 , 4 , 0 , 128 , (3, 0, None, None) , 0 , )),
	(( u'GetCoverList' , u'pList' , ), 22, (22, (), [ (16393, 10, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , ], 1 , 1 , 4 , 0 , 132 , (3, 0, None, None) , 0 , )),
	(( u'ExtendedStorage' , u'pVal' , ), 23, (23, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 136 , (3, 0, None, None) , 0 , )),
	(( u'ExecStatus' , u'pVal' , ), 24, (24, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 140 , (3, 0, None, None) , 0 , )),
	(( u'HasCoverage' , u'pVal' , ), 25, (25, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 144 , (3, 0, None, None) , 0 , )),
	(( u'LastRun' , u'pVal' , ), 26, (26, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 148 , (3, 0, None, None) , 0 , )),
	(( u'ExecDate' , u'pDate' , ), 27, (27, (), [ (16391, 10, None, None) , ], 1 , 2 , 4 , 0 , 152 , (3, 0, None, None) , 0 , )),
	(( u'TemplateTest' , u'pVal' , ), 28, (28, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 156 , (3, 0, None, None) , 0 , )),
	(( u'TemplateTest' , u'pVal' , ), 28, (28, (), [ (11, 1, None, None) , ], 1 , 4 , 4 , 0 , 160 , (3, 0, None, None) , 0 , )),
	(( u'Params' , u'pVal' , ), 29, (29, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 164 , (3, 0, None, None) , 0 , )),
	(( u'HasParam' , u'pVal' , ), 30, (30, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 168 , (3, 0, None, None) , 0 , )),
	(( u'VCS' , u'pVal' , ), 31, (31, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 172 , (3, 0, None, None) , 0 , )),
	(( u'CheckoutPathIfExist' , u'pVal' , ), 32, (32, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 176 , (3, 0, None, None) , 0 , )),
	(( u'IgnoreDataHiding' , u'pVal' , ), 33, (33, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 180 , (3, 0, None, None) , 0 , )),
	(( u'IgnoreDataHiding' , u'pVal' , ), 33, (33, (), [ (11, 1, None, None) , ], 1 , 4 , 4 , 0 , 184 , (3, 0, None, None) , 0 , )),
	(( u'FullPathEx' , u'isOrgFullPath' , u'pVal' , ), 34, (34, (), [ (3, 1, None, None) , 
			(16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 188 , (3, 0, None, None) , 64 , )),
]

ITestExecStatus_vtables_dispatch_ = 1
ITestExecStatus_vtables_ = [
	(( u'TestId' , u'pVal' , ), 1, (1, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
	(( u'Message' , u'pVal' , ), 2, (2, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 32 , (3, 0, None, None) , 0 , )),
	(( u'Status' , u'pVal' , ), 3, (3, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 36 , (3, 0, None, None) , 0 , )),
	(( u'TSTestId' , u'pVal' , ), 4, (4, (), [ (16396, 10, None, None) , ], 1 , 2 , 4 , 0 , 40 , (3, 0, None, None) , 0 , )),
	(( u'TestInstance' , u'pVal' , ), 5, (5, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 44 , (3, 0, None, None) , 0 , )),
]

ITestFactory_vtables_dispatch_ = 1
ITestFactory_vtables_ = [
	(( u'BuildSummaryGraph' , u'XAxisField' , u'GroupByField' , u'SumOfField' , u'MaxCols' , 
			u'Filter' , u'ForceRefresh' , u'ShowFullPath' , u'pGraph' , ), 9, (9, (), [ 
			(8, 49, "u''", None) , (8, 49, "u''", None) , (8, 49, "u''", None) , (3, 49, '0', None) , (12, 17, None, None) , 
			(11, 49, 'False', None) , (11, 49, 'False', None) , (16393, 10, None, None) , ], 1 , 1 , 4 , 0 , 68 , (3, 32, None, None) , 0 , )),
	(( u'BuildProgressGraph' , u'GroupByField' , u'SumOfField' , u'MajorSkip' , u'MinorSkip' , 
			u'MaxCols' , u'Filter' , u'FRDate' , u'ForceRefresh' , u'ShowFullPath' , 
			u'pGraph' , ), 10, (10, (), [ (8, 49, "u''", None) , (8, 49, "u''", None) , (3, 49, '0', None) , 
			(3, 49, '1', None) , (3, 49, '0', None) , (12, 17, None, None) , (12, 17, None, None) , (11, 49, 'False', None) , 
			(11, 49, 'False', None) , (16393, 10, None, None) , ], 1 , 1 , 4 , 0 , 72 , (3, 32, None, None) , 0 , )),
	(( u'RepositoryStorage' , u'pVal' , ), 11, (11, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 76 , (3, 0, None, None) , 0 , )),
	(( u'BuildTrendGraph' , u'GroupByField' , u'SumOfField' , u'MaxCols' , u'Filter' , 
			u'FRDate' , u'ForceRefresh' , u'ShowFullPath' , u'pGraph' , ), 12, (12, (), [ 
			(8, 49, "u''", None) , (8, 49, "u''", None) , (3, 49, '0', None) , (12, 17, None, None) , (12, 17, None, None) , 
			(11, 49, 'False', None) , (11, 49, 'False', None) , (16393, 10, None, None) , ], 1 , 1 , 4 , 0 , 80 , (3, 32, None, None) , 0 , )),
	(( u'BuildPerfGraph' , u'GroupByField' , u'SumOfField' , u'MaxCols' , u'Filter' , 
			u'FRDate' , u'ForceRefresh' , u'ShowFullPath' , u'pGraph' , ), 13, (13, (), [ 
			(8, 49, "u''", None) , (8, 49, "u''", None) , (3, 49, '0', None) , (12, 17, None, None) , (12, 17, None, None) , 
			(11, 49, 'False', None) , (11, 49, 'False', None) , (16393, 10, None, None) , ], 1 , 1 , 4 , 0 , 84 , (3, 32, None, None) , 0 , )),
	(( u'IgnoreDataHiding' , u'pVal' , ), 14, (14, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 88 , (3, 0, None, None) , 0 , )),
	(( u'IgnoreDataHiding' , u'pVal' , ), 14, (14, (), [ (11, 1, None, None) , ], 1 , 4 , 4 , 0 , 92 , (3, 0, None, None) , 0 , )),
	(( u'BuildProgressGraphEx' , u'GroupByField' , u'SumOfField' , u'ByHistory' , u'MajorSkip' , 
			u'MinorSkip' , u'MaxCols' , u'Filter' , u'FRDate' , u'ForceRefresh' , 
			u'ShowFullPath' , u'pGraph' , ), 15, (15, (), [ (8, 49, "u''", None) , (8, 49, "u''", None) , 
			(11, 49, 'True', None) , (3, 49, '0', None) , (3, 49, '1', None) , (3, 49, '0', None) , (12, 17, None, None) , 
			(12, 17, None, None) , (11, 49, 'False', None) , (11, 49, 'False', None) , (16393, 10, None, None) , ], 1 , 1 , 4 , 0 , 96 , (3, 32, None, None) , 0 , )),
]

ITestSet_vtables_dispatch_ = 1
ITestSet_vtables_ = [
	(( u'Name' , u'pVal' , ), 14, (14, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 92 , (3, 0, None, None) , 0 , )),
	(( u'Name' , u'pVal' , ), 14, (14, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 96 , (3, 0, None, None) , 0 , )),
	(( u'Status' , u'pVal' , ), 15, (15, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 100 , (3, 0, None, None) , 0 , )),
	(( u'Status' , u'pVal' , ), 15, (15, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 104 , (3, 0, None, None) , 0 , )),
	(( u'TSTestFactory' , u'pVal' , ), 16, (16, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 108 , (3, 0, None, None) , 0 , )),
	(( u'ConditionFactory' , u'pVal' , ), 17, (17, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 112 , (3, 0, None, None) , 0 , )),
	(( u'BuildSummaryGraph' , u'XAxisField' , u'GroupByField' , u'SumOfField' , u'MaxCols' , 
			u'Filter' , u'ForceRefresh' , u'ShowFullPath' , u'pGraph' , ), 18, (18, (), [ 
			(8, 49, "u''", None) , (8, 49, "u''", None) , (8, 49, "u''", None) , (3, 49, '0', None) , (12, 17, None, None) , 
			(11, 49, 'False', None) , (11, 49, 'False', None) , (16393, 10, None, None) , ], 1 , 1 , 4 , 0 , 116 , (3, 32, None, None) , 0 , )),
	(( u'BuildProgressGraph' , u'GroupByField' , u'SumOfField' , u'MajorSkip' , u'MinorSkip' , 
			u'MaxCols' , u'Filter' , u'FRDate' , u'ForceRefresh' , u'ShowFullPath' , 
			u'pGraph' , ), 19, (19, (), [ (8, 49, "u''", None) , (8, 49, "u''", None) , (3, 49, '0', None) , 
			(3, 49, '1', None) , (3, 49, '0', None) , (12, 17, None, None) , (12, 17, None, None) , (11, 49, 'False', None) , 
			(11, 49, 'False', None) , (16393, 10, None, None) , ], 1 , 1 , 4 , 0 , 120 , (3, 32, None, None) , 0 , )),
	(( u'StartExecution' , u'ServerName' , u'Scheduler' , ), 20, (20, (), [ (8, 1, None, None) , 
			(16393, 10, None, None) , ], 1 , 1 , 4 , 0 , 124 , (3, 0, None, None) , 0 , )),
	(( u'PurgeExecutions' , ), 21, (21, (), [ ], 1 , 1 , 4 , 0 , 128 , (3, 0, None, None) , 0 , )),
	(( u'ExecEventNotifyByMailSettings' , u'pVal' , ), 22, (22, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 132 , (3, 0, None, None) , 0 , )),
	(( u'ExecutionSettings' , u'pVal' , ), 23, (23, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 136 , (3, 0, None, None) , 0 , )),
	(( u'TestDefaultExecutionSettings' , u'pVal' , ), 24, (24, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 140 , (3, 0, None, None) , 0 , )),
	(( u'BuildTrendGraph' , u'GroupByField' , u'SumOfField' , u'MaxCols' , u'Filter' , 
			u'FRDate' , u'ForceRefresh' , u'ShowFullPath' , u'pGraph' , ), 25, (25, (), [ 
			(8, 49, "u''", None) , (8, 49, "u''", None) , (3, 49, '0', None) , (12, 17, None, None) , (12, 17, None, None) , 
			(11, 49, 'False', None) , (11, 49, 'False', None) , (16393, 10, None, None) , ], 1 , 1 , 4 , 0 , 144 , (3, 32, None, None) , 0 , )),
	(( u'BuildPerfGraph' , u'GroupByField' , u'SumOfField' , u'MaxCols' , u'Filter' , 
			u'FRDate' , u'ForceRefresh' , u'ShowFullPath' , u'pGraph' , ), 26, (26, (), [ 
			(8, 49, "u''", None) , (8, 49, "u''", None) , (3, 49, '0', None) , (12, 17, None, None) , (12, 17, None, None) , 
			(11, 49, 'False', None) , (11, 49, 'False', None) , (16393, 10, None, None) , ], 1 , 1 , 4 , 0 , 148 , (3, 32, None, None) , 0 , )),
	(( u'ResetTestSet' , u'DeleteRuns' , ), 27, (27, (), [ (11, 1, None, None) , ], 1 , 1 , 4 , 0 , 152 , (3, 0, None, None) , 0 , )),
	(( u'CheckTestInstances' , u'TestIDs' , u'pVal' , ), 28, (28, (), [ (8, 1, None, None) , 
			(16392, 10, None, None) , ], 1 , 1 , 4 , 0 , 156 , (3, 0, None, None) , 0 , )),
	(( u'BuildProgressGraphEx' , u'GroupByField' , u'SumOfField' , u'ByHistory' , u'MajorSkip' , 
			u'MinorSkip' , u'MaxCols' , u'Filter' , u'FRDate' , u'ForceRefresh' , 
			u'ShowFullPath' , u'pGraph' , ), 29, (29, (), [ (8, 49, "u''", None) , (8, 49, "u''", None) , 
			(11, 49, 'True', None) , (3, 49, '0', None) , (3, 49, '1', None) , (3, 49, '0', None) , (12, 17, None, None) , 
			(12, 17, None, None) , (11, 49, 'False', None) , (11, 49, 'False', None) , (16393, 10, None, None) , ], 1 , 1 , 4 , 0 , 160 , (3, 32, None, None) , 0 , )),
	(( u'TestSetFolder' , u'pNode' , ), 30, (30, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 164 , (3, 0, None, None) , 0 , )),
	(( u'TestSetFolder' , u'pNode' , ), 30, (30, (), [ (12, 1, None, None) , ], 1 , 4 , 4 , 0 , 168 , (3, 0, None, None) , 0 , )),
]

ITestSetFactory_vtables_dispatch_ = 1
ITestSetFactory_vtables_ = [
	(( u'BuildSummaryGraph' , u'TestSetID' , u'XAxisField' , u'GroupByField' , u'SumOfField' , 
			u'MaxCols' , u'Filter' , u'ForceRefresh' , u'ShowFullPath' , u'pGraph' , 
			), 8, (8, (), [ (3, 1, None, None) , (8, 49, "u''", None) , (8, 49, "u''", None) , (8, 49, "u''", None) , 
			(3, 49, '0', None) , (12, 17, None, None) , (11, 49, 'False', None) , (11, 49, 'False', None) , (16393, 10, None, None) , ], 1 , 1 , 4 , 0 , 64 , (3, 32, None, None) , 0 , )),
	(( u'BuildProgressGraph' , u'TestSetID' , u'GroupByField' , u'SumOfField' , u'MajorSkip' , 
			u'MinorSkip' , u'MaxCols' , u'Filter' , u'FRDate' , u'ForceRefresh' , 
			u'ShowFullPath' , u'pGraph' , ), 9, (9, (), [ (3, 1, None, None) , (8, 49, "u''", None) , 
			(8, 49, "u''", None) , (3, 49, '0', None) , (3, 49, '1', None) , (3, 49, '0', None) , (12, 17, None, None) , 
			(12, 17, None, None) , (11, 49, 'False', None) , (11, 49, 'False', None) , (16393, 10, None, None) , ], 1 , 1 , 4 , 0 , 68 , (3, 32, None, None) , 0 , )),
	(( u'BuildTrendGraph' , u'TestSetID' , u'GroupByField' , u'SumOfField' , u'MaxCols' , 
			u'Filter' , u'FRDate' , u'ForceRefresh' , u'ShowFullPath' , u'pGraph' , 
			), 10, (10, (), [ (3, 1, None, None) , (8, 49, "u''", None) , (8, 49, "u''", None) , (3, 49, '0', None) , 
			(12, 17, None, None) , (12, 17, None, None) , (11, 49, 'False', None) , (11, 49, 'False', None) , (16393, 10, None, None) , ], 1 , 1 , 4 , 0 , 72 , (3, 32, None, None) , 0 , )),
	(( u'BuildPerfGraph' , u'TestSetID' , u'GroupByField' , u'SumOfField' , u'MaxCols' , 
			u'Filter' , u'FRDate' , u'ForceRefresh' , u'ShowFullPath' , u'pGraph' , 
			), 11, (11, (), [ (3, 1, None, None) , (8, 49, "u''", None) , (8, 49, "u''", None) , (3, 49, '0', None) , 
			(12, 17, None, None) , (12, 17, None, None) , (11, 49, 'False', None) , (11, 49, 'False', None) , (16393, 10, None, None) , ], 1 , 1 , 4 , 0 , 76 , (3, 32, None, None) , 0 , )),
	(( u'BuildProgressGraphEx' , u'TestSetID' , u'GroupByField' , u'SumOfField' , u'ByHistory' , 
			u'MajorSkip' , u'MinorSkip' , u'MaxCols' , u'Filter' , u'FRDate' , 
			u'ForceRefresh' , u'ShowFullPath' , u'pGraph' , ), 12, (12, (), [ (3, 1, None, None) , 
			(8, 49, "u''", None) , (8, 49, "u''", None) , (11, 49, 'True', None) , (3, 49, '0', None) , (3, 49, '1', None) , 
			(3, 49, '0', None) , (12, 17, None, None) , (12, 17, None, None) , (11, 49, 'False', None) , (11, 49, 'False', None) , 
			(16393, 10, None, None) , ], 1 , 1 , 4 , 0 , 80 , (3, 32, None, None) , 0 , )),
]

ITestSetFolder_vtables_dispatch_ = 1
ITestSetFolder_vtables_ = [
	(( u'Description' , u'pVal' , ), 15, (15, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 92 , (3, 0, None, None) , 0 , )),
	(( u'Description' , u'pVal' , ), 15, (15, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 96 , (3, 0, None, None) , 0 , )),
	(( u'FatherID' , u'pVal' , ), 16, (16, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 100 , (3, 0, None, None) , 0 , )),
	(( u'FatherDisp' , u'pNode' , ), 17, (17, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 104 , (3, 0, None, None) , 0 , )),
	(( u'ViewOrder' , u'pVal' , ), 18, (18, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 108 , (3, 0, None, None) , 0 , )),
	(( u'ViewOrder' , u'pVal' , ), 18, (18, (), [ (3, 1, None, None) , ], 1 , 4 , 4 , 0 , 112 , (3, 0, None, None) , 0 , )),
	(( u'SubNodes' , u'pList' , ), 19, (19, (), [ (16393, 10, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , ], 1 , 2 , 4 , 0 , 116 , (3, 0, None, None) , 0 , )),
	(( u'AddNodeDisp' , u'NodeName' , u'pNode' , ), 20, (20, (), [ (8, 1, None, None) , 
			(16393, 10, None, None) , ], 1 , 1 , 4 , 0 , 120 , (3, 0, None, None) , 0 , )),
	(( u'RemoveNodeEx' , u'Node' , u'DeleteTestSets' , ), 21, (21, (), [ (12, 1, None, None) , 
			(11, 49, 'False', None) , ], 1 , 1 , 4 , 0 , 124 , (3, 0, None, None) , 0 , )),
	(( u'Move' , u'Father' , ), 22, (22, (), [ (12, 1, None, None) , ], 1 , 1 , 4 , 0 , 128 , (3, 0, None, None) , 0 , )),
	(( u'TestSetFactory' , u'pVal' , ), 23, (23, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 132 , (3, 0, None, None) , 0 , )),
	(( u'HasAttachment' , u'pVal' , ), 24, (24, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 136 , (3, 0, None, None) , 0 , )),
	(( u'Attachments' , u'pVal' , ), 25, (25, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 140 , (3, 0, None, None) , 0 , )),
	(( u'FindTestSets' , u'Pattern' , u'MatchCase' , u'Filter' , u'pList' , 
			), 26, (26, (), [ (8, 1, None, None) , (11, 49, 'False', None) , (8, 49, "u''", None) , (16393, 10, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , ], 1 , 1 , 4 , 0 , 144 , (3, 32, None, None) , 0 , )),
]

ITestSetFolder2_vtables_dispatch_ = 1
ITestSetFolder2_vtables_ = [
	(( u'FindTestInstances' , u'Pattern' , u'MatchCase' , u'Filter' , u'pList' , 
			), 27, (27, (), [ (8, 1, None, None) , (11, 49, 'False', None) , (8, 49, "u''", None) , (16393, 10, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , ], 1 , 1 , 4 , 0 , 148 , (3, 32, None, None) , 0 , )),
]

ITestSetTreeManager_vtables_dispatch_ = 1
ITestSetTreeManager_vtables_ = [
	(( u'Root' , u'pVal' , ), 0, (0, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
	(( u'NodeByPath' , u'Path' , u'pVal' , ), 1, (1, (), [ (8, 0, None, None) , 
			(16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 32 , (3, 0, None, None) , 0 , )),
	(( u'NodeById' , u'NodeID' , u'pVal' , ), 2, (2, (), [ (3, 0, None, None) , 
			(16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 36 , (3, 0, None, None) , 0 , )),
	(( u'Unattached' , u'pVal' , ), 3, (3, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 40 , (3, 0, None, None) , 0 , )),
]

ITextParser_vtables_dispatch_ = 1
ITextParser_vtables_ = [
	(( u'Count' , u'pVal' , ), 1, (1, (), [ (16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
	(( u'ParamValue' , u'vParam' , u'pVal' , ), 2, (2, (), [ (12, 1, None, None) , 
			(16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 32 , (3, 0, None, None) , 0 , )),
	(( u'ParamValue' , u'vParam' , u'pVal' , ), 2, (2, (), [ (12, 1, None, None) , 
			(8, 1, None, None) , ], 1 , 4 , 4 , 0 , 36 , (3, 0, None, None) , 0 , )),
	(( u'ClearParam' , u'vParam' , ), 3, (3, (), [ (12, 1, None, None) , ], 1 , 1 , 4 , 0 , 40 , (3, 0, None, None) , 0 , )),
	(( u'Type' , u'vParam' , u'pVal' , ), 4, (4, (), [ (12, 1, None, None) , 
			(16387, 10, None, None) , ], 1 , 2 , 4 , 0 , 44 , (3, 0, None, None) , 0 , )),
	(( u'ParamType' , u'vParam' , u'pVal' , ), 5, (5, (), [ (12, 1, None, None) , 
			(16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 48 , (3, 0, None, None) , 0 , )),
	(( u'ParamExist' , u'ParamName' , u'pVal' , ), 6, (6, (), [ (8, 1, None, None) , 
			(16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 52 , (3, 0, None, None) , 0 , )),
	(( u'ParamName' , u'nPosition' , u'pVal' , ), 7, (7, (), [ (3, 1, None, None) , 
			(16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 56 , (3, 0, None, None) , 0 , )),
	(( u'Initialize' , u'StartClose' , u'EndClose' , u'TypeClose' , u'MaxLen' , 
			u'DefaultType' , ), 8, (8, (), [ (8, 49, "u'<%'", None) , (8, 49, "u'%>'", None) , (8, 49, "u'?'", None) , 
			(3, 49, '-1', None) , (8, 49, "u'string'", None) , ], 1 , 1 , 4 , 0 , 60 , (3, 32, None, None) , 0 , )),
	(( u'Text' , u'pVal' , ), 9, (9, (), [ (8, 1, None, None) , ], 1 , 4 , 4 , 0 , 64 , (3, 0, None, None) , 0 , )),
	(( u'Text' , u'pVal' , ), 9, (9, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 68 , (3, 0, None, None) , 0 , )),
	(( u'EvaluateText' , ), 10, (10, (), [ ], 1 , 1 , 4 , 0 , 72 , (3, 0, None, None) , 0 , )),
]

ITreeManager_vtables_dispatch_ = 1
ITreeManager_vtables_ = [
	(( u'TreeRoot' , u'RootName' , u'pVal' , ), 0, (0, (), [ (8, 0, None, None) , 
			(16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
	(( u'NodePath' , u'NodeID' , u'pVal' , ), 1, (1, (), [ (3, 0, None, None) , 
			(16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 32 , (3, 0, None, None) , 0 , )),
	(( u'NodeByPath' , u'Path' , u'pVal' , ), 2, (2, (), [ (8, 0, None, None) , 
			(16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 36 , (3, 0, None, None) , 0 , )),
	(( u'NodeById' , u'NodeID' , u'pVal' , ), 3, (3, (), [ (3, 0, None, None) , 
			(16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 40 , (3, 0, None, None) , 0 , )),
	(( u'RootList' , u'Val' , u'pVal' , ), 4, (4, (), [ (2, 49, '0', None) , 
			(16393, 10, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , ], 1 , 2 , 4 , 0 , 44 , (3, 0, None, None) , 0 , )),
	(( u'RootList' , u'Val' , u'pVal' , ), 4, (4, (), [ (2, 49, '0', None) , 
			(16393, 10, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , ], 1 , 2 , 4 , 0 , 44 , (3, 0, None, None) , 0 , )),
]

IUnitTestSupport_vtables_dispatch_ = 0
IUnitTestSupport_vtables_ = [
	(( u'UnitTestEnviromentOption' , u'newVal' , ), 1610678272, (1610678272, (), [ (3, 1, None, None) , ], 1 , 1 , 4 , 0 , 12 , (3, 0, None, None) , 0 , )),
	(( u'UnitTestProfile' , u'newVal' , ), 1610678273, (1610678273, (), [ (8, 1, None, None) , ], 1 , 1 , 4 , 0 , 16 , (3, 0, None, None) , 0 , )),
]

IVCS_vtables_dispatch_ = 1
IVCS_vtables_ = [
	(( u'Version' , u'pVal' , ), 1, (1, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
	(( u'IsCheckedOut' , u'pVal' , ), 2, (2, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 32 , (3, 0, None, None) , 0 , )),
	(( u'IsLocked' , u'pVal' , ), 3, (3, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 36 , (3, 0, None, None) , 0 , )),
	(( u'AddObjectToVcs' , u'Version' , u'Comments' , ), 4, (4, (), [ (8, 49, "u'1.1.1'", None) , 
			(8, 49, "u'Object Created in VCS'", None) , ], 1 , 1 , 4 , 0 , 40 , (3, 32, None, None) , 0 , )),
	(( u'DeleteObjectFromVCS' , ), 5, (5, (), [ ], 1 , 1 , 4 , 0 , 44 , (3, 0, None, None) , 0 , )),
	(( u'CheckOut' , u'Version' , u'Comment' , u'Lock' , u'ReadOnly' , 
			u'Sync' , ), 6, (6, (), [ (8, 1, None, None) , (8, 1, None, None) , (11, 1, None, None) , 
			(11, 49, 'False', None) , (11, 49, 'True', None) , ], 1 , 1 , 4 , 0 , 48 , (3, 0, None, None) , 0 , )),
	(( u'CheckIn' , u'Version' , u'Comments' , u'Remove' , u'SetCurrent' , 
			), 7, (7, (), [ (8, 1, None, None) , (8, 1, None, None) , (11, 49, 'True', None) , (11, 49, 'True', None) , ], 1 , 1 , 4 , 0 , 52 , (3, 0, None, None) , 0 , )),
	(( u'SetCurrentVersion' , u'Version' , ), 8, (8, (), [ (8, 1, None, None) , ], 1 , 1 , 4 , 0 , 56 , (3, 0, None, None) , 0 , )),
	(( u'LockVcsObject' , ), 9, (9, (), [ ], 1 , 1 , 4 , 0 , 60 , (3, 0, None, None) , 0 , )),
	(( u'UndoCheckout' , u'Remove' , ), 10, (10, (), [ (11, 1, None, None) , ], 1 , 1 , 4 , 0 , 64 , (3, 0, None, None) , 0 , )),
	(( u'CurrentVersion' , u'pVal' , ), 11, (11, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 68 , (3, 0, None, None) , 0 , )),
	(( u'CheckoutInfo' , u'pVal' , ), 12, (12, (), [ (16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 72 , (3, 0, None, None) , 0 , )),
	(( u'Versions' , u'pVal' , ), 13, (13, (), [ (16393, 10, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , ], 1 , 2 , 4 , 0 , 76 , (3, 0, None, None) , 0 , )),
	(( u'LockedBy' , u'pVal' , ), 14, (14, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 80 , (3, 0, None, None) , 0 , )),
	(( u'VersionInfo' , u'Version' , u'pVal' , ), 15, (15, (), [ (8, 0, None, None) , 
			(16393, 10, None, None) , ], 1 , 2 , 4 , 0 , 84 , (3, 0, None, None) , 0 , )),
	(( u'ViewVersion' , u'bstrVersion' , u'bstrViewPath' , ), 16, (16, (), [ (8, 1, None, None) , 
			(16392, 3, None, None) , ], 1 , 1 , 4 , 0 , 88 , (3, 0, None, None) , 64 , )),
	(( u'ClearView' , u'bstrVersion' , ), 17, (17, (), [ (8, 49, "u''", None) , ], 1 , 1 , 4 , 0 , 92 , (3, 32, None, None) , 64 , )),
	(( u'VersionsEx' , u'pVal' , ), 18, (18, (), [ (16393, 10, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , ], 1 , 2 , 4 , 0 , 96 , (3, 0, None, None) , 0 , )),
	(( u'Refresh' , ), 19, (19, (), [ ], 1 , 1 , 4 , 0 , 100 , (3, 0, None, None) , 0 , )),
	(( u'CheckInEx' , u'Version' , u'Comments' , u'Remove' , u'SetCurrent' , 
			u'ForceCheckin' , u'Reserved' , ), 20, (20, (), [ (8, 1, None, None) , (8, 1, None, None) , 
			(11, 49, 'True', None) , (11, 49, 'True', None) , (11, 49, 'False', None) , (19, 49, '0', None) , ], 1 , 1 , 4 , 0 , 104 , (3, 0, None, None) , 0 , )),
]

IVcsAdmin_vtables_dispatch_ = 1
IVcsAdmin_vtables_ = [
]

IVersionItem_vtables_dispatch_ = 1
IVersionItem_vtables_ = [
	(( u'Comments' , u'pVal' , ), 1, (1, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
	(( u'Version' , u'pVal' , ), 2, (2, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 32 , (3, 0, None, None) , 0 , )),
	(( u'IsLocked' , u'pVal' , ), 3, (3, (), [ (16395, 10, None, None) , ], 1 , 2 , 4 , 0 , 36 , (3, 0, None, None) , 0 , )),
	(( u'Date' , u'pVal' , ), 4, (4, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 40 , (3, 0, None, None) , 0 , )),
	(( u'User' , u'pVal' , ), 5, (5, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 44 , (3, 0, None, None) , 0 , )),
	(( u'Time' , u'pVal' , ), 6, (6, (), [ (16392, 10, None, None) , ], 1 , 2 , 4 , 0 , 48 , (3, 0, None, None) , 0 , )),
]

_IBaseFactory_vtables_dispatch_ = 0
_IBaseFactory_vtables_ = [
	(( u'ListRefresh' , u'Filter' , u'NewObjectsArray' , ), 1610678272, (1610678272, (), [ (8, 1, None, None) , 
			(16396, 2, None, None) , ], 1 , 1 , 4 , 0 , 12 , (3, 0, None, None) , 0 , )),
	(( u'RemoveCachedList' , u'Filter' , ), 1610678273, (1610678273, (), [ (8, 1, None, None) , ], 1 , 1 , 4 , 0 , 16 , (3, 0, None, None) , 0 , )),
	(( u'GetCachedList' , u'Filter' , u'pList' , ), 1610678274, (1610678274, (), [ (8, 1, None, None) , 
			(16393, 2, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , ], 1 , 1 , 4 , 0 , 20 , (3, 0, None, None) , 0 , )),
	(( u'PutCachedList' , u'Filter' , u'pList' , ), 1610678275, (1610678275, (), [ (8, 1, None, None) , 
			(9, 1, None, "IID('{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}')") , ], 1 , 1 , 4 , 0 , 24 , (3, 0, None, None) , 0 , )),
]

_IBaseField_vtables_dispatch_ = 0
_IBaseField_vtables_ = [
	(( u'_SetNeighborhood_' , u'Vector' , u'Index' , ), 1610678272, (1610678272, (), [ (3, 1, None, None) , 
			(3, 1, None, None) , ], 1 , 1 , 4 , 0 , 12 , (3, 0, None, None) , 0 , )),
	(( u'_BeforePost_' , u'pVal' , u'pObject' , ), 1610678273, (1610678273, (), [ (16392, 2, None, None) , 
			(9, 1, None, None) , ], 1 , 1 , 4 , 0 , 16 , (3, 0, None, None) , 0 , )),
	(( u'_DoPost_' , u'Data' , u'Result' , ), 1610678274, (1610678274, (), [ (8, 1, None, None) , 
			(16392, 2, None, None) , ], 1 , 1 , 4 , 0 , 20 , (3, 0, None, None) , 0 , )),
	(( u'_AfterPost_' , u'Key' , u'pObject' , ), 1610678275, (1610678275, (), [ (8, 1, None, None) , 
			(9, 1, None, None) , ], 1 , 1 , 4 , 0 , 24 , (3, 0, None, None) , 0 , )),
	(( u'_SetRequired_' , u'FieldName' , u'Val' , ), 1610678276, (1610678276, (), [ (8, 1, None, None) , 
			(11, 1, None, None) , ], 1 , 1 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
	(( u'_GetRequired_' , u'FieldName' , u'Val' , ), 1610678277, (1610678277, (), [ (8, 1, None, None) , 
			(16395, 10, None, None) , ], 1 , 1 , 4 , 0 , 32 , (3, 0, None, None) , 0 , )),
	(( u'_RestoreRequired_' , u'FieldName' , ), 1610678278, (1610678278, (), [ (8, 1, None, None) , ], 1 , 1 , 4 , 0 , 36 , (3, 0, None, None) , 0 , )),
	(( u'_SetRoot_' , u'FieldName' , u'Val' , ), 1610678279, (1610678279, (), [ (8, 1, None, None) , 
			(9, 1, None, None) , ], 1 , 1 , 4 , 0 , 40 , (3, 0, None, None) , 0 , )),
	(( u'_GetRoot_' , u'FieldName' , u'Val' , ), 1610678280, (1610678280, (), [ (8, 1, None, None) , 
			(16393, 10, None, None) , ], 1 , 1 , 4 , 0 , 44 , (3, 0, None, None) , 0 , )),
	(( u'_RestoreRoot_' , u'FieldName' , ), 1610678281, (1610678281, (), [ (8, 1, None, None) , ], 1 , 1 , 4 , 0 , 48 , (3, 0, None, None) , 0 , )),
	(( u'_put_Field' , u'FieldName' , u'newVal' , ), 1610678282, (1610678282, (), [ (8, 1, None, None) , 
			(12, 1, None, None) , ], 1 , 1 , 4 , 0 , 52 , (3, 0, None, None) , 0 , )),
	(( u'_VerifyPutField' , u'FieldName' , u'newVal' , u'Val' , ), 1610678283, (1610678283, (), [ 
			(8, 1, None, None) , (12, 1, None, None) , (16395, 10, None, None) , ], 1 , 1 , 4 , 0 , 56 , (3, 0, None, None) , 0 , )),
]

_ITestSetFolder_vtables_dispatch_ = 0
_ITestSetFolder_vtables_ = [
	(( u'_FindChildNode_' , u'ChildName' , u'Recursive' , u'pNode' , u'pIndex' , 
			), 1610678272, (1610678272, (), [ (8, 1, None, None) , (11, 1, None, None) , (16393, 2, None, "IID('{E9E2BCFE-CAAC-492D-A210-C2E49A68C78F}')") , (16387, 2, None, None) , ], 1 , 1 , 4 , 0 , 12 , (3, 0, None, None) , 0 , )),
	(( u'_IsInitialized_' , u'pVal' , ), 1610678273, (1610678273, (), [ (16395, 10, None, None) , ], 1 , 1 , 4 , 0 , 16 , (3, 0, None, None) , 0 , )),
	(( u'_SetNodeData_' , u'NodeData' , ), 1610678274, (1610678274, (), [ (8, 1, None, None) , ], 1 , 1 , 4 , 0 , 20 , (3, 0, None, None) , 0 , )),
	(( u'_MoveChild_' , u'Node' , u'Order' , ), 1610678275, (1610678275, (), [ (9, 1, None, None) , 
			(11, 1, None, None) , ], 1 , 1 , 4 , 0 , 24 , (3, 0, None, None) , 0 , )),
]

_ITreeNode_vtables_dispatch_ = 0
_ITreeNode_vtables_ = [
	(( u'_GetFatherID_' , u'pVal' , ), 1610678272, (1610678272, (), [ (16387, 2, None, None) , ], 1 , 1 , 4 , 0 , 12 , (3, 0, None, None) , 0 , )),
	(( u'_FindChildNode_' , u'ChildName' , u'Recusive' , u'pNode' , ), 1610678273, (1610678273, (), [ 
			(8, 1, None, None) , (11, 1, None, None) , (16393, 2, None, "IID('{8538CCBA-0B3E-481F-BB2B-14F0553CA146}')") , ], 1 , 1 , 4 , 0 , 16 , (3, 0, None, None) , 0 , )),
	(( u'_IsInitialized_' , u'pVal' , ), 1610678274, (1610678274, (), [ (16395, 10, None, None) , ], 1 , 1 , 4 , 0 , 20 , (3, 0, None, None) , 0 , )),
	(( u'_SetNodeData_' , u'NodeData' , ), 1610678275, (1610678275, (), [ (8, 1, None, None) , ], 1 , 1 , 4 , 0 , 24 , (3, 0, None, None) , 0 , )),
	(( u'_MoveChild' , u'Node' , u'Order' , ), 1610678276, (1610678276, (), [ (9, 1, None, None) , 
			(11, 1, None, None) , ], 1 , 1 , 4 , 0 , 28 , (3, 0, None, None) , 0 , )),
]

RecordMap = {
}

CLSIDToClassMap = {
	'{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}' : IList,
	'{8538CCBA-0B3E-481F-BB2B-14F0553CA146}' : ISysTreeNode,
	'{5BB6D957-669D-47B2-8324-981492EDC42E}' : ITestFactory,
	'{AAFCDC5B-5E1A-4E41-829A-225C14BB0C88}' : ReqCoverageFactory,
	'{992BABA3-6360-4BD1-A337-B75F67BDB417}' : IStep,
	'{133B9314-9A98-4659-866D-4BA8DCD4A643}' : ITSTest2,
	'{3E46BE58-4943-48AA-BA08-38EEBA837A04}' : IBaseFactory2,
	'{9007A7F1-AC71-4563-A943-CFF4051E7E3D}' : List,
	'{0409CEAA-2CAA-4596-A9EF-B9F0644D17EA}' : ExecEventInfo,
	'{7A6F279C-7729-4394-B5BB-BC3E8902DA56}' : IComponentFolder,
	'{3A24FCD2-D5E3-4B5E-82BE-2202DD566FAD}' : RunFactory,
	'{D2FA8791-3A81-4D03-95CA-74EF6E059C4F}' : IComFrec,
	'{904CED76-CF4A-4C85-BB23-2B4A9DCB1D6A}' : GroupingItem,
	'{5073A186-2A46-4C93-A3F5-F8C0AA66694F}' : IAmarillusHash,
	'{C5CBD7B2-490C-45F5-8C40-B8C3D108E6D7}' : TDConnection,
	'{80A02CF4-53FB-499C-9549-2EE19815179A}' : CustomizationListNode,
	'{B2F590F7-BD30-45DD-90B7-F243D7E8B210}' : ComFrec,
	'{46C05B73-AAE3-4420-9B38-6F221471632D}' : ComponentStepFactory,
	'{36F5E5F1-C949-40AF-91E8-A4BC647B2A92}' : ComponentFolderFactory,
	'{6E67A18A-81DD-4546-9D64-10701F5E9558}' : BPIteration,
	'{49B715FA-458E-46EA-A171-0E0BFB38B3AF}' : ILinkFactory,
	'{8C63633A-6537-41B5-93A3-982823DE8689}' : IConditionFactory,
	'{1EE78B77-A68F-49D8-9707-9A7C8CEA10D2}' : IActionPermission,
	'{AAF726A2-9187-4DF5-9528-A3E66CDD28E2}' : TextParser,
	'{38D402FA-9823-4D25-979E-62C377AC2E77}' : IVersionItem,
	'{4592C936-2524-4D05-9978-95901EDE0A54}' : IReq,
	'{DCA4F956-F4B7-43BB-B544-58E56D0DEDC8}' : HierarchyFilter,
	'{6BA5E493-366C-4068-B572-93D27D405DFE}' : IBPStepParam,
	'{DEBC6003-2ABE-4C93-A769-646F300A7986}' : ITDErrorInfo,
	'{67CDC9EF-6EEB-4E19-80E1-77855970409A}' : BPParameter,
	'{FA8C3437-3B5B-4246-B775-523DEEB2734A}' : IHostGroupFactory,
	'{B2FDDFE1-6019-4444-9EE6-479FA0E554A2}' : Rule,
	'{C121682C-6B6E-4A3F-A1CE-9E78FE897009}' : TSScheduler,
	'{7EE7D348-AED6-4B3C-8FC7-5D9D8EA8E4C9}' : ICustomizationPermissions,
	'{452897AD-D9F8-4EAE-80DB-B9C11807507F}' : ITDFilter,
	'{59A79946-0678-4E59-B4B0-9967E4314CCA}' : IProjectProperties,
	'{F4332BCF-1D24-4F24-AD5E-FEDEC7FAFBB6}' : CustomizationTransitionRule,
	'{E2F29752-72F0-42DB-995C-3DB385F4CCE5}' : IBaseField,
	'{33CCFC63-CE02-47AB-9A11-BB2E0C324723}' : IGroupingManager,
	'{5DB9FED8-D26F-4F27-911F-99216ED75986}' : BugFactory,
	'{1E1C07CA-7339-4741-A41D-DEC5A065B1F1}' : ICustomizationList,
	'{71CFAB12-C911-4E7A-B4CE-3F65E45344A1}' : Component,
	'{DCB4C421-E9F4-4A89-9190-B49411B17167}' : Export,
	'{16EF2BF4-8509-475E-B34E-3BF99C221221}' : IRun2,
	'{D398384F-9D08-4E70-8400-910D8750BDAB}' : TestFactory,
	'{E60B4439-691C-433D-B6D9-793044915A01}' : ITreeManager,
	'{557AF6E9-FB13-4934-9A8F-6620BA38C547}' : IAnalysis,
	'{7300228B-0F1B-44D3-BF3A-790253622527}' : SubjectNode,
	'{37B25CC5-C319-4DAD-A57D-05A8D1201EE0}' : FileData,
	'{F0D306D2-2BE8-43E8-B229-9B8EA279B8D5}' : BPIterationParam,
	'{6F671E95-2720-4583-BCD8-7CC3E5D3C4AF}' : TDFilter,
	'{BE73DDAA-AD0F-4A89-8936-0EDA17599273}' : IBaseField2,
	'{34881384-E4DD-40D2-AF7B-F7A14D226F2D}' : ChangeEntry,
	'{CCD9F62A-3899-4233-9C2C-A25BFF1AF041}' : IReqSummaryStatus,
	'{54770024-A5FB-4710-8A9E-64A9A0DCBCFA}' : ReqCoverageStatus,
	'{79CFC0C1-7BB6-48CE-BC47-8074DDBCA542}' : ILink,
	'{6EE10992-2569-4838-86DA-DAE1E1240E79}' : IFollowUpManager,
	'{A74CE8AE-2AA0-47F4-A85E-A6E96DA8EA1E}' : CustomizationActions,
	'{DCFC6A77-C0F7-4F36-82E2-5164749254AF}' : ICustomizationUsers,
	'{15FBB8D4-7034-413E-A8F0-1E03B7FA4F0B}' : IHistory,
	'{E1582AF7-780E-4D93-9771-E196205F96C7}' : IBPHistoryRecord,
	'{FD53A549-A095-469F-AD8E-95F9F034D2DF}' : ITestSetTreeManager,
	'{2444C43F-5371-4B4A-B6B6-34D205644C35}' : ISearchOptions,
	'{20F3E82A-9A7D-44FA-94C6-03E84C49521D}' : TDField,
	'{C1ED849E-CB41-45A9-A256-9A0D3CFDB350}' : ITestExecStatus,
	'{96615E7A-A7C4-4B6F-A00A-418B92070F83}' : IGraphDefinition,
	'{8CEB0A26-08A1-4110-A851-6BB50278BF94}' : Graph,
	'{74B94507-8A42-4D11-B5BD-943B76C9A982}' : IChange,
	'{9D02F17C-BECD-4845-AA04-5326832104AA}' : AttachmentFactory,
	'{833093EE-C983-46F7-88BF-DE5D7E2FCBBD}' : IReqFactory,
	'{E5A4F777-79B4-4999-B9CF-4733946D6381}' : ComponentFolder,
	'{77D80D34-C6FF-43A5-9E67-C7E0A81CF3F2}' : TreeManager,
	'{64162251-9AE0-45BE-892D-535D8C5DD062}' : AuditProperty,
	'{1F73BDFF-AC15-47FF-8264-88ADE733AAB7}' : IReqFactory2,
	'{E214FA66-87AE-4088-8F25-75DBDCB21B7D}' : HierarchySupportList,
	'{E8BFF66F-A020-4909-B9E3-1591182D27D7}' : IComponent,
	'{615FBF36-3E96-4C0F-9827-12FA20D13C58}' : IExecEventNotifyByMailSettings,
	'{3A2C3A43-4D96-4682-91CC-9E091B64C1FE}' : IFileData,
	'{6C760530-757D-4D83-B07A-DD5F29559457}' : ReqSummaryStatus,
	'{3F004630-2A1E-4E7A-9133-6A0EA826C48F}' : IReqCoverageFactory,
	'{3F767372-C6D1-4F1B-8F27-1EABB614551C}' : FactoryList,
	'{3DE03C28-EC6B-4953-96C2-E3216051C907}' : IAuditable,
	'{D70D7E57-CD6F-4FCA-954F-D73C30B9FD90}' : Auditable,
	'{437A2A3C-00EB-4B53-89C8-BBC5DED2D52C}' : IDBManager,
	'{012F18E6-986E-4C65-A5D8-5F1696BC220E}' : AuditPropertyFactory,
	'{7A0B3B0B-60C4-4B84-8A35-1E9337AD055E}' : IAlertManager,
	'{D1D6B200-B24D-4C8D-8622-77FDBF49AC99}' : ExecEventNotifyByMailSettings,
	'{BCF7E6D7-4281-40FA-923B-32DFA90715D7}' : ICoverageEntity,
	'{41120F91-BBBE-4913-975D-5346234765A6}' : IExecutionSettings,
	'{DDE517F0-AF73-4327-A1BD-403E6A047B0A}' : IHierarchySupportList,
	'{1CD20510-7700-42B6-83AB-4AFC0419318D}' : ICustomizationUsersGroups,
	'{B14CE979-E8D8-426B-AFCF-B8AA9AFE267B}' : ITDFilter2,
	'{E0C8D290-50AF-4811-A5FB-3ED5B78B99F3}' : IRTParam,
	'{0F500A37-F2F1-4079-9BE7-48C1DA715E27}' : ISubjectNode,
	'{2291E05E-F2FE-4BDC-BA75-706788C70F62}' : ChangeFactory,
	'{4FFF066E-4D6B-444B-94B1-73209211C62B}' : ComponentParam,
	'{4777BC00-DDBD-4DBD-ACE6-BEAE379E2051}' : ITest,
	'{FCBDBAD5-380A-43F5-B4D4-4CD988B0C924}' : IRuleManager,
	'{D323F3D1-837E-4C0F-9ACB-7CBCDDA557DC}' : ITDChat,
	'{8D7BCC69-04CB-422A-8F85-2654E3539340}' : ActionPermission,
	'{E2E80F73-808A-495E-89F6-430721FE9623}' : ExecutionStatus,
	'{1570FD42-5F4B-49B2-839C-712098B3543E}' : CustomizationTransitionRules,
	'{4FB0D662-F589-4C2D-BC4E-1A6E75845472}' : IVcsAdmin,
	'{3120287D-98B1-4D49-9BC5-3324555D8D04}' : IOnExecEventSchedulerRestartParams,
	'{9BD93246-17D5-4A2C-9318-41B5FDBF0B51}' : AuditRecord,
	'{E8D39BCF-F7AF-4DE0-82B2-8B51AE2C8427}' : Analysis,
	'{BD969260-2F3A-40D0-BE71-7ACE587FB343}' : Settings,
	'{A4CEDAE3-5E32-43FC-9D39-FDA737799A1E}' : IComponentFactory,
	'{12BEFCBF-0269-4BC3-9745-8B6BF5895027}' : StepFactory,
	'{31905CFC-D317-4B8D-85DC-F63D8D3E6E28}' : ProjectProperties,
	'{21DB8AA3-957F-4ACE-9CA7-AECFF8085BB4}' : CacheMgr,
	'{03986C41-712B-4A6F-8C65-B50CBF7FC640}' : ISearchableFactory,
	'{F5DEB618-2CF4-44A1-81A1-00922B9C4F5B}' : IFieldProperty2,
	'{0977F04D-0A7C-403D-A07B-1C2780362C46}' : IChangeEntry,
	'{650BCE02-3C9A-4C22-ABCF-C2872EED73EB}' : IReqCoverageStatus,
	'{2BE06C1F-06AF-4EA7-B3D2-B5CAFD13AE88}' : CustomizationAction,
	'{FD167DFE-8DEE-4930-AFAA-A83C2B1480EB}' : ICoverageFactory,
	'{A3D667D7-2285-4FE1-A799-8581C6D8DE70}' : IAuditPropertyFactory,
	'{2EEC167C-FBBF-4A2F-8DE6-DE8A05EDDB50}' : ICustomizationModules,
	'{FAE217FE-0F81-471F-B4A8-73B045C1710C}' : CustomizationFields,
	'{D2990AC2-106D-4889-B299-D6D4223649E6}' : MultiValue,
	'{4FEDB030-AE1C-407C-8732-0A9E042E9B27}' : Recordset,
	'{30300941-F52E-4FFD-8314-3EA232206EB0}' : ICacheMgr,
	'{0940D6D1-1A04-4274-8568-7F1C82BFCAFA}' : BPGroup,
	'{85F1A554-CF12-42A1-BC3F-BBF8BBE83DF1}' : ExecSettings,
	'{CBD80CD0-1961-4191-A318-ABC50AB2ACD9}' : IOnExecEventSchedulerActionParams,
	'{C2E1AF68-28C8-4DA3-8C65-5E1D230B1FF6}' : IFieldProperty,
	'{B4776982-5666-4075-99A3-0574EDA12EF2}' : IStepParams,
	'{F65AD9CD-D5A3-4EC5-8904-017E4E9D2351}' : IComponentParam,
	'{9D4F53EF-41A2-4059-8AB3-13BBCA8333E8}' : ICustomization,
	'{E9E2BCFE-CAAC-492D-A210-C2E49A68C78F}' : ITestSetFolder,
	'{2C8D6469-AE61-4F01-801E-6B2087216786}' : IProductInfo,
	'{D68C44C2-DF91-4095-B145-7FF8E430FBA7}' : CoverageFactory,
	'{453FE104-67B0-470D-8D1E-70E7A8CF7774}' : Condition,
	'{6199FE11-C44D-4712-99DF-4F5EF3F80A29}' : ITestSetFactory,
	'{7E12617E-3B01-42BD-A16F-13118038D0B7}' : ICustomizationUsersGroup,
	'{9B1CCF47-8DC6-4B9D-AC24-35DD3361A175}' : ILinkable,
	'{1E3AB54D-4D98-4B9F-9A0B-E6427054FED6}' : CustomizationList,
	'{FFE8FF09-7522-448E-933E-724B6A149887}' : ITSTest,
	'{FDF08CE0-896F-498D-928D-F85512B52F4F}' : GraphDefinition,
	'{05A59D3B-C4D9-4E73-BD19-D7D0233D3E1C}' : ComponentParamFactory,
	'{6C66F3F1-52E8-4B39-840B-FB1C9800C676}' : IVCS,
	'{2AF970F7-6CCC-4DFB-AA78-08F689481F94}' : IBug,
	'{E04B8E16-1118-4D05-90CE-3DCC39633158}' : IDesignStep,
	'{3D416474-2373-446E-9090-DBC083B6C382}' : ITDField,
	'{D7B3D7EE-FC85-460D-AB27-21E86176599F}' : ChangeEntryFactory,
	'{F4E85688-FCD7-11D4-9D8A-0001029DEAF5}' : _DIProgressEvents,
	'{C10389DD-70AC-48F5-BCF0-9A1CBA5FCAD9}' : ICustomizationUser,
	'{5BE353A8-508B-48B5-848B-53DF49791E52}' : BusinessProcess,
	'{B1F47936-EFAC-4AEE-9876-8110B16F037D}' : IBaseFactoryEx,
	'{10747648-0DF0-4B8C-8853-7408C70D883A}' : IAuditRecordFactory,
	'{0DCB2995-2738-4BE5-873D-ADAEB29262FA}' : VersionItem,
	'{CD3E5686-4B11-462F-9619-D2FA447DCE96}' : Import,
	'{C5DF6450-F927-425A-A293-3A5CF0EBE56D}' : ICoverable,
	'{61C395DB-BDD5-4431-995D-E5F38E8FAC70}' : AmarillusHash,
	'{A870D9D0-B890-45E7-B3B3-53B5FA0C4F03}' : CustomizationUser,
	'{CE4BEF63-1922-4C81-8C81-DC18FF94D8E7}' : IAttachmentVcs,
	'{FF0182C5-D203-4875-BC2C-48FF8DA7266D}' : IAlertableEntityFactory,
	'{025854DA-9D81-40E8-853D-F4EA33073A77}' : IRecordset,
	'{56FD2617-AEC0-46C8-805A-E69481480B68}' : Command,
	'{C691D4B4-3924-4488-B554-A7E08842C625}' : IAttachmentFactory,
	'{D6F05DAE-7F99-4FE0-9432-D48CAC4BA16E}' : IComponentFolderFactory,
	'{6C25C267-173B-45CB-8150-7BD1F9584981}' : AttachmentVcs,
	'{18B58E14-B956-40A3-A37F-B8EF1136F238}' : ITestSetFolder2,
	'{7545BFC6-913F-4DF9-9F70-C815E13E4CBA}' : CustomizationUsersGroup,
	'{8937B744-DE14-4E34-9A56-4E9E308B1863}' : IBaseFieldEx,
	'{729074D5-C699-4B07-8E8E-25E827C71ED2}' : TestSet,
	'{29823A4B-9739-4E55-972D-6091096E9090}' : HostGroupFactory,
	'{5AB19406-71C9-40F0-AFD3-4EF9A77FE648}' : Run,
	'{78B7C423-5E9B-4225-811A-95ABE879B461}' : CustomizationModules,
	'{C8511847-C115-4680-9512-16D4371F9D58}' : BPStepFactory,
	'{19F58A3A-0E31-441A-A95B-70F416B7AC53}' : HostFactory,
	'{E46670F8-B7CE-4DA6-AC5F-AE1FB9181337}' : IBugFactory,
	'{46A97504-127F-4A93-BCD0-889AF362754E}' : Attachment,
	'{BB603787-CBD0-48CD-AA8A-B4532DC714D1}' : ITDUtils,
	'{FBED7041-8CD1-471D-A1D8-C15A7C1D4CE9}' : Test,
	'{A94555C2-77AD-4F2D-B061-A9ED11AE9FE6}' : Req,
	'{EB180CC0-6FDE-4A1E-A68E-F106EEED5E15}' : IMultiValue,
	'{5666ABB3-0FC1-4C22-A4B0-8D43CD1E54A3}' : RTParam,
	'{DB466018-78FF-4645-9B45-B32F823C07F3}' : IBPGroup,
	'{396C2A71-28E7-4410-8BD9-380953D4B606}' : BPStepParamFactory,
	'{2FA7E440-704F-4EFE-B500-EB93E7AFD294}' : ICustomizationField2,
	'{3A2F36D9-0CEF-4864-A067-7246D6615D9E}' : ICustomizationModules2,
	'{27F5B75D-9DB6-44A3-AAA7-0713836A15A6}' : BPHistoryRecord,
	'{EEFE895F-49F8-4E82-9E78-4A6596B5E6F2}' : SearchOptions,
	'{41BAF7D8-718C-45B6-9115-C91850F27074}' : ExtendedStorageProxy,
	'{C79EC040-A067-11D5-9D6E-000102E0AC0C}' : _DIOtaEvents,
	'{19AD0931-4FF6-4B1C-83AB-464A9D200345}' : DesignStep,
	'{2D1E1D34-561E-40BE-845B-E1F872209A32}' : VCS,
	'{5377347E-3F8D-4B54-B962-18527B652EDD}' : IExecEventInfo,
	'{F4E856D4-FCD7-11D4-9D8A-0001029DEAF5}' : ReqFactory,
	'{34023178-4154-4B16-80A4-6C610096648A}' : IRun,
	'{101CD251-91FB-4FB0-A440-DE755D905584}' : IAlertable,
	'{A57D9723-040E-44C9-829C-64439CC526C1}' : ExecEventRestartActionParams,
	'{A1314B45-32F6-4841-9EB6-922EC4A76CB2}' : IAlert,
	'{4E3DA2DC-070F-4E73-BE64-2E91A8CA7DF8}' : ITDConnection2,
	'{86BDCEC4-35BD-4DA8-8373-474C4EBDFA9F}' : StepParams,
	'{911B9D10-736A-49A2-9A19-34C3CFE6B2E9}' : TestSetTreeManager,
	'{170FFBC3-AE31-4AE8-9C65-0F0FC5AD5CCD}' : TestExecStatus,
	'{18C04A97-2141-4852-8E02-0E79D3CAEAD3}' : ICustomizationListNode,
	'{5BC906B1-56F3-4DC0-A6BB-78440F908823}' : TDErrorInfo,
	'{93FB7AD9-0302-4EA3-8BE5-3437F4F52906}' : CustomizationMailConditions,
	'{2C6320EE-DE3C-46E3-9BCB-7204F3ADEC19}' : RuleManager,
	'{509C8491-47BF-454B-B899-852A55A17F46}' : ICustomizationMailCondition,
	'{35DE061D-B934-4DEB-9F53-6A376EB034DF}' : ICommand,
	'{606FDBBB-55A7-48D6-BF70-06A7CE08049C}' : TestSetFolder,
	'{682F76CF-D479-4A34-AD8F-108F6B6C23DB}' : IRunFactory,
	'{B40742AD-3BED-40B4-BD58-0F24E41ACDD2}' : IBPIterationParam,
	'{DD1C7F94-5C11-416D-B11D-DA062309D43A}' : IAuditProperty,
	'{6687454E-8D1A-462E-BFDE-E342BFF132D3}' : IComponentStep,
	'{01A21FB9-74F1-4934-81F0-8C473F7A6E50}' : ICustomization2,
	'{90A0FD6D-D4F5-4951-AA4D-3954864997C6}' : TDMailConditions,
	'{BF873A99-2FB9-43DB-9559-F9F0872F7534}' : IBaseFieldExMail,
	'{42AD6542-9DBC-4A66-BC3F-7692832D33CB}' : IBPComponent,
	'{BF9B38B0-935B-4112-92EC-49FED46AC64D}' : IExtendedStorage,
	'{73D2B96A-0A3F-41CA-A8F2-E371220B4C63}' : ProductInfo,
	'{B3468A97-FD4C-4A05-9E49-1FD0ED7FD9E2}' : IHostGroup,
	'{AE1410CC-D940-4356-A926-6DF6C1F45AED}' : IParam,
	'{BCAE2958-39FA-4742-93B4-16D204CF67AB}' : IHistoryRecord,
	'{2D52E7C5-B6DE-4D8D-B885-7C1F1DC509B8}' : LinkFactory,
	'{328737D2-7777-4E5E-BB9A-488277C730C5}' : ITextParser,
	'{C29AB208-56D4-493C-80FB-EA952235BAFB}' : Step,
	'{5FF530DD-245E-4F97-A59F-3DE69FFCC55E}' : IStep2,
	'{C79B8E7E-8AAF-47B5-94A3-C824BF8453E6}' : ExtendedStorage,
	'{8F96D0C2-FFFE-4C6F-984E-CE022A50EA0E}' : IGraphBuilder,
	'{F8BBE233-A298-4C0D-8D86-3D442BB8ED08}' : DBManager,
	'{DB67E08D-7FA7-4DAC-87BB-2A55CBFFE008}' : ITDConnection,
	'{E6F08C7F-A34B-43DA-A632-9410170F3D00}' : Customization,
	'{B7E70913-D656-4DB9-8763-3812FC75BB46}' : FieldProperty,
	'{6B8183C1-5676-4315-B202-2B78A15E1D6C}' : CustomizationLists,
	'{F90E8FE5-EC4E-4EBA-8A81-32BCCEE3AB94}' : IHost,
	'{977FEB6A-82DF-4F53-ADA2-F722F7E07D23}' : TDUtils,
	'{AB6C55FF-1D41-4AE7-A8FD-6C7BA8BE2620}' : CustomizationUsersGroups,
	'{1586DCBE-08C3-430F-98F1-D3F078A01E34}' : TSTestFactory,
	'{BE140C38-7D92-4C50-8C1C-A4E43C0FC329}' : IHierarchyFilter,
	'{78E51781-1777-4A05-A13F-1BB9FAFE3E48}' : HostGroup,
	'{89E2DE95-A1CA-47D1-ABCE-21D48A645E98}' : HistoryRecord,
	'{1422200A-A929-430A-8683-6D8FDCA8BA89}' : History,
	'{EEBD59D7-740A-48EE-82F2-14BC33B54F14}' : ConditionFactory,
	'{08E8895B-686D-4F75-9F16-F9E130D99470}' : IExport,
	'{2824DF5E-75A6-475C-AE4D-3DC4E62EA4F5}' : BPStepParam,
	'{365FA56C-AE1E-46EB-A776-6EDDB82BF290}' : ICustomizationTransitionRules,
	'{F13E4E0F-2BF2-41A2-97B1-06AD03204518}' : IBaseFactory,
	'{93FF8BA2-20E4-40C0-BB37-638B5BFD8DAC}' : ICustomizationTransitionRule,
	'{0FC5B9FD-629D-4E3F-8A36-41347ADA3107}' : VcsAdmin,
	'{43A652C9-63BA-44DE-BDF4-642D0596EDD2}' : AuditRecordFactory,
	'{F061ABB7-A65F-4146-8621-5A8B04C87B8D}' : ICoverableReq,
	'{50A81398-8F2F-4F38-B88D-F734C727706C}' : TestSetFactory,
	'{0A29F8B6-0FFE-4783-B3B0-71FB3FB8D6D9}' : Alert,
	'{78D71CB3-1DC2-49A6-BF0C-6D36AFD9E93E}' : CustomizationMailCondition,
	'{BCE38540-36CC-4C3A-B474-11C8E8EEEDE0}' : SysTreeNode,
	'{1754ECE8-1386-456C-AA7C-AD448412EDA3}' : IAuditRecord,
	'{01C802C0-3021-4FE3-91AB-8C2AD5116BB4}' : CustomizationPermissions,
	'{4EA26B82-65DC-4315-AD7F-D963A1C4EB2F}' : ICustomizationLists,
	'{260EDCFF-BF72-4146-BC41-5B2280652ED0}' : IObjectLockingSupport,
	'{217D92D5-CD8A-4ADA-8ECC-9D13C224DCA8}' : ICustomizationAction,
	'{DC091A33-E1E8-4A17-8C55-529C09B0670B}' : IBaseParam,
	'{DBCF02A8-E281-4C0B-92B8-4F8AF677DD6C}' : Change,
	'{590E515E-D527-4436-B04C-40942DBFFB5F}' : IBusinessProcess,
	'{F87FF5C2-4631-4E5E-BF46-38490501A12B}' : CoverageEntity,
	'{AF9180F9-8C16-4824-9EA1-A9010B072B2C}' : Bug,
	'{B761098B-AC59-4C3D-A427-D09231A402B8}' : ISettings2,
	'{613E9BF2-7888-438F-979F-D05DAB87B9C8}' : IAttachment,
	'{771F8212-D6CE-48A1-9F3A-87EF3E6E4CD5}' : GraphBuilder,
	'{6AF3BFA2-C914-4FF8-9531-A047745F2597}' : Host,
	'{488D6BE8-A962-4958-8E11-6E2EE9A20EFF}' : TSTest,
	'{7F04BEF8-6460-4BF2-825A-F758E8657F51}' : AlertManager,
	'{5BBB5891-F832-497C-BE92-76A242809E67}' : ITestSet,
	'{F801F7A2-04DF-4DD3-8A5E-C0CC66E0595E}' : GroupingManager,
	'{0B280A46-1606-4FD3-90E4-EF5149725200}' : ComponentFactory,
	'{2D05E2CD-60E9-4C56-A05D-D25872A00335}' : ICustomizationMailConditions,
	'{3A6DFEA9-5E15-4E8F-B480-2B025730F7BD}' : IImport,
	'{E746670E-69C7-4DC1-93BC-9B4662B6015D}' : ICustomizationActions,
	'{C7CE06D8-F6D2-47B1-AB82-EE9A762171CD}' : TDChat,
	'{ECCB1143-8914-497A-ACA0-8789CA64C2D6}' : IGraph,
	'{48BB9422-E786-463A-8617-25ADA366FAA0}' : CustomizationField,
	'{0993EF6D-FAF3-42FF-BCF9-85BBB83753F6}' : IColumnInfo,
	'{F64C065B-D9BF-41CB-91AD-C1672A8220DF}' : ExecEventActionParams,
	'{49444E30-82A0-4786-9566-E7DA73044631}' : ProjectRepository,
	'{E34F74EC-DC52-42D8-A7E4-B4F06A56CF40}' : ICustomizationField,
	'{68FE31A3-6242-4AA4-9BBE-D0715F810DB3}' : IFactoryList,
	'{5186B0F4-DAC2-4ABD-B248-8ED6E852C40D}' : IRule,
	'{5FCA1AE7-AE4A-4709-BD98-13240D68BBD4}' : _ITestSetFolder,
	'{51A0C1D6-F748-41D1-988F-B0B93013F1BC}' : IBPParam,
	'{8FE1F909-64F2-49B3-908F-4F15C64D9F08}' : CustomizationUsers,
	'{31FDA98D-C50C-416D-BB4F-9D5294896175}' : IGroupingItem,
	'{6388B1FB-C735-4D24-B1E4-97221B302461}' : ICustomizationFields,
	'{81113FC0-ECB4-43E5-AEF6-BFEEC9560CB4}' : _ITreeNode,
	'{C64F7478-FF48-49A7-8260-251EAB8D5B7C}' : BPComponent,
	'{FCA0B016-AC98-4E63-A7EE-34759BF8038C}' : ITDMailConditions,
	'{55110A8D-110B-460C-9D28-F8B4BCF3DFF0}' : ISettings,
	'{E672B813-30DA-4429-97A7-A1616F0B7D2D}' : IHistoryRecord2,
	'{F96FB8D9-0CD2-4B82-85FA-AB07C435F87F}' : IBPIteration,
	'{75180DF9-EF06-4FB4-8EDB-5697713AB54C}' : Link,
	'{D600F6C4-1A12-4973-80A9-A976300DF1D7}' : ComponentStep,
	'{8A01DC68-2D34-4CB2-81D7-968F37C50715}' : ICondition,
	'{D5941686-5E86-431D-87E3-A06D67B3AC44}' : DesignStepFactory,
	'{E99AC0C8-46AB-42C3-9CB2-9C9AEC35A861}' : ITSScheduler,
	'{D367F107-1BCE-4AAB-8E6B-BF6399BD64FC}' : IExecutionStatus,
}
CLSIDToPackageMap = {}
win32com.client.CLSIDToClass.RegisterCLSIDsFromDict( CLSIDToClassMap )
VTablesToPackageMap = {}
VTablesToClassMap = {
	'{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}' : 'IList',
	'{59A79946-0678-4E59-B4B0-9967E4314CCA}' : 'IProjectProperties',
	'{8538CCBA-0B3E-481F-BB2B-14F0553CA146}' : 'ISysTreeNode',
	'{5BB6D957-669D-47B2-8324-981492EDC42E}' : 'ITestFactory',
	'{33CCFC63-CE02-47AB-9A11-BB2E0C324723}' : 'IGroupingManager',
	'{992BABA3-6360-4BD1-A337-B75F67BDB417}' : 'IStep',
	'{133B9314-9A98-4659-866D-4BA8DCD4A643}' : 'ITSTest2',
	'{3E46BE58-4943-48AA-BA08-38EEBA837A04}' : 'IBaseFactory2',
	'{7A6F279C-7729-4394-B5BB-BC3E8902DA56}' : 'IComponentFolder',
	'{D2FA8791-3A81-4D03-95CA-74EF6E059C4F}' : 'IComFrec',
	'{5073A186-2A46-4C93-A3F5-F8C0AA66694F}' : 'IAmarillusHash',
	'{FCBDBAD5-380A-43F5-B4D4-4CD988B0C924}' : 'IRuleManager',
	'{3120287D-98B1-4D49-9BC5-3324555D8D04}' : 'IOnExecEventSchedulerRestartParams',
	'{49B715FA-458E-46EA-A171-0E0BFB38B3AF}' : 'ILinkFactory',
	'{8C63633A-6537-41B5-93A3-982823DE8689}' : 'IConditionFactory',
	'{1EE78B77-A68F-49D8-9707-9A7C8CEA10D2}' : 'IActionPermission',
	'{38D402FA-9823-4D25-979E-62C377AC2E77}' : 'IVersionItem',
	'{6BA5E493-366C-4068-B572-93D27D405DFE}' : 'IBPStepParam',
	'{FA8C3437-3B5B-4246-B775-523DEEB2734A}' : 'IHostGroupFactory',
	'{7EE7D348-AED6-4B3C-8FC7-5D9D8EA8E4C9}' : 'ICustomizationPermissions',
	'{452897AD-D9F8-4EAE-80DB-B9C11807507F}' : 'ITDFilter',
	'{5377347E-3F8D-4B54-B962-18527B652EDD}' : 'IExecEventInfo',
	'{E2F29752-72F0-42DB-995C-3DB385F4CCE5}' : 'IBaseField',
	'{1E1C07CA-7339-4741-A41D-DEC5A065B1F1}' : 'ICustomizationList',
	'{16EF2BF4-8509-475E-B34E-3BF99C221221}' : 'IRun2',
	'{590E515E-D527-4436-B04C-40942DBFFB5F}' : 'IBusinessProcess',
	'{E60B4439-691C-433D-B6D9-793044915A01}' : 'ITreeManager',
	'{557AF6E9-FB13-4934-9A8F-6620BA38C547}' : 'IAnalysis',
	'{8A01DC68-2D34-4CB2-81D7-968F37C50715}' : 'ICondition',
	'{1CF2B120-547D-101B-8E65-08002B2BD119}' : 'IErrorInfo',
	'{BE73DDAA-AD0F-4A89-8936-0EDA17599273}' : 'IBaseField2',
	'{CCD9F62A-3899-4233-9C2C-A25BFF1AF041}' : 'IReqSummaryStatus',
	'{79CFC0C1-7BB6-48CE-BC47-8074DDBCA542}' : 'ILink',
	'{6EE10992-2569-4838-86DA-DAE1E1240E79}' : 'IFollowUpManager',
	'{DCFC6A77-C0F7-4F36-82E2-5164749254AF}' : 'ICustomizationUsers',
	'{4592C936-2524-4D05-9978-95901EDE0A54}' : 'IReq',
	'{E1582AF7-780E-4D93-9771-E196205F96C7}' : 'IBPHistoryRecord',
	'{FD53A549-A095-469F-AD8E-95F9F034D2DF}' : 'ITestSetTreeManager',
	'{15FBB8D4-7034-413E-A8F0-1E03B7FA4F0B}' : 'IHistory',
	'{96615E7A-A7C4-4B6F-A00A-418B92070F83}' : 'IGraphDefinition',
	'{74B94507-8A42-4D11-B5BD-943B76C9A982}' : 'IChange',
	'{833093EE-C983-46F7-88BF-DE5D7E2FCBBD}' : 'IReqFactory',
	'{DD1C7F94-5C11-416D-B11D-DA062309D43A}' : 'IAuditProperty',
	'{1F73BDFF-AC15-47FF-8264-88ADE733AAB7}' : 'IReqFactory2',
	'{E8BFF66F-A020-4909-B9E3-1591182D27D7}' : 'IComponent',
	'{2444C43F-5371-4B4A-B6B6-34D205644C35}' : 'ISearchOptions',
	'{615FBF36-3E96-4C0F-9827-12FA20D13C58}' : 'IExecEventNotifyByMailSettings',
	'{3A2C3A43-4D96-4682-91CC-9E091B64C1FE}' : 'IFileData',
	'{3F004630-2A1E-4E7A-9133-6A0EA826C48F}' : 'IReqCoverageFactory',
	'{3DE03C28-EC6B-4953-96C2-E3216051C907}' : 'IAuditable',
	'{437A2A3C-00EB-4B53-89C8-BBC5DED2D52C}' : 'IDBManager',
	'{7A0B3B0B-60C4-4B84-8A35-1E9337AD055E}' : 'IAlertManager',
	'{BCF7E6D7-4281-40FA-923B-32DFA90715D7}' : 'ICoverageEntity',
	'{41120F91-BBBE-4913-975D-5346234765A6}' : 'IExecutionSettings',
	'{B3468A97-FD4C-4A05-9E49-1FD0ED7FD9E2}' : 'IHostGroup',
	'{1CD20510-7700-42B6-83AB-4AFC0419318D}' : 'ICustomizationUsersGroups',
	'{B3391F80-3861-11D3-AFAB-00600855298D}' : 'ITDProgressNotify',
	'{B14CE979-E8D8-426B-AFCF-B8AA9AFE267B}' : 'ITDFilter2',
	'{E0C8D290-50AF-4811-A5FB-3ED5B78B99F3}' : 'IRTParam',
	'{0F500A37-F2F1-4079-9BE7-48C1DA715E27}' : 'ISubjectNode',
	'{4777BC00-DDBD-4DBD-ACE6-BEAE379E2051}' : 'ITest',
	'{D323F3D1-837E-4C0F-9ACB-7CBCDDA557DC}' : 'ITDChat',
	'{A4CEDAE3-5E32-43FC-9D39-FDA737799A1E}' : 'IComponentFactory',
	'{03986C41-712B-4A6F-8C65-B50CBF7FC640}' : 'ISearchableFactory',
	'{F5DEB618-2CF4-44A1-81A1-00922B9C4F5B}' : 'IFieldProperty2',
	'{0977F04D-0A7C-403D-A07B-1C2780362C46}' : 'IChangeEntry',
	'{FD167DFE-8DEE-4930-AFAA-A83C2B1480EB}' : 'ICoverageFactory',
	'{A3D667D7-2285-4FE1-A799-8581C6D8DE70}' : 'IAuditPropertyFactory',
	'{DCD217D3-9BE9-4BED-B386-1A105D84E1F0}' : '_IBaseField',
	'{2EEC167C-FBBF-4A2F-8DE6-DE8A05EDDB50}' : 'ICustomizationModules',
	'{2C8D6469-AE61-4F01-801E-6B2087216786}' : 'IProductInfo',
	'{CBD80CD0-1961-4191-A318-ABC50AB2ACD9}' : 'IOnExecEventSchedulerActionParams',
	'{C2E1AF68-28C8-4DA3-8C65-5E1D230B1FF6}' : 'IFieldProperty',
	'{EBB38286-D5A3-41DD-9EBA-F2791C10522F}' : 'ISupportCopyPaste',
	'{47E4C767-87DA-409F-A70E-04B3FCE28BCE}' : '_IBaseFactory',
	'{9D4F53EF-41A2-4059-8AB3-13BBCA8333E8}' : 'ICustomization',
	'{E9E2BCFE-CAAC-492D-A210-C2E49A68C78F}' : 'ITestSetFolder',
	'{BCAE2958-39FA-4742-93B4-16D204CF67AB}' : 'IHistoryRecord',
	'{6199FE11-C44D-4712-99DF-4F5EF3F80A29}' : 'ITestSetFactory',
	'{9B1CCF47-8DC6-4B9D-AC24-35DD3361A175}' : 'ILinkable',
	'{6C66F3F1-52E8-4B39-840B-FB1C9800C676}' : 'IVCS',
	'{2AF970F7-6CCC-4DFB-AA78-08F689481F94}' : 'IBug',
	'{E04B8E16-1118-4D05-90CE-3DCC39633158}' : 'IDesignStep',
	'{3D416474-2373-446E-9090-DBC083B6C382}' : 'ITDField',
	'{C10389DD-70AC-48F5-BCF0-9A1CBA5FCAD9}' : 'ICustomizationUser',
	'{B1F47936-EFAC-4AEE-9876-8110B16F037D}' : 'IBaseFactoryEx',
	'{10747648-0DF0-4B8C-8853-7408C70D883A}' : 'IAuditRecordFactory',
	'{C5DF6450-F927-425A-A293-3A5CF0EBE56D}' : 'ICoverable',
	'{CE4BEF63-1922-4C81-8C81-DC18FF94D8E7}' : 'IAttachmentVcs',
	'{FF0182C5-D203-4875-BC2C-48FF8DA7266D}' : 'IAlertableEntityFactory',
	'{025854DA-9D81-40E8-853D-F4EA33073A77}' : 'IRecordset',
	'{C691D4B4-3924-4488-B554-A7E08842C625}' : 'IAttachmentFactory',
	'{4FB0D662-F589-4C2D-BC4E-1A6E75845472}' : 'IVcsAdmin',
	'{18B58E14-B956-40A3-A37F-B8EF1136F238}' : 'ITestSetFolder2',
	'{8937B744-DE14-4E34-9A56-4E9E308B1863}' : 'IBaseFieldEx',
	'{30300941-F52E-4FFD-8314-3EA232206EB0}' : 'ICacheMgr',
	'{FFE8FF09-7522-448E-933E-724B6A149887}' : 'ITSTest',
	'{E46670F8-B7CE-4DA6-AC5F-AE1FB9181337}' : 'IBugFactory',
	'{BB603787-CBD0-48CD-AA8A-B4532DC714D1}' : 'ITDUtils',
	'{EB180CC0-6FDE-4A1E-A68E-F106EEED5E15}' : 'IMultiValue',
	'{DB466018-78FF-4645-9B45-B32F823C07F3}' : 'IBPGroup',
	'{DDE517F0-AF73-4327-A1BD-403E6A047B0A}' : 'IHierarchySupportList',
	'{2FA7E440-704F-4EFE-B500-EB93E7AFD294}' : 'ICustomizationField2',
	'{3A2F36D9-0CEF-4864-A067-7246D6615D9E}' : 'ICustomizationModules2',
	'{F65AD9CD-D5A3-4EC5-8904-017E4E9D2351}' : 'IComponentParam',
	'{34023178-4154-4B16-80A4-6C610096648A}' : 'IRun',
	'{101CD251-91FB-4FB0-A440-DE755D905584}' : 'IAlertable',
	'{A1314B45-32F6-4841-9EB6-922EC4A76CB2}' : 'IAlert',
	'{4E3DA2DC-070F-4E73-BE64-2E91A8CA7DF8}' : 'ITDConnection2',
	'{18C04A97-2141-4852-8E02-0E79D3CAEAD3}' : 'ICustomizationListNode',
	'{509C8491-47BF-454B-B899-852A55A17F46}' : 'ICustomizationMailCondition',
	'{35DE061D-B934-4DEB-9F53-6A376EB034DF}' : 'ICommand',
	'{682F76CF-D479-4A34-AD8F-108F6B6C23DB}' : 'IRunFactory',
	'{B40742AD-3BED-40B4-BD58-0F24E41ACDD2}' : 'IBPIterationParam',
	'{6687454E-8D1A-462E-BFDE-E342BFF132D3}' : 'IComponentStep',
	'{01A21FB9-74F1-4934-81F0-8C473F7A6E50}' : 'ICustomization2',
	'{BF873A99-2FB9-43DB-9559-F9F0872F7534}' : 'IBaseFieldExMail',
	'{42AD6542-9DBC-4A66-BC3F-7692832D33CB}' : 'IBPComponent',
	'{BF9B38B0-935B-4112-92EC-49FED46AC64D}' : 'IExtendedStorage',
	'{650BCE02-3C9A-4C22-ABCF-C2872EED73EB}' : 'IReqCoverageStatus',
	'{AE1410CC-D940-4356-A926-6DF6C1F45AED}' : 'IParam',
	'{365FA56C-AE1E-46EB-A776-6EDDB82BF290}' : 'ICustomizationTransitionRules',
	'{328737D2-7777-4E5E-BB9A-488277C730C5}' : 'ITextParser',
	'{5FF530DD-245E-4F97-A59F-3DE69FFCC55E}' : 'IStep2',
	'{8F96D0C2-FFFE-4C6F-984E-CE022A50EA0E}' : 'IGraphBuilder',
	'{DB67E08D-7FA7-4DAC-87BB-2A55CBFFE008}' : 'ITDConnection',
	'{D6F05DAE-7F99-4FE0-9432-D48CAC4BA16E}' : 'IComponentFolderFactory',
	'{F90E8FE5-EC4E-4EBA-8A81-32BCCEE3AB94}' : 'IHost',
	'{DEBC6003-2ABE-4C93-A769-646F300A7986}' : 'ITDErrorInfo',
	'{BE140C38-7D92-4C50-8C1C-A4E43C0FC329}' : 'IHierarchyFilter',
	'{08E8895B-686D-4F75-9F16-F9E130D99470}' : 'IExport',
	'{F13E4E0F-2BF2-41A2-97B1-06AD03204518}' : 'IBaseFactory',
	'{93FF8BA2-20E4-40C0-BB37-638B5BFD8DAC}' : 'ICustomizationTransitionRule',
	'{F061ABB7-A65F-4146-8621-5A8B04C87B8D}' : 'ICoverableReq',
	'{1754ECE8-1386-456C-AA7C-AD448412EDA3}' : 'IAuditRecord',
	'{B4776982-5666-4075-99A3-0574EDA12EF2}' : 'IStepParams',
	'{4EA26B82-65DC-4315-AD7F-D963A1C4EB2F}' : 'ICustomizationLists',
	'{260EDCFF-BF72-4146-BC41-5B2280652ED0}' : 'IObjectLockingSupport',
	'{217D92D5-CD8A-4ADA-8ECC-9D13C224DCA8}' : 'ICustomizationAction',
	'{B761098B-AC59-4C3D-A427-D09231A402B8}' : 'ISettings2',
	'{24B17582-D52C-4074-8BD6-D7DD82705D15}' : 'IUnitTestSupport',
	'{613E9BF2-7888-438F-979F-D05DAB87B9C8}' : 'IAttachment',
	'{C1ED849E-CB41-45A9-A256-9A0D3CFDB350}' : 'ITestExecStatus',
	'{7E12617E-3B01-42BD-A16F-13118038D0B7}' : 'ICustomizationUsersGroup',
	'{5BBB5891-F832-497C-BE92-76A242809E67}' : 'ITestSet',
	'{2D05E2CD-60E9-4C56-A05D-D25872A00335}' : 'ICustomizationMailConditions',
	'{3A6DFEA9-5E15-4E8F-B480-2B025730F7BD}' : 'IImport',
	'{E746670E-69C7-4DC1-93BC-9B4662B6015D}' : 'ICustomizationActions',
	'{ECCB1143-8914-497A-ACA0-8789CA64C2D6}' : 'IGraph',
	'{0993EF6D-FAF3-42FF-BCF9-85BBB83753F6}' : 'IColumnInfo',
	'{E34F74EC-DC52-42D8-A7E4-B4F06A56CF40}' : 'ICustomizationField',
	'{68FE31A3-6242-4AA4-9BBE-D0715F810DB3}' : 'IFactoryList',
	'{5186B0F4-DAC2-4ABD-B248-8ED6E852C40D}' : 'IRule',
	'{5FCA1AE7-AE4A-4709-BD98-13240D68BBD4}' : '_ITestSetFolder',
	'{51A0C1D6-F748-41D1-988F-B0B93013F1BC}' : 'IBPParam',
	'{31FDA98D-C50C-416D-BB4F-9D5294896175}' : 'IGroupingItem',
	'{6388B1FB-C735-4D24-B1E4-97221B302461}' : 'ICustomizationFields',
	'{81113FC0-ECB4-43E5-AEF6-BFEEC9560CB4}' : '_ITreeNode',
	'{FCA0B016-AC98-4E63-A7EE-34759BF8038C}' : 'ITDMailConditions',
	'{55110A8D-110B-460C-9D28-F8B4BCF3DFF0}' : 'ISettings',
	'{E672B813-30DA-4429-97A7-A1616F0B7D2D}' : 'IHistoryRecord2',
	'{F96FB8D9-0CD2-4B82-85FA-AB07C435F87F}' : 'IBPIteration',
	'{E99AC0C8-46AB-42C3-9CB2-9C9AEC35A861}' : 'ITSScheduler',
	'{D367F107-1BCE-4AAB-8E6B-BF6399BD64FC}' : 'IExecutionStatus',
}


NamesToIIDMap = {
	'ITestSetFolder' : '{E9E2BCFE-CAAC-492D-A210-C2E49A68C78F}',
	'ISupportCopyPaste' : '{EBB38286-D5A3-41DD-9EBA-F2791C10522F}',
	'ITSTest' : '{FFE8FF09-7522-448E-933E-724B6A149887}',
	'ICustomizationField' : '{E34F74EC-DC52-42D8-A7E4-B4F06A56CF40}',
	'ICustomization2' : '{01A21FB9-74F1-4934-81F0-8C473F7A6E50}',
	'ITDFilter' : '{452897AD-D9F8-4EAE-80DB-B9C11807507F}',
	'IComponentFolderFactory' : '{D6F05DAE-7F99-4FE0-9432-D48CAC4BA16E}',
	'ITestFactory' : '{5BB6D957-669D-47B2-8324-981492EDC42E}',
	'IAnalysis' : '{557AF6E9-FB13-4934-9A8F-6620BA38C547}',
	'ICacheMgr' : '{30300941-F52E-4FFD-8314-3EA232206EB0}',
	'IAttachment' : '{613E9BF2-7888-438F-979F-D05DAB87B9C8}',
	'ICustomization' : '{9D4F53EF-41A2-4059-8AB3-13BBCA8333E8}',
	'ITDField' : '{3D416474-2373-446E-9090-DBC083B6C382}',
	'IExecutionSettings' : '{41120F91-BBBE-4913-975D-5346234765A6}',
	'IActionPermission' : '{1EE78B77-A68F-49D8-9707-9A7C8CEA10D2}',
	'IDBManager' : '{437A2A3C-00EB-4B53-89C8-BBC5DED2D52C}',
	'IComponentFactory' : '{A4CEDAE3-5E32-43FC-9D39-FDA737799A1E}',
	'IProjectProperties' : '{59A79946-0678-4E59-B4B0-9967E4314CCA}',
	'IBaseFieldExMail' : '{BF873A99-2FB9-43DB-9559-F9F0872F7534}',
	'IBaseFactoryEx' : '{B1F47936-EFAC-4AEE-9876-8110B16F037D}',
	'IReqSummaryStatus' : '{CCD9F62A-3899-4233-9C2C-A25BFF1AF041}',
	'ICustomizationTransitionRule' : '{93FF8BA2-20E4-40C0-BB37-638B5BFD8DAC}',
	'IReqCoverageStatus' : '{650BCE02-3C9A-4C22-ABCF-C2872EED73EB}',
	'IChange' : '{74B94507-8A42-4D11-B5BD-943B76C9A982}',
	'IAlert' : '{A1314B45-32F6-4841-9EB6-922EC4A76CB2}',
	'IFieldProperty' : '{C2E1AF68-28C8-4DA3-8C65-5E1D230B1FF6}',
	'IComponent' : '{E8BFF66F-A020-4909-B9E3-1591182D27D7}',
	'IReqCoverageFactory' : '{3F004630-2A1E-4E7A-9133-6A0EA826C48F}',
	'IMultiValue' : '{EB180CC0-6FDE-4A1E-A68E-F106EEED5E15}',
	'IStep2' : '{5FF530DD-245E-4F97-A59F-3DE69FFCC55E}',
	'ITDMailConditions' : '{FCA0B016-AC98-4E63-A7EE-34759BF8038C}',
	'ICustomizationModules2' : '{3A2F36D9-0CEF-4864-A067-7246D6615D9E}',
	'ICoverableReq' : '{F061ABB7-A65F-4146-8621-5A8B04C87B8D}',
	'IObjectLockingSupport' : '{260EDCFF-BF72-4146-BC41-5B2280652ED0}',
	'IAttachmentFactory' : '{C691D4B4-3924-4488-B554-A7E08842C625}',
	'IRun2' : '{16EF2BF4-8509-475E-B34E-3BF99C221221}',
	'IOnExecEventSchedulerActionParams' : '{CBD80CD0-1961-4191-A318-ABC50AB2ACD9}',
	'IBPGroup' : '{DB466018-78FF-4645-9B45-B32F823C07F3}',
	'ITestSetTreeManager' : '{FD53A549-A095-469F-AD8E-95F9F034D2DF}',
	'ICoverageFactory' : '{FD167DFE-8DEE-4930-AFAA-A83C2B1480EB}',
	'IComFrec' : '{D2FA8791-3A81-4D03-95CA-74EF6E059C4F}',
	'IChangeEntry' : '{0977F04D-0A7C-403D-A07B-1C2780362C46}',
	'IComponentFolder' : '{7A6F279C-7729-4394-B5BB-BC3E8902DA56}',
	'ILinkFactory' : '{49B715FA-458E-46EA-A171-0E0BFB38B3AF}',
	'IExecEventNotifyByMailSettings' : '{615FBF36-3E96-4C0F-9827-12FA20D13C58}',
	'IGroupingManager' : '{33CCFC63-CE02-47AB-9A11-BB2E0C324723}',
	'ITestSet' : '{5BBB5891-F832-497C-BE92-76A242809E67}',
	'IAuditRecord' : '{1754ECE8-1386-456C-AA7C-AD448412EDA3}',
	'IExport' : '{08E8895B-686D-4F75-9F16-F9E130D99470}',
	'ICustomizationListNode' : '{18C04A97-2141-4852-8E02-0E79D3CAEAD3}',
	'IBaseField' : '{E2F29752-72F0-42DB-995C-3DB385F4CCE5}',
	'IHierarchyFilter' : '{BE140C38-7D92-4C50-8C1C-A4E43C0FC329}',
	'IRule' : '{5186B0F4-DAC2-4ABD-B248-8ED6E852C40D}',
	'IStep' : '{992BABA3-6360-4BD1-A337-B75F67BDB417}',
	'IOnExecEventSchedulerRestartParams' : '{3120287D-98B1-4D49-9BC5-3324555D8D04}',
	'ICustomizationAction' : '{217D92D5-CD8A-4ADA-8ECC-9D13C224DCA8}',
	'ICoverable' : '{C5DF6450-F927-425A-A293-3A5CF0EBE56D}',
	'IHistoryRecord' : '{BCAE2958-39FA-4742-93B4-16D204CF67AB}',
	'IBPParam' : '{51A0C1D6-F748-41D1-988F-B0B93013F1BC}',
	'ITDProgressNotify' : '{B3391F80-3861-11D3-AFAB-00600855298D}',
	'IExecutionStatus' : '{D367F107-1BCE-4AAB-8E6B-BF6399BD64FC}',
	'IRTParam' : '{E0C8D290-50AF-4811-A5FB-3ED5B78B99F3}',
	'IAmarillusHash' : '{5073A186-2A46-4C93-A3F5-F8C0AA66694F}',
	'IAuditProperty' : '{DD1C7F94-5C11-416D-B11D-DA062309D43A}',
	'IRecordset' : '{025854DA-9D81-40E8-853D-F4EA33073A77}',
	'IBPHistoryRecord' : '{E1582AF7-780E-4D93-9771-E196205F96C7}',
	'ITDConnection2' : '{4E3DA2DC-070F-4E73-BE64-2E91A8CA7DF8}',
	'IFactoryList' : '{68FE31A3-6242-4AA4-9BBE-D0715F810DB3}',
	'ICustomizationList' : '{1E1C07CA-7339-4741-A41D-DEC5A065B1F1}',
	'ISettings' : '{55110A8D-110B-460C-9D28-F8B4BCF3DFF0}',
	'IGroupingItem' : '{31FDA98D-C50C-416D-BB4F-9D5294896175}',
	'IStepParams' : '{B4776982-5666-4075-99A3-0574EDA12EF2}',
	'ITDChat' : '{D323F3D1-837E-4C0F-9ACB-7CBCDDA557DC}',
	'IHistoryRecord2' : '{E672B813-30DA-4429-97A7-A1616F0B7D2D}',
	'_DIOtaEvents' : '{C79EC040-A067-11D5-9D6E-000102E0AC0C}',
	'IList' : '{F73CCC7B-05FA-4FD7-8FAA-B53DFFFE22DC}',
	'IFileData' : '{3A2C3A43-4D96-4682-91CC-9E091B64C1FE}',
	'IBPComponent' : '{42AD6542-9DBC-4A66-BC3F-7692832D33CB}',
	'IBug' : '{2AF970F7-6CCC-4DFB-AA78-08F689481F94}',
	'IHistory' : '{15FBB8D4-7034-413E-A8F0-1E03B7FA4F0B}',
	'IGraphDefinition' : '{96615E7A-A7C4-4B6F-A00A-418B92070F83}',
	'IFieldProperty2' : '{F5DEB618-2CF4-44A1-81A1-00922B9C4F5B}',
	'ICustomizationLists' : '{4EA26B82-65DC-4315-AD7F-D963A1C4EB2F}',
	'ICoverageEntity' : '{BCF7E6D7-4281-40FA-923B-32DFA90715D7}',
	'_ITestSetFolder' : '{5FCA1AE7-AE4A-4709-BD98-13240D68BBD4}',
	'ICondition' : '{8A01DC68-2D34-4CB2-81D7-968F37C50715}',
	'IColumnInfo' : '{0993EF6D-FAF3-42FF-BCF9-85BBB83753F6}',
	'IComponentStep' : '{6687454E-8D1A-462E-BFDE-E342BFF132D3}',
	'IHierarchySupportList' : '{DDE517F0-AF73-4327-A1BD-403E6A047B0A}',
	'IExecEventInfo' : '{5377347E-3F8D-4B54-B962-18527B652EDD}',
	'ISettings2' : '{B761098B-AC59-4C3D-A427-D09231A402B8}',
	'IParam' : '{AE1410CC-D940-4356-A926-6DF6C1F45AED}',
	'ITDUtils' : '{BB603787-CBD0-48CD-AA8A-B4532DC714D1}',
	'IFollowUpManager' : '{6EE10992-2569-4838-86DA-DAE1E1240E79}',
	'IBPIteration' : '{F96FB8D9-0CD2-4B82-85FA-AB07C435F87F}',
	'_ITreeNode' : '{81113FC0-ECB4-43E5-AEF6-BFEEC9560CB4}',
	'IHostGroup' : '{B3468A97-FD4C-4A05-9E49-1FD0ED7FD9E2}',
	'IReq' : '{4592C936-2524-4D05-9978-95901EDE0A54}',
	'ISubjectNode' : '{0F500A37-F2F1-4079-9BE7-48C1DA715E27}',
	'IReqFactory' : '{833093EE-C983-46F7-88BF-DE5D7E2FCBBD}',
	'IDesignStep' : '{E04B8E16-1118-4D05-90CE-3DCC39633158}',
	'IVcsAdmin' : '{4FB0D662-F589-4C2D-BC4E-1A6E75845472}',
	'IConditionFactory' : '{8C63633A-6537-41B5-93A3-982823DE8689}',
	'IRun' : '{34023178-4154-4B16-80A4-6C610096648A}',
	'IAlertableEntityFactory' : '{FF0182C5-D203-4875-BC2C-48FF8DA7266D}',
	'ITextParser' : '{328737D2-7777-4E5E-BB9A-488277C730C5}',
	'ISearchOptions' : '{2444C43F-5371-4B4A-B6B6-34D205644C35}',
	'IUnitTestSupport' : '{24B17582-D52C-4074-8BD6-D7DD82705D15}',
	'IAuditable' : '{3DE03C28-EC6B-4953-96C2-E3216051C907}',
	'IBaseField2' : '{BE73DDAA-AD0F-4A89-8936-0EDA17599273}',
	'IBPIterationParam' : '{B40742AD-3BED-40B4-BD58-0F24E41ACDD2}',
	'ICustomizationModules' : '{2EEC167C-FBBF-4A2F-8DE6-DE8A05EDDB50}',
	'IGraphBuilder' : '{8F96D0C2-FFFE-4C6F-984E-CE022A50EA0E}',
	'IExtendedStorage' : '{BF9B38B0-935B-4112-92EC-49FED46AC64D}',
	'ICommand' : '{35DE061D-B934-4DEB-9F53-6A376EB034DF}',
	'ICustomizationUsers' : '{DCFC6A77-C0F7-4F36-82E2-5164749254AF}',
	'IRunFactory' : '{682F76CF-D479-4A34-AD8F-108F6B6C23DB}',
	'ICustomizationActions' : '{E746670E-69C7-4DC1-93BC-9B4662B6015D}',
	'ISysTreeNode' : '{8538CCBA-0B3E-481F-BB2B-14F0553CA146}',
	'ITDConnection' : '{DB67E08D-7FA7-4DAC-87BB-2A55CBFFE008}',
	'ITSScheduler' : '{E99AC0C8-46AB-42C3-9CB2-9C9AEC35A861}',
	'IHost' : '{F90E8FE5-EC4E-4EBA-8A81-32BCCEE3AB94}',
	'IImport' : '{3A6DFEA9-5E15-4E8F-B480-2B025730F7BD}',
	'IAttachmentVcs' : '{CE4BEF63-1922-4C81-8C81-DC18FF94D8E7}',
	'ITestExecStatus' : '{C1ED849E-CB41-45A9-A256-9A0D3CFDB350}',
	'IHostGroupFactory' : '{FA8C3437-3B5B-4246-B775-523DEEB2734A}',
	'ITestSetFactory' : '{6199FE11-C44D-4712-99DF-4F5EF3F80A29}',
	'IProductInfo' : '{2C8D6469-AE61-4F01-801E-6B2087216786}',
	'ISearchableFactory' : '{03986C41-712B-4A6F-8C65-B50CBF7FC640}',
	'ITSTest2' : '{133B9314-9A98-4659-866D-4BA8DCD4A643}',
	'_DIProgressEvents' : '{F4E85688-FCD7-11D4-9D8A-0001029DEAF5}',
	'IBPStepParam' : '{6BA5E493-366C-4068-B572-93D27D405DFE}',
	'IBusinessProcess' : '{590E515E-D527-4436-B04C-40942DBFFB5F}',
	'ITreeManager' : '{E60B4439-691C-433D-B6D9-793044915A01}',
	'IErrorInfo' : '{1CF2B120-547D-101B-8E65-08002B2BD119}',
	'ICustomizationFields' : '{6388B1FB-C735-4D24-B1E4-97221B302461}',
	'ITest' : '{4777BC00-DDBD-4DBD-ACE6-BEAE379E2051}',
	'IBaseFieldEx' : '{8937B744-DE14-4E34-9A56-4E9E308B1863}',
	'IAuditRecordFactory' : '{10747648-0DF0-4B8C-8853-7408C70D883A}',
	'ICustomizationUser' : '{C10389DD-70AC-48F5-BCF0-9A1CBA5FCAD9}',
	'_IBaseFactory' : '{47E4C767-87DA-409F-A70E-04B3FCE28BCE}',
	'IBaseParam' : '{DC091A33-E1E8-4A17-8C55-529C09B0670B}',
	'IBaseFactory' : '{F13E4E0F-2BF2-41A2-97B1-06AD03204518}',
	'IBugFactory' : '{E46670F8-B7CE-4DA6-AC5F-AE1FB9181337}',
	'IAlertManager' : '{7A0B3B0B-60C4-4B84-8A35-1E9337AD055E}',
	'ITDErrorInfo' : '{DEBC6003-2ABE-4C93-A769-646F300A7986}',
	'IReqFactory2' : '{1F73BDFF-AC15-47FF-8264-88ADE733AAB7}',
	'ICustomizationUsersGroup' : '{7E12617E-3B01-42BD-A16F-13118038D0B7}',
	'IVersionItem' : '{38D402FA-9823-4D25-979E-62C377AC2E77}',
	'ICustomizationMailConditions' : '{2D05E2CD-60E9-4C56-A05D-D25872A00335}',
	'ITestSetFolder2' : '{18B58E14-B956-40A3-A37F-B8EF1136F238}',
	'ICustomizationTransitionRules' : '{365FA56C-AE1E-46EB-A776-6EDDB82BF290}',
	'IAlertable' : '{101CD251-91FB-4FB0-A440-DE755D905584}',
	'IBaseFactory2' : '{3E46BE58-4943-48AA-BA08-38EEBA837A04}',
	'IRuleManager' : '{FCBDBAD5-380A-43F5-B4D4-4CD988B0C924}',
	'ICustomizationUsersGroups' : '{1CD20510-7700-42B6-83AB-4AFC0419318D}',
	'ICustomizationPermissions' : '{7EE7D348-AED6-4B3C-8FC7-5D9D8EA8E4C9}',
	'ILinkable' : '{9B1CCF47-8DC6-4B9D-AC24-35DD3361A175}',
	'ILink' : '{79CFC0C1-7BB6-48CE-BC47-8074DDBCA542}',
	'ICustomizationMailCondition' : '{509C8491-47BF-454B-B899-852A55A17F46}',
	'ICustomizationField2' : '{2FA7E440-704F-4EFE-B500-EB93E7AFD294}',
	'ITDFilter2' : '{B14CE979-E8D8-426B-AFCF-B8AA9AFE267B}',
	'IComponentParam' : '{F65AD9CD-D5A3-4EC5-8904-017E4E9D2351}',
	'IAuditPropertyFactory' : '{A3D667D7-2285-4FE1-A799-8581C6D8DE70}',
	'IGraph' : '{ECCB1143-8914-497A-ACA0-8789CA64C2D6}',
	'IVCS' : '{6C66F3F1-52E8-4B39-840B-FB1C9800C676}',
	'_IBaseField' : '{DCD217D3-9BE9-4BED-B386-1A105D84E1F0}',
}

win32com.client.constants.__dicts__.append(constants.__dict__)

