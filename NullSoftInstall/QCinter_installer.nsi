; QCinter_installer.nsi
;
; This script is based on example1.nsi, but it remember the directory, 
; has uninstall support and (optionally) installs start menu shortcuts.
;
; It will install QCinter_installer.nsi into a directory that the user selects,
; http://nsis.sourceforge.net/Simple_tutorials
; http://nsis.sourceforge.net/Docs/Chapter4.html
; http://nsis.sourceforge.net/NsDialogs_FAQ

;--------------------------------


;--------------------------------
;Include Modern UI

  !include "MUI2.nsh"

;--------------------------------

; The name of the installer
Name "QCinter"

; The file to write
OutFile "QCinter.exe"

; The default installation directory
InstallDir $PROGRAMFILES\QCinter

; Registry key to check for directory (so if you install again, it will 
; overwrite the old one automatically)
InstallDirRegKey HKCU "Software\QCinter" ""

; Request application privileges for Windows Vista
RequestExecutionLevel admin

;--------------------------------
;Variables

  Var StartMenuFolder

;--------------------------------
;Interface Configuration

	; MUI Settings / Icons
	!define MUI_ICON "Graphic_Icon\icon\qci_ikona.ico"
	!define MUI_UNICON "Graphic_Icon\icon\qci_ikona_uninstall.ico"
	 
	; MUI Settings / Header
	!define MUI_HEADERIMAGE
	!define MUI_HEADERIMAGE_LEFT
	!define MUI_HEADERIMAGE_BITMAP "Graphic_Icon\Header\qci_install.bmp"
	!define MUI_ABORTWARNING
	!define MUI_HEADERIMAGE_UNBITMAP "Graphic_Icon\Header\qci_uninstall.bmp"
	 
  

;--------------------------------
;Pages

  !insertmacro MUI_PAGE_WELCOME
  !insertmacro MUI_PAGE_LICENSE "license_trial.txt"
  !insertmacro MUI_PAGE_DIRECTORY
  
  ;Start Menu Folder Page Configuration
  !define MUI_STARTMENUPAGE_REGISTRY_ROOT "HKCU" 
  !define MUI_STARTMENUPAGE_REGISTRY_KEY "Software\QCinter" 
  !define MUI_STARTMENUPAGE_REGISTRY_VALUENAME "Start Menu Folder"
  
  !insertmacro MUI_PAGE_STARTMENU Application $StartMenuFolder
  
  !insertmacro MUI_PAGE_INSTFILES
  
      # These indented statements modify settings for MUI_PAGE_FINISH
    !define MUI_FINISHPAGE_NOAUTOCLOSE
    !define MUI_FINISHPAGE_RUN
    !define MUI_FINISHPAGE_RUN_CHECKED
    !define MUI_FINISHPAGE_RUN_TEXT "Start a QCinter"
    !define MUI_FINISHPAGE_RUN_FUNCTION "LaunchLink"
    !define MUI_FINISHPAGE_SHOWREADME_CHECKED
    !define MUI_FINISHPAGE_SHOWREADME $INSTDIR\readme.txt
  !insertmacro MUI_PAGE_FINISH
  
  !insertmacro MUI_UNPAGE_WELCOME
  !insertmacro MUI_UNPAGE_CONFIRM
  !insertmacro MUI_UNPAGE_COMPONENTS
  !insertmacro MUI_UNPAGE_INSTFILES
  
  !insertmacro MUI_UNPAGE_FINISH

;--------------------------------
;Languages
 
  !insertmacro MUI_LANGUAGE "English"

;--------------------------------

;Installer Sections

Section "QCinter" inQCinter

  SetOutPath "$INSTDIR"
  
  ;ADD YOUR OWN FILES HERE...
  File /r "conf"
  File /r "main"
  ;	File /r "Result"
  File "readme.txt"
  
  
  ;Store installation folder
  WriteRegStr HKCU "Software\QCinter" "" $INSTDIR
  
  ;Create uninstaller
  WriteUninstaller "$INSTDIR\Uninstall.exe"
  
  
  !insertmacro MUI_STARTMENU_WRITE_BEGIN Application
    
  ;Create shortcuts
  CreateDirectory "$SMPROGRAMS\$StartMenuFolder"
  CreateShortCut "$SMPROGRAMS\$StartMenuFolder\Uninstall.lnk" "$INSTDIR\Uninstall.exe"
  CreateShortCut "$SMPROGRAMS\$StartMenuFolder\QCinter.lnk" "$INSTDIR\main\QCinter.exe" "" "$INSTDIR\main\qci_ikona.ico"
  
    
  !insertmacro MUI_STARTMENU_WRITE_END

SectionEnd
	

;--------------------------------
;Uninstaller Section

Section "un.QCinter" unQCinter

  ;ADD YOUR OWN FILES HERE...

  ; Remove registry keys
  DeleteRegKey /ifempty HKCU "Software\Modern UI Test"
  
  ; Remove files and uninstaller
  Delete $INSTDIR\Uninstall.exe
  Delete $INSTDIR\logfile.log
  Delete $INSTDIR\readme.txt

  ; Remove shortcuts, if any
  Delete "$SMPROGRAMS\QCinter\*.*"

  ; Remove directories used
  RMDir "$SMPROGRAMS\QCinter"
  RMDir /r "$INSTDIR\main"  ; this will remove all folders empty and not-empty
  
  RMDir "$INSTDIR"  ; this will remove our installation folder
  

  
  !insertmacro MUI_STARTMENU_GETFOLDER Application $StartMenuFolder
    
  Delete "$SMPROGRAMS\$StartMenuFolder\Uninstall.lnk"
  RMDir "$SMPROGRAMS\$StartMenuFolder"
SectionEnd

Section "un.User data" unUserData

  #Remove additional user settings, data

  RMDir /r "$INSTDIR\conf"  ; this will remove all folders empty and not-empty
  RMDir /r "$INSTDIR\Result"  ; this will remove all folders empty and not-empty

  RMDir "$INSTDIR"  ; this will remove our installation folder
  
SectionEnd
  
Function un.onInit
	; Unselect checkbox ${unUserData}
	!insertmacro UnselectSection ${unUserData}
FunctionEnd

Function LaunchLink
 ; MessageBox MB_OK "Reached LaunchLink $\r$\n \
  ;                 SMPROGRAMS: $SMPROGRAMS  $\r$\n \
   ;                Start Menu Folder: $STARTMENU_FOLDER $\r$\n \
    ;               InstallDirectory: $INSTDIR "
  ExecShell "" "$SMPROGRAMS\$StartMenuFolder\QCinter.lnk"
FunctionEnd


;Descriptions

  ;Language strings
  LangString DESC_inQCinter ${LANG_ENGLISH} "Main program"
  LangString DESC_unQCinter ${LANG_ENGLISH} "Main program"
  LangString DESC_unUserData ${LANG_ENGLISH} "User data, like configuration and result files "

  ;Assign language strings to Instal sections
  !insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN
    !insertmacro MUI_DESCRIPTION_TEXT ${inQCinter} $(DESC_inQCinter)	
  !insertmacro MUI_FUNCTION_DESCRIPTION_END
  
  ;Assign language strings to Uninstal sections
  !insertmacro MUI_UNFUNCTION_DESCRIPTION_BEGIN
	!insertmacro MUI_DESCRIPTION_TEXT ${unQCinter} $(DESC_unQCinter)
	!insertmacro MUI_DESCRIPTION_TEXT ${unUserData} $(DESC_unUserData)
  !insertmacro MUI_UNFUNCTION_DESCRIPTION_END
 
;--------------------------------


